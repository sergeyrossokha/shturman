using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

using Shturman.MultyLanguage;

namespace Shturman.Forms
{
    public partial class BrowseForShedule : Form
    {
        public BrowseForShedule()
        {
            InitializeComponent();
        }

        private void BrowseForShedule_Load(object sender, EventArgs e)
        {
            Text = Vocabruary.Translate("BrowseDlg.SheduleTitle");
            
            buttonCancel.Text = Vocabruary.Translate("Button.Cancel");
            buttonOk.Text = Vocabruary.Translate("Button.Ok");

            nameHeader.Text = Vocabruary.Translate("SheduleDescription.Caption.Caption");
            fileNameHeader.Text = Vocabruary.Translate("SheduleDescription.FileName.Caption");
            beginDateHeader.Text = Vocabruary.Translate("SheduleDescription.BeginDate.Caption");
            endDateHeader.Text = Vocabruary.Translate("SheduleDescription.EndDate.Caption");
            versionHeader.Text = Vocabruary.Translate("SheduleDescription.Version.Caption");
        }

        public void AddSheduleInfo(object[] infoArray)
        {
            ListViewItem lvi = new ListViewItem();
            lvi.Text = (string)infoArray[1];

            ListViewItem.ListViewSubItem fileNameItem = new ListViewItem.ListViewSubItem(lvi, (string)infoArray[2]);
            lvi.SubItems.Add(fileNameItem);
            ListViewItem.ListViewSubItem beginDateItem = new ListViewItem.ListViewSubItem(lvi, ((DateTime)infoArray[3]).ToShortDateString());
            lvi.SubItems.Add(beginDateItem);
            ListViewItem.ListViewSubItem endDateItem = new ListViewItem.ListViewSubItem(lvi, ((DateTime)infoArray[4]).ToShortDateString());
            lvi.SubItems.Add(endDateItem);
            ListViewItem.ListViewSubItem versionItem = new ListViewItem.ListViewSubItem(lvi, (string)infoArray[0]);
            lvi.SubItems.Add(versionItem);

            sheduleView.Items.Add(lvi);
        }

        public string SelectedShedule
        {
            get { return sheduleView.SelectedItems[0].SubItems[1].Text; }
        }

        private void sheduleView_DoubleClick(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
        }

        private void BrowseForShedule_Shown(object sender, EventArgs e)
        {
            sheduleView.Focus();
        }

        private void sheduleView_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
                DialogResult = DialogResult.OK;
        }

        private void sheduleView_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}