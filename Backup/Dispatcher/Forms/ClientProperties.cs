using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Configuration;
using System.IO;
using Shturman.MultyLanguage;

namespace Shturman.Forms
{
    public partial class ClientProperties : Form
    {
        private System.Configuration.Configuration cnf;
        private bool isChanged = false;

        public ClientProperties()
        {
            InitializeComponent();
            cnf = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        }

        private void ClientProperties_Load(object sender, EventArgs e)
        {
            this.Text = Vocabruary.Translate("ClientProperties.Titles");
            this.tabPage1.Text = Vocabruary.Translate("ClientProperties.DatabaseSettings");
            this.tabPage2.Text = Vocabruary.Translate("ClientProperties.ClientSettings");

            this.label5.Text = Vocabruary.Translate("ClientProperties.DatabaseSettings.Description");
            this.label6.Text = Vocabruary.Translate("ClientProperties.ClientSettings.Description");

            this.label4.Text = Vocabruary.Translate("ClientProperties.DatabaseSettings.ServerPwd");
            this.label3.Text = Vocabruary.Translate("ClientProperties.DatabaseSettings.ServerUser");
            this.label2.Text = Vocabruary.Translate("ClientProperties.DatabaseSettings.ServerPort");
            this.label1.Text = Vocabruary.Translate("ClientProperties.DatabaseSettings.Server");

            this.label7.Text = Vocabruary.Translate("ClientProperties.ClientSettings.Language");
            this.label8.Text = Vocabruary.Translate("ClientProperties.ClientSettings.ScriptDir");
            this.label9.Text = Vocabruary.Translate("ClientProperties.ClientSettings.XLogicBaseFile");

            this.button1.Text = Vocabruary.Translate("Button.Exit");
            this.button2.Text = Vocabruary.Translate("Button.Apply");

            this.textBox1.Text = cnf.AppSettings.Settings["Server"].Value;
            this.textBox2.Text = cnf.AppSettings.Settings["ServerPort"].Value;
            this.textBox3.Text = cnf.AppSettings.Settings["ServerUser"].Value;
            this.textBox4.Text = cnf.AppSettings.Settings["ServerPwd"].Value;

            this.textBox5.Text = cnf.AppSettings.Settings["XLogicBaseFile"].Value;
            this.textBox6.Text = cnf.AppSettings.Settings["ScriptDir"].Value;

            string[] languages = cnf.AppSettings.Settings["SupportedLanguages"].Value.Replace(" ", "").Split(',');
            comboBox1.Items.AddRange(languages);
            comboBox1.SelectedIndex = comboBox1.Items.IndexOf(cnf.AppSettings.Settings["Language"].Value);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            cnf.Save(ConfigurationSaveMode.Full);
            ConfigurationManager.RefreshSection("appSettings");
            isChanged = true;
            this.DialogResult = DialogResult.OK;
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            OpenFileDialog fd = new OpenFileDialog();
            fd.CheckFileExists = true;
            fd.Filter = "*.xml file|*.xml";
            fd.InitialDirectory = Path.GetDirectoryName( textBox5.Text );
            if (fd.ShowDialog(this) == DialogResult.OK)
            {
                textBox5.Text = fd.FileName;
            }
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.Description = "���� ����� � ���������";
            fbd.SelectedPath = textBox5.Text;
            if (fbd.ShowDialog(this) == DialogResult.OK)
            {
                textBox6.Text = fbd.SelectedPath;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            cnf.AppSettings.Settings["Server"].Value = textBox1.Text;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
              cnf.AppSettings.Settings["ServerPort"].Value = this.textBox2.Text;
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {
            cnf.AppSettings.Settings["ServerUser"].Value = this.textBox3.Text;
        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {
             cnf.AppSettings.Settings["ServerPwd"].Value = this.textBox4.Text;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            cnf.AppSettings.Settings["Language"].Value = comboBox1.Text;
        }

        public bool Changed
        {
            get { return isChanged; }
        }
    }
}