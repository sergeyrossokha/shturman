namespace Shturman.Forms
{
    partial class BrowseForShedule
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.sheduleView = new System.Windows.Forms.ListView();
            this.nameHeader = new System.Windows.Forms.ColumnHeader();
            this.fileNameHeader = new System.Windows.Forms.ColumnHeader();
            this.beginDateHeader = new System.Windows.Forms.ColumnHeader();
            this.endDateHeader = new System.Windows.Forms.ColumnHeader();
            this.versionHeader = new System.Windows.Forms.ColumnHeader();
            this.buttonOk = new System.Windows.Forms.Button();
            this.buttonCancel = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // sheduleView
            // 
            this.sheduleView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.nameHeader,
            this.fileNameHeader,
            this.beginDateHeader,
            this.endDateHeader,
            this.versionHeader});
            this.sheduleView.FullRowSelect = true;
            this.sheduleView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.sheduleView.Location = new System.Drawing.Point(12, 12);
            this.sheduleView.MultiSelect = false;
            this.sheduleView.Name = "sheduleView";
            this.sheduleView.Size = new System.Drawing.Size(594, 173);
            this.sheduleView.TabIndex = 0;
            this.sheduleView.UseCompatibleStateImageBehavior = false;
            this.sheduleView.View = System.Windows.Forms.View.Details;
            this.sheduleView.DoubleClick += new System.EventHandler(this.sheduleView_DoubleClick);
            this.sheduleView.SelectedIndexChanged += new System.EventHandler(this.sheduleView_SelectedIndexChanged);
            this.sheduleView.KeyUp += new System.Windows.Forms.KeyEventHandler(this.sheduleView_KeyUp);
            // 
            // nameHeader
            // 
            this.nameHeader.Width = 120;
            // 
            // fileNameHeader
            // 
            this.fileNameHeader.Width = 125;
            // 
            // beginDateHeader
            // 
            this.beginDateHeader.Width = 129;
            // 
            // endDateHeader
            // 
            this.endDateHeader.Width = 113;
            // 
            // versionHeader
            // 
            this.versionHeader.Width = 88;
            // 
            // buttonOk
            // 
            this.buttonOk.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonOk.Location = new System.Drawing.Point(450, 191);
            this.buttonOk.Name = "buttonOk";
            this.buttonOk.Size = new System.Drawing.Size(75, 23);
            this.buttonOk.TabIndex = 1;
            this.buttonOk.Text = "button1";
            this.buttonOk.UseVisualStyleBackColor = true;
            // 
            // buttonCancel
            // 
            this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.buttonCancel.Location = new System.Drawing.Point(531, 191);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonCancel.TabIndex = 2;
            this.buttonCancel.Text = "button2";
            this.buttonCancel.UseVisualStyleBackColor = true;
            // 
            // BrowseForShedule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(618, 223);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.buttonOk);
            this.Controls.Add(this.sheduleView);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "BrowseForShedule";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BrowseForShedule";
            this.Shown += new System.EventHandler(this.BrowseForShedule_Shown);
            this.Load += new System.EventHandler(this.BrowseForShedule_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListView sheduleView;
        private System.Windows.Forms.Button buttonOk;
        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.ColumnHeader nameHeader;
        private System.Windows.Forms.ColumnHeader fileNameHeader;
        private System.Windows.Forms.ColumnHeader beginDateHeader;
        private System.Windows.Forms.ColumnHeader endDateHeader;
        private System.Windows.Forms.ColumnHeader versionHeader;
    }
}