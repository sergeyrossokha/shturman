using System;
using System.Collections;

using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

using Shturman.Shedule.Objects;

namespace Shturman.Pr1
{
	/// <summary>
	/// Summary description for XMLObjectReader.
	/// </summary>
	public class XMLObjectReader
	{
		public Hashtable _restoredObjects = new Hashtable();
		Queue queue = new Queue();

		private class QueueObject
		{
			public QueueObject(int queueKey, IList objectToChange)
			{
				QueueKey = queueKey;
				ObjectToChange = objectToChange;
			}

			public int QueueKey;
			public IList ObjectToChange;
		}

		XmlTextReader _reader;

		public XMLObjectReader(string filename)
		{
			_reader = new XmlTextReader(filename);
		}
		
		private object ReadObjectReference(string attrName)
		{
			string ref_val = _reader.GetAttribute(attrName);
			if(ref_val!=""&& ref_val!=null)
			{
				long refToObj = long.Parse(ref_val);
				if (_restoredObjects.Contains(refToObj))
					return _restoredObjects[refToObj];
			}
			
			return null;
		}

		
		private void RestoreReference()
		{
			while(queue.Count != 0)
			{
				QueueObject qo = queue.Dequeue() as QueueObject;
				
				if (qo.ObjectToChange is IList)
				{
					IList listToChange = qo.ObjectToChange as IList;
					listToChange[qo.QueueKey] = _restoredObjects[(long)listToChange[(int)qo.QueueKey]];
				}
				else
				{
					//qo.ObjectToChange = _restoredObjects[(long)qo.QueueKey];
				}
			}
		}

	
		private IList ReadObjectReferenceList(string attrName)
		{
			ArrayList array = new ArrayList();
			_reader.Read();
			if(_reader.NodeType == XmlNodeType.Element && _reader.Name == attrName && !_reader.IsEmptyElement)
			{
				while(_reader.Read())
				{
					if(_reader.NodeType == XmlNodeType.Text)
					{
						array.Add( long.Parse(_reader.Value));
					}
					if(_reader.NodeType == XmlNodeType.EndElement && _reader.Name == attrName)
					{
						break;
					}
				}
			}
			
			return array;
		}

	
		public AcademicStatus ReadAcademicStatus(out long uid )
		{
			uid =  long.Parse( _reader.GetAttribute("uid"));
			
			AcademicStatus ast = new AcademicStatus();
			
			ast.MnemoCode = _reader.GetAttribute("MnemoCode");
			ast.Name = _reader.GetAttribute("Name");
			
			return ast;
		}

		
		public Building ReadBuilding(out long uid)
		{
			uid =  long.Parse( _reader.GetAttribute("uid"));
			
			Building bld = new Building();
			
			bld.MnemoCode = _reader.GetAttribute("MnemoCode");
			bld.Name = _reader.GetAttribute("Name");
            
			return bld;
		}

		
		public Department ReadDepartment(out long uid)
		{
			uid =  long.Parse( _reader.GetAttribute("uid"));
			Department dpt = new Department();
			
			dpt.MnemoCode = _reader.GetAttribute("MnemoCode");
			dpt.Name = _reader.GetAttribute("Name");
			dpt.Telephone = _reader.GetAttribute("Tel");
            
			// ReadReference
			dpt.DepartmentFaculty = (Faculty)this.ReadObjectReference("DepartmentFaculty_ref");
			foreach(object obj in this.ReadObjectReferenceList("DepartmentSubjects"))
			{
				dpt.DepartmentSubjects.Add(obj);
				queue.Enqueue(new QueueObject(dpt.DepartmentSubjects.Count-1, dpt.DepartmentSubjects));
			}

			foreach(object obj in this.ReadObjectReferenceList("DepartmentTutors"))
			{
				dpt.DepartmentTutors.Add(obj);
				queue.Enqueue(new QueueObject(dpt.DepartmentTutors.Count-1, dpt.DepartmentTutors));
			}

			return dpt;
		}

		
		public Faculty ReadFaculty(out long uid)
		{
			uid =  long.Parse( _reader.GetAttribute("uid"));
			
			Faculty fcl = new Faculty();
			
			fcl.MnemoCode = _reader.GetAttribute("MnemoCode");
			fcl.Name = _reader.GetAttribute("Name");
            
			return fcl;
		}


		public Group ReadGroup(out long uid)
		{
			uid =  long.Parse( _reader.GetAttribute("uid"));
			
			Group grp = new Group();

			grp.MnemoCode = _reader.GetAttribute("MnemoCode");
			grp.GroupCapacity = byte.Parse( _reader.GetAttribute("GroupCapacity") );
			
			// ReadReference
			grp.GroupFaculty = (Faculty)this.ReadObjectReference("Faculty_ref");
			grp.GroupDepartment = (Department)this.ReadObjectReference("Department_ref");
			grp.GroupSpecialty = (Specialty)this.ReadObjectReference("Specialty_ref");
			
			return grp;
		}

		
		public LectureHall ReadLectureHall(out long uid)
		{
			uid =  long.Parse( _reader.GetAttribute("uid"));
			
			LectureHall lhl = new LectureHall();
			lhl.MnemoCode = _reader.GetAttribute("MnemoCode");
			lhl.Capacity = byte.Parse( _reader.GetAttribute("Capacity") );
			
			// ReadReference
			lhl.HallType = (LectureHallType)this.ReadObjectReference("HallType_ref");
			lhl.Department = (Department)this.ReadObjectReference("Department_ref");
			lhl.LectureHallBuilding = (Building)this.ReadObjectReference("Build_ref");

			return lhl;
		}

		
		public LectureHallType ReadLectureHallType(out long uid)
		{
			uid =  long.Parse( _reader.GetAttribute("uid"));
			
			LectureHallType lht = new LectureHallType();
			lht.MnemoCode = _reader.GetAttribute("MnemoCode");
			lht.Name = _reader.GetAttribute("Name");
			lht.FlatColor =  System.Drawing.Color.FromArgb(int.Parse(_reader.GetAttribute("Color")));

			return lht;
		}
		
		
		public Specialty ReadSpecialty(out long uid)
		{
			uid =  long.Parse( _reader.GetAttribute("uid"));
			
			Specialty spl = new Specialty();
			spl.MnemoCode = _reader.GetAttribute("MnemoCode");
			spl.Name = _reader.GetAttribute("Name");
			spl.ShortName = _reader.GetAttribute("ShortName");
			spl.SpecialtyFaculty = (Faculty)this.ReadObjectReference("Faculty_ref");
			spl.SpecialtyDepartment = (Department)this.ReadObjectReference("Department_ref");
			
			return spl;
		}

		
		public Post ReadPost(out long uid)
		{
			uid =  long.Parse( _reader.GetAttribute("uid"));
			
			Post pst = new Post();
			pst.MnemoCode = _reader.GetAttribute("MnemoCode");
			pst.Name = _reader.GetAttribute("Name");
			return pst;
		}
		
		
		public StudyCycles ReadStudyCycles(out long uid)
		{
			uid =  long.Parse( _reader.GetAttribute("uid"));
			
			StudyCycles scl = new StudyCycles();
			scl.MnemoCode = _reader.GetAttribute("MnemoCode");
			scl.Name = _reader.GetAttribute("Name");
			scl.ShortName = _reader.GetAttribute("ShortName");
			return scl;
		}
		
		
		public StudyDay ReadStudyDay(out long uid)
		{
			uid =  long.Parse( _reader.GetAttribute("uid"));
			
			StudyDay sdy = new StudyDay();
			sdy.MnemoCode = _reader.GetAttribute("MnemoCode");
			sdy.Name = _reader.GetAttribute("Name");
			sdy.ShortName = _reader.GetAttribute("ShortName");
			return sdy;
		}
		
		
		public StudyHour ReadStudyHour(out long uid)
		{
			uid =  long.Parse( _reader.GetAttribute("uid"));
			StudyHour shr = new StudyHour();
			shr.MnemoCode = _reader.GetAttribute("MnemoCode");
			shr.FromTime = TimeSpan.Parse( _reader.GetAttribute("FromTime"));
			shr.ToTime = TimeSpan.Parse( _reader.GetAttribute("ToTime"));
			return shr;
		}

		
		public StudyType ReadStudyType(out long uid)
		{
			uid =  long.Parse( _reader.GetAttribute("uid"));
			
			StudyType stp = new StudyType();
			stp.MnemoCode = _reader.GetAttribute("MnemoCode");
			stp.Name = _reader.GetAttribute("Name");
			return stp;
		}
		
		
		public Subject ReadSubject(out long uid)
		{
			uid =  long.Parse( _reader.GetAttribute("uid"));
			
			Subject sbj = new Subject();
			sbj.MnemoCode = _reader.GetAttribute("MnemoCode");
			sbj.Name = _reader.GetAttribute("Name");
			sbj.ShortName = _reader.GetAttribute("ShortName");
			return sbj;
		}
		
		
		public Tutor ReadTutor(out long uid)
		{
			uid =  long.Parse( _reader.GetAttribute("uid"));
			
			Tutor ttr = new Tutor();
			ttr.MnemoCode = _reader.GetAttribute("MnemoCode");
			ttr.TutorSurname =  _reader.GetAttribute("Surname");
			ttr.TutorName = _reader.GetAttribute("Name");
			ttr.TutorPatronimic =  _reader.GetAttribute("Patronimic");

			ttr.TutorPost = (Post)this.ReadObjectReference("Post_ref");
			ttr.TutorStatus = (AcademicStatus)this.ReadObjectReference("AcademicStatus_ref");

			foreach(object obj in this.ReadObjectReferenceList("TutorSubjects"))
			{
				ttr.TutorSubjects.Add(obj);	
				queue.Enqueue(new QueueObject(ttr.TutorSubjects.Count-1, ttr.TutorSubjects));
			}
			foreach(object obj in this.ReadObjectReferenceList("DepartmentTutors"))
			{
				ttr.TutorDepartments.Add(obj);
				queue.Enqueue(new QueueObject(ttr.TutorDepartments.Count-1, ttr.TutorDepartments));
			}

			return ttr;
		}


        public ShturmanUser ReadUser(out long uid)
		{
			uid =  long.Parse( _reader.GetAttribute("uid"));

            ShturmanUser usr = new ShturmanUser();
			usr.UserUid = long.Parse(_reader.GetAttribute("id"));
			usr.Name = _reader.GetAttribute("Name");
			usr.Password = _reader.GetAttribute("Pwd");
			return usr;
		}

		
		public long ReadObject(out object obj, string ObjectName)
		{
			long uid = -1;
			
			switch(ObjectName)
			{
				case "AcademicStatus":
					obj = ReadAcademicStatus( out uid );
					break;
				case "Build":
					obj = ReadBuilding( out uid );
					break;
				case "Faculty":
					obj = ReadFaculty( out uid );
					break;
				case "Subject":
					obj = ReadSubject( out uid );
					break;
				case "LectureHallType":
					obj = ReadLectureHallType( out uid );
					break;
				case "LectureHall":
					obj = ReadLectureHall( out uid );
					break;
				case "Post":
					obj = ReadPost( out uid );
					break;
				case "User":
					obj = ReadUser( out uid );
					break;
				case "StudyType":
					obj = ReadStudyType( out uid );
					break;
				case "StudyHour":
					obj = ReadStudyHour( out uid );
					break;
				case "StudyDay":
					obj = ReadStudyDay( out uid );
					break;
				case "StudyCycles":
					obj = ReadStudyCycles( out uid );
					break;
				case "Specialty":
					obj = ReadSpecialty( out uid );
					break;
				case "Department":
					obj = ReadDepartment( out uid );
					break;
				case "Tutor":
					obj = ReadTutor( out uid );
					break;
				case "Group":
					obj = ReadGroup( out uid );
					break;
				default:
					obj= null;
					break;
			}
			return uid;
		}

		
		public void ReadAllObject()
		{	
			while(_reader.Read())
			{
				if(_reader.NodeType == XmlNodeType.Element)
				{
					object currentObj;
					long uid = ReadObject(out currentObj, _reader.Name);
					if(uid != -1)
						this._restoredObjects.Add( uid, currentObj);
				}
			}

			this.RestoreReference();

            _reader.Close();
		} 
	}
}
