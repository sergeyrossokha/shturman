using System;
using System.CodeDom.Compiler;
using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Security.Policy;
using System.Xml;

using Microsoft.CSharp;

namespace Shturman.Script
{
	/// <summary>
	/// Summary description for DotNetScriptCompiler.
	/// </summary>
	public class DotNetScriptCompiler
	{
		public DotNetScriptCompiler()
		{
		}

		public virtual Assembly Compile(string scriptFileName, out string menuPath)
		{
			XmlDocument xmlDocument = new XmlDocument();
			xmlDocument.Load(scriptFileName);
            if (xmlDocument.DocumentElement.Attributes.Count > 0)
                menuPath = xmlDocument.DocumentElement.Attributes["MenuLocation"].Value;
            else
                menuPath = "";
			ArrayList referencesList = new ArrayList();
			foreach(XmlNode xmlNode in xmlDocument.GetElementsByTagName("reference"))
				referencesList.Add( xmlNode.Attributes["assembly"].Value );
			
			string source = xmlDocument.InnerText;

			return this.Compile(referencesList, source, "", scriptFileName+".dll");
		}

        public virtual Assembly Compile(string scriptName, string sourse, ArrayList referencesList, out CompilerResults results)
        { 
            return this.Compile(referencesList, sourse, "", scriptName+".dll", out results);
        }

        public virtual Assembly Compile(ArrayList references, string source, string compilerOptions, string fileName)
        {
            CompilerResults results;
            return Compile(references, source, compilerOptions, fileName, out results);
        }

		public virtual Assembly Compile(ArrayList references, string source, string compilerOptions, string fileName, out CompilerResults results)
		{
			Assembly generatedAssembly=null;

			// get a compiler for the appropriate language.
			CodeDomProvider codeProvider= new CSharpCodeProvider();

			// setup the compiler defaults.  Maybe make this application definable at some point.
			ICodeCompiler compiler=codeProvider.CreateCompiler();
			CompilerParameters compilerParams=new CompilerParameters();
			
			compilerParams.CompilerOptions="/target:library "+compilerOptions;
			
			string fileDir=Path.GetDirectoryName(fileName);
			
			compilerParams.CompilerOptions+=" \"/lib:"+Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)+";"+fileDir+"\"";
			Trace.WriteLine("Compiling inline code with COMPILER OPTIONS:\r\n" + compilerParams.CompilerOptions);

			compilerParams.GenerateExecutable=false;
			compilerParams.GenerateInMemory=true;
			compilerParams.IncludeDebugInformation=false;
			compilerParams.ReferencedAssemblies.Add("mscorlib.dll");
			compilerParams.ReferencedAssemblies.Add("System.dll");
			
			if (references != null)
				compilerParams.ReferencedAssemblies.AddRange((string[])references.ToArray(typeof(string)));

			compilerParams.Evidence=GetEvidence(fileName);

			// compile the source code
			results=compiler.CompileAssemblyFromSource(compilerParams, source);
			if (results.Errors.Count > 0)
			{
				// uh oh.  Errors!
				Trace.WriteLine("Compiler errors:");
				foreach(CompilerError error in results.Errors)
				{
					Trace.WriteLine(error.ErrorText);
				}
				// as per Michael's request:
				Trace.WriteLine("End of Errors"); // only really here for putting a breakpoint on so we can catch the errors easier
			}
			else
			{
				// get the resultant assembly.
				generatedAssembly=results.CompiledAssembly;
			}
			return generatedAssembly;
		}

		protected virtual Evidence GetEvidence(string fileName)
		{
			Evidence evidence=new Evidence();
			evidence.AddHost(Zone.CreateFromUrl(Path.Combine(Environment.CurrentDirectory, fileName)));
			return evidence;
		}
	}
}
