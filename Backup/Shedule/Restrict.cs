using System.ComponentModel;
using Shturman.Nestor.DataAttributes;
using Shturman.Shedule.Objects;

namespace Shturman.Shedule
{
	/// <summary>
	/// ��������� ����� �����������
	/// </summary>
	public class Restrict
	{
		private StudyCycles week;
		private StudyDay day;
		private StudyHour hour;

		private string _restrictMessage = "";

		public Restrict()
		{
			_restrictMessage = "";
		}

		public Restrict(string restrictMessage)
		{
			_restrictMessage = restrictMessage;
		}

		[PropertyCaption("���� ������")]
		[PropertyVisible(true)]
		[PropertyIndex(1)]
		[PropertyReference(typeof(StudyDay))]
		[PropertyNotNull(true)]
		[Description("������� ���� ������")]
		public StudyDay RestrictDay
		{
			get{ return day; }
			set{ day = value; }
		}
		
		[PropertyCaption("����� �������")]
		[PropertyVisible(true)]
		[PropertyIndex(2)]
		[PropertyReference(typeof(StudyHour))]
		[PropertyNotNull(true)]
		[Description("����� �������")]
		public StudyHour RestrictPair
		{
			get{ return hour; }
			set{ hour = value; }
		}

		[PropertyCaption("��� ������")]
		[PropertyVisible(false)]
		[PropertyIndex(3)]
		[PropertyReference(typeof(StudyCycles))]
		[PropertyNotNull(true)]
		[Description("������� ��������� ����")]
		public StudyCycles RestrictWeek
		{
			get{ return week; }
			set{ week = value; }
		}

		[PropertyCaption("����������")]
		[PropertyVisible(true)]
		[PropertyIndex(4)]
		[Description("����������, ������� �����������")]
		public string Comment
		{
			get {return _restrictMessage;}
			set {_restrictMessage = value;}
		}

		public override string ToString()
		{
			return RestrictDay.ToString()+" "+RestrictWeek.ToString()+" "+RestrictPair.ToString()+" [ "+Comment+" ]";
		}

	}
}
