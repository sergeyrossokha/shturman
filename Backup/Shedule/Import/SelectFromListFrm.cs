using System;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Shturman.Nestor.DataEditors;
using Shturman.Nestor.Interfaces;

namespace Shturman.Shedule.Import
{
	/// <summary>
	/// Summary description for SelectFromListFrm.
	/// </summary>
	public class SelectFromListFrm : Form
	{
		private Label objectText;
		private Panel panel1;
		private ListBox listBox1;
		private Button button1;
		private Button button2;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private Container components = null;
		private IObjectStorage _storage;
		private IList _filteredObjects;
		private IList _allObjects;
		private object _newobject;
		private CheckBox checkBox1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.Label labelExistObjects;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.TextBox textBox1;

		private object _selectedobject = null;

		public SelectFromListFrm(IObjectStorage storage, IList filteredObjects, IList allObjects, object newobject)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
			
			_storage = storage;
			_filteredObjects = filteredObjects;
			_allObjects = allObjects;
			_newobject = newobject;

			this.listBox1.DataSource = _filteredObjects;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.objectText = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.button1 = new System.Windows.Forms.Button();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.button2 = new System.Windows.Forms.Button();
			this.listBox1 = new System.Windows.Forms.ListBox();
			this.panel2 = new System.Windows.Forms.Panel();
			this.labelExistObjects = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.button3 = new System.Windows.Forms.Button();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.panel1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// objectText
			// 
			this.objectText.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.objectText.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(204)));
			this.objectText.Location = new System.Drawing.Point(0, 24);
			this.objectText.Name = "objectText";
			this.objectText.Size = new System.Drawing.Size(342, 56);
			this.objectText.TabIndex = 0;
			this.objectText.Text = "Text";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.button3);
			this.panel1.Controls.Add(this.button1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 351);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(342, 32);
			this.panel1.TabIndex = 1;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(256, 5);
			this.button1.Name = "button1";
			this.button1.TabIndex = 0;
			this.button1.Text = "������";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// checkBox1
			// 
			this.checkBox1.Location = new System.Drawing.Point(232, 8);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.TabIndex = 2;
			this.checkBox1.Text = "�������� ���";
			this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(8, 228);
			this.button2.Name = "button2";
			this.button2.TabIndex = 1;
			this.button2.Text = "��������";
			this.button2.Click += new System.EventHandler(this.button2_Click);
			// 
			// listBox1
			// 
			this.listBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.listBox1.IntegralHeight = false;
			this.listBox1.Location = new System.Drawing.Point(8, 32);
			this.listBox1.Name = "listBox1";
			this.listBox1.Size = new System.Drawing.Size(328, 192);
			this.listBox1.TabIndex = 2;
			this.listBox1.DoubleClick += new System.EventHandler(this.button1_Click);
			// 
			// panel2
			// 
			this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panel2.Controls.Add(this.listBox1);
			this.panel2.Controls.Add(this.checkBox1);
			this.panel2.Controls.Add(this.button2);
			this.panel2.Controls.Add(this.labelExistObjects);
			this.panel2.Controls.Add(this.textBox1);
			this.panel2.Location = new System.Drawing.Point(0, 88);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(342, 255);
			this.panel2.TabIndex = 3;
			// 
			// labelExistObjects
			// 
			this.labelExistObjects.Location = new System.Drawing.Point(8, 9);
			this.labelExistObjects.Name = "labelExistObjects";
			this.labelExistObjects.Size = new System.Drawing.Size(100, 24);
			this.labelExistObjects.TabIndex = 3;
			this.labelExistObjects.Text = "������� ������";
			this.labelExistObjects.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(-1, 0);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(120, 24);
			this.label1.TabIndex = 3;
			this.label1.Text = "�� ���������� �����";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// button3
			// 
			this.button3.DialogResult = System.Windows.Forms.DialogResult.Abort;
			this.button3.Location = new System.Drawing.Point(7, 5);
			this.button3.Name = "button3";
			this.button3.TabIndex = 1;
			this.button3.Text = "³������";
			// 
			// textBox1
			// 
			this.textBox1.BackColor = System.Drawing.SystemColors.GrayText;
			this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.textBox1.Location = new System.Drawing.Point(96, 231);
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(240, 20);
			this.textBox1.TabIndex = 4;
			this.textBox1.Text = "����� ������";
			this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
			this.textBox1.Leave += new System.EventHandler(this.textBox1_Leave);
			this.textBox1.Enter += new System.EventHandler(this.textBox1_Enter);
			// 
			// SelectFromListFrm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(342, 383);
			this.Controls.Add(this.panel2);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.objectText);
			this.Controls.Add(this.label1);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.Name = "SelectFromListFrm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "SelectFromListFrm";
			this.panel1.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void button1_Click(object sender, EventArgs e)
		{
			_selectedobject = listBox1.SelectedItem;
			this.DialogResult = DialogResult.OK;
		}

		private void button2_Click(object sender, EventArgs e)
		{
			ObjectEditor oe = new ObjectEditor( _storage );
			oe.SelectedObject = _newobject;
			if (oe.ShowDialog(this) == DialogResult.OK)
			{
				this._storage.Save( _newobject );
				int newIndex = _allObjects.Add( _newobject );

				this.listBox1.DataSource = null;

				if (this.checkBox1.Checked)
				{
					this.listBox1.DataSource = this._allObjects;
					this.listBox1.SetSelected(newIndex, true);
				}
				else
					this.listBox1.DataSource = this._filteredObjects;

			}
		}

		private void checkBox1_CheckedChanged(object sender, EventArgs e)
		{
			if (this.checkBox1.Checked)
				this.listBox1.DataSource = this._allObjects;
			else
				this.listBox1.DataSource = this._filteredObjects;
		}

		private void textBox1_TextChanged(object sender, System.EventArgs e)
		{
			if ((sender as TextBox).Focused)
			{
				int index = this.listBox1.FindString( this.textBox1.Text, 0);
				if (index != -1)
					this.listBox1.SetSelected(index, true);
			}
		}

		private void textBox1_Leave(object sender, System.EventArgs e)
		{
			(sender as TextBox).Text = "����� ������";
			(sender as TextBox).BackColor = System.Drawing.Color.Gray;
		}

		private void textBox1_Enter(object sender, System.EventArgs e)
		{
			(sender as TextBox).Text = "";
			(sender as TextBox).BackColor = System.Drawing.Color.White;
		}

		public string ObjectText
		{
			get { return this.objectText.Text; }
			set { this.objectText.Text = value; }
		}

		public object SelectedObject
		{
			get { return _selectedobject; }
			set { _selectedobject = value; }
		}
	}
}
