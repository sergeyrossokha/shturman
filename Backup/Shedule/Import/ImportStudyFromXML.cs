// My Assemblies
using System.Collections;
using System.Windows.Forms;
using System.Xml;
using Shturman.MultyLanguage;
using Shturman.Nestor.DataBrowser;
using Shturman.Nestor.Interfaces;
using Shturman.Shedule.Objects.Searching;
using Shturman.Shedule.Objects;

namespace Shturman.Shedule.Import
{
	/// <summary>
	/// Summary description for ImportStudyFromXML.
	/// </summary>
	public class ImportStudyFromXML
	{
		private IObjectStorage _storage;

		private Hashtable _Groups = new Hashtable();
		private Hashtable _RecognizedObjects = new Hashtable();
		private ArrayList _Studies = new ArrayList();

		public ImportStudyFromXML(IObjectStorage storage)
		{
			_storage = storage;
		}

		public void Import(string filename)
		{
			XmlTextReader xmlrd = new XmlTextReader(filename);

			xmlrd.MoveToContent();
			if (xmlrd.HasAttributes)
			{
				/* �� ����� ���� ��������� ������ �������� ��������*/
			}

			while (xmlrd.Read())
			{
				/* ������ ���� */
				if (xmlrd.NodeType == XmlNodeType.Element && xmlrd.Name == "Group")
				{
					/* ������ �������� �����*/
					int groupRef = int.Parse(xmlrd.GetAttribute("id"));
					string groupNumber = xmlrd.GetAttribute("Name");
//					int groupFaculty = int.Parse("" + groupNumber[0]);
					byte groupCapacity = byte.Parse(xmlrd.GetAttribute("Count"));
					/* �� ���� �������� ������ �� ������� �� ���������� � ��� 
					 * � ���� ���� ��� �������� �� ��� ��
					 * */
					Group gr = null;
					if (!_RecognizedObjects.ContainsKey(groupNumber))
					{
						IList groupList = this._storage.QueryObjects(new GroupSearchByNumber(groupNumber));
						IList allGroupList = this._storage.ObjectList(typeof (Group));


						if (groupList.Count == 1)
						{
							gr = groupList[0] as Group;
						}
						else
						{
							Group _gr = new Group();
							_gr.MnemoCode = groupNumber;
							_gr.GroupCapacity = groupCapacity;

							SelectFromListFrm selfrm = new SelectFromListFrm(_storage, groupList, allGroupList, _gr);

							selfrm.Text = Vocabruary.Translate("SelectFromList.GroupTitle");
							selfrm.ObjectText = groupNumber;
							if (selfrm.ShowDialog() == DialogResult.OK)
							{
								gr = selfrm.SelectedObject as Group;
								_RecognizedObjects.Add(groupNumber, gr);
							}
						}
						/* ������� ������� �������� */
						gr.GroupCapacity = groupCapacity;
					}
					else
					{
						gr = _RecognizedObjects[groupNumber] as Group;
					}
					/* ������ ������ �� ��� ������� */
					_Groups.Add(groupRef, gr);
				}

				if (xmlrd.NodeType == XmlNodeType.Element && xmlrd.Name == "TrimPlanItem")
				{
					/* ������ ���� �������� */
					StudyLoading stdld = new StudyLoading();

					/* ���������� ��������� */
					string subjectText = xmlrd.GetAttribute("Subject");
					if (!_RecognizedObjects.ContainsKey(subjectText))
					{
						IList subjectList = this._storage.QueryObjects(new SubjectSearchByName(subjectText));
						IList allSubjectList = this._storage.ObjectList(typeof (Subject));
						if (subjectList.Count == 1)
						{
							stdld.SubjectName = subjectList[0] as Subject;
						}
						else
						{
							Subject subject = new Subject();
							subject.MnemoCode = subjectText;
							subject.Name = subjectText;

							SelectFromListFrm selfrm = new SelectFromListFrm(_storage, subjectList, allSubjectList, subject);
							selfrm.Text = Vocabruary.Translate("SelectFromList.SubjectTitle");
							selfrm.ObjectText = subjectText;
							if (selfrm.ShowDialog() == DialogResult.OK)
							{
								stdld.SubjectName = selfrm.SelectedObject as Subject;
								_RecognizedObjects.Add(subjectText, stdld.SubjectName);
							}
						}
					}
					else
					{
						stdld.SubjectName = _RecognizedObjects[subjectText] as Subject;
					}
					/* ��� ������� */
					string studyFormText = xmlrd.GetAttribute("StudyForm") + ".";
					if (!_RecognizedObjects.ContainsKey(studyFormText))
					{
						IList studyFormList = this._storage.QueryObjects(new StudyTypeSearchByName(studyFormText));
						IList allStudyFormList = this._storage.ObjectList(typeof (StudyType));
						if (studyFormList.Count == 1)
						{
							stdld.StudyForm = studyFormList[0] as StudyType;
						}
						else
						{
							StudyType studyForm = new StudyType();
							studyForm.MnemoCode = studyFormText;
							studyForm.Name = studyFormText;

							SelectFromListFrm selfrm = new SelectFromListFrm(_storage, studyFormList, allStudyFormList, studyForm);
							selfrm.Text = Vocabruary.Translate("SelectFromList.StudyTypeTitle");
							selfrm.ObjectText = studyFormText;
							if (selfrm.ShowDialog() == DialogResult.OK)
							{
								stdld.StudyForm = selfrm.SelectedObject as StudyType;
								_RecognizedObjects.Add(studyFormText, stdld.StudyForm);
							}
						}
					}
					else
					{
						stdld.StudyForm = _RecognizedObjects[studyFormText] as StudyType;
					}

					string departmentNumber = xmlrd.GetAttribute("Department");
					if (!_RecognizedObjects.ContainsKey(departmentNumber))
					{
						IList departmentList = this._storage.QueryObjects(new DepartmentSearchByNumber(departmentNumber));
						IList allDepartmentList = this._storage.ObjectList(typeof (Department));
						if (departmentList.Count == 1)
						{
							stdld.Dept = departmentList[0] as Department;
						}
						else
						{
							Department department = new Department();
							department.MnemoCode = departmentNumber;

							SelectFromListFrm selfrm = new SelectFromListFrm(_storage, departmentList, allDepartmentList, department);
							selfrm.Text = Vocabruary.Translate("SelectFromList.DepartmentTitle");
							selfrm.ObjectText = departmentNumber;
							if (selfrm.ShowDialog() == DialogResult.OK)
							{
								stdld.Dept = selfrm.SelectedObject as Department;
								_RecognizedObjects.Add(departmentNumber, stdld.StudyForm);
							}
						}
					}
					else
					{
						stdld.Dept = _RecognizedObjects[departmentNumber] as Department;
					}

					stdld.HourByWeek = int.Parse(xmlrd.GetAttribute("Hours"));

					while (xmlrd.Read())
					{
						if (xmlrd.NodeType == XmlNodeType.Element && xmlrd.Name == "Group")
						{
							/*������ ����� �� �����*/
							int groupid = int.Parse(xmlrd.GetAttribute("ref"));
							if (_Groups.ContainsKey(groupid))
								stdld.Groups.Add(_Groups[groupid]);

						}

						if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "TrimPlanItem")
						{
							/* ������ ���������� ������ ����� �� ������ ����������� ������ */
							if (stdld.StudyForm != null)
								if ((stdld.Groups.Count > 1) && (stdld.StudyForm.MnemoCode == "��." || stdld.StudyForm.MnemoCode == "��."))
								{
									foreach (Group group in stdld.Groups)
									{
										StudyLoading stdldClone = stdld.Clone() as StudyLoading;
										stdldClone.Groups.Clear();
										stdldClone.Groups.Add(group);
									}
									break;
								}

							_Studies.Add(stdld);
							break;
						}
					}
				}
			}
			if (MessageBox.Show(Vocabruary.Translate("Message.WhantSeeImportedPlan"), Vocabruary.Translate("Message.QuestionTitle"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
			{
				Browser browser = new Browser(this._storage, typeof (StudyLoading), this._Studies);
				browser.Text += " - " + Vocabruary.Translate("SheduleBrowser.Title.Import");
				browser.ShowDialog();
			}
		}

		public IList Studies
		{
			get { return _Studies; }
		}
	}

}
