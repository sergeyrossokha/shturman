using System;
using System.Collections;
using System.Reflection;
using System.Windows.Forms;
using System.Xml;
using Shturman.MultyLanguage;
using Shturman.Nestor.DataBrowser;

using Excel;

namespace SupplementXLS2XML
{
	public class GroupXLS
	{
		public string GroupCaption;
		public int GroupCapacity;

		public override bool Equals(object obj)
		{
			if (obj is GroupXLS)
			{
				return (obj as GroupXLS).GroupCaption == this.GroupCaption;
			}
			return false;
		}
	}

	public class TutorXLS
	{
		public string TutorCaption;
		public string TutorDegree;

		public override bool Equals(object obj)
		{
			if (obj is TutorXLS)
			{
				return (obj as TutorXLS).TutorCaption == this.TutorCaption;
			}
			return false;
		}
	}

	public class SupplementXLSItem
	{
        private string _departmentCaption;
		private string _subjectCaption;
		private string _subjectTypeCaption;
		private byte[] _hours = new byte[3];
		private ArrayList _groups = new ArrayList();
		private ArrayList _tutors = new ArrayList();
		private string _comment;

        public string DepartmentCaption
        {
            get { return _departmentCaption; }
            set { _departmentCaption = value; }
        }

		public string SubjectCaption
		{
			get { return _subjectCaption; }
			set {_subjectCaption = value; }
		}
		public string SubjectTypeCaption
		{
			get { return _subjectTypeCaption; }
			set {_subjectTypeCaption = value; }
		}
		public byte[] Hours
		{
			get { return _hours; }
		}

		public ArrayList Groups
		{
			get { return _groups; }
		}
		public ArrayList Tutors
		{
			get { return _tutors; }
		}
		public string Comment
		{
			get { return _comment; }
			set {_comment = value; }
		}

		public string XXX
		{
			get { return ""; } 
		}
	}

	class SupplementXLS
	{
		public string DepartmentCaption;
		public IList Items = new ArrayList();

        public string[] ReadExcelLine(int rowIndex, Excel.Worksheet excelSheet)
		{
			string[] line = new string[12];
			for (int index = 0; index < 12; index++)
			{
				try
				{
                    Excel.Range cell = excelSheet.Cells[rowIndex, (index + 1)] as Excel.Range;
					line[index] = (cell.Text as string).TrimEnd(' ');
				}
				catch
				{
					MessageBox.Show("������� �������: ����� - "+rowIndex.ToString()+" ������� - "+index.ToString());
				}
			}

			return line;
		}


        public bool StopReadExcel(int lastReadLine, Excel.Worksheet excelSheet)
		{
			for (int rowindex = lastReadLine; rowindex < lastReadLine + 10; rowindex++ )
				for (int index = 0; index < 12; index++)
				{
                    Excel.Range cell = excelSheet.Cells[rowindex, (index + 1)] as Excel.Range;
					string text = cell.Text as string;
					if (text != "")
						return false;
				}
			return true;
		}

		
		public bool ArrayEquals(object[] array1, object[] array2)
		{
			if(array1.Length == array2.Length)
			{
				for(int index=0; index<array1.Length; index++)
					if (!array1[index].Equals(array2[index]))
						return false;
				return true;
			}
			else
				return false;
		}
	
		
		public void ReadFromXls(string fileName)
		{
            Excel.Application excelApp = new Excel.ApplicationClass();
			try
			{
				try
				{
                    Excel.Workbook workbook = excelApp.Workbooks._Open(fileName, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value, Missing.Value);
					if (workbook.Sheets.Count > 0)											
					{
						IList workSheets = new ArrayList();
						for(int index=1; index<= workbook.Sheets.Count; index++)
                            workSheets.Add((workbook.Sheets[index] as Excel.Worksheet).Name);

						SelectForm sheetsSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.ExcelSheets"), true, workSheets);
						//����� �����
						if (sheetsSelectForm.ShowDialog(null) == DialogResult.OK)
						{
							foreach(string sheetName in sheetsSelectForm.SelectedItems)
							{
                                Excel.Worksheet excelSheet = (Excel.Worksheet)workbook.Sheets[workSheets.IndexOf(sheetName) + 1];
                                Excel.Range cell = excelSheet.Cells["3", "K"] as Excel.Range;
								try
								{
									this.DepartmentCaption = cell.Text as string;
								}
								catch
								{
									MessageBox.Show("������� �������, �3");
								}

								string[] startSetTag = { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" };
								string[] endSetTag = { "", "", "", "", "", "", "", "", "", "", "", "" };
                    
								int excelRowIndex = 3;
								// ��������� � ������� ����
								while (!ArrayEquals(ReadExcelLine(excelRowIndex, excelSheet),startSetTag))
									excelRowIndex++;

								while (!StopReadExcel(excelRowIndex, excelSheet))
								{                                            
									excelRowIndex++;
									string[] rowValue = ReadExcelLine(excelRowIndex, excelSheet);
									if (!ArrayEquals(rowValue, endSetTag))
									{
										if (rowValue[0] != "")
										{
											SupplementXLSItem supplementItem = new SupplementXLSItem();
                                            supplementItem.DepartmentCaption = this.DepartmentCaption;
											supplementItem.SubjectCaption = rowValue[1];
											supplementItem.SubjectTypeCaption = rowValue[2];

											if(!supplementItem.SubjectTypeCaption.EndsWith("."))
												supplementItem.SubjectTypeCaption += ".";

											// Read hours by trimesters
											if (rowValue[3] != "")
												supplementItem.Hours[0] = byte.Parse(rowValue[3]);
											if (rowValue[4] != "")
												supplementItem.Hours[1] = byte.Parse(rowValue[4]);
											if (rowValue[5] != "")
												supplementItem.Hours[2] = byte.Parse(rowValue[5]);

											if (rowValue[6] != "")
											{
												GroupXLS group = new GroupXLS();
												group.GroupCaption = rowValue[6];

												if(rowValue[7] != "")
													try
													{
														group.GroupCapacity = int.Parse(rowValue[7]);
													}
													catch
													{
														group.GroupCapacity = 0;
													}
                                    
												supplementItem.Groups.Add(group);
											}

											if (rowValue[9] != "")
											{
												TutorXLS tutor = new TutorXLS();
												tutor.TutorCaption = rowValue[9];

												if(rowValue[8] != "")
													tutor.TutorDegree = rowValue[8];
                                    
												supplementItem.Tutors.Add(tutor);
											}

											if (rowValue[10] != "")
											{
												supplementItem.Comment =  rowValue[10];
											}

											if (rowValue[11] != "")
											{
												if (supplementItem.Comment != "")
													supplementItem.Comment += ";" + rowValue[11];
												else
													supplementItem.Comment = rowValue[11];							
											}

											Items.Add(supplementItem);
										}
										else
										{
											SupplementXLSItem supplementItem = Items[Items.Count-1] as SupplementXLSItem;

											if (rowValue[6] != "")
											{
												GroupXLS group = new GroupXLS();
												group.GroupCaption = rowValue[6];

												if(rowValue[7] != "")
													try
													{
														group.GroupCapacity = int.Parse(rowValue[7]);
													}
													catch
													{
														group.GroupCapacity = 0;
													}
                                    
												supplementItem.Groups.Add(group);
											}

											if (rowValue[9] != "")
											{
												TutorXLS tutor = new TutorXLS();
												tutor.TutorCaption = rowValue[9];

												if(rowValue[8] != "")
													tutor.TutorDegree = rowValue[8];
                                    
												supplementItem.Tutors.Add(tutor);
											}								
										}
									}
									else
									{
										excelRowIndex ++;
										if (ReadExcelLine(excelRowIndex, excelSheet)[0]=="")
											if (!StopReadExcel(excelRowIndex, excelSheet))
												while (!ArrayEquals(ReadExcelLine(excelRowIndex, excelSheet),startSetTag))
													excelRowIndex++;
									}
								}
							}
						}
					}
				}
				catch(Exception e )
				{
					MessageBox.Show(e.Message);
				}
			}
			finally
			{
				excelApp.Workbooks.Close();
				excelApp.Application.Quit();
			}

		}


		public void SaveToXml(string fileName)
		{	
			XmlTextWriter writer = new XmlTextWriter(fileName, System.Text.Encoding.UTF8);
			try
			{
				writer.WriteStartDocument(true);
				writer.WriteStartElement("SupplementDocument");
				writer.WriteAttributeString("Department", this.DepartmentCaption);

				writer.WriteStartElement("Items");
				foreach (SupplementXLSItem item in Items)
				{
					writer.WriteStartElement("Supplement");
					writer.WriteAttributeString("SubjectCaption", item.SubjectCaption);
					writer.WriteAttributeString("SubjectTypeCaption", item.SubjectTypeCaption);
                    writer.WriteAttributeString("Comment", item.Comment);

					#region Write Hours by Trimester
					writer.WriteStartElement("Hours");
					foreach(byte hour in item.Hours)
					{
						writer.WriteStartElement("Hour");
						writer.WriteAttributeString("Count", hour.ToString());
						writer.WriteEndElement();
					}
					writer.WriteEndElement(); //Hours
					#endregion

					#region Write Groups
					writer.WriteStartElement("Groups");
					foreach(GroupXLS group in item.Groups)
					{
						writer.WriteStartElement("Group");
						writer.WriteAttributeString("GroupCaption", group.GroupCaption);
						writer.WriteAttributeString("GroupCapacity", group.GroupCapacity.ToString());
						writer.WriteEndElement();
					}
					writer.WriteEndElement(); //Groups
					#endregion

					#region Write Tutors
					writer.WriteStartElement("Tutors");
					foreach(TutorXLS tutor in item.Tutors)
					{
						writer.WriteStartElement("Tutor");
						writer.WriteAttributeString("TutorCaption", tutor.TutorCaption);
						writer.WriteAttributeString("TutorDegree", tutor.TutorDegree);
						writer.WriteEndElement();
					}
					writer.WriteEndElement(); //Tutors
					#endregion

					writer.WriteEndElement(); //Supplement
				}
				writer.WriteEndElement(); //Items
				writer.WriteEndElement(); //SupplementDocument
			}
			finally
			{
				writer.Flush();
				writer.Close();
			}
		}
	}
}