using System;
using System.Collections;
using System.Windows.Forms;
using System.Xml;
using Shturman.MultyLanguage;
using Shturman.Nestor.DataBrowser;
using Shturman.Nestor.Interfaces;
using Shturman.Shedule.Objects.Searching;
using Shturman.Shedule.Objects;
using SupplementXLS2XML;
using Db4objects.Db4o.Query;

namespace Shturman.Shedule.Import
{
	public class ImportSupplementFromXLS
	{
		private IObjectStorage _storage;

		private Hashtable _Groups = new Hashtable();
		private Hashtable _RecognizedObjects = new Hashtable();
		private ArrayList _Studies = new ArrayList();

        private IList allSubjectList;
        private IList allStudyFormList;
        private IList allGroupList;
        private IList allTutorList;
        

		/// <summary>
		/// ����������� ������ ImportSupplementFromXLS
		/// </summary>
		/// <param name="storage">��������� ��������</param>
		public ImportSupplementFromXLS(IObjectStorage storage)
		{
			_storage = storage;

            allSubjectList = this._storage.ObjectList(typeof(Subject));
            allStudyFormList = this._storage.ObjectList(typeof(StudyType));
            allGroupList = this._storage.ObjectList(typeof(Group));
            allTutorList = this._storage.ObjectList(typeof(Tutor));

            ArrayList.Adapter(allSubjectList).Sort();
            ArrayList.Adapter(allStudyFormList).Sort();
            ArrayList.Adapter(allGroupList).Sort();
            ArrayList.Adapter(allTutorList).Sort();
		}

		/// <summary>
		/// ������� ������� ���������� � ������������ ������ �� ����� ������� XML
		/// </summary>
		/// <param name="filename">��� ����� XML ������� �������� ����������</param>
		public void ImportXML(string filename)
		{
			XmlTextReader xmlrd = new XmlTextReader(filename);

			xmlrd.MoveToContent();
			if (xmlrd.HasAttributes)
			{
				/* �� ����� ���� ��������� ������ �������� ��������*/
			}

			while (xmlrd.Read())
			{
				/* ������ ���� */
				if (xmlrd.NodeType == XmlNodeType.Element && xmlrd.Name == "Group")
				{
					/* ������ �������� �����*/
					int groupRef = int.Parse(xmlrd.GetAttribute("id"));
					string groupNumber = xmlrd.GetAttribute("Name");
					//					int groupFaculty = int.Parse("" + groupNumber[0]);
					byte groupCapacity = byte.Parse(xmlrd.GetAttribute("Count"));
					/* �� ���� �������� ������ �� ������� �� ���������� � ��� 
					 * � ���� ���� ��� �������� �� ��� ��
					 * */
					Group gr = null;
					if (!_RecognizedObjects.ContainsKey(groupNumber))
					{
						IList groupList = this._storage.QueryObjects(new GroupSearchByNumber(groupNumber));
						IList allGroupList = this._storage.ObjectList(typeof (Group));


						if (groupList.Count == 1)
						{
							gr = groupList[0] as Group;
						}
						else
						{
							Group _gr = new Group();
							_gr.MnemoCode = groupNumber;
							_gr.GroupCapacity = groupCapacity;

							SelectFromListFrm selfrm = new SelectFromListFrm(_storage, groupList, allGroupList, _gr);

							selfrm.Text = Vocabruary.Translate("SelectFromList.GroupTitle");
							selfrm.ObjectText = groupNumber;
							if (selfrm.ShowDialog() == DialogResult.OK)
							{
								gr = selfrm.SelectedObject as Group;
								_RecognizedObjects.Add(groupNumber, gr);
							}
						}
						/* ������� ������� �������� */
						gr.GroupCapacity = groupCapacity;
					}
					else
					{
						gr = _RecognizedObjects[groupNumber] as Group;
					}
					/* ������ ������ �� ��� ������� */
					_Groups.Add(groupRef, gr);
				}

				if (xmlrd.NodeType == XmlNodeType.Element && xmlrd.Name == "TrimPlanItem")
				{
					/* ������ ���� �������� */
					StudyLoading stdld = new StudyLoading();

					/* ���������� ��������� */
					string subjectText = xmlrd.GetAttribute("Subject");
					if (!_RecognizedObjects.ContainsKey(subjectText))
					{
						IList subjectList = this._storage.QueryObjects(new SubjectSearchByName(subjectText));
						IList allSubjectList = this._storage.ObjectList(typeof (Subject));
						if (subjectList.Count == 1)
						{
							stdld.SubjectName = subjectList[0] as Subject;
						}
						else
						{
							Subject subject = new Subject();
							subject.MnemoCode = subjectText;
							subject.Name = subjectText;

							SelectFromListFrm selfrm = new SelectFromListFrm(_storage, subjectList, allSubjectList, subject);
							selfrm.Text = Vocabruary.Translate("SelectFromList.SubjectTitle");
							selfrm.ObjectText = subjectText;
							if (selfrm.ShowDialog() == DialogResult.OK)
							{
								stdld.SubjectName = selfrm.SelectedObject as Subject;
								_RecognizedObjects.Add(subjectText, stdld.SubjectName);
							}
						}
					}
					else
					{
						stdld.SubjectName = _RecognizedObjects[subjectText] as Subject;
					}
					/* ��� ������� */
					string studyFormText = xmlrd.GetAttribute("StudyForm") + ".";
					if (!_RecognizedObjects.ContainsKey(studyFormText))
					{
						IList studyFormList = this._storage.QueryObjects(new StudyTypeSearchByName(studyFormText));
						IList allStudyFormList = this._storage.ObjectList(typeof (StudyType));
						if (studyFormList.Count == 1)
						{
							stdld.StudyForm = studyFormList[0] as StudyType;
						}
						else
						{
							StudyType studyForm = new StudyType();
							studyForm.MnemoCode = studyFormText;
							studyForm.Name = studyFormText;

							SelectFromListFrm selfrm = new SelectFromListFrm(_storage, studyFormList, allStudyFormList, studyForm);
							selfrm.Text = Vocabruary.Translate("SelectFromList.StudyTypeTitle");
							selfrm.ObjectText = studyFormText;
							if (selfrm.ShowDialog() == DialogResult.OK)
							{
								stdld.StudyForm = selfrm.SelectedObject as StudyType;
								_RecognizedObjects.Add(studyFormText, stdld.StudyForm);
							}
						}
					}
					else
					{
						stdld.StudyForm = _RecognizedObjects[studyFormText] as StudyType;
					}

					string departmentNumber = xmlrd.GetAttribute("Department");
					if (!_RecognizedObjects.ContainsKey(departmentNumber))
					{
						IList departmentList = this._storage.QueryObjects(new DepartmentSearchByNumber(departmentNumber));
						IList allDepartmentList = this._storage.ObjectList(typeof (Department));
						if (departmentList.Count == 1)
						{
							stdld.Dept = departmentList[0] as Department;
						}
						else
						{
							Department department = new Department();
							department.MnemoCode = departmentNumber;

							SelectFromListFrm selfrm = new SelectFromListFrm(_storage, departmentList, allDepartmentList, department);
							selfrm.Text = Vocabruary.Translate("SelectFromList.DepartmentTitle");
							selfrm.ObjectText = departmentNumber;
							if (selfrm.ShowDialog() == DialogResult.OK)
							{
								stdld.Dept = selfrm.SelectedObject as Department;
								_RecognizedObjects.Add(departmentNumber, stdld.StudyForm);
							}
						}
					}
					else
					{
						stdld.Dept = _RecognizedObjects[departmentNumber] as Department;
					}

					stdld.HourByWeek = int.Parse(xmlrd.GetAttribute("Hours"));

					while (xmlrd.Read())
					{
						if (xmlrd.NodeType == XmlNodeType.Element && xmlrd.Name == "Group")
						{
							/*������ ����� �� �����*/
							int groupid = int.Parse(xmlrd.GetAttribute("ref"));
							if (_Groups.ContainsKey(groupid))
								stdld.Groups.Add(_Groups[groupid]);

						}

						if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "TrimPlanItem")
						{
							/* ������ ���������� ������ ����� �� ������ ����������� ������ */
							if (stdld.StudyForm != null)
								if ((stdld.Groups.Count > 1) && (stdld.StudyForm.MnemoCode == "��." || stdld.StudyForm.MnemoCode == "��."))
								{
									foreach (Group group in stdld.Groups)
									{
										StudyLoading stdldClone = stdld.Clone() as StudyLoading;
										stdldClone.Groups.Clear();
										stdldClone.Groups.Add(group);
									}
									break;
								}

							_Studies.Add(stdld);
							break;
						}
					}
				}
			}
			if (MessageBox.Show(Vocabruary.Translate("Message.WhantSeeImportedPlan"), Vocabruary.Translate("Message.QuestionTitle"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
			{
				Browser browser = new Browser(this._storage, typeof (StudyLoading), this._Studies);
				browser.Text += " - " + Vocabruary.Translate("SheduleBrowser.Title.Import");
				browser.ShowDialog();
			}
		}
		
		/// <summary>
		/// ������� ������� ���������� � ������������ ������ �� ����� �������
		/// Microsoft Excel
		/// </summary>
		/// <param name="filename">��� ����� Microsoft Excel ������� �������� ����������</param>
		/// <param name="trimestr">����� ��������� ��� �������� �������������� �������</param>
		public void ImportXLS(string filename, int trimestr)
		{
			SupplementXLS SupplementDocument = new SupplementXLS();
			try
			{
				SupplementDocument.ReadFromXls(filename);
                Import(SupplementDocument.Items, trimestr);
			}
			catch(Exception e)
			{
				MessageBox.Show("������� �������: "+e.Message );
			}
		}

        /// <summary>
        /// ������ �������� �������� � ��������� ���� � ���� ������
        /// </summary>
        /// <param name="supplementList">������ ������� ��������</param>
        /// <param name="trimestr">����� ��������� ��� �������� �������������� �������</param>
        public void Import(IList supplementList, int trimestr)
        {
            foreach (SupplementXLSItem item in supplementList)
                if (item.Hours[trimestr - 1] != 0)
                {
                    StudyLoading stdld = Recognize(item, trimestr);
                    if (stdld != null)
                        _Studies.Add(stdld);
                }
        }

        public StudyLoading Recognize(SupplementXLSItem item, int trimestr)
        {
                /* ������ ���� �������� */
                StudyLoading stdld = new StudyLoading();

                #region Department
                Department dept = null;
                if (!_RecognizedObjects.ContainsKey(item.DepartmentCaption))
                {
                    IList departmentList = this._storage.QueryObjects(new DepartmentSearchByNumber(item.DepartmentCaption));
                    if (departmentList.Count == 1)
                    {
                        dept = departmentList[0] as Department;
                        _RecognizedObjects.Add(item.DepartmentCaption, dept);
                    }
                    else
                    {
                        Department department = new Department();
                        department.MnemoCode = item.DepartmentCaption;

                        IList allDepartmentList = this._storage.ObjectList(typeof(Department));
                        SelectFromListFrm selfrm = new SelectFromListFrm(_storage, departmentList, allDepartmentList, department);
                        selfrm.Text = Vocabruary.Translate("SelectFromList.DepartmentTitle");
                        selfrm.ObjectText = item.DepartmentCaption;
                        if (selfrm.ShowDialog() == DialogResult.OK)
                        {
                            dept = selfrm.SelectedObject as Department;
                            _RecognizedObjects.Add(item.DepartmentCaption, dept);
                        }
                        else
                            return null;
                    }
                }
                else
                {
                    dept = _RecognizedObjects[item.DepartmentCaption] as Department; 
                }
                #endregion

                stdld.Dept = dept;

                #region Analize Subject
                if (!_RecognizedObjects.ContainsKey(item.SubjectCaption))
                {
                    IList subjectList = this._storage.QueryObjects(new SubjectSearchByName(item.SubjectCaption));

                    if (subjectList.Count == 1)
                    {
                        stdld.SubjectName = subjectList[0] as Subject;
                        _RecognizedObjects.Add(item.SubjectCaption, stdld.SubjectName);
                    }
                    else
                    {
                        Subject subject = new Subject();
                        subject.MnemoCode = item.SubjectCaption.GetHashCode().ToString();
                        subject.Name = item.SubjectCaption;
                        subject.ShortName = item.SubjectCaption;

                        SelectFromListFrm selfrm = new SelectFromListFrm(_storage, subjectList, allSubjectList, subject);
                        selfrm.Text = Vocabruary.Translate("SelectFromList.SubjectTitle");
                        selfrm.ObjectText = item.SubjectCaption;
                        if (selfrm.ShowDialog() == DialogResult.OK)
                        {
                            stdld.SubjectName = selfrm.SelectedObject as Subject;
                            if (dept != null)
                                if (!dept.DepartmentSubjects.Contains(stdld.SubjectName))
                                {
                                    dept.DepartmentSubjects.Add(stdld.SubjectName);
                                    _storage.Save(dept);
                                }
                            _RecognizedObjects.Add(item.SubjectCaption, stdld.SubjectName);
                        }
                        else
                            return null;
                    }
                }
                else
                {
                    stdld.SubjectName = _RecognizedObjects[item.SubjectCaption] as Subject;

                    if (dept != null)
                        if (!dept.DepartmentSubjects.Contains(stdld.SubjectName))
                        {
                            dept.DepartmentSubjects.Add(stdld.SubjectName);
                            _storage.Save(dept);
                        }
                }
                #endregion

                #region SubjectTypeCaption
                if (!_RecognizedObjects.ContainsKey(item.SubjectTypeCaption))
                {
                    IList studyFormList = this._storage.QueryObjects(new StudyTypeSearchByName(item.SubjectTypeCaption));
                    if (studyFormList.Count == 1)
                    {
                        stdld.StudyForm = studyFormList[0] as StudyType;
                        _RecognizedObjects.Add(item.SubjectTypeCaption, stdld.StudyForm);
                    }
                    else
                    {
                        StudyType studyForm = new StudyType();
                        studyForm.MnemoCode = item.SubjectTypeCaption;
                        studyForm.Name = item.SubjectTypeCaption;

                        SelectFromListFrm selfrm = new SelectFromListFrm(_storage, studyFormList, allStudyFormList, studyForm);
                        selfrm.Text = Vocabruary.Translate("SelectFromList.StudyTypeTitle");
                        selfrm.ObjectText = item.SubjectTypeCaption;
                        if (selfrm.ShowDialog() == DialogResult.OK)
                        {
                            stdld.StudyForm = selfrm.SelectedObject as StudyType;
                            _RecognizedObjects.Add(item.SubjectTypeCaption, stdld.StudyForm);
                        }
                        else
                            return null;
                    }
                }
                else
                {
                    stdld.StudyForm = _RecognizedObjects[item.SubjectTypeCaption] as StudyType;
                }
                #endregion

                stdld.HourByWeek = item.Hours[trimestr - 1];

                #region ReadGroups
                foreach (GroupXLS groupXLS in item.Groups)
                {
                    Group gr = null;
                    if (!_RecognizedObjects.ContainsKey(groupXLS.GroupCaption))
                    {
                        IList groupList = this._storage.QueryObjects(new GroupSearchByNumber(groupXLS.GroupCaption, GroupSearchByNumber.GroupSearchType.equal));
                        if (groupList.Count == 0)
                            groupList = this._storage.QueryObjects(new GroupSearchByNumber(groupXLS.GroupCaption));

                        if (groupList.Count == 1)
                        {
                            gr = groupList[0] as Group;
                            _RecognizedObjects.Add(groupXLS.GroupCaption, gr);
                        }
                        else
                        {
                            Group _gr = new Group();
                            _gr.MnemoCode = groupXLS.GroupCaption;
                            _gr.GroupCapacity = (byte)groupXLS.GroupCapacity;

                            SelectFromListFrm selfrm = new SelectFromListFrm(_storage, groupList, allGroupList, _gr);

                            selfrm.Text = Vocabruary.Translate("SelectFromList.GroupTitle");
                            selfrm.ObjectText = groupXLS.GroupCaption;
                            if (selfrm.ShowDialog() == DialogResult.OK)
                            {
                                gr = selfrm.SelectedObject as Group;
                                _RecognizedObjects.Add(groupXLS.GroupCaption, gr);
                            }
                            else
                                return null;
                        }
                        /* ������� ������� �������� */
                        if (gr.GroupCapacity != (byte)groupXLS.GroupCapacity)
                        {
                            gr.GroupCapacity = (byte)groupXLS.GroupCapacity;
                            _storage.Save(gr);
                        }
                    }
                    else
                    {
                        gr = _RecognizedObjects[groupXLS.GroupCaption] as Group;
                    }
                    /* ������ ������ */
                    stdld.Groups.Add(gr);
                }
                #endregion

                #region ReadTutors
                foreach (TutorXLS tutorXLS in item.Tutors)
                {
                    Tutor tr = null;
                    if (!_RecognizedObjects.ContainsKey(tutorXLS.TutorCaption))
                    {
                        IList tutorList = this._storage.QueryObjects(new TutorSearchByFIO(tutorXLS.TutorCaption));

                        if (tutorList.Count == 1)
                        {
                            tr = tutorList[0] as Tutor;
                            _RecognizedObjects.Add(tutorXLS.TutorCaption, tr);
                        }
                        else
                        {
                            Tutor _tr = new Tutor();
                            _tr.MnemoCode = tutorXLS.TutorCaption.GetHashCode().ToString();
                            _tr.TutorSurname = tutorXLS.TutorCaption;

                            if (dept != null)
                                if (!_tr.TutorDepartments.Contains(dept))
                                {
                                    _tr.TutorDepartments.Add(dept);
                                }


                            #region TutorPost
                            if (tutorXLS.TutorDegree != null)
                                if (!_RecognizedObjects.ContainsKey(tutorXLS.TutorDegree))
                                {
                                    IList postList = this._storage.QueryObjects(new PostSearchByName(tutorXLS.TutorDegree));
                                    IList allPostList = this._storage.ObjectList(typeof(Post));
                                    if (postList.Count == 1)
                                    {
                                        _tr.TutorPost = postList[0] as Post;
                                        _RecognizedObjects.Add(tutorXLS.TutorDegree, _tr.TutorPost);
                                    }
                                    else
                                    {
                                        Post postForm = new Post();
                                        postForm.MnemoCode = tutorXLS.TutorDegree;
                                        postForm.Name = tutorXLS.TutorDegree;

                                        SelectFromListFrm _selfrm = new SelectFromListFrm(_storage, postList, allPostList, postForm);
                                        _selfrm.Text = Vocabruary.Translate("SelectFromList.PostTitle");
                                        _selfrm.ObjectText = tutorXLS.TutorDegree;
                                        if (_selfrm.ShowDialog() == DialogResult.OK)
                                        {
                                            _tr.TutorPost = _selfrm.SelectedObject as Post;
                                            _RecognizedObjects.Add(tutorXLS.TutorDegree, _tr.TutorPost);
                                        }
                                        else
                                            return null;
                                    }
                                }
                                else
                                {
                                    _tr.TutorPost = _RecognizedObjects[tutorXLS.TutorDegree] as Post;
                                }
                            #endregion

                            SelectFromListFrm selfrm = new SelectFromListFrm(_storage, tutorList, allTutorList, _tr);

                            selfrm.Text = Vocabruary.Translate("SelectFromList.TutorTitle");
                            selfrm.ObjectText = tutorXLS.TutorDegree + " " + tutorXLS.TutorCaption;

                            if (selfrm.ShowDialog() == DialogResult.OK)
                            {
                                tr = selfrm.SelectedObject as Tutor;
                                if (dept != null)
                                    if (!dept.DepartmentTutors.Contains(tr))
                                    {
                                        dept.DepartmentTutors.Add(tr);
                                        _storage.Save(dept);
                                    }
                                _RecognizedObjects.Add(tutorXLS.TutorCaption, tr);
                            }
                            else
                                return null;
                        }
                    }
                    else
                    {
                        tr = _RecognizedObjects[tutorXLS.TutorCaption] as Tutor;

                        /* ������������ ������� � ���������� */
                        if (dept != null)
                        {
                            if (!dept.DepartmentTutors.Contains(tr))
                            {
                                dept.DepartmentTutors.Add(tr);
                                _storage.Save(dept);
                            }

                            if (!tr.TutorDepartments.Contains(dept))
                            {
                                tr.TutorDepartments.Add(dept);
                                _storage.Save(tr);
                            }
                        }
                    }
                    /* ������ ������ */
                    stdld.Tutors.Add(tr);
                }
                #endregion

                stdld.Note = item.Comment;

                /* ������ �� ������ ������������� ������*/
                return stdld;
        }

		/// <summary>
		/// ��������������� ������ ���������� ������������� ����� 
		/// </summary>
		public IList Studies
		{
			get { return _Studies; }
		}
	}
}
