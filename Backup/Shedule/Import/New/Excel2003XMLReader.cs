using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml.XPath;

using System.Xml;

namespace Shturman.Shedule.Import.New
{
    public delegate void ColumnReadCompleate (string sheetName, int rowIndex, int columnIndex, string cellValue);
    public delegate void RowReadCompleate (string sheetName, int rowIndex, string[] cellsValue );
    public delegate void SheetReadEvent(string sheetName);
    public delegate void DocumentEvent();
  
    class Excel2003XMLReader : IExcelReader
    {
        private string _fileName = "";
        private List<string> _sheets = new List<string>();
        
        public Excel2003XMLReader()
        {
        }

        public Excel2003XMLReader(string excelXMLFileName):this()
        {
            _fileName = excelXMLFileName;
            GetExistSheets();
        }

        private void GetExistSheets()
        {
            XmlNamespaceManager nsmgr = new XmlNamespaceManager(new NameTable());
            nsmgr.AddNamespace("ss", "urn:schemas-microsoft-com:office:spreadsheet");

            XPathDocument xp_doc = new XPathDocument(_fileName);
            
            XPathNavigator xp_nav = xp_doc.CreateNavigator();
            xp_nav.MoveToRoot();

            XPathNodeIterator xp_iter = xp_nav.Select("ss:Workbook/ss:Worksheet/@ss:Name", nsmgr);

            _sheets.Clear();
            while (xp_iter.MoveNext())
            {
                _sheets.Add( xp_iter.Current.Value);
            }
        }

        List<string> IExcelReader.Sheets
        {
            get { return _sheets; }
        }

        public string FileName
        {
            get { return _fileName; }
            set 
            { 
                _fileName = value;
                GetExistSheets();
            }
        }

        public void ParseDocument()
        {
            XmlReader reader = new XmlTextReader(_fileName);
            try
            {
                int rowIndex = 0, columnIndex = 0;
                IList cellValueList = new ArrayList();
                string currentSheet = "";
                
                while (!reader.EOF)
                {
                    if (reader.NodeType == XmlNodeType.ProcessingInstruction)
                    {
                        if (reader.Value != "progid=\"Excel.Sheet\"")
                            return;
                    }

                    if (reader.NodeType == XmlNodeType.EndElement)
                    {
                        if (reader.Name == "Workbook")
                        {
                            if (OnDocumentReadEnd != null)
                                foreach (DocumentEvent de in OnDocumentReadEnd.GetInvocationList())
                                    de.Invoke();
                        }
                                                
                        if (reader.Name == "Worksheet")
                        {
                            if (this.OnSheetReadEnd != null)
                                foreach (SheetReadEvent sre in OnSheetReadEnd.GetInvocationList())
                                    sre.Invoke(currentSheet);
                        }
                        
                        if (reader.Name == "Row")
                        {
                            if (_sheets.Contains(currentSheet))
                            {
                                string[] cellValues = new string[cellValueList.Count];
                                cellValueList.CopyTo(cellValues, 0);

                                if (OnRowRead != null)
                                    foreach (RowReadCompleate rrc in OnRowRead.GetInvocationList())
                                        rrc.Invoke(currentSheet, rowIndex, cellValues);
                            }
                        }
                    }

                    if (reader.NodeType == XmlNodeType.Element)
                    {
                        if (reader.Name == "Workbook")
                        {
                            if (OnDocumentReadStart != null)
                                foreach (DocumentEvent de in OnDocumentReadStart.GetInvocationList())
                                    de.Invoke();
                        }

                        if (reader.Name == "Worksheet")
                        {
                            currentSheet = reader.GetAttribute("ss:Name");

                            if (this.OnSheetReadBegin != null)
                                foreach (SheetReadEvent sre in OnSheetReadBegin.GetInvocationList())
                                    sre.Invoke(currentSheet);

                            rowIndex = 0;
                        }

                        if (reader.Name == "Row")
                        {
                            if (_sheets.Contains(currentSheet))
                            {
                                rowIndex++;
                                columnIndex = 0;
                                cellValueList.Clear();
                            }
                        }

                        if (reader.Name == "Cell")
                        {
                            if (_sheets.Contains(currentSheet))
                            {

                                string newColumnIndexStr = reader.GetAttribute("ss:Index");
                                if (newColumnIndexStr != null)
                                {
                                    while (columnIndex < int.Parse(newColumnIndexStr))
                                    {
                                        cellValueList.Add(string.Empty);
                                        columnIndex++;

                                        if (this.OnColumnRead != null)
                                            foreach (ColumnReadCompleate crc in OnColumnRead.GetInvocationList())
                                                crc.Invoke(currentSheet, rowIndex, columnIndex, string.Empty);
                                    }
                                }
                                else
                                    columnIndex++;

                                string str = "";

                                XmlReader subreader = reader.ReadSubtree();
                                while (!subreader.EOF)
                                {
                                    if (subreader.NodeType == XmlNodeType.Element)
                                    {
                                        if (subreader.Name == "Data")
                                        {
                                            str = reader.ReadInnerXml();
                                        }
                                    }
                                    subreader.Read();
                                }

                                if (this.OnColumnRead != null)
                                    foreach (ColumnReadCompleate crc in OnColumnRead.GetInvocationList())
                                        crc.Invoke(currentSheet, rowIndex, columnIndex, str);

                               // if (newColumnIndexStr != null)
                               // {
                               //     cellValueList[int.Parse(newColumnIndexStr) - 1] = str;
                               // }
                               // else
                                    cellValueList.Add(str);
                            }
                        }

                    }
                    reader.Read();
                }
            }
            finally
            {
                reader.Close();
            }
        }

        public event ColumnReadCompleate OnColumnRead;
        public event RowReadCompleate OnRowRead;
        public event SheetReadEvent OnSheetReadBegin;
        public event SheetReadEvent OnSheetReadEnd;
        public event DocumentEvent OnDocumentReadStart;
        public event DocumentEvent OnDocumentReadEnd;
    }
}
