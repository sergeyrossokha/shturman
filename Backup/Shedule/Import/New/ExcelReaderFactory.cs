using System;
using System.Collections.Generic;
using System.Text;

namespace Shturman.Shedule.Import.New
{
    class ExcelReaderFactory
    {
        public static IExcelReader CreateExcelReader(string filename)
        {
            if (System.IO.Path.GetExtension(filename) == ".xlsx")
                return new Excel2007Reader(filename);
            else
                return new Excel2003XMLReader(filename);
        }
    }
}
