using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using SupplementXLS2XML;
using System.Windows.Forms;

namespace Shturman.Shedule.Import.New
{
    public class SupplementBuilder
    {
        private ImportSupplementSchema _schema;
        private IExcelReader _excelXmlRader;

        private bool _canAnalizeSheetData = false;
        private string _sheetDepartmentCaption = "";

        private IList _supplements = new ArrayList();

        private ColumnReadCompleate crc;
        private RowReadCompleate rrc;

        public SupplementBuilder(ImportSupplementSchema schema, IExcelReader excelXmlReader)
        {
            _schema = schema;
            _excelXmlRader = excelXmlReader;
        }

        public void BuildSupplement()
        {
            crc = new ColumnReadCompleate(_excelXmlRader_OnColumnRead);
            rrc = new RowReadCompleate(_excelXmlRader_OnRowRead);

            _excelXmlRader.OnRowRead += rrc;
            _excelXmlRader.OnColumnRead += crc;

            _excelXmlRader.ParseDocument();
        }

        public void BuildSupplement(string[] excelXmlFileList)
        {
            foreach (string fileName in excelXmlFileList)
                if (System.IO.File.Exists(fileName))
                {
                    BuildSupplement();
                }
        }

        void _excelXmlRader_OnColumnRead(string sheetName, int rowIndex, int columnIndex, string cellValue)
        {
            if ((rowIndex == _schema.DepartmentRow) && (columnIndex == _schema.DepartmentColumn))
            {
                _sheetDepartmentCaption = cellValue;

                if (cellValue == "")
                    MessageBox.Show("�� ���� " + sheetName+
                        " � ����� "+rowIndex.ToString()+" �� ������� "+columnIndex.ToString()+" �������� ����� �������" );
            }

            if ((rowIndex == 4) && (columnIndex == 6))
            {
                if (cellValue.StartsWith("(��������� �������)"))
                {
                    _canAnalizeSheetData = true;
                }
                else
                {
                    _canAnalizeSheetData = false;
                }
            }
        }

        void _excelXmlRader_OnRowRead(string sheetName, int rowIndex, string[] cellsValue)
        {
            if ((rowIndex > 13) && _canAnalizeSheetData && cellsValue.Length>=6)
                try
            {
                SupplementXLSItem supplementItem;
                bool isNew = cellsValue[_schema.NumberColumn - 1] != "" && cellsValue[_schema.SubjectNameColumn - 1] != "";
                if (isNew)
                {
                    supplementItem = new SupplementXLSItem();
                    supplementItem.DepartmentCaption = _sheetDepartmentCaption;
                    supplementItem.SubjectCaption = cellsValue[_schema.SubjectNameColumn - 1];
                    supplementItem.SubjectTypeCaption = cellsValue[_schema.SubjectTypeNameColumn - 1].EndsWith(".") ?
                        cellsValue[_schema.SubjectTypeNameColumn - 1] : cellsValue[_schema.SubjectTypeNameColumn - 1] + ".";
                    supplementItem.Hours[0] = cellsValue[_schema.HoursColumnStart - 1] != "" ? byte.Parse(cellsValue[_schema.HoursColumnStart - 1]) : (byte)0;
                    supplementItem.Hours[1] = cellsValue[_schema.HoursColumnStart - 1 + 1] != "" ? byte.Parse(cellsValue[_schema.HoursColumnStart - 1 + 1]) : (byte)0;
                }
                else
                    supplementItem = _supplements[_supplements.Count - 1] as SupplementXLSItem;

                if (cellsValue[_schema.GroupNameColumn-1] != "")
                {
                    GroupXLS group = new GroupXLS();
                    group.GroupCaption = cellsValue[_schema.GroupNameColumn-1];
                    group.GroupCapacity = cellsValue[_schema.GroupStudentCountColumn-1] != "" ? (int)double.Parse(cellsValue[_schema.GroupStudentCountColumn-1]) : 0;
                    
                    supplementItem.Groups.Add(group );
                }

                if (cellsValue[_schema.TutorNameColumn-1] != "")
                {
                    TutorXLS tutor = new TutorXLS();
                    tutor.TutorCaption = cellsValue[_schema.TutorNameColumn-1];
                    tutor.TutorDegree = cellsValue[_schema.TutorPostColumn-1] != "" ? cellsValue[_schema.TutorPostColumn-1] : "";

                    supplementItem.Tutors.Add(tutor);
                }

                if (cellsValue[_schema.FlatColumn-1] != "")
                    supplementItem.Comment = cellsValue[_schema.FlatColumn-1];

                if (cellsValue[_schema.Comment-1] != "")
                    supplementItem.Comment = supplementItem.Comment != "" ?
                        supplementItem.Comment + ";" + cellsValue[_schema.Comment-1] : cellsValue[_schema.Comment-1];

                if (isNew)
                    _supplements.Add(supplementItem);
            }
            catch(Exception e)
            {
               if (e is IndexOutOfRangeException)
                {
                        MessageBox.Show("�� ���� " + sheetName +
                           " ����� " + rowIndex.ToString() + " �� ������� ������ ");
                }
            }
        }

        public IList Supplement
        { get { return _supplements; } }
    }
}
