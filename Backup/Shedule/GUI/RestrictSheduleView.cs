using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Shturman.Shedule.Objects;
using Shturman.Shedule;
using Shturman.Shedule.Server.Interface;
using Shturman.Shedule.Server.Remoting;
using SourceGrid;
using SourceGrid.Cells;
using SourceGrid.Cells.Views;
using Common = SourceGrid.Cells.Views.Cell;
using FlatHeader = SourceGrid.Cells.Views.Header;

namespace Shturman.Shedule.GUI
{
	/// <summary>
	/// Summary description for RestrictSheduleView.
	/// </summary>
	public class RestrictSheduleView : UserControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private Grid grid;

        private IView headerView;
        private IView weekView;
        
        private IView dayView;

		private int dayCount = 0;
		private int hourCount = 0;
		private int weekCount = 0;

		private int _cellHeight = 16;
		private int _cellWidth = 16;

        private bool _readOnly = false;

		private Container components = null;

		public RestrictSheduleView()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
			
			grid.Selection.FocusStyle = FocusStyle.None;

            dayView = new CustomHeaderModel();
            dayView.TextAlignment = DevAge.Drawing.ContentAlignment.MiddleCenter;
            dayView.BackColor = Color.LightSkyBlue;
            (dayView as Common).ElementText = new DevAge.Drawing.VisualElements.TextGDI();
            (dayView as CustomHeaderModel).FormatFlags = StringFormatFlags.DirectionVertical | StringFormatFlags.NoWrap;

            headerView = new CustomHeaderModel();
            (headerView as CustomHeaderModel).ChangeDefaultColor(System.Drawing.SystemColors.Control);
			headerView.TextAlignment = DevAge.Drawing.ContentAlignment.MiddleCenter;

            weekView = new WeekHeaderModel();
		}

		public void LoadShedule(IList dayList, IList hourList, IList weekList)
		{
			dayCount = dayList.Count;
			hourCount = hourList.Count;
			weekCount = weekList.Count;

			//
			// Grid regim 2 - weeks, 6- days, 6 - hours, 5 - tutor
			//
			grid.Redim(1+dayCount*weekCount,  2 + hourCount);

			//
			// Load grid header
			//
            grid[0, 0] = new SourceGrid.Cells.Cell("", typeof(string));
            grid[0, 0].View = headerView;
            grid[0, 0].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);
            grid[0,0].RowSpan = 1;
			grid[0,0].ColumnSpan = 2;

			//
			// Create grid day caption
			//
			for (int dayindex=1; dayindex< grid.RowsCount; dayindex+= weekCount )
			{
				grid.Columns[0].Width = _cellWidth;
				grid.Columns[1].Width = _cellWidth;
				grid.Rows[0].Height = 20;

                grid[dayindex, 0] = new SourceGrid.Cells.Cell(dayList[(dayindex-1)/(weekCount)].ToString(), typeof(string));
                grid[dayindex, 0].View = dayView;
                grid[dayindex, 0].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);

                grid[dayindex, 0].RowSpan = weekCount;
					
				for(int weekindex = dayindex; weekindex<(dayindex+2); weekindex++)
				{
					grid[weekindex, 1] = new SourceGrid.Cells.Cell(weekList[(weekindex-dayindex)].ToString(), typeof(string));
                    grid[weekindex, 1].View = weekView;
                    grid[weekindex, 1].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);
					grid.Rows[weekindex].Height = _cellHeight;
				}
			}

			grid.FixedColumns = 2;
			grid.FixedRows = 1;

			for (int colindex=2; colindex< grid.ColumnsCount; colindex++)
			{
                grid[0, colindex] = new SourceGrid.Cells.Cell(hourList[colindex-2].ToString(), typeof(string));
                grid[0, colindex].View = headerView;
                grid[0, colindex].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);
				grid.Columns[colindex].Width = _cellWidth;				
				grid[0, colindex].RowSpan = 1;

				for (int rowindex=1; rowindex<grid.RowsCount; rowindex++ )
				{
                    grid[rowindex, colindex] = new SourceGrid.Cells.Cell("", typeof(string));
                    grid[rowindex, colindex].View = new RestrictCellModel();
                    if (_readOnly)
                        grid[rowindex, colindex].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);

                    grid[rowindex, colindex].Editor.EnableEdit = false;
                   
                    grid[rowindex, colindex].AddController(SourceGrid.Cells.Controllers.ToolTipText.Default);

                    grid[rowindex, colindex].Model.AddModel(MyToolTipModel.Default);
				}
			}
		}

        public class MyToolTipModel : SourceGrid.Cells.Models.IToolTipText
        {
            public static readonly MyToolTipModel Default = new MyToolTipModel();

            public string GetToolTipText(SourceGrid.CellContext cellContext)
            {
                SourceGrid.Grid grid = (SourceGrid.Grid)cellContext.Grid;
                return grid[cellContext.Position.Row, cellContext.Position.Column].ToolTipText;
            }

        }
		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// FlatSheduleView
			//
			this.grid = new SourceGrid.Grid();
			this.SuspendLayout();
			// 
			// grid
			// 
			this.grid.AutoStretchColumnsToFitWidth = false;
			this.grid.AutoStretchRowsToFitHeight = false;
			this.grid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.grid.CustomSort = false;
			this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.grid.Location = new System.Drawing.Point(0, 0);
			this.grid.Name = "grid";
			this.grid.Size = new System.Drawing.Size(360, 340);
			this.grid.SpecialKeys = SourceGrid.GridSpecialKeys.Default;
			this.grid.TabIndex = 0;
            grid.ToolTip.Active = true;
            grid.ToolTipText = "sdfsdf";
			this.grid.MouseUp += new MouseEventHandler(grid_MouseUp);
			this.grid.Click += new EventHandler(grid_OnClick);
			this.grid.MouseMove += new MouseEventHandler(grid_MouseMove);
			// 
			// RestrictSheduleView
			// 
			this.Controls.Add(this.grid);
			this.Name = "RestrictSheduleView";
			this.Size = new System.Drawing.Size(320, 296);

			//this.Resize += new System.EventHandler(this.grid_Resize);
			this.ResumeLayout(false);
		}
		#endregion

		public void Show(object restrictObject, IShedule shmatr)
		{			
			for (int colindex=2; colindex< grid.ColumnsCount; colindex++)
				for (int rowindex=1; rowindex<grid.RowsCount; rowindex++ )				
				{
					int day = (rowindex - 1) / (shmatr.GetWeeksCount()) + 1;
					int pair = colindex - 2 + 1; 
					int week = (rowindex - 1) % shmatr.GetWeeksCount() + 1;

					int key =  shmatr.GetKey(week, day, pair);

					if (restrictObject is Group)
					{
						Group _group = restrictObject as Group;
						// Lessons
						if (shmatr.GroupHasLesson(_group.ObjectHardKey, key))
						{
							(grid[rowindex, colindex].View as RestrictCellModel).IsBusy = true;	
						}
						else
                            (grid[rowindex, colindex].View as RestrictCellModel).IsBusy = false;						
						// Constraints
						if (shmatr.IsGroupHasRestricts(_group.ObjectHardKey, key))
						{
                            (grid[rowindex, colindex].View as RestrictCellModel).HaveConstraint = true;
							grid[rowindex, colindex].ToolTipText = shmatr.GetGroupRestrictsText(_group.ObjectHardKey, key);
						}
						else
                            (grid[rowindex, colindex].View as RestrictCellModel).HaveConstraint = false;
					}
					
					if (restrictObject is LectureHall)
					{ 
						LectureHall _lectureHall = restrictObject as LectureHall;
						// Lesson
						if (shmatr.FlatHasLesson(_lectureHall.ObjectHardKey, key))						
						{
                            (grid[rowindex, colindex].View as RestrictCellModel).IsBusy = true;

							IList lessons = shmatr.GetFlatLesson(_lectureHall.ObjectHardKey, key);
							
							string tooltip = "";
							foreach (RemoteLesson remlsn in lessons)
							{
								Lesson lsn = RemoteAdapter.Adapter.AdaptLesson(remlsn);
								tooltip += lsn.FlatViewString+"\n";
							}
							grid[rowindex, colindex].ToolTipText = tooltip;
						}
						else
                            (grid[rowindex, colindex].View as RestrictCellModel).IsBusy = false;						
						// Constraints
						if (shmatr.IsFlatHasRestricts(_lectureHall.ObjectHardKey, key))							
						{
                            (grid[rowindex, colindex].View as RestrictCellModel).HaveConstraint = true;
							grid[rowindex, colindex].ToolTipText = shmatr.GetFlatRestrictsText(_lectureHall.ObjectHardKey, key);
						}
						else
                            (grid[rowindex, colindex].View as RestrictCellModel).HaveConstraint = false;
					}

					if (restrictObject is Tutor)
					{	
						Tutor _tutor = restrictObject as Tutor;
						// Lesson
						if (shmatr.TutorHasLesson(_tutor.ObjectHardKey, key))
						{
                            (grid[rowindex, colindex].View as RestrictCellModel).IsBusy = true;
							Lesson lsn = RemoteAdapter.Adapter.AdaptLesson(shmatr.GetTutorLesson(_tutor.ObjectHardKey, key));
							grid[rowindex, colindex].ToolTipText = lsn.TutorViewString;
						}
						else
                            (grid[rowindex, colindex].View as RestrictCellModel).IsBusy = false;
						// Constraints						
						if( shmatr.IsTutorHasRestricts(_tutor.ObjectHardKey, key))
                            (grid[rowindex, colindex].View as RestrictCellModel).HaveConstraint = true;
						else
                            (grid[rowindex, colindex].View as RestrictCellModel).HaveConstraint = false;
					}
				}
			this.grid.Refresh();
		}

		public void ShowOnlyConstrains(object restrictObject, IShedule shmatr)
		{
			for (int colindex=2; colindex< grid.ColumnsCount; colindex++)
				for (int rowindex=1; rowindex<grid.RowsCount; rowindex++ )				
				{
					int day = (rowindex - 1) / (shmatr.GetWeeksCount()) + 1;
					int pair = colindex - 2 + 1; 
					int week = (rowindex - 1) % shmatr.GetWeeksCount() + 1;

					int key =  shmatr.GetKey(week, day, pair);

					if (restrictObject is Group)
					{
						Group _group = restrictObject as Group;
                        if (shmatr.IsGroupHasRestricts(_group.ObjectHardKey, key))
                        {
                            (grid[rowindex, colindex].View as RestrictCellModel).HaveConstraint = true;
                            
                            grid[rowindex, colindex].ToolTipText =
                               shmatr.GetFlatRestrictsText(_group.ObjectHardKey, key);
                        }
                        else
                            (grid[rowindex, colindex].View as RestrictCellModel).HaveConstraint = false;
					}
					
					if (restrictObject is LectureHall)
					{ 
						LectureHall _lectureHall = restrictObject as LectureHall;
                        if (shmatr.IsFlatHasRestricts(_lectureHall.ObjectHardKey, key))
                        {
                            (grid[rowindex, colindex].View as RestrictCellModel).HaveConstraint = true;
                            
                            grid[rowindex, colindex].ToolTipText = 
                                shmatr.GetFlatRestrictsText(_lectureHall.ObjectHardKey, key);
                        }
                        else
                            (grid[rowindex, colindex].View as RestrictCellModel).HaveConstraint = false;
					}

					if (restrictObject is Tutor)
					{	
						Tutor _tutor = restrictObject as Tutor;
                        if (shmatr.IsTutorHasRestricts(_tutor.ObjectHardKey, key))
                        {
                            (grid[rowindex, colindex].View as RestrictCellModel).HaveConstraint = true;
                            grid[rowindex, colindex].ToolTipText =
                               shmatr.GetFlatRestrictsText(_tutor.ObjectHardKey, key);
                        }
                        else
                            (grid[rowindex, colindex].View as RestrictCellModel).HaveConstraint = false;
					}
				}
			this.grid.Refresh();
		}

		public void ShowOnlyShedule(object restrictObject, IShedule shmatr)
		{
			if (restrictObject is Group)
			{
				Group _group = restrictObject as Group;							
				int[] keys = shmatr.GetKeyGroupLessons(_group.ObjectHardKey);
				for(int index=0; index<keys.Length; index++)
				{
					/* �������������� ����� */
					int curday = (keys[index]) / (hourCount*weekCount) + 1;
					int curpair = ((keys[index]) - (curday-1)*hourCount*weekCount)/weekCount+1;
					int curweek = (keys[index]) % weekCount + 1;
                    (grid[(curday - 1) * (weekCount) + curweek, curpair + 1].View as RestrictCellModel).IsBusy = true;
				}                            							
			}
					
			if (restrictObject is LectureHall)
			{ 
				LectureHall _lectureHall = restrictObject as LectureHall;
				int[] keys = shmatr.GetKeyFlatLessons(_lectureHall.ObjectHardKey);
				for(int index=0; index<keys.Length; index++)
				{
					/* �������������� ����� */
					int curday = (keys[index]) / (hourCount*weekCount) + 1;
					int curpair = ((keys[index]) - (curday-1)*hourCount*weekCount)/weekCount+1;
					int curweek = (keys[index]) % weekCount + 1 ;

                    (grid[(curday - 1) * (weekCount) + curweek, curpair + 1].View as RestrictCellModel).IsBusy = true;
				}                            							
			}

			if (restrictObject is Tutor)
			{	
				Tutor _tutor = restrictObject as Tutor;
				int[] keys = shmatr.GetKeyTutorLessons(_tutor.ObjectHardKey);
				for(int index=0; index<keys.Length; index++)
				{
					/* �������������� ����� */
					int curday = (keys[index]) / (hourCount*weekCount) + 1;
					int curpair = ((keys[index]) - (curday-1)*hourCount*weekCount)/weekCount+1;
					int curweek = (keys[index]) % weekCount + 1 ;
                    (grid[(curday - 1) * (weekCount) + curweek, curpair + 1].View as RestrictCellModel).IsBusy = true;
				}                            							
			}		
		this.grid.Refresh();
	}

		public void ShowRestricts(object[] objects,  IShedule shmatr)
		{
            ClearShedule();
			// ������� ���������, �������, ��� �� ���� ����� ������ �������� � ��������� � ���������
			foreach(object restrictObject in objects)
			{
				if (restrictObject is Group)
				{
					Group _group = restrictObject as Group;			
					int[] restrictkey = shmatr.GetKeyGroupRestricts(_group.ObjectHardKey);
					for(int index=0; index<restrictkey.Length; index++)
					{
						/* �������������� ����� */
						int curday = (restrictkey[index]) / (hourCount*weekCount) + 1;
						int curpair = ((restrictkey[index]) - (curday-1)*hourCount*weekCount)/weekCount+1;
						int curweek = (restrictkey[index]) % weekCount + 1;
                        (grid[(curday - 1) * (weekCount) + curweek, curpair + 1].View as RestrictCellModel).HaveConstraint = true;
					}                            							                          							
				}
					
				if (restrictObject is LectureHall)
				{ 
					LectureHall _lectureHall = restrictObject as LectureHall;
					int[] restrictkey = shmatr.GetKeyFlatRestricts(_lectureHall.ObjectHardKey);
					for(int index=0; index<restrictkey.Length; index++)
					{
						/* �������������� ����� */
						int curday = (restrictkey[index]) / (hourCount*weekCount) + 1;
						int curpair = ((restrictkey[index]) - (curday-1)*hourCount*weekCount)/weekCount+1;
						int curweek = (restrictkey[index]) % weekCount + 1;
                        (grid[(curday - 1) * (weekCount) + curweek, curpair + 1].View as RestrictCellModel).HaveConstraint = true;
						//������� ������� � ��������
						if (shmatr.FlatHasLesson(_lectureHall.ObjectHardKey, restrictkey[index]))
						{								
						}
						else
							grid[(curday-1)*(weekCount) + curweek, curpair+1].ToolTipText = 
								shmatr.GetFlatRestrictsText(_lectureHall.ObjectHardKey, restrictkey[index]);
					}                                    							
				}

				if (restrictObject is Tutor)
				{	
					Tutor _tutor = restrictObject as Tutor;
					int[] restrictkey = shmatr.GetKeyTutorRestricts(_tutor.ObjectHardKey);
					for(int index=0; index<restrictkey.Length; index++)
					{
						/* �������������� ����� */
						int curday = (restrictkey[index]) / (hourCount*weekCount) + 1;
						int curpair = ((restrictkey[index]) - (curday-1)*hourCount*weekCount)/weekCount+1;
						int curweek = (restrictkey[index]) % weekCount + 1;
                        (grid[(curday - 1) * (weekCount) + curweek, curpair + 1].View as RestrictCellModel).HaveConstraint = true;
						//������� ������� � ��������
						grid[(curday-1)*(weekCount) + curweek, curpair+1].ToolTipText = 
							shmatr.GetTutorRestrictsText(_tutor.ObjectHardKey, restrictkey[index]);
					}                                    							
				}
			}
			// ������� ������ ��������� ����
			foreach(object restrictObject in objects)
			{
				if (restrictObject is Group)
				{
					Group _group = restrictObject as Group;                           							

					int[] keys = shmatr.GetKeyGroupLessons(_group.ObjectHardKey);					
					for(int index=0; index<keys.Length; index++)
					{
						/* �������������� ����� */
						int curday = (keys[index]) / (hourCount*weekCount) + 1;
						int curpair = ((keys[index]) - (curday-1)*hourCount*weekCount)/weekCount+1;
						int curweek = (keys[index]) % weekCount + 1;
                        (grid[(curday - 1) * (weekCount) + curweek, curpair + 1].View as RestrictCellModel).IsBusy = true;
					}                            							
				}
					
				if (restrictObject is LectureHall)
				{ 
					LectureHall _lectureHall = restrictObject as LectureHall;      

					int[] keys = shmatr.GetKeyFlatLessons(_lectureHall.ObjectHardKey);
					for(int index=0; index<keys.Length; index++)
					{
						/* �������������� ����� */
						int curday = (keys[index]) / (hourCount*weekCount) + 1;
						int curpair = ((keys[index]) - (curday-1)*hourCount*weekCount)/weekCount+1;
						int curweek = (keys[index]) % weekCount + 1 ;

						(grid[(curday-1)*(weekCount) + curweek, curpair+1].View as RestrictCellModel).IsBusy = true;
					}                            							
				}

				if (restrictObject is Tutor)
				{	
					Tutor _tutor = restrictObject as Tutor;        

					int[] keys = shmatr.GetKeyTutorLessons(_tutor.ObjectHardKey);
					for(int index=0; index<keys.Length; index++)
					{
						/* �������������� ����� */
						int curday = (keys[index]) / (hourCount*weekCount) + 1;
						int curpair = ((keys[index]) - (curday-1)*hourCount*weekCount)/weekCount+1;
						int curweek = (keys[index]) % weekCount + 1 ;
						(grid[(curday-1)*(weekCount) + curweek, curpair+1].View as RestrictCellModel).IsBusy = true;
					}                            							
				}
			}
			this.grid.Refresh();
		}
		
		public void ClearShedule()
		{
			for (int colindex=2; colindex< grid.ColumnsCount; colindex++)
				for (int rowindex=1; rowindex<grid.RowsCount; rowindex++ )
				{
					(grid[rowindex, colindex].View as RestrictCellModel).IsBusy = false;
                    (grid[rowindex, colindex].View as RestrictCellModel).HaveConstraint = false;
					grid[rowindex, colindex].ToolTipText = "";
				}
		}

		public int ShedulePositionByXY(int x, int y, out int week, out int day, out int pair)
		{
			Point _clientPoint = new Point(x,y);			
			Position _position = grid.PositionAtPoint(_clientPoint);
			
			pair = _position.Column - 1;

			day = (_position.Row-1) / this.weekCount + 1;
						
			week = (_position.Row-1) % this.weekCount + 1 ;
			
			if (_position.Row > 0 && _position.Column > 1 ) return 1;
			else return 0;
		}
		
		private void grid_MouseUp(object sender, MouseEventArgs e)
		{
			this.OnMouseUp(e);
		}

		private void grid_OnClick(object sender, EventArgs e)
		{
			this.OnClick(e);
		}
		
		public int CellHeight
		{
			get { return this._cellHeight; }
			set { this._cellHeight = value; }
		}

		public int CellWidth
		{
			get { return this._cellWidth; }
			set { this._cellWidth = value; }
		}

        public void ChangeCellSize()
        {
            for (int i = 0; i < grid.ColumnsCount; i++)
                grid.Columns.SetWidth(i, _cellWidth);

            for (int i = 1; i < grid.RowsCount; i++)
                grid.Rows.SetHeight(i, _cellHeight);
            
        }

		private void grid_MouseMove(object sender, MouseEventArgs e)
		{
			this.OnMouseMove(e);
        }

        public IList getSheduleSelPositions()
        {
            IList shposcol = new ArrayList();
            PositionCollection poscol = grid.Selection.GetSelectionRegion().GetCellsPositions();
            foreach (Position _position in poscol)
            {
                int day = (_position.Row - 1) / (weekCount) + 1;
                int pair = (_position.Column - 1);
                int week = (_position.Row - 1) % weekCount + 1;

                ShedulePosition shpos = new ShedulePosition(week, day, pair);
                shpos.Text = grid[_position.Row, _position.Column].ToString();

                shposcol.Add(shpos);
            }
            return shposcol;
        }

        #region Public Properties
        public bool ReadOnly
        {
            get { return _readOnly; }
            set { _readOnly = value; }
        }
        #endregion
    }
}
