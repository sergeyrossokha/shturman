using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Shturman.Nestor.Interfaces;
using Shturman.Shedule.Objects;
using Shturman.Shedule;
using Shturman.Shedule.Server.Interface;
using Shturman.Shedule.Server.Remoting;

using SourceGrid;
using SourceGrid.Cells;
using SourceGrid.Cells.Views;
using Common = SourceGrid.Cells.Views.Cell;
using ContentAlignment = DevAge.Drawing.ContentAlignment;
using Shturman.Shedule.GUI;

namespace Shturman.Shedule.GUI
{
	/// <summary>
	/// Summary description for RestrictSheduleView.
	/// </summary>
	public class RestrictSheduleView2 : UserControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>

        internal class GridHeaders
        {
            public static IView FlatHeaderDefault
            {
                get 
                {
                    IView headerView = new CustomHeaderModel();
                    (headerView as CustomHeaderModel).ChangeDefaultColor(System.Drawing.SystemColors.Control);
                    headerView.TextAlignment = DevAge.Drawing.ContentAlignment.MiddleCenter;

                    return headerView;
                }
            }

            public static IView DayHeaderDefault
            {
                get 
                {
                    IView dayView = new CustomHeaderModel();
                    dayView.TextAlignment = DevAge.Drawing.ContentAlignment.MiddleCenter;
                    dayView.BackColor = Color.LightSkyBlue;
                    (dayView as Common).ElementText = new DevAge.Drawing.VisualElements.TextGDI();
                    (dayView as CustomHeaderModel).FormatFlags = StringFormatFlags.DirectionVertical | StringFormatFlags.NoWrap;
                    
                    return dayView;
                }
            }
        }

		private Grid grid;
        private IView HeaderView = GridHeaders.FlatHeaderDefault;
		private IView dayView;
		private int dayCount = 0;
		private int hourCount = 0;		
		private int _cellHeight = 16;
		private int _cellWidth = 16;
		private Container components = null;

		public RestrictSheduleView2()
		{
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();
			
			grid.Selection.FocusStyle = FocusStyle.None;

            dayView = GridHeaders.DayHeaderDefault;
		}


		public void LoadShedule(IList dayList, IList hourList)
		{
			dayCount = dayList.Count;
			hourCount = hourList.Count;
			//
			// Grid regim 2 - weeks, 6- days, 6 - hours, 5 - tutor
			//
			grid.Redim(1+dayCount,  1 + hourCount);
			//
			// Load grid header
			//
			grid[0,0] = new SourceGrid.Cells.Cell("", typeof(string));
            grid[0, 0].View = HeaderView;
            grid[0, 0].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);			
			//
			// Create grid day caption
			//
			for (int dayindex=1; dayindex< grid.RowsCount; dayindex++ )
			{
				grid.Columns[0].Width = _cellWidth;
				grid.Columns[1].Width = _cellWidth;
				grid.Rows[0].Height = 20;


				grid[dayindex,0] = new SourceGrid.Cells.Cell(dayList[(dayindex-1)].ToString(), typeof(string));
                grid[dayindex,0].View = dayView;
                grid[dayindex, 0].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);
			}

			grid.FixedColumns = 1;
			grid.FixedRows = 1;

			for (int colindex=1; colindex< grid.ColumnsCount; colindex++)
			{
				grid[0, colindex] = new SourceGrid.Cells.Cell(hourList[colindex-1].ToString(), typeof(string));
                grid[0, colindex].View = HeaderView;
				grid.Columns[colindex].Width = _cellWidth;				
                
                grid[0, colindex].RowSpan = 1;

                grid[0, colindex].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);
				
				for (int rowindex=1; rowindex<grid.RowsCount; rowindex++ )
				{
                    grid[rowindex, colindex] = new SourceGrid.Cells.Cell("", typeof(string));
                    grid[rowindex, colindex].View = new RestrictCellModel();
                    grid[rowindex, colindex].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);
                   
                    grid[rowindex, colindex].AddController(SourceGrid.Cells.Controllers.ToolTipText.Default);

                    grid[rowindex, colindex].Model.AddModel(Shturman.Shedule.GUI.RestrictSheduleView.MyToolTipModel.Default);
				}
			}
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			// 
			// FlatSheduleView
			// 
			this.grid = new SourceGrid.Grid();
			this.SuspendLayout();
			// 
			// grid
			//
			this.grid.AutoStretchColumnsToFitWidth = false;
			this.grid.AutoStretchRowsToFitHeight = false;
			this.grid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.grid.CustomSort = false;
			this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.grid.Location = new System.Drawing.Point(0, 0);
			this.grid.Name = "grid";
			this.grid.Size = new System.Drawing.Size(360, 340);
			this.grid.SpecialKeys = SourceGrid.GridSpecialKeys.Default;
			this.grid.TabIndex = 0;
			this.grid.MouseUp += new MouseEventHandler(grid_MouseUp);
			this.grid.Click += new EventHandler(grid_OnClick);
			this.grid.MouseMove += new MouseEventHandler(grid_MouseMove);
			// 
			// RestrictSheduleView
			// 
			this.Controls.Add(this.grid);
			this.Name = "RestrictSheduleView";
			this.Size = new System.Drawing.Size(320, 296);

			//this.Resize += new System.EventHandler(this.grid_Resize);
			this.ResumeLayout(false);
		}
		#endregion

		public void Show(object restrictObject, IShedule2 shmatr, int weekToShow)
		{			
			IList dates = SheduleKeyBuilder.WeekDatesList(shmatr, weekToShow);
			
			for (int colindex=1; colindex< grid.ColumnsCount; colindex++)
				for (int rowindex=1; rowindex<grid.RowsCount; rowindex++ )
				{
					if (restrictObject is Group)
					{
						Group _group = restrictObject as Group;
                        ExtObjKey key = _group.ObjectHardKey;

						// Lessons
						if (shmatr.GroupHasLesson(key, (DateTime)dates[rowindex-1], colindex))
						{
							(grid[rowindex, colindex].View as RestrictCellModel).IsBusy = true;	
						}
						else
							(grid[rowindex, colindex].View as RestrictCellModel).IsBusy = false;						
						// Constraints
						if (shmatr.IsGroupHasRestricts(key, (DateTime)dates[rowindex-1], colindex))
						{
							(grid[rowindex, colindex].View as RestrictCellModel).HaveConstraint = true;
							grid[rowindex, colindex].ToolTipText = shmatr.GetGroupRestrictsText(key, (DateTime)dates[rowindex-1], colindex);
						}
						else
							(grid[rowindex, colindex].View as RestrictCellModel).HaveConstraint = false;
					}
					
					if (restrictObject is LectureHall)
					{ 
						LectureHall _lectureHall = restrictObject as LectureHall;
                        ExtObjKey key = _lectureHall.ObjectHardKey;

						// Lesson
						if (shmatr.FlatHasLesson(key, (DateTime)dates[rowindex-1], colindex))						
						{
							(grid[rowindex, colindex].View as RestrictCellModel).IsBusy = true;

							IList lessons = shmatr.GetFlatLesson(key, (DateTime)dates[rowindex-1], colindex);
							
							string tooltip = "";
							foreach (RemoteLesson2 remlsn in lessons)
							{
								Lesson2 lsn = RemoteAdapter.Adapter.AdaptLesson(remlsn);
								tooltip += lsn.FlatViewString+"\n";
							}
							grid[rowindex, colindex].ToolTipText = tooltip;
						}
						else
							(grid[rowindex, colindex].View as RestrictCellModel).IsBusy = false;
						// Constraints
						if (shmatr.IsFlatHasRestricts(key, (DateTime)dates[rowindex-1], colindex))							
						{
							(grid[rowindex, colindex].View as RestrictCellModel).HaveConstraint = true;
							grid[rowindex, colindex].ToolTipText = shmatr.GetFlatRestrictsText(key, (DateTime)dates[rowindex-1], colindex);
						}
						else
							(grid[rowindex, colindex].View as RestrictCellModel).HaveConstraint = false;
					}
//
//					if (restrictObject is Tutor)
//					{	
//						Tutor _tutor = restrictObject as Tutor;
//						// Lesson
//						if (shmatr.TutorHasLesson(_tutor.Uid, (DateTime)dates[rowindex-1], colindex))
//						{
//							(grid[rowindex, colindex].VisualModel as RestrictCellModel).IsBusy = true;
//							Lesson lsn = RemoteAdapter.Adapter.AdaptLesson(shmatr.GetTutorLesson(_tutor.Uid, (DateTime)dates[rowindex-1], colindex));
//							grid[rowindex, colindex].ToolTipText = lsn.TutorViewString;
//						}
//						else
//							(grid[rowindex, colindex].VisualModel as RestrictCellModel).IsBusy = false;
//						// Constraints						
//						if( shmatr.IsTutorHasRestricts(_tutor.Uid, (DateTime)dates[rowindex-1], colindex))
//							(grid[rowindex, colindex].VisualModel as RestrictCellModel).HaveConstraint = true;
//						else
//							(grid[rowindex, colindex].VisualModel as RestrictCellModel).HaveConstraint = false;
//					}
				}
			this.grid.Refresh();
		}


		public void ShowOnlyConstrains(object restrictObject, IShedule2 shmatr, int weekToShow)
		{
			IList dates = SheduleKeyBuilder.WeekDatesList(shmatr, weekToShow);

            ExtObjKey key = (restrictObject as StoredObject).ObjectHardKey;                        
			
			for (int colindex=2; colindex< grid.ColumnsCount; colindex++)
				for (int rowindex=1; rowindex<grid.RowsCount; rowindex++ )				
				{
					if (restrictObject is Group)
					{
						Group _group = restrictObject as Group;						

						if (shmatr.IsGroupHasRestricts(key, (DateTime)dates[rowindex-1], colindex))
							(grid[rowindex, colindex].View as RestrictCellModel).HaveConstraint = true;
						else
                            (grid[rowindex, colindex].View as RestrictCellModel).HaveConstraint = false;
					}
					
					if (restrictObject is LectureHall)
					{ 
						LectureHall _lectureHall = restrictObject as LectureHall;
						if (shmatr.IsFlatHasRestricts(key, (DateTime)dates[rowindex-1], colindex))
                            (grid[rowindex, colindex].View as RestrictCellModel).HaveConstraint = true;
						else
                            (grid[rowindex, colindex].View as RestrictCellModel).HaveConstraint = false;
					}

					if (restrictObject is Tutor)
					{	
						Tutor _tutor = restrictObject as Tutor;
						if (shmatr.IsTutorHasRestricts(key, (DateTime)dates[rowindex-1], colindex))
                            (grid[rowindex, colindex].View as RestrictCellModel).HaveConstraint = true;
						else
                            (grid[rowindex, colindex].View as RestrictCellModel).HaveConstraint = false;
					}
				}
			this.grid.Refresh();
		}


		public void ShowOnlyShedule(object restrictObject, IShedule2 shmatr)
		{
			if (restrictObject is Group)
			{
				Group _group = restrictObject as Group;
				ExtObjKey key = _group.ObjectHardKey;
							
				int[] keys = shmatr.GetKeyGroupLessons(key);
				for(int index=0; index<keys.Length; index++)
				{
					/* �������������� ����� */
//					int curday = (keys[index]) / (hourCount*weekCount) + 1;
//					int curpair = ((keys[index]) - (curday-1)*hourCount*weekCount)/weekCount+1;
//					int curweek = (keys[index]) % weekCount + 1;
//					(grid[(curday-1)*(weekCount) + curweek, curpair+1].VisualModel as RestrictCellModel).IsBusy = true;
				}                            							
			}
					
			if (restrictObject is LectureHall)
			{ 
				LectureHall _lectureHall = restrictObject as LectureHall;
                ExtObjKey key = _lectureHall.ObjectHardKey;

				int[] keys = shmatr.GetKeyFlatLessons(key);
				for(int index=0; index<keys.Length; index++)
				{
					/* �������������� ����� */
//					int curday = (keys[index]) / (hourCount*weekCount) + 1;
//					int curpair = ((keys[index]) - (curday-1)*hourCount*weekCount)/weekCount+1;
//					int curweek = (keys[index]) % weekCount + 1 ;
//
//					(grid[(curday-1)*(weekCount) + curweek, curpair+1].VisualModel as RestrictCellModel).IsBusy = true;
				}                            							
			}

			if (restrictObject is Tutor)
			{	
				Tutor _tutor = restrictObject as Tutor;
                ExtObjKey key = _tutor.ObjectHardKey;

				int[] keys = shmatr.GetKeyTutorLessons(key);
				for(int index=0; index<keys.Length; index++)
				{
					/* �������������� ����� */
//					int curday = (keys[index]) / (hourCount*weekCount) + 1;
//					int curpair = ((keys[index]) - (curday-1)*hourCount*weekCount)/weekCount+1;
//					int curweek = (keys[index]) % weekCount + 1 ;
//					(grid[(curday-1)*(weekCount) + curweek, curpair+1].VisualModel as RestrictCellModel).IsBusy = true;
				}                            							
			}		
		this.grid.Refresh();
	}

		
		public void ShowRestricts(object[] objects,  IShedule2 shmatr, int weekToShow)
		{
            ClearShedule();

			// ���� �����
			IList dates = SheduleKeyBuilder.WeekDatesList(shmatr, weekToShow);

			#region ������� ���������, �������, ��� �� ���� ����� ������ �������� � ��������� � ���������
			foreach(object restrictObject in objects)
			{
                ExtObjKey key = (restrictObject as StoredObject).ObjectHardKey;
				
				if (restrictObject is Group)
				{
					Group _group = restrictObject as Group;			
					int[] restrictkey = shmatr.GetKeyGroupRestricts(key);
					for(int index=0; index<restrictkey.Length; index++)
					{
						/* �������������� ����� */
						DateTime restrictDate;
						int restrictPair;
						SheduleKeyBuilder.DecompileKey(shmatr, restrictkey[index], out restrictDate, out restrictPair );
						
						int dateIndex = dates.IndexOf(restrictDate);
						if (dateIndex != -1)
							(grid[dateIndex+1, restrictPair].View as RestrictCellModel).HaveConstraint = true;
					}                            							                          							
				}
					
				if (restrictObject is LectureHall)
				{ 
					LectureHall _lectureHall = restrictObject as LectureHall;					

					int[] restrictkey = shmatr.GetKeyFlatRestricts(key);
					for(int index=0; index<restrictkey.Length; index++)
					{
						/* �������������� ����� */
						DateTime restrictDate;
						int restrictPair;
						SheduleKeyBuilder.DecompileKey(shmatr, restrictkey[index], out restrictDate, out restrictPair );
						
						int dateIndex = dates.IndexOf(restrictDate);
						if (dateIndex != -1)
						{
							(grid[dateIndex+1, restrictPair].View as RestrictCellModel).HaveConstraint = true;
							 grid[dateIndex+1, restrictPair].ToolTipText = shmatr.GetFlatRestrictsText(key, restrictDate, restrictPair);
						}
					}                                    							
				}

				if (restrictObject is Tutor)
				{	
					Tutor _tutor = restrictObject as Tutor;
					int[] restrictkey = shmatr.GetKeyTutorRestricts(key);
					for(int index=0; index<restrictkey.Length; index++)
					{
						/* �������������� ����� */
						DateTime restrictDate;
						int restrictPair;
						SheduleKeyBuilder.DecompileKey(shmatr, restrictkey[index], out restrictDate, out restrictPair );
						
						int dateIndex = dates.IndexOf(restrictDate);
						if (dateIndex != -1)
						{
							(grid[dateIndex+1, restrictPair].View as RestrictCellModel).HaveConstraint = true;
							//������� ������� � ��������
							grid[dateIndex+1, restrictPair].ToolTipText = shmatr.GetTutorRestrictsText(key, restrictDate, restrictPair);
						}                                  							
					}
				}
			}
			#endregion
			
			#region ������� ������ ��������� ����
			foreach(object restrictObject in objects)
			{
                ExtObjKey key = (restrictObject as StoredObject).ObjectHardKey;

				if (restrictObject is Group)
				{
					Group _group = restrictObject as Group;                           							

					int[] keys = shmatr.GetKeyGroupLessons(key);					
					for(int index=0; index<keys.Length; index++)
					{
						/* �������������� ����� */
						DateTime restrictDate;
						int restrictPair;
						SheduleKeyBuilder.DecompileKey(shmatr, keys[index], out restrictDate, out restrictPair );
						
						int dateIndex = dates.IndexOf(restrictDate);
						if (dateIndex != -1)
							(grid[dateIndex+1, restrictPair].View as RestrictCellModel).IsBusy = true;
					}                            							
				}
					
				if (restrictObject is LectureHall)
				{ 
					LectureHall _lectureHall = restrictObject as LectureHall;      

					int[] keys = shmatr.GetKeyFlatLessons(key);
					for(int index=0; index<keys.Length; index++)
					{
						/* �������������� ����� */
						DateTime restrictDate;
						int restrictPair;
						SheduleKeyBuilder.DecompileKey(shmatr, keys[index], out restrictDate, out restrictPair );
						
						int dateIndex = dates.IndexOf(restrictDate);
						if (dateIndex != -1)
							(grid[dateIndex+1, restrictPair].View as RestrictCellModel).IsBusy = true;
					}                            							
				}

				if (restrictObject is Tutor)
				{	
					Tutor _tutor = restrictObject as Tutor;        

					int[] keys = shmatr.GetKeyTutorLessons(key);
					for(int index=0; index<keys.Length; index++)
					{
						/* �������������� ����� */
						DateTime restrictDate;
						int restrictPair;
						SheduleKeyBuilder.DecompileKey(shmatr, keys[index], out restrictDate, out restrictPair );
						
						int dateIndex = dates.IndexOf(restrictDate);
						if (dateIndex != -1)
							(grid[dateIndex+1, restrictPair].View as RestrictCellModel).IsBusy = true;
					}                            							
				}
				#endregion
			}
			this.grid.Refresh();
		}

		
		public void ClearShedule()
		{
			for (int colindex=1; colindex< grid.ColumnsCount; colindex++)
				for (int rowindex=1; rowindex<grid.RowsCount; rowindex++ )
				{
                    (grid[rowindex, colindex].View as RestrictCellModel).IsBusy = false;
                    (grid[rowindex, colindex].View as RestrictCellModel).HaveConstraint = false;
					grid[rowindex, colindex].ToolTipText = "";
				}
		}


		public int ShedulePositionByXY(int x, int y, out int week, out int day, out int pair)
		{
			Point _clientPoint = new Point(x,y);			
			Position _position = grid.PositionAtPoint(_clientPoint);
			
			pair = _position.Column - 1;

			day = _position.Row;
						
			week = _position.Row;
			
			if (_position.Row > 0 && _position.Column > 1 ) return 1;
			else return 0;
		}
		
		
		private void grid_MouseUp(object sender, MouseEventArgs e)
		{
			this.OnMouseUp(e);
		}

		
		private void grid_OnClick(object sender, EventArgs e)
		{
			this.OnClick(e);
		}

		
		public int CellHeight
		{
			get { return this._cellHeight; }
			set { this._cellHeight = value; }
		}

		
		public int CellWidth
		{
			get { return this._cellWidth; }
			set { this._cellWidth = value; }
		}

		
		private void grid_MouseMove(object sender, MouseEventArgs e)
		{
			this.OnMouseMove(e);
		}
	}
}
