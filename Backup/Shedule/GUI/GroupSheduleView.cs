using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Shturman.Shedule.Objects;
using Shturman.Shedule;
using Shturman.Shedule.Server.Interface;
using Shturman.Shedule.Server.Remoting;
using SourceGrid;
using SourceGrid.Cells;
using SourceGrid.Cells.Views;
using Common = SourceGrid.Cells.Views.Cell;
using FlatHeader = SourceGrid.Cells.Views.Header;

namespace Shturman.Shedule.GUI
{
	/// <summary>
	/// Summary description for SheduleGrid.
	/// </summary>
	public class GroupSheduleGrid : UserControl
	{
        private Grid sheduleGrid;

        private CustomHeaderModel HeaderView;
        private IView dayView;
        private IView hourView;
        private IView weekView;
        private CustomHeaderModel groupView;

		private IList groupList;
		private IShedule sheduleMatrix;

		private double _scale = 1;
		private int dayCount;
		private int hourCount;
		private int weekCount;

		private Container components = null;

		public GroupSheduleGrid()
		{
			///
			/// Required for Windows.Forms Class Composition Designer support
			///
			InitializeComponent();

            this.sheduleGrid.OptimizeMode = CellOptimizeMode.ForRows;

            this.sheduleGrid.Click += new EventHandler(GroupSheduleGrid_Click);

			sheduleGrid.Selection.FocusStyle = FocusStyle.None;
            IView defaultView = new CustomHeaderModel();
			defaultView.TextAlignment = DevAge.Drawing.ContentAlignment.MiddleCenter;
			defaultView.Font = sheduleGrid.Font.Clone() as Font;

			dayView = defaultView.Clone() as IView;

            (dayView as FlatHeader).ElementText = new DevAge.Drawing.VisualElements.TextGDI();
            (dayView as CustomHeaderModel).FormatFlags = StringFormatFlags.DirectionVertical | StringFormatFlags.NoWrap;
           

			dayView.Font = sheduleGrid.Font.Clone() as Font;

            hourView = (defaultView.Clone() as IView);
			hourView.Font = sheduleGrid.Font.Clone() as Font;

			weekView = new WeekHeaderModel();
			weekView.Font = sheduleGrid.Font.Clone() as Font;

            groupView = new CustomHeaderModel();
            groupView.TextAlignment = DevAge.Drawing.ContentAlignment.MiddleCenter;
            groupView.ChangeDefaultColor(System.Drawing.SystemColors.Control);
			groupView.Font = sheduleGrid.Font.Clone() as Font;

            HeaderView = new CustomHeaderModel();
            HeaderView.ChangeDefaultColor(System.Drawing.SystemColors.Control);
		}

        void GroupSheduleGrid_Click(object sender, EventArgs e)
        {
            this.OnClick(e);
        }

		public void RefreshShedule()
		{
            /*������� ��������� �����*/
           

			for (int colindex = 3; colindex < sheduleGrid.Columns.Count; colindex++)
			{
				ExtObjKey groupObjectHardKey = (groupList[colindex - 3] as Group).ObjectHardKey;

				for (int rowindex = 1; rowindex < sheduleGrid.RowsCount - 1; rowindex++)
				{

					RemoteLesson remoteLesson = sheduleMatrix.GetGroupLesson(groupObjectHardKey, rowindex - 1);
					if (remoteLesson != null)
					{
						string lessonstr = RemoteAdapter.Adapter.AdaptLesson(remoteLesson).GroupViewString;
						sheduleGrid[rowindex, colindex].Value = lessonstr;
                        sheduleGrid[rowindex, colindex].Tag = remoteLesson.leading_uid;
						continue;
					}
					else
                        if (sheduleGrid[rowindex, colindex].Tag != null)
					    {
						    sheduleGrid[rowindex, colindex].Value = "";
                            sheduleGrid[rowindex, colindex].Tag = null;
					    }
				}
			}
		}

		public void ClearShedule()
		{
			sheduleGrid.Redim(sheduleGrid.RowsCount, 3);
		}

        DateTime dt;
        public void ShowShedule(int supplementId, IList groupList, IShedule shmatr)
        {
            this.groupList = groupList;
            sheduleMatrix = shmatr;

            // �������� ���������� ������� ������
            ClearShedule();

            // ������ ����� ������� �� ������
            dt = DateTime.Now;

            foreach (Group gr in groupList)
                if (gr != null)
                {
                    sheduleGrid.Columns.Insert(sheduleGrid.Columns.Count);
                    sheduleGrid[0, sheduleGrid.Columns.Count - 1] = new SourceGrid.Cells.Cell(gr.ToString(), typeof(string));
                    sheduleGrid[0, sheduleGrid.Columns.Count - 1].View = groupView;

                    // ������ ���� ��� �� ���� ���� �������
                    sheduleGrid[0, sheduleGrid.Columns.Count - 1].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);
                    sheduleGrid[0, sheduleGrid.Columns.Count - 1].AddController(SourceGrid.Cells.Controllers.Resizable.ResizeWidth);
                    sheduleGrid.Columns[sheduleGrid.Columns.Count - 1].Width = (int)Math.Round((double)150 * _scale); // * ������
                    
                    System.ComponentModel.BackgroundWorker bw = new BackgroundWorker();
                    bw.DoWork += new DoWorkEventHandler(bw_DoWork);
                    bw.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bw_RunWorkerCompleted);
                    
                    bw.RunWorkerAsync(new object[] { gr, sheduleGrid.Columns.Count - 1, supplementId });

                    for (int rowindex = 1; rowindex < sheduleGrid.RowsCount - 1; rowindex++)
                    {
                        if (rowindex % (hourCount * weekCount) == 0)
                        {
                            sheduleGrid[rowindex, sheduleGrid.Columns.Count - 1] = new SourceGrid.Cells.Cell("", typeof(string));
                            sheduleGrid[rowindex, sheduleGrid.Columns.Count - 1].View = new RestrictCellModel();
                            DevAge.Drawing.RectangleBorder border = new DevAge.Drawing.RectangleBorder(ViewBase.DefaultBorder.Top, ViewBase.DefaultBorder.Bottom, ViewBase.DefaultBorder.Left,
                                ViewBase.DefaultBorder.Right);
                            border.Bottom.Width = 3;
                            border.Bottom.Color = Color.Black;

                            sheduleGrid[rowindex, sheduleGrid.Columns.Count - 1].View.Border = border;
                        }
                        else
                        {
                            sheduleGrid[rowindex, sheduleGrid.Columns.Count - 1] = new SourceGrid.Cells.Cell("", typeof(string));
                            sheduleGrid[rowindex, sheduleGrid.Columns.Count - 1].View = new RestrictCellModel();
                        }

                        // ������ ��������������
                        if (sheduleGrid[rowindex, sheduleGrid.Columns.Count - 1].Editor != null)
                            sheduleGrid[rowindex, sheduleGrid.Columns.Count - 1].Editor.EnableEdit = false;
                        // * ������
                        sheduleGrid.Rows[rowindex].Height = (int)Math.Round((double)25 * _scale);
                    }

                    /*������� ��������� �����*/
                    //foreach (RemoteRestrict restrict in sheduleMatrix.GetGroupRestricts(gr.ObjectHardKey))
                    //{
                    //    int row = (restrict.DayIndex - 1) * weekCount * hourCount + (restrict.HourIndex - 1) * weekCount + (restrict.WeekIndex - 1) + 1;
                    //    sheduleGrid[row, sheduleGrid.Columns.Count - 1].Value = restrict.Text;
                    //}

                    /*������� ������� �����*/
                    //IList groupLessons = sheduleMatrix.GetGroupLessons(gr.ObjectHardKey);
                    //foreach (RemoteLesson remLesson in groupLessons)
                    //    if (remLesson != null)
                    //    {
                    //        int row = (remLesson.day_index - 1) * weekCount * hourCount + (remLesson.hour_index - 1) * weekCount + (remLesson.week_index - 1) + 1;
                    //        sheduleGrid[row, sheduleGrid.Columns.Count - 1].Value = RemoteAdapter.Adapter.ExtractGroupViewString(remLesson);
                    //        sheduleGrid[row, sheduleGrid.Columns.Count - 1].ToolTipText = sheduleGrid[row, sheduleGrid.Columns.Count - 1].Value.ToString();
                    //        sheduleGrid[row, sheduleGrid.Columns.Count - 1].Tag = remLesson.leading_uid;
                    //    }
                }
            //TimeSpan ts = DateTime.Now - dt;
            //sheduleGrid[0, 0].Value = ts.Seconds.ToString() + "." + ts.Milliseconds.ToString() + "���.";
        }

        void bw_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                int index = (int)(e.Result as object[])[0];
                int ldid = (int)(e.Result as object[])[3];
         
                IList groupLessons = (e.Result as object[])[2] as IList;
                foreach (RemoteLesson remLesson in groupLessons)
                    if (remLesson != null)
                    {
                        int row = (remLesson.day_index - 1) * weekCount * hourCount + (remLesson.hour_index - 1) * weekCount + (remLesson.week_index - 1) + 1;
                        sheduleGrid[row, index].Value = RemoteAdapter.Adapter.ExtractGroupViewString(remLesson);
                        sheduleGrid[row, index].ToolTipText = sheduleGrid[row, index].Value.ToString();
                        sheduleGrid[row, index].Tag = remLesson.leading_uid;
                    }
                TimeSpan ts = DateTime.Now - dt;
                sheduleGrid[0, 0].Value = ts.Seconds.ToString() + "." + ts.Milliseconds.ToString() + "���.";

                //dt = DateTime.Now;

                ShowLessons(ldid);

                //ts = DateTime.Now - dt;
                //MessageBox.Show(ts.Seconds.ToString() + "." + ts.Milliseconds.ToString() + "���.");

            }
            catch (Exception e1)
            {
                MessageBox.Show(e1.Message);
            }
        }

        void bw_DoWork(object sender, DoWorkEventArgs e)
        {
            Group gr = (e.Argument as object[])[0] as Group;
            int index = (int)(e.Argument as object[])[1];
            IList restricts = sheduleMatrix.GetGroupRestricts(gr.ObjectHardKey);
            foreach (RemoteRestrict restrict in restricts)
            {
                int row = (restrict.DayIndex - 1) * weekCount * hourCount + (restrict.HourIndex - 1) * weekCount + (restrict.WeekIndex - 1) + 1;
                sheduleGrid[row, index].Value = restrict.Text;
            }

            IList groupLessons = sheduleMatrix.GetGroupLessons(gr.ObjectHardKey);
            
            /*foreach (RemoteLesson remLesson in groupLessons)
                if (remLesson != null)
                {
                    int row = (remLesson.day_index - 1) * weekCount * hourCount + (remLesson.hour_index - 1) * weekCount + (remLesson.week_index - 1) + 1;
                    sheduleGrid[row, index].Value = RemoteAdapter.Adapter.ExtractGroupViewString(remLesson);
                    sheduleGrid[row, index].ToolTipText = sheduleGrid[row, index].Value.ToString();
                    sheduleGrid[row, index].Tag = remLesson.leading_uid;

                    if(remLesson.leading_uid == le)
                    if (!scroledTo)
                    {
                        this.sheduleGrid.ShowCell(new Position(rowindex, colindex));
                        scroledTo = true;
                    }
                    (sheduleGrid[rowindex, colindex].View as RestrictCellModel).BackColor = Color.Turquoise;
                    (sheduleGrid[rowindex, colindex].View as RestrictCellModel).BackColor2 = Color.MediumTurquoise;

                }*/

            e.Result = new object[] { (e.Argument as object[])[1], restricts, groupLessons, (e.Argument as object[])[2] };
        }

        public void ShowLessons(int LeadingUid)
        {
            bool scroledTo = false;
            for (int rowindex = 1; rowindex < sheduleGrid.RowsCount - 1; rowindex++)
                for (int colindex = 3; colindex < sheduleGrid.Columns.Count; colindex++)
                    if (sheduleGrid[rowindex, colindex].Tag != null && sheduleGrid[rowindex, colindex].Tag is int && sheduleGrid[rowindex, colindex].Tag.Equals(LeadingUid))
                    {
                        if (!scroledTo)
                        {
                            this.sheduleGrid.ShowCell(new Position(rowindex, colindex));
                            scroledTo = true;
                        }
                        (sheduleGrid[rowindex, colindex].View as RestrictCellModel).BackColor = Color.Turquoise;
                        (sheduleGrid[rowindex, colindex].View as RestrictCellModel).BackColor2 = Color.MediumTurquoise;
                    }
                    else
                    {
                        (sheduleGrid[rowindex, colindex].View as RestrictCellModel).RestoreColor();
                    }
        }

		public void LoadShedule(IList dayList, IList hourList, IList weekList)
		{
            Grid.MaxSpan = 50;

			dayCount = dayList.Count;
			hourCount = hourList.Count;
			weekCount = weekList.Count;

			//
			// Grid regim 2 - weeks, 6- days, 6 - hours, 5 - group
			//
			sheduleGrid.Redim(2 + dayCount*hourCount*weekCount, 3);

			//
			// Load grid header
			//
            sheduleGrid[0, 0] = new SourceGrid.Cells.Cell("", typeof(string));
            sheduleGrid[0, 0].View = HeaderView;

            // ������ ���� ��� �� ���� ���� �������
            sheduleGrid[0, 0].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);
			sheduleGrid[0, 0].RowSpan = 1;
			sheduleGrid[0, 0].ColumnSpan = 3;

            sheduleGrid.Rows[0].Height = (int)Math.Round((double)25 * _scale);
			//
			// Create grid day caption
			//
			for (int dayindex = 1; dayindex < sheduleGrid.RowsCount - 1; dayindex += hourCount*weekCount)
			{
				sheduleGrid.Columns[0].Width = 20; // * ������
				sheduleGrid.Columns[1].Width = 20; // * ������
				sheduleGrid.Columns[2].Width = 20; // * ������

                sheduleGrid[dayindex, 0] = new SourceGrid.Cells.Cell(dayList[(dayindex - 1) / (hourCount * weekCount)].ToString(), typeof(string)); 
                sheduleGrid[dayindex, 0].View = dayView;
                //
				sheduleGrid[dayindex, 0].RowSpan = hourCount*weekCount;
                sheduleGrid[dayindex, 0].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);
				
                for (int hourindex = dayindex; hourindex < (dayindex + hourCount*weekCount); hourindex += weekCount)
				{
					sheduleGrid[hourindex, 1] = new SourceGrid.Cells.Cell(hourList[(hourindex - dayindex)/(weekCount)], typeof(string));
                    sheduleGrid[hourindex, 1].View = hourView;
					sheduleGrid[hourindex, 1].RowSpan = 2;

                    sheduleGrid[hourindex, 1].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);

					for (int weekindex = hourindex; weekindex < (hourindex + 2); weekindex++)
					{
						sheduleGrid[weekindex, 2] = new SourceGrid.Cells.Cell (weekList[(weekindex - hourindex)].ToString(), typeof(string));
                        sheduleGrid[weekindex, 2].View = weekView;
                        sheduleGrid[weekindex, 2].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);
					}
				}
			}

			sheduleGrid.FixedColumns = 3;
			sheduleGrid.FixedRows = 1;
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.sheduleGrid = new SourceGrid.Grid();
            this.SuspendLayout();
            // 
            // sheduleGrid
            // 
            this.sheduleGrid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.sheduleGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.sheduleGrid.Location = new System.Drawing.Point(0, 0);
            this.sheduleGrid.Name = "sheduleGrid";
            this.sheduleGrid.OptimizeMode = SourceGrid.CellOptimizeMode.ForRows;
            this.sheduleGrid.SelectionMode = SourceGrid.GridSelectionMode.Cell;
            this.sheduleGrid.Size = new System.Drawing.Size(364, 344);
            this.sheduleGrid.TabIndex = 0;
            this.sheduleGrid.TabStop = true;
            this.sheduleGrid.ToolTipText = "";
            this.sheduleGrid.MouseMove += new System.Windows.Forms.MouseEventHandler(this.sheduleGrid_MouseMove);
            this.sheduleGrid.DragDrop += new System.Windows.Forms.DragEventHandler(this.sheduleGrid_DragDrop);
            // 
            // GroupSheduleGrid
            // 
            this.AutoSize = true;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.sheduleGrid);
            this.Name = "GroupSheduleGrid";
            this.Size = new System.Drawing.Size(364, 344);
            this.ResumeLayout(false);

		}

		#endregion

		public IList getSheduleSelPositions()
		{
			IList shposcol = new ArrayList();
            PositionCollection poscol = this.sheduleGrid.Selection.GetSelectionRegion().GetCellsPositions();
			foreach (Position _position in poscol)
			{
				int day = (_position.Row - 1)/(hourCount*weekCount) + 1;
				int pair = ((_position.Row - 1) - (day - 1)*hourCount*weekCount)/weekCount + 1;
				int week = (_position.Row - 1)%weekCount + 1;

                

				ShedulePosition shpos = new ShedulePosition(week, day, pair);
                shpos.Text = sheduleGrid[_position.Row, _position.Column].ToString();

				if (shposcol.Contains(shpos))
				{
					shpos = shposcol[shposcol.IndexOf(shpos)] as ShedulePosition;
					if (!shpos.Columns.Contains(groupList[_position.Column - 3]))
						shpos.Columns.Add(groupList[_position.Column - 3]);
				}
				else
				{
					shposcol.Add(shpos);
					if (!shpos.Columns.Contains(groupList[_position.Column - 3]))
						shpos.Columns.Add(groupList[_position.Column - 3]);
				}
			}
			return shposcol;
		}

		public int GetShedulePosition(int x, int y, out int week, out int day, out int pair)
		{
			Point _clientPoint = sheduleGrid.PointToClient(new Point(x, y));
			Position _position = sheduleGrid.PositionAtPoint(_clientPoint);

			day = (_position.Row - 1)/(hourCount*weekCount) + 1;
			pair = ((_position.Row - 1) - (day - 1)*hourCount*weekCount)/weekCount + 1;
			week = (_position.Row - 1)%weekCount + 1;

			if (_position.Row > 0 && _position.Column > 2) return 1;
			else return 0;
		}

		public void Scale(int scaleTo)
		{
			_scale = scaleTo/100.0;
			/* ������������ ����� ����� */
			Font newFont = new Font(sheduleGrid.Font.FontFamily, (float) (this.Font.Size*_scale));
			sheduleGrid.Font = newFont;
			if (weekView.Font != null)
				weekView.Font = newFont;
			if (dayView.Font != null)
				dayView.Font = newFont;
			if (hourView.Font != null)
				hourView.Font = newFont;
			if (HeaderView.Font != null)
				HeaderView.Font = newFont;
			if (groupView.Font != null)
				groupView.Font = newFont;

			/* ������� ����� �������� ���� */
			for (int colindex = 0; colindex < sheduleGrid.Columns.Count; colindex++)
			{
				for (int rowindex = 0; rowindex < sheduleGrid.RowsCount - 1; rowindex++)
				{
					sheduleGrid[rowindex, colindex].View.Font = newFont;
					// ������
					sheduleGrid.Rows[rowindex].Height = (int) Math.Round((double) 25*_scale);
				}
				// ������
				if (colindex > 2)
					sheduleGrid.Columns[colindex].Width = (int) Math.Round((double) 150*_scale);
				else
					sheduleGrid.Columns[colindex].Width = (int) Math.Round((double) 20*_scale);
			}
		}

		public void SetFocus(int day, int week, int pair)
		{
			int cellIndex = (day - 1)*(hourCount*weekCount) + pair*weekCount + week - 2;
            sheduleGrid.Selection.ResetSelection();
            sheduleGrid.Selection.SelectCell(new Position(cellIndex, 3), true);
            sheduleGrid.ShowCell(new Position(cellIndex, 3));
		}

        public int GetLeadingID()
        {
            if (sheduleGrid.Selection == null)
                return -1;

            object tag = sheduleGrid[sheduleGrid.Selection.ActivePosition.Row,
                sheduleGrid.Selection.ActivePosition.Column].Tag;

            if (tag != null)
            {
                return (int)tag;
            }
            else
                return -1;
        }

        private void sheduleGrid_DragDrop(object sender, DragEventArgs e)
        {
                RaiseDragEvent(sender, e);
        }

        private void sheduleGrid_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.getSheduleSelPositions().Count != 0)
                if (e.Button == MouseButtons.Left)
                    this.sheduleGrid.DoDragDrop(23, DragDropEffects.Copy | DragDropEffects.Move);
        }
	}
}