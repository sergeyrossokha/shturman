using System;
using System.Drawing;
using System.Windows.Forms;

namespace Shturman.Shedule.GUI
{
	/// <summary>
	/// �������, ������������ �������� ��������� ������ ��� ��������� �����
	/// </summary>
	public delegate void DataGridColorEventHandler(
	object sender, 
	DataGridColorlEventArgs e);

	/// <summary>
	/// Summary description for DataGridColorlEventArgs.
	/// </summary>
	public class DataGridColorlEventArgs: EventArgs
	{
		/// <summary>
		/// ����� ������� �������� ������
		/// </summary>
		public readonly int Column;
		/// <summary>
		///  ����� ������ �������� ������
		/// </summary>
		public readonly int Row;
		/// <summary>
		/// ������� �������� � ������
		/// </summary>
		public readonly object CurrentCellValue;

		/// <summary>
		///  ����� ��� ������ ������ � ������
		/// </summary>
		public Font TextFont;

		/// <summary>
		/// �����, ������������ ��� ��������� ���� ������
		/// </summary>
		public Brush BackBrush;

		/// <summary>
		///  ����� ��� ��������� ������ � ������
		/// </summary>
		public Brush ForeBrush;

		/// <summary>
		/// ���� ����� true, ���������� TextFont.Dispose
		/// </summary>
		public bool TextFontDispose;

		/// <summary>
		/// ���� ����� true, ���������� BackBrush.Dispose
		/// </summary>
		public bool BackBrushDispose;

		/// <summary>
		///  ���� ����� true, ���������� ForeBrush.Dispose
		/// </summary>
		public bool ForeBrushDispose;

		/// <summary>
		/// ���� ����� true, ���������� MyBase.Paint
		/// </summary>
		public bool UseBaseClassDrawing;

		/// <summary>
		/// �������� ������� �����
		/// </summary>
		/// <param name="column">����� �������</param>
		/// <param name="row">����� ������</param>
		/// <param name="currentCellValue">�������� ������� ������</param>
		public DataGridColorlEventArgs(
			int column,
			int row,
			object currentCellValue)
		{
			Column = column;
			Row = row;
			CurrentCellValue = currentCellValue;
		}
	}



	public class DataGridColoredTextBoxColumn : DataGridTextBoxColumn
	{
		public DataGridColoredTextBoxColumn()
		{}

		protected override void Paint(
			Graphics g, 
			Rectangle bounds, 
			CurrencyManager source, 
			int rowNum, 
			Brush backBrush, 
			Brush foreBrush, 
			bool alignToRight)
		{
			try
			{
				int col = DataGridTableStyle.GridColumnStyles.IndexOf(this);
				DataGridColorlEventArgs e = 
					new DataGridColorlEventArgs(col, rowNum,
					this.GetColumnValueAtRow(source, rowNum));
				OnCellPaint(e);

				if (e.BackBrush != null)
					backBrush = e.BackBrush;
				if (e.ForeBrush != null)
					foreBrush = e.ForeBrush;
			}
			finally
			{
				// ����������� �������� ��������� �������� ������
				base.Paint (g, bounds, source, rowNum, backBrush, 
					foreBrush, alignToRight);
			}
		}

		/// <summary>
		/// �������, ������� ���������� ������������ �������
		/// ��� ������� � ����� � ������ ����������� ������
		/// </summary>
		public event DataGridColorEventHandler CellPaint;
		/// <summary>
		/// �������� ������� SetCellFormat
		/// </summary>
		/// <param name="e"></param>
		protected virtual void OnCellPaint(
			DataGridColorlEventArgs e)
		{
			if (CellPaint != null)
				CellPaint(this, e);
		}

	}
}
