using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Shturman.Nestor.Interfaces;
using Shturman.Shedule.Objects;
using Shturman.Shedule;
using Shturman.Shedule.Server.Interface;
using Shturman.Shedule.Server.Remoting;

using SourceGrid;
using SourceGrid.Cells;
using SourceGrid.Cells.Views;
using Common = SourceGrid.Cells.Views.Cell;
using FlatHeader = SourceGrid.Cells.Views.Header;

namespace Shturman.Shedule.GUI
{
	/// <summary>
	/// Summary description for SheduleGrid.
	/// </summary>
	public class GroupSheduleView2 : UserControl
	{		
		private Grid sheduleGrid;

        private RestrictCellModel EmptyView = new RestrictCellModel();
        private CustomHeaderModel HeaderView;
        private IView dayView;
        private IView hourView;
        private CustomHeaderModel groupView;

		private IList groupList;
		private IList weekDaysData;
		private IShedule2 sheduleMatrix;

		private double _scale = 1;
		private int dayCount;
		private int hourCount;
		private int weekIndex = 1;

		private Container components = null;

		/// <summary>
		/// ����������� ����� GroupSheduleDateView
		/// </summary>
		public GroupSheduleView2()
		{
			///
			/// Required for Windows.Forms Class Composition Designer support
			///
			InitializeComponent();

            this.sheduleGrid.OptimizeMode = CellOptimizeMode.ForRows;

            sheduleGrid.Selection.FocusStyle = FocusStyle.None;
            IView defaultView = new CustomHeaderModel();
            defaultView.TextAlignment = DevAge.Drawing.ContentAlignment.MiddleCenter;
            defaultView.Font = sheduleGrid.Font.Clone() as Font;

            dayView = defaultView.Clone() as IView;

            (dayView as FlatHeader).ElementText = new DevAge.Drawing.VisualElements.TextGDI();
            (dayView as CustomHeaderModel).FormatFlags = StringFormatFlags.DirectionVertical | StringFormatFlags.NoWrap;


            dayView.Font = sheduleGrid.Font.Clone() as Font;

            hourView = (defaultView.Clone() as IView);
            hourView.Font = sheduleGrid.Font.Clone() as Font;

            groupView = new CustomHeaderModel();
            groupView.TextAlignment = DevAge.Drawing.ContentAlignment.MiddleCenter;
            groupView.ChangeDefaultColor(System.Drawing.SystemColors.Control);
            groupView.Font = sheduleGrid.Font.Clone() as Font;

            HeaderView = new CustomHeaderModel();
            HeaderView.ChangeDefaultColor(System.Drawing.SystemColors.Control);



			/*sheduleGrid.Selection.FocusStyle = FocusStyle.None;
			IVisualModel defaultView = new FlatHeader();
			defaultView.TextAlignment = ContentAlignment.MiddleCenter;
			defaultView.BackColor = Color.LightSteelBlue;	
			defaultView.Font = sheduleGrid.Font.Clone() as Font;

			dayView = defaultView.Clone() as IVisualModel;
			(dayView as Common).StringFormat.FormatFlags = StringFormatFlags.DirectionVertical;
			dayView.Font = sheduleGrid.Font.Clone() as Font;

			hourView = (defaultView.Clone() as IVisualModel);
			hourView.Font = sheduleGrid.Font.Clone() as Font;

			groupView = new FlatHeader();
			groupView.TextAlignment = ContentAlignment.MiddleLeft;
			groupView.BackColor = Color.Transparent;
			groupView.Font = sheduleGrid.Font.Clone() as Font;*/
		}


		public void RefreshShedule()
		{
			for(int colindex = 3; colindex<sheduleGrid.Columns.Count; colindex++ )
			{
				ExtObjKey key = (groupList[colindex-3] as Group).ObjectHardKey;
				
				for (int rowindex=1; rowindex<sheduleGrid.RowsCount-1; rowindex++ )
				{
					int dayIndex = (rowindex-1) / (this.hourCount)+1;
					int pairIndex = ((rowindex-1) - (dayIndex-1)*this.hourCount)+1;

					if (sheduleMatrix.GroupHasLesson(key, (DateTime)weekDaysData[dayIndex-1],pairIndex))								
					{
						string lessonstr = RemoteAdapter.Adapter.AdaptLesson(sheduleMatrix.GetGroupLesson(key, (DateTime)weekDaysData[dayIndex-1],pairIndex)).GroupViewString;
						sheduleGrid[rowindex, colindex].Value = lessonstr;
						continue;
					}
					else
					{
						sheduleGrid[rowindex, colindex].Value = "";
					}
				}
			}
		}

		
		/// <summary>
		/// ������� �������� �������� ������
		/// </summary>
		public void ClearShedule()
		{
			sheduleGrid.Redim(sheduleGrid.RowsCount, 3);
		}


		/// <summary>
		/// ������� ����������� �������� ������ �� ������
		/// </summary>
		/// <param name="groupList">������ ���� ��� ���� ��������� ������� �������</param>
		/// <param name="shmatr">������� ������</param>
		public void ShowShedule(IList groupList, IShedule2 shmatr, int weekToShow)
		{            
			//sheduleGrid.Redraw = false;
			
			this.weekIndex = weekToShow;
			this.groupList = groupList;
			sheduleMatrix = shmatr;

			ClearShedule();			

			// �������� ���� ��� ���� �� �����
			weekDaysData = SheduleKeyBuilder.WeekDatesList(shmatr, weekToShow);
			
			foreach(Group gr in groupList)
			{
				sheduleGrid.Columns.Insert(sheduleGrid.Columns.Count);
                sheduleGrid[0, sheduleGrid.Columns.Count - 1] = new SourceGrid.Cells.Cell(gr.ToString(), typeof(string));
                sheduleGrid[0, sheduleGrid.Columns.Count - 1].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);
                sheduleGrid[0, sheduleGrid.Columns.Count - 1].AddController(SourceGrid.Cells.Controllers.Resizable.ResizeWidth);
				sheduleGrid.Columns[sheduleGrid.Columns.Count-1].Width = (int)Math.Round( (double) 150 * _scale );	// * ������
				
				for (int rowindex=1; rowindex<sheduleGrid.RowsCount-1; rowindex++ )
				{
					int dayIndex = (rowindex-1) / (this.hourCount)+1;
					int pairIndex = ((rowindex-1) - (dayIndex-1)*this.hourCount)+1;
					//³��������� ���� � ������ �������
					if ( (rowindex-2) % (hourCount) == 0)
					{	
						int	day = (rowindex-2) / hourCount;					
						sheduleGrid[rowindex-1, 0].Value = ((DateTime)weekDaysData[day]).ToShortDateString();
					}

					if (sheduleMatrix.GroupHasLesson(gr.ObjectHardKey, (DateTime)weekDaysData[dayIndex-1],pairIndex))						
					{
                        RemoteLesson2 remoteLesson2 = sheduleMatrix.GetGroupLesson(gr.ObjectHardKey, (DateTime)weekDaysData[dayIndex - 1], pairIndex);
						string lessonstr = RemoteAdapter.Adapter.ExtractGroupViewString(remoteLesson2);

                        sheduleGrid[rowindex, sheduleGrid.Columns.Count - 1] = new SourceGrid.Cells.Cell(lessonstr, typeof(string));
                        sheduleGrid[rowindex, sheduleGrid.Columns.Count - 1].View = new RestrictCellModel();
						sheduleGrid[rowindex, sheduleGrid.Columns.Count-1].Tag = remoteLesson2.leading_uid;

						sheduleGrid.Rows[rowindex].Height = (int)Math.Round( (double) 25 * _scale );// * ������
						continue;
					}
					else
					{
                        sheduleGrid[rowindex, sheduleGrid.Columns.Count - 1] = new SourceGrid.Cells.Cell("", typeof(string));
                        sheduleGrid[rowindex, sheduleGrid.Columns.Count - 1].View = new RestrictCellModel();
						sheduleGrid.Rows[rowindex].Height = (int)Math.Round( (double) 25 * _scale );// * ������
					}

//					if (sheduleMatrix.IsGroupHasRestricts(groupUID, rowindex-1))
//					{	
//						string comment = sheduleMatrix.GetGroupRestrictsText(groupUID, rowindex-1);
//						sheduleGrid[rowindex, sheduleGrid.Columns.Count-1] = new Cell(comment, null, new RestrictCellModel());
//						sheduleGrid.Rows[rowindex].Height = (int)Math.Round( (double) 25 * _scale ); // * ������
//						continue;
//					}

					if ( (rowindex % hourCount) == 0)
					{
                        DevAge.Drawing.RectangleBorder border = new DevAge.Drawing.RectangleBorder(ViewBase.DefaultBorder.Top, ViewBase.DefaultBorder.Bottom, ViewBase.DefaultBorder.Left,
                                ViewBase.DefaultBorder.Right);
                        border.Bottom.Width = 3;
                        border.Bottom.Color = Color.Black;

                        sheduleGrid[rowindex, sheduleGrid.Columns.Count - 1].View.Border = border;
					}
				}
			}
		}


		/// <summary>
		/// ϳ�������� ���� ��� ����������� �������� 
		/// </summary>
		/// <param name="dayList">������ ���� ���� ��� ����������� � �������</param>
		/// <param name="hourList">������ ���� ��� ��� ����������� � �������</param>
		public void LoadShedule(IList dayList, IList hourList)
		{
			dayCount = dayList.Count;
			hourCount = hourList.Count;

			//
			// Grid regim 2 - weeks, 6- days, 6 - hours
			//
			sheduleGrid.Redim(2+dayCount*hourCount, 3);

			//
			// Load grid header
			//
            sheduleGrid[0, 0] = new SourceGrid.Cells.Cell("", typeof(string));
            sheduleGrid[0, 0].View = HeaderView;

			sheduleGrid[0,0].RowSpan =1;
			sheduleGrid[0,0].ColumnSpan = 3;
            sheduleGrid[0, 0].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);
			//
			// Create grid day caption
			//
	
			sheduleGrid.Columns[0].Width = 20; // * ������
			sheduleGrid.Columns[1].Width = 20; // * ������
			sheduleGrid.Columns[2].Width = 20; // * ������
			
			for(int dayindex=0; dayindex<dayCount; dayindex++)
			{
				int cellindex = dayindex*hourCount+1;

                sheduleGrid[cellindex, 0] = new SourceGrid.Cells.Cell("", typeof(string));
                sheduleGrid[cellindex, 0].View = dayView;
				sheduleGrid[cellindex,0].RowSpan = hourCount;
				sheduleGrid[cellindex,0].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);

                sheduleGrid[cellindex, 1] = new SourceGrid.Cells.Cell(dayList[dayindex], typeof(string));
                sheduleGrid[cellindex, 1].View = dayView;
				sheduleGrid[cellindex,1].RowSpan = hourCount;
				sheduleGrid[cellindex,1].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);

				for(int hourindex=cellindex; hourindex<(cellindex+hourCount); hourindex++ )
				{
                    sheduleGrid[hourindex, 2] = new SourceGrid.Cells.Cell(hourList[(hourindex - cellindex)], typeof(string));
                    sheduleGrid[cellindex, 1].View = hourView;
					sheduleGrid[hourindex, 2].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);
				}
			}
			sheduleGrid.FixedColumns = 2;
			sheduleGrid.FixedRows = 1;
		}

		
		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.sheduleGrid = new SourceGrid.Grid();
			this.SuspendLayout();
			// 
			// grid
			// 
			//this.sheduleGrid.AutoSizeMinHeight = 10;
			//this.sheduleGrid.AutoSizeMinWidth = 10;
			this.sheduleGrid.AutoStretchColumnsToFitWidth = false;
			this.sheduleGrid.AutoStretchRowsToFitHeight = false;
			this.sheduleGrid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			//this.sheduleGrid.ContextMenuStyle = SourceGrid2.ContextMenuStyle.None;
			this.sheduleGrid.CustomSort = false;
			this.sheduleGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			//this.sheduleGrid.GridToolTipActive = true;
			this.sheduleGrid.Location = new System.Drawing.Point(0, 0);
			this.sheduleGrid.Name = "grid";
			this.sheduleGrid.Size = new System.Drawing.Size(360, 340);
			//this.sheduleGrid.SpecialKeys = SourceGrid2.GridSpecialKeys.Default;
			this.sheduleGrid.TabIndex = 0;
			// 
			// GroupSheduleGrid
			// 
			this.Controls.Add(this.sheduleGrid);
			this.Name = "GroupSheduleGrid";
			this.Size = new System.Drawing.Size(360, 340);
			this.ResumeLayout(false);
		}
		#endregion

		public IList getSheduleSelPositions()
		{
			IList shposcol = new ArrayList();
            PositionCollection poscol = this.sheduleGrid.Selection.GetSelectionRegion().GetCellsPositions();
			foreach (Position pos in poscol )
			{
				int day = (pos.Row-1) / (sheduleMatrix.GetPairsPerDayCount()) + 1;
				int pair = ((pos.Row-1) - (day-1)*sheduleMatrix.GetPairsPerDayCount())+1;
				
				ShedulePosition2 shpos = new ShedulePosition2((DateTime)weekDaysData[day-1], pair);
				
				if (shposcol.Contains(shpos))
				{
					shpos = shposcol [shposcol.IndexOf(shpos)] as ShedulePosition2;
					if (!shpos.Columns.Contains(groupList[pos.Column-3]))
						shpos.Columns.Add(groupList[pos.Column-3]);
				}
				else 
				{
					shposcol.Add( shpos );
					if (!shpos.Columns.Contains(groupList[pos.Column-3]))
						shpos.Columns.Add(groupList[pos.Column-3]);
				}
			}
			return shposcol;
		}
		
		public int GetShedulePosition(int x, int y, out DateTime dayDate, out int dayIndex, out int pairIndex)
		{
			Point _clientPoint = sheduleGrid.PointToClient(new Point(x,y));			
			Position _position = sheduleGrid.PositionAtPoint(_clientPoint);

			dayIndex = (_position.Row-1) / (this.hourCount)+1;
			pairIndex = ((_position.Row-1) - (dayIndex-1)*this.hourCount)+1;			
			dayDate = (DateTime)weekDaysData[dayIndex-1];
			
			if (_position.Row > 0 && _position.Column > 2 ) return 1;
			else return 0;
		}

		public void ShowLessons(int LeadingUid)
		{
			bool scroledTo = false;
			for (int rowindex = 1; rowindex < sheduleGrid.RowsCount - 1; rowindex++)
				for (int colindex = 2; colindex < sheduleGrid.Columns.Count; colindex++)
					if(sheduleGrid[rowindex, colindex].Tag != null && sheduleGrid[rowindex, colindex].Tag is int && sheduleGrid[rowindex, colindex].Tag.Equals(LeadingUid))
					{
						if (!scroledTo)
						{
                            this.sheduleGrid.ShowCell(new Position(rowindex, colindex));
							scroledTo = true;
						}
                        (sheduleGrid[rowindex, colindex].View as RestrictCellModel).BackColor = Color.Turquoise;
                        (sheduleGrid[rowindex, colindex].View as RestrictCellModel).BackColor2 = Color.MediumTurquoise;
                    }
		}
		
		public void Scale(int scaleTo)
		{	
			_scale = scaleTo/100.0;
			/* ������������ ����� ����� */
			Font newFont = new Font( sheduleGrid.Font.FontFamily, (float)(this.Font.Size * _scale));
			sheduleGrid.Font = newFont;
			if (dayView.Font != null)
				dayView.Font = newFont;
			if (hourView.Font != null)
				hourView.Font = newFont;
			if (HeaderView.Font != null)
				HeaderView.Font = newFont;
			if (groupView.Font != null)
				groupView.Font = newFont;
			
			/* ������� ����� �������� ���� */
			for(int colindex = 0; colindex<sheduleGrid.Columns.Count; colindex++ )			
			{
				for (int rowindex=0; rowindex<sheduleGrid.RowsCount-1; rowindex++ )
				{	
                    sheduleGrid[rowindex, colindex].View.Font = newFont;	
					// ������
					sheduleGrid.Rows[rowindex].Height = (int)Math.Round( (double) 25 * _scale ); 
				}
				// ������
				if (colindex > 2)
					sheduleGrid.Columns[colindex].Width = (int)Math.Round( (double) 150 * _scale );
				else
					sheduleGrid.Columns[colindex].Width = (int)Math.Round( (double) 20 * _scale );
			}			
		}


		public void SetFocus(int day, int week, int pair)
		{
			int cellIndex = (day-1)*(sheduleMatrix.GetPairsPerDayCount()*sheduleMatrix.GetWeeksCount())+ pair*sheduleMatrix.GetWeeksCount() + week -2;

            sheduleGrid.Selection.ResetSelection();
            sheduleGrid.Selection.SelectCell(new Position(cellIndex, 3), true);
            sheduleGrid.ShowCell(new Position(cellIndex, 3));
		}
	}
}
