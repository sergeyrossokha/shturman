using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using Shturman.Shedule.Objects;
using Shturman.Shedule.Server.Interface;
using Shturman.Shedule.Server.Remoting;

namespace Shturman.Shedule.Commands
{
    class ChangeFlatCmd : ICommand
    {
        private IList _lessons;
        private IList _newFlats;

        public ChangeFlatCmd(IList lessons, IList newFlats)
        {
            _lessons = lessons;
            _newFlats = newFlats;
        }

        #region ICommand
        public bool Do(IShedule shedule)
        {
            try
            {
                RemoteLesson[] lesson_remList = new RemoteLesson[_lessons.Count];
                for (int i = 0; i < _lessons.Count; i++)
                    lesson_remList[i] = RemoteAdapter.Adapter.AdaptLesson((Lesson)_lessons[i]);

                ExtObjKey[] flat_uidList = new ExtObjKey[_newFlats.Count];
                for (int i = 0; i < _newFlats.Count; i++)
                    flat_uidList[i] = ((LectureHall)_newFlats[i]).ObjectHardKey;

                // ����� ��������, ����� ������ ��������� ������� ����� SheduleMatrix
                shedule.ChangeLessonFlats(lesson_remList, flat_uidList);

                if (OnDo != null)
                    foreach (AfterDo ad in OnDo.GetInvocationList())
                        ad.Invoke();

                return true;
            }
            catch
            {
                return false;
            }
        }
        
        public void UnDo(IShedule shedule)
        {
            foreach (Lesson lsn in _lessons)
            {
                RemoteLesson remlsn = RemoteAdapter.Adapter.AdaptLesson(lsn);
                
                ExtObjKey[] flat_uidList = new ExtObjKey[remlsn.flatList.Count];
                remlsn.flatList.CopyTo(flat_uidList, 0);

                // ����� ��������, ����� ������ ��������� ������� ����� SheduleMatrix
                RemoteLesson[] rml= new RemoteLesson[] {remlsn};
                shedule.ChangeLessonFlats(rml, flat_uidList);
            }

            if (OnUndo != null)
                foreach (AfterUndo aud in OnUndo.GetInvocationList())
                    aud.Invoke();
        }
        #endregion

        public event AfterDo OnDo;
        public event AfterUndo OnUndo;
    }
}
