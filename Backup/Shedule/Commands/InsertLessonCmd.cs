using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using Shturman.Shedule.Objects;
using Shturman.Shedule.Server.Interface;
using Shturman.Shedule.Server.Remoting;


namespace Shturman.Shedule.Commands
{
    public delegate bool BeforeInsertLesson (IShedule shedule, StudyDay sd, StudyCycles sc, StudyHour sh,
            StudyLoading sl, IList flats);

    public delegate void AfterInsertLesson(IShedule shedule, Lesson lsn);
    
    public delegate void AfterDo();
    public delegate void AfterUndo();

    class InsertLessonCmd: ICommand
    {
        private StudyDay _sd; 
        private StudyCycles _sc; 
        private StudyHour _sh;
        private StudyLoading _sl;
        private IList _flats;
        
        public InsertLessonCmd( StudyDay sd, StudyCycles sc, StudyHour sh,
            StudyLoading sl, IList flats)
        {
            _sd = sd;
            _sc = sc;
            _sh = sh;
            _sl = sl;
            _flats = flats;
        }

        #region inplementation ICommand
        public bool Do(IShedule shedule)
        {
            if (BeforeInsert != null)
                foreach(BeforeInsertLesson bil in BeforeInsert.GetInvocationList())
                    if (bil.Invoke(shedule, _sd, _sc, _sh, _sl, _flats) == false)
                        return false;

            int _week = int.Parse(_sc.MnemoCode);
            int _day = int.Parse(_sd.MnemoCode);
            int _pair = int.Parse(_sh.MnemoCode);

            Lesson newlesson = new Lesson(_sl.SubjectName, _sl.StudyForm, _sl.Groups, _sl.Tutors, _flats);

            newlesson.LeadingUid = _sl.Uid;
            newlesson.CurLessonDay = _sd;
            newlesson.CurLessonPair = _sh;
            newlesson.CurLessonWeek = _sc;
            newlesson.Owner = CurrentSession.instance.CurrentUser;

            RemoteLesson remoteLesson = RemoteAdapter.Adapter.AdaptLesson(newlesson);

            if (shedule.Insert(remoteLesson.week_index, remoteLesson.day_index, remoteLesson.hour_index, remoteLesson) != -1)
            {
                _sl.UsedHourByWeek++;
                
                if (AfterInsert != null)
                    foreach (AfterInsertLesson ail in AfterInsert.GetInvocationList())
                        ail.Invoke(shedule, newlesson);

                if (OnDo != null)
                    foreach (AfterDo ad in OnDo.GetInvocationList())
                        ad.Invoke();
            }

            return true;
        }

        public void UnDo(IShedule shedule)
        {
            IList groups = new ArrayList();

            int _week = int.Parse(_sc.MnemoCode);
            int _day = int.Parse(_sd.MnemoCode);
            int _pair = int.Parse(_sh.MnemoCode);

            foreach (Group gr in _sl.Groups)
                if (gr != null)
                {
                    groups.Add(gr.ObjectHardKey);

                    shedule.DeleteByGroup(_week, _day, _pair, groups);
                    
                    _sl.UsedHourByWeek--;

                    break;
                }

            if (OnUndo != null)
                foreach (AfterUndo aud in OnUndo.GetInvocationList())
                    aud.Invoke();
        }
        #endregion

        public event BeforeInsertLesson BeforeInsert;
        public event AfterInsertLesson AfterInsert;
        public event AfterDo OnDo;
        public event AfterUndo OnUndo;
    }
}
