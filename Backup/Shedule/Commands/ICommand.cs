using System;
using System.Collections.Generic;
using System.Text;

using Shturman.Shedule.Server.Interface;

namespace Shturman.Shedule.Commands
{
    interface ICommand
    {
        bool Do(IShedule shedule);
        void UnDo(IShedule shedule);
    }
}
