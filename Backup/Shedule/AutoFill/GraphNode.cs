// ϳ��������� �������� ��'����
using System.Collections;
using Shturman.Shedule.Objects;

namespace Shturman.Shedule.AutoFill
{
	/// <summary>
	/// ������� �����
	/// </summary>
	public class GraphNode
	{
		public GraphNode(StudyLoading stld)
		{
			this._stld = stld;
		}

		private int hour = 0;

		private int dayIndex = -1;
		private int pairIndex = -1;

		private StudyLoading _stld = null;

		private IList _nodeLinks = new ArrayList();


		public StudyLoading Stld
		{
			get { return this._stld; }
		}

		public int Hour
		{
			get { return hour; }
			set { hour = value; }
		}

		public int DayIndex
		{
			get { return dayIndex; }
			set { dayIndex = value; }
		}

		public int PairIndex
		{
			get { return pairIndex; }
			set { pairIndex = value; }
		}

		public IList NodeLinks
		{
			get { return this._nodeLinks; }
		}
	}
}
