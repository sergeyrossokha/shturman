using System;
using System.Collections;

namespace Shturman.Shedule.AutoFill
{
	/// <summary>
	/// Summary description for Graph.
	/// </summary>
	public class Graph
	{
		private IList _graphNodes = new ArrayList();

		public IList GraphNodes
		{
			get { return _graphNodes; }
		}

		public void SetDoubleLink(GraphNode node1, GraphNode node2)
		{
			SetLink(node1, node2);
			SetLink(node2, node1);
		}

		private void SetLink(GraphNode node1, GraphNode node2)
		{
			node1.NodeLinks.Add( node2 );
		}

		
		public void ColorGraphByPair()
		{

		}

		public int ColorGraphByDay()
		{
			int maxColor = 0;
			(_graphNodes[0] as GraphNode).DayIndex = 1;
			foreach(GraphNode gn in _graphNodes)
			{
				if (gn.DayIndex == -1)
				{
					/* �������� ������ ������������ ������ ������� ������ */
					IList usedColors = new ArrayList();
					foreach(GraphNode ogn in gn.NodeLinks)
					{
						if (ogn.DayIndex != -1)
							usedColors.Add(ogn.DayIndex);
					}

					/* ��������� ������������ ����� */
					int minColor = -1;
					for (int index= 1; index<100; index ++)
						if (!usedColors.Contains(index))
						{
							minColor = index;
							maxColor = Math.Max(maxColor, minColor);
							break;
						}
					/* ������������� ���� ������� */
					gn.DayIndex = minColor;
				}
			}
			return maxColor;
		}
	}
}
