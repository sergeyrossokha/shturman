using System;
using System.Threading;
using System.Windows.Forms;

namespace Shturman.Shedule.Export
{
	/// <summary>
	/// Summary description for ProgressThread.
	/// </summary>
	public class ProgressThread
	{
		private ProgressForm _progressForm;
		private Thread _progressThread;

		public ProgressThread()
			{
				_progressForm = new ProgressForm();
			}
			
		public void Show()
		{
			_progressThread = new Thread(new ThreadStart(ShowThread));
			_progressThread.CurrentCulture = Thread.CurrentThread.CurrentCulture;
			_progressThread.CurrentUICulture = Thread.CurrentThread.CurrentUICulture;
			_progressThread.IsBackground = true;
			_progressThread.ApartmentState = ApartmentState.STA;
			_progressThread.Start();
			while(_progressForm == null) Thread.Sleep(new TimeSpan(50));
		}

		private void ShowThread()
		{
			Application.Run(_progressForm);
		}

		public void Close()
		{
			if(_progressThread == null)
				return;
			if(_progressForm == null)
				return;
			try
			{
				if(_progressForm.InvokeRequired)
					_progressForm.Invoke(new MethodInvoker(_progressForm.Close));
				else
					_progressForm.Close();
			}
			catch
			{
			}
			_progressForm = null;
			_progressForm = null;
		}

		public string Message
		{
			get
			{
				if(_progressForm == null)
					return string.Empty;
				return _progressForm.Text;
			}
		}
	
		public int MaxValue
		{
			get { return _progressForm.MaxPosition; }
			set {_progressForm.MaxPosition = value; }
		}

		public int Position
		{
			get { return _progressForm.Position; }
		}
		
		public void CallBack(string text, int position)
		{
            _progressForm.SetText(text);
            _progressForm.SetPosition(position);
            _progressForm.SetPositionText(((int)(Math.Round((float)position / (float)this.MaxValue * 100))).ToString() + "%");
		}
	}
}
