using System;
using System.IO;
using System.Windows.Forms;
using System.Xml;
using Shturman.Nestor.Interfaces;
using Shturman.Shedule.Server.Interface;
using Shturman.Shedule.Server.Remoting;

namespace Shturman.Shedule.Export
{
//	public delegate 
	/// <summary>
	/// Summary description for XMLExporter.
	/// </summary>
	public class XMLExporter
	{
		private object _shedule;
		IObjectStorage _storage;

		public XMLExporter(object shedule, IObjectStorage storage)
		{
			_shedule = shedule;
			_storage = storage;
		}

		public void ExportSupplement(XmlTextWriter xmlWriter)
		{
			if (_shedule != null)
			{
				#region Export Supplement from Shedule
				if (_shedule is IShedule)
				{
//					using (IShedule shedule = _shedule as IShedule)
//					{
//						new NotImplementedException();
//					}
				}
				#endregion

				#region Export Supplement from Shedule2
				if (_shedule is IShedule2)
				{
//					using (IShedule2 shedule2 = _shedule as IShedule2)
//					{
//						new NotImplementedException();
//					}
				}
				#endregion
			}			
		}

		public void ExportDeptLeading(XmlTextWriter xmlWriter, ExtObjKey deptNumber)
		{
			if (_shedule != null)
			{
				#region Export Supplement from Shedule
				if (_shedule is IShedule)
				{
					IShedule shedule = _shedule as IShedule;
					xmlWriter.WriteStartDocument(true);
					
					xmlWriter.WriteStartElement("LeadingDocument");
					string str = shedule.WriteDeptLeading(deptNumber);
					XmlTextReader xmlReader = new XmlTextReader( new StringReader(str));					
					xmlReader.WhitespaceHandling = WhitespaceHandling.None;
					try
					{
						while (!xmlReader.EOF)					
							xmlWriter.WriteNode(xmlReader, true);
					}
					catch(Exception e)
					{
						MessageBox.Show(e.Message);
					}
					xmlWriter.WriteEndElement();
					
					xmlWriter.WriteEndDocument();				
				}
				#endregion

				#region Export Supplement from Shedule2
				if (_shedule is IShedule2)
				{
					//					using (IShedule2 shedule2 = _shedule as IShedule2)
					//					{
					//						new NotImplementedException();
					//					}
				}
				#endregion
			}			
		}

		public void ExportTutorsRestricts(XmlTextWriter xmlWriter)
		{
			if (_shedule != null)
			{
				#region Export TutorsRestricts from Shedule
				if (_shedule is IShedule)
				{
					IShedule shedule = _shedule as IShedule;
					xmlWriter.WriteStartDocument(true);
					
					xmlWriter.WriteStartElement("TutorsRestrictDocument");
					string str = shedule.WriteTutorRestricts();
					XmlTextReader xmlReader = new XmlTextReader( new StringReader(str));					
					xmlReader.WhitespaceHandling = WhitespaceHandling.None;
					try
					{
						while (!xmlReader.EOF)					
							xmlWriter.WriteNode(xmlReader, true);
					}
					catch(Exception e)
					{
						MessageBox.Show(e.Message);
					}
					xmlWriter.WriteEndElement(); // TutorsRestrictDocument
					
					xmlWriter.WriteEndDocument();						
				}
				#endregion

				#region Export TutorsRestricts from Shedule2
				if (_shedule is IShedule2)
				{
//					using (IShedule2 shedule2 = _shedule as IShedule2)
//					{
//						new NotImplementedException();
//					}
				}
				#endregion
			}		
		}

		public void ExportFlatsRestricts(XmlTextWriter xmlWriter)
		{
			if (_shedule != null)
			{
				#region Export FlatsRestricts from Shedule
				if (_shedule is IShedule)
				{
					IShedule shedule = _shedule as IShedule;
					xmlWriter.WriteStartDocument(true);
					
					xmlWriter.WriteStartElement("FlatsRestrictDocument");
					string str = shedule.WriteFlatRestricts();
					XmlTextReader xmlReader = new XmlTextReader( new StringReader(str));					
					xmlReader.WhitespaceHandling = WhitespaceHandling.None;
					try
					{
						while (!xmlReader.EOF)					
							xmlWriter.WriteNode(xmlReader, true);
					}
					catch(Exception e)
					{
						MessageBox.Show(e.Message);
					}					
					xmlWriter.WriteEndElement(); // FlatsRestrictDocument
                    
					xmlWriter.WriteEndDocument();	
				}
				#endregion

				#region Export FlatsRestricts from Shedule2
				if (_shedule is IShedule2)
				{
//					using (IShedule2 shedule2 = _shedule as IShedule2)
//					{
//						new NotImplementedException();
//					}
				}
				#endregion
			}		

		}

		public void ExportGroupsRestricts(XmlTextWriter xmlWriter)
		{
			if (_shedule != null)
			{
				#region Export FlatsRestricts from Shedule
				if (_shedule is IShedule)
				{
					IShedule shedule = _shedule as IShedule;
					xmlWriter.WriteStartDocument(true);
					
					xmlWriter.WriteStartElement("GrousRestrictDocument");
					string str = shedule.WriteGroupRestricts();
					XmlTextReader xmlReader = new XmlTextReader( new StringReader(str));					
					xmlReader.WhitespaceHandling = WhitespaceHandling.None;
					try
					{
						while (!xmlReader.EOF)					
							xmlWriter.WriteNode(xmlReader, true);
					}
					catch(Exception e)
					{
						MessageBox.Show(e.Message);
					}											
					xmlWriter.WriteEndElement();
                    
					xmlWriter.WriteEndDocument();					
				}
				#endregion

				#region Export FlatsRestricts from Shedule2
				if (_shedule is IShedule2)
				{
//					using (IShedule2 shedule2 = _shedule as IShedule2)
//					{
//						new NotImplementedException();
//					}
				}
				#endregion
			}	
		}
	}
}
