using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

using Shturman.MultyLanguage;
using Shturman.Shedule.Objects;
using Shturman.Shedule.Commands;
using Shturman.Shedule.Server.Interface;
using Shturman.Shedule.Server.Remoting;
using Shturman.Shedule.Objects.Container;

namespace Shturman.Shedule.Checkers
{
    class RestrictChecker
    {
        private RestrictChecker(){}
        public static RestrictChecker instance = new RestrictChecker();

        public bool CheckGroup(IShedule shedule, StudyDay sd, StudyCycles sc, StudyHour sh,
            StudyLoading sl, IList flats)
        {
            // �������� �� ��������� ���������
            int key = shedule.GetKey(int.Parse(sc.MnemoCode), int.Parse(sd.MnemoCode), 
                int.Parse(sh.MnemoCode));

            foreach (Group _group in sl.Groups)
            {
                if (shedule.GroupBusyCheck(key, _group.ObjectHardKey) == GroupBusyCheckerState.isBusy)
                {
                    if (sl.Groups.Count == 1)
                        MessageBox.Show(string.Format(Vocabruary.Translate("Message.GroupBusyText"), RemoteAdapter.Adapter.AdaptLesson(shedule.GetGroupLesson(_group.ObjectHardKey, key)).GroupViewString),
                            Vocabruary.Translate("Message.GroupBusyTitle"), MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    else
                        MessageBox.Show(string.Format(Vocabruary.Translate("Message.GroupsBusyText"), RemoteAdapter.Adapter.AdaptLesson(shedule.GetGroupLesson(_group.ObjectHardKey, key)).GroupViewString),
                            string.Format(Vocabruary.Translate("Message.GroupsBusyTitle"), _group.MnemoCode), MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

                    return false;
                }

                if (shedule.GroupBusyCheck(key, _group.ObjectHardKey) == GroupBusyCheckerState.isRestrict)
                {
                    if (MessageBox.Show(string.Format(Vocabruary.Translate("Message.GroupRestrictText"), shedule.GetGroupRestrictsText(_group.ObjectHardKey, key)),
                        Vocabruary.Translate("Message.GroupBusyTitle"), MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk) == DialogResult.No)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        public bool CheckTutor(IShedule shedule, StudyDay sd, StudyCycles sc, StudyHour sh,
            StudyLoading sl, IList flats)
        {
            // �������� �� ��������� ���������
            int key = shedule.GetKey(int.Parse(sc.MnemoCode), int.Parse(sd.MnemoCode),
                int.Parse(sh.MnemoCode));

            foreach (Tutor _tutor in sl.Tutors)
            {
                if (shedule.TutorBusyCheck(key, _tutor.ObjectHardKey) == TutorBusyCheckerState.isBusy)
                {
                    if (sl.Tutors.Count == 1)
                        MessageBox.Show(string.Format(Vocabruary.Translate("Message.TutorBusyText"), _tutor.ToString(), RemoteAdapter.Adapter.AdaptLesson(shedule.GetTutorLesson(_tutor.ObjectHardKey, key)).GroupViewString),
                            Vocabruary.Translate("Message.TutorBusyTitle"), MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    else
                        MessageBox.Show(string.Format(Vocabruary.Translate("Message.TutorsBusyText"), RemoteAdapter.Adapter.AdaptLesson(shedule.GetTutorLesson(_tutor.ObjectHardKey, key)).GroupViewString),
                            string.Format(Vocabruary.Translate("Message.TutorsBusyTitle"), _tutor.ToString()), MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    return false;
                }

                if (shedule.TutorBusyCheckBoolRestrict(key, _tutor.ObjectHardKey))
                {
                    string restrictText = shedule.GetTutorRestrictsText(_tutor.ObjectHardKey, key);
                    if (MessageBox.Show(_tutor.ToString() + ":" + restrictText + "\n������������?",
                        Vocabruary.Translate("Message.TutorBusyTitle"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                    {
                        return false;
                    }
                }
            }
            return true;
        }

        public bool CheckPhizika(IShedule shedule, StudyDay sd, StudyCycles sc, StudyHour sh,
           StudyLoading sl, IList flats)
        {
            // �������� �� ��������� ���������
            int key = shedule.GetKey(int.Parse(sc.MnemoCode), int.Parse(sd.MnemoCode),
                int.Parse(sh.MnemoCode));

            if (sl.Dept != null && sl.StudyForm != null && sl.SubjectName != null)
                if (sl.Dept.MnemoCode == "505" && sl.StudyForm.MnemoCode.StartsWith("��"))
                {
                    ExtObjKey[] checkedStudyForms = new ExtObjKey[2];
                    foreach (StudyType st in DB4OBridgeRem.theOneObject.ObjectList(typeof(StudyType)))
                    {
                        if (st.MnemoCode.StartsWith("��."))
                            checkedStudyForms[0] = st.ObjectHardKey;
                        if (st.MnemoCode.StartsWith("��1."))
                            checkedStudyForms[1] = st.ObjectHardKey;
                         
                    }
                    if (shedule.CheckDeptLessonCountByPair(key, checkedStudyForms, new ExtObjKey[] { sl.Dept.ObjectHardKey }, 4))
                    {
                        MessageBox.Show(Vocabruary.Translate("Message.FizikaLimit"),
                            Vocabruary.Translate("Message.FizikaLimitTitle"), MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                        return false;
                    }
                }
            return true;
        }
        
    }
}
