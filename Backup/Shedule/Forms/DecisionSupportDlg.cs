/*�����: ������� ����� �������������
 * ����: ����� ������������� ���������� �� ��������� ����� �� �� ��������
 * 
 * ������������ �� ������������ � ������ ������
 * */
using System;
using System.ComponentModel;
using System.Windows.Forms;
using Shturman.MultyLanguage;

namespace Shturman.Shedule.Forms
{
	/// <summary>
	/// Summary description for DecisionSupportDlg.
	/// </summary>
	public class DecisionSupportDlg : Form
	{
		private TreeView treeView1;
		private Label label1;
		private ContextMenu contextMenu1;
		private MenuItem menuItem1;
		private MenuItem menuItem2;
		private Button button1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private Container components = null;

		public DecisionSupportDlg()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.treeView1 = new System.Windows.Forms.TreeView();
			this.contextMenu1 = new System.Windows.Forms.ContextMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.label1 = new System.Windows.Forms.Label();
			this.button1 = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// treeView1
			// 
			this.treeView1.ContextMenu = this.contextMenu1;
			this.treeView1.ImageIndex = -1;
			this.treeView1.Location = new System.Drawing.Point(8, 32);
			this.treeView1.Name = "treeView1";
			this.treeView1.SelectedImageIndex = -1;
			this.treeView1.Size = new System.Drawing.Size(352, 344);
			this.treeView1.TabIndex = 0;
			// 
			// contextMenu1
			// 
			this.contextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						 this.menuItem1,
																						 this.menuItem2});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.Text = "�������� � ����������";
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 1;
			this.menuItem2.Text = "���������";
			// 
			// label1
			// 
			this.label1.AllowDrop = true;
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(352, 23);
			this.label1.TabIndex = 1;
			this.label1.Text = "��������";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// button1
			// 
			this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.button1.Location = new System.Drawing.Point(286, 384);
			this.button1.Name = "button1";
			this.button1.TabIndex = 2;
			this.button1.Text = "�������";
			// 
			// DecisionSupportDlg
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(368, 413);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.treeView1);
			this.Name = "DecisionSupportDlg";
			this.Text = "��������� �������� �������";
			this.Load += new System.EventHandler(this.DecisionSupportDlg_Load);
			this.ResumeLayout(false);

		}
		#endregion

		private void DecisionSupportDlg_Load(object sender, EventArgs e)
		{
			/// ��������� ������
			this.button1.Text = Vocabruary.Translate("Button.Exit");
			/// �������
			this.label1.Text = Vocabruary.Translate("DecisionSupportDlg.ItemsCaption"); 
			/// ��������� ���� 
			this.menuItem1.Text = Vocabruary.Translate("DecisionSupportDlg.ContextMenuInsertToShedule");
			this.menuItem2.Text = Vocabruary.Translate("DecisionSupportDlg.ContextMenuTalkItem");
			/// ��������� �����
			this.Text = Vocabruary.Translate("DecisionSupportDlg.Title");
		}

		public TreeNodeCollection Nodes
		{
			get { return this.treeView1.Nodes; }
		}
	}
}
