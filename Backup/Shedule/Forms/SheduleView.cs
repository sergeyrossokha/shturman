using System;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Shturman.MultyLanguage;
using Shturman.Nestor.Interfaces;
using Shturman.Shedule.Objects;

using GUI=Shturman.Shedule.GUI;
using Shturman.Shedule.Server.Interface;

namespace Shturman.Shedule.Forms
{
	/// <summary>
	/// Summary description for SheduleView.
	/// </summary>
	public class SheduleView : Form
	{
		private Splitter splitter1;
		private Panel panel1;
		private ComboBox comboBox1;
		private Label label3;
		private GUI.SheduleView sheduleView1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private Container components = null;

		private IObjectStorage objectFactory;
		private IShedule shmatr;
		
		private IList weekCycles;
		private IList weekDays;
		private IList dayPairs;
		private TreeView objectTree;
		private System.Windows.Forms.LinkLabel linkLabel1;

		/// <summary>
		/// ��� ����������� ��������
		/// </summary>
		private SheduleViewType viewType = SheduleViewType.byTutor;

		public enum SheduleViewType {byTutor, byGroup, byFlat};

		public SheduleView(IObjectStorage objectFactory, IShedule shedule, SheduleViewType shvwt)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			this.objectFactory = objectFactory;
			this.shmatr = shedule;
			this.viewType = shvwt;
            this.Text = "";
			
			this.weekCycles = objectFactory.ObjectList(typeof(StudyCycles));
			this.weekDays = objectFactory.ObjectList(typeof(StudyDay));
			// ��������� �� ����� ����
			this.weekDays.RemoveAt(this.weekDays.Count - 1);
			this.dayPairs = objectFactory.ObjectList(typeof(StudyHour));

			this.sheduleView1.InitSheduleGrid(weekDays, dayPairs, weekCycles);

			switch(this.viewType)
			{
				case SheduleViewType.byTutor:
				{
					this.BuildDepartmentTutorTree();
					break;
				}
				case SheduleViewType.byGroup:
				{
					this.BuildFacultyGroupTree();
					break;
				}
				case SheduleViewType.byFlat:
				{
					this.BuildBuildingFlatTree();
					break;
				}
			}
		}

		/// <summary>
		/// �������� ������ ������� - ��������
		/// </summary>
		private void BuildDepartmentTutorTree()
		{
			IList departments = objectFactory.ObjectList(typeof(Department));

			foreach (Department dp in departments)
				if (dp != null)
				{
					TreeNode tn = new TreeNode();
					tn.Text = dp.MnemoCode;
					tn.Tag = dp;
                    tn.Nodes.Add(new TreeNode("-1"));

					// ������ ����� �� ������ 
					this.objectTree.Nodes.Add(tn);
				}
		}

		/// <summary>
		/// �������� ������ ��������� - �����
		/// </summary>
		private void BuildFacultyGroupTree()
		{
			IList faculties = objectFactory.ObjectList(typeof(Faculty));

			foreach (Faculty ft in faculties)
				if (ft != null)
				{
					TreeNode tn = new TreeNode();
					tn.Text = ft.MnemoCode;
					tn.Tag = ft;
					tn.Nodes.Add(new TreeNode("-1"));

					// ������ ����� �� ������ 
					this.objectTree.Nodes.Add(tn);
				}
		}

		/// <summary>
		/// �������� ������ ������ - ��������
		/// </summary>
		private void BuildBuildingFlatTree()
		{
			IList buildings = objectFactory.ObjectList(typeof(Building));

			foreach (Building bd in buildings)
				if (bd != null)
				{
					TreeNode tn = new TreeNode();
					tn.Text = bd.MnemoCode;
					tn.Tag = bd;
					tn.Nodes.Add(new TreeNode("-1"));

					// ������ ����� �� ������ 
					this.objectTree.Nodes.Add(tn);
				}
		}


		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(SheduleView));
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.panel1 = new System.Windows.Forms.Panel();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.label3 = new System.Windows.Forms.Label();
			this.sheduleView1 = new GUI.SheduleView();
			this.objectTree = new System.Windows.Forms.TreeView();
			this.linkLabel1 = new System.Windows.Forms.LinkLabel();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// splitter1
			// 
			this.splitter1.Location = new System.Drawing.Point(208, 0);
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(3, 493);
			this.splitter1.TabIndex = 1;
			this.splitter1.TabStop = false;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.linkLabel1);
			this.panel1.Controls.Add(this.comboBox1);
			this.panel1.Controls.Add(this.label3);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel1.Location = new System.Drawing.Point(211, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(493, 32);
			this.panel1.TabIndex = 3;
			// 
			// comboBox1
			// 
			this.comboBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBox1.Items.AddRange(new object[] {
														   "10%",
														   "20%",
														   "30%",
														   "40%",
														   "50%",
														   "60%",
														   "70%",
														   "80%",
														   "90%",
														   "100%"});
			this.comboBox1.Location = new System.Drawing.Point(400, 6);
			this.comboBox1.MaxDropDownItems = 10;
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(88, 21);
			this.comboBox1.TabIndex = 16;
			this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
			// 
			// label3
			// 
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label3.Location = new System.Drawing.Point(344, 9);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(54, 15);
			this.label3.TabIndex = 15;
			this.label3.Text = "������:";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// sheduleView1
			// 
			this.sheduleView1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.sheduleView1.Location = new System.Drawing.Point(211, 32);
			this.sheduleView1.Name = "sheduleView1";
			this.sheduleView1.Size = new System.Drawing.Size(493, 461);
			this.sheduleView1.TabIndex = 4;
			// 
			// objectTree
			// 
			this.objectTree.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.objectTree.Dock = System.Windows.Forms.DockStyle.Left;
			this.objectTree.ImageIndex = -1;
			this.objectTree.Location = new System.Drawing.Point(0, 0);
			this.objectTree.Name = "objectTree";
			this.objectTree.SelectedImageIndex = -1;
			this.objectTree.Size = new System.Drawing.Size(208, 493);
			this.objectTree.TabIndex = 0;
			this.objectTree.MouseDown += new System.Windows.Forms.MouseEventHandler(this.objectTree_MouseDown);
			this.objectTree.DoubleClick += new System.EventHandler(this.objectTree_DoubleClick);
			this.objectTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.objectTree_AfterSelect);
			this.objectTree.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this.objectTree_BeforeExpand);
			// 
			// linkLabel1
			// 
			this.linkLabel1.Location = new System.Drawing.Point(8, 6);
			this.linkLabel1.Name = "linkLabel1";
			this.linkLabel1.Size = new System.Drawing.Size(48, 23);
			this.linkLabel1.TabIndex = 17;
			this.linkLabel1.TabStop = true;
			this.linkLabel1.Text = "-> HTML";
			this.linkLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
			// 
			// SheduleView
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(704, 493);
			this.Controls.Add(this.sheduleView1);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.splitter1);
			this.Controls.Add(this.objectTree);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "SheduleView";
			this.Text = "SheduleView";
			this.Load += new System.EventHandler(this.SheduleView_Load);
			this.panel1.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void SheduleView_Load(object sender, EventArgs e)
		{
			// select a 100% scale for shedule grid
			comboBox1.SelectedIndex = 9;

			this.label3.Text = Vocabruary.Translate("SheduleBrowser.Scale");

			switch(this.viewType)
			{
				case SheduleViewType.byTutor:
				{
					this.Text += Vocabruary.Translate("SheduleView.ByTutor");
					break;
				}
				case SheduleViewType.byGroup:
				{
					this.Text += Vocabruary.Translate("SheduleView.ByGroup");
					break;
				}
				case SheduleViewType.byFlat:
				{
					this.Text += Vocabruary.Translate("SheduleView.ByFlat");
					break;
				}
			}
		}

		private void objectTree_BeforeExpand(object sender, TreeViewCancelEventArgs e)
		{
			if (e.Node.Nodes.Count == 1)
				if (e.Node.Nodes[0] != null )
					if (e.Node.Nodes[0].Text == "-1")
					{
						e.Node.Nodes.Clear();

						switch(this.viewType)
						{
							case SheduleViewType.byTutor:
							{
								if (e.Node.Tag !=null && e.Node.Tag is Department)
									foreach (Tutor tr in (e.Node.Tag as Department).DepartmentTutors)
										if (tr != null)
										{
											TreeNode tn = new TreeNode(tr.ToString());
											tn.Tag = tr;

											e.Node.Nodes.Add(tn);
										}
								break;
							}
							case SheduleViewType.byGroup:
							{
								if (e.Node.Tag !=null && e.Node.Tag is Faculty)
									foreach (Group gr in (e.Node.Tag as Faculty).Groups)
										if (gr != null)
										{
											TreeNode tn = new TreeNode(gr.ToString());
											tn.Tag = gr;

											e.Node.Nodes.Add(tn);
										}
								break;
							}
							case SheduleViewType.byFlat:
							{
								if (e.Node.Tag !=null && e.Node.Tag is Building)
									foreach (LectureHall lh in (e.Node.Tag as Building).LectureHalls)
										if (lh != null)
										{
											TreeNode tn = new TreeNode(lh.ToString());
											tn.Tag = lh;

											e.Node.Nodes.Add(tn);
										}
								break;
							}
						}
					}

		}

		private void objectTree_AfterSelect(object sender, TreeViewEventArgs e)
		{
			if(e.Node.Nodes.Count == 0)
			{
				IList _objectList = new ArrayList();
				_objectList.Add(e.Node.Tag);
				this.sheduleView1.ShowShedule( _objectList, this.shmatr );
			}
		}

		private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.comboBox1.SelectedIndex != -1)
				this.sheduleView1.Scale((this.comboBox1.SelectedIndex+1) * 10);
		}

		private void objectTree_DoubleClick(object sender, EventArgs e)
		{			
			if (objectTree.SelectedNode != null)
				if (objectTree.SelectedNode.Nodes.Count != 0)
				{
					objectTree_BeforeExpand(sender, new TreeViewCancelEventArgs(objectTree.SelectedNode, false, TreeViewAction.Expand));
				
					IList _objectList = new ArrayList();
					foreach(TreeNode tn in objectTree.SelectedNode.Nodes)
						_objectList.Add(tn.Tag);
					this.sheduleView1.ShowShedule( _objectList, this.shmatr );
				}
				else
				{
					IList _objectList = new ArrayList();
					_objectList.Add(objectTree.SelectedNode.Tag);
					this.sheduleView1.ShowShedule( _objectList, this.shmatr );
				}
		}

		private void objectTree_MouseDown(object sender, MouseEventArgs e)
		{			
		}

		private void linkLabel1_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
		{
			this.sheduleView1.ToHTML();
		}
	}
}
