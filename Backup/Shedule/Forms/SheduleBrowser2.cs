using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using Shturman.MultyLanguage;
using System.Configuration;
using Shturman.Nestor.DataEditors;
using Shturman.Nestor.DataLists;
using Shturman.Nestor.Interfaces;
using Shturman.Shedule.Objects;
using Shturman.Shedule.Commands;
using Shturman.Shedule.Checkers;

using GUI = Shturman.Shedule.GUI;
using Shturman.Shedule;
using Shturman.Shedule.Server.Interface;
using Shturman.Shedule.Server.Remoting;

using Shturman.Sherlock.XLogic;
using System.Xml.XPath;
using Shturman.Shedule.GUI;

namespace Shturman.Shedule.Forms
{
	/// <summary>
	/// Summary description for SheduleView.
	/// </summary>
	public class SheduleBrowser2 : Form
	{
		private IContainer components;
		
		private TabPage tabPage1;
		private TabControl tabControl1;
		private DataGrid leadingGrid;
		private ImageList imageList1;

		private IObjectStorage objectFactory;
		private IShedule2 shmatr;
		private IList groupList;
		
		private IList buildingList;
		private IList weekDays;
		private IList dayPairs;

		private IList leadingList;
		private ListFiltrator lsfiltr = new ListFiltrator();
		private ArrayList DetailTypes = new ArrayList();		
		private ArrayList DetailPropList  = new ArrayList();
		private ListBox listBox2;
		private Panel panel2;
		private Panel panel3;
		private Panel panel1;
		private Panel panel4;
		private Label label1;
		private Label label2;
		private Label label3;
		private ComboBox comboBox1;
		private LinkLabel linkLabel1;
		private LinkLabel linkLabel2;
		private LinkLabel linkLabel3;
		private LinkLabel linkLabel4;
		private LinkLabel linkLabel5;
		private LinkLabel linkLabel6;
		private MenuItem menuItem1;
		private MenuItem menuItem2;
		private MenuItem menuItem3;
		private MenuItem menuItem4;
		private MenuItem menuItem5;
		private MenuItem menuItem6;
		private ContextMenu SupportMenu;
		private Panel panelNav;
		private Panel panelBigShedule;
		private MenuItem menuItem7;
		private MenuItem menuItem8;
		private ContextMenu contextEditMenu;
		private LinkLabel linkLabel7;
		private System.Windows.Forms.ContextMenu contextMenuDelLoading;
		private System.Windows.Forms.MenuItem menuDelLessons;
		private System.Windows.Forms.MenuItem menuItemDelLoading;
		private Shedule.GUI.GroupSheduleView2 groupSheduleView2;
		private Shedule.GUI.RestrictSheduleView2 SheduleNav;
		private System.Windows.Forms.ComboBox comboBoxWeeks;
		private System.Windows.Forms.Label comboBoxWeeksLabel;
		private System.Windows.Forms.Panel panel5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Panel panel6;
		private System.Windows.Forms.Label label5;
		private Shedule.GUI.RestrictSheduleView2 tutorRestrictView;
//		private ToolTip toolTip1;

		private ListBox subObjectList;

		/// <summary>
		/// 
		/// </summary>
		/// <param name="objectFactory">̳�� ��� ������� �� ������� �����</param>
		/// <param name="shedule"></param>
		/// <param name="groupList"></param>
		public SheduleBrowser2(IObjectStorage objectFactory, IShedule2 shedule, IList groupList)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			this.objectFactory = objectFactory;
			this.shmatr = shedule;
			this.groupList = groupList;
			
			buildingList = objectFactory.ObjectList(typeof(Building));			
			this.weekDays = objectFactory.ObjectList(typeof(StudyDay));
			this.dayPairs = objectFactory.ObjectList(typeof(StudyHour));

			this.groupSheduleView2.LoadShedule(weekDays, dayPairs);
			
			this.comboBoxWeeks.DataSource = SheduleKeyBuilder.GetAllWeekDatesList(shmatr);

			IList uidList = new ArrayList();
			foreach(Group gr in groupList)
			{ 
				ExtObjKey gr_key = new ExtObjKey();
				gr_key.UID = objectFactory.GetUUID(gr, out gr_key.Signature, out gr_key.Index);
				uidList.Add( gr_key );
			}
			ExtObjKey[] uidListArray = new ExtObjKey[uidList.Count];
			uidList.CopyTo(uidListArray, 0);

			IList filteredStudyLeading =  RemoteAdapter.Adapter.AdaptLeadingList( shmatr.GetStudyLeadingByGroups( uidListArray ) );

			this.leadingList = new ListWrapper(typeof(StudyLoading), filteredStudyLeading);
			this.leadingGrid.DataSource = leadingList;
			this.leadingGrid.TableStyles.Clear();
			this.leadingGrid.TableStyles.Add( CreateTableStyle() );
			
			DetailTypes.Clear();
			DetailPropList.Clear();		
		}

		
		/// <summary>
		/// ������� ��� 
		/// </summary>
		public void RefreshStudyLeading()
		{
			IList uidList = new ArrayList();
			foreach(Group gr in groupList)
			{ 
				ExtObjKey gr_key = new ExtObjKey();
				gr_key.UID = objectFactory.GetUUID(gr, out gr_key.Signature, out gr_key.Index);
				uidList.Add( gr_key );
			}
			ExtObjKey[] uidListArray = new ExtObjKey[uidList.Count];
			uidList.CopyTo(uidListArray, 0);

			IList filteredStudyLeading =  RemoteAdapter.Adapter.AdaptLeadingList( shmatr.GetStudyLeadingByGroups( uidListArray ) );

			this.leadingList = new ListWrapper(typeof(StudyLoading), filteredStudyLeading);
			this.leadingGrid.DataSource = leadingList;

		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="e"></param>
		protected override void OnResize(EventArgs e)
		{
			base.OnResize (e);
			if (weekDays != null && dayPairs!=null)
			{
				this.SheduleNav.CellWidth = 18;
				this.SheduleNav.CellHeight = (this.SheduleNav.Height / 13);
				this.SheduleNav.LoadShedule(weekDays, dayPairs);
			}
		}

		
		/// <summary>
		/// �������� ����� ��� ����������� ���������� � ��������
		/// </summary>
		/// <returns></returns>
		DataGridTableStyle CreateTableStyle() 
		{ 
			DataGridColorEventHandler colorEvent = new DataGridColorEventHandler(DataGridEx_CellPaint);

			DataGridColoredTextBoxColumn coloredColumn1 = new DataGridColoredTextBoxColumn(); 
			coloredColumn1.MappingName = "���������"; 
			coloredColumn1.HeaderText = "���������";
			coloredColumn1.CellPaint += colorEvent;
 
			DataGridColoredTextBoxColumn coloredColumn2 = new DataGridColoredTextBoxColumn(); 
			coloredColumn2.MappingName = "��� �������"; 
			coloredColumn2.HeaderText = "��� �������";
			coloredColumn2.CellPaint += colorEvent;

			DataGridColoredTextBoxColumn coloredColumn3 = new DataGridColoredTextBoxColumn(); 
			coloredColumn3.MappingName = "�������"; 
			coloredColumn3.HeaderText = "�������";
			coloredColumn3.CellPaint += colorEvent;

			DataGridColoredTextBoxColumn coloredColumn4 = new DataGridColoredTextBoxColumn(); 
			coloredColumn4.MappingName = "���./���.";
			coloredColumn4.HeaderText = "���./���.";
			coloredColumn4.CellPaint += colorEvent;

			DataGridColoredTextBoxColumn coloredColumn5 = new DataGridColoredTextBoxColumn();  
			coloredColumn5.MappingName = "�����������"; 
			coloredColumn5.HeaderText = "�����������";
			coloredColumn5.CellPaint += colorEvent;

			DataGridColoredTextBoxColumn coloredColumn6 = new DataGridColoredTextBoxColumn(); 
			coloredColumn6.MappingName = "�����"; 
			coloredColumn6.HeaderText = "�����";
			coloredColumn6.CellPaint += colorEvent;
			
			DataGridTableStyle tableStyle = new DataGridTableStyle(); 
			tableStyle.MappingName = leadingGrid.DataSource.GetType().Name;
			tableStyle.GridColumnStyles.AddRange (new DataGridColumnStyle[]{coloredColumn1, coloredColumn2, coloredColumn3, coloredColumn4, coloredColumn5, coloredColumn6}); 
			
			return tableStyle; 
		} 


		/// <summary>
		/// ����� ��������� 
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		public void DataGridEx_CellPaint(object sender, DataGridColorlEventArgs e)
		{
			if ((leadingList[ e.Row ] as StudyLoading).UsedHourByWeek >= (leadingList[ e.Row ] as StudyLoading).HourByWeek)
			{
				if (leadingGrid.CurrentRowIndex == e.Row)
				{
					e.BackBrush =  Brushes.DarkBlue;
					e.ForeBrush = Brushes.White;
				}
				else
				{
					e.BackBrush =  Brushes.LightBlue;
					e.ForeBrush = Brushes.Black;
				}
			}
			else
			{
				if (leadingGrid.CurrentRowIndex == e.Row)
				{
					e.BackBrush =  Brushes.DarkBlue;
					e.ForeBrush = Brushes.White;
				}
				else
				{
					e.BackBrush = Brushes.GreenYellow;
					e.ForeBrush = Brushes.Black;
				}
			}
		}
		
		
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		
		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(SheduleBrowser2));
			this.leadingGrid = new System.Windows.Forms.DataGrid();
			this.SupportMenu = new System.Windows.Forms.ContextMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			this.menuItem4 = new System.Windows.Forms.MenuItem();
			this.menuItem5 = new System.Windows.Forms.MenuItem();
			this.menuItem6 = new System.Windows.Forms.MenuItem();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.panelBigShedule = new System.Windows.Forms.Panel();
			this.groupSheduleView2 = new Shedule.GUI.GroupSheduleView2();
			this.panelNav = new System.Windows.Forms.Panel();
			this.panel6 = new System.Windows.Forms.Panel();
			this.tutorRestrictView = new Shedule.GUI.RestrictSheduleView2();
			this.label5 = new System.Windows.Forms.Label();
			this.panel5 = new System.Windows.Forms.Panel();
			this.SheduleNav = new Shedule.GUI.RestrictSheduleView2();
			this.label4 = new System.Windows.Forms.Label();
			this.subObjectList = new System.Windows.Forms.ListBox();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.listBox2 = new System.Windows.Forms.ListBox();
			this.panel2 = new System.Windows.Forms.Panel();
			this.panel4 = new System.Windows.Forms.Panel();
			this.linkLabel7 = new System.Windows.Forms.LinkLabel();
			this.linkLabel3 = new System.Windows.Forms.LinkLabel();
			this.contextMenuDelLoading = new System.Windows.Forms.ContextMenu();
			this.menuDelLessons = new System.Windows.Forms.MenuItem();
			this.menuItemDelLoading = new System.Windows.Forms.MenuItem();
			this.linkLabel2 = new System.Windows.Forms.LinkLabel();
			this.linkLabel1 = new System.Windows.Forms.LinkLabel();
			this.panel1 = new System.Windows.Forms.Panel();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.contextEditMenu = new System.Windows.Forms.ContextMenu();
			this.menuItem7 = new System.Windows.Forms.MenuItem();
			this.menuItem8 = new System.Windows.Forms.MenuItem();
			this.panel3 = new System.Windows.Forms.Panel();
			this.label3 = new System.Windows.Forms.Label();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.linkLabel4 = new System.Windows.Forms.LinkLabel();
			this.linkLabel5 = new System.Windows.Forms.LinkLabel();
			this.linkLabel6 = new System.Windows.Forms.LinkLabel();
			this.comboBoxWeeks = new System.Windows.Forms.ComboBox();
			this.comboBoxWeeksLabel = new System.Windows.Forms.Label();
			((System.ComponentModel.ISupportInitialize)(this.leadingGrid)).BeginInit();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.panelBigShedule.SuspendLayout();
			this.panelNav.SuspendLayout();
			this.panel6.SuspendLayout();
			this.panel5.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panel4.SuspendLayout();
			this.panel1.SuspendLayout();
			this.panel3.SuspendLayout();
			this.SuspendLayout();
			// 
			// leadingGrid
			// 
			this.leadingGrid.AllowDrop = true;
			this.leadingGrid.AllowNavigation = false;
			this.leadingGrid.BackgroundColor = System.Drawing.Color.WhiteSmoke;
			this.leadingGrid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.leadingGrid.CaptionBackColor = System.Drawing.SystemColors.Control;
			this.leadingGrid.CaptionForeColor = System.Drawing.SystemColors.ControlText;
			this.leadingGrid.CaptionText = "��������";
			this.leadingGrid.ContextMenu = this.SupportMenu;
			this.leadingGrid.DataMember = "";
			this.leadingGrid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.leadingGrid.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.leadingGrid.Location = new System.Drawing.Point(0, 0);
			this.leadingGrid.Name = "leadingGrid";
			this.leadingGrid.Size = new System.Drawing.Size(712, 224);
			this.leadingGrid.TabIndex = 0;
			this.leadingGrid.KeyDown += new System.Windows.Forms.KeyEventHandler(this.leadingGrid_KeyDown);
			this.leadingGrid.BindingContextChanged += new System.EventHandler(this.leadingGrid_BindingContextChanged);
			this.leadingGrid.CurrentCellChanged += new System.EventHandler(this.leadingGrid_CurrentCellChanged);
			this.leadingGrid.Leave += new System.EventHandler(this.leadingGrid_Leave);
			this.leadingGrid.MouseMove += new System.Windows.Forms.MouseEventHandler(this.leadingGrid_MouseMove);
			this.leadingGrid.Enter += new System.EventHandler(this.leadingGrid_Enter);
			// 
			// SupportMenu
			// 
			this.SupportMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																						this.menuItem1,
																						this.menuItem4});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem2,
																					  this.menuItem3});
			this.menuItem1.Text = "���������";
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 0;
			this.menuItem2.Text = "����� -> ���������";
			this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
			// 
			// menuItem3
			// 
			this.menuItem3.Index = 1;
			this.menuItem3.Text = "��������� -> �����";
			// 
			// menuItem4
			// 
			this.menuItem4.Index = 1;
			this.menuItem4.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem5,
																					  this.menuItem6});
			this.menuItem4.Text = "������������";
			// 
			// menuItem5
			// 
			this.menuItem5.Index = 0;
			this.menuItem5.Text = "�� �������";
			// 
			// menuItem6
			// 
			this.menuItem6.Index = 1;
			this.menuItem6.Text = "�� ���������";
			// 
			// tabControl1
			// 
			this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl1.ItemSize = new System.Drawing.Size(58, 21);
			this.tabControl1.Location = new System.Drawing.Point(0, 0);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(864, 261);
			this.tabControl1.TabIndex = 0;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.panelBigShedule);
			this.tabPage1.Controls.Add(this.panelNav);
			this.tabPage1.Location = new System.Drawing.Point(4, 25);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Size = new System.Drawing.Size(856, 232);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "���������� �� �������";
			// 
			// panelBigShedule
			// 
			this.panelBigShedule.Controls.Add(this.groupSheduleView2);
			this.panelBigShedule.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelBigShedule.Location = new System.Drawing.Point(0, 0);
			this.panelBigShedule.Name = "panelBigShedule";
			this.panelBigShedule.Size = new System.Drawing.Size(708, 232);
			this.panelBigShedule.TabIndex = 2;
			// 
			// groupSheduleView2
			// 
			this.groupSheduleView2.AllowDrop = true;
			this.groupSheduleView2.BackColor = System.Drawing.SystemColors.Control;
			this.groupSheduleView2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupSheduleView2.Location = new System.Drawing.Point(0, 0);
			this.groupSheduleView2.Name = "groupSheduleView2";
			this.groupSheduleView2.Size = new System.Drawing.Size(708, 232);
			this.groupSheduleView2.TabIndex = 1;
			this.groupSheduleView2.DragEnter += new System.Windows.Forms.DragEventHandler(this.groupSheduleView1_DragEnter);
			this.groupSheduleView2.DragDrop += new System.Windows.Forms.DragEventHandler(this.groupSheduleView1_DragDrop);
			// 
			// panelNav
			// 
			this.panelNav.Controls.Add(this.panel6);
			this.panelNav.Controls.Add(this.panel5);
			this.panelNav.Controls.Add(this.label4);
			this.panelNav.Dock = System.Windows.Forms.DockStyle.Right;
			this.panelNav.Location = new System.Drawing.Point(708, 0);
			this.panelNav.Name = "panelNav";
			this.panelNav.Size = new System.Drawing.Size(148, 232);
			this.panelNav.TabIndex = 1;
			// 
			// panel6
			// 
			this.panel6.Controls.Add(this.tutorRestrictView);
			this.panel6.Controls.Add(this.label5);
			this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel6.Location = new System.Drawing.Point(0, 192);
			this.panel6.Name = "panel6";
			this.panel6.Size = new System.Drawing.Size(148, 40);
			this.panel6.TabIndex = 4;
			// 
			// tutorRestrictView
			// 
			this.tutorRestrictView.CellHeight = 16;
			this.tutorRestrictView.CellWidth = 16;
			this.tutorRestrictView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tutorRestrictView.Location = new System.Drawing.Point(0, 16);
			this.tutorRestrictView.Name = "tutorRestrictView";
			this.tutorRestrictView.Size = new System.Drawing.Size(148, 24);
			this.tutorRestrictView.TabIndex = 2;
			this.tutorRestrictView.Load += new System.EventHandler(this.tutorRestrictView_Load);
			// 
			// label5
			// 
			this.label5.Dock = System.Windows.Forms.DockStyle.Top;
			this.label5.Location = new System.Drawing.Point(0, 0);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(148, 16);
			this.label5.TabIndex = 0;
			this.label5.Text = "��������� ���������(��)";
			// 
			// panel5
			// 
			this.panel5.Controls.Add(this.SheduleNav);
			this.panel5.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel5.Location = new System.Drawing.Point(0, 16);
			this.panel5.Name = "panel5";
			this.panel5.Size = new System.Drawing.Size(148, 176);
			this.panel5.TabIndex = 3;
			// 
			// SheduleNav
			// 
			this.SheduleNav.CellHeight = 16;
			this.SheduleNav.CellWidth = 16;
			this.SheduleNav.Dock = System.Windows.Forms.DockStyle.Fill;
			this.SheduleNav.Location = new System.Drawing.Point(0, 0);
			this.SheduleNav.Name = "SheduleNav";
			this.SheduleNav.Size = new System.Drawing.Size(148, 176);
			this.SheduleNav.TabIndex = 1;
			this.SheduleNav.MouseUp += new System.Windows.Forms.MouseEventHandler(this.SheduleNav_MouseUp);
			// 
			// label4
			// 
			this.label4.Dock = System.Windows.Forms.DockStyle.Top;
			this.label4.Location = new System.Drawing.Point(0, 0);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(148, 16);
			this.label4.TabIndex = 2;
			this.label4.Text = "��������� ����(�)";
			// 
			// subObjectList
			// 
			this.subObjectList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.subObjectList.BackColor = System.Drawing.Color.WhiteSmoke;
			this.subObjectList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.subObjectList.IntegralHeight = false;
			this.subObjectList.Location = new System.Drawing.Point(8, 17);
			this.subObjectList.Name = "subObjectList";
			this.subObjectList.Size = new System.Drawing.Size(136, 92);
			this.subObjectList.TabIndex = 1;
			// 
			// imageList1
			// 
			this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth16Bit;
			this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// listBox2
			// 
			this.listBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.listBox2.BackColor = System.Drawing.Color.WhiteSmoke;
			this.listBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.listBox2.IntegralHeight = false;
			this.listBox2.Location = new System.Drawing.Point(8, 128);
			this.listBox2.Name = "listBox2";
			this.listBox2.Size = new System.Drawing.Size(136, 92);
			this.listBox2.TabIndex = 11;
			this.listBox2.Leave += new System.EventHandler(this.listBox2_Leave);
			this.listBox2.Enter += new System.EventHandler(this.listBox2_Enter);
			this.listBox2.SelectedIndexChanged += new System.EventHandler(this.listBox2_SelectedIndexChanged);
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.panel4);
			this.panel2.Controls.Add(this.panel1);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel2.Location = new System.Drawing.Point(0, 261);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(864, 224);
			this.panel2.TabIndex = 11;
			// 
			// panel4
			// 
			this.panel4.Controls.Add(this.linkLabel7);
			this.panel4.Controls.Add(this.linkLabel3);
			this.panel4.Controls.Add(this.linkLabel2);
			this.panel4.Controls.Add(this.linkLabel1);
			this.panel4.Controls.Add(this.leadingGrid);
			this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel4.Location = new System.Drawing.Point(0, 0);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(712, 224);
			this.panel4.TabIndex = 13;
			// 
			// linkLabel7
			// 
			this.linkLabel7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.linkLabel7.BackColor = System.Drawing.SystemColors.Control;
			this.linkLabel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.linkLabel7.ForeColor = System.Drawing.SystemColors.ControlText;
			this.linkLabel7.LinkArea = new System.Windows.Forms.LinkArea(0, 15);
			this.linkLabel7.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
			this.linkLabel7.LinkColor = System.Drawing.SystemColors.ControlText;
			this.linkLabel7.Location = new System.Drawing.Point(630, 4);
			this.linkLabel7.Name = "linkLabel7";
			this.linkLabel7.Size = new System.Drawing.Size(75, 14);
			this.linkLabel7.TabIndex = 4;
			this.linkLabel7.TabStop = true;
			this.linkLabel7.Text = "��������";
			this.linkLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.linkLabel7.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel7_LinkClicked);
			// 
			// linkLabel3
			// 
			this.linkLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.linkLabel3.BackColor = System.Drawing.SystemColors.Control;
			this.linkLabel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.linkLabel3.ContextMenu = this.contextMenuDelLoading;
			this.linkLabel3.LinkArea = new System.Windows.Forms.LinkArea(0, 15);
			this.linkLabel3.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
			this.linkLabel3.LinkColor = System.Drawing.SystemColors.ControlText;
			this.linkLabel3.Location = new System.Drawing.Point(552, 4);
			this.linkLabel3.Name = "linkLabel3";
			this.linkLabel3.Size = new System.Drawing.Size(75, 14);
			this.linkLabel3.TabIndex = 3;
			this.linkLabel3.TabStop = true;
			this.linkLabel3.Text = "�������";
			this.linkLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.linkLabel3.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel3_LinkClicked);
			// 
			// contextMenuDelLoading
			// 
			this.contextMenuDelLoading.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																								  this.menuDelLessons,
																								  this.menuItemDelLoading});
			// 
			// menuDelLessons
			// 
			this.menuDelLessons.Index = 0;
			this.menuDelLessons.Text = "�������� �������";
			this.menuDelLessons.Click += new System.EventHandler(this.menuDelLessons_Click);
			// 
			// menuItemDelLoading
			// 
			this.menuItemDelLoading.Index = 1;
			this.menuItemDelLoading.Text = "�������� ������������";
			this.menuItemDelLoading.Click += new System.EventHandler(this.menuItemDelLoading_Click);
			// 
			// linkLabel2
			// 
			this.linkLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.linkLabel2.BackColor = System.Drawing.SystemColors.Control;
			this.linkLabel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.linkLabel2.LinkArea = new System.Windows.Forms.LinkArea(0, 15);
			this.linkLabel2.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
			this.linkLabel2.LinkColor = System.Drawing.SystemColors.ControlText;
			this.linkLabel2.Location = new System.Drawing.Point(462, 4);
			this.linkLabel2.Name = "linkLabel2";
			this.linkLabel2.Size = new System.Drawing.Size(88, 14);
			this.linkLabel2.TabIndex = 2;
			this.linkLabel2.TabStop = true;
			this.linkLabel2.Text = "�������������";
			this.linkLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
			// 
			// linkLabel1
			// 
			this.linkLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.linkLabel1.BackColor = System.Drawing.SystemColors.Control;
			this.linkLabel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.linkLabel1.LinkArea = new System.Windows.Forms.LinkArea(0, 15);
			this.linkLabel1.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
			this.linkLabel1.LinkColor = System.Drawing.SystemColors.ControlText;
			this.linkLabel1.Location = new System.Drawing.Point(384, 4);
			this.linkLabel1.Name = "linkLabel1";
			this.linkLabel1.Size = new System.Drawing.Size(75, 14);
			this.linkLabel1.TabIndex = 1;
			this.linkLabel1.TabStop = true;
			this.linkLabel1.Text = "��������";
			this.linkLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.label2);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Controls.Add(this.subObjectList);
			this.panel1.Controls.Add(this.listBox2);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
			this.panel1.Location = new System.Drawing.Point(712, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(152, 224);
			this.panel1.TabIndex = 12;
			// 
			// label2
			// 
			this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label2.Location = new System.Drawing.Point(8, 114);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(93, 12);
			this.label2.TabIndex = 13;
			this.label2.Text = "�������������";
			// 
			// label1
			// 
			this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.label1.Location = new System.Drawing.Point(8, 1);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(60, 15);
			this.label1.TabIndex = 12;
			this.label1.Text = "������";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// contextEditMenu
			// 
			this.contextEditMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																							this.menuItem7,
																							this.menuItem8});
			// 
			// menuItem7
			// 
			this.menuItem7.Index = 0;
			this.menuItem7.Text = "�������� ���������";
			this.menuItem7.Click += new System.EventHandler(this.menuItem7_Click);
			// 
			// menuItem8
			// 
			this.menuItem8.Index = 1;
			this.menuItem8.Text = "���������";
			this.menuItem8.Click += new System.EventHandler(this.menuItem8_Click);
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.tabControl1);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel3.Location = new System.Drawing.Point(0, 0);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(864, 261);
			this.panel3.TabIndex = 12;
			// 
			// label3
			// 
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.label3.Location = new System.Drawing.Point(716, 4);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(54, 15);
			this.label3.TabIndex = 13;
			this.label3.Text = "������:";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// comboBox1
			// 
			this.comboBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.comboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBox1.Items.AddRange(new object[] {
														   "10%",
														   "20%",
														   "30%",
														   "40%",
														   "50%",
														   "60%",
														   "70%",
														   "80%",
														   "90%",
														   "100%",
														   "110%",
														   "120%",
														   "130%",
														   "140%",
														   "150%"});
			this.comboBox1.Location = new System.Drawing.Point(772, 1);
			this.comboBox1.MaxDropDownItems = 10;
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(88, 21);
			this.comboBox1.TabIndex = 14;
			this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
			// 
			// linkLabel4
			// 
			this.linkLabel4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.linkLabel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.linkLabel4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((System.Byte)(204)));
			this.linkLabel4.LinkArea = new System.Windows.Forms.LinkArea(0, 15);
			this.linkLabel4.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
			this.linkLabel4.LinkColor = System.Drawing.SystemColors.ControlText;
			this.linkLabel4.Location = new System.Drawing.Point(463, 5);
			this.linkLabel4.Name = "linkLabel4";
			this.linkLabel4.Size = new System.Drawing.Size(75, 14);
			this.linkLabel4.TabIndex = 15;
			this.linkLabel4.TabStop = true;
			this.linkLabel4.Text = "��������";
			this.linkLabel4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.linkLabel4.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel4_LinkClicked);
			// 
			// linkLabel5
			// 
			this.linkLabel5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.linkLabel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.linkLabel5.ContextMenu = this.contextEditMenu;
			this.linkLabel5.LinkArea = new System.Windows.Forms.LinkArea(0, 15);
			this.linkLabel5.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
			this.linkLabel5.LinkColor = System.Drawing.SystemColors.ControlText;
			this.linkLabel5.Location = new System.Drawing.Point(540, 5);
			this.linkLabel5.Name = "linkLabel5";
			this.linkLabel5.Size = new System.Drawing.Size(88, 14);
			this.linkLabel5.TabIndex = 16;
			this.linkLabel5.TabStop = true;
			this.linkLabel5.Text = "�������������";
			this.linkLabel5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.linkLabel5.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel5_LinkClicked);
			// 
			// linkLabel6
			// 
			this.linkLabel6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.linkLabel6.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.linkLabel6.LinkArea = new System.Windows.Forms.LinkArea(0, 15);
			this.linkLabel6.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
			this.linkLabel6.LinkColor = System.Drawing.SystemColors.ControlText;
			this.linkLabel6.Location = new System.Drawing.Point(630, 5);
			this.linkLabel6.Name = "linkLabel6";
			this.linkLabel6.Size = new System.Drawing.Size(75, 14);
			this.linkLabel6.TabIndex = 17;
			this.linkLabel6.TabStop = true;
			this.linkLabel6.Text = "�������";
			this.linkLabel6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			this.linkLabel6.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel6_LinkClicked);
			// 
			// comboBoxWeeks
			// 
			this.comboBoxWeeks.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxWeeks.Location = new System.Drawing.Point(184, 1);
			this.comboBoxWeeks.MaxDropDownItems = 10;
			this.comboBoxWeeks.Name = "comboBoxWeeks";
			this.comboBoxWeeks.Size = new System.Drawing.Size(192, 21);
			this.comboBoxWeeks.TabIndex = 18;
			this.comboBoxWeeks.SelectedIndexChanged += new System.EventHandler(this.comboBoxWeeks_SelectedIndexChanged);
			// 
			// comboBoxWeeksLabel
			// 
			this.comboBoxWeeksLabel.Location = new System.Drawing.Point(140, 1);
			this.comboBoxWeeksLabel.Name = "comboBoxWeeksLabel";
			this.comboBoxWeeksLabel.Size = new System.Drawing.Size(40, 21);
			this.comboBoxWeeksLabel.TabIndex = 19;
			this.comboBoxWeeksLabel.Text = "�����";
			this.comboBoxWeeksLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// SheduleBrowser2
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(864, 485);
			this.Controls.Add(this.comboBoxWeeksLabel);
			this.Controls.Add(this.comboBoxWeeks);
			this.Controls.Add(this.linkLabel6);
			this.Controls.Add(this.linkLabel5);
			this.Controls.Add(this.linkLabel4);
			this.Controls.Add(this.comboBox1);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.panel3);
			this.Controls.Add(this.panel2);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "SheduleBrowser2";
			this.Text = "���������� �������";
			this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
			this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SheduleView_KeyDown);
			this.Load += new System.EventHandler(this.SheduleView_Load);
			((System.ComponentModel.ISupportInitialize)(this.leadingGrid)).EndInit();
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.panelBigShedule.ResumeLayout(false);
			this.panelNav.ResumeLayout(false);
			this.panel6.ResumeLayout(false);
			this.panel5.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.panel4.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void SheduleView_Load(object sender, EventArgs e)
		{
			leadingGrid_BindingContextChanged(leadingGrid, e);

			// select a 100% scale for shedule grid
			comboBox1.SelectedIndex = 9;

			this.SheduleNav.CellWidth = 18;
			this.SheduleNav.CellHeight = (this.SheduleNav.Height / 13);
			this.SheduleNav.LoadShedule(weekDays, dayPairs);

			this.tutorRestrictView.CellWidth = 18;
			this.tutorRestrictView.CellHeight = (this.tutorRestrictView.Height / 13);
			this.tutorRestrictView.LoadShedule(weekDays, dayPairs);

			// show shedule by existing in lesson "refference list groups" group
			leadingGrid_CurrentCellChanged(leadingGrid, e);

			// ��������� ���������� �� ������� �����
			this.Text = Vocabruary.Translate("SheduleBrowser.Title");
			this.leadingGrid.CaptionText = Vocabruary.Translate("SheduleBrowser.StudyLeading");
			this.tabPage1.Text = Vocabruary.Translate("SheduleBrowser.SheduleByGroup");
			this.label2.Text = Vocabruary.Translate("SheduleBrowser.TutorTitle");
			this.label1.Text = Vocabruary.Translate("SheduleBrowser.GroupTitle");
			this.label3.Text = Vocabruary.Translate("SheduleBrowser.Scale");
			
			this.linkLabel1.Text = this.linkLabel4.Text = Vocabruary.Translate("Button.Add");
			this.linkLabel2.Text = this.linkLabel5.Text = Vocabruary.Translate("Button.Edit");
			this.linkLabel3.Text = this.linkLabel6.Text = Vocabruary.Translate("Button.Drop");
			this.linkLabel7.Text = Vocabruary.Translate("Button.Update");

			this.menuItem1.Text = Vocabruary.Translate("SheduleBrowser.ChooseMenu");
			this.menuItem2.Text = this.menuItem5.Text = Vocabruary.Translate("SheduleBrowser.ByTimeMenu");
			this.menuItem3.Text = this.menuItem6.Text = Vocabruary.Translate("SheduleBrowser.ByFlatMenu");
			this.menuItem4.Text = Vocabruary.Translate("SheduleBrowser.ConsultMenu");

			this.menuItem7.Text = Vocabruary.Translate("SheduleBrowser.ChangeFlatMenu");
			this.menuItem8.Text = Vocabruary.Translate("SheduleBrowser.MoveLessonMenu");

			this.menuDelLessons.Text = Vocabruary.Translate("SheduleBrowser.DelLessons");
			this.menuItemDelLoading.Text = Vocabruary.Translate("SheduleBrowser.DelLoading");
		}

		private int _oldRow = -1; // ����� ������ ������� ���� �������� �����
		private object[] _restrictsObjects = null;
		private object[] _restrictTutorObjects = null;
		private void leadingGrid_CurrentCellChanged(object sender, EventArgs e)
		{
			Cursor.Current = Cursors.WaitCursor;

			CurrencyManager leadingCurrencyManager = (CurrencyManager)leadingGrid.BindingContext[leadingList];

			// �������� ��������� �� ����� ������
			if (comboBoxWeeks.SelectedIndex != -1 && leadingCurrencyManager.Count != 0)
			if (_oldRow != leadingCurrencyManager.Position || comboBoxWeeks.SelectedIndex != -1)
			{				
				_oldRow = leadingCurrencyManager.Position;
				

				_restrictsObjects = new object[(leadingCurrencyManager.Current as StudyLoading).Groups.Count];
				(leadingCurrencyManager.Current as StudyLoading).Groups.CopyTo(_restrictsObjects, 0);
				
				_restrictTutorObjects = new object[(leadingCurrencyManager.Current as StudyLoading).Tutors.Count];
				(leadingCurrencyManager.Current as StudyLoading).Tutors.CopyTo(_restrictTutorObjects, 0);
				
				SheduleNav.ShowRestricts(_restrictsObjects, shmatr, comboBoxWeeks.SelectedIndex+1);
				tutorRestrictView.ShowRestricts(_restrictTutorObjects, shmatr, comboBoxWeeks.SelectedIndex+1);
				
				// ��������� ������ ������� ������ � ��������
				this.groupSheduleView2.ShowShedule(groupList,
					this.shmatr,
					comboBoxWeeks.SelectedIndex+1);	
				// ϳ������� ������� � ���� ���������
				
				this.groupSheduleView2.ShowLessons((leadingCurrencyManager.Current as StudyLoading).Uid);

				this.subObjectList.DataSource = (leadingList[ leadingCurrencyManager.Position ] as StudyLoading).Groups;
				this.listBox2.DataSource = 	(leadingList[ leadingCurrencyManager.Position ] as StudyLoading).Tutors;
			}
			Cursor.Current = Cursors.Default;
		}

		private void leadingGrid_BindingContextChanged(object sender, EventArgs e)
		{
			DataGrid dg = (DataGrid) sender;
			///������ ������� DataGrid'� ����������� ������
			Type dgt = dg.GetType();
			MethodInfo mi = dgt.GetMethod("ColAutoResize",BindingFlags.NonPublic | BindingFlags.Instance);
			for( int i = dg.FirstVisibleColumn; i< dg.VisibleColumnCount; i++)
			{
				object[] methodArgs = {i};
				mi.Invoke(sender, methodArgs);
			}
		}

		private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			ObjectEditor oe = new ObjectEditor(objectFactory);
			object newobject = objectFactory.New(typeof(StudyLoading));
					
			oe.SelectedObject = newobject;					
			if (oe.ShowDialog(this) == DialogResult.OK)
			{				
				(newobject as StudyLoading).Uid = shmatr.AddStudyLeading( RemoteAdapter.Adapter.AdaptStudyLeading2(newobject as StudyLoading));
				 
				if (lsfiltr.FilterObject(newobject))
				{
					(leadingGrid.DataSource as ListWrapper).OnListChanged(new ListChangedEventArgs(ListChangedType.ItemAdded, leadingList.Add(newobject)));
					leadingGrid_BindingContextChanged( leadingGrid, e);
				}
			}
		}

		private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			ObjectEditor oe = new ObjectEditor(objectFactory);

			oe.SelectedObject = leadingGrid.BindingContext[leadingList].Current;
					
					
			if (oe.ShowDialog(this) == DialogResult.OK)
			{
				StudyLoading editedStdLtd = leadingGrid.BindingContext[leadingList].Current as StudyLoading;

				if (editedStdLtd != null)
				{
					shmatr.ChangeStudyLeading(editedStdLtd.Uid, RemoteAdapter.Adapter.AdaptStudyLeading2( editedStdLtd ));
					/* ���� �������� */

					shmatr.SetIsSaved( false );
				}

				int CurrentPos = leadingGrid.CurrentRowIndex;
				(leadingGrid.DataSource as ListWrapper).OnListChanged(new ListChangedEventArgs(ListChangedType.ItemChanged, CurrentPos));
			}
		}

		private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			linkLabel3.ContextMenu.Show(linkLabel3, new Point( 0 , linkLabel3.Height));
			
			if ( MessageBox.Show(Vocabruary.Translate("DropDlg.Question"), Vocabruary.Translate("DropDlg.Title"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes )
			{
                shmatr.DelStudyLeading(RemoteAdapter.Adapter.AdaptStudyLeading2(leadingList[leadingGrid.BindingContext[leadingList].Position] as StudyLoading));
				leadingList.Remove( leadingList[ leadingGrid.BindingContext[leadingList].Position ] );
				
				//(leadingGrid.DataSource as ListWrapper).OnListChanged(new System.ComponentModel.ListChangedEventArgs(System.ComponentModel.ListChangedType.ItemDeleted, CurrentPos));
				(leadingGrid.DataSource as ListWrapper).OnListChanged(new ListChangedEventArgs(ListChangedType.ItemChanged, 0));
				this.groupSheduleView2.ShowShedule((leadingList[ leadingGrid.BindingContext[leadingList].Position ] as StudyLoading).Groups, shmatr, this.comboBoxWeeks.SelectedIndex+1);
			}
		}

		private void linkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			
			FlatBrowser2 _flatBrowser = new FlatBrowser2(this.weekDays, this.dayPairs, this.shmatr, this.buildingList);
			StudyLoading stdLoad = leadingGrid.BindingContext[leadingList].Current as StudyLoading;
			_flatBrowser.Text = string.Format( Vocabruary.Translate("NewLesson.Title"), stdLoad.ToString());			

			if ( _flatBrowser.ShowDialog(shmatr.GetSheduleBeginDate(), 0, stdLoad) == DialogResult.OK)
				foreach( StudyHour sp in _flatBrowser.SelectedPairList)
					if (sp!=null)
					{
						int _pair = int.Parse(sp.MnemoCode);

						
								#region �������� �� ��������� �����
								foreach (Group _group in stdLoad.Groups)
								{
									ExtObjKey key = new ExtObjKey();
									key.UID = this.objectFactory.GetUUID(_group, out key.Signature, out key.Index);

									if (this.shmatr.GroupBusyCheck(_flatBrowser.SelectedDay, _pair, key) == GroupBusyCheckerState.isBusy)					
									{
										if (stdLoad.Groups.Count == 1)								
											MessageBox.Show(string.Format(Vocabruary.Translate("Message.GroupBusyText"), RemoteAdapter.Adapter.AdaptLesson(shmatr.GetGroupLesson(key, _flatBrowser.SelectedDay, _pair)).GroupViewString),
												Vocabruary.Translate("Message.GroupBusyTitle"), MessageBoxButtons.OK, MessageBoxIcon.Asterisk);									
										else								
											MessageBox.Show(string.Format(Vocabruary.Translate("Message.GroupsBusyText"), RemoteAdapter.Adapter.AdaptLesson(shmatr.GetGroupLesson(key, _flatBrowser.SelectedDay, _pair)).GroupViewString), 
												string.Format(Vocabruary.Translate("Message.GroupsBusyTitle"), _group.MnemoCode), MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
								
										return;
									}

									if (this.shmatr.GroupBusyCheck(_flatBrowser.SelectedDay, _pair, key) == GroupBusyCheckerState.isRestrict)					
									{											
										if (MessageBox.Show(string.Format(Vocabruary.Translate("Message.GroupRestrictText"), shmatr.GetGroupRestrictsText(key, _flatBrowser.SelectedDay, _pair)),
											Vocabruary.Translate("Message.GroupBusyTitle"), MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk) == DialogResult.No)
										{
											return;
										}
									}
								}
								#endregion

								#region �������� �� ��������� ����������
								foreach(Tutor _tutor in stdLoad.Tutors)
								{
									ExtObjKey key = new ExtObjKey();
									key.UID = this.objectFactory.GetUUID(_tutor, out key.Signature, out key.Index);

									if (this.shmatr.TutorBusyCheck(_flatBrowser.SelectedDay, _pair, key) == TutorBusyCheckerState.isBusy)
									{
										if (stdLoad.Tutors.Count == 1)								
											MessageBox.Show(string.Format(Vocabruary.Translate("Message.TutorBusyText"), _tutor.ToString(), RemoteAdapter.Adapter.AdaptLesson(shmatr.GetTutorLesson(key, _flatBrowser.SelectedDay, _pair)).GroupViewString),
												Vocabruary.Translate("Message.TutorBusyTitle"), MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
										else
											MessageBox.Show(string.Format(Vocabruary.Translate("Message.TutorsBusyText"),RemoteAdapter.Adapter.AdaptLesson(shmatr.GetTutorLesson(key, _flatBrowser.SelectedDay, _pair )).GroupViewString),
												string.Format(Vocabruary.Translate("Message.TutorsBusyTitle"),_tutor.ToString()), MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
										return;
									}
				
									if (this.shmatr.TutorBusyCheckBoolRestrict(_flatBrowser.SelectedDay, _pair, key))
									{
										string restrictText = this.shmatr.GetTutorRestrictsText(key, _flatBrowser.SelectedDay, _pair);
										if ( MessageBox.Show(_tutor.ToString()+":"+restrictText + "\n������������?",
											Vocabruary.Translate("Message.TutorBusyTitle"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) 
										{
											return;
										}
									}
								}
								#endregion

//								#region �������� ��������� �� ������
//								if (stdLoad.Dept != null && stdLoad.StudyForm != null && stdLoad.SubjectName != null)
//									if(stdLoad.Dept.MnemoCode == "505" && stdLoad.StudyForm.MnemoCode=="��." && stdLoad.SubjectName.Name=="Գ����")
//									{							
//										if ( shmatr.CheckLessonCountByPair(key, stdLoad.StudyForm.Uid, stdLoad.SubjectName.Uid, 4))
//										{
//											MessageBox.Show(Vocabruary.Translate("Message.FizikaLimit"),
//												Vocabruary.Translate("Message.FizikaLimitTitle"), MessageBoxButtons.OK);
//											return;
//										}
//									}
//								#endregion
//
								// ����� ������� �� ��������
								ArrayList flatList = new ArrayList();
								foreach(LectureHall lh in _flatBrowser.SelectedFlatList)
									if (lh != null)
										flatList.Add(lh);

								Lesson2 newlesson  = new Lesson2(stdLoad.SubjectName, stdLoad.StudyForm, stdLoad.Groups, stdLoad.Tutors, flatList);

								newlesson.LeadingUid = stdLoad.Uid;
								newlesson.CurLessonDay = _flatBrowser.SelectedDay;
								newlesson.CurLessonPair = sp;

                                RemoteLesson2 remoteLesson = RemoteAdapter.Adapter.AdaptLesson(newlesson);
								if ( shmatr.Insert( remoteLesson.date, remoteLesson.pair, remoteLesson) != -1)
								{
									// ����� �� ��������������� ������ � ������� //
									stdLoad.UsedHourByWeek += 2;
								}
							}
			this.groupSheduleView2.RefreshShedule();
			
			SheduleNav.ShowRestricts(_restrictsObjects, shmatr, comboBoxWeeks.SelectedIndex+1);
			tutorRestrictView.ShowRestricts(_restrictTutorObjects, shmatr, comboBoxWeeks.SelectedIndex+1);

			(leadingGrid.DataSource as ListWrapper).OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
		}

		private void linkLabel6_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			//�������� �������			
			IList lesshedpos = this.groupSheduleView2.getSheduleSelPositions();
			foreach(ShedulePosition2 shpos in  lesshedpos)
			{
				IList groups = new ArrayList();
				foreach (Group gr in shpos.Columns)
					if(gr != null)
					{
						ExtObjKey key = new ExtObjKey();
						key.UID = this.objectFactory.GetUUID(gr, out key.Signature, out key.Index);

						groups.Add(key);
					}

				this.shmatr.DeleteByGroup(shpos.WeekDay, shpos.DayPair, groups);
			}

			int rownumber = leadingGrid.CurrentCell.RowNumber ;
					
			ListSortDirection lsd = (leadingGrid.DataSource as ListWrapper).SortDirection;
			PropertyDescriptor pd = (leadingGrid.DataSource as ListWrapper).SortProperty;

			this.RefreshStudyLeading();

			if (pd != null)
				(leadingGrid.DataSource as ListWrapper).ApplySort(pd, lsd);	
				
			leadingGrid.CurrentCell = new DataGridCell(rownumber, 1);
				
			this.groupSheduleView2.RefreshShedule();
			SheduleNav.ShowRestricts(_restrictsObjects, shmatr, comboBoxWeeks.SelectedIndex+1);
			tutorRestrictView.ShowRestricts(_restrictTutorObjects, shmatr, comboBoxWeeks.SelectedIndex+1);

			(leadingGrid.DataSource as ListWrapper).OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
		}

		private void linkLabel5_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			linkLabel5.ContextMenu.Show(linkLabel5, new Point( 0 , linkLabel5.Height));
		}

		private void groupSheduleView1_DragEnter(object sender, DragEventArgs e)
		{	
			if (e.Data.GetDataPresent(typeof(StudyLoading)))
			{
				StudyLoading stdLoading = e.Data.GetData(typeof(StudyLoading)) as StudyLoading;

				if (stdLoading.HourByWeek > stdLoading.UsedHourByWeek)
					e.Effect = DragDropEffects.Copy;
				else
					e.Effect = DragDropEffects.None;
			}
			else
				e.Effect = DragDropEffects.None;
		}

		private void leadingGrid_MouseMove(object sender, MouseEventArgs e)
		{
			DataGrid.HitTestInfo _hti= leadingGrid.HitTest(new Point(e.X, e.Y));
			
			if (_hti.Row != -1 && _hti.Column != -1)
				if (e.Button == MouseButtons.Left)
					leadingGrid.DoDragDrop( leadingGrid.BindingContext[leadingList].Current as StudyLoading, DragDropEffects.Copy | DragDropEffects.Move);
		}

		private void groupSheduleView1_DragDrop(object sender, DragEventArgs e)
		{
			if(e.Data.GetDataPresent(typeof(StudyLoading)))
			{
				StudyLoading stdLoad = e.Data.GetData( typeof(StudyLoading)) as StudyLoading;
				DateTime _dayDate;
				int _dayIndex, _pairIndex;
				if (groupSheduleView2.GetShedulePosition(e.X, e.Y, out _dayDate, out _dayIndex, out _pairIndex) == 1)
				{						
					// �������� �� ���������
					// ��������� �� ���
					if (_dayDate < shmatr.GetSheduleBeginDate() || _dayDate > shmatr.GetSheduleEndDate())
						return;

					foreach (Group _group in stdLoad.Groups)
					{
						ExtObjKey key = new ExtObjKey();
						key.UID = this.objectFactory.GetUUID(_group, out key.Signature, out key.Index);

						if (this.shmatr.GroupBusyCheck(_dayDate, _pairIndex, key) == GroupBusyCheckerState.isBusy)					
						{
							if (stdLoad.Groups.Count == 1)								
								MessageBox.Show(string.Format(Vocabruary.Translate("Message.GroupBusyText"), RemoteAdapter.Adapter.AdaptLesson(shmatr.GetGroupLesson(key, _dayDate, _pairIndex)).GroupViewString),
									Vocabruary.Translate("Message.GroupBusyTitle"), MessageBoxButtons.OK, MessageBoxIcon.Asterisk);									
							else								
								MessageBox.Show(string.Format(Vocabruary.Translate("Message.GroupsBusyText"), RemoteAdapter.Adapter.AdaptLesson(shmatr.GetGroupLesson(key, _dayDate, _pairIndex)).GroupViewString), 
									string.Format(Vocabruary.Translate("Message.GroupsBusyTitle"), _group.MnemoCode), MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
							
							return;
						}

						if (this.shmatr.GroupBusyCheck(_dayDate, _pairIndex, key) == GroupBusyCheckerState.isRestrict)					
						{	
							if (MessageBox.Show(string.Format(Vocabruary.Translate("Message.GroupRestrictText"), shmatr.GetGroupRestrictsText(key, _dayDate, _pairIndex)),
								Vocabruary.Translate("Message.GroupBusyTitle"), MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk) == DialogResult.No)
							{
								return;
							}
						}
					}

					// �������� �� ���������
					foreach(Tutor _tutor in stdLoad.Tutors)
					{
						ExtObjKey key = new ExtObjKey();
						key.UID = this.objectFactory.GetUUID(_tutor, out key.Signature, out key.Index);

						if (this.shmatr.TutorBusyCheck(_dayDate, _pairIndex, key) == TutorBusyCheckerState.isBusy)
						{
							if (stdLoad.Tutors.Count == 1)								
								MessageBox.Show(string.Format(Vocabruary.Translate("Message.TutorBusyText"), _tutor.ToString(), RemoteAdapter.Adapter.AdaptLesson(shmatr.GetTutorLesson(key, _dayDate, _pairIndex )).GroupViewString),
									Vocabruary.Translate("Message.TutorBusyTitle"), MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
							else
								MessageBox.Show(string.Format(Vocabruary.Translate("Message.TutorsBusyText"),RemoteAdapter.Adapter.AdaptLesson(shmatr.GetTutorLesson(key, _dayDate, _pairIndex )).GroupViewString),
									string.Format(Vocabruary.Translate("Message.TutorsBusyTitle"),_tutor.ToString()), MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
							return;
						}
			
						if (this.shmatr.TutorBusyCheckBoolRestrict(_dayDate, _pairIndex, key))
						{	
							string restrictText = this.shmatr.GetTutorRestrictsText(key, _dayDate, _pairIndex);
							if ( MessageBox.Show(_tutor.ToString()+":"+restrictText + "\n������������?",
									Vocabruary.Translate("Message.TutorBusyTitle"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) 
							{
								return;
							}
						}
					}

//					// ��������� ������
//					if (stdLoad.Dept != null && stdLoad.StudyForm != null && stdLoad.SubjectName != null)
//						if(stdLoad.Dept.MnemoCode == "505" && stdLoad.StudyForm.MnemoCode=="��.")
//						{							
//							if ( shmatr.CheckDeptLessonCountByPair(key, stdLoad.StudyForm.Uid, stdLoad.Dept.Uid, 4))
//							{
//								MessageBox.Show(Vocabruary.Translate("Message.FizikaLimit"),
//									Vocabruary.Translate("Message.FizikaLimitTitle"), MessageBoxButtons.OK);
//								return;
//							}
//						}

					// ����� ��� ������ ��������(�)
					FlatBrowser2 _flatBrowser = new FlatBrowser2(this.weekDays, this.dayPairs, this.shmatr, this.buildingList);

					if (_flatBrowser.ShowDialog(_dayDate, _pairIndex, stdLoad) == DialogResult.OK)
					{						
						foreach( StudyHour sp in _flatBrowser.SelectedPairList)
						{
							ArrayList flatList = new ArrayList();
							foreach(LectureHall lh in _flatBrowser.SelectedFlatList)
								if (lh != null)
									flatList.Add(lh);

							Lesson2 newlesson  = new Lesson2(stdLoad.SubjectName, stdLoad.StudyForm, stdLoad.Groups, stdLoad.Tutors, flatList);															

							newlesson.LeadingUid = stdLoad.Uid;
							newlesson.CurLessonDay = _flatBrowser.SelectedDay;
							newlesson.CurLessonPair = sp;

                            RemoteLesson2 remoteLesson = RemoteAdapter.Adapter.AdaptLesson(newlesson);
							if ( shmatr.Insert( remoteLesson.date, remoteLesson.pair, remoteLesson) != -1)
							{
								// ����� �� ��������������� ������ � ������� //
								stdLoad.UsedHourByWeek += 2;
							}
						}
						this.groupSheduleView2.RefreshShedule();												
						this.groupSheduleView2.ShowLessons(stdLoad.Uid);
						
						SheduleNav.ShowRestricts(_restrictsObjects, shmatr, comboBoxWeeks.SelectedIndex+1);
						tutorRestrictView.ShowRestricts(_restrictTutorObjects, shmatr, comboBoxWeeks.SelectedIndex+1);

						(leadingGrid.DataSource as ListWrapper).OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));								
					}
				}
			}
		}

		private void leadingGrid_KeyDown(object sender, KeyEventArgs e)
		{
		}

		private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
		{			
			if (this.comboBox1.SelectedIndex != -1)
				this.groupSheduleView2.Scale((this.comboBox1.SelectedIndex+1) * 10);
		}

		private void SheduleView_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.Control)
			{
				if (e.KeyCode == Keys.Insert)
					MessageBox.Show("Insert");
				if (e.KeyCode == Keys.Delete)
					MessageBox.Show("Delete");
				e.Handled = true;
			}
		}

		private void menuItem2_Click(object sender, EventArgs e)
		{
//			StudyLoading selectedObject = leadingGrid.BindingContext[leadingList].Current as StudyLoading;
//			if (selectedObject != null)
//			{
//				//��������� �������� ������� �� ������ ������� ���������� ������� � ����� ��� ���������� 
//				// ������
//				if (selectedObject.StudyForm.MnemoCode == "�.")
//				{
////					DecisionSupportDlg dlg = new DecisionSupportDlg();
//					//IList Pairs = (new DB4OBridge()).ObjectList(typeof(StudyHour));
//					//for(int index = 0; index< 3; index++)
//					//{
//
//					//}
//				}
//
//				// ������������
//				if (selectedObject.StudyForm.MnemoCode == "��.")
//				{
//
//				}
//
//				// ��������
//				if (selectedObject.StudyForm.MnemoCode == "�.")
//				{
//
//				}
//			}
		}

		private void SheduleNav_MouseUp(object sender, MouseEventArgs e)
		{
//			int _week, _day, _pair;
//			if(e.Button == MouseButtons.Left)			
//			{
//				if (SheduleNav.ShedulePositionByXY( e.X, e.Y, out _week, out _day, out _pair) == 1)
//					this.groupSheduleView2.SetFocus(_day, _week, _pair);					
//			}
		}

		private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (listBox2.Focused)
			{
				tutorRestrictView.Show(listBox2.SelectedItem, this.shmatr, comboBoxWeeks.SelectedIndex+1);
			}
		}

		private void listBox2_Enter(object sender, EventArgs e)
		{
			listBox2.BackColor = Color.LightGray;
		}

		private void listBox2_Leave(object sender, EventArgs e)
		{
			listBox2.BackColor = Color.WhiteSmoke;
		}

		private void leadingGrid_Enter(object sender, EventArgs e)
		{
			leadingGrid.BackColor = Color.LightGray;
		}

		private void leadingGrid_Leave(object sender, EventArgs e)
		{
			leadingGrid.BackColor = Color.WhiteSmoke;
		}

		private void menuItem8_Click(object sender, EventArgs e)
		{	
			IList lesshedpos = this.groupSheduleView2.getSheduleSelPositions();				
			#region ���������� ������� � ���� ����
			if (lesshedpos.Count == 1 && lesshedpos[0] != null && (lesshedpos[0] as ShedulePosition2).Columns.Count == 1)
			{	
				ExtObjKey key1 = new ExtObjKey();
				key1.UID = this.objectFactory.GetUUID(((lesshedpos[0] as ShedulePosition2).Columns[0] as Group), out key1.Signature, out key1.Index);
						
				Lesson2 lesson = RemoteAdapter.Adapter.AdaptLesson(
						this.shmatr.GetGroupLesson(
						key1,
						(lesshedpos[0] as ShedulePosition2).WeekDay,
						(lesshedpos[0] as ShedulePosition2).DayPair
						)
						);

					Lesson2[] lessonList = new Lesson2[1];
					lessonList[0] = lesson;

					// �����'������ ����� 
					int leadingID = 
						this.shmatr.GetGroupLesson(
						key1,
						(lesshedpos[0] as ShedulePosition2).WeekDay,
						(lesshedpos[0] as ShedulePosition2).DayPair						
						).leading_uid;
					
					StudyLoading sl = RemoteAdapter.Adapter.AdaptStudyLeading( this.shmatr.GetStudyLeading(leadingID) );
					sl.UsedHourByWeek -=2;
						
					// ����� ��� ������ �������
					FlatBrowser2 _flatBrowser = new FlatBrowser2(this.weekDays, this.dayPairs, this.shmatr, this.buildingList);
					_flatBrowser.CanChangeTime = true;
					_flatBrowser.CanChangeFlat = true;

					foreach (object obj in lesson.FlatList)
						_flatBrowser.SelectedFlatList.Add( obj );
					
					if(_flatBrowser.ShowDialog((lesshedpos[0] as ShedulePosition2).WeekDay, (lesshedpos[0] as ShedulePosition2).DayPair, sl) == DialogResult.OK)
					{							
						RemoteLesson2[] lesson_remList = new RemoteLesson2[lessonList.Length];
						for(int index=0; index<lessonList.Length; index++)
                            lesson_remList[index] = RemoteAdapter.Adapter.AdaptLesson(lessonList[index]);
						
						IList _pairList = new ArrayList();
						IList _pairListUid = new ArrayList();

						foreach( StudyHour sp in _flatBrowser.SelectedPairList)
							if (sp!=null)
							{
								_pairList.Add( int.Parse(sp.MnemoCode));	
								_pairListUid.Add(sp.Uid);
							}						

						if( lesson_remList.Length<=_pairList.Count )
							for(int index=0; index<lesson_remList.Length; index++)
							{
								lesson_remList[index].flatList.Clear();
								foreach(LectureHall lh in _flatBrowser.SelectedFlatList)
								{
									ExtObjKey lh_key = new ExtObjKey();
									lh_key.UID = objectFactory.GetUUID(lh, out lh_key.Signature, out lh_key.Index);
									lesson_remList[index].flatList.Add( lh_key );
								}								

								if (this.shmatr.FlatBusyCheckBool(_flatBrowser.SelectedDay ,(int)_pairList[index], lesson_remList[index].flatList))
									if( MessageBox.Show(this, Vocabruary.Translate("Message.CanUseFlatText"), Vocabruary.Translate("Message.WarningTitle"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
										return;
								
								// ��������� �� ���
								if ((lesshedpos[0] as ShedulePosition2).WeekDay < shmatr.GetSheduleBeginDate() || (lesshedpos[0] as ShedulePosition2).WeekDay > shmatr.GetSheduleEndDate())
									return;

								#region �������� �� ��������� � �����
								foreach (Group _group in sl.Groups)
								{
									ExtObjKey key = new ExtObjKey();
									key.UID = objectFactory.GetUUID(_group, out key.Signature, out key.Index);

									if (this.shmatr.GroupBusyCheck(_flatBrowser.SelectedDay ,(int)_pairList[index], key) == GroupBusyCheckerState.isBusy)					
									{
										if (sl.Groups.Count == 1)
											MessageBox.Show(string.Format(Vocabruary.Translate("Message.GroupBusyText"), RemoteAdapter.Adapter.AdaptLesson(shmatr.GetGroupLesson(key, _flatBrowser.SelectedDay ,(int)_pairList[index])).GroupViewString),
												Vocabruary.Translate("Message.GroupBusyTitle"), MessageBoxButtons.OK, MessageBoxIcon.Asterisk);									
										else								
											MessageBox.Show(string.Format(Vocabruary.Translate("Message.GroupsBusyText"), RemoteAdapter.Adapter.AdaptLesson(shmatr.GetGroupLesson(key, _flatBrowser.SelectedDay ,(int)_pairList[index])).GroupViewString), 
												string.Format(Vocabruary.Translate("Message.GroupsBusyTitle"), _group.MnemoCode), MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
							
										return;
									}

									if (this.shmatr.GroupBusyCheck(_flatBrowser.SelectedDay ,(int)_pairList[index], key) == GroupBusyCheckerState.isRestrict)					
									{
										
										if (MessageBox.Show(string.Format(Vocabruary.Translate("Message.GroupRestrictText"), shmatr.GetGroupRestrictsText(key, _flatBrowser.SelectedDay ,(int)_pairList[index])),
											Vocabruary.Translate("Message.GroupBusyTitle"), MessageBoxButtons.YesNo, MessageBoxIcon.Asterisk) == DialogResult.No)
										{
											return;
										}
									}
								}
								#endregion

								#region �������� �� ��������� ���������
								foreach(Tutor _tutor in sl.Tutors)
								{
									ExtObjKey key = new ExtObjKey();
									key.UID = objectFactory.GetUUID(_tutor, out key.Signature, out key.Index);

									if (this.shmatr.TutorBusyCheck(_flatBrowser.SelectedDay ,(int)_pairList[index], key) == TutorBusyCheckerState.isBusy)
									{
										if (sl.Tutors.Count == 1)								
											MessageBox.Show(string.Format(Vocabruary.Translate("Message.TutorBusyText"), _tutor.ToString(), RemoteAdapter.Adapter.AdaptLesson(shmatr.GetTutorLesson(key, _flatBrowser.SelectedDay ,(int)_pairList[index] )).GroupViewString),
												Vocabruary.Translate("Message.TutorBusyTitle"), MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
										else
											MessageBox.Show(string.Format(Vocabruary.Translate("Message.TutorsBusyText"),RemoteAdapter.Adapter.AdaptLesson(shmatr.GetTutorLesson(key, _flatBrowser.SelectedDay ,(int)_pairList[index] )).GroupViewString),
												string.Format(Vocabruary.Translate("Message.TutorsBusyTitle"),_tutor.ToString()), MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
										return;
									}
			
									if (this.shmatr.TutorBusyCheckBoolRestrict(_flatBrowser.SelectedDay ,(int)_pairList[index], key))
									{		
										string restrictText = this.shmatr.GetTutorRestrictsText(key, _flatBrowser.SelectedDay ,(int)_pairList[index]);
										if ( MessageBox.Show(_tutor.ToString()+":"+restrictText + "\n������������?",
											Vocabruary.Translate("Message.TutorBusyTitle"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No) 
										{
											return;
										}
									}
								}
								#endregion

								#region �������� ������ �������� �� ����� 4 ��� � ����� ���� �� ����
//								if (sl.Dept != null && sl.StudyForm != null && sl.SubjectName != null)
//									if(sl.Dept.MnemoCode == "505" && sl.StudyForm.MnemoCode=="��.")
//									{							
//										if ( shmatr.CheckDeptLessonCountByPair(key, sl.StudyForm.Uid, sl.Dept.Uid, 4))
//										{
//											MessageBox.Show(Vocabruary.Translate("Message.FizikaLimit"),
//												Vocabruary.Translate("Message.FizikaLimitTitle"), MessageBoxButtons.OK);
//											return;
//										}
//									}
								#endregion

								// ����� ��������, ����� ������ ��������� ������� ����� SheduleMatrix
								this.shmatr.MoveLesson(lesson_remList[index], _flatBrowser.SelectedDay, (int)_pairList[index]);
							}
					}

					this.groupSheduleView2.RefreshShedule();
					SheduleNav.ShowRestricts(_restrictsObjects, shmatr, comboBoxWeeks.SelectedIndex+1);
					(leadingGrid.DataSource as ListWrapper).OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));								
				}
				#endregion
		}

		private void menuItem7_Click(object sender, EventArgs e)
		{
			IList lesshedpos = this.groupSheduleView2.getSheduleSelPositions();				
			#region ���� �������
				if (lesshedpos.Count == 1 && lesshedpos[0] != null)
				{	
					ExtObjKey key1 = new ExtObjKey();
					key1.UID = this.objectFactory.GetUUID(((lesshedpos[0] as ShedulePosition2).Columns[0] as Group), out key1.Signature, out key1.Index);
					
					Lesson2 lesson = RemoteAdapter.Adapter.AdaptLesson(
						this.shmatr.GetGroupLesson(
						key1,						
						(lesshedpos[0] as ShedulePosition2).WeekDay,
						(lesshedpos[0] as ShedulePosition2).DayPair
						)
						);

					Lesson2[] lessonList = new Lesson2[1];
					lessonList[0] = lesson;

					// �����'������ ����� 
					int leadingID = 
						this.shmatr.GetGroupLesson(
						key1,
						(lesshedpos[0] as ShedulePosition2).WeekDay,
						(lesshedpos[0] as ShedulePosition2).DayPair
						).leading_uid;

					
					StudyLoading sl = RemoteAdapter.Adapter.AdaptStudyLeading( this.shmatr.GetStudyLeading(leadingID) );
						
					// ����� ��� ������ ��������(�)
					FlatBrowser2 _flatBrowser = new FlatBrowser2(this.weekDays, this.dayPairs, this.shmatr, this.buildingList);
					_flatBrowser.CanChangeTime = false;

					#region Select Flat and Insert Lesson
					if (_flatBrowser.ShowDialog((lesshedpos[0] as ShedulePosition2).WeekDay, (lesshedpos[0] as ShedulePosition2).DayPair, sl) == DialogResult.OK)
					{
						ExtObjKey[] flat_uidList = new ExtObjKey[_flatBrowser.SelectedFlatList.Count];
						for(int index=0; index<_flatBrowser.SelectedFlatList.Count; index++)
						{
							flat_uidList[index] = new ExtObjKey();
							flat_uidList[index].UID = objectFactory.GetUUID((_flatBrowser.SelectedFlatList[index] as LectureHall),out flat_uidList[index].Signature, out flat_uidList[index].Index );
						}
							
						RemoteLesson2[] lesson_remList = new RemoteLesson2[lessonList.Length];
						for(int index=0; index<lessonList.Length; index++)
                            lesson_remList[index] = RemoteAdapter.Adapter.AdaptLesson(lessonList[index]);

						
						// ����� ��������, ����� ������ ��������� ������� ����� SheduleMatrix
						this.shmatr.ChangeLessonFlats(lesson_remList, flat_uidList);
					}
					#endregion

					this.groupSheduleView2.RefreshShedule();
					SheduleNav.ShowRestricts(_restrictsObjects, shmatr, comboBoxWeeks.SelectedIndex+1);
					(leadingGrid.DataSource as ListWrapper).OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));								
				}
				#endregion
		}

		private void linkLabel7_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			this.RefreshStudyLeading();
		}

		private void menuItemDelLoading_Click(object sender, System.EventArgs e)
		{	
			if ( MessageBox.Show(Vocabruary.Translate("DropDlg.Question"), Vocabruary.Translate("DropDlg.Title"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes )
			{
                shmatr.DelStudyLeading(RemoteAdapter.Adapter.AdaptStudyLeading2(leadingList[leadingGrid.BindingContext[leadingList].Position] as StudyLoading));
				leadingList.Remove( leadingList[ leadingGrid.BindingContext[leadingList].Position ] );
				
				//(leadingGrid.DataSource as ListWrapper).OnListChanged(new System.ComponentModel.ListChangedEventArgs(System.ComponentModel.ListChangedType.ItemDeleted, CurrentPos));
				(leadingGrid.DataSource as ListWrapper).OnListChanged(new ListChangedEventArgs(ListChangedType.ItemChanged, 0));
				
				if ((leadingList[ leadingGrid.BindingContext[leadingList].Position ] as StudyLoading)!=null)
					this.groupSheduleView2.ShowShedule((leadingList[ leadingGrid.BindingContext[leadingList].Position ] as StudyLoading).Groups, shmatr, comboBoxWeeks.SelectedIndex+1);
			}
		}

		private void menuDelLessons_Click(object sender, System.EventArgs e)
		{
			if ( MessageBox.Show(Vocabruary.Translate("DropDlg.Question"), Vocabruary.Translate("DropDlg.Title"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes )
			{				
				shmatr.DelLessonsByLeading( (leadingGrid.BindingContext[leadingList].Current as StudyLoading).Uid );
				this.groupSheduleView2.ShowShedule((leadingList[ leadingGrid.BindingContext[leadingList].Position ] as StudyLoading).Groups, shmatr, comboBoxWeeks.SelectedIndex+1);
				this.RefreshStudyLeading();
			}
		}

		private void comboBoxWeeks_SelectedIndexChanged(object sender, System.EventArgs e)
		{
			if (leadingGrid.CurrentRowIndex != -1)
				leadingGrid_CurrentCellChanged(sender, e);
		}

		private void tutorRestrictView_Load(object sender, System.EventArgs e)
		{
		
		}
	}
}
