using System;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Shturman.MultyLanguage;
using Shturman.Nestor.DataEditors;
using Shturman.Nestor.Interfaces;
using Shturman.Shedule.Objects;
using Shturman.Shedule;
using Shturman.Shedule.Server.Interface;
using Shturman.Shedule.Server.Remoting;

namespace Shturman.Shedule.Forms
{
	//public enum ConstraintsBrowserType {tutorConstraints, groupConstraints, flatConstraints};
	/// <summary>
	/// Summary description for TutorConstraints.
	/// </summary>
	public class ConstraintsBrowser2 : Form
	{
		private Panel panelClient;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private Container components = null;

		private ConstraintsBrowserType _browserType;
		private IShedule2 _sheduleMatrix;
		private Panel panel2;
		private TreeView objectTree;
		private Panel panelButton;
		private Button buttonDrop;
		private Button buttonExit;
		private Button buttonClear;
		private Button buttonAdd;
		private ListBox listRestrict;
		private System.Windows.Forms.Splitter splitter1;
		private IObjectStorage _objectStorage;

		public ConstraintsBrowser2(IObjectStorage objectFactory, IShedule2 shedule, ConstraintsBrowserType browserType)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			_objectStorage = objectFactory;
			_sheduleMatrix = shedule;
			_browserType = browserType;		

			switch (_browserType)
			{
				case ConstraintsBrowserType.tutorConstraints:
					this.Text = Vocabruary.Translate("Constraint.TutorTitle");
					#region ���������� ������ �������������� �� ��������
					// ������� ������ ��������
					objectTree.Nodes.Clear();
					// ��� ���� ������
					IList departments = _objectStorage.ObjectList( typeof( Department ) );
					objectTree.BeginUpdate();
					foreach ( Department dept in departments )
					{
						TreeNode deptNode = new TreeNode();						
						deptNode.Text = dept.MnemoCode;
						deptNode.Tag = dept;
						deptNode.Nodes.Add(new TreeNode("-1"));
						
						objectTree.Nodes.Add(deptNode);
					}
					objectTree.EndUpdate();
					#endregion
					break;
				case ConstraintsBrowserType.groupConstraints:
					this.Text = Vocabruary.Translate("Constraint.GroupTitle");
					#region ���������� ������ ����� �� �����������
					// ������� ������ ��������
					objectTree.Nodes.Clear();
					// ��� ���� �����������
					foreach (Faculty faculty in _objectStorage.ObjectList( typeof(Faculty) ) )
					{
						TreeNode facultyNode = new TreeNode( );
						facultyNode.Text = faculty.ToString();
						facultyNode.Tag = faculty;
						facultyNode.Nodes.Add(new TreeNode("-1"));

						objectTree.Nodes.Add( facultyNode );
					}
					#endregion
					break;
				case ConstraintsBrowserType.flatConstraints:
					this.Text = Vocabruary.Translate("Constraint.FlatTitle");
					#region ���������� ������ ��������� �� ��������
					// ������� ������ ��������
					objectTree.Nodes.Clear();
					// ��� ���� �����������
					foreach(Building build in _objectStorage.ObjectList( typeof(Building) ) )
					{
						TreeNode buildTree = new TreeNode();
						buildTree.Text = build.ToString();
						buildTree.Tag = build;
						buildTree.Nodes.Add(new TreeNode("-1"));

						objectTree.Nodes.Add( buildTree );
					}
					#endregion
					break;
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.objectTree = new System.Windows.Forms.TreeView();
			this.panelClient = new System.Windows.Forms.Panel();
			this.panel2 = new System.Windows.Forms.Panel();
			this.listRestrict = new System.Windows.Forms.ListBox();
			this.panelButton = new System.Windows.Forms.Panel();
			this.buttonDrop = new System.Windows.Forms.Button();
			this.buttonExit = new System.Windows.Forms.Button();
			this.buttonClear = new System.Windows.Forms.Button();
			this.buttonAdd = new System.Windows.Forms.Button();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.panelClient.SuspendLayout();
			this.panel2.SuspendLayout();
			this.panelButton.SuspendLayout();
			this.SuspendLayout();
			// 
			// objectTree
			// 
			this.objectTree.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.objectTree.Dock = System.Windows.Forms.DockStyle.Left;
			this.objectTree.ImageIndex = -1;
			this.objectTree.Location = new System.Drawing.Point(0, 0);
			this.objectTree.Name = "objectTree";
			this.objectTree.SelectedImageIndex = -1;
			this.objectTree.Size = new System.Drawing.Size(224, 399);
			this.objectTree.TabIndex = 1;
			this.objectTree.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.objectTree_AfterSelect);
			this.objectTree.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this.objectTree_BeforeExpand);
			// 
			// panelClient
			// 
			this.panelClient.Controls.Add(this.panel2);
			this.panelClient.Controls.Add(this.panelButton);
			this.panelClient.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelClient.Location = new System.Drawing.Point(0, 0);
			this.panelClient.Name = "panelClient";
			this.panelClient.Size = new System.Drawing.Size(690, 399);
			this.panelClient.TabIndex = 9;
			this.panelClient.Paint += new System.Windows.Forms.PaintEventHandler(this.panelClient_Paint);
			// 
			// panel2
			// 
			this.panel2.Controls.Add(this.splitter1);
			this.panel2.Controls.Add(this.listRestrict);
			this.panel2.Controls.Add(this.objectTree);
			this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel2.Location = new System.Drawing.Point(0, 0);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(602, 399);
			this.panel2.TabIndex = 10;
			// 
			// listRestrict
			// 
			this.listRestrict.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.listRestrict.Dock = System.Windows.Forms.DockStyle.Fill;
			this.listRestrict.IntegralHeight = false;
			this.listRestrict.Location = new System.Drawing.Point(224, 0);
			this.listRestrict.Name = "listRestrict";
			this.listRestrict.Size = new System.Drawing.Size(378, 399);
			this.listRestrict.TabIndex = 12;
			// 
			// panelButton
			// 
			this.panelButton.Controls.Add(this.buttonDrop);
			this.panelButton.Controls.Add(this.buttonExit);
			this.panelButton.Controls.Add(this.buttonClear);
			this.panelButton.Controls.Add(this.buttonAdd);
			this.panelButton.Dock = System.Windows.Forms.DockStyle.Right;
			this.panelButton.Location = new System.Drawing.Point(602, 0);
			this.panelButton.Name = "panelButton";
			this.panelButton.Size = new System.Drawing.Size(88, 399);
			this.panelButton.TabIndex = 11;
			// 
			// buttonDrop
			// 
			this.buttonDrop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.buttonDrop.Location = new System.Drawing.Point(8, 40);
			this.buttonDrop.Name = "buttonDrop";
			this.buttonDrop.TabIndex = 4;
			this.buttonDrop.Text = "�������";
			this.buttonDrop.Click += new System.EventHandler(this.buttonDrop_Click);
			// 
			// buttonExit
			// 
			this.buttonExit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.buttonExit.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.buttonExit.Location = new System.Drawing.Point(8, 368);
			this.buttonExit.Name = "buttonExit";
			this.buttonExit.TabIndex = 6;
			this.buttonExit.Text = "�����";
			this.buttonExit.Click += new System.EventHandler(this.buttonExit_Click);
			// 
			// buttonClear
			// 
			this.buttonClear.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.buttonClear.Location = new System.Drawing.Point(8, 72);
			this.buttonClear.Name = "buttonClear";
			this.buttonClear.TabIndex = 5;
			this.buttonClear.Text = "��������";
			// 
			// buttonAdd
			// 
			this.buttonAdd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.buttonAdd.Location = new System.Drawing.Point(8, 8);
			this.buttonAdd.Name = "buttonAdd";
			this.buttonAdd.TabIndex = 3;
			this.buttonAdd.Text = "��������";
			this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
			// 
			// splitter1
			// 
			this.splitter1.Location = new System.Drawing.Point(224, 0);
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(3, 399);
			this.splitter1.TabIndex = 13;
			this.splitter1.TabStop = false;
			// 
			// ConstraintsBrowser2
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(690, 399);
			this.Controls.Add(this.panelClient);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "ConstraintsBrowser2";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "����������� �������������";
			this.Load += new System.EventHandler(this.ConstraintsBrowser_Load);
			this.panelClient.ResumeLayout(false);
			this.panel2.ResumeLayout(false);
			this.panelButton.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void buttonExit_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void ConstraintsBrowser_Load(object sender, EventArgs e)
		{
			this.buttonDrop.Text = Vocabruary.Translate("Button.Drop");
			this.buttonExit.Text = Vocabruary.Translate("Button.Exit");
			this.buttonClear.Text = Vocabruary.Translate("Button.Clear");
			this.buttonAdd.Text = Vocabruary.Translate("Button.Add");
		}

		private void buttonAdd_Click(object sender, EventArgs e)
		{
			Restrict2 restrict = new Restrict2();

			ObjectEditor oe = new ObjectEditor( this._objectStorage );
			oe.SelectedObject = restrict;
			oe.CanChangeInputMode = false;
			oe.Text = "����� �����������";

			if (oe.ShowDialog() != DialogResult.OK) return;

			ExtObjKey key = new ExtObjKey();
			key.UID = _objectStorage.GetUUID(objectTree.SelectedNode.Tag, out key.Signature, out key.Index);

			switch (_browserType)
			{
				case ConstraintsBrowserType.tutorConstraints:					
					_sheduleMatrix.InsertTutorRestrict(restrict.RestrictDay, int.Parse(restrict.RestrictPair.MnemoCode),
					key, RemoteAdapter.Adapter.AdaptRestrict(restrict));
					listRestrict.Items.Add( restrict );					
					break;
				case ConstraintsBrowserType.groupConstraints:					
					_sheduleMatrix.InsertGroupRestrict(restrict.RestrictDay, int.Parse(restrict.RestrictPair.MnemoCode),
					key, RemoteAdapter.Adapter.AdaptRestrict(restrict));
					listRestrict.Items.Add( restrict );					
					break;
				case ConstraintsBrowserType.flatConstraints:					
					_sheduleMatrix.InsertFlatRestrict(restrict.RestrictDay, int.Parse(restrict.RestrictPair.MnemoCode),
                    key, RemoteAdapter.Adapter.AdaptRestrict(restrict));
					listRestrict.Items.Add( restrict );					
					break;
			}
		}

		private void objectTree_AfterSelect(object sender, TreeViewEventArgs e)
		{
			listRestrict.Items.Clear();

			buttonAdd.Enabled = (e.Node.Nodes.Count == 0) && (e.Node.Parent != null);
			if (buttonAdd.Enabled)
			{
				ExtObjKey key = new ExtObjKey();
				key.UID = _objectStorage.GetUUID(objectTree.SelectedNode.Tag, out key.Signature, out key.Index);

				switch (_browserType)
				{
					case ConstraintsBrowserType.tutorConstraints:
						foreach(RemoteRestrict2 remoteRestrict in _sheduleMatrix.GetTutorRestricts(key))
						{														
							listRestrict.Items.Add( RemoteAdapter.Adapter.AdaptRestrict(remoteRestrict) );
						} 											
						break;
					case ConstraintsBrowserType.groupConstraints:
						foreach(RemoteRestrict2 remoteRestrict in _sheduleMatrix.GetGroupRestricts(key))
						{														
							listRestrict.Items.Add( RemoteAdapter.Adapter.AdaptRestrict(remoteRestrict) );
						} 
						break;
					case ConstraintsBrowserType.flatConstraints:
						foreach(RemoteRestrict2 remoteRestrict in _sheduleMatrix.GetFlatRestricts(key))
						{														
							listRestrict.Items.Add( RemoteAdapter.Adapter.AdaptRestrict(remoteRestrict) );
						} 
						break;
				}
			}
		}

		private void objectTree_BeforeExpand(object sender, TreeViewCancelEventArgs e)
		{
			if (e.Node.Nodes.Count == 1)
				if (e.Node.Nodes[0] != null )
					if (e.Node.Nodes[0].Text == "-1")
					{
						switch (_browserType)
						{
							case ConstraintsBrowserType.tutorConstraints:
								e.Node.Nodes.Clear();
								foreach(Tutor ttr in (e.Node.Tag as Department).DepartmentTutors)
									if(ttr != null)
									{
										TreeNode tutorNode = new TreeNode();
										tutorNode.Text = ttr.ToString();
										tutorNode.Tag = ttr;
									
										e.Node.Nodes.Add( tutorNode );
									}
								break;
							case ConstraintsBrowserType.groupConstraints:
								e.Node.Nodes.Clear();
								foreach(Group grp in (e.Node.Tag as Faculty).Groups)
									if(grp != null)
									{
										TreeNode groupNode = new TreeNode();
										groupNode.Text = grp.ToString();
										groupNode.Tag = grp;

										e.Node.Nodes.Add( groupNode );								
									}
								break;
							case ConstraintsBrowserType.flatConstraints:
								e.Node.Nodes.Clear();
								foreach(LectureHall flat in (e.Node.Tag as Building).LectureHalls)
									if(flat != null)
									{
										TreeNode flatNode = new TreeNode();
										flatNode.Text = flat.ToString();
										flatNode.Tag = flat;

										e.Node.Nodes.Add( flatNode );
									}
								break;
						}

					}
		}

		private void buttonDrop_Click(object sender, EventArgs e)
		{
			IList selectedItems = new ArrayList(listRestrict.SelectedItems);
			ExtObjKey key = new ExtObjKey();
			key.UID = _objectStorage.GetUUID(objectTree.SelectedNode.Tag, out key.Signature, out key.Index);
			foreach(Restrict2 restrict in selectedItems)
			{
				switch (_browserType)
				{
					case ConstraintsBrowserType.tutorConstraints:
						_sheduleMatrix.DeleteTutorRestrict( restrict.RestrictDay, int.Parse(restrict.RestrictPair.MnemoCode),
							key);
						break;
					case ConstraintsBrowserType.groupConstraints:
						_sheduleMatrix.DeleteGroupRestrict(restrict.RestrictDay, int.Parse(restrict.RestrictPair.MnemoCode),
							key);
						break;
					case ConstraintsBrowserType.flatConstraints:
						_sheduleMatrix.DeleteFlatRestrict(restrict.RestrictDay, int.Parse(restrict.RestrictPair.MnemoCode),
							key);
						break;
				}
				listRestrict.Items.Remove( restrict );
			}
		}

		private void panelClient_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
		
		}
	}
}
