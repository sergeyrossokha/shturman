/*�����: ������� ����� �������������
 * ����: ����� ������������� ���������� �� ��������� ����� �� �� ��������
 * 
 * ������������ �� ������������ � ������ ������
 * */
using System;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Shturman.MultyLanguage;
using Shturman.Nestor.DataLists;
using Shturman.Nestor.Interfaces;
using Shturman.Shedule.Objects.Container;
using Shturman.Shedule.Objects;
using Shturman.Shedule.GUI;
using Shturman.Shedule.Server.Interface;
using Shturman.Shedule.Server.Remoting;

namespace Shturman.Shedule.Forms
{
	/// <summary>
	/// Summary description for FlatBrowser.
	/// </summary>
	public class FlatBrowser2 : Form
	{
		private Button button1;
		private Button button2;
		private Label label1;
		private Label label2;
		private ListBox selectedFlatList;

		private IShedule2 _shmatr;
		private int WeekNumber;
		private IList _buildingList;
		private StudyLoading _studyLoading;
		private IObjectStorage _storage = DB4OBridgeRem.GetObjectStorage();
		private ImageList imageList1;
		private Button button3;
		private Button button4;
		private Button button5;
		private Label label3;
		private Label label5;
		private Label label6;
		private CheckedListBox checkedListPairs;
		private ErrorProvider errorProvider1;
		private IContainer components;
		private GroupBox timeGroupBox;
		private Label Recomend;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.RadioButton radioButton1;
		private System.Windows.Forms.RadioButton radioButton2;
		private System.Windows.Forms.Button buttonSearchFlat;
		private System.Windows.Forms.TreeView treeSearchFlat;
		private System.Windows.Forms.TreeView treeFlats;
        private Shturman.Shedule.GUI.RestrictSheduleView2 restrictSheduleView21;
		private System.Windows.Forms.DateTimePicker dateTimePicker1;
		private ToolTip toolTip1;
//		private ListFiltrator lsfiltr = new ListFiltrator();

		public FlatBrowser2(IList dayList, IList pairList, IShedule2 shmatr, IList buildingList)
		{

			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			toolTip1.SetToolTip(this.treeFlats, "");			
			toolTip1.SetToolTip(this.treeSearchFlat, "");

			this._shmatr = shmatr;
			this._buildingList = buildingList;

			restrictSheduleView21.LoadShedule(dayList, pairList);
			
			foreach (object obj in pairList)
				this.checkedListPairs.Items.Add( obj );			
		}

		public DialogResult ShowDialog(DateTime selDay, int selHour, StudyLoading stdLoad)
		{
			this.dateTimePicker1.Value = selDay;						
//			if (selDay.)
//			{
//				this.checkedListDays.SetItemChecked( this.checkedListDays.Items.IndexOf(selDay), true);
//				this.checkedListDays.SetItemCheckState( this.checkedListDays.Items.IndexOf(selDay),	CheckState.Checked);
//			}
			
//				if(selDay != null && selHour != null) // ������� ����, �� ������� �����������
//					for(int index=0; index<this.checkedListWeeks.Items.Count; index++)
//					{
//						this.checkedListWeeks.SetItemChecked( index, true );
//						this.checkedListWeeks.SetItemCheckState( index,	CheckState.Checked);
//					}
			
			if (selHour != 0)
			{
				this.checkedListPairs.SetItemChecked( selHour-1, true );
				this.checkedListPairs.SetItemCheckState( selHour-1, CheckState.Checked );
			}
			
			this._studyLoading = stdLoad;
			this.Text = stdLoad.ToString();

			this.Recomend.Text = Vocabruary.Translate("FlatBrowser.Recomendation")+" "+stdLoad.Note;
            
			if ( 
				(this._studyLoading.Dept != null && this._studyLoading.Dept.MnemoCode == "705") /*����������� ��� ���������*/
				||(this._studyLoading.SubjectName.Name == "����������� ��������� �� �������") /*������������ ���������� �� ������ ��� ���������*/
				)
			{
				this.tabPage1.Enabled = false;
				this.tabPage2.Enabled = false;
				this.button1.Enabled = true;
			}
			else
			{
				// Build tree
				this.RebuildTree();
				button1.Enabled = false;
			}
			
			return ShowDialog();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		private void RebuildTree()
		{
			this.treeFlats.Nodes.Clear();
			this.BuildTree();
		}

		private void BuildTree()
		{			
			foreach(Building _building in _buildingList)
			{
				TreeNode _buildTreeNode = new TreeNode();
				_buildTreeNode.Text = _building.MnemoCode;
				_buildTreeNode.Tag = _building;
				_buildTreeNode.ImageIndex = 0;
				_buildTreeNode.SelectedImageIndex = 0;

				_buildTreeNode.Nodes.Add(new TreeNode("-1"));

				this.treeFlats.Nodes.Add( _buildTreeNode );
			}
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(FlatBrowser2));
			this.treeFlats = new System.Windows.Forms.TreeView();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.selectedFlatList = new System.Windows.Forms.ListBox();
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.button3 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.checkedListPairs = new System.Windows.Forms.CheckedListBox();
			this.timeGroupBox = new System.Windows.Forms.GroupBox();
			this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
			this.label5 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.Recomend = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.errorProvider1 = new System.Windows.Forms.ErrorProvider();
			this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.restrictSheduleView21 = new Shedule.GUI.RestrictSheduleView2();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.treeSearchFlat = new System.Windows.Forms.TreeView();
			this.radioButton2 = new System.Windows.Forms.RadioButton();
			this.radioButton1 = new System.Windows.Forms.RadioButton();
			this.buttonSearchFlat = new System.Windows.Forms.Button();
			this.button6 = new System.Windows.Forms.Button();
			this.timeGroupBox.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.SuspendLayout();
			// 
			// treeFlats
			// 
			this.treeFlats.AllowDrop = true;
			this.treeFlats.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.treeFlats.ImageList = this.imageList1;
			this.treeFlats.ItemHeight = 16;
			this.treeFlats.Location = new System.Drawing.Point(0, 16);
			this.treeFlats.Name = "treeFlats";
			this.treeFlats.Size = new System.Drawing.Size(304, 304);
			this.treeFlats.TabIndex = 0;
			this.treeFlats.DoubleClick += new System.EventHandler(this.treeView1_DoubleClick);
			this.treeFlats.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
			this.treeFlats.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this.treeView1_BeforeExpand);
			this.treeFlats.DragEnter += new System.Windows.Forms.DragEventHandler(this.treeView1_DragEnter);
			this.treeFlats.MouseMove += new System.Windows.Forms.MouseEventHandler(this.treeView1_MouseMove);
			this.treeFlats.DragDrop += new System.Windows.Forms.DragEventHandler(this.treeView1_DragDrop);
			// 
			// imageList1
			// 
			this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// selectedFlatList
			// 
			this.selectedFlatList.AllowDrop = true;
			this.selectedFlatList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.selectedFlatList.IntegralHeight = false;
			this.selectedFlatList.Location = new System.Drawing.Point(344, 19);
			this.selectedFlatList.Name = "selectedFlatList";
			this.selectedFlatList.Size = new System.Drawing.Size(136, 112);
			this.selectedFlatList.TabIndex = 1;
			this.selectedFlatList.DragDrop += new System.Windows.Forms.DragEventHandler(this.selectedFlatList_DragDrop);
			this.selectedFlatList.MouseMove += new System.Windows.Forms.MouseEventHandler(this.selectedFlatList_MouseMove);
			this.selectedFlatList.DragEnter += new System.Windows.Forms.DragEventHandler(this.selectedFlatList_DragEnter);
			// 
			// button1
			// 
			this.errorProvider1.SetIconAlignment(this.button1, System.Windows.Forms.ErrorIconAlignment.MiddleLeft);
			this.button1.Location = new System.Drawing.Point(481, 357);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(85, 23);
			this.button1.TabIndex = 3;
			this.button1.Text = "���������";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button2
			// 
			this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.button2.Location = new System.Drawing.Point(577, 357);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(85, 23);
			this.button2.TabIndex = 4;
			this.button2.Text = "��������";
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 1);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(256, 14);
			this.label1.TabIndex = 5;
			this.label1.Text = "��������� ���������";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(347, 4);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(128, 14);
			this.label2.TabIndex = 6;
			this.label2.Text = "��������� ���������";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(308, 34);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(32, 23);
			this.button3.TabIndex = 7;
			this.button3.Text = ">>";
			this.button3.Click += new System.EventHandler(this.treeView1_DoubleClick);
			// 
			// button4
			// 
			this.button4.Location = new System.Drawing.Point(308, 66);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(32, 23);
			this.button4.TabIndex = 8;
			this.button4.Text = "<<";
			this.button4.Click += new System.EventHandler(this.button4_Click);
			// 
			// button5
			// 
			this.button5.Location = new System.Drawing.Point(308, 98);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(32, 23);
			this.button5.TabIndex = 9;
			this.button5.Text = "<>";
			this.button5.Click += new System.EventHandler(this.button5_Click);
			// 
			// checkedListPairs
			// 
			this.checkedListPairs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.checkedListPairs.CheckOnClick = true;
			this.checkedListPairs.IntegralHeight = false;
			this.checkedListPairs.Location = new System.Drawing.Point(8, 70);
			this.checkedListPairs.Name = "checkedListPairs";
			this.checkedListPairs.Size = new System.Drawing.Size(152, 104);
			this.checkedListPairs.TabIndex = 16;
			this.checkedListPairs.Validating += new System.ComponentModel.CancelEventHandler(this.checkedListDays_Validating);
			this.checkedListPairs.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListDays_ItemCheck);
			// 
			// timeGroupBox
			// 
			this.timeGroupBox.Controls.Add(this.dateTimePicker1);
			this.timeGroupBox.Controls.Add(this.label5);
			this.timeGroupBox.Controls.Add(this.label3);
			this.timeGroupBox.Controls.Add(this.checkedListPairs);
			this.timeGroupBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.timeGroupBox.Location = new System.Drawing.Point(8, 0);
			this.timeGroupBox.Name = "timeGroupBox";
			this.timeGroupBox.Size = new System.Drawing.Size(168, 184);
			this.timeGroupBox.TabIndex = 13;
			this.timeGroupBox.TabStop = false;
			this.timeGroupBox.Text = "�����";
			// 
			// dateTimePicker1
			// 
			this.dateTimePicker1.Location = new System.Drawing.Point(8, 30);
			this.dateTimePicker1.Name = "dateTimePicker1";
			this.dateTimePicker1.Size = new System.Drawing.Size(152, 20);
			this.dateTimePicker1.TabIndex = 18;
			this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(8, 52);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(40, 16);
			this.label5.TabIndex = 15;
			this.label5.Text = "����";
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 14);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(38, 14);
			this.label3.TabIndex = 13;
			this.label3.Text = "����";
			// 
			// Recomend
			// 
			this.Recomend.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.Recomend.Location = new System.Drawing.Point(8, 190);
			this.Recomend.Name = "Recomend";
			this.Recomend.Size = new System.Drawing.Size(168, 160);
			this.Recomend.TabIndex = 11;
			this.Recomend.Text = "label7";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(348, 133);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(128, 14);
			this.label6.TabIndex = 10;
			this.label6.Text = "�����������";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// errorProvider1
			// 
			this.errorProvider1.BlinkRate = 150;
			this.errorProvider1.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink;
			this.errorProvider1.ContainerControl = this;
			// 
			// tabControl1
			// 
			this.tabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons;
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Controls.Add(this.tabPage2);
			this.tabControl1.HotTrack = true;
			this.tabControl1.Location = new System.Drawing.Point(176, 5);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.ShowToolTips = true;
			this.tabControl1.Size = new System.Drawing.Size(495, 352);
			this.tabControl1.TabIndex = 15;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.restrictSheduleView21);
			this.tabPage1.Controls.Add(this.label1);
			this.tabPage1.Controls.Add(this.label2);
			this.tabPage1.Controls.Add(this.button3);
			this.tabPage1.Controls.Add(this.button5);
			this.tabPage1.Controls.Add(this.selectedFlatList);
			this.tabPage1.Controls.Add(this.treeFlats);
			this.tabPage1.Controls.Add(this.button4);
			this.tabPage1.Controls.Add(this.label6);
			this.tabPage1.Location = new System.Drawing.Point(4, 25);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Size = new System.Drawing.Size(487, 323);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "���� �������";
			// 
			// restrictSheduleView21
			// 
			this.restrictSheduleView21.CellHeight = 16;
			this.restrictSheduleView21.CellWidth = 16;
			this.restrictSheduleView21.Location = new System.Drawing.Point(344, 152);
			this.restrictSheduleView21.Name = "restrictSheduleView21";
			this.restrictSheduleView21.Size = new System.Drawing.Size(136, 168);
			this.restrictSheduleView21.TabIndex = 12;
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.treeSearchFlat);
			this.tabPage2.Controls.Add(this.radioButton2);
			this.tabPage2.Controls.Add(this.radioButton1);
			this.tabPage2.Controls.Add(this.buttonSearchFlat);
			this.tabPage2.Controls.Add(this.button6);
			this.tabPage2.Location = new System.Drawing.Point(4, 25);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Size = new System.Drawing.Size(487, 323);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "����� �������";
			// 
			// treeSearchFlat
			// 
			this.treeSearchFlat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.treeSearchFlat.HideSelection = false;
			this.treeSearchFlat.ImageIndex = -1;
			this.treeSearchFlat.Location = new System.Drawing.Point(8, 32);
			this.treeSearchFlat.Name = "treeSearchFlat";
			this.treeSearchFlat.SelectedImageIndex = -1;
			this.treeSearchFlat.Size = new System.Drawing.Size(472, 256);
			this.treeSearchFlat.TabIndex = 15;
			this.treeSearchFlat.MouseMove += new System.Windows.Forms.MouseEventHandler(this.treeView1_MouseMove);
			// 
			// radioButton2
			// 
			this.radioButton2.Location = new System.Drawing.Point(128, 8);
			this.radioButton2.Name = "radioButton2";
			this.radioButton2.TabIndex = 13;
			this.radioButton2.Text = "��������";
			// 
			// radioButton1
			// 
			this.radioButton1.Checked = true;
			this.radioButton1.Location = new System.Drawing.Point(8, 8);
			this.radioButton1.Name = "radioButton1";
			this.radioButton1.Size = new System.Drawing.Size(120, 24);
			this.radioButton1.TabIndex = 12;
			this.radioButton1.TabStop = true;
			this.radioButton1.Text = "� ��� ��������";
			// 
			// buttonSearchFlat
			// 
			this.buttonSearchFlat.Location = new System.Drawing.Point(384, 8);
			this.buttonSearchFlat.Name = "buttonSearchFlat";
			this.buttonSearchFlat.Size = new System.Drawing.Size(96, 23);
			this.buttonSearchFlat.TabIndex = 11;
			this.buttonSearchFlat.Text = "������";
			this.buttonSearchFlat.Click += new System.EventHandler(this.buttonSearchFlat_Click);
			// 
			// button6
			// 
			this.button6.Location = new System.Drawing.Point(400, 296);
			this.button6.Name = "button6";
			this.button6.Size = new System.Drawing.Size(85, 23);
			this.button6.TabIndex = 10;
			this.button6.Text = "�������";
			this.button6.Click += new System.EventHandler(this.button6_Click);
			// 
			// FlatBrowser2
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(6, 13);
			this.ClientSize = new System.Drawing.Size(666, 383);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.timeGroupBox);
			this.Controls.Add(this.button2);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.Recomend);
			this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(204)));
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "FlatBrowser2";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "����� ���������";
			this.Load += new System.EventHandler(this.FlatBrowser_Load);
			this.timeGroupBox.ResumeLayout(false);
			this.tabControl1.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void selectedFlatList_DragEnter(object sender, DragEventArgs e)
		{			
			if (e.Data.GetDataPresent( typeof(LectureHall)))
				e.Effect = DragDropEffects.Copy;
			else
				e.Effect = DragDropEffects.None;

		}

		private void treeView1_MouseMove(object sender, MouseEventArgs e)
		{
			TreeView tv = (sender as TreeView);
			if (tv != null)
			{
					TreeNode tn = tv.GetNodeAt(e.X, e.Y);
					if (tn != null && tn.Nodes.Count == 0)
					{
						LectureHall lh = (tn.Tag as LectureHall);
						if(lh!=null)
							if(lh.Department != null)
								toolTip1.SetToolTip(tv, lh.Department.MnemoCode);
							else
								toolTip1.SetToolTip(tv, Vocabruary.Translate("ToolTip.TotalFlat"));
					}
			}
			

			if (e.Button == MouseButtons.Left)
				if(treeFlats.SelectedNode != null)
					treeFlats.DoDragDrop( treeFlats.SelectedNode.Tag , DragDropEffects.Copy | DragDropEffects.Move);
		}

		private void selectedFlatList_DragDrop(object sender, DragEventArgs e)
		{
			object data = e.Data.GetData( typeof( LectureHall ) );
			// �������� �� ��������� 
			bool isEveryWereFree = true;
			
			ExtObjKey key = new ExtObjKey();
			key.UID = this._storage.GetUUID(data, out key.Signature, out key.Index);

			foreach( StudyHour sp in this.checkedListPairs.CheckedItems)				
				isEveryWereFree = isEveryWereFree && (this._shmatr.FlatBusyCheck(dateTimePicker1.Value, int.Parse(sp.MnemoCode), key) == FlatBusyCheckerState.isNotBusy);			
			
			if (isEveryWereFree)
				if ( !this.selectedFlatList.Items.Contains( data ) )
					this.selectedFlatList.Items.Add( data );
			
			if (!isEveryWereFree)
				if( MessageBox.Show(this, Vocabruary.Translate("Message.CanUseFlatText"), Vocabruary.Translate("Message.WarningTitle"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
					if ( !this.selectedFlatList.Items.Contains( data ) )
						this.selectedFlatList.Items.Add( data );

			if (this.selectedFlatList.Items.Count == 0)
				this.button1.Enabled = false;
			else
				this.button1.Enabled = true;
		}

		private void treeView1_DragEnter(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent( typeof(LectureHall)))
				e.Effect = DragDropEffects.Copy;
			else
				e.Effect = DragDropEffects.None;
		}

		private void treeView1_DragDrop(object sender, DragEventArgs e)
		{
			object data = e.Data.GetData( typeof( LectureHall ) );
			if ( this.selectedFlatList.Items.Contains( data ) )
				this.selectedFlatList.Items.Remove( data );
			
			if (this.selectedFlatList.Items.Count == 0)
				this.button1.Enabled = false;
			else	
				this.button1.Enabled = true;
		}

		private void selectedFlatList_MouseMove(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
				if (selectedFlatList.SelectedItem != null)			
					selectedFlatList.DoDragDrop( selectedFlatList.SelectedItem , DragDropEffects.Copy | DragDropEffects.Move);			
		}

		private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
		{
			if (e.Node.Tag is LectureHall)
				this.restrictSheduleView21.Show(e.Node.Tag, this._shmatr, this.WeekNumber);
		}

		private void treeView1_DoubleClick(object sender, EventArgs e)
		{
			TreeNode _node = treeFlats.SelectedNode;
			if (_node.Tag is LectureHall)
			{
				ExtObjKey key = new ExtObjKey();
				key.UID = this._storage.GetUUID(_node.Tag, out key.Signature, out key.Index);

				// �������� �� ��������� 
				bool isEveryWereFree = true;
				
				foreach( StudyHour sp in this.checkedListPairs.CheckedItems)						
					isEveryWereFree = isEveryWereFree && (this._shmatr.FlatBusyCheck(dateTimePicker1.Value, int.Parse(sp.MnemoCode), key) == FlatBusyCheckerState.isNotBusy);

				if (isEveryWereFree)
					if ( !this.selectedFlatList.Items.Contains( _node.Tag ) )
						this.selectedFlatList.Items.Add( _node.Tag );

				if (!isEveryWereFree)
					if( MessageBox.Show(this, Vocabruary.Translate("Message.CanUseFlatText"), Vocabruary.Translate("Message.WarningTitle"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
						if ( !this.selectedFlatList.Items.Contains( _node.Tag ) )
							this.selectedFlatList.Items.Add( _node.Tag );
			}

			
			if (this.selectedFlatList.Items.Count == 0)
				this.button1.Enabled = false;
			else
				this.button1.Enabled = true;
		}

		private void button1_Click(object sender, EventArgs e)
		{			
			if (
				(this._studyLoading.Dept != null && this._studyLoading.Dept.MnemoCode == "705") /*����������� ��� ���������*/
				||(this._studyLoading.SubjectName.Name == "����������� ��������� �� �������") /*������������ ���������� �� ������ ��� ���������*/
				)
				this.DialogResult = DialogResult.OK;
			else
				if(this.selectedFlatList.Items.Count>0)
					this.DialogResult = DialogResult.OK;
		}

		private void FlatBrowser_Load(object sender, EventArgs e)
		{
			/// ������� ��������� ������
			button1.Text = Vocabruary.Translate("Button.Apply");
			button2.Text = Vocabruary.Translate("Button.Cancel");			
			button3.Text = Vocabruary.Translate("Button.MoveTo");
			button4.Text = Vocabruary.Translate("Button.MoveFrom");
			//button5.Text = Vocabruary.Translate("Button.Clear");

			/// ������� ��������� ��� ����� ������
			label1.Text = Vocabruary.Translate("FlatBrowser.FreeFlatTitle");
			label2.Text = Vocabruary.Translate("FlatBrowser.SelectedFlatTitle");			
			label3.Text = Vocabruary.Translate("FlatBrowser.DayListTitle");
			label5.Text = Vocabruary.Translate("FlatBrowser.PairListTitle");
			label6.Text = Vocabruary.Translate("FlatBrowser.RestrictGridTitle");

			timeGroupBox.Text = Vocabruary.Translate("FlatBrowser.TimeBrowseGroupTitle");
			//flatGroupBox.Text = Vocabruary.Translate("FlatBrowser.FlatBrowseGroupTitle");

			
			this.Text = Vocabruary.Translate("FlatBrowser.Title");
		}

		private void checkedListDays_ItemCheck(object sender, ItemCheckEventArgs e)
		{
			if (e.NewValue == CheckState.Checked)
				foreach(TreeNode tn in this.treeFlats.Nodes)
					if (tn.Nodes.Count != 1 || (tn.Nodes.Count == 1 && tn.Nodes[0].Text != "-1"))
					{
						tn.Nodes.Clear();
						tn.Nodes.Add(new TreeNode("-1"));
						tn.Collapse();
					}
			Check();
		}

		private int SelectedHour()
		{
			return this.checkedListPairs.CheckedItems.Count;
		}

		private void checkedListDays_Validating(object sender, CancelEventArgs e)
		{
			if ((sender as CheckedListBox).CheckedItems.Count == 0 )
				this.errorProvider1.SetError((sender as CheckedListBox), "������ ���� ������� ���� �� ���� �������");
			else
				this.errorProvider1.SetError((sender as CheckedListBox), "");

			if (this.SelectedHour() > (_studyLoading.HourByWeek - _studyLoading.UsedHourByWeek))
				this.errorProvider1.SetError(this.button1, "�������� ������ ����� ��� ����������");
			else
				this.errorProvider1.SetError(this.button1, "");
			
		}

		private void button4_Click(object sender, EventArgs e)
		{
			if (this.selectedFlatList.SelectedIndex != -1)
				this.selectedFlatList.Items.RemoveAt( this.selectedFlatList.SelectedIndex );
		}

		private void button5_Click(object sender, EventArgs e)
		{
			this.selectedFlatList.Items.Clear();
		}

		private void treeView1_BeforeExpand(object sender, TreeViewCancelEventArgs e)
		{
			if (e.Node.Nodes.Count == 1)
				if (e.Node.Nodes[0] != null )
					if (e.Node.Nodes[0].Text == "-1")
					{
						// Display a wait cursor while the TreeNodes are being created.
						Cursor.Current = Cursors.WaitCursor;

						e.Node.Nodes.Clear();
						// ��������� ������ �������� �� ���������� ������ �� ������ �����
						ListWrapper _buildingFlatList = new ListWrapper( typeof(LectureHall), (e.Node.Tag as Building).LectureHalls );
						_buildingFlatList.Sort(0);

						//IList filteredLst = _buildingFlatList;
						//if (_studyLoading.StudyForm != null)
						//	if (_studyLoading.StudyForm.MnemoCode == "�.")
						//		foreach (LectureHallType lht in DB4OBridge.theOneObject.ObjectList(typeof(LectureHallType)))
						//			if (lht.MnemoCode == "�.")
						//			{
						//				lsfiltr.AddCondition(LogicOperations.loEMPTY,"HallType","",ConditionOperators.coEqual, lht);
						//				break;
						//			}

						//if (_studyLoading.StudyForm != null)
						//	if (_studyLoading.StudyForm.MnemoCode == "�.")
						//		foreach (LectureHallType lht in DB4OBridge.theOneObject.ObjectList(typeof(LectureHallType)))
						//		{
						//			if (lht.MnemoCode == "�." || lht.MnemoCode == "�.")
						//			{
						//				lsfiltr.AddCondition(LogicOperations.loOR,"HallType","",ConditionOperators.coEqual, lht);
						//			}

						//			if (lht.MnemoCode == "�.")
						//			{
						//				lsfiltr.AddCondition(LogicOperations.loOR,"HallType","",ConditionOperators.coEqual, lht);
						//			}

						//			if (lht.MnemoCode == "��.")
						//			{
						//				lsfiltr.AddCondition(LogicOperations.loOR,"HallType","",ConditionOperators.coEqual, lht);
						//			}
						//		}
				
						//if (_studyLoading.StudyForm != null)
						//	if (_studyLoading.StudyForm.MnemoCode == "��.")
						//		foreach (LectureHallType lht in DB4OBridge.theOneObject.ObjectList(typeof(LectureHallType)))
						//		{
						//			if (lht.MnemoCode == "��.")
						//			{
						//				lsfiltr.AddCondition(LogicOperations.loOR,"HallType","",ConditionOperators.coEqual, lht);
						//			}

						//			if (lht.MnemoCode == "�.")
						//			{
						//				lsfiltr.AddCondition(LogicOperations.loOR,"HallType","",ConditionOperators.coEqual, lht);
						//			}	
						//		}
				
						//filteredLst = lsfiltr; //.Filter( _buildingFlatList );


						// ������ ����� �� ������
						foreach(LectureHall lh in _buildingFlatList)
						{
							TreeNode _flatTreeNode = new TreeNode();
							float _usingPercent = 0;
							
							ExtObjKey key = new ExtObjKey();
							key.UID = this._storage.GetUUID(lh, out key.Signature, out key.Index);

							int _lessonCount = _shmatr.GetFlatLessons(key).Count;
							_usingPercent = (_lessonCount*100)/(_shmatr.GetDaysPerWeekCount()*_shmatr.GetPairsPerDayCount());														
							_flatTreeNode.Text = lh.ToString()+" - "+_usingPercent.ToString()+"%";
							_flatTreeNode.Tag = lh;
					
							bool isEveryWereFree = true;														
							foreach( StudyHour sp in this.checkedListPairs.CheckedItems)									
								isEveryWereFree = isEveryWereFree && (this._shmatr.FlatBusyCheck(dateTimePicker1.Value, int.Parse(sp.MnemoCode), key) == FlatBusyCheckerState.isNotBusy);

							if (isEveryWereFree)
							{
								_flatTreeNode.ImageIndex = 1;
								_flatTreeNode.SelectedImageIndex = 1;
							}
							else
							{
								_flatTreeNode.ImageIndex = 2;
								_flatTreeNode.SelectedImageIndex = 2;
							}

							e.Node.Nodes.Add( _flatTreeNode );
						}
						// Reset the cursor to the default for all controls.
						Cursor.Current = Cursors.Default;
					}
		}

		public DateTime SelectedDay
		{
			get { return this.dateTimePicker1.Value;}
		}

		public IList SelectedFlatList
		{
			get { return this.selectedFlatList.Items; }
		}

		public IList SelectedPairList
		{
			get { return this.checkedListPairs.CheckedItems; }
		}

		public bool CanChangeTime
		{
			get { return timeGroupBox.Enabled; }
			set { timeGroupBox.Enabled = value; }
		}

		public bool CanChangeFlat
		{
			get { return this.tabPage1.Enabled; }
			set { this.tabPage1.Enabled = value; }
		}
	
		public void Check()
		{
			// ���� ������� 705
			if(this._studyLoading != null)
				if(this._studyLoading.Dept != null)
					if(this._studyLoading.Dept.MnemoCode == "705")
					{
						this.button1.Enabled = true;
						return;
					}
			// ���� ������ �������� �� ������
			if (this.selectedFlatList.Items.Count != 0)
			{	
				this.button1.Enabled = true;
				return;
			}
			// � ����� ��������
			this.button1.Enabled = false;
		}

		private void buttonSearchFlat_Click(object sender, System.EventArgs e)
		{
			Cursor.Current = Cursors.WaitCursor;

			/* ����� ������� */
			treeSearchFlat.Nodes.Clear();

			foreach (Building build in _buildingList)
			{
				TreeNode buildNode = new TreeNode(build.MnemoCode);
				buildNode.ImageIndex = 0;
				buildNode.SelectedImageIndex = 0;

				IList findedFlats = new ArrayList();
				foreach(LectureHall lh in  build.LectureHalls)
					if(lh != null)
					{
						ExtObjKey key = new ExtObjKey();
						key.UID = this._storage.GetUUID(lh, out key.Signature, out key.Index);

						foreach( StudyHour sp in this.checkedListPairs.CheckedItems)
						{									
							if (this._shmatr.FlatBusyCheck(dateTimePicker1.Value, int.Parse(sp.MnemoCode), key) == FlatBusyCheckerState.isNotBusy)
							{
								if (!findedFlats.Contains(lh))
									findedFlats.Add( lh );
							}
							else
								if (!radioButton2.Checked)
								findedFlats.Remove( lh );											
						}
					}
				
				foreach(LectureHall lh in findedFlats)
				{
					TreeNode lhNode = new TreeNode();					
					lhNode.Text = lh.ToString();
					lhNode.ImageIndex = -1;
					lhNode.SelectedImageIndex = -1;					
					lhNode.Tag = lh;

					/* ����� �� ������*/
					buildNode.Nodes.Add( lhNode );
				}

				treeSearchFlat.Nodes.Add(buildNode);
			}
			Cursor.Current = Cursors.Default;
		}

		private void button6_Click(object sender, System.EventArgs e)
		{
			/* ���� ����� � ��������� ��������*/
			TreeNode _node = treeSearchFlat.SelectedNode;
			if (_node.Tag is LectureHall)
			{
				ExtObjKey key = new ExtObjKey();
				key.UID = this._storage.GetUUID(_node.Tag, out key.Signature, out key.Index);

				// �������� �� ��������� 
				bool isEveryWereFree = true;
			
				foreach( StudyHour sp in this.checkedListPairs.CheckedItems)						
					isEveryWereFree = isEveryWereFree && (this._shmatr.FlatBusyCheck(dateTimePicker1.Value, int.Parse(sp.MnemoCode), key) == FlatBusyCheckerState.isNotBusy);					

				if (isEveryWereFree)
					if ( !this.selectedFlatList.Items.Contains( _node.Tag ) )
						this.selectedFlatList.Items.Add( _node.Tag );

				if (!isEveryWereFree)
					if( MessageBox.Show(this, Vocabruary.Translate("Message.CanUseFlatText"), Vocabruary.Translate("Message.WarningTitle"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
						if ( !this.selectedFlatList.Items.Contains( _node.Tag ) )
							this.selectedFlatList.Items.Add( _node.Tag );
			}

			
			if (this.selectedFlatList.Items.Count == 0)
				this.button1.Enabled = false;
			else
				this.button1.Enabled = true;

		}

		private void dateTimePicker1_ValueChanged(object sender, System.EventArgs e)
		{
			this.WeekNumber = SheduleKeyBuilder.WeekByDate(this._shmatr, this.dateTimePicker1.Value);
			if(treeFlats.SelectedNode != null)
				if (treeFlats.SelectedNode.Tag is LectureHall)
					this.restrictSheduleView21.Show(treeFlats.SelectedNode.Tag, this._shmatr, this.WeekNumber);
		}
	}
}
