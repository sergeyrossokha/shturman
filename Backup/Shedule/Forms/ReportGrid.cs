using System;
using System.Collections;
using System.ComponentModel;
using System.Reflection;
using System.Threading;
using System.Windows.Forms;
using Shturman.Nestor.DataLists;
using Shturman.Nestor.Interfaces;
using Shturman.Shedule.Objects;
using Shturman.Shedule;
using Shturman.Shedule.Export;
using Shturman.Shedule.Server.Interface;
using Shturman.Shedule.Server.Remoting;

namespace Shturman.Shedule.Forms
{
	/// <summary>
	/// Summary description for SubjectReport.
	/// </summary>
	public class ReportGrid : Form
	{
		private Panel panel1;
		private DataGrid dataGrid1;
		private Button button1;	
		private Button button3;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private Container components = null;
		private System.Windows.Forms.Button exitButton;			

		public ReportGrid()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();			
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ReportGrid));
			this.panel1 = new System.Windows.Forms.Panel();
			this.button1 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.exitButton = new System.Windows.Forms.Button();
			this.dataGrid1 = new System.Windows.Forms.DataGrid();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.button1);
			this.panel1.Controls.Add(this.button3);
			this.panel1.Controls.Add(this.exitButton);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 413);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(680, 32);
			this.panel1.TabIndex = 1;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(128, 5);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(104, 23);
			this.button1.TabIndex = 2;
			this.button1.Text = "������� -> Excel";
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(8, 5);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(104, 23);
			this.button3.TabIndex = 1;
			this.button3.Text = "������� -> Word";
			this.button3.Click += new System.EventHandler(this.button3_Click);
			// 
			// exitButton
			// 
			this.exitButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.exitButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.exitButton.Location = new System.Drawing.Point(600, 5);
			this.exitButton.Name = "exitButton";
			this.exitButton.TabIndex = 0;
			this.exitButton.Text = "�����";
			this.exitButton.Click += new System.EventHandler(this.exitButton_Click);
			// 
			// dataGrid1
			// 
			this.dataGrid1.AllowNavigation = false;
			this.dataGrid1.DataMember = "";
			this.dataGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGrid1.Location = new System.Drawing.Point(0, 0);
			this.dataGrid1.Name = "dataGrid1";
			this.dataGrid1.Size = new System.Drawing.Size(680, 413);
			this.dataGrid1.TabIndex = 2;
			this.dataGrid1.BindingContextChanged += new System.EventHandler(this.dataGrid1_BindingContextChanged);
			// 
			// ReportGrid
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(680, 445);
			this.Controls.Add(this.dataGrid1);
			this.Controls.Add(this.panel1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "ReportGrid";
			this.Text = "��� \"����? ����?\"";
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion


		private void dataGrid1_BindingContextChanged(object sender, EventArgs e)
		{
			DataGrid dg = (DataGrid) sender;
			///������ ������� DataGrid'� ����������� ������
			Type dgt = dg.GetType();
			MethodInfo mi = dgt.GetMethod("ColAutoResize",BindingFlags.NonPublic | BindingFlags.Instance);
			for( int i = dg.FirstVisibleColumn; i< dg.VisibleColumnCount; i++)
			{
				object[] methodArgs = {i};
				mi.Invoke(sender, methodArgs);
			}
		}

		private void button3_Click(object sender, EventArgs e)
		{				
		}

		private void button1_Click(object sender, System.EventArgs e)
		{
		}

		private void exitButton_Click(object sender, System.EventArgs e)
		{
			this.Close();
		}

		public DataGrid GridReport
		{
			get{ return this.dataGrid1; }
		}

		public bool ShowButtons
		{
			get{ return this.button1.Visible; }
			set
			{
				this.button1.Visible = value;
				this.button3.Visible = value;
			}
		}

		public Button ExportToWordButton
		{
			get { return this.button3; }
		}		

		public Button ExportToExcelButton
		{
			get { return this.button1; }
		}
	}
}
