using System;
using System.Collections.Generic;
using System.Text;

using Shturman.Shedule.Server.Remoting;

namespace Shturman.Shedule.Server.Interface
{
    /// <summary>
    /// ��������� ���� �������� ��������� ������������ ����� ��������,
    /// ������������ ������ � ����������� �� ���� �������� ������
    /// </summary>
    public interface ISheduleReference
    {
        /// <summary>
        /// ��������� ��������� �� ������� ������
        /// </summary>
        /// <param name="name">����� ���������</param>
        /// <param name="shedule">������� ������</param>
        void AddReference(string name, object shedule);

        /// <summary>
        /// ��������� ���������
        /// </summary>
        /// <param name="name">����� ���������</param>
        void RemoveReference(string name);

        /// <summary>
        /// ��������� ������ ���� ��������
        /// </summary>
        /// <returns>������ ���� ��������</returns>
        string[] GetReferenceNameList();

        /// <summary>
        /// ��������� ��������
        /// </summary>
        /// <returns>������ �������� �� ���� ��������</returns>
        object[] GelReferenceList();

        /// <summary>
        /// �������� ��������� ���������, �����, �������
        /// </summary>
        /// <param name="objKey">���� ���������, �����, �������</param>
        /// <param name="day">������ ��� �����</param>
        /// <param name="pair">������ ����</param>
        /// <param name="week">������ �����</param>
        /// <param name="message">�����������</param>
        /// <returns>true, ���� �������� ��������� �� false � ������ �������</returns>
        bool CkeckBusy(ExtObjKey objKey, int day, int pair, int week, out string message);

        /// <summary>
        /// �������� ��������� ������ ���������, �����, �������
        /// </summary>
        /// <param name="objKeyList">������ ������ ���������, �����, �������</param>
        /// <param name="day">������ ��� �����</param>
        /// <param name="pair">������ ����</param>
        /// <param name="week">������ �����</param>
        /// <param name="message">�����������</param>
        /// <returns>true, ���� �������� ��������� �� false � ������ �������</returns>
        bool CkeckBusy(List<ExtObjKey> objKeyList, int day, int pair, int week, out string message);

        /// <summary>
        /// �������� ��������� ������ ���������, �����, �������
        /// </summary>
        /// <param name="objKey">���� ���������, �����, �������</param>
        /// <param name="date">������ ��� �����</param>
        /// <param name="pair">������ ����</param>
        /// <param name="message">�����������</param>
        /// <returns>true, ���� �������� ��������� �� false � ������ �������</returns>
        bool CkeckBusy(ExtObjKey objKey, DateTime date, int pair, out string message);

        /// �������� ��������� ������ ���������, �����, �������
        /// </summary>
        /// <param name="objKeyList">������ ������ ���������, �����, �������</param>
        /// <param name="date">������ ��� �����</param>
        /// <param name="pair">������ ����</param>
        /// <param name="week">������ �����</param>
        /// <param name="message">�����������</param>
        /// <returns>true, ���� �������� ��������� �� false � ������ �������</returns>
        bool CkeckBusy(List<ExtObjKey> objKeyList, DateTime date, int pair, out string message);
    }
}
