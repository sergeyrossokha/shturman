using System.Collections;

namespace Shturman.Shedule.Server.Interface
{
	/// <summary>
	/// Summary description for ISheduleServer.
	/// </summary>
	public interface ISheduleServer
	{
		IShedule GetSheduleByName(string sheduleName);
		IList GetAllShedules();
        IList GetAllShedulesInfo();
		bool Save(string sheduleName);
		bool SaveAs(string sheduleName, string newName);
		IShedule NewShedule(string sheduleName);
	}
}
