using System;
using System.Collections;

namespace Shturman.Shedule.Server.Remoting
{
	/// <summary>
	/// Summary description for UUIDConverter.
	/// </summary>
	public class UUIDConverter
	{
		/// <summary>
		///  ����� ��� ��������� ���� � ������ ����� �������
		/// </summary>
		/// <param name="signature">��������� ��`����</param>
		/// <param name="index">������ ��`����</param>
		/// <returns>��� ����� � ������� ����� �������</returns>
		public static string UUIDToString(byte[] signature, long index)
		{
			string str = string.Empty;
			// convert signature
			if(signature != null && index!= -1)
			{
				foreach (byte bit in signature)
					str += bit.ToString() + "-";
				// add index
				str += index.ToString();
			}

			return str;
		}

		/// <summary>
		/// ����� ��� ��������� ����� � ����� �������
		/// </summary>
		/// <param name="uuidString">����� �������, �� ���������� ���� � ������ �����</param>
		/// <param name="signature">��������� ��`����</param>
		/// <param name="index">������ ��`����</param>
		public static void StringToUUID(string uuidString, out byte[] signature, out long index)
		{
			if(uuidString == string.Empty)
			{
				signature = null;
				index = -1;
				return;
			}
			IList lst = new ArrayList();
			string numberStr = "";
			foreach (char ch in uuidString)
			{
				if (Char.IsNumber(ch))
					numberStr += ch;
				else
				{
					lst.Add(byte.Parse(numberStr));
					numberStr = "";
				}
			}
			if (numberStr != null)
				lst.Add(long.Parse(numberStr));
			// Key
			index = (long) lst[lst.Count - 1];
			lst.RemoveAt(lst.Count - 1);
			// Signature
			signature = new byte[lst.Count];
			lst.CopyTo(signature, 0);
		}
	}
}