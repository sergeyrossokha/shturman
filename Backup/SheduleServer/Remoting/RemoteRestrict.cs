using System;

namespace Shturman.Shedule.Server.Remoting
{
	/// <summary>
	/// ��������� ����� �����������
	/// </summary>
	[Serializable]
	public class RemoteRestrict
	{
		public ExtObjKey week_key;
		public ExtObjKey day_key;
		public ExtObjKey hour_key;

		public long week_uid;
		public long day_uid;
		public long hour_uid;

		public int WeekIndex;
		public int DayIndex;
		public int HourIndex;

		public string Text;
	}
}
