using System;
using System.Collections;

namespace Shturman.Shedule.Server.Remoting
{
	/// <summary>
	/// Summary description for StudyLeading.
	/// </summary>
	[Serializable]
	public class RemoteStudyLeading2
	{
		public int uid;

		public ExtObjKey subject_key;
		public ExtObjKey subjectType_key;
		public ExtObjKey department_key;

		public IList listOfGroups;
		public IList listOfTutors;

		public int hourByWeek;
		public int usedHourByWeek;
		public string note;
	}
}
