using System;
using System.Collections;
namespace Shturman.Shedule.Server.Remoting
{
	/// <summary>
	/// Summary description for LessonStruct.
	/// </summary>
	[Serializable]
	public class RemoteLesson: RemLessonBase
	{
		public int leading_uid;
		
		/* ���������� ������ ������ ���� � ��� */
		#region Old fields exists for support old version of shedule file format
		public int week_index;
		public int day_index;
		public int hour_index;
		public long week_uid;
		public long day_uid;
		public long hour_uid;
		public long user_uid = -1;
		#endregion
		
		#region New Fields 
		public ExtObjKey week_key;
		public ExtObjKey day_key;
		public ExtObjKey hour_key;
		public ExtObjKey user_key;
		#endregion
	}
}
