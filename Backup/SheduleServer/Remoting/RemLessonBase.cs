using System;
using System.Collections;

namespace Shturman.Shedule.Server.Remoting
{
	/// <summary>
	/// ��������� ����� "�������" � ������� ��� ����� �����
	/// </summary>
	[Serializable]
	public class RemLessonBase
	{
		/// <summary>
		/// ��� �������� �� ���� ����������� �������
		/// </summary>
		public ExtObjKey subject_key;
		/// <summary>
		/// ��� ���� �������
		/// </summary>
		public ExtObjKey studyType_key;
		
		/// <summary>
		/// ������ ���� ���� � ���� ����������� �������
		/// </summary>
		public IList groupList = new ArrayList();
		/// <summary>
		/// ������ ���� ���������� �� ��������� �������
		/// </summary>
		public IList tutorList = new ArrayList();
		/// <summary>
		/// ������ ���� �������� �� �� ����������� �������
		/// </summary>
		public IList flatList = new ArrayList();
	}
}
