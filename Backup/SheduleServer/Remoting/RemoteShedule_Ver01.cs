using System;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Collections;

using Shturman.Shedule.Server.Interface;

namespace Shturman.Shedule.Server.Remoting
{
	/// <summary>
	/// ������� ���������� �������
	/// </summary>	
	[Serializable]
	public class RemoteShedule_Ver01: MarshalByRefObject//, IShedule
	{
		// ������� �� �������
		private Hashtable lessonsByGroup = new Hashtable();
		// ������� �� ����������
		private Hashtable lessonsByFlat = new Hashtable();
		// ������� �� ��������������
		private Hashtable lessonsByTutor = new Hashtable();

		// ����������� �� �������
		private Hashtable restrictByGroup = new Hashtable();
		// ����������� �� ����������
		private Hashtable restrictByFlat = new Hashtable();
		// ����������� �� ��������������
		private Hashtable restrictByTutor = new Hashtable();

		// �������� �������� ��� ������� ����������
		private Hashtable studyLeading = new Hashtable();

		// ���������� �������
		private Hashtable shedule = new Hashtable();

		// ����� ��������� ������ ����������
		private int weekCyclesCount = 0;
		// ����� ������� ���� � ������
		private int weekDaysCount = 0;
		// ����� ��� � ����
		private int dayPairsCount =0;

		// ��������� ��� ��� ������� ��������
		private int LeadingUidCode = 1;
		
		// ��������� ����������, ������ "���������� 2004/2005 �����"
		private string sheduleTitle;

		// ������ ���������� � ������ ������ ���� �������� �� ������������
		private string version;

		//���� ������ �������� ����������
		private DateTime sheduleBegin;

		//���� ��������� �������� ����������
		private DateTime sheduleEnd;

		private string fileName = "";

		private bool isSaved = false;

		public RemoteShedule_Ver01( string title, int CyclesCount, int DaysCount, int PairsCount): 
			this(CyclesCount, DaysCount, PairsCount)
		{
			sheduleTitle = title;
		}

		public RemoteShedule_Ver01(int CyclesCount, int DaysCount, int PairsCount)
		{			
			weekCyclesCount = CyclesCount;
			weekDaysCount = DaysCount;
			dayPairsCount = PairsCount;
		}

        public object[] GetSheduleInfo()
        {
            return new object[10];
        }

		/// <summary>
		/// ���������� ���-��� ��� �������
		/// </summary>
		/// <param name="weekCycle">��� ���� ������</param>
		/// <param name="weekDay">��� ��� ������</param>
		/// <param name="weekPair">��� ������ �������</param>
		/// <returns>���������� ���-��� �������</returns>
		public int GetKey(int weekCycle, int weekDay, int weekPair)
		{
			return  (weekDay-1)*weekCyclesCount*dayPairsCount+ (weekPair-1)*weekCyclesCount+ (weekCycle-1);
		}


		public int GetStudyLeadingUid()
		{
			return LeadingUidCode++;
		}
		
		#region ����곿 ������ � ��������������
		/// <summary>
		/// ��������� ������������
		/// </summary>
		/// <param name="stLeading">������������ ��� ������� ������</param>
		/// <returns></returns>
		public int AddStudyLeading(RemoteStudyLeading stLeading)
		{
			stLeading.uid = GetStudyLeadingUid();
			
			this.studyLeading.Add(stLeading.uid ,stLeading);			

			this.isSaved = false;

			return stLeading.uid;			
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="leadingUid">��� ������������</param>
		public void DelLessonsByLeading(int leadingUid)
		{
			/* �������� ��������� � ��������� ��������� ������� �� ���������� */
			IList Weeks = new ArrayList();
			IList Days = new ArrayList();
			IList Pairs = new ArrayList();

			// ��������� � �������� ������ ���'������� � ������ �������������	
			IDictionaryEnumerator idea = shedule.GetEnumerator();
			idea.Reset();
			while (idea.MoveNext())
			{
				IDictionaryEnumerator ideb = (idea.Value as Hashtable).GetEnumerator();
				ideb.Reset();
				while (ideb.MoveNext())
				{
					RemoteLesson lsn = ideb.Value as RemoteLesson;
					if (lsn.leading_uid == leadingUid)
					{
						Weeks.Add(lsn.week_index);
						Days.Add(lsn.day_index);
						Pairs.Add(lsn.hour_index);
					}
				}
			}
			if (studyLeading.Contains(leadingUid))
			{
				IList groups = (studyLeading[leadingUid] as RemoteStudyLeading).listOfGroups;

				for(int index=0; index< Weeks.Count; index++ )			                
					this.DeleteByGroup( (int)Weeks[index], (int)Days[index], (int)Pairs[index], groups);
			}
			
		}


		/// <summary>
		/// 
		/// </summary>
		/// <param name="leadingUid"></param>
		/// <param name="checkRight"></param>
		public void DelLessonsByLeading(int leadingUid, CheckRight checkRight)
		{
			/* �������� ��������� � ��������� ��������� ������� �� ���������� */
			IList Weeks = new ArrayList();
			IList Days = new ArrayList();
			IList Pairs = new ArrayList();

			// ��������� � �������� ������ ���'������� � ������ �������������	
			IDictionaryEnumerator idea = shedule.GetEnumerator();
			idea.Reset();
			while (idea.MoveNext())
			{
				IDictionaryEnumerator ideb = (idea.Value as Hashtable).GetEnumerator();
				ideb.Reset();
				while (ideb.MoveNext())
				{
					RemoteLesson lsn = ideb.Value as RemoteLesson;
					
					if (lsn.leading_uid == leadingUid)
					{
						if (checkRight != null)
						{
							if (!checkRight(lsn.user_key)) break;					
						}
						Weeks.Add(lsn.week_index);
						Days.Add(lsn.day_index);
						Pairs.Add(lsn.hour_index);
					}
				}
			}
			if (studyLeading.Contains(leadingUid))
			{
				IList groups = (studyLeading[leadingUid] as RemoteStudyLeading).listOfGroups;

				for(int index=0; index< Weeks.Count; index++ )			                
					this.DeleteByGroup( (int)Weeks[index], (int)Days[index], (int)Pairs[index], groups);
			}
		}

		
		/// <summary>
		/// 
		/// </summary>
		/// <param name="stLeading"></param>
		public void DelStudyLeading(RemoteStudyLeading stLeading)
		{				
			this.DelLessonsByLeading(stLeading.uid);

			this.studyLeading.Remove( stLeading.uid );
			this.isSaved = false;
		}
		
		public RemoteStudyLeading GetStudyLeading(int leading_uid)
		{			
			return this.studyLeading[leading_uid] as RemoteStudyLeading;
		}

		public void ChangeStudyLeading(int leading_uid, RemoteStudyLeading stLeading)
		{
			if (this.studyLeading.Contains(leading_uid))
				this.studyLeading[leading_uid] = stLeading;
		}
		#endregion

		#region ������� ���������, ��������� �� ����������� �������� ������
		public bool CanInsertTo(int weekCycle, int weekDay, int weekPair, RemoteLesson lesson)
		{
			// Create position index in shedule matrix
			int customKey = GetKey(weekCycle, weekDay, weekPair);
			
			// Check ==> Group(s) is FREE 
			bool groupIsFree = (this.GroupBusyCheck(customKey, lesson.groupList) != GroupBusyCheckerState.isBusy);
			
			// Check ==> Tutor(s) is FREE 
			bool tutorIsFree = (this.TutorBusyCheck(customKey, lesson.tutorList) != TutorBusyCheckerState.isBusy);

			// Check ==> All is FREE
			return groupIsFree && tutorIsFree;
		}

		public int SimpleInsert(int weekCycle, int weekDay, int weekPair, RemoteLesson lesson)
		{
			// Create position index in shedule matrix
			int customKey = GetKey(weekCycle, weekDay, weekPair);
			
			// Check ==> Group(s) is FREE 
			bool groupIsFree = (this.GroupBusyCheck(customKey, lesson.groupList) != GroupBusyCheckerState.isBusy);
			
			// Check ==> Tutor(s) is FREE 
			bool tutorIsFree = (TutorBusyCheck(customKey, lesson.tutorList) != TutorBusyCheckerState.isBusy);

			if (customKey == 25)
			{
				int a = 0;
				int b = a*a;
			}
			// Check ==> All is FREE
			bool isFree = groupIsFree && tutorIsFree;
		
			if (!isFree)
			{
				this.GetStudyLeading( lesson.leading_uid ).usedHourByWeek --;
				return -1;
			}

			if (lesson.week_index == -1 || lesson.hour_index == -1 || lesson.day_index == -1)
			{
				// ��-�� �������� � ��������, �.� � ���� ������� ������ ���� �� ��������������
				this.GetStudyLeading( lesson.leading_uid ).usedHourByWeek --;
				return -1;
			}

			// Add to existing lesson additional flatroom 
			if (!groupIsFree && (shedule[customKey] as Hashtable).ContainsKey(lesson.leading_uid))
			{	
				// Insert refference to additional flatroom
				foreach(object obj in lesson.flatList)
					((shedule[customKey] as Hashtable)[lesson.leading_uid] as RemoteLesson).flatList.Add(obj);

				// Insert in shedule by flats refference to lesson
				foreach(ExtObjKey flt_uid in lesson.flatList)
				{
					// if refference list of flats does not exist do create it!
					if (!lessonsByFlat.Contains(flt_uid))
						lessonsByFlat.Add(flt_uid, new Hashtable());

					// Insert refference to refference list	
					(lessonsByFlat[flt_uid] as Hashtable).Add(customKey, (shedule[customKey] as Hashtable)[lesson.leading_uid] as RemoteLesson);
				}
				// Set that shedule is changed and need to be saved 
				isSaved = false;
				// Exit
				return customKey;
			}

			// if shedule doesnt contain refference list do create tham
			if (!shedule.Contains(customKey))
				shedule.Add(customKey, new Hashtable());
			
			// Insert new lesson to a shedule			
			(shedule[customKey] as Hashtable).Add( lesson.leading_uid ,lesson );

			try
			{// insert refference of lesson to refference list of group			
				foreach(ExtObjKey gr_uid in lesson.groupList)
				{
					if (!lessonsByGroup.Contains(gr_uid))
						lessonsByGroup.Add(gr_uid, new Hashtable());
				
					(lessonsByGroup[gr_uid] as Hashtable).Add(customKey, lesson);
				}
			}
			catch
			{
				MessageBox.Show("1");
			}

			try{		// insert refference of lesson to refference list of flatroom
				foreach(ExtObjKey flt_uid in lesson.flatList)
				{
					if (!lessonsByFlat.Contains(flt_uid))
						lessonsByFlat.Add(flt_uid, new Hashtable());
					
					if(!(lessonsByFlat[flt_uid] as Hashtable).Contains(customKey))
						(lessonsByFlat[flt_uid] as Hashtable).Add(customKey, new Hashtable());

					((lessonsByFlat[flt_uid] as Hashtable)[customKey] as Hashtable).Add(lesson.leading_uid, lesson);
				}
			}
			catch
			{
				MessageBox.Show("1");
			}

			try
			{
				// insert refference of lesson to refference list of tutor
				foreach(ExtObjKey ttr_uid in lesson.tutorList)
				{
					if (!lessonsByTutor.Contains(ttr_uid))
						lessonsByTutor.Add(ttr_uid, new Hashtable());
				
					(lessonsByTutor[ttr_uid] as Hashtable).Add(customKey, lesson);
				}
			}
			catch
			{
				MessageBox.Show("1");
			}

			// Set that shedule is changed and need to be saved 
			isSaved = false;

			return customKey;
		}

		public int Insert(int weekCycle, int weekDay, int weekPair, RemoteLesson lesson)
		{
			int key = SimpleInsert(weekCycle, weekDay, weekPair, lesson);
			
			// Change count used hour by lesson
			this.GetStudyLeading( lesson.leading_uid ).usedHourByWeek ++;

			// Exit, by return lesson position index in shedule matrix
			return key;
		}

		public void DeleteByGroup(int weekCycle, int weekDay, int weekPair, IList groupList )
		{
			int lessonKey = this.GetKey(weekCycle, weekDay, weekPair);
			
			foreach (ExtObjKey gr_uid in groupList)
			{
				if (!(lessonsByGroup[gr_uid] as Hashtable).Contains(lessonKey)) continue;
				RemoteLesson lesson = ((lessonsByGroup[gr_uid] as Hashtable)[lessonKey] as RemoteLesson);
				if ((shedule[lessonKey] as Hashtable).Contains( lesson.leading_uid ))
					(shedule[lessonKey] as Hashtable).Remove( lesson.leading_uid );

				foreach(ExtObjKey tr_uid in lesson.tutorList)
					if ((lessonsByTutor[tr_uid] as Hashtable).Contains(lessonKey))
						(lessonsByTutor[tr_uid] as Hashtable).Remove(lessonKey);

				foreach(ExtObjKey lh_uid in lesson.flatList)
					if ((lessonsByFlat[lh_uid] as Hashtable).Contains(lessonKey))
						if(((lessonsByFlat[lh_uid] as Hashtable)[lessonKey] as Hashtable).Contains(lesson.leading_uid))
							((lessonsByFlat[lh_uid] as Hashtable)[lessonKey] as Hashtable).Remove(lesson.leading_uid);
				
				foreach(ExtObjKey grp_uid in lesson.groupList)
					if ((lessonsByGroup[grp_uid] as Hashtable).Contains(lessonKey))
						(lessonsByGroup[grp_uid] as Hashtable).Remove(lessonKey);

				
				RemoteStudyLeading remoteleading = GetStudyLeading(lesson.leading_uid);
				if (remoteleading.usedHourByWeek != 0)
					remoteleading.usedHourByWeek --;
				
				// Set that shedule is changed and need to be saved 
				isSaved = false;

			}
		}
	
		public void DeleteByFlat(int weekCycle, int weekDay, int weekPair, IList flatList )
		{
			// Create position index in shedule matrix
			int lessonKey = this.GetKey(weekCycle, weekDay, weekPair);			
			
			foreach (ExtObjKey lh_uid in flatList)
			{
				if (!(lessonsByFlat[lh_uid] as Hashtable).Contains(lessonKey)) continue;
				
				foreach(RemoteLesson lesson in new ArrayList(((lessonsByFlat[lh_uid] as Hashtable)[lessonKey] as Hashtable).Values))
				{
				
					// �������� �� �������� �������������� ���������
					if (lesson.flatList.Count > 1)
					{
						lesson.flatList.Remove(lh_uid);
						// 
						if ((lessonsByFlat[lh_uid] as Hashtable).Contains(lessonKey))
						{
							(lessonsByFlat[lh_uid] as Hashtable).Remove(lessonKey);
							lesson.flatList.Remove(lh_uid);
							continue;
						}					
					}
                				
					if ((shedule[lessonKey] as Hashtable).Contains( lesson.leading_uid ))
						(shedule[lessonKey] as Hashtable).Remove( lesson.leading_uid );

					foreach(ExtObjKey tr_uid in lesson.tutorList)
						if ((lessonsByTutor[tr_uid] as Hashtable).Contains(lessonKey))
							(lessonsByTutor[tr_uid] as Hashtable).Remove(lessonKey);

					foreach(ExtObjKey gr_uid in lesson.groupList)
						if ((lessonsByGroup[gr_uid] as Hashtable).Contains(lessonKey))
							(lessonsByGroup[gr_uid] as Hashtable).Remove(lessonKey);
					
					if ((lessonsByFlat[lh_uid] as Hashtable).Contains(lessonKey))
						(lessonsByFlat[lh_uid] as Hashtable).Remove(lessonKey);

					GetStudyLeading(lesson.leading_uid).usedHourByWeek --;		
				}
			}
		}

		public void MoveLesson(RemoteLesson lesson, int nweekCycle, int nweekDay, int nweekPair)
		{
			// ��������� �� ��������� ��������� ������� � ���� ����
			if (CanInsertTo(nweekCycle, nweekDay, nweekPair, lesson))
			{
				this.DeleteByGroup(lesson.week_index, lesson.day_index, lesson.hour_index, lesson.groupList);
				lesson.week_index = nweekCycle;
				lesson.day_index = nweekDay;
				lesson.hour_index = nweekPair;
				this.Insert(nweekCycle, nweekDay, nweekPair, lesson);
			}
		}

		public void ChangeLessonFlats(RemoteLesson[] lessonList, ExtObjKey[] flatList)
		{
			foreach(RemoteLesson lsn in lessonList)
			{								
				// Create position index in shedule matrix
				int customKey = GetKey( lsn.week_index, lsn.day_index, lsn.hour_index );

				RemoteLesson existLesson = null;
				if ((shedule[customKey] as Hashtable).Contains( lsn.leading_uid ))
					existLesson = (shedule[customKey] as Hashtable)[ lsn.leading_uid ] as RemoteLesson;				

				if (existLesson == null)
					continue;				

				// ������� ���� �� ������ �������
				foreach(ExtObjKey lh_uid in existLesson.flatList)
				{
					if (lh_uid.Index != -1)
						if ((lessonsByFlat[lh_uid] as Hashtable).Contains(customKey))
							if (((lessonsByFlat[lh_uid] as Hashtable)[customKey] as Hashtable).Contains(existLesson.leading_uid))
							{
								((lessonsByFlat[lh_uid] as Hashtable)[customKey] as Hashtable).Remove(existLesson.leading_uid);								
							}
				}
				existLesson.flatList.Clear();

				// ������ ��� �������
				foreach(ExtObjKey lh_uid in flatList)
				{
					if (!lessonsByFlat.Contains(lh_uid))
						lessonsByFlat.Add(lh_uid, new Hashtable());

					if(!(lessonsByFlat[lh_uid] as Hashtable).Contains(customKey))
						(lessonsByFlat[lh_uid] as Hashtable).Add(customKey, new Hashtable());

					((lessonsByFlat[lh_uid] as Hashtable)[customKey] as Hashtable).Add(existLesson.leading_uid, existLesson);
										
					existLesson.flatList.Add(lh_uid);
				}
				this.isSaved = false;
			}
		}
		#endregion

		#region ��������� �� ��������� �������� ����������, ���� �� ���������
		public int InsertGroupRestrict(int weekCycle, int weekDay, int weekPair, ExtObjKey group_uid, RemoteRestrict restrict)
		{
			this.SetIsSaved( false );
			// Create position index in shedule matrix
			int customKey = GetKey(weekCycle, weekDay, weekPair);

			if(!this.restrictByGroup.ContainsKey(group_uid))
				this.restrictByGroup.Add(group_uid, new Hashtable());

			if(this.restrictByGroup.ContainsKey(group_uid))
				if(!(this.restrictByGroup[group_uid] as Hashtable).ContainsKey(customKey))
					(this.restrictByGroup[group_uid] as Hashtable).Add(customKey, restrict);
 
			return customKey;
		}


		public int InsertTutorRestrict(int weekCycle, int weekDay, int weekPair, ExtObjKey tutor_uid, RemoteRestrict restrict)
		{
			this.SetIsSaved( false );
			// Create position index in shedule matrix
			int customKey = GetKey(weekCycle, weekDay, weekPair);

			if(!this.restrictByTutor.ContainsKey(tutor_uid))
				this.restrictByTutor.Add(tutor_uid, new Hashtable());

			if(this.restrictByTutor.ContainsKey(tutor_uid))
				if(!(this.restrictByTutor[tutor_uid] as Hashtable).ContainsKey(customKey))
					(this.restrictByTutor[tutor_uid] as Hashtable).Add(customKey, restrict);

			return customKey;
		}


		public int InsertFlatRestrict(int weekCycle, int weekDay, int weekPair, ExtObjKey flat_uid, RemoteRestrict restrict)
		{
			this.SetIsSaved( false );
			// Create position index in shedule matrix
			int customKey = GetKey(weekCycle, weekDay, weekPair);

			if(!this.restrictByFlat.ContainsKey(flat_uid))
				this.restrictByFlat.Add(flat_uid, new Hashtable());

			if(this.restrictByFlat.ContainsKey(flat_uid))
				if(!(this.restrictByFlat[flat_uid] as Hashtable).ContainsKey(customKey))
					(this.restrictByFlat[flat_uid] as Hashtable).Add(customKey, restrict);

			return customKey;
		}


		public void DeleteGroupRestrict(int weekCycle, int weekDay, int weekPair, ExtObjKey group_uid)
		{
			this.SetIsSaved( false );
			// Create position index in shedule matrix
			int customKey = GetKey(weekCycle, weekDay, weekPair);

			if(this.restrictByGroup.ContainsKey(group_uid))
				if((this.restrictByGroup[group_uid] as Hashtable).ContainsKey(customKey))
					(this.restrictByGroup[group_uid] as Hashtable).Remove(customKey);
		}


		public void DeleteTutorRestrict(int weekCycle, int weekDay, int weekPair, ExtObjKey tutor_uid)
		{
			this.SetIsSaved( false );
			// Create position index in shedule matrix
			int customKey = GetKey(weekCycle, weekDay, weekPair);

			if(this.restrictByTutor.ContainsKey(tutor_uid))
				if((this.restrictByTutor[tutor_uid] as Hashtable).ContainsKey(customKey))
					(this.restrictByTutor[tutor_uid] as Hashtable).Remove(customKey);
		}


		public void DeleteFlatRestrict(int weekCycle, int weekDay, int weekPair, ExtObjKey flat_uid)
		{
			this.SetIsSaved( false );
			// Create position index in shedule matrix
			int customKey = GetKey(weekCycle, weekDay, weekPair);

			if(this.restrictByFlat.ContainsKey(flat_uid))
				if((this.restrictByFlat[flat_uid] as Hashtable).ContainsKey(customKey))
					(this.restrictByFlat[flat_uid] as Hashtable).Remove(customKey);
		}
		#endregion

		#region ������� �������� ��������� ����, ����������, �� ��������
		/// <summary>
		/// ������� ��� ����������� ��������� ������������� �� ��������,
		/// � ������������ �����
		/// </summary>
		/// <param name="TutorUID">��� �������������</param>
		/// <param name="SheduleKey">��� �������(����, ����, ������)</param>
		/// <returns></returns>
		public bool TutorHasLesson(ExtObjKey TutorUID, int SheduleKey)
		{
			if (this.lessonsByTutor.Contains(TutorUID))
				if ((lessonsByTutor[TutorUID] as Hashtable).Contains(SheduleKey))
					return true;
			return false;
		}

		/// <summary>
		/// ������� ��� ����������� ��������� ������ �� ��������, 
		/// � ������������ �����
		/// </summary>
		/// <param name="GroupUID">��� ������</param>
		/// <param name="SheduleKey">��� �������(����, ����, ������)</param>
		/// <returns></returns>
		public bool GroupHasLesson(ExtObjKey GroupUID, int SheduleKey)
		{
			if (lessonsByGroup.Contains(GroupUID))
				if ((lessonsByGroup[GroupUID] as Hashtable).Contains(SheduleKey))
					return true;
			return false;
		}

		/// <summary>
		/// ������� ��� ����������� ������� ������� � ���������
		/// </summary>
		/// <param name="FlatUID">��� ���������</param>
		/// <param name="SheduleKey">��� �������(����, ����, ������)</param>
		/// <returns>��, ���� ������� ���� � ��� � ��������� ������</returns>
		public bool FlatHasLesson(ExtObjKey FlatUID, int SheduleKey)
		{
			if (lessonsByFlat.Contains(FlatUID))
				if ((lessonsByFlat[FlatUID] as Hashtable).Contains(SheduleKey))
					if (((lessonsByFlat[FlatUID] as Hashtable)[SheduleKey] as Hashtable).Count != 0)
					return true;
			return false;
		}

		/* ������� */

		/// <summary>
		/// ���������� ������ ������ ���������� ������� � ������� � 
		/// ������������� ���� �������
		/// </summary>
		/// <param name="TutorUID">��� �������������</param>
		/// <returns>������ ������</returns>
		public int[] GetKeyTutorLessons(ExtObjKey TutorUID)
		{	
			int[] keys = {};	
			if (this.lessonsByTutor.Contains(TutorUID))
			{			
				keys = new int[(lessonsByTutor[TutorUID] as Hashtable).Keys.Count];
				(lessonsByTutor[TutorUID] as Hashtable).Keys.CopyTo(keys, 0);
			}

			return keys;
		}

		/// <summary>
		/// ������� ��� ����������� ������ ���������� ������� � �������
		/// ������ ������ �� ��������
		/// </summary>
		/// <param name="GroupUID">��� ������</param>
		/// <returns>������ ������ �������(����, ����, ������)</returns>
		public int[] GetKeyGroupLessons(ExtObjKey GroupUID)
		{
			int[] keys = {};
			if (this.lessonsByGroup.Contains(GroupUID))
			{	
				keys = new int[(lessonsByGroup[GroupUID] as Hashtable).Keys.Count];
				(lessonsByGroup[GroupUID] as Hashtable).Keys.CopyTo(keys, 0);
			}
			return keys;
		}

		/// <summary>
		/// ������� ��� ����������� ������ ���������� ������� � �������
		/// ��������� ������ ����������� �������
		/// </summary>
		/// <param name="FlatUID">��� ���������</param>
		/// <returns>������ ������ �������(����, ����, ������)</returns>
		public int[] GetKeyFlatLessons(ExtObjKey FlatUID)
		{
			int[] keys = {};
			if (this.lessonsByFlat.Contains(FlatUID))
			{	
				keys = new int[(lessonsByFlat[FlatUID] as Hashtable).Keys.Count];
				(lessonsByFlat[FlatUID] as Hashtable).Keys.CopyTo(keys, 0);
			}			
			return keys;
		}

		/* ����������� */

		/// <summary>
		/// ���������� ������ ������ ���������� ������� � ������� � 
		/// ������������� ����� �����������
		/// </summary>
		/// <param name="TutorUID">��� �������������</param>
		/// <returns>������ ������</returns>
		public int[] GetKeyTutorRestricts(ExtObjKey TutorUID)
		{	
			int[] keys = {};	
			if (this.restrictByTutor.Contains(TutorUID))
			{			
				keys = new int[(restrictByTutor[TutorUID] as Hashtable).Keys.Count];
				(restrictByTutor[TutorUID] as Hashtable).Keys.CopyTo(keys, 0);
			}

			return keys;
		}

		/// <summary>
		/// ������� ��� ����������� ������ ���������� ������� � �������
		/// ������ ����� �����������
		/// </summary>
		/// <param name="GroupUID">��� ������</param>
		/// <returns>������ ������ �������(����, ����, ������)</returns>
		public int[] GetKeyGroupRestricts(ExtObjKey GroupUID)
		{
			int[] keys = {};
			if (this.restrictByGroup.Contains(GroupUID))
			{	
				keys = new int[(restrictByGroup[GroupUID] as Hashtable).Keys.Count];
				(restrictByGroup[GroupUID] as Hashtable).Keys.CopyTo(keys, 0);
			}
			return keys;
		}

		/// <summary>
		/// ������� ��� ����������� ������ ���������� ������� � �������
		/// ��������� ����� �����������
		/// </summary>
		/// <param name="FlatUID">��� ���������</param>
		/// <returns>������ ������ �������(����, ����, ������)</returns>
		public int[] GetKeyFlatRestricts(ExtObjKey FlatUID)
		{
			int[] keys = {};
			if (this.restrictByFlat.Contains(FlatUID))
			{	
				keys = new int[(restrictByFlat[FlatUID] as Hashtable).Keys.Count];
				(restrictByFlat[FlatUID] as Hashtable).Keys.CopyTo(keys, 0);
			}			
			return keys;
		}

		/* ��������� � ������ ���������� � ����������� */

		/// <summary>
		/// ���������� ������ ������ ���������� ������� � ������� � 
		/// ������������� ����� �����������
		/// </summary>
		/// <param name="TutorUID">��� �������������</param>
		/// <returns>������ ������</returns>
		public int[] GetKeyTutorBusy(ExtObjKey TutorUID)
		{	
			int[] keys = {};
			int[] keysA = {};
			int[] keysB = {};

			if (this.lessonsByTutor.Contains(TutorUID))
			{	
				keysA = new int[(lessonsByTutor[TutorUID] as Hashtable).Keys.Count];
				(restrictByTutor[TutorUID] as Hashtable).Keys.CopyTo(keys, 0);
			}

			if (this.restrictByTutor.Contains(TutorUID))
			{	
				keysB = new int[(restrictByTutor[TutorUID] as Hashtable).Keys.Count];
				(restrictByTutor[TutorUID] as Hashtable).Keys.CopyTo(keys, 0);
			}

			if (keysA.Length>0 || keysB.Length>0)
			{
				keys = new int[keysA.Length+keysB.Length];
				keysA.CopyTo(keys, 0);
				keysB.CopyTo(keys, keysA.Length);
			}

			return keys;
		}

		/// <summary>
		/// ������� ��� ����������� ������ ���������� ������� � �������
		/// ������ ����� �����������
		/// </summary>
		/// <param name="GroupUID">��� ������</param>
		/// <returns>������ ������ �������(����, ����, ������)</returns>
		public int[] GetKeyGroupBusy(ExtObjKey GroupUID)
		{
			int[] keys = {};
			int[] keysA = {};
			int[] keysB = {};

			if (this.lessonsByGroup.Contains(GroupUID))
			{	
				keysA = new int[(lessonsByGroup[GroupUID] as Hashtable).Keys.Count];
				(restrictByGroup[GroupUID] as Hashtable).Keys.CopyTo(keys, 0);
			}

			if (this.restrictByGroup.Contains(GroupUID))
			{	
				keysB = new int[(restrictByGroup[GroupUID] as Hashtable).Keys.Count];
				(restrictByGroup[GroupUID] as Hashtable).Keys.CopyTo(keys, 0);
			}

			if (keysA.Length>0 || keysB.Length>0)
			{
				keys = new int[keysA.Length+keysB.Length];
				keysA.CopyTo(keys, 0);
				keysB.CopyTo(keys, keysA.Length);
			}

			return keys;
		}

		/// <summary>
		/// ������� ��� ����������� ������ ���������� ������� � �������
		/// ��������� ����� �����������
		/// </summary>
		/// <param name="FlatUID">��� ���������</param>
		/// <returns>������ ������ �������(����, ����, ������)</returns>
		public int[] GetKeyFlatBusy(ExtObjKey FlatUID)
		{
			int[] keys = {};
			int[] keysA = {};
			int[] keysB = {};

			if (this.lessonsByFlat.Contains(FlatUID))
			{	
				keysA = new int[(lessonsByFlat[FlatUID] as Hashtable).Keys.Count];
				(lessonsByFlat[FlatUID] as Hashtable).Keys.CopyTo(keys, 0);
			}


			if (this.restrictByFlat.Contains(FlatUID))
			{	
				keysB = new int[(restrictByFlat[FlatUID] as Hashtable).Keys.Count];
				(restrictByFlat[FlatUID] as Hashtable).Keys.CopyTo(keys, 0);
			}

			if (keysA.Length>0 || keysB.Length>0)
			{
				keys = new int[keysA.Length+keysB.Length];
				keysA.CopyTo(keys, 0);
				keysB.CopyTo(keys, keysA.Length);
			}

			return keys;
		}
		#endregion

		public void RemoteGroupLessonAndLeading(ExtObjKey groupUid)
		{	
			// ��������� ������������
			foreach(RemoteStudyLeading remStudyLeading in GetStudyLeadingByGroups(new ExtObjKey[] {groupUid}))
			{
				if (remStudyLeading.listOfGroups.Count == 1)
					this.DelStudyLeading(remStudyLeading);
				else
					remStudyLeading.listOfGroups.Remove(groupUid);
			}

			// ��������� ��������
			this.lessonsByGroup.Remove(groupUid);
		}


		public void CreateGroupWithSimilarLeading(ExtObjKey newGroupKey, ExtObjKey similarGroupKey, ExtObjKey subjectTypeKey)
		{
			IList similarGroupLeadings = GetStudyLeadingByGroups(new ExtObjKey[] {similarGroupKey});
			IList similarGroupLessons = GetGroupLessons( similarGroupKey );

			foreach(RemoteStudyLeading remStudyLeading in similarGroupLeadings)
			{
				if (remStudyLeading.subjectType_key == subjectTypeKey)
				{
					// ����� ����� �� ����� ������������
					remStudyLeading.listOfGroups.Add( newGroupKey );
					// ��������� �������� ������
					foreach(RemoteLesson remLesson in similarGroupLessons)
						if (remLesson.leading_uid == remStudyLeading.uid)
						{
							// ����� ����� �� �������
							remLesson.groupList.Add( newGroupKey );
							// ����� ������� �� �������� �����
							int customKey = this.GetKey(remLesson.week_index, remLesson.day_index, remLesson.hour_index);

							if (!lessonsByGroup.Contains(newGroupKey))
								lessonsByGroup.Add(newGroupKey, new Hashtable());
				
							(lessonsByGroup[newGroupKey] as Hashtable).Add(customKey, remLesson);
						}
				}
				else
				{
					// ���� ������ ������������ �� ����� ���� �� ������
                    RemoteStudyLeading newRemoteStudyLeading = new RemoteStudyLeading();
					newRemoteStudyLeading.department_key = remStudyLeading.department_key;
					newRemoteStudyLeading.hourByWeek = remStudyLeading.hourByWeek;
					newRemoteStudyLeading.listOfTutors = remStudyLeading.listOfTutors;
					
					newRemoteStudyLeading.listOfGroups = new ArrayList();
					newRemoteStudyLeading.listOfGroups.Add( newGroupKey );

                    newRemoteStudyLeading.note = remStudyLeading.note;
					newRemoteStudyLeading.subject_key = remStudyLeading.subject_key;
					newRemoteStudyLeading.subjectType_key = remStudyLeading.subjectType_key;
					newRemoteStudyLeading.usedHourByWeek = 0;

					// ����� ������������
					this.AddStudyLeading( newRemoteStudyLeading );
				}
			}
		}
		
		
		public IList GetAllStudyLeading()
		{
			return new ArrayList( this.studyLeading.Values );
		}

		
		public IList GetStudyLeadingByGroups(ExtObjKey[] groupUidList)
		{
			IList filtered = new ArrayList();
			foreach(RemoteStudyLeading rsl in new ArrayList(this.studyLeading.Values))
			{
				foreach(ExtObjKey group_uid in groupUidList)
					if (rsl.listOfGroups.Contains(group_uid))
					{
						filtered.Add(rsl);
						break;
					}
			}
			return filtered;
		}

		
		public IList GetStudyLeadingByDept(ExtObjKey deptKey)
		{
			IList filtered = new ArrayList();
			
			foreach(RemoteStudyLeading rsl in new ArrayList(this.studyLeading.Values))
				if (rsl.department_key == deptKey) filtered.Add( rsl );

			return filtered;
		}

		
		public IList GetFlatLesson(ExtObjKey flat_uid, int SheduleKey)
		{
			if (FlatHasLesson(flat_uid, SheduleKey))
				return new ArrayList(((this.lessonsByFlat[flat_uid] as Hashtable)[SheduleKey] as Hashtable).Values);
			else
				return null;
		}

		
		public IList GetFlatLessons(ExtObjKey flat_uid)
		{
			if (this.lessonsByFlat.Contains(flat_uid))
				return new ArrayList((this.lessonsByFlat[flat_uid] as Hashtable).Values);
			else
				return new ArrayList();
		}
		
		
		public RemoteLesson GetGroupLesson(ExtObjKey group_uid, int SheduleKey)
		{
			if (GroupHasLesson(group_uid, SheduleKey))
				return (this.lessonsByGroup[group_uid] as Hashtable)[SheduleKey] as RemoteLesson;
			else
				return null;
		}
		
		
		public IList GetGroupLessons(ExtObjKey group_uid)
		{
			if (this.lessonsByGroup.Contains(group_uid))
			{
				Hashtable ht = this.lessonsByGroup[group_uid] as Hashtable;
				ArrayList values = new ArrayList( ht.Values );
				return  values ;
			}
			else
				return new ArrayList();
		}

		
		public RemoteLesson GetTutorLesson(ExtObjKey tutor_uid, int SheduleKey)
		{
			if (TutorHasLesson(tutor_uid, SheduleKey))
				return (this.lessonsByTutor[tutor_uid] as Hashtable)[SheduleKey] as RemoteLesson;
			else
				return null;
		}

		
		public IList GetTutorLessons(ExtObjKey tutor_uid)
		{
			if (this.lessonsByTutor.Contains(tutor_uid))
				return new ArrayList((this.lessonsByTutor[tutor_uid] as Hashtable).Values);
			else
				return new ArrayList();
		}
		
	
		public IList GetTutorRestricts(ExtObjKey tutor_uid)
		{
			if (this.restrictByTutor.Contains(tutor_uid))
				return new ArrayList((this.restrictByTutor[tutor_uid] as Hashtable).Values);
			else
				return new ArrayList();
		}

		
		public bool IsTutorHasRestricts(ExtObjKey tutor_uid, int SheduleKey)
		{
			if (this.restrictByTutor.Contains(tutor_uid))
				if((this.restrictByTutor[tutor_uid] as Hashtable).Contains(SheduleKey))
					return true;
			return false;
		}
		
		
		public string GetTutorRestrictsText(ExtObjKey tutor_uid, int SheduleKey)
		{
			if (IsTutorHasRestricts(tutor_uid, SheduleKey))
				return ((this.restrictByTutor[tutor_uid] as Hashtable)[SheduleKey] as RemoteRestrict).Text;
			else
				return "";
		}
		
		
		public IList GetGroupRestricts(ExtObjKey group_uid)
		{			
			if (this.restrictByGroup.Contains(group_uid))
				return new ArrayList((this.restrictByGroup[group_uid] as Hashtable).Values);
			else
				return new ArrayList();
		}
		
		
		public bool IsGroupHasRestricts(ExtObjKey group_uid, int SheduleKey)
		{			
			if (this.restrictByGroup.Contains(group_uid))
				if((this.restrictByGroup[group_uid] as Hashtable).Contains(SheduleKey))
					return true;
			return false;
		}

		
		public string GetGroupRestrictsText(ExtObjKey group_uid, int SheduleKey)
		{
			if (IsGroupHasRestricts(group_uid, SheduleKey))
				return ((this.restrictByGroup[group_uid] as Hashtable)[SheduleKey] as RemoteRestrict).Text;
			else
				return "";
		}

		
		public IList GetFlatRestricts(ExtObjKey flat_uid)
		{
			if (this.restrictByFlat.Contains(flat_uid))
				return new ArrayList((this.restrictByFlat[flat_uid] as Hashtable).Values);
			else
				return new ArrayList();
		}
		
		
		public bool IsFlatHasRestricts(ExtObjKey flat_uid, int SheduleKey)
		{
			if (this.restrictByFlat.Contains(flat_uid))
				if((this.restrictByFlat[flat_uid] as Hashtable).Contains(SheduleKey))
					return true;
			return false;
		}
		
		
		public string GetFlatRestrictsText(ExtObjKey flat_uid, int SheduleKey)
		{
			if (IsFlatHasRestricts(flat_uid, SheduleKey))
				return ((this.restrictByFlat[flat_uid] as Hashtable)[SheduleKey] as RemoteRestrict).Text;
			else
				return "";
		}

		
		public IList GetLessons(ExtObjKey[] subject_uids, ExtObjKey[] subjectType_uids)
		{
			IList subjectList = new ArrayList(subject_uids);
			IList typeList = new ArrayList(subjectType_uids);
			IList lessons = new ArrayList();
			
			foreach(Hashtable ht in shedule.Values)
				foreach(RemoteLesson rl in ht.Values)
					if (rl.subject_key.UID != -1 && rl.studyType_key.UID != -1)
						if(subjectList.Contains(rl.subject_key.UID) && typeList.Contains(rl.studyType_key))
							lessons.Add(rl);
			return lessons;
		}

		
		public IList GetLessons(int leading_uid)
		{
			IList lessons = new ArrayList();
			
			foreach(Hashtable ht in shedule.Values)
				foreach(RemoteLesson rl in ht.Values)
					if (rl.leading_uid == leading_uid)
						lessons.Add(rl);
			return lessons;
		}

		
		#region Additional Info Methods
		//������� ����, ��������� ��� ��� ����������
		public bool GetIsSaved()
		{
			return isSaved;
		}

		public void SetIsSaved(bool savedFlag)
		{
			isSaved = savedFlag;
		}

		// ��������� ����������
		public string GetSheduleTitle()
		{
			return sheduleTitle;			
		}

		public void SetSheduleTitle(string title)
		{
			sheduleTitle = title;
		}
		
		//��� �����
		public string GetFileName()
		{
		 	return fileName;			
		}
		public void SetFileName(string fileName)
		{
		 	this.fileName = fileName;
		}
		
		//���� ������ �������� ����������
		public DateTime GetSheduleBeginDate()
		{
			return sheduleBegin;			
		}
		public void SetSheduleBeginDate(DateTime beginDate)
		{
			sheduleBegin = beginDate;	
		}

		//���� ��������� �������� ����������
		public DateTime GetSheduleEndDate()
		{		
			return sheduleEnd;			
		}

		public void SetSheduleEndDate(DateTime sheduleEndDate)
		{
			sheduleEnd = sheduleEndDate;
		}
		
		// ����� ��������� ������ ����������
		public int GetWeeksCount()
		{
			return weekCyclesCount;
		}		
		// ����� ������� ���� � ������
		public int GetDaysPerWeekCount()
		{
			return weekDaysCount; 
		}
		// ����� ������� ��� � ����
		public int GetPairsPerDayCount()
		{
			return dayPairsCount;
		}
		#endregion

		#region Open and Save
		public void Save(string filename)
		{
			//fileName = filename;
			//string tempDir = Path.Combine(Path.GetTempPath(), "ShturmanTemp");

			//Path.GetTempPath() 
			string tempFileName = filename +"."+DateTime.Now.Year+DateTime.Now.Month+DateTime.Now.Day+DateTime.Now.Hour+DateTime.Now.Minute+DateTime.Now.Millisecond;
			// ������ �� ��������� ����
			XmlTextWriter xmlwr = new XmlTextWriter(tempFileName, System.Text.Encoding.UTF8);
			try
			{
				xmlwr.WriteStartDocument(true);
				xmlwr.WriteStartElement("Shedule");
				xmlwr.WriteAttributeString("Version", "0.2");
				xmlwr.WriteAttributeString("Title", this.sheduleTitle);
				xmlwr.WriteAttributeString("BeginDate", this.sheduleBegin.Date.ToShortDateString());
				xmlwr.WriteAttributeString("EndDate", this.sheduleEnd.Date.ToShortDateString());


				xmlwr.WriteStartElement("Leadings");

				WriteLeading(xmlwr);

				xmlwr.WriteEndElement();

				xmlwr.WriteStartElement("Lessons");
				//������ ��������� ����������
				IEnumerator enumerator = shedule.Values.GetEnumerator();

				while(enumerator.MoveNext())
				{
					IEnumerator lessonEnumerator = (enumerator.Current as Hashtable).Values.GetEnumerator();
					while(lessonEnumerator.MoveNext())
					{
						RemoteLesson lsn = (lessonEnumerator.Current as RemoteLesson);
						if (lsn != null)
						{
							xmlwr.WriteStartElement("Lesson");

							xmlwr.WriteStartElement("Cycle");
							xmlwr.WriteAttributeString("UID", lsn.week_key.UID.ToString());
							xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString(  lsn.week_key.Signature,  lsn.week_key.Index));
							xmlwr.WriteAttributeString("Index", lsn.week_index.ToString());
							xmlwr.WriteEndElement();

							xmlwr.WriteStartElement("Day");
							xmlwr.WriteAttributeString("UID", lsn.day_key.UID.ToString());
							xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString(  lsn.day_key.Signature,  lsn.day_key.Index));
							xmlwr.WriteAttributeString("Index", lsn.day_index.ToString());
							xmlwr.WriteEndElement();

							xmlwr.WriteStartElement("Pair");
							xmlwr.WriteAttributeString("UID", lsn.hour_key.UID.ToString());
							xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString(  lsn.hour_key.Signature,  lsn.hour_key.Index));
							xmlwr.WriteAttributeString("Index", lsn.hour_index.ToString());

							xmlwr.WriteEndElement();

							xmlwr.WriteStartElement("Subject");
							if( lsn.subject_key.UID != -1)
							{
								xmlwr.WriteAttributeString("UID", lsn.subject_key.UID.ToString());
								xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString(  lsn.subject_key.Signature,  lsn.subject_key.Index));

							}
							xmlwr.WriteEndElement();

							xmlwr.WriteStartElement("SubjectType");
							if(lsn.studyType_key.UID != -1)
							{
								xmlwr.WriteAttributeString("UID", lsn.studyType_key.UID.ToString());
								xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString(  lsn.studyType_key.Signature,  lsn.studyType_key.Index));
							}
							xmlwr.WriteEndElement();

							xmlwr.WriteStartElement("LeadingUid");						
							xmlwr.WriteString(lsn.leading_uid.ToString() );
							xmlwr.WriteEndElement();

							xmlwr.WriteStartElement("UserUid");						
							xmlwr.WriteAttributeString("UID", lsn.user_key.UID.ToString() );
							xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString(  lsn.user_key.Signature,  lsn.user_key.Index));
							xmlwr.WriteEndElement();

							xmlwr.WriteStartElement("Groups");
							foreach(ExtObjKey gr_uid in lsn.groupList)
							{
								if (gr_uid!=null && !gr_uid.IsEmpty)
								{
									xmlwr.WriteStartElement("Group");
									xmlwr.WriteAttributeString("UID", gr_uid.UID.ToString());
									xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString(  gr_uid.Signature,  gr_uid.Index));
									xmlwr.WriteEndElement();
								}
							}
							xmlwr.WriteEndElement();

							xmlwr.WriteStartElement("Flats");
							foreach(ExtObjKey flt_uid in lsn.flatList)
							{
								if (flt_uid != null && !flt_uid.IsEmpty)
								{
									xmlwr.WriteStartElement("Flat");
									xmlwr.WriteAttributeString("UID", flt_uid.UID.ToString());
									xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString(  flt_uid.Signature,  flt_uid.Index));
									xmlwr.WriteEndElement();
								}
							}
							xmlwr.WriteEndElement();

							xmlwr.WriteStartElement("Tutors");
							foreach(ExtObjKey ttr_uid in lsn.tutorList)
							{
								if (ttr_uid != null && !ttr_uid.IsEmpty)
								{
									xmlwr.WriteStartElement("Tutor");
									xmlwr.WriteAttributeString("UID", ttr_uid.UID.ToString());
									xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString(  ttr_uid.Signature,  ttr_uid.Index));
									xmlwr.WriteEndElement();
								}
							}
							xmlwr.WriteEndElement();



							xmlwr.WriteEndElement();
						}
					}
				}
				xmlwr.WriteEndElement();
				

				xmlwr.WriteStartElement("Restricts");

				WriteFlatRestricts(xmlwr);

				WriteGroupRestricts(xmlwr);

				WriteTutorRestricts(xmlwr);

				xmlwr.WriteEndElement(); // restricts

				xmlwr.WriteEndElement();
				xmlwr.WriteEndDocument();
				xmlwr.Flush();
				xmlwr.Close();

				isSaved = true;

			}
			catch
			{
				isSaved = false;
			}

			if (isSaved)
			{
				//string tempFileName1 = Path.GetFileName(filename) +"."+DateTime.Now.Year+DateTime.Now.Month+DateTime.Now.Day+DateTime.Now.Hour+DateTime.Now.Minute+DateTime.Now.Millisecond;
				//string tempDir = Path.Combine(Path.GetTempPath(), tempFileName1);
				//Directory.CreateDirectory(tempDir);
				
				//File.Copy(filename, tempDir+"\\"+tempFileName1,  true);

				//SheduleCompressor.Compress(tempDir+"\\"+Path.GetFileName(filename), tempDir+"\\"+tempFileName1);
				//File.Copy(tempDir+"\\"+Path.GetFileName(filename), filename, true);
				File.Copy(tempFileName, filename, true);
  			}

		}

		
		private void WriteLeading(XmlTextWriter xmlwr)
		{
			foreach (RemoteStudyLeading sl in new ArrayList(this.studyLeading.Values))
			{
				if (sl.uid == 0)
					sl.uid = this.LeadingUidCode++;
			}
	
			xmlwr.WriteAttributeString("GeneratorValue", this.LeadingUidCode.ToString());
	
			IEnumerator leadingenum = this.studyLeading.Values.GetEnumerator();
			while(leadingenum.MoveNext())
			{
				if (leadingenum.Current is RemoteStudyLeading)
				{
					RemoteStudyLeading stload = leadingenum.Current as RemoteStudyLeading;					
					
					xmlwr.WriteStartElement("StudyLeading");
					
					//���
					xmlwr.WriteStartElement("UID");
					xmlwr.WriteString(stload.uid.ToString());
					xmlwr.WriteEndElement();

					//�������
					xmlwr.WriteStartElement("Subject");
					//if(stload.subject_uid != -1)
					//{
					xmlwr.WriteAttributeString("UID", stload.subject_key.UID.ToString());
					xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString( stload.subject_key.Signature, stload.subject_key.Index));
					//}
					xmlwr.WriteEndElement();

					//��� �������
					xmlwr.WriteStartElement("StudyForm");
					xmlwr.WriteAttributeString("UID", stload.subjectType_key.UID.ToString());
					xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString( stload.subjectType_key.Signature, stload.subjectType_key.Index));
					xmlwr.WriteEndElement();

					//�������
					xmlwr.WriteStartElement("Department");
					xmlwr.WriteAttributeString("UID", stload.department_key.UID.ToString());
					xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString( stload.department_key.Signature, stload.department_key.Index));
					xmlwr.WriteEndElement();					

					//�������������� �����
					xmlwr.WriteStartElement("Hour");
					xmlwr.WriteString(stload.hourByWeek.ToString());
					xmlwr.WriteEndElement();

					//������������� �����
					xmlwr.WriteStartElement("UsedHour");
					xmlwr.WriteString(stload.usedHourByWeek.ToString());
					xmlwr.WriteEndElement();

					//����������
					xmlwr.WriteStartElement("Note");
					if (stload.note != string.Empty)
						xmlwr.WriteString( stload.note );
					else
						xmlwr.WriteString("");
					xmlwr.WriteEndElement();

					//������
					xmlwr.WriteStartElement("ForGroups");
					foreach(ExtObjKey gr_key in stload.listOfGroups)
					{
						if (gr_key != null && !gr_key.IsEmpty)
						{
							xmlwr.WriteStartElement("Group");
							xmlwr.WriteAttributeString("UID",gr_key.UID.ToString());
							xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString( gr_key.Signature, gr_key.Index));
							xmlwr.WriteEndElement();
						}
					}
					xmlwr.WriteEndElement();

					//�������������
					xmlwr.WriteStartElement("ForTutors");
					foreach(ExtObjKey tr_key in stload.listOfTutors )
					{
						if (tr_key != null && !tr_key.IsEmpty)
						{
							xmlwr.WriteStartElement("Tutor");
							xmlwr.WriteAttributeString("UID",tr_key.UID.ToString());
							xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString( tr_key.Signature, tr_key.Index));
							xmlwr.WriteEndElement();
						}
					}
					xmlwr.WriteEndElement();

					xmlwr.WriteEndElement();
				}

			}
		}


		public string WriteDeptLeading(ExtObjKey deptKey)
		{
			StringBuilder sb = new StringBuilder("");
			XmlTextWriter xmlwr = new XmlTextWriter( new StringWriter(sb));
			try
			{	
				xmlwr.WriteStartElement("Leadings");
				IEnumerator leadingenum = this.studyLeading.Values.GetEnumerator();
				while(leadingenum.MoveNext())
				{
					if (leadingenum.Current is RemoteStudyLeading)
						if ((leadingenum.Current as RemoteStudyLeading).department_key == deptKey)
					{
						RemoteStudyLeading stload = leadingenum.Current as RemoteStudyLeading;					
					
						xmlwr.WriteStartElement("StudyLeading");
					
						//���
						xmlwr.WriteStartElement("UID");
						xmlwr.WriteString(stload.uid.ToString());
						xmlwr.WriteEndElement();

						//�������
						xmlwr.WriteStartElement("Subject");
						//if(stload.subject_uid != -1)
						//{
						xmlwr.WriteAttributeString("UID", stload.subject_key.UID.ToString());
						xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString( stload.subject_key.Signature, stload.subject_key.Index));

						//}
						xmlwr.WriteEndElement();

						//��� �������
						xmlwr.WriteStartElement("StudyForm");
						xmlwr.WriteAttributeString("UID", stload.subjectType_key.UID.ToString());
						xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString( stload.subjectType_key.Signature, stload.subjectType_key.Index));
						xmlwr.WriteEndElement();

						//�������
						xmlwr.WriteStartElement("Department");
						xmlwr.WriteAttributeString("UID", stload.department_key.UID.ToString());
						xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString( stload.department_key.Signature, stload.department_key.Index));
						xmlwr.WriteEndElement();					

						//�������������� �����
						xmlwr.WriteStartElement("Hour");
						xmlwr.WriteString(stload.hourByWeek.ToString());
						xmlwr.WriteEndElement();

						//������������� �����
						xmlwr.WriteStartElement("UsedHour");
						xmlwr.WriteString(stload.usedHourByWeek.ToString());
						xmlwr.WriteEndElement();

						//����������
						xmlwr.WriteStartElement("Note");
						if (stload.note != string.Empty)
							xmlwr.WriteString( stload.note );
						else
							xmlwr.WriteString("");
						xmlwr.WriteEndElement();

						//������
						xmlwr.WriteStartElement("ForGroups");
						foreach(ExtObjKey gr_key in stload.listOfGroups)
						{
							if (gr_key != null && !gr_key.IsEmpty)
							{
								xmlwr.WriteStartElement("Group");
								xmlwr.WriteAttributeString("UID",gr_key.UID.ToString());
								xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString( gr_key.Signature, gr_key.Index));
								xmlwr.WriteEndElement();
							}
						}
						xmlwr.WriteEndElement();

						//�������������
						xmlwr.WriteStartElement("ForTutors");
						foreach(ExtObjKey tr_key in stload.listOfTutors )
						{
							if (tr_key != null && !tr_key.IsEmpty)
							{
								xmlwr.WriteStartElement("Tutor");
								xmlwr.WriteAttributeString("UID",tr_key.UID.ToString());
								xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString( tr_key.Signature, tr_key.Index));
								xmlwr.WriteEndElement();
							}
						}
						xmlwr.WriteEndElement();

						xmlwr.WriteEndElement();
					}
				}
				xmlwr.WriteEndElement();
			}
			finally
			{
				xmlwr.Flush();
				xmlwr.Close();
			}
			return sb.ToString();
		}

		
		private void WriteTutorRestricts(XmlTextWriter xmlwr)
		{
			#region write tutor restrict
			IEnumerator enumeratorTutor = restrictByTutor.Values.GetEnumerator();
			IEnumerator keyEnumeratorTutor = restrictByTutor.Keys.GetEnumerator();
			xmlwr.WriteStartElement("TutorRestricts");
			while(enumeratorTutor.MoveNext() && keyEnumeratorTutor.MoveNext())
			{		
				IEnumerator restrictEnumerator = (enumeratorTutor.Current as Hashtable).Values.GetEnumerator();
				while(restrictEnumerator.MoveNext())
				{
					xmlwr.WriteStartElement("TutorRestrict");
					xmlwr.WriteAttributeString("TutorUID", keyEnumeratorTutor.Current.ToString());
						
					RemoteRestrict restrict = restrictEnumerator.Current as RemoteRestrict;

					xmlwr.WriteStartElement("Comment");					
					xmlwr.WriteString(restrict.Text);
					xmlwr.WriteEndElement();

					xmlwr.WriteStartElement("Cycle");
					xmlwr.WriteAttributeString("UID", restrict.week_key.UID.ToString());
					xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString(  restrict.week_key.Signature,  restrict.week_key.Index));
					xmlwr.WriteAttributeString("Index", restrict.WeekIndex.ToString());
					xmlwr.WriteEndElement();

					xmlwr.WriteStartElement("Day");
					xmlwr.WriteAttributeString("UID", restrict.day_key.UID.ToString());
					xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString(  restrict.day_key.Signature,  restrict.day_key.Index));
					xmlwr.WriteAttributeString("Index", restrict.DayIndex.ToString());
					xmlwr.WriteEndElement();

					xmlwr.WriteStartElement("Pair");
					xmlwr.WriteAttributeString("UID", restrict.hour_key.UID.ToString());
					xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString(  restrict.hour_key.Signature,  restrict.hour_key.Index));
					xmlwr.WriteAttributeString("Index", restrict.HourIndex.ToString());
					xmlwr.WriteEndElement();

					xmlwr.WriteEndElement();
				}				
			}
			xmlwr.WriteEndElement();
			#endregion			
		}

		
		public string WriteTutorRestricts()
		{
			StringBuilder sb = new StringBuilder("");
			XmlTextWriter xmlwr = new XmlTextWriter( new StringWriter(sb));
			try
			{				
				WriteTutorRestricts(xmlwr);
			}
			finally
			{
				xmlwr.Flush();
				xmlwr.Close();
			}
			return sb.ToString();
		}

		
		private void WriteGroupRestricts(XmlTextWriter xmlwr)
		{
			#region write group restrict
			IEnumerator enumeratorGroup = restrictByGroup.Values.GetEnumerator();
			IEnumerator keyEnumeratorGroup = restrictByGroup.Keys.GetEnumerator();
			xmlwr.WriteStartElement("GroupRestricts");
			while(enumeratorGroup.MoveNext() && keyEnumeratorGroup.MoveNext())
			{
				IEnumerator restrictEnumerator = (enumeratorGroup.Current as Hashtable).Values.GetEnumerator();
				while(restrictEnumerator.MoveNext())
				{
					xmlwr.WriteStartElement("GroupRestrict");
					xmlwr.WriteAttributeString("GroupUID", keyEnumeratorGroup.Current.ToString());
						
					RemoteRestrict restrict = restrictEnumerator.Current as RemoteRestrict;

					xmlwr.WriteStartElement("Comment");					
					xmlwr.WriteString(restrict.Text);
					xmlwr.WriteEndElement();

					xmlwr.WriteStartElement("Cycle");
					xmlwr.WriteAttributeString("UID", restrict.week_key.UID.ToString());
					xmlwr.WriteAttributeString("Index", restrict.WeekIndex.ToString());
					xmlwr.WriteEndElement();

					xmlwr.WriteStartElement("Day");
					xmlwr.WriteAttributeString("UID", restrict.day_key.UID.ToString());
					xmlwr.WriteAttributeString("Index", restrict.DayIndex.ToString());
					xmlwr.WriteEndElement();

					xmlwr.WriteStartElement("Pair");
					xmlwr.WriteAttributeString("UID", restrict.hour_key.UID.ToString());
					xmlwr.WriteAttributeString("Index", restrict.HourIndex.ToString());
					xmlwr.WriteEndElement();

					xmlwr.WriteEndElement();
				}				
			}
			xmlwr.WriteEndElement();
			#endregion
		}

		
		public string WriteGroupRestricts()
		{
			StringBuilder sb = new StringBuilder("");
			XmlTextWriter xmlwr = new XmlTextWriter( new StringWriter( sb ));
			try
			{
				WriteGroupRestricts(xmlwr);				
			}
			finally
			{
				xmlwr.Flush();
				xmlwr.Close();
			}
			return sb.ToString();
		}

		
		private void WriteFlatRestricts(XmlTextWriter xmlwr)
		{
			#region write flat restricts
			IEnumerator enumeratorFlat = restrictByFlat.Values.GetEnumerator();
			IEnumerator keyEnumeratorFlat = restrictByFlat.Keys.GetEnumerator();
			xmlwr.WriteStartElement("FlatRestricts");
			while(enumeratorFlat.MoveNext() && keyEnumeratorFlat.MoveNext())
			{
				IEnumerator restrictEnumerator = (enumeratorFlat.Current as Hashtable).Values.GetEnumerator();
				while(restrictEnumerator.MoveNext())
				{
					xmlwr.WriteStartElement("FlatRestrict");
					xmlwr.WriteAttributeString("FlatUID", keyEnumeratorFlat.Current.ToString());
						
					RemoteRestrict restrict = restrictEnumerator.Current as RemoteRestrict;

					xmlwr.WriteStartElement("Comment");					
					xmlwr.WriteString(restrict.Text);
					xmlwr.WriteEndElement();

					xmlwr.WriteStartElement("Cycle");
					xmlwr.WriteAttributeString("UID", restrict.week_key.UID.ToString());
					xmlwr.WriteAttributeString("Index", restrict.WeekIndex.ToString());
					xmlwr.WriteEndElement();

					xmlwr.WriteStartElement("Day");
					xmlwr.WriteAttributeString("UID", restrict.day_key.UID.ToString());
					xmlwr.WriteAttributeString("Index", restrict.DayIndex.ToString());
					xmlwr.WriteEndElement();

					xmlwr.WriteStartElement("Pair");
					xmlwr.WriteAttributeString("UID", restrict.hour_key.UID.ToString());
					xmlwr.WriteAttributeString("Index", restrict.HourIndex.ToString());
					xmlwr.WriteEndElement();

					xmlwr.WriteEndElement();
				}				
			}
			xmlwr.WriteEndElement();
			#endregion
		}
		
		
		public string WriteFlatRestricts()
		{
			StringBuilder sb = new StringBuilder("");
			XmlTextWriter xmlwr = new XmlTextWriter( new StringWriter( sb));
			try
			{				
				WriteFlatRestricts(xmlwr);				
			}
			finally
			{
				xmlwr.Flush();
				xmlwr.Close();
			}

			return sb.ToString();
		}

		
		public void Open(string filename)
		{
			//fileName = filename;
			isSaved = true;
			XmlTextReader xmlrd = new XmlTextReader(filename);			
			try
			{
				try
				{
					xmlrd.MoveToContent();
					if (xmlrd.HasAttributes)
					{						
						this.fileName = Path.GetFileName(fileName);

						this.version = xmlrd.GetAttribute("Version");						
						this.sheduleTitle = xmlrd.GetAttribute("Title");
						
						if (xmlrd.AttributeCount == 4)
						{	
							this.sheduleBegin = DateTime.Parse(xmlrd.GetAttribute("BeginDate"));
							this.sheduleEnd = DateTime.Parse(xmlrd.GetAttribute("EndDate"));
						}
					}

					while( xmlrd.Read() )					
					{	
						if (xmlrd.NodeType == XmlNodeType.Element && xmlrd.Name == "Leadings")
						{
							this.LeadingUidCode = int.Parse(xmlrd.GetAttribute("GeneratorValue"));
						}

						ReadLeading(xmlrd, false);

						# region Read Lesson
						if (xmlrd.NodeType == XmlNodeType.Element && xmlrd.Name == "Lesson")
						{
							int _leadingUid = 0;

							ExtObjKey _studyCycle = new ExtObjKey(-1);
							int _studyCycleIndex = -1;
							
							ExtObjKey _studyDay = new ExtObjKey(-1);
							int _studyDayIndex = -1;
							
							ExtObjKey _studyHour = new ExtObjKey(-1);
							int _studyHourIndex = -1;
							
							ExtObjKey _studySubject = new ExtObjKey(-1);
							ExtObjKey _studyType = new ExtObjKey(-1);
							
							ExtObjKey _User = new ExtObjKey(-1);

							ArrayList _groupList = new ArrayList();
							ArrayList _flatList = new ArrayList();
							ArrayList _tutorList = new ArrayList();

							while ( xmlrd.Read() )
							{
								if (xmlrd.NodeType == XmlNodeType.Element)
								{
									#region Read Cycle
									if (xmlrd.Name == "Cycle")
									{										
										_studyCycle.UID = long.Parse( xmlrd.GetAttribute("UID") );

										if (xmlrd.AttributeCount == 3)
										{
											string str = xmlrd.GetAttribute("UUID");
											UUIDConverter.StringToUUID(str, out _studyCycle.Signature, out _studyCycle.Index);
										}
										string indexString = xmlrd.GetAttribute("Index");
										if ( indexString != string.Empty && indexString != null)
											_studyCycleIndex = int.Parse( xmlrd.GetAttribute("Index") );
									}
									#endregion

									#region Read Day
									if (xmlrd.Name == "Day")
									{										
										_studyDay.UID = long.Parse( xmlrd.GetAttribute("UID") );

										if (xmlrd.AttributeCount == 3)
											UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out _studyDay.Signature, out _studyDay.Index);

										string indexString = xmlrd.GetAttribute("Index");
										if ( indexString != string.Empty && indexString != null)
											_studyDayIndex = int.Parse( xmlrd.GetAttribute("Index") );
									}
									#endregion

									#region Read Pair
									if (xmlrd.Name == "Pair")
									{
										_studyHour.UID = long.Parse( xmlrd.GetAttribute("UID") );

										if (xmlrd.AttributeCount == 3)
											UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out _studyHour.Signature, out _studyHour.Index);

										string indexString = xmlrd.GetAttribute("Index");
										if ( indexString != string.Empty && indexString != null)
											_studyHourIndex = int.Parse( xmlrd.GetAttribute("Index") );
									}
									#endregion

									#region Read Subject
									if (xmlrd.Name == "Subject")
									{
										_studySubject.UID = long.Parse( xmlrd.GetAttribute("UID") );

										if (xmlrd.AttributeCount == 2)
											UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out _studySubject.Signature, out _studySubject.Index);
									}
									#endregion

									#region Read SubjectType
									if (xmlrd.Name == "SubjectType")
									{
										_studyType.UID = long.Parse( xmlrd.GetAttribute("UID") );

										if (xmlrd.AttributeCount == 2)
											UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out _studyType.Signature, out _studyType.Index);
									}
									#endregion
									
									#region Read LeadingUid
									if (xmlrd.Name == "LeadingUid")
									{	
										xmlrd.Read();

										_leadingUid = int.Parse( xmlrd.Value);
									}
									#endregion

									#region Read UserUid
									if (xmlrd.Name == "UserUid")
									{
										_User.UID = long.Parse( xmlrd.GetAttribute("UID") );

										if (xmlrd.AttributeCount == 2)
											UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out _User.Signature, out _User.Index);

									}
									#endregion

									#region Read ������
									if (xmlrd.Name == "Groups" && !xmlrd.IsEmptyElement)
									{
										while( xmlrd.Read() )					
										{
											if (xmlrd.NodeType == XmlNodeType.Element)
											{
												if (xmlrd.Name == "Group")
												{
													ExtObjKey groupKey = new ExtObjKey(-1);
													
													groupKey.UID = long.Parse( xmlrd.GetAttribute("UID") );
													if (xmlrd.AttributeCount == 2)
														UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out groupKey.Signature, out groupKey.Index);

													if (!_groupList.Contains( groupKey )) _groupList.Add( groupKey );
												}
											}
											if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "Groups")
												break;
										}
									}
									#endregion
									
									#region ���������
									if (xmlrd.Name == "Flats" && !xmlrd.IsEmptyElement)
									{
										while( xmlrd.Read() )					
										{
											if (xmlrd.NodeType == XmlNodeType.Element)
											{
												ExtObjKey flatKey = new ExtObjKey(-1);
												flatKey.UID = long.Parse( xmlrd.GetAttribute("UID") );
												if (xmlrd.AttributeCount == 2)
													UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out flatKey.Signature, out flatKey.Index);

												_flatList.Add( flatKey );
											}
											if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "Flats")
												break;
										}
									}
									#endregion

									#region �������������
									if (xmlrd.Name == "Tutors" && !xmlrd.IsEmptyElement)
									{
										while( xmlrd.Read() )					
										{
											if (xmlrd.NodeType == XmlNodeType.Element)
											{
												ExtObjKey tutorKey = new ExtObjKey(-1);
												
												tutorKey.UID = long.Parse( xmlrd.GetAttribute("UID") );
												if (xmlrd.AttributeCount == 2)
													UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out tutorKey.Signature, out tutorKey.Index);

												_tutorList.Add( tutorKey );
											}
											if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "Tutors")
												break;
										}
									}
									#endregion
								}
								if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "Lesson")
								{
									RemoteLesson newRemoteLesson = new RemoteLesson();
									newRemoteLesson.leading_uid = _leadingUid;
									newRemoteLesson.user_key = _User;
									newRemoteLesson.subject_key = _studySubject;
									newRemoteLesson.studyType_key = _studyType;
									newRemoteLesson.week_key = _studyCycle;
									newRemoteLesson.week_index = _studyCycleIndex;
									newRemoteLesson.day_key= _studyDay;
									newRemoteLesson.day_index = _studyDayIndex;
									newRemoteLesson.hour_key = _studyHour;
									newRemoteLesson.hour_index = _studyHourIndex;
									newRemoteLesson.groupList = _groupList;
									newRemoteLesson.flatList = _flatList;
									newRemoteLesson.tutorList = _tutorList;	
									
									//������� ���������� ����� � ����������
									try
									{
										SimpleInsert( newRemoteLesson.week_index, newRemoteLesson.day_index, newRemoteLesson.hour_index, newRemoteLesson );
										this.GetStudyLeading( newRemoteLesson.leading_uid ).usedHourByWeek ++;
									}
									catch(Exception e)
									{
										MessageBox.Show("������ ������ �����: \n"+e.Message);	
									}
									break;
								}
							}
						}
						#endregion

						ReadRestricts(xmlrd);

					}
				}
				catch(Exception e)
				{
					MessageBox.Show("������ ������ �����: /n"+e.Message);
				}
			}
			finally
			{
				if (xmlrd != null)
					xmlrd.Close();
			}

//			ExtObjKey shedCount = 0;
//			foreach(Hashtable list in this.shedule.Values)
//				if (list != null)
//					shedCount += list.Count;

			isSaved = true;
		}

		

		private void ReadLeading(XmlTextReader xmlrd, bool createNew)
		{
			if (xmlrd.NodeType == XmlNodeType.Element && xmlrd.Name == "StudyLeading")
			{
				int _leading_uid = -1;
				ExtObjKey _subject_uid = new ExtObjKey(-1);
				ExtObjKey _studyType_uid = new ExtObjKey(-1);
				ExtObjKey _department_uid = new ExtObjKey(-1);
				int _hourByWeek = 0;
				int _usedHourByWeek = 0;
				string _note = "";
				IList _groups = new ArrayList();
				IList _tutors = new ArrayList();

				while ( xmlrd.Read() )
				{
					if (xmlrd.NodeType == XmlNodeType.Element)
					{
						#region UID
						if (xmlrd.Name == "UID")
						{	
							xmlrd.Read();
							_leading_uid = int.Parse( xmlrd.Value);
						}
						#endregion

						#region Subject
						if (xmlrd.Name == "Subject")
						{
							_subject_uid.UID = long.Parse( xmlrd.GetAttribute("UID") );

							if (xmlrd.AttributeCount == 2)
								UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out _subject_uid.Signature, out _subject_uid.Index);
						}
						#endregion

						#region StudyForm
						if (xmlrd.Name == "StudyForm")
						{
							_studyType_uid.UID = long.Parse( xmlrd.GetAttribute("UID") );

							if (xmlrd.AttributeCount == 2)
								UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out _studyType_uid.Signature, out _studyType_uid.Index);
						}
						#endregion

						#region Department
						if (xmlrd.Name == "Department")
						{
							_department_uid.UID = long.Parse( xmlrd.GetAttribute("UID") );

							if (xmlrd.AttributeCount == 2)
								UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out _department_uid.Signature, out _department_uid.Index);
						}
						#endregion

						#region Hour
						if (xmlrd.Name == "Hour")
						{	
							xmlrd.Read();

							_hourByWeek = int.Parse( xmlrd.Value);
						}
						#endregion

						#region UsedHour
						if (xmlrd.Name == "UsedHour")
						{	
							xmlrd.Read();

							//_usedHourByWeek = int.Parse( xmlrd.Value);
						}
						#endregion

						#region Note
						if (xmlrd.Name == "Note")
						{	
							xmlrd.Read();

							_note = xmlrd.Value;
						}
						#endregion

						#region Read ������
						if (xmlrd.Name == "ForGroups")
						{
							if (!xmlrd.IsEmptyElement)
							{
								while( xmlrd.Read() )					
								{
									if (xmlrd.NodeType == XmlNodeType.Element)
									{
										if (xmlrd.Name == "Group")
										{
											ExtObjKey  groupUID = new ExtObjKey();								

											groupUID.UID = long.Parse( xmlrd.GetAttribute("UID") );

											if (xmlrd.AttributeCount == 2)
												UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out groupUID.Signature, out groupUID.Index);
											
											if (!_groups.Contains(groupUID))
												_groups.Add(groupUID);
										}
									}
									if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "ForGroups")
										break;
								}
							}
						}
						#endregion

						#region �������������
						if (xmlrd.Name == "ForTutors")
						{
							if (!xmlrd.IsEmptyElement)
							{
								while( xmlrd.Read() )					
								{											
									if (xmlrd.NodeType == XmlNodeType.Element)
									{
										if (xmlrd.Name == "Tutor")
										{	
											ExtObjKey  tutorUID = new ExtObjKey();								

											tutorUID.UID = long.Parse( xmlrd.GetAttribute("UID") );

											if (xmlrd.AttributeCount == 2)
												UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out tutorUID.Signature, out tutorUID.Index);
											
											if (!_tutors.Contains(tutorUID))
												_tutors.Add(tutorUID);
										}												
									}
									if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "ForTutors")
										break;											
								}
							}
						}
						#endregion

					}
					if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "StudyLeading")
					{
						/* ������� ����� �������� */

						RemoteStudyLeading newRemoteStudyLeading = new RemoteStudyLeading();
						newRemoteStudyLeading.uid = _leading_uid;
						newRemoteStudyLeading.subject_key = _subject_uid;
						newRemoteStudyLeading.subjectType_key = _studyType_uid;
						newRemoteStudyLeading.department_key = _department_uid;
						newRemoteStudyLeading.hourByWeek = _hourByWeek;
						newRemoteStudyLeading.usedHourByWeek = _usedHourByWeek;
						newRemoteStudyLeading.note = _note;
						newRemoteStudyLeading.listOfGroups = _groups;
						newRemoteStudyLeading.listOfTutors = _tutors;

						if (createNew)
						{
							newRemoteStudyLeading.uid = this.GetStudyLeadingUid();
							newRemoteStudyLeading.usedHourByWeek = 0;
						}

						studyLeading.Add( newRemoteStudyLeading.uid, newRemoteStudyLeading );
						break;
					}
				}

			}
		}

		
		public void ImportLeading(string xmlString)
		{
			XmlTextReader xmlReader = new XmlTextReader( new StringReader( xmlString ));
			while( xmlReader.Read() )					
			{
				ReadLeading(xmlReader, true);
			}
		}

		private void ReadRestricts(XmlTextReader xmlrd)
		{
			#region Read Flat Restricts
			if (xmlrd.NodeType == XmlNodeType.Element && xmlrd.Name == "FlatRestrict")
			{														
				ExtObjKey studyCycle_uid = new ExtObjKey(-1);
				int studyCycle_index = -1;
				ExtObjKey studyDay_uid = new ExtObjKey(-1);
				int studyDay_index = -1;
				ExtObjKey studyHour_uid = new ExtObjKey(-1);
				int studyHour_index = -1;
				string comment = "";


				#region Read lecture hall
				ExtObjKey flat_uid = new ExtObjKey( long.Parse( xmlrd.GetAttribute("UID") ));

				if (xmlrd.AttributeCount == 2)
					UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out flat_uid.Signature, out flat_uid.Index);
				#endregion

				while ( xmlrd.Read() )
				{
					if (xmlrd.NodeType == XmlNodeType.Element)
					{
						#region Read Comment
						if (xmlrd.Name == "Comment")
						{	
							xmlrd.Read();									
							try
							{
								comment =  xmlrd.Value;
							}
							catch
							{
								//MessageBox.Show(e.Message, "�������", MessageBoxButtons.OK, MessageBoxIcon.Error);
								//throw new Exception(e.Message);
								//while( xmlrd.NodeType != XmlNodeType.Text ) xmlrd.Read();
								//string cycleMnemo = xmlrd.Value;
								//studyCycle = new StudyCycles(cycleMnemo, cycleMnemo);
							}
						}
						#endregion

						#region Read Cycle
						if (xmlrd.Name == "Cycle")
						{										
							studyCycle_uid.UID = long.Parse( xmlrd.GetAttribute("UID") );
							if (xmlrd.AttributeCount == 2)
								UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out studyCycle_uid.Signature, out studyCycle_uid.Index);

							studyCycle_index = int.Parse( xmlrd.GetAttribute("Index") );
						}
						#endregion

						#region Read Day
						if (xmlrd.Name == "Day")
						{
							studyDay_uid.UID = long.Parse( xmlrd.GetAttribute("UID") );
							if (xmlrd.AttributeCount == 2)
								UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out studyDay_uid.Signature, out studyDay_uid.Index);

							studyDay_index = int.Parse( xmlrd.GetAttribute("Index") ) ;
						}
						#endregion

						#region Read Pair
						if (xmlrd.Name == "Pair")
						{
							studyHour_uid.UID = long.Parse( xmlrd.GetAttribute("UID") );
							if (xmlrd.AttributeCount == 2)
								UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out studyHour_uid.Signature, out studyHour_uid.Index);

							studyHour_index = int.Parse( xmlrd.GetAttribute("Index") );
						}
						#endregion
					}
					if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "FlatRestrict")
					{
						RemoteRestrict newRestrict = new RemoteRestrict();									
						newRestrict.day_key = studyDay_uid;
						newRestrict.DayIndex = studyDay_index;
						newRestrict.hour_key = studyHour_uid; 
						newRestrict.HourIndex= studyHour_index; 
						newRestrict.week_key = studyCycle_uid;
						newRestrict.WeekIndex = studyCycle_index;
						newRestrict.Text = comment;

						InsertFlatRestrict(studyCycle_index, studyDay_index, studyHour_index, flat_uid, newRestrict );
						break;
					}
				}
			}
			#endregion
	
			#region Read Group Restricts
			if (xmlrd.NodeType == XmlNodeType.Element && xmlrd.Name == "GroupRestrict")
			{														
				ExtObjKey studyCycle_uid = new ExtObjKey(-1);
				int studyCycle_index = -1;
				ExtObjKey studyDay_uid = new ExtObjKey(-1);
				int studyDay_index = -1;
				ExtObjKey studyHour_uid = new ExtObjKey(-1);
				int studyHour_index = -1;
				string comment = "";

				#region Read group 
				ExtObjKey group_uid = new ExtObjKey(long.Parse( xmlrd.GetAttribute("GroupUID") ));

				if (xmlrd.AttributeCount == 2)
					UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out group_uid.Signature, out group_uid.Index);

				#endregion

				while ( xmlrd.Read() )
				{
					if (xmlrd.NodeType == XmlNodeType.Element)
					{
						#region Read Comment
						if (xmlrd.Name == "Comment")
						{																				
							try
							{
								xmlrd.Read();
								comment =  xmlrd.ReadString();
							}
							catch
							{
								//MessageBox.Show(e.Message, "�������", MessageBoxButtons.OK, MessageBoxIcon.Error);
								//throw new Exception(e.Message);
							}
						}
						#endregion

						#region Read Cycle
						if (xmlrd.Name == "Cycle")
						{
							studyCycle_uid.UID = long.Parse( xmlrd.GetAttribute("UID") );
							if (xmlrd.AttributeCount == 2)
								UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out studyCycle_uid.Signature, out studyCycle_uid.Index);

							studyCycle_index = int.Parse( xmlrd.GetAttribute("Index") );
						}
						#endregion

						#region Read Day
						if (xmlrd.Name == "Day")
						{
							studyDay_uid.UID = long.Parse( xmlrd.GetAttribute("UID") );
							if (xmlrd.AttributeCount == 2)
								UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out studyDay_uid.Signature, out studyDay_uid.Index);

							studyDay_index = int.Parse( xmlrd.GetAttribute("Index") ) ;
						}
						#endregion

						#region Read Pair
						if (xmlrd.Name == "Pair")
						{
							studyHour_uid.UID = long.Parse( xmlrd.GetAttribute("UID") );
							if (xmlrd.AttributeCount == 2)
								UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out studyHour_uid.Signature, out studyHour_uid.Index);

							studyHour_index = int.Parse( xmlrd.GetAttribute("Index") );
						}
						#endregion
					}
					if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "GroupRestrict")
					{
						RemoteRestrict newRestrict = new RemoteRestrict();									
						newRestrict.day_key = studyDay_uid;
						newRestrict.DayIndex = studyDay_index;
						newRestrict.hour_key = studyHour_uid; 
						newRestrict.HourIndex= studyHour_index; 
						newRestrict.week_key = studyCycle_uid;
						newRestrict.WeekIndex = studyCycle_index;
						newRestrict.Text = comment;

						InsertGroupRestrict(studyCycle_index, studyDay_index, studyHour_index, group_uid, newRestrict );
						break;
					}
				}
			}
			#endregion
	
			#region Read Tutor Restricts						
			if (xmlrd.NodeType == XmlNodeType.Element && xmlrd.Name == "TutorRestrict")
			{														
				ExtObjKey studyCycle_uid = new ExtObjKey(-1);
				int studyCycle_index = -1;
				ExtObjKey studyDay_uid = new ExtObjKey(-1);
				int studyDay_index = -1;
				ExtObjKey studyHour_uid = new ExtObjKey(-1);
				int studyHour_index = -1;
				string comment = "";


				#region Read tutor 
				ExtObjKey tutor_uid = new ExtObjKey(long.Parse( xmlrd.GetAttribute("TutorUID") ));
				if (xmlrd.AttributeCount == 2)
					UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out tutor_uid.Signature, out tutor_uid.Index);
				#endregion

				while ( xmlrd.Read() )
				{
					if (xmlrd.NodeType == XmlNodeType.Element)
					{
						#region Read Comment
						if (xmlrd.Name == "Comment")
						{																				
							try
							{
								xmlrd.Read();
								comment =  xmlrd.Value;
							}
							catch
							{
								//MessageBox.Show(e.Message, "�������", MessageBoxButtons.OK, MessageBoxIcon.Error);
								//throw new Exception(e.Message);
							}
						}
						#endregion

						#region Read Cycle
						if (xmlrd.Name == "Cycle")
						{
							studyCycle_uid.UID = long.Parse( xmlrd.GetAttribute("UID") );
							if (xmlrd.AttributeCount == 2)
								UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out studyCycle_uid.Signature, out studyCycle_uid.Index);

							studyCycle_index = int.Parse( xmlrd.GetAttribute("Index") );
						}
						#endregion

						#region Read Day
						if (xmlrd.Name == "Day")
						{
							studyDay_uid.UID = long.Parse( xmlrd.GetAttribute("UID") );
							if (xmlrd.AttributeCount == 2)
								UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out studyDay_uid.Signature, out studyDay_uid.Index);

							studyDay_index = int.Parse( xmlrd.GetAttribute("Index") ) ;
						}
						#endregion

						#region Read Pair
						if (xmlrd.Name == "Pair")
						{
							studyHour_uid.UID = long.Parse( xmlrd.GetAttribute("UID") );
							if (xmlrd.AttributeCount == 2)
								UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out studyHour_uid.Signature, out studyHour_uid.Index);

							studyHour_index = int.Parse( xmlrd.GetAttribute("Index") );
						}
						#endregion
					}
					if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "TutorRestrict")
					{
						RemoteRestrict newRestrict = new RemoteRestrict();									
						newRestrict.day_key = studyDay_uid;
						newRestrict.DayIndex = studyDay_index;
						newRestrict.hour_key = studyHour_uid; 
						newRestrict.HourIndex= studyHour_index; 
						newRestrict.week_key = studyCycle_uid;
						newRestrict.WeekIndex = studyCycle_index;
						newRestrict.Text = comment;
									
						InsertTutorRestrict(studyCycle_index, studyDay_index, studyHour_index, tutor_uid, newRestrict );
						break;
					}
				}
			}
			#endregion
		}

		public void ImportRestricts(string xmlString)
		{
			XmlTextReader xmlReader = new XmlTextReader( new StringReader( xmlString ));
			while( xmlReader.Read() )					
			{
				ReadRestricts(xmlReader);
			}
		}

		#endregion

		#region All Checkers
		/* FlatBusyChecker */
		public bool FlatBusyCheckBool(int sheduleKey, ExtObjKey flat_uid)
		{
			if (this.lessonsByFlat.Contains(flat_uid))
				if ( (lessonsByFlat[flat_uid] as Hashtable).Contains(sheduleKey) )
					if (((lessonsByFlat[flat_uid] as Hashtable)[sheduleKey]as Hashtable).Count!=0)
					return true;
			return false;
		}

		public bool FlatBusyCheckBoolRestrict(int sheduleKey, ExtObjKey flat_uid)
		{
			if (restrictByFlat.Contains(flat_uid))
				if ( (restrictByFlat[flat_uid] as Hashtable).Contains(sheduleKey) )				
					return true;
				else 
					return false;
			else
				return false;
		}

		public bool FlatBusyCheckBool(int sheduleKey, IList flats)
		{
			bool isNotBusy = true;
			foreach(ExtObjKey flt_uid in flats)
				isNotBusy = isNotBusy && !FlatBusyCheckBool(sheduleKey, flt_uid);
			return !isNotBusy;
		}

		public bool FlatBusyCheckBoolRestrict(int sheduleKey, IList flats)
		{
			bool isNotBusy = true;
			foreach(ExtObjKey flt_uid in flats)
				isNotBusy = isNotBusy && !FlatBusyCheckBoolRestrict(sheduleKey, flt_uid);
			return !isNotBusy;
		}

		public FlatBusyCheckerState FlatBusyCheck(int sheduleKey, ExtObjKey flat_uid)
		{
			if ( FlatBusyCheckBool(sheduleKey, flat_uid) )
				return FlatBusyCheckerState.isBusy;
			else
				if ( FlatBusyCheckBoolRestrict(sheduleKey, flat_uid) )
				return FlatBusyCheckerState.isRestrict;
			else
				return FlatBusyCheckerState.isNotBusy;
		}

		public FlatBusyCheckerState FlatBusyCheck(int sheduleKey, IList flats)
		{
			if ( FlatBusyCheckBool(sheduleKey, flats) == true )
				return FlatBusyCheckerState.isBusy;
			else
				if (  FlatBusyCheckBoolRestrict(sheduleKey, flats) )
				return FlatBusyCheckerState.isRestrict;
			else
				return FlatBusyCheckerState.isNotBusy;
		}

		/*GroupBusyChecker*/
		public bool GroupBusyCheckBool(int sheduleKey, ExtObjKey group_uid)
		{
			if ( lessonsByGroup.Contains(group_uid) )
				if ( (lessonsByGroup[group_uid] as Hashtable).Contains(sheduleKey) )
					return true;
				else 
					return false;
			else
				return false;
		}

		public bool GroupBusyCheckBoolRestrict(int sheduleKey, ExtObjKey group_uid)
		{
			if ( restrictByGroup.Contains(group_uid) )
				if ( (restrictByGroup[group_uid] as Hashtable).Contains(sheduleKey) )
					return true;
				else 
					return false;
			else
				return false;
		}

		public bool GroupBusyCheckBool(int sheduleKey, IList groups)
		{
			bool isNotBusy = true;
			foreach(ExtObjKey group_uid in groups)
				isNotBusy = isNotBusy && !GroupBusyCheckBool(sheduleKey, group_uid);
			return !isNotBusy;
		}

		public bool GroupBusyCheckBoolRestrict(int sheduleKey, IList groups)
		{
			bool isNotBusy = true;
			foreach(ExtObjKey group_uid in groups)
				isNotBusy = isNotBusy && !GroupBusyCheckBoolRestrict(sheduleKey, group_uid);
			return !isNotBusy;
		}

		public GroupBusyCheckerState GroupBusyCheck(int sheduleKey, ExtObjKey group_uid)
		{
			if ( GroupBusyCheckBool(sheduleKey, group_uid) )
				return GroupBusyCheckerState.isBusy;
			else
				if ( GroupBusyCheckBoolRestrict(sheduleKey, group_uid) )
				return GroupBusyCheckerState.isRestrict;
			else
				return GroupBusyCheckerState.isNotBusy;
		}

		public GroupBusyCheckerState GroupBusyCheck(int sheduleKey, IList groups)
		{
			if ( GroupBusyCheckBool(sheduleKey, groups) == true )
				return GroupBusyCheckerState.isBusy;
			else
				if ( GroupBusyCheckBoolRestrict(sheduleKey, groups) == true )
				return GroupBusyCheckerState.isRestrict;
			else
				return GroupBusyCheckerState.isNotBusy;
		}

		/*TutorBusyChecker*/
		public bool TutorBusyCheckBool(int sheduleKey, ExtObjKey tutor_uid)
		{
			if ( lessonsByTutor.Contains(tutor_uid) )
				if ( (lessonsByTutor[tutor_uid] as Hashtable).Contains(sheduleKey) )
					return true;
				else 
					return false;
			else
				return false;
		}

		public bool TutorBusyCheckBoolRestrict(int sheduleKey, ExtObjKey tutor_uid)
		{
			if ( restrictByTutor.Contains(tutor_uid) )
				if ( (restrictByTutor[tutor_uid] as Hashtable).Contains(sheduleKey) )
					return true;
				else 
					return false;
			else
				return false;
		}

		public bool TutorBusyCheckBool(int sheduleKey, IList tutors)
		{
			bool isNotBusy = true;
			foreach(ExtObjKey tutor_uid in tutors)
				isNotBusy = isNotBusy && !TutorBusyCheckBool(sheduleKey, tutor_uid);
			return !isNotBusy;
		}
		
		public bool TutorBusyCheckBoolRestrict(int sheduleKey, IList tutors)
		{
			bool isNotBusy = true;
			foreach(ExtObjKey tutor_uid in tutors)
				isNotBusy = isNotBusy && !TutorBusyCheckBoolRestrict(sheduleKey, tutor_uid);
			return !isNotBusy;
		}

		public TutorBusyCheckerState TutorBusyCheck(int sheduleKey, ExtObjKey tutor_uid)
		{
			if ( TutorBusyCheckBool(sheduleKey, tutor_uid) )
				return TutorBusyCheckerState.isBusy;
			else
				if ( TutorBusyCheckBoolRestrict(sheduleKey, tutor_uid) )
				return TutorBusyCheckerState.isRestrict;
			else
				return TutorBusyCheckerState.isNotBusy;
		}

		public TutorBusyCheckerState TutorBusyCheck(int sheduleKey, IList tutors)
		{
			if ( TutorBusyCheckBool( sheduleKey, tutors) == true )
				return TutorBusyCheckerState.isBusy;
			else
				if ( TutorBusyCheckBoolRestrict( sheduleKey, tutors) == true )
				return TutorBusyCheckerState.isRestrict;
			else
				return TutorBusyCheckerState.isNotBusy;
		}

		public bool CheckLessonCountByPair(int sheduleKey, ExtObjKey studyForm, ExtObjKey subject, int limit)
		{
			if (!shedule.Contains(sheduleKey))
				return false;
			IList lst = new ArrayList((shedule[sheduleKey] as Hashtable).Values);
			int entryCount = 0;
			foreach(RemoteLesson remLes in lst)			
				if (remLes.studyType_key == studyForm && remLes.subject_key == subject)
					entryCount++;
			return (entryCount>=limit);
		}

		public bool CheckDeptLessonCountByPair(int sheduleKey, ExtObjKey[] studyForm, ExtObjKey[] department, int limit)
		{
			if (!shedule.Contains(sheduleKey))
				return false;
			IList lst = new ArrayList((shedule[sheduleKey] as Hashtable).Values);
			int entryCount = 0;
			//foreach(RemoteLesson remLes in lst)			
			//	if (remLes.studyType_key == studyForm &&  (studyLeading[remLes.leading_uid] as RemoteStudyLeading).department_key.UID == department.UID)
			//		entryCount++;
			return (entryCount>=limit);
		}

		#endregion

		public override object InitializeLifetimeService()
		{
			return null;
		}
	}
	
}
