using System;
using System.IO;
using System.IO.Compression;

using ICSharpCode.SharpZipLib.Zip;

namespace Shturman.Shedule.Server.Remoting
{
	/// <summary>
	/// Summary description for SheduleCompressor.
	/// </summary>
	public class SheduleCompressor
	{
		static public void Compress(string fileName, string tempFileName)
		{			
			// 'using' statements gaurantee the stream is closed properly which is a big source
			// of problems otherwise.  Its exception safe as well which is great.
			using (ZipOutputStream s = new ZipOutputStream(File.Create(fileName))) 
			{
			
				s.SetLevel(9); // 0 - store only to 9 - means best compression
		
				byte[] buffer = new byte[4096];
				
				// Using GetFileName makes the result compatible with XP
				// as the resulting path is not absolute.
				ZipEntry entry = new ZipEntry(Path.GetFileName(tempFileName));
					
				// Setup the entry data as required.
					
				// Crc and size are handled by the library for seakable streams
				// so no need to do them here.

				// Could also use the last write time or similar for the file.
				entry.DateTime = DateTime.Now;
				s.PutNextEntry(entry);
					
				using ( FileStream fs = File.OpenRead(tempFileName) ) 
				{
					// Using a fixed size buffer here makes no noticeable difference for output
					// but keeps a lid on memory usage.
					int sourceBytes;
					do 
					{
						sourceBytes = fs.Read(buffer, 0, buffer.Length);
						s.Write(buffer, 0, sourceBytes);
					} while ( sourceBytes > 0 );
				}
				
				// Finish/Close arent needed strictly as the using statement does this automatically
				
				// Finish is important to ensure trailing information for a Zip file is appended.  Without this
				// the created file would be invalid.
				s.Finish();
				
				// Close is important to wrap things up and unlock the file.
				s.Close();
			}
		}

		static public void Decompress(string fileName1)
		{
			using (ZipInputStream s = new ZipInputStream(File.OpenRead(fileName1))) 
			{
		
				ZipEntry theEntry;
				while ((theEntry = s.GetNextEntry()) != null) 
				{
				
					string directoryName = Path.GetDirectoryName(theEntry.Name);
					string fileName      = Path.GetFileName(theEntry.Name);
				
					// create directory
					if ( directoryName.Length > 0 ) 
					{
						Directory.CreateDirectory(directoryName);
					}
				
					if (fileName != String.Empty) 
					{
						using (FileStream streamWriter = File.Create(theEntry.Name)) 
						{
					
							int size = 2048;
							byte[] data = new byte[2048];
							while (true) 
							{
								size = s.Read(data, 0, data.Length);
								if (size > 0) 
								{
									streamWriter.Write(data, 0, size);
								} 
								else 
								{
									break;
								}
							}
						}
					}
				}
			}
		}

        static public void MultyCompress(string fileName, string[] tempFileNameList)
        {
            // 'using' statements gaurantee the stream is closed properly which is a big source
            // of problems otherwise.  Its exception safe as well which is great.
            using (ZipOutputStream s = new ZipOutputStream(File.Create(fileName)))
            {

                s.SetLevel(9); // 0 - store only to 9 - means best compression

                byte[] buffer = new byte[4096];

                foreach (string tempFileName in tempFileNameList)
                {
                    // Using GetFileName makes the result compatible with XP
                    // as the resulting path is not absolute.
                    ZipEntry entry = new ZipEntry(Path.GetFileName(tempFileName));

                    // Setup the entry data as required.

                    // Crc and size are handled by the library for seakable streams
                    // so no need to do them here.

                    // Could also use the last write time or similar for the file.
                    entry.DateTime = DateTime.Now;
                    s.PutNextEntry(entry);

                    using (FileStream fs = File.OpenRead(tempFileName))
                    {
                        // Using a fixed size buffer here makes no noticeable difference for output
                        // but keeps a lid on memory usage.
                        int sourceBytes;
                        do
                        {
                            sourceBytes = fs.Read(buffer, 0, buffer.Length);
                            s.Write(buffer, 0, sourceBytes);
                        } while (sourceBytes > 0);
                    }
                }

                // Finish/Close arent needed strictly as the using statement does this automatically

                // Finish is important to ensure trailing information for a Zip file is appended.  Without this
                // the created file would be invalid.
                s.Finish();

                // Close is important to wrap things up and unlock the file.
                s.Close();
            }
        }

        static public void MultyDecompress(string fileName, string toDirectory)
        {
            using (ZipInputStream s = new ZipInputStream(File.OpenRead(fileName)))
            {
                ZipEntry theEntry;
                while ((theEntry = s.GetNextEntry()) != null)
                {

                    string directoryName = toDirectory;
                    string file = Path.GetFileName(theEntry.Name);

                    // create directory
                    if (directoryName.Length > 0 && !Directory.Exists(directoryName))
                    {
                        Directory.CreateDirectory(directoryName);
                    }

                    if (file != String.Empty)
                    {
                        using (FileStream streamWriter = File.Create(Path.Combine(directoryName, theEntry.Name)))
                        {
                            int size = 2048;
                            byte[] data = new byte[2048];
                            while (true)
                            {
                                size = s.Read(data, 0, data.Length);
                                if (size > 0)
                                {
                                    streamWriter.Write(data, 0, size);
                                }
                                else
                                {
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
	}
}
