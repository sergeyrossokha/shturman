using System;
using System.Collections;

namespace Shturman.Shedule.Server.Remoting
{
	/// <summary>
	/// ������������
	/// </summary>
	[Serializable]
	public class RemoteStudyLeading
	{
		/// <summary>
		/// ���������� ��� ������������
		/// </summary>
		public int uid;
		/// <summary>
		/// ��� ���������
		/// </summary>
		public ExtObjKey subject_key;
		/// <summary>
		/// ��� ���� �������
		/// </summary>
		public ExtObjKey subjectType_key;
		/// <summary>
		/// ��� �������
		/// </summary>
		public ExtObjKey department_key;
		/// <summary>
		/// ������ ���� ����
		/// </summary>
		public IList listOfGroups;
		/// <summary>
		/// ������ ���� ����������
		/// </summary>
		public IList listOfTutors;
		/// <summary>
		/// ʳ������ ����� �� �������
		/// </summary>
		public int hourByWeek;
		/// <summary>
		/// ʳ������ ������������� � ������� �����
		/// </summary>
		public int usedHourByWeek;
		/// <summary>
		/// �������
		/// </summary>
		public string note;
	}
}
