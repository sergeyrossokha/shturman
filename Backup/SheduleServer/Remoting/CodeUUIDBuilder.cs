using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace Shturman.Shedule.Server.Remoting
{
    public class CodeUUIDBuilder
    {
        private Dictionary<string, int> _uuid_shortkey_Dictionary = new Dictionary<string, int>();
        
        public CodeUUIDBuilder()
        {
        }

        public string GetShortCode(string uuidString)
        {
            if (_uuid_shortkey_Dictionary.ContainsKey(uuidString))
                return _uuid_shortkey_Dictionary[uuidString].ToString();
            else
            {
                int shortCode = uuidString.GetHashCode();
                _uuid_shortkey_Dictionary.Add(uuidString, shortCode);
                return shortCode.ToString();
            }
        }

        public void WriteShortCodeTable(XmlTextWriter xmlwr)
        {
            xmlwr.WriteStartElement("Keys");
            IEnumerator enumerator = _uuid_shortkey_Dictionary.Keys.GetEnumerator();
            while (enumerator.MoveNext())
            {
                xmlwr.WriteStartElement("Key");
                string key = enumerator.Current as string;
                xmlwr.WriteAttributeString("uuid", key);
                xmlwr.WriteAttributeString("ref", _uuid_shortkey_Dictionary[key].ToString());
                xmlwr.WriteEndElement();
            }
            xmlwr.WriteEndElement();
        }

        public void ReadShortCodeTable(XmlTextReader xmlrd)
        {
            try
            {
                while (xmlrd.Read())
                {
                    if (xmlrd.NodeType == XmlNodeType.Element)
                    {
                        if (xmlrd.Name == "Key")
                        {
                            string key = xmlrd.GetAttribute("uuid");
                            int _ref = int.Parse( xmlrd.GetAttribute("ref"));
                            _uuid_shortkey_Dictionary.Add(key, _ref);
                        }

                    }
                    xmlrd.Close();
                }
            }
            catch
            {
                System.Windows.Forms.MessageBox.Show("������� ��� ������� ����� ������", "�������", System.Windows.Forms.MessageBoxButtons.OK, 
                    System.Windows.Forms.MessageBoxIcon.Error);
            }
        }
    }
}
