using System;
using System.Collections;

namespace Shturman.Nestor.Interfaces
{
	/// <summary>
	/// Summary description for IObjectFactory.
	/// </summary>
	public interface IObjectStorage
	{
		void Open();
		void Close();

		object GetObjectByID(long objectID);
		object GetObjectByUUID(byte[] signature, long uuid);

		long GetID(object customObject);
		long GetUUID(object customObject, out byte[] signature, out long uuid);

		/// <summary>
		/// �������� ������ ������������ �������
		/// </summary>
		/// <param name="objectType">��� ������������ �������</param>
		/// <returns>�����, ��������� ������</returns>
		object New(Type objectType);

		/// <summary>
		/// �������� ������ ������������ �������
		/// </summary>
		/// <param name="objectType">��� ������������ �������</param>
		/// <param name="parent">�������� ������������ �������</param>
		/// <returns>�����, ��������� ������</returns>
		object New(Type objectType, object parent); 

		/// <summary>
		/// ���������� ������� � ��������� ������
		/// </summary>
		/// <param name="storedObject">������ ������� ���������� ���������</param>
		void Save(object storedObject);

		/// <summary>
		/// �������� ������� �� ��������� ������
		/// </summary>
		/// <param name="storedObject">������ ������� ���������� ������� �� ��������� ������</param>
		void Remove(object storedObject);

		/// <summary>
		/// ��������� ������� ��������, ��������� ����
		/// </summary>
		/// <param name="listType">����� ��� ���������� ��������</param>
		/// <returns>������ ��������</returns>
		IList ObjectList(Type listType);

		/// <summary>
		/// 
		/// </summary>
		/// <param name="obj"></param>
		/// <returns></returns>
		IList LikeObjectList(object obj);

		/// <summary>
		/// ��������� ������� ��������, ��������� ����, � �������� ���������
		/// </summary>
		/// <param name="listType">����� ��� ���������� ��������</param>
		/// <param name="parent">������������ ������</param>
		/// <returns>������ ��������</returns>
		IList ObjectList(Type listType, object parent);

		/// <summary>
		/// ������ 
		/// </summary>
		/// <param name="predicate"></param>
		/// <returns></returns>
		IList QueryObjects(object predicate);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
		IList EntityList();
	}
}
