using System.Collections;
using System.Reflection;

/// <summary>
/// 
/// </summary>
public delegate bool EditEventHandler();

/// <summary>
/// 
/// </summary>
public delegate void ChangeEventHandler();

/// <summary>
/// 
/// </summary>
public delegate void DeleteEventHandler();

/// <summary>
/// 
/// </summary>
public delegate IList PropertyValuesEventHandler(PropertyInfo pi);

/// <summary>
/// 
/// </summary>
public delegate void ChangePropertyEventHandler(PropertyInfo pi, object oldvalue, object newValue);

/// <summary>
/// 
/// </summary>
public delegate void CreateEventHandler();

/// <summary>
/// 
/// </summary>
public delegate void DestroyEventHandler();
