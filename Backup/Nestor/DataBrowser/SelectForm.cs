/*�����: ������� ����� �������������
 * ����: ����� ������������� ���������� �� ��������� ����� �� �� ��������
 * 
 * ������������ �� ������������ � ������ ������
 * */
using System;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Drawing;
using Shturman.MultyLanguage;

namespace Shturman.Nestor.DataBrowser
{
	/// <summary>
	/// Summary description for SelectForm.
	/// </summary>
	public class SelectForm : Form
	{
		private Panel panel1;
		private ListBox listBox1;
		private CheckedListBox checkedListBox1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private Container components = null;
		private Button button1;
		private Button button2;
		private System.Windows.Forms.LinkLabel linkCheckAll;
		private System.Windows.Forms.TextBox textSearch;
		private System.Windows.Forms.Panel panel3;

		private bool myltySelectMode = false;

		public SelectForm(string dialogTitle, bool multySelect, IList list)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			this.Text = dialogTitle;

			if (multySelect)
			{
				this.listBox1.Visible = false;
				myltySelectMode = true;

				this.linkCheckAll.Visible = true;

				this.checkedListBox1.Items.Clear();
				//this.listBox1.Sorted = true;

				foreach(object obj in list)
					this.checkedListBox1.Items.Add(obj);
			}
			else
			{
				this.checkedListBox1.Visible = false;
				myltySelectMode = false;
				this.linkCheckAll.Visible = false;

				this.listBox1.Items.Clear();
				//this.listBox1.Sorted = true;

				foreach(object obj in list)
					this.listBox1.Items.Add(obj);
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(SelectForm));
			this.panel1 = new System.Windows.Forms.Panel();
			this.button2 = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.linkCheckAll = new System.Windows.Forms.LinkLabel();
			this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
			this.listBox1 = new System.Windows.Forms.ListBox();
			this.textSearch = new System.Windows.Forms.TextBox();
			this.panel3 = new System.Windows.Forms.Panel();
			this.panel1.SuspendLayout();
			this.panel3.SuspendLayout();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.button2);
			this.panel1.Controls.Add(this.button1);
			this.panel1.Controls.Add(this.linkCheckAll);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 317);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(304, 32);
			this.panel1.TabIndex = 0;
			// 
			// button2
			// 
			this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button2.Location = new System.Drawing.Point(214, 5);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(88, 23);
			this.button2.TabIndex = 1;
			this.button2.Text = "��������";
			// 
			// button1
			// 
			this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button1.Location = new System.Drawing.Point(118, 5);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(88, 23);
			this.button1.TabIndex = 0;
			this.button1.Text = "���������";
			// 
			// linkCheckAll
			// 
			this.linkCheckAll.Location = new System.Drawing.Point(0, 3);
			this.linkCheckAll.Name = "linkCheckAll";
			this.linkCheckAll.Size = new System.Drawing.Size(80, 23);
			this.linkCheckAll.TabIndex = 2;
			this.linkCheckAll.TabStop = true;
			this.linkCheckAll.Text = "³������ ���";
			this.linkCheckAll.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.linkCheckAll.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkCheckAll_LinkClicked);
			// 
			// checkedListBox1
			// 
			this.checkedListBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.checkedListBox1.CheckOnClick = true;
			this.checkedListBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.checkedListBox1.IntegralHeight = false;
			this.checkedListBox1.Location = new System.Drawing.Point(0, 0);
			this.checkedListBox1.Name = "checkedListBox1";
			this.checkedListBox1.Size = new System.Drawing.Size(304, 297);
			this.checkedListBox1.TabIndex = 1;
			this.checkedListBox1.DoubleClick += new System.EventHandler(this.checkedListBox1_DoubleClick);
			// 
			// listBox1
			// 
			this.listBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.listBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.listBox1.HorizontalExtent = 1;
			this.listBox1.IntegralHeight = false;
			this.listBox1.Location = new System.Drawing.Point(0, 0);
			this.listBox1.Name = "listBox1";
			this.listBox1.Size = new System.Drawing.Size(304, 297);
			this.listBox1.TabIndex = 0;
			this.listBox1.DoubleClick += new System.EventHandler(this.checkedListBox1_DoubleClick);
			this.listBox1.DrawItem += new System.Windows.Forms.DrawItemEventHandler(this.listBox1_DrawItem);
			// 
			// textSearch
			// 
			this.textSearch.BackColor = System.Drawing.Color.Gray;
			this.textSearch.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.textSearch.Dock = System.Windows.Forms.DockStyle.Top;
			this.textSearch.Location = new System.Drawing.Point(0, 0);
			this.textSearch.Name = "textSearch";
			this.textSearch.Size = new System.Drawing.Size(304, 20);
			this.textSearch.TabIndex = 2;
			this.textSearch.Text = "����� ������";
			this.textSearch.TextChanged += new System.EventHandler(this.textSearch_TextChanged);
			this.textSearch.Leave += new System.EventHandler(this.textSearch_Leave);
			this.textSearch.Enter += new System.EventHandler(this.textSearch_Enter);
			// 
			// panel3
			// 
			this.panel3.Controls.Add(this.listBox1);
			this.panel3.Controls.Add(this.checkedListBox1);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panel3.Location = new System.Drawing.Point(0, 20);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(304, 297);
			this.panel3.TabIndex = 3;
			// 
			// SelectForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(304, 349);
			this.Controls.Add(this.panel3);
			this.Controls.Add(this.textSearch);
			this.Controls.Add(this.panel1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "SelectForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "SelectForm";
			this.Load += new System.EventHandler(this.SelectForm_Load);
			this.Activated += new System.EventHandler(this.SelectForm_Activated);
			this.Enter += new System.EventHandler(this.SelectForm_Enter);
			this.panel1.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void checkedListBox1_DoubleClick(object sender, EventArgs e)
		{
			if (!myltySelectMode) 
				this.DialogResult = DialogResult.OK;
		}

		private void SelectForm_Load(object sender, EventArgs e)
		{			
			this.button1.Text = Vocabruary.Translate("Button.Apply");
			this.button2.Text = Vocabruary.Translate("Button.Cancel");
		}

		private void linkCheckAll_LinkClicked(object sender, System.Windows.Forms.LinkLabelLinkClickedEventArgs e)
		{
			for(int index=0; index<this.checkedListBox1.Items.Count; index++ )
				this.checkedListBox1.SetItemChecked(index, true);
		}

		private void SelectForm_Activated(object sender, System.EventArgs e)
		{
//			if (myltySelectMode)
//				this.listBox1.SetSelected(1, true);
//			else
//				this.checkedListBox1.SetSelected(1,true);
		}

		private void SelectForm_Enter(object sender, System.EventArgs e)
		{
		}

		private void textSearch_TextChanged(object sender, System.EventArgs e)
		{
			if ((sender as TextBox).Focused)
				if (myltySelectMode)
				{
					int index = this.checkedListBox1.FindString((sender as TextBox).Text, 0);
					if (index != -1)
						this.checkedListBox1.SetSelected(index, true);
				}
				else
				{
					int index = this.listBox1.FindString((sender as TextBox).Text, 0);
					if (index != -1)
						this.listBox1.SetSelected(index, true);
				}
		}

		private void textSearch_Leave(object sender, System.EventArgs e)
		{
			(sender as TextBox).Text = "����� ������";
			(sender as TextBox).BackColor = System.Drawing.Color.Gray;
		}

		private void textSearch_Enter(object sender, System.EventArgs e)
		{
			(sender as TextBox).Text = "";
			(sender as TextBox).BackColor = System.Drawing.Color.White;
		}

		private void listBox1_DrawItem(object sender, System.Windows.Forms.DrawItemEventArgs e)
		{
//			// Set the DrawMode property to draw fixed sized items.
//			listBox1.DrawMode = DrawMode.OwnerDrawFixed;
//			// Draw the background of the ListBox control for each item.
//			e.Graphics.DrawRectangle( Pens.Black, e.Bounds.X, e.Bounds.Y, e.Bounds.Width, e.Bounds.Height);
//			// Create a new Brush and initialize to a Black colored brush by default.
//			Brush myBrush = Brushes.Black;
//
//			// Draw the current item text based on the current Font and the custom brush settings.
//			e.Graphics.DrawString(listBox1.Items[e.Index].ToString(), e.Font, myBrush,e.Bounds,StringFormat.GenericDefault);
//			// If the ListBox has focus, draw a focus rectangle around the selected item.
//			e.DrawFocusRectangle();
		}

		public IList SelectedItems
		{
			get 
			{				
				if (myltySelectMode)
					return 	this.checkedListBox1.CheckedItems;//this.checkedListBox1.SelectedItems;
				else
					return this.listBox1.SelectedItems;
			}
		}

		public object SelectedItem
		{
			get{
				if (myltySelectMode)
					return this.checkedListBox1.SelectedItem;
				else
					return this.listBox1.SelectedItem;
			}
		}

        public IList Items
        {
            get
            {
                if (myltySelectMode)
                    return this.checkedListBox1.Items;
                else
                    return this.listBox1.Items;
            }
        }

        public void SetSelected(object selObject)
        {

            if (myltySelectMode)
            {
                int objectIndex = this.checkedListBox1.Items.IndexOf(selObject);
                this.checkedListBox1.SetItemCheckState(objectIndex, CheckState.Checked);
            }
            else
            {
                int objectIndex = this.listBox1.Items.IndexOf(selObject);
                this.listBox1.SetSelected(objectIndex, true);
            }
        }

        public void SetSelected(int objectIndex)
        {
            if (myltySelectMode)
                this.checkedListBox1.SetItemCheckState(objectIndex, CheckState.Checked);
            else
                this.listBox1.SetSelected(objectIndex, true);
        }
	}
}
