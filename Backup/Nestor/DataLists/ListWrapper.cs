using System;
using System.Collections;
using System.Reflection;
using System.ComponentModel;
using System.Globalization;

using Shturman.Nestor.DataAttributes;
using Shturman.Nestor.DataEditors;

namespace Shturman.Nestor.DataLists
{
	#region internal class ObjectPropertyComparer
	/// <summary>
	/// 
	/// </summary>
	internal class ObjectPropertyComparer : IComparer
	{
		private string mPropertyName;

		/// <summary>
		/// Initialize a new Comparer for a given property.
		/// </summary>
		/// <param name="propertyName">The name of the property to compare.</param>
		internal ObjectPropertyComparer( string propertyName)
		{
			mPropertyName = propertyName;
		}

		/// <summary>
		/// Compare two objects based on the property name passed to the constructor.
		/// </summary>
		/// <param name="x">The first object to compare.</param>
		/// <param name="y">The second object to compare.</param>
		/// <returns>
		/// <div class="tablediv">
		/// <table class="dtTABLE" cellspacing="0">
		///		<tr>
		///			<th width="50%">Value</th>
		///			<th width="50%">Condition</th>
		///		</tr>
		///		<tr>
		///			<td width="50%">Less than zero</td>
		///			<td width="50%"><i>a</i> is less than <i>b</i></td>
		///		</tr>
		///		<tr>
		///			<td width="50%">Zero</td>
		///			<td width="50%"><i>a</i> equals <i>b</i></td>
		///		</tr>
		///		<tr>
		///			<td width="50%">Greater than zero</td>
		///			<td width="50%"><i>a</i> is greater than <i>b</i></td>
		///		</tr>
		/// </table>
		/// </div>
		/// </returns>
		public int Compare(object x, object y)
		{
			if ( x != null && y == null )
			{
				return 1;
			}

			if ( x == null && y != null )
			{
				return -1;
			}

			if ( x == null && y == null )
			{
				return 0;
			}
			
			//if (x is IComparable && y is IComparable)
			//	return (x as IComparable).CompareTo(y);

			Type t = x.GetType();			
			object a = x.GetType().GetProperty(mPropertyName).GetValue(x, null);
			object b = y.GetType().GetProperty(mPropertyName).GetValue(y, null);

			if ( a != null && b == null )
			{
				return 1;
			}

			if ( a == null && b != null )
			{
				return -1;
			}

			if ( a == null && b == null )
			{
				return 0;
			}
			
			/* ��� ����� ���� ���������� ����� ��������� ��� �������������� ������������� ��������*/
			if (t.Name == "Group" && mPropertyName=="MnemoCode")
			{
				// ��������� + ���� 
				string a1 = (a as string).Substring(0, 2) + 
					// �������������
					(a as string).Substring(3).Replace(" ", "") +
					// ����� ������
					(a as string).Substring(2, 1);

				string b1 = (b as string).Substring(0, 2) + 
					// �������������
					(b as string).Substring(3).Replace(" ", "") +
					// ����� ������
					(b as string).Substring(2, 1);

				return ((IComparable)a1).CompareTo(b1);
			}
			
			return ((IComparable)a).CompareTo(b);
		}
	}
	#endregion

	public interface IDataList : IList, ITypedList, IBindingList
	{
		void OnListChanged(ListChangedEventArgs e);
	}

	/// <summary>
	/// Summary description for ListWrapper.
	/// </summary>
	[Serializable]
	public class ListWrapper: IList, ITypedList, IBindingList 
	{
		#region Members
		// Occurs when the list changes or an item in the list changes
		private ListChangedEventHandler mListChanged;
		// Wrapped list
		private IList wrappedList;
		//
		private IList nonFilteredList;
		// The type of the objects in the array
		private Type elementType;
		// Flags, if the current TypedArrayList is sorted
		private bool               mIsSorted          = false;
		// The direction of sorting
		private ListSortDirection  mListSortDirection = ListSortDirection.Ascending;
		// The Descriptor of the property which is the current sort column
		private PropertyDescriptor mSortProperty      = null;
		// bool flag to a filtered collection
		private bool _isFiltered = false;
		
		private ListFiltrator _objectFilter = new ListFiltrator();
        private PropertyDescriptor[] propertyDescriptors;
		#endregion

		#region Constructors 
		public ListWrapper(Type entityType, IList customList)
		{
			this.wrappedList = customList;
            /* Get Descriptors List*/
            PropertyInfo[] piList = entityType.GetProperties();
            ArrayList descriptors = new ArrayList();
            for (int index = 0; index < piList.Length; index++)
            {
                PropertyInfo propertyInfo = piList[index];
                object[] attributes = propertyInfo.GetCustomAttributes(typeof(PropertyVisibleAttribute), true);
                if (attributes.Length == 1)
                {
                    PropertyVisibleAttribute visible = (PropertyVisibleAttribute)attributes[0];
                    if (visible.IsVisible)
                    {
                        descriptors.Add(new PropertyWrapper(propertyInfo.Name, propertyInfo));
                        continue;
                    }
                }
            }
            this.propertyDescriptors = new PropertyDescriptor[descriptors.Count];
            descriptors.CopyTo(propertyDescriptors, 0);
            this.elementType = entityType;
		}
		#endregion

		#region Events

		/// <summary>
		/// 
		/// </summary>
		public event ListChangedEventHandler ListChanged
		{
			add { mListChanged += value; }
			remove { mListChanged -= value; }
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="e"></param>
		public void OnListChanged(ListChangedEventArgs e)
		{
			if(mListChanged != null)
			{
				mListChanged(this, e);				
			}
		}

		#endregion Events

		#region Methods

		/// <summary>
		/// Helper method for the <see cref="Find"/> method. Checks
		/// if the current object meets the search crieria.
		/// </summary>
		/// <param name="data">The current object.</param>
		/// <param name="searchValue">The searched object.</param>
		/// <returns>�<b>True</b>, if the current object meets the search
		/// criteria, <b>false</b> if not.</returns>
		protected bool Match(object data, object searchValue)
		{
			if((data == null) || (searchValue == null))
			{
				return data == searchValue;
			}

			bool isString = data is string;
			
			if(data.GetType() != searchValue.GetType())
				throw new ArgumentException("Objects must be of the same type");

			if((!(data.GetType().IsValueType) || (isString)))
				throw new ArgumentException("Objects must be a value type");

			if(isString)
			{
				string stringData  = ((string)data).ToLower(CultureInfo.CurrentCulture);
				string stringMatch = ((string)searchValue).ToLower(CultureInfo.CurrentCulture);

				return stringData == stringMatch;			
			}		
			else
			{
				return Comparer.Default.Compare(data,searchValue) == 0;			
			}
		}

		#endregion

		#region ITypedList Implementation

		/// <summary>
		/// Implementation of <see cref="System.ComponentModel.ITypedList.GetItemProperties"/>.
		/// </summary>
		/// <remarks>
		/// For each property method of the type contained in the list, a <c>PropertyDescriptor</c>
		/// will be created. If a property is flagged <i>not visible</i>, it will be omitted.
		/// The <c>TypedArrayList</c> uses a custom implementation of the PropertyDescriptor,
		/// </remarks>
		public PropertyDescriptorCollection GetItemProperties(PropertyDescriptor[] listAccessors)
		{
            /*PropertyInfo[] piList = elementType.GetProperties();
			ArrayList descriptors = new ArrayList();

            for (int index=0; index<piList.Length; index++)
			{
                PropertyInfo propertyInfo = piList[index];
				object[] attributes = propertyInfo.GetCustomAttributes(typeof(PropertyVisibleAttribute), true);
				if ( attributes.Length == 1 )
				{
					PropertyVisibleAttribute visible = (PropertyVisibleAttribute) attributes[0];
					if ( visible.IsVisible )
					{
						descriptors.Add( new PropertyWrapper(propertyInfo.Name, propertyInfo) );
						continue;
					}
				}
			}
            
			PropertyDescriptor[] propertyDescriptors = new PropertyDescriptor[descriptors.Count];
			descriptors.CopyTo(propertyDescriptors, 0);
            //Array.Sort(propertyDescriptors, new PropertyGridSortComparer());*/
            
			return new PropertyDescriptorCollection(this.propertyDescriptors);
		}

		/// <summary>
		/// Implementation of <see cref="System.ComponentModel.ITypedList.GetListName"/>.
		/// </summary>
		/// <returns>The name of the type of the objects in the list.</returns>
		public string GetListName(PropertyDescriptor[] listAccessors)
		{
			return this.GetType().Name; //elementType.Name;
		}

		#endregion ITypedList Members

		#region IBindingList Implementation

		/// <summary>
		/// Implementation of <see cref="System.ComponentModel.IBindingList.AddIndex"/>.
		/// </summary>
		public void AddIndex(PropertyDescriptor property)
		{
			mIsSorted = true;
			mSortProperty = property;
		}

		/// <summary>
		/// Implementation of <see cref="System.ComponentModel.IBindingList.AllowNew"/>.
		/// </summary>
		public bool AllowNew
		{
			get	{	return false;	}
		}

		/// <summary>
		/// Implementation of <see cref="System.ComponentModel.IBindingList.ApplySort"/>.
		/// </summary>
		public void ApplySort(PropertyDescriptor property, System.ComponentModel.ListSortDirection direction)
		{
			mIsSorted = true;
			mSortProperty = property;
			mListSortDirection = direction;

			if(property is PropertyWrapper)
			{
				PropertyWrapper prop = (PropertyWrapper)property;
				(wrappedList as ArrayList).Sort( new ObjectPropertyComparer(prop.OriginName) );
				if (direction == ListSortDirection.Descending) (wrappedList as ArrayList).Reverse();			
			}
			else
			{
				(wrappedList as ArrayList).Sort(new ObjectPropertyComparer(property.Name));
				if (direction == ListSortDirection.Descending) (wrappedList as ArrayList).Reverse();
			}
		}

		/// <summary>
		/// Implementation of <see cref="System.ComponentModel.IBindingList.SortProperty"/>.
		/// </summary>
		public PropertyDescriptor SortProperty
		{
			get	{	return mSortProperty;	}
		}

		/// <summary>
		/// Implementation of <see cref="System.ComponentModel.IBindingList.Find"/>.
		/// </summary>
		public int Find(PropertyDescriptor property, object key)
		{
			foreach(object o in this)
			{
				if (Match(elementType.GetProperty(property.Name).GetValue(o,null) , key)) 
					return this.IndexOf(o);
			}
			return -1;
		}

		/// <summary>
		/// Implementation of <see cref="System.ComponentModel.IBindingList.SupportsSorting"/>.
		/// </summary>
		public bool SupportsSorting
		{
			get	{	return true;	}
		}

		/// <summary>
		/// Implementation of <see cref="System.ComponentModel.IBindingList.IsSorted"/>.
		/// </summary>
		public bool IsSorted
		{
			get	{	return mIsSorted;	}
		}

		/// <summary>
		/// Implementation of <see cref="System.ComponentModel.IBindingList.AllowRemove"/>.
		/// </summary>
		public bool AllowRemove
		{
			get	{	return false;	}
		}

		/// <summary>
		/// Implementation of <see cref="System.ComponentModel.IBindingList.SupportsSearching"/>.
		/// </summary>
		public bool SupportsSearching
		{
			get	{	return true;	}
		}

		/// <summary>
		/// Implementation of <see cref="System.ComponentModel.IBindingList.SortDirection"/>.
		/// </summary>
		public ListSortDirection SortDirection
		{
			get	{	return mListSortDirection; }
		} 

		/// <summary>
		/// Implementation of <see cref="System.ComponentModel.IBindingList.SupportsChangeNotification"/>.
		/// </summary>
		public bool SupportsChangeNotification
		{
			get	{	return true; }
		}

		/// <summary>
		/// Implementation of <see cref="System.ComponentModel.IBindingList.RemoveSort"/>.
		/// </summary>
		public void RemoveSort()
		{
			mIsSorted = false;
			mSortProperty = null;
		}

		/// <summary>
		/// Implementation of <see cref="System.ComponentModel.IBindingList.AddNew"/>.
		/// </summary>
		/// <remarks>
		/// The implementation uses reflection to instantiate an object of the current 
		/// object type. If the types contained in the list do not implement the IBindable
		/// interface, adding new objects to the list will not work properly.
		/// </remarks>
		public object AddNew()
		{
			object o = elementType.InvokeMember(null, 
				BindingFlags.DeclaredOnly | BindingFlags.Public | BindingFlags.NonPublic | 
				BindingFlags.Instance | BindingFlags.CreateInstance, 
				null, null, null);
			EventInfo evt = elementType.GetEvent("RemoveObject", BindingFlags.Instance | BindingFlags.Public);
			if(evt != null)
			{
				Delegate deleg = Delegate.CreateDelegate(evt.EventHandlerType, this, "RemoveChild");
				evt.AddEventHandler(o, deleg);
			}
			//base.OnValidate(o);
			//this.OnInsert(base.Count, o);
			int index = wrappedList.Add(o);
			try
			{
				//this.OnInsertComplete(index, o);				
			}
			catch(Exception e)
			{
				this.RemoveAt(index);
				throw e;
			}
			return o;
		}

		/// <summary>
		/// Implementation of <see cref="System.ComponentModel.IBindingList.AllowEdit"/>.
		/// </summary>
		public bool AllowEdit
		{
			get	{	return false;	}
		}

		/// <summary>
		/// Implementation of <see cref="System.ComponentModel.IBindingList.RemoveIndex"/>.
		/// </summary>
		public void RemoveIndex(PropertyDescriptor property)
		{
			mSortProperty = null;
		}
		#endregion

		#region IList Implementation
		public bool IsReadOnly
		{
			get {return wrappedList.IsReadOnly;}
		}

		public object this[int index]
		{
			get { 
					if (index != -1)
						return  wrappedList[index] ;
					else
						return null;
				}
			set {
				if(value==null ||!value.GetType().Equals(elementType))
				{
					//throw new InvalidTypeException(elementType, value); 
				}
				wrappedList[index] = value;
				OnListChanged(new ListChangedEventArgs(ListChangedType.ItemChanged, index, 0));
			}
		}

		public void Insert(int index, object value)
		{
			// ������� � ������� � ������������� ������
			wrappedList.Insert(index, value);
			// ���������� ������ � ������������� ������( � ����� )
			if (isFiltered) nonFilteredList.Add(value);
			// ��������� ����� ������
			OnListChanged(new ListChangedEventArgs(ListChangedType.ItemAdded, index, 0));
		}

		public void Remove(object value)
		{
			// ������ ������� � ������
			int index = wrappedList.IndexOf(value);
			// ������� �� ���������������� ������
			if (isFiltered) nonFilteredList.Remove(value);
			// ������� �� �������������� ������
			wrappedList.RemoveAt(index);
			// ��������� ����� ������
			OnListChanged(new ListChangedEventArgs(ListChangedType.ItemDeleted, index, 0));
		}

		public void RemoveAt(int index)
		{
			// ������� ������ �� �� ���������������� ������
			if (isFiltered)
				nonFilteredList.Remove(wrappedList[index]);
			// ������� ������ �� �������������� ������
			wrappedList.RemoveAt(index);
			// ��������� ����� ������
			OnListChanged(new ListChangedEventArgs(ListChangedType.ItemDeleted, index, 0));
		}

		public bool Contains(object value)
		{
			if (isFiltered) return nonFilteredList.Contains(value);
			else return wrappedList.Contains(value);
		}

		public void Clear()
		{
			wrappedList.Clear();
			if (isFiltered) nonFilteredList.Clear();
			OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, 0));
		}

		public int IndexOf(object value)
		{
			return wrappedList.IndexOf(value);
		}

		public int Add(object value)
		{
			if(value == null || !(value.GetType()).Equals(elementType))
			{
				//throw new System.InvalidTypeException(elementType, value);  
			}
			int index = -1;
			if (isFiltered)
				if (_objectFilter.FilterObject(value))
				{
					index = wrappedList.Add(value);	
					nonFilteredList.Add(value);
				}
				else
				{
					nonFilteredList.Add(value);
				}
			else
			{
				index = wrappedList.Add(value);
			}
			
			//int index = wrappedList.Add(value);
			OnListChanged(new ListChangedEventArgs(ListChangedType.ItemAdded, index, 0));
			return index;
		}

		public bool IsFixedSize
		{
			get { return wrappedList.IsFixedSize; }
		}

		public bool IsSynchronized
		{
			get { return wrappedList.IsSynchronized; }
		}

		public void CopyTo(Array array, int index)
		{
			wrappedList.CopyTo(array, index);
		}

		public object SyncRoot
		{
			get { return wrappedList.SyncRoot; }
		}

		public int Count
		{
			get { return wrappedList.Count; }
		}

		public IEnumerator GetEnumerator()
		{
			return wrappedList.GetEnumerator();
		}

		public void Sort()
		{
			if (this.IsSorted)
				this.ApplySort(mSortProperty, mListSortDirection);
			else
				(wrappedList as ArrayList).Sort(new ObjectPropertyComparer((elementType).GetProperties()[0].Name));
		}

		public void Sort(int index)		
		{
			PropertyInfo[] pi = (elementType).GetProperties();
			if (pi.Length > index)
				(wrappedList as ArrayList).Sort(new ObjectPropertyComparer(pi[index].Name));
		}

		public void Reverse()
		{
			(wrappedList as ArrayList).Sort(new ObjectPropertyComparer((elementType).GetProperties()[0].Name));
			(wrappedList as ArrayList).Reverse();
		}

		#endregion

		public bool isFiltered
		{
			get { return _isFiltered; }
			set 
			{ 	
				_isFiltered = value;

				if (_objectFilter.LogicExpessions.Count == 0)
					_isFiltered = false;
			 
				if (_isFiltered ) 
					filterlist();
				else	
					this.wrappedList = this.nonFilteredList;

				this.OnListChanged(new System.ComponentModel.ListChangedEventArgs(System.ComponentModel.ListChangedType.Reset, -1));				
			}
		}

		public ListFiltrator objectFilter
		{
			get { return _objectFilter; }
		}

		private void filterlist()
		{	
			if (_objectFilter.LogicExpessions.Count != 0)
			{
				this.nonFilteredList = this.wrappedList;
				this.wrappedList = _objectFilter.Filter( this.nonFilteredList );			
			}
		}

		public IList InternalList
		{
			get { return this.wrappedList; }
		}
	}
}
