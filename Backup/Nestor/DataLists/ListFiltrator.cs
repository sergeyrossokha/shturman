using System;
using System.Collections;
using System.Text.RegularExpressions;

using System.Reflection;

namespace Shturman.Nestor.DataLists
{
	public enum LogicOperations
	{
		loEMPTY,
		loAND,
		loOR
	}

	public enum ConditionOperators
	{
		coEqual, // ==
		coNotEqual, //!=
		coLess, // <
		coLessOrEqual, //<=
		coGreater,// >
		coGreatOrEqual,//>=
		coIn, //����� ������� y in [1,2,3,5]
		coInclude //��������, �.�. ������ y �������� 3
	}

	internal struct LogicExpressionItem
	{
		public LogicOperations logicOperation;
		public string PropertyCaption;
		public string PropertyName;
		public ConditionOperators conditionOperator;
		public object PropertyValue;

		public override string ToString()
		{
			string str = PropertyCaption+" ";
			
			switch(conditionOperator)
			{
				case ConditionOperators.coEqual:
					str += "����� ";
					break;
				case ConditionOperators.coNotEqual:
					str += "�� ����� ";
					break;
				case ConditionOperators.coGreater:
					str += "������ ";
					break;
				case ConditionOperators.coGreatOrEqual:
					str += "������ ��� ����� ";
					break;
				case ConditionOperators.coLess:
					str += "������ ";
					break;
				case ConditionOperators.coLessOrEqual:
					str += "������ ��� ����� ";
					break;
				case ConditionOperators.coInclude:
				    str += "�������� ";
					break;
			};
			str += PropertyValue.ToString(); 
			return str;
		}

	}

	/// <summary>
	/// Summary description for ListFiltrator.
	/// </summary>
	public class ListFiltrator
	{
		private IList LogicExpression = new ArrayList();

		public ListFiltrator()
		{}

		public void AddCondition(LogicOperations logicOperation, string propertyName, string propertyCaption, ConditionOperators conditionOperator, object propertyValue)
		{
			LogicExpressionItem lei = new LogicExpressionItem();
			lei.logicOperation = logicOperation;
			lei.PropertyName = propertyName;
			lei.PropertyCaption = propertyCaption;
			lei.conditionOperator = conditionOperator;
			lei.PropertyValue = propertyValue;

			if (LogicExpression.Count == 0)
			{
				lei.logicOperation = LogicOperations.loEMPTY;
			}
			LogicExpression.Add(lei);
		}

		public void Clear()
		{
			LogicExpression.Clear();
		}

		public bool FilterObject(object obj)
		{			
			bool filterOk = true;
			Type objectType = obj.GetType();
			foreach (LogicExpressionItem lei in LogicExpression)
			{
				bool conditionOk = true;

				PropertyInfo pi = objectType.GetProperty(lei.PropertyName);
				object propVal = pi.GetValue(obj, null);

				if (propVal != null)
					switch(lei.conditionOperator)
					{
						case ConditionOperators.coEqual:
						{
							if (propVal is string)
							{
								Regex r = new Regex(lei.PropertyValue.ToString());
								Match match = r.Match(propVal.ToString());
								conditionOk = match.Success;
							}
							else
								conditionOk = propVal.Equals( lei.PropertyValue );
							break;
						}
						case ConditionOperators.coNotEqual:
							conditionOk = !propVal.Equals( lei.PropertyValue );
							break;
						case ConditionOperators.coGreater:
							conditionOk =  Comparer.DefaultInvariant.Compare(propVal, lei.PropertyValue) > 0;
							break;
						case ConditionOperators.coGreatOrEqual:
							conditionOk = Comparer.DefaultInvariant.Compare(propVal, lei.PropertyValue) >= 0;
							break;
						case ConditionOperators.coLess:
							conditionOk = Comparer.DefaultInvariant.Compare(propVal, lei.PropertyValue) < 0;
							break;
						case ConditionOperators.coLessOrEqual:
							conditionOk = Comparer.DefaultInvariant.Compare(propVal, lei.PropertyValue) <= 0;
							break;
						case ConditionOperators.coInclude:
							if (propVal is IList)
								conditionOk = (propVal as IList).Contains(lei.PropertyValue);
							else
								conditionOk = false;
							break;
					}
				else
					conditionOk = false;
					
				switch(lei.logicOperation)
				{
					case LogicOperations.loEMPTY:
						filterOk = conditionOk;
						break;
					case LogicOperations.loAND:
						filterOk = filterOk && conditionOk;
						break;
					case LogicOperations.loOR:
						filterOk = filterOk || conditionOk;
						break;
				}
			}
			return filterOk;
		}

		public IList Filter(IList listForFilter)
		{
			if (LogicExpression.Count == 0) 
				return listForFilter;

			IList filteredList = new ArrayList();
			foreach(object obj in listForFilter)
			{
				if (FilterObject(obj))
					filteredList.Add(obj);
			}
			return filteredList;
		}

		public object this[int index]
		{
			get 
			{
				if (index != -1)
					return LogicExpression[index];
				else
					return null;
			}
		}

		public IList LogicExpessions
		{
			get { return LogicExpression; }
		}
	}
}
