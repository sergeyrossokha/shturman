using System;

namespace Shturman.Nestor.DataAttributes
{
	public enum InsertListItemMode {imNewItem, imSelectItem };

	[AttributeUsage(AttributeTargets.Property)]
	[Serializable]
	public class PropertyInsertListItemAttribute: Attribute
	{
		private InsertListItemMode _insertMode = InsertListItemMode.imNewItem;

		public PropertyInsertListItemAttribute(): this(InsertListItemMode.imNewItem)
		{
		}

		public PropertyInsertListItemAttribute(InsertListItemMode insertMode)
		{
			_insertMode = insertMode;
		}

		public InsertListItemMode InsertMode
		{
			get { return _insertMode; }
			set { _insertMode = value; }
		}
	}
}