using System;

namespace Shturman.Nestor.DataAttributes
{
	/// <summary>
	/// ������� ��������� ������� ��� �������� ��� ���,
	/// ���� ��� �����������, �� ������ �������� �� ����� ������ ���
	/// � �����(DataGrid), ��� � � ��������� �������(PropertyGrid)
	/// </summary>
	[Serializable]
	public class PropertyVisibleAttribute: Attribute
	{
		private bool visible;
		 
		public PropertyVisibleAttribute(bool visible)
		{
			this.visible = visible;
		}

		public bool IsVisible
		{
			get {return visible;}
		}

	}
}
