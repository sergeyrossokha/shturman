using System;

namespace Shturman.Nestor.DataAttributes
{
	/// <summary>
	/// �������� PropertyBrowserDetailAttribute ��������� ��� ������� ���� 
	/// ����������� ���������, ������� ������������ � ����� ���� ������-������
	/// ����� ��������� ������
	/// </summary>
	[Serializable]
	public class PropertyBrowserDetailAttribute: Attribute
	{
		private Type detailType;
		private bool nmRelation = false;

		public PropertyBrowserDetailAttribute(Type detailtype, bool nmRelation)
		{
			detailType = detailtype;
			this.nmRelation = nmRelation;
		}

		public PropertyBrowserDetailAttribute(Type detailtype): this(detailtype, false)
		{
		}

		public Type DetailType
		{
			get { return detailType; }
			set { detailType = value; }
		}

		public bool NMRelation
		{
			get { return nmRelation; }
			set { nmRelation = value; }
		}
	}
}
