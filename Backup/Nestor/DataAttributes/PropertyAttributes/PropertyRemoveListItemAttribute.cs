using System;

namespace Shturman.Nestor.DataAttributes
{
	public enum RemoveListItemMode {rmRemoveItem, rmRemoveListItem };
	
	[AttributeUsage(AttributeTargets.Property)]
	[Serializable]
	public class PropertyRemoveListItemAttribute: Attribute
	{
		private RemoveListItemMode _removeMode = RemoveListItemMode.rmRemoveItem;

		public PropertyRemoveListItemAttribute(): this(RemoveListItemMode.rmRemoveItem)
		{
		}

		public PropertyRemoveListItemAttribute(RemoveListItemMode removeMode)
		{
			_removeMode = removeMode;
		}

		public RemoveListItemMode RemoveMode
		{
			get { return _removeMode; }
			set { _removeMode = value; }
		}
	}
}