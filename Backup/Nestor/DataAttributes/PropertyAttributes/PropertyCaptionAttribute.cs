using System;

namespace Shturman.Nestor.DataAttributes
{
	/// <summary>
	/// �������� PropertyCaptionAttribute ��������� ��� ������� ��������
	/// ���������, ������� ������������ � ������� �����(DataGrid) � � 
	/// ��������� �������(PropertyGrid) 
	/// </summary>
	[Serializable]
	public class PropertyCaptionAttribute: Attribute
	{
		private string caption;
		
		public PropertyCaptionAttribute(string caption)
		{
			this.caption = caption;
		}
        
		public string Caption
		{
			get {return caption;}
		}
	}
}
