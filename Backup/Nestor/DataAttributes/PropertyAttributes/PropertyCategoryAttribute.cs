using System;

namespace Shturman.Nestor.DataAttributes
{
	/// <summary>
	/// �������� PropertyCategoryAttribute ��������� ��� ������� ��������
	/// ���������, ������� ������������ � ��������� �������(PropertyGrid) 
	/// </summary>
	[Serializable]
	public class PropertyCategoryAttribute: Attribute
	{
		private string category;

		public PropertyCategoryAttribute(string category)
		{
			this.category = category;
		}

		public string Category
		{
			get {return category;}
		}
	}
}
