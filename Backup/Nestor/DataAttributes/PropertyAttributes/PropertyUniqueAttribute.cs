using System;

namespace Shturman.Nestor.DataAttributes
{
	/// <summary>
	/// Summary description for PropertyUniqueAttribute.
	/// </summary>
	[Serializable]
	public class PropertyUniqueAttribute: Attribute
	{
		private bool _hasUniqueValue;

		public PropertyUniqueAttribute(bool isUnique)
		{
			_hasUniqueValue = isUnique;
		}

		public bool IsUnique
		{
			get {return _hasUniqueValue;}
		}
	}
}
