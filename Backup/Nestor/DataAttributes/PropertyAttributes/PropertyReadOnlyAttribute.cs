using System;

namespace Shturman.Nestor.DataAttributes
{
	/// <summary>
	/// �������� PropertyReadOnlyAttribute ��������� ��� ����������� ��������,
	/// ��� �������� ������ ��� ������, ������������ ���������� �������(PropertyGrid)
	/// </summary>
	[Serializable]
	public class PropertyReadOnlyAttribute: Attribute
	{
		private bool isReadOnly;

		public PropertyReadOnlyAttribute(bool isReadOnly)
		{
			this.isReadOnly = isReadOnly;
		}

		public bool IsReadOnly
		{
			get {return isReadOnly;}
		}
	}
}
