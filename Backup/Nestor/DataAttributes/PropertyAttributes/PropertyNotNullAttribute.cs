using System;

namespace Shturman.Nestor.DataAttributes
{
	/// <summary>
	/// Summary description for PropertyNotNullAttribute.
	/// </summary>
	[Serializable]
	public class PropertyNotNullAttribute: Attribute
	{
		private bool notnull;

		public PropertyNotNullAttribute(bool notnull)
		{
			this.notnull = notnull;
		}

		public bool NotNull
		{
			get { return notnull; }
		}
	}
}
