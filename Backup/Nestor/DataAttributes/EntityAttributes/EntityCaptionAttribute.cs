using System;

namespace Shturman.Nestor.DataAttributes
{
	/// <summary>
	/// ������ ������� �������� � �������� ������ �������, 
	/// ���������� ������ ��������� � ������ �� ��������� 
	/// ��� ����������� ���� � ������ ��������� ��������.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
	[Serializable]
	public class EntityCaptionAttribute: Attribute
	{
		/// <summary>
		/// ��������� ������ � ������ ��������� ��������
		/// </summary>
		private string entityCaption = "";

		public EntityCaptionAttribute(string entityCaption)
		{
			this.entityCaption = entityCaption;
		}

		public string EntityCaption
		{
			get {return entityCaption;}
		}
	}
}
