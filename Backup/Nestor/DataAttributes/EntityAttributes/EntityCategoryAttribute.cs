using System;

namespace Shturman.Nestor.DataAttributes
{
	/// <summary>
	/// ������ ������� �������� � �������� ������ �������, 
	/// ���������� ������ ��������� � ������ ��������� � �������
	/// ������ ���������, ������������ ��� ��� ����������� ���� � 
	/// ������ ��������� ��������.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
	[Serializable]
	public class EntityCategoryAttribute: Attribute
	{
		/// <summary>
		/// ��������� ��������� � ������ ��������� �����
		/// </summary>
		private string entityCategory = "";

		public EntityCategoryAttribute(string entityCategory)
		{
			this.entityCategory = entityCategory;
		}

		public string Category
		{
			get{ return entityCategory; }
			set{ entityCategory = value; }
		}
	}
}
