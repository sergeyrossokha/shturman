using System;
using System.Collections;
using System.ComponentModel;

using Shturman.Nestor.DataAttributes;

namespace Shturman.Nestor.DataEditors
{
	/// <summary>
	/// ����� ��������� ��� ������������ ����������� �������, � 
	/// ��� ���������� ��� ����������� � PropertyGrid
	/// </summary>
	public class EntityWrapper: ICustomTypeDescriptor
	{
		// ������������� ��������� ��������.
		private object instance;
		
		// ���������, �������� ������� ��� ���������� �������.
		private PropertyDescriptorCollection propertyCollection;

		// ��������� �������� ��������� ������.
		public object Instance{ get{ return instance; } }


		public EntityWrapper(object instance)
		{
			// ��������� ������������� ������.
			this.instance = instance;
					
			// ������� ����� (������) �������� �������� ������ 
			// � ������� �������� ������� ��� ��������� ����������.
			propertyCollection = new PropertyDescriptorCollection(null);
			PropertyDescriptorCollection pdc = TypeDescriptor.GetProperties(this.instance, true);
			// ���������� �������� �������, ������� ��� ������� 
			// �� ��� ������� � �������� �� � ���������.
			
			ArrayList al=new ArrayList();		

			foreach(PropertyDescriptor pd in pdc)
			{
				/*
				 * ��-�� ����, ��� �� ������ ��������� �������� IsBrowsable � 
				 * ����� PropertyWrapper.cs ���������� ����������� � ����������� �� ��������,
				 * ������� �� ����� ��������� PropertyVisible �������������� � true.
				 * 
				 * �������, ��� ��� ����������, �� ������ �������� �� ���� :(
				 * 
				 * ������� �.�. 14.07.2005
				 * */

				// �������� �������� ������� PropertyVisibleAttribute.
				// � ������ ������� ����� ��������� null.
				PropertyVisibleAttribute mna = 
					pd.Attributes[typeof(PropertyVisibleAttribute)] as PropertyVisibleAttribute;
				// ���� ������� ������� BrowsableAttribute,
				// ���������� �����, ���������� � ����.
				if(mna != null && mna.IsVisible)					
					al.Add(pd);				
			}
			
			al.Sort(0, al.Count, new PropertyGridSortComparer());
			
			for(int index=0; index < al.Count; index++)
				propertyCollection.Add(new PropertyWrapper((PropertyDescriptor)al[index]));

		}

		#region ICustomTypeDescriptor implementation
		AttributeCollection ICustomTypeDescriptor.GetAttributes() 
		{
			return new AttributeCollection(null);
		}

		string ICustomTypeDescriptor.GetClassName() 
		{
			return null;
		}

		string ICustomTypeDescriptor.GetComponentName() 
		{
			return null;
		}

		TypeConverter ICustomTypeDescriptor.GetConverter() 
		{
			return null;
		}

		EventDescriptor ICustomTypeDescriptor.GetDefaultEvent() 
		{
			return null;
		}


		PropertyDescriptor ICustomTypeDescriptor.GetDefaultProperty() 
		{
			return null;
		}

		object ICustomTypeDescriptor.GetEditor(Type editorBaseType) 
		{
			return null;
		}

		EventDescriptorCollection ICustomTypeDescriptor.GetEvents() 
		{
			return new EventDescriptorCollection(null);
		}

		EventDescriptorCollection ICustomTypeDescriptor.GetEvents
			(Attribute[] attributes) 
		{
			return new EventDescriptorCollection(null);
		}

		PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties() 
		{
			return propertyCollection;
		}

		PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties(
			Attribute[] attributes) 
		{
			return propertyCollection;
		}

		object ICustomTypeDescriptor.GetPropertyOwner(PropertyDescriptor pd) 
		{
			return this;
		}
		#endregion

	}
}
