using System;
using System.Collections;
using System.ComponentModel;
using System.Reflection;
using System.Windows.Forms;
using Shturman.Nestor.DataAttributes;
using Shturman.Nestor.DataLists;
using Shturman.Nestor.Interfaces;

using Shturman.MultyLanguage;

namespace Shturman.Nestor.DataEditors
{
	public enum CollectionEditorType {AsCollectionBrowser, AsCollectionEditor};

	/// <summary>
	/// Summary description for CollectionEditorForm.
	/// </summary>
	public class CollectionEditorForm : Form
	{
		private Panel panel1;
		private DataGrid dataGrid1;
		private Button cancelButton;
		private Button okButton;
		private Button delButton;
		private Button newButton;
		private Button selectButton;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private Container components = null;

		/// <summary>
		/// ������� ��������
		/// </summary>
		private IObjectStorage objectFactory = null;
		
		private IList collection;
		private CurrencyManager collectionCurrencyManager;
		private bool canEditList;
		private object parentKey;
		private Type collectionType;
		private CheckBox checkBox1;				
		private CollectionEditorType editorType;
		private IList fromCollection = null;

		public CollectionEditorForm(IObjectStorage objectFactory, CollectionEditorType state, Type elementType,  IList editCollection, IList fromCollection)
		{
			InitializeComponent();									

			this.objectFactory = objectFactory;

			// ���������� ��������� �����
			this.editorType = state;
			// ���������� ��� ���������
			this.collectionType = elementType;
			
			this.collection =  new ListWrapper(collectionType, editCollection );
			if (fromCollection != null)
				if (fromCollection is ListWrapper)
					this.fromCollection = fromCollection;
				else
					this.fromCollection = new ListWrapper(collectionType, fromCollection);
			
			// ������������� �������� ������ � DataGrid
			this.dataGrid1.DataSource = this.collection;
			this.collectionCurrencyManager = (CurrencyManager)this.dataGrid1.BindingContext[this.collection];

			switch(this.editorType)
			{
				case CollectionEditorType.AsCollectionBrowser:
				{
					newButton.Visible = true;
					checkBox1.Visible = true;
					delButton.Visible = false;
					selectButton.Visible = false;

					if (this.fromCollection != null && this.fromCollection.Count != 0)
					{
						// ������������� �������� ������ � DataGrid
						this.dataGrid1.DataSource = this.fromCollection;
						this.collectionCurrencyManager = (CurrencyManager)this.dataGrid1.BindingContext[this.fromCollection];
					}
					else
					{
						checkBox1.Visible = false;
						// ������������� �������� ������ � DataGrid
						this.dataGrid1.DataSource = this.collection;
						this.collectionCurrencyManager = (CurrencyManager)this.dataGrid1.BindingContext[this.collection];
					}

					Text = "����� �� ������";
					// ��������� ��������� ���� � ����� ������
                    object[] eca = CollectionType.GetCustomAttributes(typeof(EntityCaptionAttribute), true);
					
					if (eca.Length == 1)
						this.dataGrid1.CaptionText = Shturman.MultyLanguage.Vocabruary.Translate((eca[0] as EntityCaptionAttribute).EntityCaption);
					break;
				}
				case CollectionEditorType.AsCollectionEditor:
				{
					newButton.Visible = true;
					delButton.Visible = true;
					selectButton.Visible = true; //??????????
					checkBox1.Visible = false;

					Text = "�������������� ������";
					// ��������� ��������� ���� � ����� ������
					object[] eca = CollectionType.GetCustomAttributes(typeof(EntityCaptionAttribute), true);
					if (eca.Length == 1)
                        this.dataGrid1.CaptionText = Vocabruary.Translate((eca[0] as EntityCaptionAttribute).EntityCaption);
					break;
				}
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(CollectionEditorForm));
			this.panel1 = new System.Windows.Forms.Panel();
			this.selectButton = new System.Windows.Forms.Button();
			this.newButton = new System.Windows.Forms.Button();
			this.cancelButton = new System.Windows.Forms.Button();
			this.okButton = new System.Windows.Forms.Button();
			this.delButton = new System.Windows.Forms.Button();
			this.dataGrid1 = new System.Windows.Forms.DataGrid();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.selectButton);
			this.panel1.Controls.Add(this.newButton);
			this.panel1.Controls.Add(this.cancelButton);
			this.panel1.Controls.Add(this.okButton);
			this.panel1.Controls.Add(this.delButton);
			this.panel1.Controls.Add(this.checkBox1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 325);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(632, 32);
			this.panel1.TabIndex = 0;
			// 
			// selectButton
			// 
			this.selectButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.selectButton.Location = new System.Drawing.Point(157, 5);
			this.selectButton.Name = "selectButton";
			this.selectButton.TabIndex = 5;
			this.selectButton.Text = "�������...";
			this.selectButton.Click += new System.EventHandler(this.selectButton_Click);
			// 
			// newButton
			// 
			this.newButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.newButton.Location = new System.Drawing.Point(5, 5);
			this.newButton.Name = "newButton";
			this.newButton.TabIndex = 4;
			this.newButton.Text = "�������";
			this.newButton.Click += new System.EventHandler(this.newButton_Click);
			// 
			// cancelButton
			// 
			this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.cancelButton.Location = new System.Drawing.Point(552, 5);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.TabIndex = 3;
			this.cancelButton.Text = "������";
			// 
			// okButton
			// 
			this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
			this.okButton.Location = new System.Drawing.Point(472, 5);
			this.okButton.Name = "okButton";
			this.okButton.TabIndex = 2;
			this.okButton.Text = "��";
			// 
			// delButton
			// 
			this.delButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.delButton.Location = new System.Drawing.Point(81, 5);
			this.delButton.Name = "delButton";
			this.delButton.TabIndex = 1;
			this.delButton.Text = "�������";
			this.delButton.Click += new System.EventHandler(this.delButton_Click);
			// 
			// dataGrid1
			// 
			this.dataGrid1.AllowNavigation = false;
			this.dataGrid1.DataMember = "";
			this.dataGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dataGrid1.HeaderForeColor = System.Drawing.SystemColors.ControlText;
			this.dataGrid1.Location = new System.Drawing.Point(0, 0);
			this.dataGrid1.Name = "dataGrid1";
			this.dataGrid1.Size = new System.Drawing.Size(632, 325);
			this.dataGrid1.TabIndex = 1;
			this.dataGrid1.BindingContextChanged += new System.EventHandler(this.dataGrid1_BindingContextChanged);
			// 
			// checkBox1
			// 
			this.checkBox1.Location = new System.Drawing.Point(85, 5);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(155, 24);
			this.checkBox1.TabIndex = 6;
			this.checkBox1.Text = "��� ��� �� �������?";
			this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
			// 
			// CollectionEditorForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(632, 357);
			this.Controls.Add(this.dataGrid1);
			this.Controls.Add(this.panel1);
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.MaximizeBox = false;
			this.Name = "CollectionEditorForm";
			this.ShowInTaskbar = false;
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
			this.Text = "C�����";
			this.Load += new System.EventHandler(this.CollectionEditorForm_Load);
			this.panel1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dataGrid1)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void newButton_Click(object sender, EventArgs e)
		{
			object newitem = null;

			if (ParentKey == null)
				newitem = objectFactory.New( CollectionType );
			else
				newitem = objectFactory.New( CollectionType, ParentKey );
				
			//foreach (FieldInfo fi in newItemType.GetFields())
			//{
				//fi.Attributes[typeof(PropertyReferenceAttribute)] as PropertyReferenceAttribute;
			//}
			
			//foreach(PropertyReferenceAttribute pra in newItemType.Attributes)
			//{
			//}
		
			//��������� �������� �������, �� �������� ���������� 
			// �������� ������ ����������� � ���������
			ObjectEditor oe = new ObjectEditor(this.objectFactory);

			oe.SelectedObject = newitem;

			if (oe.ShowDialog() == DialogResult.OK)
			{   
				//��������� ������, ������ ����� ����� ��� ��������� ����� CollectionEditorType.AsCollectionBrowser
				if (this.editorType == CollectionEditorType.AsCollectionBrowser) 
					objectFactory.Save(newitem);
				collection.Add(newitem);
				dataGrid1.DataSource = null;
				dataGrid1.DataSource = collection;
				dataGrid1_BindingContextChanged(dataGrid1 ,e);
			}
		}

		private void dataGrid1_BindingContextChanged(object sender, EventArgs e)
		{
			DataGrid dg = (DataGrid) sender;
			///������ ������� DataGrid'� ����������� ������
			Type dgt = sender.GetType();
			MethodInfo mi = dgt.GetMethod("ColAutoResize",BindingFlags.NonPublic | BindingFlags.Instance);
			for( int i = dg.FirstVisibleColumn; i< dg.VisibleColumnCount; i++)
			{
				object[] methodArgs = {i};
				mi.Invoke(sender, methodArgs);
			}			
		}

		private void delButton_Click(object sender, EventArgs e)
		{
			this.collection.Remove( this.collection[ collectionCurrencyManager.Position ] );
			dataGrid1_BindingContextChanged(dataGrid1 ,e);
		}

		private void CollectionEditorForm_Load(object sender, EventArgs e)
		{
			/// � ����� � ���, ��� ������ ����� �������� ����� CollectionUITypeEditor
			/// � � ��������� ��� �� ����� ����������� �������� �������� � ���� ������������,
			/// ���������� �����-��� ������� ����������.... � �������� ������ �� ObjectFactory
			/// ����� ��������� �������� ���������
			if (this.Owner is ObjectEditor)
				this.objectFactory = (this.Owner as ObjectEditor).ObjectFactory;
		}

		private void selectButton_Click(object sender, EventArgs e)
		{			
			CollectionEditorForm objsel = new CollectionEditorForm(objectFactory, CollectionEditorType.AsCollectionBrowser,
				collectionType, objectFactory.ObjectList( collectionType ), fromCollection);
			if (objsel.ShowDialog() == DialogResult.OK)
			{
				foreach(object obj in objsel.SelectedItems)
					this.collection.Add(obj);
			}
		}

		private void checkBox1_CheckedChanged(object sender, EventArgs e)
		{
			if (checkBox1.Checked)
			{
				// ������������� �������� ������ � DataGrid
				this.dataGrid1.DataSource = this.collection;
				this.collectionCurrencyManager = (CurrencyManager)this.dataGrid1.BindingContext[this.collection];
			}
			else
			{
				// ������������� �������� ������ � DataGrid
				this.dataGrid1.DataSource = this.fromCollection;
				this.collectionCurrencyManager = (CurrencyManager)this.dataGrid1.BindingContext[this.fromCollection];
			}
		}

		public IList Collection
		{
			get { return collection; }
		}

		public bool CanEditList
		{
			get {return canEditList;}
			set {
				this.newButton.Enabled = value; 
				this.delButton.Enabled = value;  

				canEditList = value;
			}
		}

		/// <summary>
		/// �������� ���������� ����� ��������. ��� �������� ������ �������
		/// � ��� �������� ��������������, �� �������� � �������� �������� 
		/// PropertyReferenceAttribute ����� �������� ������ ���� �������������
		/// ����������. �.�. ��� �������� ������� ����������� ��� ��� �������� � 
		/// ������ �������� � �������� ���� �������� PropertyReferenceAttribute.
		/// </summary>
		public object ParentKey
		{
			get { return parentKey; }
			set { parentKey = value; }
		}

		/// <summary>
		/// ��� ������������ � ���������. ������ ����� ��������� ����������, 
		/// �������������� ������������ ITypedList, ��� �������� ������ ����������
		/// ����� �� ����������. � ���� ��� ���������� ��� �������� ����� ��������,
		/// � ������ �������� �� ������������� ������.
		/// </summary>
		public Type CollectionType
		{
			get { return collectionType; }
			set { collectionType = value; }
		}

		public IList SelectedItems
		{
			get {
				ArrayList arrlst = new ArrayList();

				for(int index=0;index<collectionCurrencyManager.Count; index++)
					if (dataGrid1.IsSelected(index))						
						arrlst.Add( collectionCurrencyManager.List[index] );
				return arrlst;
			}
		}

		public bool ShowBrowserButton
		{
			get { return selectButton.Visible; }
			set { selectButton.Visible = value; }
		}
	}
}
