using System;
using System.Collections;
using System.ComponentModel;
using System.Globalization;

namespace Shturman.Nestor.DataEditors
{
	/// <summary>
	/// TypeConverter ��� ������������� ���������
	/// </summary>
	public class CollectionTypeConverter : TypeConverter
	{
		public override bool CanConvertTo(ITypeDescriptorContext context, Type destType)
		{
			return destType == typeof (string);
		}

		public override object ConvertTo(ITypeDescriptorContext context, CultureInfo culture, object value, Type destinationType)
		{
			if (destinationType == typeof (string))
			{
				string str = "";
				bool addcoma = false;
				foreach (object obj in (value as IList))
				{
					if (addcoma)
						str += "," + obj.ToString();
					else
						str += obj.ToString();

					addcoma = true;
				}
				return str; //"(������)";
			}
			return base.ConvertTo(context, culture, value, destinationType);
		}
	}
}
