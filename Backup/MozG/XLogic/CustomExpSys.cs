using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.IO;


namespace Shturman.Sherlock.XLogic
{
    public class CustomExpSys
    {
        protected const string selectLayerString = "/KvantBase/Layer[@name=\"LAYER_NAME\"]/Select[QUERY_CONDITION]/Then/Property[@Name=\"THEN_PROPERTY_NAME\"]/@Value";
        protected const string propertyString = "When/Property[@Name=\"PROPERTY_NAME\" and contains(@Value,\"PROPERTY_VALUE\")]";

        public CustomExpSys()
        {
        }

        public string BuildXPathQuery(string layerName, string goalPropName,
            string[] propWhen, IRuleContext ruleContext)
        {
            string xPathQuery = selectLayerString;
            xPathQuery = xPathQuery.Replace("LAYER_NAME", layerName);
            xPathQuery = xPathQuery.Replace("THEN_PROPERTY_NAME", goalPropName);

            string condition = "";

            condition = propertyString.Replace("PROPERTY_NAME", propWhen[0]).Replace("PROPERTY_VALUE", ruleContext.GetContext(propWhen[0]));
            for (int prop_index = 1; prop_index < propWhen.Length; prop_index++)
            {
                condition += " and " + propertyString.Replace("PROPERTY_NAME", propWhen[prop_index]).Replace("PROPERTY_VALUE", ruleContext.GetContext(propWhen[prop_index]));
            }
            xPathQuery = xPathQuery.Replace("QUERY_CONDITION", condition);

            return xPathQuery;
        }

        protected string RunXPathQuery(XPathDocument xpathDoc, string[] xPathQueryList, bool cooperate)
        {
            // Create a navigator to query with XPath.
            XPathNavigator nav = xpathDoc.CreateNavigator();

            List<string> stringList = new List<string>();

            foreach (string xPathQuery in xPathQueryList)
            {
                // Select the node and place the results in an iterator.
                XPathNodeIterator NodeIter = nav.Select(xPathQuery);

                //Iterate through the results showing the element value.
                while (NodeIter.MoveNext())
                {
                    string[] list = NodeIter.Current.Value.Split(';');

                    for (int index = 0; index < list.Length; index++)
                        if (!stringList.Contains(list[index]))
                            stringList.Add(list[index]);
                };

            }
            if (stringList.Count != 0)
                return string.Join(" ��� ", stringList.ToArray());
            else
                return "�� ����";
        }
        
        protected string RunXPathQuery(XPathDocument xpathDoc, string xPathQuery)
        {
            return this.RunXPathQuery(xpathDoc, new string[] { xPathQuery }, true);
        }
    }
}
