using System;
using System.Collections.Generic;
using System.Text;

namespace Shturman.Sherlock.XLogic
{
    public class RuleContext: IRuleContext
    {
        Dictionary<string, string> _internalContextDic;

        public RuleContext()
        {
            _internalContextDic = new Dictionary<string, string>();
        }

        #region IRuleContext Members

        public void AddRuleContext(string property, string value)
        {
            _internalContextDic.Add(property, value);
        }

        public void RemoveRuleContext(string property)
        {
            _internalContextDic.Remove(property);
        }

        public void ClearContext()
        {
            _internalContextDic.Clear();
        }

        public string GetContext(string property)
        {
            string val = _internalContextDic[property];
            
            if (val == null)
                return string.Empty;

            return val;
        }

        #endregion
    }
}
