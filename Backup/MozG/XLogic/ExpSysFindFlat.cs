using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.XPath;

namespace Shturman.Sherlock.XLogic
{
    public class ExpSysFindFlat : CustomExpSys
    {
        // strings template for building XPath query
        protected string layerName = "BrowseForFlat";
        protected string goalPropertyName = "Flat";

        // Properties by default
        protected string[] properties = { "Predmet", "StudyType" };
        protected string[] properties2 = { "Department", "StudyType" };
        protected string[] properties3 = { "Department", "Predmet" };
        protected string[] properties4 = { "Department" };

        #region Constructors
        public ExpSysFindFlat()
            : base()
        {
        }

        public ExpSysFindFlat(string[] propertiesList)
            : this()
        {
            properties = propertiesList;
        }
        #endregion

        public string Find(XPathDocument xPathDoc, IRuleContext ruleContext)
        {
            string[] xPathQueryList = new string[]
            {
                base.BuildXPathQuery(layerName, goalPropertyName, properties, ruleContext),
                base.BuildXPathQuery(layerName, goalPropertyName, properties2, ruleContext),
                base.BuildXPathQuery(layerName, goalPropertyName, properties3, ruleContext),
                base.BuildXPathQuery(layerName, goalPropertyName, properties4, ruleContext)
            };

            return base.RunXPathQuery(xPathDoc, xPathQueryList, true);
        }
    }
}
