using System;

namespace Shturman.Sherlock.TrueLogic
{
	/// <summary>
	/// Summary description for RuleInfo.
	/// </summary>
	[Serializable]
	public class RuleSetInfo
	{
		// ������� �������
		private CustomPropertyInfo _goalProperty = null;
		// ��������
		private Properties _properties = new Properties();

		private string _caption = "";

		/// <summary>
		/// �����������
		/// </summary>
		/// <param name="goalProperty">������� �������</param>
		public RuleSetInfo(string caption, CustomPropertyInfo goalProperty)
		{
			this._caption = caption;
			this._goalProperty = goalProperty;
		}

		#region Properties

		/// <summary>
		/// ������� �������
		/// </summary>
		public CustomPropertyInfo GoalProperty
		{
			get { return this._goalProperty; }
		}

		/// <summary>
		/// �������� ��������� ��� ������� ������
		/// </summary>
		public Properties PropertiesInfoList
		{
			get { return this._properties; }
			set { this._properties = value; }
		}

		public string Caption
		{
			get { return this._caption; }
			set { this._caption = value; }
		}

		#endregion

		public override string ToString()
		{
			return this._caption;
		}

	}
}