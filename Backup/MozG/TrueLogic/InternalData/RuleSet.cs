using System;
using System.Collections;

namespace Shturman.Sherlock.TrueLogic
{
	/// <summary>
	/// ����� ������ ��������� ����� ������� ���������
	/// ������ "������ ������" ������� ������ �� ������ �.�.������� 
	/// </summary>
	[Serializable]
	public class RuleSet
	{
		// ����������
		private RuleSetInfo _ruleSetInfo;
		// ������ ������
		private IList _rulesSet = new ArrayList();

		public RuleSet(RuleSetInfo ruleSetInfo)
		{
			_ruleSetInfo = ruleSetInfo;
		}

		public void AddRule(Rule rule)
		{
			lock (_rulesSet)
			{
				if (_rulesSet.Count != 0)
					// ��������� ����� ��� ������� ������ ��������� ������ ����
					if (_ruleSetInfo.GoalProperty.PropertyName == rule.RuleGoal.ItemName)
						_rulesSet.Add(rule);
					else
					{
						// Error
					}
				else
					_rulesSet.Add(rule);
			}
		}

		public Rule GetRule(int id)
		{
			return (Rule) _rulesSet[id];
		}

		public IList Evaluate(IList facts)
		{
			IList goalFactsSet = new ArrayList();
			foreach (Rule rule in _rulesSet)
				rule.EvaluateRule(facts, goalFactsSet);
			return goalFactsSet;
		}

		/// <summary>
		/// �������� �������� ��������
		/// </summary>
		public string GoalPropertyName
		{
			get { return _ruleSetInfo.GoalProperty.PropertyName; }
		}

		/// <summary>
		/// ���������� ������ ������ 
		/// </summary>
		public RuleSetInfo Info
		{
			get { return this._ruleSetInfo; }
		}

		/// <summary>
		/// ����� ������ ��������� ����� �����(����� ������� ���������) 
		/// </summary>
		public IList Rules
		{
			get { return this._rulesSet; }
		}

		/// <summary>
		/// ��������������� �������� ��� ������� � ��������
		/// </summary>
		public Rule this[int index]
		{
			get { return this._rulesSet[index] as Rule; }
		}

		public override string ToString()
		{
			return Info.ToString() + " [" + GoalPropertyName + "]";
		}

	}
}