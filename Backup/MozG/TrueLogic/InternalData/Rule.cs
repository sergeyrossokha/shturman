using System;
using System.Collections;

namespace Shturman.Sherlock.TrueLogic
{
	/// <summary>
	/// Summary description for Rule.
	/// </summary>
	[Serializable]
	public class Rule : Fact
	{
		private string _ruleExpression = ""; // Text representation of rule
		private RuleItem _ruleGoal = null; // Domen Goal Values
		private IList _ruleItems = new ArrayList(); //Domens

		public Rule(string name) : base(name)
		{
		}

		public Rule(string name, string expression) : base(name)
		{
			_ruleExpression = expression;
			CompileRule();
		}

		public void CompileRule()
		{
			Deductor.Compile(_ruleExpression, this);
		}

		public void EvaluateRule(IList facts, IList goalfacts)
		{
			Deductor.Evaluate(this, facts, goalfacts);
		}

		#region Properties

		public string ExpressionStr
		{
			get { return _ruleExpression; }
		}

		public IList RuleItems
		{
			get { return _ruleItems; }
		}

		public RuleItem RuleGoal
		{
			get { return _ruleGoal; }
			set { _ruleGoal = value; }
		}

		#endregion
	}
}