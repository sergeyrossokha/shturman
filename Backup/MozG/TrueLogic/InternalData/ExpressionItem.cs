using System;
using System.Runtime.Serialization;
using Shturman.Shedule.Objects;
using Shturman.Shedule.Objects.Container;

namespace Shturman.Sherlock.TrueLogic
{
	/// <summary>
	/// Summary description for ExpressionItem.
	/// </summary>
	[Serializable]
	public class ExpressionItem : ISerializable
	{
		public enum ExpressionOperator
		{
			iboEqual,
			iboNotEqual
		} ;

		private ExpressionOperator _boolOperator;
		private object _itemValue;
		private float _itemPd = 0.0f;

		public void GetObjectData(SerializationInfo si, StreamingContext ctx)
		{
			if (_itemValue != null)
			{
				si.AddValue("Operator", _boolOperator, typeof (ExpressionOperator));
				si.AddValue("IsStoredObject", _itemValue is StoredObject);

				if (_itemValue is StoredObject)
					si.AddValue("Value", (_itemValue as StoredObject).Uid);
				else
					si.AddValue("Value", _itemValue.ToString());
			}
		}

		private ExpressionItem(SerializationInfo si, StreamingContext ctx)
		{
			_boolOperator = (ExpressionOperator) si.GetValue("Operator", typeof (ExpressionOperator));
			bool isStoredObject = si.GetBoolean("IsStoredObject");
			if (isStoredObject)
			{
				long objectCode = si.GetInt64("Value");
				_itemValue = DB4OBridgeRem.theOneObject.GetObjectByID(objectCode);
			}
			else
				_itemValue = si.GetString("Value");
		}

		public ExpressionItem()
		{
		}

		public ExpressionItem(ExpressionOperator boolOperator, object itemValue, float itemPd)
		{
			_boolOperator = boolOperator;
			_itemValue = itemValue;
			_itemPd = itemPd;
		}

		public ExpressionOperator boolOperator
		{
			get { return _boolOperator; }
			set { _boolOperator = value; }
		}

		public object ItemValue
		{
			get { return _itemValue; }
			set { _itemValue = value; }
		}

		public float ItemPd
		{
			get { return _itemPd; }
			set { _itemPd = value; }
		}
	}
}