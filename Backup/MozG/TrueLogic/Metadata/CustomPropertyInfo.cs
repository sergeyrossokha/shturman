using System;
using System.Collections;

namespace Shturman.Sherlock.TrueLogic
{
	/// <summary>
	/// Summary description for CustomPropertyInfo.
	/// </summary>
	[Serializable]
	public abstract class CustomPropertyInfo
	{
		private string _propertyName = "";

		public CustomPropertyInfo(string propertyName)
		{
			_propertyName = propertyName;
		}

		public string PropertyName
		{
			get { return _propertyName; }
			set { _propertyName = value; }
		}

		public abstract IList PropertyValues { get; }

		public override string ToString()
		{
			return PropertyName;
		}

	}
}