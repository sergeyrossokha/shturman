using System;
using System.Collections;
using System.Xml.Serialization;
using Shturman.Nestor.Interfaces;

namespace Shturman.Sherlock.TrueLogic
{
	/// <summary>
	/// Summary description for PropertyExternInfo.
	/// </summary>
	[Serializable]
	public class PropertyExtInfo : CustomPropertyInfo
	{
		private Type _propertyType = null;
		private IObjectStorage store;

		public PropertyExtInfo(IObjectStorage store, string propertyName, Type propertyType) : base(propertyName)
		{
			this.store = store;
			_propertyType = propertyType;
		}

		[XmlIgnore]
		public override IList PropertyValues
		{
			get { return store.ObjectList(_propertyType); }
		}
	}
}