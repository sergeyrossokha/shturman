using System;
using System.ComponentModel;
using System.Windows.Forms;
using Shturman.Nestor.Interfaces;
using Shturman.Sherlock.TrueLogic;

namespace Shturman.Sherlock.Forms
{
	/// <summary>
	/// Summary description for KBConstructor.
	/// </summary>
	public class KBConstructor : Form
	{
		private TabControl tabControl;
		private TabPage pagePropertiesRedactor;
		private Panel panelProperies;
		private Panel panelValues;
		private Panel panelProperiesAction;
		private ListBox listProperies;
		private Panel panelValueAction;
		private ListBox listValues;
		private LinkLabel linkAddProperty;
		private LinkLabel linkRemoveProperty;
		private LinkLabel linkAddValue;
		private LinkLabel linkRemoveValue;
		private LinkLabel linkRemoveAllValues;
		private Panel panelLayers;
		private Splitter splitterPropertyValue;
		private Panel panelLayerProperties;
		private Panel panelLayerAction;
		private Panel panelLayerPropertiesAction;
		private ListBox listLayer;
		private Label labelPropertiesCaption;
		private Label labelValueCaptions;
		private Label labelLayerCaption;
		private Label labelLayerPropertiesCaption;
		private LinkLabel linkAddLayer;
		private LinkLabel linkRemoveLayer;
		private LinkLabel linkAddLeyerProperies;
		private LinkLabel linkRemoveLayerProperties;
		private Splitter splitterLayerLayerProperties;
		private IContainer components;
		private Panel panel1;
		private Label label1;
		private ListBox listLayerProperties;
		private Label labelGoalProperty;
		private Panel panelBkzLayer;
		private Label labelBkzLayerAction;
		private ListBox listBkzLayers;
		private Splitter splitter1;
		private Panel panelBkzQuantsTree;
		private Panel panelBkzQuantsTreeAction;
		private TreeView treeViewQuants;
		private Label label2;
		private LinkLabel linkLabel1;
		private LinkLabel linkLabel2;
		private LinkLabel linkLabel3;
		private ImageList imageList1;

		private LogicBase _lb = null;
		private TabPage pageBaseSchemaRedactor;
		private TabPage tabBaseRedactor;
		private TabPage tabSupport;
		private MainMenu mainMenu1;
		private MenuItem menuItem1;
		private MenuItem menuItem2;
		private MenuItem menuItem3;
		private MenuItem menuItem4;
		private MenuItem menuItem7;
		private System.Windows.Forms.ComboBox comboBox1;
		private System.Windows.Forms.TreeView treeView1;
		private System.Windows.Forms.ListBox listBox1;
		private System.Windows.Forms.TextBox textBox1;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private IObjectStorage _os = null;

		public KBConstructor(IObjectStorage os, LogicBase lb)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			_os = os;
			_lb = lb;

			foreach (object obj in _lb.ExistingProperties)
				this.listProperies.Items.Add(obj);

			foreach (object obj in _lb.RuleSetInfoList)
			{
				this.listLayer.Items.Add(obj);
			}

			foreach (object obj in _lb.RuleSets)
			{
				this.listBkzLayers.Items.Add(obj);
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(KBConstructor));
			this.tabControl = new System.Windows.Forms.TabControl();
			this.pagePropertiesRedactor = new System.Windows.Forms.TabPage();
			this.panelValues = new System.Windows.Forms.Panel();
			this.listValues = new System.Windows.Forms.ListBox();
			this.panelValueAction = new System.Windows.Forms.Panel();
			this.linkRemoveAllValues = new System.Windows.Forms.LinkLabel();
			this.linkRemoveValue = new System.Windows.Forms.LinkLabel();
			this.linkAddValue = new System.Windows.Forms.LinkLabel();
			this.labelValueCaptions = new System.Windows.Forms.Label();
			this.splitterPropertyValue = new System.Windows.Forms.Splitter();
			this.panelProperies = new System.Windows.Forms.Panel();
			this.listProperies = new System.Windows.Forms.ListBox();
			this.panelProperiesAction = new System.Windows.Forms.Panel();
			this.linkRemoveProperty = new System.Windows.Forms.LinkLabel();
			this.linkAddProperty = new System.Windows.Forms.LinkLabel();
			this.labelPropertiesCaption = new System.Windows.Forms.Label();
			this.tabBaseRedactor = new System.Windows.Forms.TabPage();
			this.panelBkzQuantsTree = new System.Windows.Forms.Panel();
			this.treeViewQuants = new System.Windows.Forms.TreeView();
			this.imageList1 = new System.Windows.Forms.ImageList(this.components);
			this.panelBkzQuantsTreeAction = new System.Windows.Forms.Panel();
			this.linkLabel3 = new System.Windows.Forms.LinkLabel();
			this.linkLabel2 = new System.Windows.Forms.LinkLabel();
			this.linkLabel1 = new System.Windows.Forms.LinkLabel();
			this.label2 = new System.Windows.Forms.Label();
			this.splitter1 = new System.Windows.Forms.Splitter();
			this.panelBkzLayer = new System.Windows.Forms.Panel();
			this.listBkzLayers = new System.Windows.Forms.ListBox();
			this.labelBkzLayerAction = new System.Windows.Forms.Label();
			this.pageBaseSchemaRedactor = new System.Windows.Forms.TabPage();
			this.panelLayerProperties = new System.Windows.Forms.Panel();
			this.listLayerProperties = new System.Windows.Forms.ListBox();
			this.panel1 = new System.Windows.Forms.Panel();
			this.labelGoalProperty = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.panelLayerPropertiesAction = new System.Windows.Forms.Panel();
			this.linkRemoveLayerProperties = new System.Windows.Forms.LinkLabel();
			this.linkAddLeyerProperies = new System.Windows.Forms.LinkLabel();
			this.labelLayerPropertiesCaption = new System.Windows.Forms.Label();
			this.splitterLayerLayerProperties = new System.Windows.Forms.Splitter();
			this.panelLayers = new System.Windows.Forms.Panel();
			this.listLayer = new System.Windows.Forms.ListBox();
			this.panelLayerAction = new System.Windows.Forms.Panel();
			this.linkRemoveLayer = new System.Windows.Forms.LinkLabel();
			this.linkAddLayer = new System.Windows.Forms.LinkLabel();
			this.labelLayerCaption = new System.Windows.Forms.Label();
			this.tabSupport = new System.Windows.Forms.TabPage();
			this.button3 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.textBox1 = new System.Windows.Forms.TextBox();
			this.listBox1 = new System.Windows.Forms.ListBox();
			this.treeView1 = new System.Windows.Forms.TreeView();
			this.comboBox1 = new System.Windows.Forms.ComboBox();
			this.mainMenu1 = new System.Windows.Forms.MainMenu();
			this.menuItem1 = new System.Windows.Forms.MenuItem();
			this.menuItem7 = new System.Windows.Forms.MenuItem();
			this.menuItem4 = new System.Windows.Forms.MenuItem();
			this.menuItem3 = new System.Windows.Forms.MenuItem();
			this.menuItem2 = new System.Windows.Forms.MenuItem();
			this.tabControl.SuspendLayout();
			this.pagePropertiesRedactor.SuspendLayout();
			this.panelValues.SuspendLayout();
			this.panelValueAction.SuspendLayout();
			this.panelProperies.SuspendLayout();
			this.panelProperiesAction.SuspendLayout();
			this.tabBaseRedactor.SuspendLayout();
			this.panelBkzQuantsTree.SuspendLayout();
			this.panelBkzQuantsTreeAction.SuspendLayout();
			this.panelBkzLayer.SuspendLayout();
			this.pageBaseSchemaRedactor.SuspendLayout();
			this.panelLayerProperties.SuspendLayout();
			this.panel1.SuspendLayout();
			this.panelLayerPropertiesAction.SuspendLayout();
			this.panelLayers.SuspendLayout();
			this.panelLayerAction.SuspendLayout();
			this.tabSupport.SuspendLayout();
			this.SuspendLayout();
			// 
			// tabControl
			// 
			this.tabControl.Controls.Add(this.pagePropertiesRedactor);
			this.tabControl.Controls.Add(this.tabBaseRedactor);
			this.tabControl.Controls.Add(this.pageBaseSchemaRedactor);
			this.tabControl.Controls.Add(this.tabSupport);
			this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl.Location = new System.Drawing.Point(0, 0);
			this.tabControl.Name = "tabControl";
			this.tabControl.SelectedIndex = 0;
			this.tabControl.Size = new System.Drawing.Size(752, 469);
			this.tabControl.TabIndex = 0;
			// 
			// pagePropertiesRedactor
			// 
			this.pagePropertiesRedactor.Controls.Add(this.panelValues);
			this.pagePropertiesRedactor.Controls.Add(this.splitterPropertyValue);
			this.pagePropertiesRedactor.Controls.Add(this.panelProperies);
			this.pagePropertiesRedactor.Location = new System.Drawing.Point(4, 22);
			this.pagePropertiesRedactor.Name = "pagePropertiesRedactor";
			this.pagePropertiesRedactor.Size = new System.Drawing.Size(744, 443);
			this.pagePropertiesRedactor.TabIndex = 0;
			this.pagePropertiesRedactor.Text = "�������� � �� ��������";
			// 
			// panelValues
			// 
			this.panelValues.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panelValues.Controls.Add(this.listValues);
			this.panelValues.Controls.Add(this.panelValueAction);
			this.panelValues.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelValues.Location = new System.Drawing.Point(259, 0);
			this.panelValues.Name = "panelValues";
			this.panelValues.Size = new System.Drawing.Size(485, 443);
			this.panelValues.TabIndex = 2;
			// 
			// listValues
			// 
			this.listValues.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.listValues.Dock = System.Windows.Forms.DockStyle.Fill;
			this.listValues.IntegralHeight = false;
			this.listValues.Location = new System.Drawing.Point(0, 24);
			this.listValues.Name = "listValues";
			this.listValues.Size = new System.Drawing.Size(483, 417);
			this.listValues.TabIndex = 1;
			// 
			// panelValueAction
			// 
			this.panelValueAction.Controls.Add(this.linkRemoveAllValues);
			this.panelValueAction.Controls.Add(this.linkRemoveValue);
			this.panelValueAction.Controls.Add(this.linkAddValue);
			this.panelValueAction.Controls.Add(this.labelValueCaptions);
			this.panelValueAction.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelValueAction.Location = new System.Drawing.Point(0, 0);
			this.panelValueAction.Name = "panelValueAction";
			this.panelValueAction.Size = new System.Drawing.Size(483, 24);
			this.panelValueAction.TabIndex = 0;
			// 
			// linkRemoveAllValues
			// 
			this.linkRemoveAllValues.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.linkRemoveAllValues.Location = new System.Drawing.Point(415, 6);
			this.linkRemoveAllValues.Name = "linkRemoveAllValues";
			this.linkRemoveAllValues.Size = new System.Drawing.Size(72, 14);
			this.linkRemoveAllValues.TabIndex = 3;
			this.linkRemoveAllValues.TabStop = true;
			this.linkRemoveAllValues.Text = "������� ��";
			this.linkRemoveAllValues.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkRemoveAllValues_LinkClicked);
			// 
			// linkRemoveValue
			// 
			this.linkRemoveValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.linkRemoveValue.Location = new System.Drawing.Point(369, 6);
			this.linkRemoveValue.Name = "linkRemoveValue";
			this.linkRemoveValue.Size = new System.Drawing.Size(56, 14);
			this.linkRemoveValue.TabIndex = 2;
			this.linkRemoveValue.TabStop = true;
			this.linkRemoveValue.Text = "�������";
			this.linkRemoveValue.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkRemoveValue_LinkClicked);
			// 
			// linkAddValue
			// 
			this.linkAddValue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.linkAddValue.Location = new System.Drawing.Point(316, 6);
			this.linkAddValue.Name = "linkAddValue";
			this.linkAddValue.Size = new System.Drawing.Size(56, 14);
			this.linkAddValue.TabIndex = 1;
			this.linkAddValue.TabStop = true;
			this.linkAddValue.Text = "��������";
			this.linkAddValue.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkAddValue_LinkClicked);
			// 
			// labelValueCaptions
			// 
			this.labelValueCaptions.Location = new System.Drawing.Point(8, 5);
			this.labelValueCaptions.Name = "labelValueCaptions";
			this.labelValueCaptions.Size = new System.Drawing.Size(56, 14);
			this.labelValueCaptions.TabIndex = 0;
			this.labelValueCaptions.Text = "��������";
			// 
			// splitterPropertyValue
			// 
			this.splitterPropertyValue.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.splitterPropertyValue.Location = new System.Drawing.Point(256, 0);
			this.splitterPropertyValue.Name = "splitterPropertyValue";
			this.splitterPropertyValue.Size = new System.Drawing.Size(3, 443);
			this.splitterPropertyValue.TabIndex = 1;
			this.splitterPropertyValue.TabStop = false;
			// 
			// panelProperies
			// 
			this.panelProperies.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panelProperies.Controls.Add(this.listProperies);
			this.panelProperies.Controls.Add(this.panelProperiesAction);
			this.panelProperies.Dock = System.Windows.Forms.DockStyle.Left;
			this.panelProperies.Location = new System.Drawing.Point(0, 0);
			this.panelProperies.Name = "panelProperies";
			this.panelProperies.Size = new System.Drawing.Size(256, 443);
			this.panelProperies.TabIndex = 0;
			// 
			// listProperies
			// 
			this.listProperies.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.listProperies.Dock = System.Windows.Forms.DockStyle.Fill;
			this.listProperies.IntegralHeight = false;
			this.listProperies.Location = new System.Drawing.Point(0, 24);
			this.listProperies.Name = "listProperies";
			this.listProperies.Size = new System.Drawing.Size(254, 417);
			this.listProperies.TabIndex = 1;
			this.listProperies.SelectedIndexChanged += new System.EventHandler(this.listProperies_SelectedIndexChanged);
			// 
			// panelProperiesAction
			// 
			this.panelProperiesAction.Controls.Add(this.linkRemoveProperty);
			this.panelProperiesAction.Controls.Add(this.linkAddProperty);
			this.panelProperiesAction.Controls.Add(this.labelPropertiesCaption);
			this.panelProperiesAction.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelProperiesAction.Location = new System.Drawing.Point(0, 0);
			this.panelProperiesAction.Name = "panelProperiesAction";
			this.panelProperiesAction.Size = new System.Drawing.Size(254, 24);
			this.panelProperiesAction.TabIndex = 0;
			// 
			// linkRemoveProperty
			// 
			this.linkRemoveProperty.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.linkRemoveProperty.Location = new System.Drawing.Point(206, 6);
			this.linkRemoveProperty.Name = "linkRemoveProperty";
			this.linkRemoveProperty.Size = new System.Drawing.Size(56, 14);
			this.linkRemoveProperty.TabIndex = 2;
			this.linkRemoveProperty.TabStop = true;
			this.linkRemoveProperty.Text = "�������";
			this.linkRemoveProperty.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkRemoveProperty_LinkClicked);
			// 
			// linkAddProperty
			// 
			this.linkAddProperty.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.linkAddProperty.Location = new System.Drawing.Point(154, 6);
			this.linkAddProperty.Name = "linkAddProperty";
			this.linkAddProperty.Size = new System.Drawing.Size(56, 14);
			this.linkAddProperty.TabIndex = 1;
			this.linkAddProperty.TabStop = true;
			this.linkAddProperty.Text = "��������";
			this.linkAddProperty.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkAddProperty_LinkClicked);
			// 
			// labelPropertiesCaption
			// 
			this.labelPropertiesCaption.Location = new System.Drawing.Point(8, 5);
			this.labelPropertiesCaption.Name = "labelPropertiesCaption";
			this.labelPropertiesCaption.Size = new System.Drawing.Size(56, 14);
			this.labelPropertiesCaption.TabIndex = 0;
			this.labelPropertiesCaption.Text = "��������";
			// 
			// tabBaseRedactor
			// 
			this.tabBaseRedactor.Controls.Add(this.panelBkzQuantsTree);
			this.tabBaseRedactor.Controls.Add(this.splitter1);
			this.tabBaseRedactor.Controls.Add(this.panelBkzLayer);
			this.tabBaseRedactor.Location = new System.Drawing.Point(4, 22);
			this.tabBaseRedactor.Name = "tabBaseRedactor";
			this.tabBaseRedactor.Size = new System.Drawing.Size(744, 443);
			this.tabBaseRedactor.TabIndex = 2;
			this.tabBaseRedactor.Text = "���� ������� ������";
			// 
			// panelBkzQuantsTree
			// 
			this.panelBkzQuantsTree.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panelBkzQuantsTree.Controls.Add(this.treeViewQuants);
			this.panelBkzQuantsTree.Controls.Add(this.panelBkzQuantsTreeAction);
			this.panelBkzQuantsTree.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelBkzQuantsTree.Location = new System.Drawing.Point(259, 0);
			this.panelBkzQuantsTree.Name = "panelBkzQuantsTree";
			this.panelBkzQuantsTree.Size = new System.Drawing.Size(485, 443);
			this.panelBkzQuantsTree.TabIndex = 2;
			// 
			// treeViewQuants
			// 
			this.treeViewQuants.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.treeViewQuants.Dock = System.Windows.Forms.DockStyle.Fill;
			this.treeViewQuants.FullRowSelect = true;
			this.treeViewQuants.ImageList = this.imageList1;
			this.treeViewQuants.Location = new System.Drawing.Point(0, 24);
			this.treeViewQuants.Name = "treeViewQuants";
			this.treeViewQuants.ShowPlusMinus = false;
			this.treeViewQuants.ShowRootLines = false;
			this.treeViewQuants.Size = new System.Drawing.Size(483, 417);
			this.treeViewQuants.TabIndex = 1;
			this.treeViewQuants.AfterExpand += new System.Windows.Forms.TreeViewEventHandler(this.treeViewQuants_AfterExpand);
			this.treeViewQuants.AfterCollapse += new System.Windows.Forms.TreeViewEventHandler(this.treeViewQuants_AfterExpand);
			this.treeViewQuants.BeforeCollapse += new System.Windows.Forms.TreeViewCancelEventHandler(this.treeViewQuants_BeforeCollapse);
			// 
			// imageList1
			// 
			this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
			this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
			this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
			// 
			// panelBkzQuantsTreeAction
			// 
			this.panelBkzQuantsTreeAction.Controls.Add(this.linkLabel3);
			this.panelBkzQuantsTreeAction.Controls.Add(this.linkLabel2);
			this.panelBkzQuantsTreeAction.Controls.Add(this.linkLabel1);
			this.panelBkzQuantsTreeAction.Controls.Add(this.label2);
			this.panelBkzQuantsTreeAction.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelBkzQuantsTreeAction.Location = new System.Drawing.Point(0, 0);
			this.panelBkzQuantsTreeAction.Name = "panelBkzQuantsTreeAction";
			this.panelBkzQuantsTreeAction.Size = new System.Drawing.Size(483, 24);
			this.panelBkzQuantsTreeAction.TabIndex = 0;
			// 
			// linkLabel3
			// 
			this.linkLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.linkLabel3.Location = new System.Drawing.Point(432, 8);
			this.linkLabel3.Name = "linkLabel3";
			this.linkLabel3.Size = new System.Drawing.Size(56, 14);
			this.linkLabel3.TabIndex = 3;
			this.linkLabel3.TabStop = true;
			this.linkLabel3.Text = "�������";
			this.linkLabel3.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel3_LinkClicked);
			// 
			// linkLabel2
			// 
			this.linkLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.linkLabel2.Location = new System.Drawing.Point(352, 8);
			this.linkLabel2.Name = "linkLabel2";
			this.linkLabel2.Size = new System.Drawing.Size(88, 14);
			this.linkLabel2.TabIndex = 2;
			this.linkLabel2.TabStop = true;
			this.linkLabel2.Text = "�������������";
			this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
			// 
			// linkLabel1
			// 
			this.linkLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.linkLabel1.Location = new System.Drawing.Point(296, 8);
			this.linkLabel1.Name = "linkLabel1";
			this.linkLabel1.Size = new System.Drawing.Size(56, 14);
			this.linkLabel1.TabIndex = 1;
			this.linkLabel1.TabStop = true;
			this.linkLabel1.Text = "��������";
			this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 5);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(160, 14);
			this.label2.TabIndex = 0;
			this.label2.Text = "���� ������� ������(������)";
			// 
			// splitter1
			// 
			this.splitter1.Location = new System.Drawing.Point(256, 0);
			this.splitter1.Name = "splitter1";
			this.splitter1.Size = new System.Drawing.Size(3, 443);
			this.splitter1.TabIndex = 1;
			this.splitter1.TabStop = false;
			// 
			// panelBkzLayer
			// 
			this.panelBkzLayer.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panelBkzLayer.Controls.Add(this.listBkzLayers);
			this.panelBkzLayer.Controls.Add(this.labelBkzLayerAction);
			this.panelBkzLayer.Dock = System.Windows.Forms.DockStyle.Left;
			this.panelBkzLayer.Location = new System.Drawing.Point(0, 0);
			this.panelBkzLayer.Name = "panelBkzLayer";
			this.panelBkzLayer.Size = new System.Drawing.Size(256, 443);
			this.panelBkzLayer.TabIndex = 0;
			// 
			// listBkzLayers
			// 
			this.listBkzLayers.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.listBkzLayers.Dock = System.Windows.Forms.DockStyle.Fill;
			this.listBkzLayers.IntegralHeight = false;
			this.listBkzLayers.Location = new System.Drawing.Point(0, 24);
			this.listBkzLayers.Name = "listBkzLayers";
			this.listBkzLayers.Size = new System.Drawing.Size(254, 417);
			this.listBkzLayers.TabIndex = 2;
			this.listBkzLayers.SelectedIndexChanged += new System.EventHandler(this.listBkzLayers_SelectedIndexChanged);
			// 
			// labelBkzLayerAction
			// 
			this.labelBkzLayerAction.Dock = System.Windows.Forms.DockStyle.Top;
			this.labelBkzLayerAction.Location = new System.Drawing.Point(0, 0);
			this.labelBkzLayerAction.Name = "labelBkzLayerAction";
			this.labelBkzLayerAction.Size = new System.Drawing.Size(254, 24);
			this.labelBkzLayerAction.TabIndex = 1;
			this.labelBkzLayerAction.Text = "���� ��";
			this.labelBkzLayerAction.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// pageBaseSchemaRedactor
			// 
			this.pageBaseSchemaRedactor.Controls.Add(this.panelLayerProperties);
			this.pageBaseSchemaRedactor.Controls.Add(this.splitterLayerLayerProperties);
			this.pageBaseSchemaRedactor.Controls.Add(this.panelLayers);
			this.pageBaseSchemaRedactor.Location = new System.Drawing.Point(4, 22);
			this.pageBaseSchemaRedactor.Name = "pageBaseSchemaRedactor";
			this.pageBaseSchemaRedactor.Size = new System.Drawing.Size(744, 443);
			this.pageBaseSchemaRedactor.TabIndex = 1;
			this.pageBaseSchemaRedactor.Text = "����� ���� ������";
			// 
			// panelLayerProperties
			// 
			this.panelLayerProperties.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panelLayerProperties.Controls.Add(this.listLayerProperties);
			this.panelLayerProperties.Controls.Add(this.panel1);
			this.panelLayerProperties.Controls.Add(this.panelLayerPropertiesAction);
			this.panelLayerProperties.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelLayerProperties.Location = new System.Drawing.Point(259, 0);
			this.panelLayerProperties.Name = "panelLayerProperties";
			this.panelLayerProperties.Size = new System.Drawing.Size(485, 443);
			this.panelLayerProperties.TabIndex = 2;
			// 
			// listLayerProperties
			// 
			this.listLayerProperties.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.listLayerProperties.Dock = System.Windows.Forms.DockStyle.Fill;
			this.listLayerProperties.IntegralHeight = false;
			this.listLayerProperties.Location = new System.Drawing.Point(0, 24);
			this.listLayerProperties.Name = "listLayerProperties";
			this.listLayerProperties.Size = new System.Drawing.Size(483, 393);
			this.listLayerProperties.TabIndex = 4;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.labelGoalProperty);
			this.panel1.Controls.Add(this.label1);
			this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
			this.panel1.Location = new System.Drawing.Point(0, 417);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(483, 24);
			this.panel1.TabIndex = 3;
			// 
			// labelGoalProperty
			// 
			this.labelGoalProperty.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(204)));
			this.labelGoalProperty.ForeColor = System.Drawing.SystemColors.Highlight;
			this.labelGoalProperty.Location = new System.Drawing.Point(107, 8);
			this.labelGoalProperty.Name = "labelGoalProperty";
			this.labelGoalProperty.Size = new System.Drawing.Size(373, 14);
			this.labelGoalProperty.TabIndex = 1;
			this.labelGoalProperty.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 8);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 14);
			this.label1.TabIndex = 0;
			this.label1.Text = "������� �������";
			// 
			// panelLayerPropertiesAction
			// 
			this.panelLayerPropertiesAction.Controls.Add(this.linkRemoveLayerProperties);
			this.panelLayerPropertiesAction.Controls.Add(this.linkAddLeyerProperies);
			this.panelLayerPropertiesAction.Controls.Add(this.labelLayerPropertiesCaption);
			this.panelLayerPropertiesAction.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelLayerPropertiesAction.Location = new System.Drawing.Point(0, 0);
			this.panelLayerPropertiesAction.Name = "panelLayerPropertiesAction";
			this.panelLayerPropertiesAction.Size = new System.Drawing.Size(483, 24);
			this.panelLayerPropertiesAction.TabIndex = 0;
			// 
			// linkRemoveLayerProperties
			// 
			this.linkRemoveLayerProperties.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.linkRemoveLayerProperties.Location = new System.Drawing.Point(432, 6);
			this.linkRemoveLayerProperties.Name = "linkRemoveLayerProperties";
			this.linkRemoveLayerProperties.Size = new System.Drawing.Size(56, 14);
			this.linkRemoveLayerProperties.TabIndex = 2;
			this.linkRemoveLayerProperties.TabStop = true;
			this.linkRemoveLayerProperties.Text = "�������";
			this.linkRemoveLayerProperties.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkRemoveLayerProperties_LinkClicked);
			// 
			// linkAddLeyerProperies
			// 
			this.linkAddLeyerProperies.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.linkAddLeyerProperies.Location = new System.Drawing.Point(376, 6);
			this.linkAddLeyerProperies.Name = "linkAddLeyerProperies";
			this.linkAddLeyerProperies.Size = new System.Drawing.Size(56, 14);
			this.linkAddLeyerProperies.TabIndex = 1;
			this.linkAddLeyerProperies.TabStop = true;
			this.linkAddLeyerProperies.Text = "��������";
			this.linkAddLeyerProperies.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkAddLeyerProperies_LinkClicked);
			// 
			// labelLayerPropertiesCaption
			// 
			this.labelLayerPropertiesCaption.Location = new System.Drawing.Point(8, 5);
			this.labelLayerPropertiesCaption.Name = "labelLayerPropertiesCaption";
			this.labelLayerPropertiesCaption.Size = new System.Drawing.Size(56, 14);
			this.labelLayerPropertiesCaption.TabIndex = 0;
			this.labelLayerPropertiesCaption.Text = "��������";
			// 
			// splitterLayerLayerProperties
			// 
			this.splitterLayerLayerProperties.Location = new System.Drawing.Point(256, 0);
			this.splitterLayerLayerProperties.Name = "splitterLayerLayerProperties";
			this.splitterLayerLayerProperties.Size = new System.Drawing.Size(3, 443);
			this.splitterLayerLayerProperties.TabIndex = 1;
			this.splitterLayerLayerProperties.TabStop = false;
			// 
			// panelLayers
			// 
			this.panelLayers.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.panelLayers.Controls.Add(this.listLayer);
			this.panelLayers.Controls.Add(this.panelLayerAction);
			this.panelLayers.Dock = System.Windows.Forms.DockStyle.Left;
			this.panelLayers.Location = new System.Drawing.Point(0, 0);
			this.panelLayers.Name = "panelLayers";
			this.panelLayers.Size = new System.Drawing.Size(256, 443);
			this.panelLayers.TabIndex = 0;
			// 
			// listLayer
			// 
			this.listLayer.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.listLayer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.listLayer.IntegralHeight = false;
			this.listLayer.Location = new System.Drawing.Point(0, 24);
			this.listLayer.Name = "listLayer";
			this.listLayer.Size = new System.Drawing.Size(254, 417);
			this.listLayer.TabIndex = 1;
			this.listLayer.SelectedIndexChanged += new System.EventHandler(this.listLayer_SelectedIndexChanged);
			// 
			// panelLayerAction
			// 
			this.panelLayerAction.Controls.Add(this.linkRemoveLayer);
			this.panelLayerAction.Controls.Add(this.linkAddLayer);
			this.panelLayerAction.Controls.Add(this.labelLayerCaption);
			this.panelLayerAction.Dock = System.Windows.Forms.DockStyle.Top;
			this.panelLayerAction.Location = new System.Drawing.Point(0, 0);
			this.panelLayerAction.Name = "panelLayerAction";
			this.panelLayerAction.Size = new System.Drawing.Size(254, 24);
			this.panelLayerAction.TabIndex = 0;
			// 
			// linkRemoveLayer
			// 
			this.linkRemoveLayer.Location = new System.Drawing.Point(206, 6);
			this.linkRemoveLayer.Name = "linkRemoveLayer";
			this.linkRemoveLayer.Size = new System.Drawing.Size(56, 14);
			this.linkRemoveLayer.TabIndex = 2;
			this.linkRemoveLayer.TabStop = true;
			this.linkRemoveLayer.Text = "�������";
			this.linkRemoveLayer.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkRemoveLayer_LinkClicked);
			// 
			// linkAddLayer
			// 
			this.linkAddLayer.Location = new System.Drawing.Point(154, 6);
			this.linkAddLayer.Name = "linkAddLayer";
			this.linkAddLayer.Size = new System.Drawing.Size(56, 14);
			this.linkAddLayer.TabIndex = 1;
			this.linkAddLayer.TabStop = true;
			this.linkAddLayer.Text = "��������";
			this.linkAddLayer.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkAddLayer_LinkClicked);
			// 
			// labelLayerCaption
			// 
			this.labelLayerCaption.Location = new System.Drawing.Point(8, 5);
			this.labelLayerCaption.Name = "labelLayerCaption";
			this.labelLayerCaption.Size = new System.Drawing.Size(56, 14);
			this.labelLayerCaption.TabIndex = 0;
			this.labelLayerCaption.Text = "���� ��";
			// 
			// tabSupport
			// 
			this.tabSupport.Controls.Add(this.button3);
			this.tabSupport.Controls.Add(this.button2);
			this.tabSupport.Controls.Add(this.button1);
			this.tabSupport.Controls.Add(this.label6);
			this.tabSupport.Controls.Add(this.label5);
			this.tabSupport.Controls.Add(this.label4);
			this.tabSupport.Controls.Add(this.label3);
			this.tabSupport.Controls.Add(this.textBox1);
			this.tabSupport.Controls.Add(this.listBox1);
			this.tabSupport.Controls.Add(this.treeView1);
			this.tabSupport.Controls.Add(this.comboBox1);
			this.tabSupport.Location = new System.Drawing.Point(4, 22);
			this.tabSupport.Name = "tabSupport";
			this.tabSupport.Size = new System.Drawing.Size(744, 443);
			this.tabSupport.TabIndex = 3;
			this.tabSupport.Text = "������������";
			// 
			// button3
			// 
			this.button3.Location = new System.Drawing.Point(97, 408);
			this.button3.Name = "button3";
			this.button3.TabIndex = 11;
			this.button3.Text = "��������";
			// 
			// button2
			// 
			this.button2.Location = new System.Drawing.Point(178, 408);
			this.button2.Name = "button2";
			this.button2.TabIndex = 10;
			this.button2.Text = "�������";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(16, 408);
			this.button1.Name = "button1";
			this.button1.TabIndex = 9;
			this.button1.Text = "�������";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(16, 74);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(64, 23);
			this.label6.TabIndex = 8;
			this.label6.Text = "�����";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(16, 16);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(112, 23);
			this.label5.TabIndex = 7;
			this.label5.Text = "����� ���� ������";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(272, 16);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(56, 23);
			this.label4.TabIndex = 6;
			this.label4.Text = "�������";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(270, 146);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(82, 23);
			this.label3.TabIndex = 5;
			this.label3.Text = "����������";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// textBox1
			// 
			this.textBox1.Location = new System.Drawing.Point(264, 176);
			this.textBox1.Multiline = true;
			this.textBox1.Name = "textBox1";
			this.textBox1.Size = new System.Drawing.Size(472, 256);
			this.textBox1.TabIndex = 4;
			this.textBox1.Text = "���� ��������=\"������\"  ��� ��������=\"���������\"  �� ���=1\r\n���� ��������=\"������" +
				"������\"  �� ���=5\r\n���� ��������=\"�������������\"  �� ���=5\r\n���� ��������=\"�����" +
				"���\"  �� ���=1\r\n���� ��������=\"������\" �� ���=13";
			this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
			// 
			// listBox1
			// 
			this.listBox1.Items.AddRange(new object[] {
														  "{(6=�����-1; 7=1-1; 8=1-1; 9=1,5;), (2=�����-5;)}; 4=5-5; 5=5-1;"});
			this.listBox1.Location = new System.Drawing.Point(264, 45);
			this.listBox1.Name = "listBox1";
			this.listBox1.Size = new System.Drawing.Size(472, 95);
			this.listBox1.TabIndex = 3;
			// 
			// treeView1
			// 
			this.treeView1.ImageIndex = -1;
			this.treeView1.Indent = 15;
			this.treeView1.Location = new System.Drawing.Point(16, 104);
			this.treeView1.Name = "treeView1";
			this.treeView1.Nodes.AddRange(new System.Windows.Forms.TreeNode[] {
																				  new System.Windows.Forms.TreeNode("�����", new System.Windows.Forms.TreeNode[] {
																																									 new System.Windows.Forms.TreeNode("������=����� ����.305341.019", new System.Windows.Forms.TreeNode[] {
																																																																			   new System.Windows.Forms.TreeNode("��������1=���������� � ��������")}),
																																									 new System.Windows.Forms.TreeNode("������=������� ����.745316.017", new System.Windows.Forms.TreeNode[] {
																																																																				 new System.Windows.Forms.TreeNode("��������2=���������������")}),
																																									 new System.Windows.Forms.TreeNode("��������3=������"),
																																									 new System.Windows.Forms.TreeNode("��������4=��������"),
																																									 new System.Windows.Forms.TreeNode("��������5=������")}),
																				  new System.Windows.Forms.TreeNode("����� �����", new System.Windows.Forms.TreeNode[] {
																																										   new System.Windows.Forms.TreeNode("��������6=��������"),
																																										   new System.Windows.Forms.TreeNode("�������7=��������"),
																																										   new System.Windows.Forms.TreeNode("��������8=�������������"),
																																										   new System.Windows.Forms.TreeNode("��������9=���������")})});
			this.treeView1.SelectedImageIndex = -1;
			this.treeView1.ShowLines = false;
			this.treeView1.Size = new System.Drawing.Size(240, 296);
			this.treeView1.TabIndex = 2;
			// 
			// comboBox1
			// 
			this.comboBox1.Location = new System.Drawing.Point(16, 46);
			this.comboBox1.Name = "comboBox1";
			this.comboBox1.Size = new System.Drawing.Size(240, 21);
			this.comboBox1.TabIndex = 1;
			this.comboBox1.Text = "����������";
			// 
			// mainMenu1
			// 
			this.mainMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem1});
			// 
			// menuItem1
			// 
			this.menuItem1.Index = 0;
			this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
																					  this.menuItem7,
																					  this.menuItem4,
																					  this.menuItem3,
																					  this.menuItem2});
			this.menuItem1.Text = "�����������";
			// 
			// menuItem7
			// 
			this.menuItem7.Index = 0;
			this.menuItem7.Text = "������";
			// 
			// menuItem4
			// 
			this.menuItem4.Index = 1;
			this.menuItem4.Text = "���������";
			this.menuItem4.Click += new System.EventHandler(this.menuItem4_Click);
			// 
			// menuItem3
			// 
			this.menuItem3.Index = 2;
			this.menuItem3.Text = "-";
			// 
			// menuItem2
			// 
			this.menuItem2.Index = 3;
			this.menuItem2.Text = "�����";
			this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
			// 
			// KBConstructor
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(752, 469);
			this.Controls.Add(this.tabControl);
			this.Menu = this.mainMenu1;
			this.Name = "KBConstructor";
			this.Text = "����������� ������";
			this.tabControl.ResumeLayout(false);
			this.pagePropertiesRedactor.ResumeLayout(false);
			this.panelValues.ResumeLayout(false);
			this.panelValueAction.ResumeLayout(false);
			this.panelProperies.ResumeLayout(false);
			this.panelProperiesAction.ResumeLayout(false);
			this.tabBaseRedactor.ResumeLayout(false);
			this.panelBkzQuantsTree.ResumeLayout(false);
			this.panelBkzQuantsTreeAction.ResumeLayout(false);
			this.panelBkzLayer.ResumeLayout(false);
			this.pageBaseSchemaRedactor.ResumeLayout(false);
			this.panelLayerProperties.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.panelLayerPropertiesAction.ResumeLayout(false);
			this.panelLayers.ResumeLayout(false);
			this.panelLayerAction.ResumeLayout(false);
			this.tabSupport.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private void linkAddProperty_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			NewPropertyInfoDlg newPropertyInfoFrm = new NewPropertyInfoDlg(_os);

			if (newPropertyInfoFrm.ShowDialog() == DialogResult.OK)
			{
				int index = _lb.ExistingProperties.Add(newPropertyInfoFrm.GetPropertyInfo());
				this.listProperies.Items.Add(_lb.ExistingProperties[index]);
			}
			this._lb.HasChanges = true;
		}

		private void linkRemoveProperty_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			if (MessageBox.Show(this, "����������� �������� ��������", "�������������", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
			{
				int index = this.listProperies.SelectedIndex;
				this.listProperies.Items.RemoveAt(index);
				_lb.ExistingProperties.RemoveAt(index);
				this._lb.HasChanges = true;
			}
		}

		private void linkAddValue_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			PropertyValueDlg propertyValueForm = new PropertyValueDlg();
			if (propertyValueForm.ShowDialog() == DialogResult.OK)
			{
				(this.listProperies.Items[this.listProperies.SelectedIndex] as PropertyInfo).PropertyValues.Add(propertyValueForm.Value);
				listProperies_SelectedIndexChanged(this.listProperies, new EventArgs());
				this._lb.HasChanges = true;
			}
		}

		private void linkRemoveValue_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			if (MessageBox.Show(this, "����������� �������� ��������", "�������������", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
			{
				(this.listProperies.Items[this.listProperies.SelectedIndex] as PropertyInfo).PropertyValues.RemoveAt(this.listValues.SelectedIndex);
				listProperies_SelectedIndexChanged(this.listProperies, new EventArgs());
				this._lb.HasChanges = true;
			}
		}

		private void linkRemoveAllValues_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			if (MessageBox.Show(this, "����������� �������� ���� ��������", "�������������", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
			{
				(this.listProperies.Items[this.listProperies.SelectedIndex] as PropertyInfo).PropertyValues.Clear();
				listProperies_SelectedIndexChanged(this.listProperies, new EventArgs());
				this._lb.HasChanges = true;
			}
		}

		private void listProperies_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (this.listValues.DataSource == null)
				this.listValues.Items.Clear();
			else
				this.listValues.DataSource = null;

			if ((sender as ListBox).SelectedItem != null)
			{
				if ((sender as ListBox).SelectedItem is PropertyExtInfo)
				{
					this.listValues.DataSource = ((sender as ListBox).SelectedItem as PropertyExtInfo).PropertyValues;

					this.linkAddValue.Enabled = false;
					this.linkRemoveValue.Enabled = false;
					this.linkRemoveAllValues.Enabled = false;
				}
				else
				{
					foreach (object obj in ((sender as ListBox).SelectedItem as PropertyInfo).PropertyValues)
						this.listValues.Items.Add(obj);

					this.linkAddValue.Enabled = true;
					this.linkRemoveValue.Enabled = true;
					this.linkRemoveAllValues.Enabled = true;
				}
			}
		}

		private void linkAddLayer_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			RuleSetInfoDlg ruleSetInfoFrm = new RuleSetInfoDlg(this._lb.ExistingProperties);
			if (ruleSetInfoFrm.ShowDialog() == DialogResult.OK)
			{
				int index = _lb.RuleSetInfoList.Add(ruleSetInfoFrm.GetRuleSetInfo());
				this.listLayer.Items.Add(_lb.RuleSetInfoList[index]);
				index = _lb.RuleSets.Add(new RuleSet(ruleSetInfoFrm.GetRuleSetInfo()));
				this.listBkzLayers.Items.Add((_lb.RuleSets[index] as RuleSet).Info.Caption + "[" + (_lb.RuleSets[index] as RuleSet).Info.GoalProperty.PropertyName + "]");
				this._lb.HasChanges = true;
			}
		}

		private void linkRemoveLayer_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			if (MessageBox.Show(this, "����������� �������� ���������� ������", "�������������", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
			{
				int index = this.listLayer.SelectedIndex;
				this.listLayer.Items.Remove(this.listLayer.SelectedItem);
				_lb.RuleSetInfoList.RemoveAt(index);
				_lb.RuleSets.RemoveAt(index);
				this.listBkzLayers.Items.RemoveAt(index);
				this._lb.HasChanges = true;
			}
		}

		private void linkAddLeyerProperies_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			BrowseForPropertyDlg browseForPropertyFrm = new BrowseForPropertyDlg(this.listLayer.Items[this.listLayer.SelectedIndex] as RuleSetInfo, _lb.ExistingProperties);
			if (browseForPropertyFrm.ShowDialog() == DialogResult.OK)
			{
				foreach (object obj in browseForPropertyFrm.LayerProperties)
					(this.listLayer.Items[this.listLayer.SelectedIndex] as RuleSetInfo).PropertiesInfoList.Add(obj as CustomPropertyInfo);
				listLayer_SelectedIndexChanged(this.listLayer, new EventArgs());
				this._lb.HasChanges = true;
			}
		}

		private void listLayer_SelectedIndexChanged(object sender, EventArgs e)
		{
			if ((sender as ListBox).SelectedItem == null) return;
			this.listLayerProperties.Items.Clear();
			if ((sender as ListBox).SelectedItem != null)
				foreach (object obj in ((sender as ListBox).SelectedItem as RuleSetInfo).PropertiesInfoList)
					this.listLayerProperties.Items.Add(obj);
			if (((sender as ListBox).SelectedItem as RuleSetInfo).GoalProperty != null)
				labelGoalProperty.Text = ((sender as ListBox).SelectedItem as RuleSetInfo).GoalProperty.ToString();
			else
				labelGoalProperty.Text = "������� ������� �� �����";
		}

		private void linkRemoveLayerProperties_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			if (MessageBox.Show(this, "����������� �������� �������� � ��������� ������", "�������������", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
			{
				int index = this.listLayerProperties.SelectedIndex;
				this.listLayerProperties.Items.RemoveAt(index);
				(_lb.RuleSetInfoList[this.listLayer.SelectedIndex] as RuleSetInfo).PropertiesInfoList.RemoveAt(index);
				listLayer_SelectedIndexChanged(this.listLayer, new EventArgs());
				this._lb.HasChanges = true;
			}
		}

		private void listBkzLayers_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (!this.listBkzLayers.CanFocus)
				return;

			this.treeViewQuants.Nodes.Clear();
			foreach (Rule rule in (_lb.RuleSets[this.listBkzLayers.SelectedIndex] as RuleSet).Rules)
			{
				TreeNode tn = new TreeNode();
				tn.Text = rule.Name;
				tn.Tag = rule;
				tn.ImageIndex = 3;
				tn.SelectedImageIndex = 3;

				// ������� ������������������ ���������
				for (int domenindex = 0; domenindex < rule.RuleItems.Count; domenindex++)
				{
					TreeNode domtn = new TreeNode();
					foreach (CustomPropertyInfo pi in (_lb.RuleSetInfoList[this.listBkzLayers.SelectedIndex] as RuleSetInfo).PropertiesInfoList)
						if (pi.PropertyName == (rule.RuleItems[domenindex] as RuleItem).ItemName)
						{
							domtn.Text = pi.PropertyName;
							domtn.Tag = pi;
							domtn.ImageIndex = 0;
							domtn.SelectedImageIndex = 0;
							break;
						}

					if ((rule.RuleItems[domenindex] as RuleItem).ItemExpressions.Count != 0)
						foreach (ExpressionItem ei in (rule.RuleItems[domenindex] as RuleItem).ItemExpressions)
						{
							TreeNode domval = new TreeNode();
							if (ei.ItemValue != null)
								domval.Text = ei.ItemValue.ToString();
							else
								domval.Text = "(null)";
							domval.Tag = ei;
							domval.ImageIndex = 2;
							domval.SelectedImageIndex = 2;

							domtn.Nodes.Add(domval);
						}
					else
					{
						TreeNode domval = new TreeNode();
						domval.Text = "*";
						domval.Tag = null;
						domval.ImageIndex = 2;
						domval.SelectedImageIndex = 2;

						domtn.Nodes.Add(domval);
					}

					tn.Nodes.Add(domtn);
				}

				// ������� �������� ��������
				TreeNode goalDom = new TreeNode();
				goalDom.Text = (_lb.RuleSets[this.listBkzLayers.SelectedIndex] as RuleSet).GoalPropertyName;
				goalDom.ImageIndex = 1;
				goalDom.SelectedImageIndex = 1;

				foreach (ExpressionItem ei in rule.RuleGoal.ItemExpressions)
				{
					TreeNode domval = new TreeNode();
					if (ei.ItemValue != null)
						domval.Text = ei.ItemValue.ToString();
					else
						domval.Text = "(null)";
					domval.Tag = ei;
					domval.ImageIndex = 2;
					domval.SelectedImageIndex = 2;

					goalDom.Nodes.Add(domval);
				}
				tn.Nodes.Add(goalDom);

				this.treeViewQuants.Nodes.Add(tn);
				this.treeViewQuants.ExpandAll();
			}
		}

		private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			// Create new rule of knowledgebase 
			Rule newRule = new Rule((_lb.RuleSets[this.listBkzLayers.SelectedIndex] as RuleSet).Rules.Count.ToString());

			//()

			RuleDlg ruleFrm = new RuleDlg(_os, newRule, _lb.RuleSetInfoList[this.listBkzLayers.SelectedIndex] as RuleSetInfo);

			if (ruleFrm.ShowDialog() == DialogResult.OK)
			{
				(_lb.RuleSets[this.listBkzLayers.SelectedIndex] as RuleSet).AddRule(newRule);
				listBkzLayers_SelectedIndexChanged(sender, new EventArgs());
				this._lb.HasChanges = true;
			}
		}

		private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			while (treeViewQuants.SelectedNode.Parent != null)
				treeViewQuants.SelectedNode = treeViewQuants.SelectedNode.Parent;

			Rule newRule = treeViewQuants.SelectedNode.Tag as Rule;

			RuleDlg ruleFrm = new RuleDlg(_os, newRule, _lb.RuleSetInfoList[this.listBkzLayers.SelectedIndex] as RuleSetInfo);

			if (ruleFrm.ShowDialog() == DialogResult.OK)
			{
				listBkzLayers_SelectedIndexChanged(sender, new EventArgs());
				this._lb.HasChanges = true;
			}
		}

		private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
		{
			// Remove selected rule from knowlargebase 

			if (MessageBox.Show("�� ������������� ������� ������� �������?", "��������", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
			{
				while (treeViewQuants.SelectedNode.Parent != null)
					treeViewQuants.SelectedNode = treeViewQuants.SelectedNode.Parent;

				Rule newRule = treeViewQuants.SelectedNode.Tag as Rule;

				(_lb.RuleSets[this.listBkzLayers.SelectedIndex] as RuleSet).Rules.Remove(newRule);

				listBkzLayers_SelectedIndexChanged(sender, new EventArgs());
				this._lb.HasChanges = true;
			}
		}

		private void treeViewQuants_AfterExpand(object sender, TreeViewEventArgs e)
		{
			/*// When Expand rule do expand all subnodes of expanded rule
			if (e.Node.Parent == null)
			{
				if (e.Action == TreeViewAction.Expand)
					e.Node.ExpandAll();
				else
					e.Node.Collapse();
			}*/
		}

		private void treeViewQuants_BeforeCollapse(object sender, TreeViewCancelEventArgs e)
		{
			//if (e.Node.Parent != null)
			//	e.Cancel = true;
		}

		private void menuItem2_Click(object sender, EventArgs e)
		{
			this.Close();
		}

		private void menuItem4_Click(object sender, EventArgs e)
		{
			this._lb.Save();
		}

		private void textBox1_TextChanged(object sender, System.EventArgs e)
		{
		
		}
	}
}