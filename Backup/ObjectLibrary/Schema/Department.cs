// ���������� ������ � xml-�����
// ������������� ���������� ���������������� ����� ������
using System;
using System.Collections;
using System.ComponentModel;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Shturman.Nestor.DataAttributes;

namespace Shturman.Shedule.Objects
{
	/// <summary>
	/// ������ ����������� �������� "�������"
	/// </summary>
	[EntityCaption("Department.EntityCaption")]
	[EntityCategory("Department.EntityCategory")]
	[Serializable]
	public class Department : StoredObject, IXmlSerializable
	{
		private string _mnemoCode;
		private string _name;
		private string _tel;
		private Faculty _faculty;
		private IList _subjects = new ArrayList();
		private IList _tutors = new ArrayList();

		#region ������������

		public Department()
		{
			this._mnemoCode = null;
			this._name = null;
			this._tel = null;
			this._faculty = null;
		}

		public Department(string mnemoCode, string name, string tel, Faculty faculty, IList subjects, IList tutors)
		{
			this._mnemoCode = mnemoCode;
			this._name = name;
			this._tel = tel;
			this._faculty = faculty;
			this._subjects = new ArrayList(subjects);
			this._tutors = new ArrayList(tutors);
		}

		#endregion

		#region ��������

		/// <summary>
		/// ��� ������� ������ ������������� (��������)
		/// </summary>
		[PropertyCaption("Department.MnemoCode.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(0)]
		[Description("Department.MnemoCode.Description")]
		public string MnemoCode
		{
			get { return _mnemoCode; }
			set { _mnemoCode = value; }
		}

		/// <summary>
		/// ������������ �������
		/// </summary>
		[PropertyCaption("Department.Name.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(1)]
		[Description("Department.Name.Description")]
		public string Name
		{
			get { return _name; }
			set { _name = value; }
		}

		/// <summary>
		/// ������� �������
		/// </summary>
		[PropertyCaption("Department.Telephone.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(2)]
		[Description("Department.Telephone.Description")]
		public string Telephone
		{
			get { return _tel; }
			set { _tel = value; }
		}

		/// <summary>
		/// ���������� ������ ��������� �� ���������� �����
		/// </summary>
		[PropertyCaption("Department.Faculty.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(3)]
		[PropertyReference(typeof (Faculty))]
		[Description("Department.Faculty.Description")]
		[PropertyInsertListItem]
		[PropertyRemoveListItem]
		public Faculty DepartmentFaculty
		{
			get { return this._faculty; }
			set { this._faculty = value; }
		}

		[PropertyBrowserDetail(typeof (Group))]
		[PropertyCaption("Department.Groups.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(4)]
		[Description("Department.Groups.Description")]
		[PropertyInsertListItem]
		[PropertyRemoveListItem]
		public IList Groups
		{
			get { return Store.ObjectList(typeof (Group), this); }
		}

		[PropertyBrowserDetail(typeof (Specialty))]
		[PropertyCaption("Department.Specialyties.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(5)]
		[Description("Department.Specialyties.Description")]
		[PropertyInsertListItem]
		[PropertyRemoveListItem]
		public IList Specialyties
		{
			get { return Store.ObjectList(typeof (Specialty), this); }
		}

		/// <summary>
		/// ���������� ������ ��������� ������� ������ �������������
		/// </summary>
		[PropertyCaption("Department.DepartmentSubjects.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(6)]
		[PropertyBrowserDetail(typeof (Subject), true)]
		[Description("Department.DepartmentSubjects.Description")]
		[PropertyInsertListItem(InsertListItemMode.imSelectItem)]
		[PropertyRemoveListItem(RemoveListItemMode.rmRemoveListItem)]
		public IList DepartmentSubjects
		{
			get { return this._subjects; }
		}

		/// <summary>		
		/// </summary>
		[PropertyCaption("Department.DepartmentTutors.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(7)]
		[PropertyBrowserDetail(typeof (Tutor), false)]
		[Description("Department.DepartmentTutors.Description")]
		[PropertyInsertListItem(InsertListItemMode.imNewItem)]
		[PropertyRemoveListItem(RemoveListItemMode.rmRemoveItem)]
		public IList DepartmentTutors
		{
			get { return this._tutors; }
		}

		#endregion		

		#region Implementation IXmlSerializable

		XmlSchema IXmlSerializable.GetSchema()
		{
			return null;
		}

		void IXmlSerializable.ReadXml(XmlReader reader)
		{
			reader.ReadStartElement("Department");

			reader.ReadStartElement("MnemoCode");
			this._mnemoCode = reader.ReadString();
			reader.ReadEndElement();

			reader.ReadStartElement("Name");
			this._name = reader.ReadString();
			reader.ReadEndElement();

			reader.ReadStartElement("Tel");
			this._tel = reader.ReadString();
			reader.ReadEndElement();

			reader.ReadStartElement("Faculty");

			reader.ReadStartElement("Uid");
			int facultyId = int.Parse(reader.ReadString());
			this._faculty = this.Store.GetObjectByID(facultyId) as Faculty;
			reader.ReadEndElement();

			reader.ReadStartElement("Name");
			reader.ReadEndElement();

			reader.ReadEndElement();

			reader.ReadEndElement();
		}

		void IXmlSerializable.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("MnemoCode");
			writer.WriteString(this._mnemoCode);
			writer.WriteEndElement();

			writer.WriteStartElement("Name");
			writer.WriteString(this._name);
			writer.WriteEndElement();

			writer.WriteStartElement("Tel");
			writer.WriteString(this._tel);
			writer.WriteEndElement();

			writer.WriteStartElement("Faculty");

			writer.WriteStartElement("Uid");
			writer.WriteString(this._faculty.Uid.ToString());
			writer.WriteEndElement();

			writer.WriteStartElement("Name");
			writer.WriteString(this._faculty.Name);
			writer.WriteEndElement();

			writer.WriteEndElement();
		}

		#endregion

		public override string ToString()
		{
			return _mnemoCode;
		}

		public override int CompareTo(object obj)
		{
			if (obj is Department)
				return Comparer.Default.Compare(this.MnemoCode, (obj as Department).MnemoCode);
			else
				return Comparer.Default.Compare(this.ToString(), obj.ToString());
		}
	}
}
