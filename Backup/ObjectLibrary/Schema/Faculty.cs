// ���������� ������ � xml-�����
// ������������� ���������� ���������������� ����� ������
using System;
using System.Collections;
using System.ComponentModel;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Shturman.Nestor.DataAttributes;

namespace Shturman.Shedule.Objects
{
	/// <summary>
	/// ������ ����������� �������� "���������"
	/// </summary>
	[EntityCaption("Faculty.EntityCaption")]
	[EntityCategory("Faculty.EntityCategory")]
	[Serializable]
	public class Faculty : StoredObject, IXmlSerializable
	{
		// �������� �������� �������������
		private string _mnemoCode;
		// ������������ ����������
		private string _name;
		// ������� ����������
		private string _tel;

		#region ������������

		public Faculty()
		{
			this._mnemoCode = null;
			this._name = null;
			this._tel = null;
		}

		public Faculty(string mnemoCode, string name, string tel)
		{
			this._mnemoCode = mnemoCode;
			this._name = name;
			this._tel = tel;
		}

		#endregion

		#region ��������

		/// <summary>
		/// ��� ������� ������ ������������� (��������)
		/// </summary>
		[PropertyCaption("Faculty.MnemoCode.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(0)]
		[Description("Faculty.MnemoCode.Description")]
		public string MnemoCode
		{
			get { return _mnemoCode; }
			set { _mnemoCode = value; }
		}

		/// <summary>
		/// ������������ �������
		/// </summary>
		[PropertyCaption("Faculty.Name.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(1)]
		[Description("Faculty.Name.Description")]
		public string Name
		{
			get { return _name; }
			set { _name = value; }
		}

		/// <summary>
		/// ������� �������
		/// </summary>
		[PropertyCaption("Faculty.Telephone.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(2)]
		[Description("Faculty.Telephone.Description")]
		public string Telephone
		{
			get { return _tel; }
			set { _tel = value; }
		}

		[PropertyBrowserDetail(typeof (Department))]
		[PropertyCaption("Faculty.Departments.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(4)]
		[Description("Faculty.Departments.Description")]
		[PropertyInsertListItem]
		[PropertyRemoveListItem]
		public IList Departments
		{
			get { return Store.ObjectList(typeof (Department), this); }
		}

		[PropertyBrowserDetail(typeof (Group))]
		[PropertyCaption("Faculty.Groups.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(5)]
		[Description("Faculty.Groups.Description")]
		[PropertyInsertListItem]
		[PropertyRemoveListItem]
		public IList Groups
		{
			get { return Store.ObjectList(typeof (Group), this); }
		}

		[PropertyBrowserDetail(typeof (Specialty))]
		[PropertyCaption("Faculty.Specialyties.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(6)]
		[Description("Faculty.Specialyties.Description")]
		[PropertyInsertListItem]
		[PropertyRemoveListItem]
		public IList Specialyties
		{
			get { return Store.ObjectList(typeof (Specialty), this); }
		}

		#endregion

		#region Implementation IXmlSerializable

		XmlSchema IXmlSerializable.GetSchema()
		{
			return null;
		}

		void IXmlSerializable.ReadXml(XmlReader reader)
		{
			reader.ReadStartElement("Faculty");

			reader.ReadStartElement("MnemoCode");
			this._mnemoCode = reader.ReadString();
			reader.ReadEndElement();

			reader.ReadStartElement("Name");
			this._name = reader.ReadString();
			reader.ReadEndElement();

			reader.ReadStartElement("Tel");
			this._tel = reader.ReadString();
			reader.ReadEndElement();

			reader.ReadEndElement();
		}

		void IXmlSerializable.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("MnemoCode");
			writer.WriteString(this._mnemoCode);
			writer.WriteEndElement();

			writer.WriteStartElement("Name");
			writer.WriteString(this._name);
			writer.WriteEndElement();

			writer.WriteStartElement("Tel");
			writer.WriteString(this._tel);
			writer.WriteEndElement();
		}

		#endregion

		public override string ToString()
		{
			return _mnemoCode + " " + _name;
		}

		public override int CompareTo(object obj)
		{
			if (obj != null)
				if (obj is Faculty)
					return Int32.Parse(this.MnemoCode).CompareTo(Int32.Parse((obj as Faculty).MnemoCode));
				else
					return Comparer.Default.Compare(this.ToString(), obj.ToString());
			else
				return 0;
		}
	}
}
