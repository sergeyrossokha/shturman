// ������������� ���������� ���������������� ����� ������
using System;
using System.Collections;
using System.Reflection;
using Shturman.Nestor.DataAttributes;
using Shturman.Nestor.Interfaces;

using Shturman.Shedule.Objects.Container;
using Shturman.Shedule.Server.Remoting;

namespace Shturman.Shedule.Objects
{
	/// <summary>
	/// ������� ����� ��� ���� ����������� ��������
	/// </summary>
	[Serializable]
	public class StoredObject : IBizObject, IComparable //, ISerializable
	{
		public long Uid
		{
			get 
			{ 
				return Store.GetID(this); 
			}
		}

        private ExtObjKey key = null;
		public ExtObjKey ObjectHardKey
		{
			get 
			{
                if (key == null || key.IsEmpty)
                {
                    key = new ExtObjKey();
                    key.TypeName = this.GetType().Name;
                    key.UID = this.Store.GetUUID(this, out key.Signature, out key.Index);
                }

				return key;
			}
		}

		public IObjectStorage Store
		{
			get { return DB4OBridgeRem.theOneObject; }
		}

		public virtual int CompareTo(object obj)
		{
			return Comparer.Default.Compare(this.ToString(), obj.ToString());
		}

		#region Object Properties and Events

		public event EditEventHandler Edit;
		public event ChangeEventHandler Change;
		public event DeleteEventHandler Delete;

		public event ChangePropertyEventHandler ChangeProperty;
		public event PropertyValuesEventHandler PropertyValues;

		public event CreateEventHandler Create;
		public event DestroyEventHandler Destroy;

		public bool OnEdit()
		{
			if (this.Edit != null)
				return this.Edit();
			else
				return false;
		}

		public void OnChange()
		{
			if (this.Change != null)
				this.Change();
		}

		public void OnDelete()
		{
			if (this.Delete != null)
				this.Delete();
		}

		public IList OnPropertyValues(PropertyInfo pi)
		{
			if (this.PropertyValues != null)
				return this.PropertyValues(pi);
			else
				return null;
		}

		public void OnChangeProperty(PropertyInfo pi, object oldvalue, object newValue)
		{
			if (this.ChangeProperty != null)
				this.ChangeProperty(pi, oldvalue, newValue);
		}

		public void OnCreate()
		{
			if (this.Create != null)
				this.Create();
		}

		public void OnDestroy()
		{
			if (this.Destroy != null)
				this.Destroy();
		}

		#endregion
	}
}