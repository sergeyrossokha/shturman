// ���������� ������ � xml-�����
// ������������� ���������� ���������������� ����� ������
using System;
using System.Collections;
using System.ComponentModel;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Shturman.Nestor.DataAttributes;

namespace Shturman.Shedule.Objects
{
	/// <summary>
	/// ������ ����������� �������� "������� ������"
	/// </summary>
	[EntityCaption("Group.EntityCaption")]
	[EntityCategory("Group.EntityCategory")]
	[Serializable]
	public class Group : StoredObject, IXmlSerializable, ICloneable, IComparable
	{
		private string _mnemoCode;
		private Department _department;
		private Faculty _faculty;
		private Specialty _specialty;
		private byte _count = 0;

		#region ������������

		public Group()
		{
			this._mnemoCode = null;
			this._department = null;
			this._faculty = null;
			this._specialty = null;
			this._count = 0;
		}

		public Group(string mnemoCode, Department department, Faculty faculty, Specialty specialty, byte count)
		{
			this._mnemoCode = mnemoCode;
			this._department = department;
			this._faculty = faculty;
			this._specialty = specialty;
			this._count = count;
		}

		#endregion

		#region ��������

		/// <summary>
		/// ��� ������� ������ ������������� (��������)
		/// </summary>
		[PropertyCaption("Group.MnemoCode.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(0)]
		[Description("Group.MnemoCode.Description")]
		public string MnemoCode
		{
			get { return _mnemoCode; }
			set { _mnemoCode = value; }
		}

		/// <summary>
		/// ����������� �������
		/// </summary>
		[PropertyCaption("Group.Department.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(1)]
		[PropertyReference(typeof (Department))]
		[Description("Group.Department.Description")]
		public Department GroupDepartment
		{
			get { return this._department; }
			set { this._department = value; }
		}

		/// <summary>
		/// ���������
		/// </summary>
		[PropertyCaption("Group.Faculty.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(2)]
		[PropertyReference(typeof (Faculty))]
		[Description("Group.Faculty.Description")]
		public Faculty GroupFaculty
		{
			get { return this._faculty; }
			set { this._faculty = value; }
		}

		/// <summary>
		/// ������������� ������
		/// </summary>
		[PropertyCaption("Group.Specialty.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(3)]
		[PropertyReference(typeof (Specialty))]
		[Description("Group.Specialty.Description")]
		public Specialty GroupSpecialty
		{
			get { return this._specialty; }
			set { this._specialty = value; }
		}

		/// <summary>
		/// ���-�� ��������� � ������
		/// </summary>
		[PropertyCaption("Group.Capacity.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(4)]
		[Description("Group.Capacity.Description")]
		public byte GroupCapacity
		{
			get { return _count; }
			set { _count = value; }
		}

		#endregion

		#region Implementation IXmlSerializable

		XmlSchema IXmlSerializable.GetSchema()
		{
			return null;
		}

		void IXmlSerializable.ReadXml(XmlReader reader)
		{
			reader.ReadStartElement("Group");

			reader.ReadStartElement("MnemoCode");
			this._mnemoCode = reader.ReadString();
			reader.ReadEndElement();

			reader.ReadStartElement("Department");
			reader.ReadStartElement("Uid");
			int deptUid = int.Parse(reader.ReadString());
			this._department = this.Store.GetObjectByID(deptUid) as Department;
			reader.ReadEndElement();

			reader.ReadStartElement("Name");
			reader.ReadEndElement();

			reader.ReadEndElement();

			reader.ReadStartElement("Faculty");
			reader.ReadStartElement("Uid");
			int facUid = int.Parse(reader.ReadString());
			this._faculty = this.Store.GetObjectByID(facUid) as Faculty;
			reader.ReadEndElement();

			reader.ReadStartElement("Name");
			reader.ReadEndElement();

			reader.ReadEndElement();

			reader.ReadStartElement("Specialty");
			reader.ReadStartElement("Uid");
			int specUid = int.Parse(reader.ReadString());
			this._specialty = this.Store.GetObjectByID(specUid) as Specialty;
			reader.ReadEndElement();

			reader.ReadStartElement("Name");
			reader.ReadEndElement();

			reader.ReadEndElement();

			reader.ReadStartElement("Count");
			this._count = byte.Parse(reader.ReadString());
			reader.ReadEndElement();

			reader.ReadEndElement();
		}

		void IXmlSerializable.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("MnemoCode");
			writer.WriteString(this._mnemoCode);
			writer.WriteEndElement();

			writer.WriteStartElement("Department");
			writer.WriteStartElement("Uid");
			writer.WriteString(this._department.Uid.ToString());
			writer.WriteEndElement();

			writer.WriteStartElement("Name");
			writer.WriteString(this._department.Name);
			writer.WriteEndElement();
			writer.WriteEndElement(); //Department

			writer.WriteStartElement("Faculty");
			writer.WriteStartElement("Uid");
			writer.WriteString(this._faculty.Uid.ToString());
			writer.WriteEndElement();

			writer.WriteStartElement("Name");
			writer.WriteString(this._faculty.Name);
			writer.WriteEndElement();
			writer.WriteEndElement(); //Faculty

			writer.WriteStartElement("Specialty");
			writer.WriteStartElement("Uid");
			writer.WriteString(this._specialty.Uid.ToString());
			writer.WriteEndElement();

			writer.WriteStartElement("Name");
			writer.WriteString(this._specialty.Name);
			writer.WriteEndElement();
			writer.WriteEndElement(); //Specialty

			writer.WriteStartElement("Count");
			writer.WriteString(this._count.ToString());
			writer.WriteEndElement();
		}

		#endregion

		public override string ToString()
		{
			return _mnemoCode + "[" + _count.ToString() + "]";
		}

		public override int GetHashCode()
		{
            if (this._mnemoCode == null || this._mnemoCode == string.Empty)
                return 0;

			return this._mnemoCode.GetHashCode();
		}

		public override bool Equals(object obj)
		{
			if (obj != null)
			{
				Type objType = obj.GetType();

				if (objType == typeof (Group))
				{
					Group gr = obj as Group;
					return gr._mnemoCode == this._mnemoCode;
				}
				return false;
			}
			else
				return false;
		}

		public override int CompareTo(object obj)
		{
			if (obj is Group)
			{
				Group a = this, b = (obj as Group);
				// ��������� + ���� 
				string a1 = a.MnemoCode.Substring(0, 2) + 
					// �������������
					a.MnemoCode.Substring(3).Replace(" ", "") +
					// ����� ������
					a.MnemoCode.Substring(2, 1);

				string b1 = b.MnemoCode.Substring(0, 2) + 
					// �������������
					b.MnemoCode.Substring(3).Replace(" ", "") +
					// ����� ������
					b.MnemoCode.Substring(2, 1);

				return ((IComparable)a1).CompareTo(b1);
			}
			else
				return Comparer.Default.Compare(this, obj);
		}

		public object Clone()
		{
			return new Group(this.MnemoCode, this.GroupDepartment, this.GroupFaculty, this.GroupSpecialty, this.GroupCapacity);
		}

	}

}
