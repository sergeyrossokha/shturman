// ���������� ������ � xml-�����
// ������������� ���������� ���������������� ����� ������
using System;
using System.Collections;
using System.ComponentModel;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Shturman.Nestor.DataAttributes;

namespace Shturman.Shedule.Objects
{
	/// <summary>
	/// ������ ����������� �������� "������� ���������"
	/// </summary>
	[EntityCaption("LectureHall.EntityCaption")]
	[EntityCategory("LectureHall.EntityCategory")]
	[Serializable]
	public class LectureHall : StoredObject, IXmlSerializable
	{
		private string _mnemoCode;
		private Building _building = null;
		private LectureHallType _halltype = null;
		//private Faculty _faculty = null;
		private Department _department = null;
		private int _capacity = 0;
		private double _groupcapacity = 0;

		#region ������������

		public LectureHall()
		{
			this._mnemoCode = null;
			this._building = null;
			this._halltype = null;
			//this._faculty = null;
			this._department = null;
			this._capacity = 0;
			this._groupcapacity = 0;
		}

		public LectureHall(string mnemoCode, Building building, LectureHallType lecturehalltype /*, Faculty faculty*/, Department department, int capacity)
		{
			this._mnemoCode = mnemoCode;
			this._building = building;
			this._halltype = lecturehalltype;
			//this._faculty = faculty;
			this._department = department;
			this._capacity = capacity;
			this._capacity = this._capacity/25;
		}

		#endregion

		#region ��������

		/// <summary>
		/// ��� ������� ������ ������������� (��������)
		/// </summary>
		[PropertyCaption("LectureHall.MnemoCode.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(0)]
		[PropertyUnique(true)]
		[Description("LectureHall.MnemoCode.Description")]
		public string MnemoCode
		{
			get { return _mnemoCode; }
			set { _mnemoCode = value; }
		}

		/// <summary>
		/// ������ � ������� ��������� ���������
		/// </summary>
		[PropertyCaption("LectureHall.Building.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(1)]
		[PropertyReference(typeof (Building))]
		[Description("LectureHall.Building.Description")]
		public Building LectureHallBuilding
		{
			get { return this._building; }
			set { this._building = value; }
		}

		/// <summary>
		/// ��� ���������
		/// </summary>
		[PropertyCaption("LectureHall.Type.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(2)]
		[PropertyReference(typeof (LectureHallType))]
		[Description("LectureHall.Type.Description")]
		public LectureHallType HallType
		{
			get { return this._halltype; }
			set { this._halltype = value; }
		}

		/// <summary>
		/// ����������� ���������(� �������)
		/// </summary>
		[PropertyCaption("LectureHall.StudentCapacity.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(3)]
		[Description("LectureHall.StudentCapacity.Description")]
		public int Capacity
		{
			get { return this._capacity; }
			set 
            { 
                this._capacity = value;
                _groupcapacity = _capacity / 25;
            }
		}

		/// <summary>
		/// ����������� ���������(� �������)
		/// </summary>
		[PropertyCaption("LectureHall.GroupCapacity.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(4)]
		[Description("LectureHall.GroupCapacity.Description")]
		public double GroupsCapacity
		{
			get { return Math.Round(this._groupcapacity, 1); }
			set { this._groupcapacity = value; }
		}

		/// <summary>
		/// ����������� �������
		/// </summary>
		[PropertyCaption("LectureHall.Department.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(5)]
		[PropertyReference(typeof (Department))]
		[Description("LectureHall.Department.Description")]
		public Department Department
		{
			get { return this._department; }
			set { this._department = value; }
		}

		#endregion

		#region Implementation IXmlSerializable

		XmlSchema IXmlSerializable.GetSchema()
		{
			return null;
		}


		void IXmlSerializable.ReadXml(XmlReader reader)
		{
			reader.ReadStartElement("LectureHall");

			reader.ReadStartElement("MnemoCode");
			this._mnemoCode = reader.ReadString();
			reader.ReadEndElement();

			reader.ReadStartElement("Building");

			reader.ReadStartElement("Uid");
			int buildingUid = int.Parse(reader.ReadString());
			this._building = this.Store.GetObjectByID(buildingUid) as Building;
			reader.ReadEndElement();

			reader.ReadStartElement("Name");
			reader.ReadEndElement();

			reader.ReadEndElement(); // Building

			reader.ReadStartElement("LectureHallType");

			reader.ReadStartElement("Uid");
			int hallTypeUid = int.Parse(reader.ReadString());
			this._halltype = this.Store.GetObjectByID(hallTypeUid) as LectureHallType;
			reader.ReadEndElement();

			reader.ReadStartElement("Name");
			reader.ReadEndElement();

			reader.ReadEndElement(); // LectureHallType

			reader.ReadStartElement("Faculty");

			reader.ReadStartElement("Uid");
//			int facultyUid = int.Parse(reader.ReadString());
//			this._faculty = this.GetObjectStorage().GetObjectByID(facultyUid) as Faculty;
			reader.ReadEndElement();

			reader.ReadStartElement("Name");
			reader.ReadEndElement();

			reader.ReadEndElement(); // Faculty

			reader.ReadStartElement("Department");

			reader.ReadStartElement("Uid");
			int departmentUid = int.Parse(reader.ReadString());
			this._department = this.Store.GetObjectByID(departmentUid) as Department;
			reader.ReadEndElement();

			reader.ReadStartElement("Name");
			reader.ReadEndElement();

			reader.ReadEndElement(); // Department

			reader.ReadStartElement("Capacity");
			this._capacity = int.Parse(reader.ReadString());
			reader.ReadEndElement();

			reader.ReadEndElement();
		}


		void IXmlSerializable.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("MnemoCode");
			writer.WriteString(this._mnemoCode);
			writer.WriteEndElement();

			writer.WriteStartElement("Building");

			writer.WriteStartElement("Uid");
			writer.WriteString(this._building.Uid.ToString());
			writer.WriteEndElement();

			writer.WriteStartElement("Name");
			writer.WriteString(this._building.Name);
			writer.WriteEndElement();

			writer.WriteEndElement(); //Building

			writer.WriteStartElement("LectureHallType");

			writer.WriteStartElement("Uid");
			writer.WriteString(this._halltype.Uid.ToString());
			writer.WriteEndElement();

			writer.WriteStartElement("Name");
			writer.WriteString(this._halltype.Name);
			writer.WriteEndElement();

			writer.WriteEndElement(); //LectureHallType

			writer.WriteStartElement("Faculty");

			writer.WriteStartElement("Uid");
			//writer.WriteString(this._faculty.Uid.ToString());
			writer.WriteEndElement();

			writer.WriteStartElement("Name");
			//writer.WriteString(this._faculty.Name);
			writer.WriteEndElement();

			writer.WriteEndElement(); //Faculty

			writer.WriteStartElement("Department");

			writer.WriteStartElement("Uid");
			writer.WriteString(this._department.Uid.ToString());
			writer.WriteEndElement();

			writer.WriteStartElement("Name");
			writer.WriteString(this._department.Name);
			writer.WriteEndElement();

			writer.WriteEndElement(); //Department

			writer.WriteStartElement("Capacity");
			writer.WriteString(this._capacity.ToString());
			writer.WriteEndElement();
		}

		#endregion

		public override string ToString()
		{
			return _mnemoCode + " " + this._building.ToString() + " " + this._halltype.ToString() + " [" + _capacity.ToString() + "]";
		}

		public override int GetHashCode()
		{
            if (this._mnemoCode == null)
                return string.Empty.GetHashCode();
            else
			    return this._mnemoCode.GetHashCode();
		}

		public override bool Equals(object obj)
		{
			if (obj is LectureHall)
				return ((obj as LectureHall)._mnemoCode == this._mnemoCode && (obj as LectureHall).LectureHallBuilding == this.LectureHallBuilding);
			else
				return base.Equals(obj);
		}

		public override int CompareTo(object obj)
		{
			if (obj is LectureHall)
				return Comparer.Default.Compare(this.MnemoCode, (obj as LectureHall).MnemoCode);
			else
				return Comparer.Default.Compare(this.ToString(), obj.ToString());
		}
	}

}
