// ������������� ���������� ���������������� ����� ������
using System;
using System.ComponentModel;
using Shturman.Nestor.DataAttributes;

namespace Shturman.Shedule.Objects
{
	/// <summary>
	/// ������ ����������� �������� "�������������"
	/// </summary>
	[EntityCaption("Specialty.EntityCaption")]
	[EntityCategory("Specialty.EntityCategory")]
	[Serializable]
	public class Specialty : StoredObject
	{
		private string _mnemoCode;
		private string _name;
		private string _shortName;
		private Faculty _faculty;
		private Department _department;

		#region ������������

		public Specialty()
		{
			this._mnemoCode = null;
			this._name = null;
			this._shortName = null;
			this._faculty = null;
			this._department = null;
		}

		public Specialty(string mnemoCode, string name, string shortName, Faculty faculty, Department department)
		{
			this._mnemoCode = mnemoCode;
			this._name = name;
			this._shortName = shortName;
			this._faculty = faculty;
			this._department = department;
		}

		#endregion

		#region ��������

		/// <summary>
		/// ��� ������� ������ ������������� (��������)
		/// </summary>
		[PropertyCaption("Specialty.MnemoCode.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(0)]
		[Description("Specialty.MnemoCode.Description")]
		public string MnemoCode
		{
			get { return _mnemoCode; }
			set { _mnemoCode = value; }
		}

		/// <summary>
		/// ������������ �������������
		/// </summary>
		[PropertyCaption("Specialty.Name.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(1)]
		[Description("Specialty.Name.Description")]
		public string Name
		{
			get { return _name; }
			set { _name = value; }
		}

		/// <summary>
		/// ����������� �������� �������������
		/// </summary>
		[PropertyCaption("Specialty.ShortName.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(2)]
		[Description("Specialty.ShortName.Description")]
		public string ShortName
		{
			get { return _shortName; }
			set { _shortName = value; }
		}

		/// <summary>
		/// ���������
		/// </summary>
		[PropertyCaption("Specialty.Faculty.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(3)]
		[PropertyReference(typeof (Faculty))]
		[Description("Specialty.Faculty.Description")]
		public Faculty SpecialtyFaculty
		{
			get { return this._faculty; }
			set { this._faculty = value; }
		}

		/// <summary>
		/// ����������� �������
		/// </summary>
		[PropertyCaption("Specialty.Department.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(4)]
		[PropertyReference(typeof (Department))]
		[Description("Specialty.Department.Description")]
		public Department SpecialtyDepartment
		{
			get { return this._department; }
			set { this._department = value; }
		}

		#endregion

		public override string ToString()
		{
			return _mnemoCode + " " + _shortName;
		}
	}

}
