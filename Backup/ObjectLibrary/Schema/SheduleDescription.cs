// ������������� ���������� ���������������� ����� ������
// MultyLang Support
using System;
using System.Collections;
using System.ComponentModel;
using System.ComponentModel.Design;
using System.Drawing.Design;
using System.Reflection;
using Shturman.Nestor.DataAttributes;
using Shturman.Nestor.Interfaces;

namespace Shturman.Shedule.Objects
{
	/// <summary>
	/// Summary description for SheduleDescription.
	/// </summary>
	[EntityCaption("SheduleDescription.EntityCaption")]
	[EntityCategory("SheduleDescription.EntityCategory")]
	[Serializable]
	public class SheduleDescription : IBizObject
	{
		private string _caption;
		private string _filename;
		private DateTime _beginDate;
		private DateTime _endDate;

        private int _weeksCount;
        private int _daysPerWeek;
        private int _pairPerDay;

        ///     [8] - shedule Leading count
        ///     [9] - shedule Restrinct count
        ///     [10] - shedule Lesson count
        private int _leadingCount;
        private int _restrictCount;
        private int _lessonCount;


		#region Properties

		[PropertyCaption("SheduleDescription.Caption.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(0)]
		[Description("SheduleDescription.Caption.Description")]
		public string Caption
		{
			get { return _caption; }
			set { _caption = value; }
		}

		[PropertyCaption("SheduleDescription.FileName.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(1)]
		[Description("SheduleDescription.FileName.Description")]
		public string FileName
		{
			get { return _filename; }
			set { _filename = value; }
		}

		[PropertyCaption("SheduleDescription.BeginDate.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(2)]
		[Editor(typeof (DateTimeEditor), typeof (UITypeEditor))]
		[Description("SheduleDescription.BeginDate.Description")]
		public DateTime BeginDate
		{
			get { return _beginDate; }
			set { _beginDate = value; }
		}

		[PropertyCaption("SheduleDescription.EndDate.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(3)]
		[Editor(typeof (DateTimeEditor), typeof (UITypeEditor))]
		[Description("SheduleDescription.EndDate.Description")]
		public DateTime EndDate
		{
			get { return _endDate; }
			set { _endDate = value; }
		}

        [PropertyCaption("SheduleDescription.WeeksCount.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(4)]
        [Description("SheduleDescription.WeeksCount.Description")]
        public int WeeksCount
        {
            get { return _weeksCount; }
            set { _weeksCount = value; }
        }

        [PropertyCaption("SheduleDescription.DaysPerWeek.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(5)]
        [Description("SheduleDescription.DaysPerWeek.Description")]
        public int DaysPerWeek
        {
            get { return _daysPerWeek; }
            set { _daysPerWeek = value; }
        }

        [PropertyCaption("SheduleDescription.PairPerDay.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(6)]
        [Description("SheduleDescription.PairPerDay.Description")]
        public int PairPerDay
        {
            get { return _pairPerDay; }
            set { _pairPerDay = value; }
        }

        [PropertyCaption("SheduleDescription.LeadingCount.Caption")]
        [PropertyVisible(true)]
        [PropertyReadOnly(true)]
        [PropertyIndex(7)]
        [Description("SheduleDescription.LeadingCount.Description")]
        public int LeadingCount
        {
            get { return _leadingCount; }
            set { _leadingCount = value; }
        }

        [PropertyCaption("SheduleDescription.RestrictCount.Caption")]
        [PropertyVisible(true)]
        [PropertyReadOnly(true)]
        [PropertyIndex(8)]
        [Description("SheduleDescription.RestrictCount.Description")]
        public int RestrictCount
        {
            get { return _restrictCount; }
            set { _restrictCount = value; }
        }

        [PropertyCaption("SheduleDescription.LessonCount.Caption")]
        [PropertyVisible(true)]
        [PropertyReadOnly(true)]
        [PropertyIndex(9)]
        [Description("SheduleDescription.LessonCount.Description")]
        public int LessonCount
        {
            get { return _lessonCount; }
            set { _lessonCount = value; }
        }


		#endregion

		public SheduleDescription()
		{
		}

		public IList StudyLoading_OnPropertyValues(PropertyInfo pi)
		{
			return new ArrayList();
		}

		#region Object Properties and Events

		public IObjectStorage Store
		{
			get { return null; }
		}

		public event EditEventHandler Edit;
		public event ChangeEventHandler Change;
		public event DeleteEventHandler Delete;

		public event ChangePropertyEventHandler ChangeProperty;
		public event PropertyValuesEventHandler PropertyValues;

		public event CreateEventHandler Create;
		public event DestroyEventHandler Destroy;

		public bool OnEdit()
		{
			if (this.Edit != null)
				return this.Edit();
			else
				return false;
		}

		public void OnChange()
		{
			if (this.Change != null)
				this.Change();
		}

		public void OnDelete()
		{
			if (this.Delete != null)
				this.Delete();
		}

		public IList OnPropertyValues(PropertyInfo pi)
		{
			if (this.PropertyValues != null)
				return this.PropertyValues(pi);
			else
				return null;
		}

		public void OnChangeProperty(PropertyInfo pi, object oldvalue, object newValue)
		{
			if (this.ChangeProperty != null)
				this.ChangeProperty(pi, oldvalue, newValue);
		}

		public void OnCreate()
		{
			if (this.Create != null)
				this.Create();
		}

		public void OnDestroy()
		{
			if (this.Destroy != null)
				this.Destroy();
		}

		#endregion
	}
}
