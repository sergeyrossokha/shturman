// ���������� ������ � xml-�����
// ������������� ���������� ���������������� ����� ������
using System;
using System.Collections;
using System.ComponentModel;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Shturman.Nestor.DataAttributes;

namespace Shturman.Shedule.Objects
{
	/// <summary>
	/// ������ ����������� �������� "������� ���"
	/// </summary>
	[EntityCaption("StudyHour.EntityCaption")]
	[EntityCategory("StudyHour.EntityCategory")]
	[Serializable]
	public class StudyHour : StoredObject, IXmlSerializable
	{
		// �������� �������� �������������
		private string _mnemoCode;
		// ����� ������ �������
		private DateTime _fromTime;
		// ����� ��������� �������
		private DateTime _toTime;

		#region ������������

		/// <summary>
		/// ����������� �� ���������
		/// </summary>
		public StudyHour()
		{			
			this._mnemoCode = null;
		}

		/// <summary>
		/// ����������� � �����������
		/// </summary>
		/// <param name="mnemoCode">�������� �������� �������������</param>
		/// <param name="fromTime">����� ������ �������</param>
		/// <param name="toTime">����� ��������� �������</param>
		public StudyHour(string mnemoCode, DateTime fromTime, DateTime toTime)
		{
			this._mnemoCode = mnemoCode;
			this._fromTime = fromTime;
			this._toTime = toTime;
		}

		#endregion

		#region ��������

		/// <summary>
		/// ��� ������� ������ ������������� (��������)
		/// </summary>
		[PropertyCaption("StudyHour.MnemoCode.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(0)]
		[Description("StudyHour.MnemoCode.Description")]
		public string MnemoCode
		{
			get { return _mnemoCode; }
			set { _mnemoCode = value; }
		}

		[PropertyCaption("StudyHour.FromTime.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(1)]
		[Description("StudyHour.FromTime.Description")]
		public TimeSpan FromTime
		{
			get { return _fromTime.TimeOfDay; }
			set { _fromTime = DateTime.Parse(value.ToString()); }
		}

		[PropertyCaption("StudyHour.ToTime.Caption")]
		[PropertyVisible(true)]
		[PropertyIndex(2)]
		[Description("StudyHour.ToTime.Description")]
		public TimeSpan ToTime
		{
			get { return _toTime.TimeOfDay; }
			set { _toTime = DateTime.Parse(value.ToString()); }
		}
		#endregion			

		#region Implementation IXmlSerializable

		XmlSchema IXmlSerializable.GetSchema()
		{
			return null;
		}

		void IXmlSerializable.ReadXml(XmlReader reader)
		{
			reader.ReadStartElement("StudyHour");

			reader.ReadStartElement("MNEMOCODE");
			this._mnemoCode = reader.ReadString();
			reader.ReadEndElement();

			reader.ReadStartElement("FROMTIME");
			this._fromTime = DateTime.Parse(reader.ReadString());
			reader.ReadEndElement();

			reader.ReadStartElement("TOTIME");
			this._toTime = DateTime.Parse(reader.ReadString());
			reader.ReadEndElement();

			reader.ReadEndElement();
		}

		void IXmlSerializable.WriteXml(XmlWriter writer)
		{
			writer.WriteStartElement("MNEMOCODE");
			writer.WriteString(this._mnemoCode.ToString());
			writer.WriteEndElement();

			writer.WriteStartElement("FROMTIME");
			writer.WriteString(this._fromTime.ToString());
			writer.WriteEndElement();

			writer.WriteStartElement("TOTIME");
			writer.WriteString(this._toTime.ToString());
			writer.WriteEndElement();
		}

		#endregion

		public override string ToString()
		{
			return _mnemoCode;
		}

		public override int CompareTo(object obj)
		{
			if (obj is StudyHour)
				return Comparer.Default.Compare(Int32.Parse(this.MnemoCode), Int32.Parse((obj as StudyHour).MnemoCode));
			else
				return Comparer.Default.Compare(this.ToString(), obj.ToString());
		}
	}
}