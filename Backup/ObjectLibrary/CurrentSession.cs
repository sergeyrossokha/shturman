using System;
using Shturman.Nestor.Interfaces;
using System.Windows.Forms;
using Shturman.Shedule.Server.Remoting;

namespace Shturman.Shedule.Objects
{
	/// <summary>
	/// Summary description for CurrentSession.
	/// </summary>
	public class CurrentSession: MarshalByRefObject
	{
        private ShturmanUser _currentUser = null;
		private IObjectStorage _objectStorage = null;

		private CurrentSession()
		{
		}
		
		public static readonly CurrentSession instance = new CurrentSession();
		
		/// <summary>
		/// ������� ������������
		/// </summary>
        public ShturmanUser CurrentUser
		{
			get { return _currentUser; }
			set { _currentUser = value; }
		}
		
		/// <summary>
		/// ��������� ��������
		/// </summary>
		public IObjectStorage ObjectStorage
		{
			get { return _objectStorage; }
			set { _objectStorage = value; }
		}

		public bool CheckRightCallBack(ExtObjKey user_key)
		{
			if (user_key.UID != -1)
			{
                ShturmanUser usr = _objectStorage.GetObjectByUUID(user_key.Signature, user_key.Index) as ShturmanUser;
				if (usr != null)
					if (CurrentUser.UserUid < usr.UserUid)
					{
						MessageBox.Show("� ��� ����������� ���� ��� ��������� ����� �������, ���������� �� ��������� �� �������� ("+usr.Name+")\n ��� �� �������������");
						return false;
					}
					else
						return true;
			}
			
			return true;
		}
	}
}
