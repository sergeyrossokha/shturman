using System;
using Shturman.Shedule.Objects;
using Db4objects.Db4o.Query;

namespace Shturman.Shedule.Objects.Searching
{
	/// <summary>
	/// Summary description for PostSearchByName.
	/// </summary>
	public class PostSearchByName: Predicate
	{
		private string _postName;
		public PostSearchByName(string postName)
		{			
			_postName = postName;
		}

		public bool Match(Post post)
		{
			if (post != null)
				return StringFuzzyComparer.Compare(post.MnemoCode, _postName) > 75;
			else
				return false;
		}
	}
}
