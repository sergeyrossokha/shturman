// My Assemblies
using Shturman.Shedule.Objects;
using Db4objects.Db4o.Query;

namespace Shturman.Shedule.Objects.Searching
{
	/// <summary>
	/// Summary description for SubjectSearchByName.
	/// </summary>
    public class SubjectSearchByName : Db4objects.Db4o.Query.Predicate
	{
		private string _subjectName = "";

		public SubjectSearchByName(string subjectName)
		{
			_subjectName = subjectName;
		}

		public bool Match(Subject subject)
		{
            if (subject != null)
            {
                return subject.Name.StartsWith(_subjectName) ||
                    subject.ShortName.StartsWith(_subjectName)||(StringFuzzyComparer.Compare(subject.Name, _subjectName) > 75);
            }
            else
                return false;
		}
	}
}
