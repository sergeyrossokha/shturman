// My Assemblies
using Db4objects.Db4o.Query;
using Shturman.Shedule.Objects;

namespace Shturman.Shedule.Objects.Searching
{
	/// <summary>
	/// Summary description for DepartmentSearchByNumber.
	/// </summary>
	public class DepartmentSearchByNumber : Predicate
	{
		private string _departmentNumber;

		public DepartmentSearchByNumber(string departmentNumber)
		{
			_departmentNumber = departmentNumber;
		}

		public bool Match(Department department)
		{
			return department.MnemoCode == _departmentNumber;
		}
	}
}
