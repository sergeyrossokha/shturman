﻿using System;
using System.Collections;
using System.IO;
using System.Windows.Forms;
using System.Xml.Serialization;

namespace Shturman.Nestor.DataExporter
{
    public enum FileType { XMLFile, CVSFile };
    /// <summary>
    /// Главное предназначение - сохранение списка объектов в файл
    /// заданного формата
    /// </summary>
    public class DataExporter
    {
        public static void Save(FileType fileType, Type elementType, IList list)
        {
            SaveFileDialog fileDlg = new SaveFileDialog();

            fileDlg.InitialDirectory = "\\";
            fileDlg.Filter = "xml files (*.xml)|*.xml|cvs files (*.cvs)|*.cvs";
            fileDlg.FilterIndex = 1;
            fileDlg.RestoreDirectory = true;


            if (fileDlg.ShowDialog() == DialogResult.OK)
            {
                switch (fileType)
                {
                    case FileType.XMLFile:
                        {
                            Stream myStream = File.Create(fileDlg.FileName);

                            // Копирую элементы списка в фиксированный массив
                            Array myTargetArray = Array.CreateInstance(elementType, list.Count);
                            list.CopyTo(myTargetArray, 0);

                            XmlSerializer myXmlSerializer = new XmlSerializer(myTargetArray.GetType());
                            myXmlSerializer.Serialize(myStream, myTargetArray);
                            myStream.Close();

                            break;
                        }
                    case FileType.CVSFile:
                        {
                            break;
                        }
                }
            }
        }
    }
}