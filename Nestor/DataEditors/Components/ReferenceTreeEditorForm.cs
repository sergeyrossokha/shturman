﻿using System;
using System.Collections;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace Shturman.Nestor.DataEditors
{
    /// <summary>
    /// Класс описывающий компонент, используемый в качестве выпадающего списка.
    /// </summary>
    public class ReferenceTreeEditorForm : TreeView
    {
        private bool _valueNotNull = false;
        /// <summary>
        /// Необходим для закрытия выпадающего списка при двойном клике, 
        /// а не только по нажатию клавиши Enter.
        /// </summary>
        private IWindowsFormsEditorService wfes;
        //private IObjectFactory objfactory = new ObjectFactory();
        public ReferenceTreeEditorForm(IWindowsFormsEditorService wfes)
            : base()
        {
            this.wfes = wfes;
            this.BorderStyle = BorderStyle.None;

            this.Height = 200;
        }

        //private string searchStr = "";
        //private void FindNode(TreeNode node, string filter)
        //{ 
        //	foreach(TreeNode tn in node.Nodes)
        //	{
        //	if (tn.Text.ToLower().StartsWith(filter.ToLower()))
        //	{
        //	this.SelectedNode = tn;
        //	break;
        //	}
        //	else
        //	if (tn.Nodes != null && tn.Nodes.Count>0 )
        //	FindNode(tn, filter);
        //	}
        //}

        protected override bool IsInputChar(char charCode)
        {
            /*if (char.IsLetterOrDigit( charCode )) 
            searchStr += charCode;

            if (searchStr.Length == 1)
            {
            foreach(TreeNode tn in Nodes)
            {
            if (tn.Text.ToLower().StartsWith(searchStr.ToLower()))
            {
            this.SelectedNode = tn;
            break;
            }
            else
            if (tn.Nodes != null && tn.Nodes.Count>0 )
            FindNode(tn, searchStr);
            }
            }
            else
            {
            TreeNode searchnode = this.SelectedNode;
            do
            {
            if (searchnode.Text.StartsWith(searchStr))
            {
            this.SelectedNode = searchnode;
            break;
            }
            searchnode = searchnode.NextNode;
            }while (searchnode.NextNode != null);
            }*/
            return base.IsInputChar(charCode);
        }

        protected override bool IsInputKey(Keys keyData)
        {
            //if (keyData == Keys.Escape)
            //	searchStr = "";

            return base.IsInputKey(keyData);
        }



        protected override void OnDoubleClick(EventArgs e)
        {
            base.OnDoubleClick(e);
            wfes.CloseDropDown();
        }

        public bool ValueNotNull
        {
            get { return _valueNotNull; }
            set { _valueNotNull = value; }
        }

        public void SetListForBrowsing(IList items, IList allAvaiblesItems)
        {
            if (!_valueNotNull)
                this.Nodes.Add("Нет значения");

            if (allAvaiblesItems.Count != 0)
            {
                TreeNode avaible = new TreeNode("Вероятные");
                foreach (object obj in allAvaiblesItems)
                {
                    if (obj != null)
                    {
                        TreeNode currentNode = new TreeNode(obj.ToString());
                        currentNode.Tag = obj;
                        avaible.Nodes.Add(currentNode);
                    }
                }
                this.Nodes.Add(avaible);
            }

            if (items.Count != 0)
            {
                TreeNode allvalues = new TreeNode("Все значения");
                foreach (object obj in items)
                {
                    if (obj != null)
                    {
                        TreeNode currentNode = new TreeNode(obj.ToString());
                        currentNode.Tag = obj;
                        allvalues.Nodes.Add(currentNode);
                    }
                }
                this.Nodes.Add(allvalues);
            }

            if (this.Nodes.Count > 1)
                this.Nodes[1].Expand();
            else
                if (this.Nodes.Count == 1)
                    this.Nodes[0].Expand();

        }

        public object SelectedItem
        {
            get
            {
                if (this.SelectedNode != null)
                    return this.SelectedNode.Tag;
                else
                    return null;
            }
        }
    }
}