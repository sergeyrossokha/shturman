﻿using System;
using System.Collections;
using System.Windows.Forms;
using System.Windows.Forms.Design;

namespace Shturman.Nestor.DataEditors
{
    /// <summary>
    /// Класс описывающий компонент, используемый в качестве выпадающего списка.
    /// </summary>
    public class ReferenceEditorForm : System.Windows.Forms.ListBox
    {
        private bool _valueNotNull = false;
        /// <summary>
        /// Необходим для закрытия выпадающего списка при двойном клике, 
        /// а не только по нажатию клавиши Enter.
        /// </summary>
        private IWindowsFormsEditorService wfes;
        //private IObjectFactory objfactory = new ObjectFactory();
        public ReferenceEditorForm(IWindowsFormsEditorService wfes)
            : base()
        {
            this.wfes = wfes;
            this.BorderStyle = BorderStyle.None;

        }

        protected override void OnDoubleClick(EventArgs e)
        {
            base.OnDoubleClick(e);
            wfes.CloseDropDown();
        }

        public bool ValueNotNull
        {
            get { return _valueNotNull; }
            set { _valueNotNull = value; }
        }

        public void SetListForBrowsing(IList items)
        {
            if (!_valueNotNull)
                this.Items.Add("(пусто)");
            foreach (object obj in items)
                this.Items.Add(obj);
        }
    }
}