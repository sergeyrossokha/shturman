﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using Shturman.Nestor.DataAttributes;
using Shturman.Nestor.Interfaces;

namespace Shturman.Nestor.DataEditors
{
    /// <summary>
    /// Редактор коллекций компонент, поддерживающий атрибут DisplayName
    /// </summary>
    public class CollectionUITypeEditor : UITypeEditor
    {
        public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider,
        object value)
        {
            Type collectionType;

            // проверяем и устанавливаем тип коллекции
            PropertyBrowserDetailAttribute pra = context.PropertyDescriptor.Attributes[typeof(PropertyBrowserDetailAttribute)] as PropertyBrowserDetailAttribute;
            if (pra != null)
                collectionType = pra.DetailType;
            else
                collectionType = typeof(object);

            IWindowsFormsEditorService edSvc = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
            if (edSvc == null)
                return null;

            //IList allValues = objfactory.ObjectList(pra.ReferenceType);	

            CollectionEditorForm frm = null;
            if (context.Instance is EntityWrapper)
            {
                IList availableValues = ((context.Instance as EntityWrapper).Instance as IBizObject).OnPropertyValues((context.Instance as EntityWrapper).Instance.GetType().GetProperty(context.PropertyDescriptor.Name));
                if (availableValues != null)
                    frm = new CollectionEditorForm(null, CollectionEditorType.AsCollectionEditor, collectionType, (IList)value, availableValues);
                else
                    frm = new CollectionEditorForm(null, CollectionEditorType.AsCollectionEditor, collectionType, (IList)value, null);
            }
            frm.ParentKey = context.Instance;

            frm.ShowBrowserButton = pra.NMRelation;

            edSvc.ShowDialog(frm);

            return (IList)value;//frm.Collection;
        }

        public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
        {
            return UITypeEditorEditStyle.Modal;
        }
    }
}