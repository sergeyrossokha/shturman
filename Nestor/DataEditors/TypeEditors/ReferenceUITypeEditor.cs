using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing.Design;
using System.Windows.Forms.Design;
using Shturman.Nestor.DataAttributes;
using Shturman.Nestor.Interfaces;

namespace Shturman.Nestor.DataEditors
{
	/// <summary>
	/// Summary description for ReferenceUITypeEditor.
	/// </summary>
	public class ReferenceUITypeEditor: UITypeEditor
	{		
		public override object EditValue(ITypeDescriptorContext context, IServiceProvider provider, object value)
		{
			IWindowsFormsEditorService edSvc = (IWindowsFormsEditorService)provider.GetService(typeof(IWindowsFormsEditorService));
			if( edSvc == null )
				return value;         
			// Displays a ReferenceEditor Form to get a reference 
			PropertyReferenceAttribute pra = context.PropertyDescriptor.Attributes[typeof(PropertyReferenceAttribute)] as PropertyReferenceAttribute;
			PropertyNotNullAttribute nna = context.PropertyDescriptor.Attributes[typeof(PropertyNotNullAttribute)] as PropertyNotNullAttribute;
			
			try
			{
				object obj = Activator.CreateInstance( pra.ReferenceType); 
				IObjectStorage objfactory = (obj as IBizObject).Store;

				ReferenceTreeEditorForm frm = new ReferenceTreeEditorForm(edSvc);			

				if (nna != null)
					frm.ValueNotNull = nna.NotNull;

				IList allValues = objfactory.ObjectList(pra.ReferenceType);

				if (context.Instance != null && context.Instance is EntityWrapper)
				{							
					IList availableValues = null;
					IBizObject bizObject = (context.Instance as EntityWrapper).Instance as IBizObject;
					if (bizObject != null)
						availableValues = bizObject.OnPropertyValues((context.Instance as EntityWrapper).Instance.GetType().GetProperty(context.PropertyDescriptor.Name));
					
					if (availableValues != null)
						frm.SetListForBrowsing( allValues, availableValues );
					else
						frm.SetListForBrowsing( allValues, new ArrayList() );
				}
				else
					frm.SetListForBrowsing( allValues, new ArrayList() );

				edSvc.DropDownControl(frm);
				if (frm.SelectedItem != null)
					if (frm.SelectedItem.GetType() != typeof(string) )
						return frm.SelectedItem;
					else
					{
						return null;
					}
				else
					return null;
			}
			catch
			{
				return null;
			}
		}

		public override UITypeEditorEditStyle GetEditStyle(ITypeDescriptorContext context)
		{
			return UITypeEditorEditStyle.DropDown;
		}
	}
}
