﻿using System;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using Shturman.Nestor.Interfaces;

namespace Shturman.Nestor.DataEditors
{
    /// <summary>
    /// Summary description for ObjectEditor.
    /// </summary>
    public class ObjectEditor : Form
    {
        private Panel panel1;
        private Panel panel2;
        private PropertyGrid propertyGrid1;
        private Button cancelButton;
        private Button okButton;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private Container components = null;
        private CheckBox checkBox1;

        private bool _onlyExitButton = false;
        private IObjectStorage objectFactory = null;
        public ObjectEditor(IObjectStorage objectFactory)
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            this.objectFactory = objectFactory;
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(ObjectEditor));
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.cancelButton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.propertyGrid1 = new System.Windows.Forms.PropertyGrid();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.checkBox1);
            this.panel1.Controls.Add(this.cancelButton);
            this.panel1.Controls.Add(this.okButton);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel1.Location = new System.Drawing.Point(0, 317);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(416, 32);
            this.panel1.TabIndex = 1;
            // 
            // checkBox1
            // 
            this.checkBox1.Location = new System.Drawing.Point(8, 9);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(136, 16);
            this.checkBox1.TabIndex = 2;
            this.checkBox1.Text = "Режим ввода данных";
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(336, 4);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.TabIndex = 1;
            this.cancelButton.Text = "Отмена";
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // okButton
            // 
            this.okButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(256, 4);
            this.okButton.Name = "okButton";
            this.okButton.TabIndex = 0;
            this.okButton.Text = "Да";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.propertyGrid1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(416, 317);
            this.panel2.TabIndex = 2;
            // 
            // propertyGrid1
            // 
            this.propertyGrid1.CommandsVisibleIfAvailable = true;
            this.propertyGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.propertyGrid1.LargeButtons = false;
            this.propertyGrid1.LineColor = System.Drawing.SystemColors.ScrollBar;
            this.propertyGrid1.Location = new System.Drawing.Point(0, 0);
            this.propertyGrid1.Name = "propertyGrid1";
            this.propertyGrid1.PropertySort = System.Windows.Forms.PropertySort.NoSort;
            this.propertyGrid1.Size = new System.Drawing.Size(416, 317);
            this.propertyGrid1.TabIndex = 1;
            this.propertyGrid1.Text = "PropertyGrid";
            this.propertyGrid1.ToolbarVisible = false;
            this.propertyGrid1.ViewBackColor = System.Drawing.SystemColors.Window;
            this.propertyGrid1.ViewForeColor = System.Drawing.SystemColors.WindowText;
            this.propertyGrid1.SelectedGridItemChanged += new System.Windows.Forms.SelectedGridItemChangedEventHandler(this.propertyGrid1_SelectedGridItemChanged);
            this.propertyGrid1.SelectedObjectsChanged += new System.EventHandler(this.propertyGrid1_SelectedObjectsChanged);
            // 
            // ObjectEditor
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(416, 349);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "ObjectEditor";
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Редактирование";
            this.Load += new System.EventHandler(this.ObjectEditor_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion

        private void propertyGrid1_SelectedGridItemChanged(object sender, SelectedGridItemChangedEventArgs e)
        {
        }

        private void propertyGrid1_SelectedObjectsChanged(object sender, EventArgs e)
        {

        }

        private void ObjectEditor_Load(object sender, EventArgs e)
        {
            PropertyInfo controlsProp = propertyGrid1.GetType().GetProperty("Controls");
            Control.ControlCollection cc = controlsProp.GetValue(propertyGrid1, null) as Control.ControlCollection;
            foreach (Control c in cc)
            {
                if (c.GetType().Name == "PropertyGridView")
                {
                    FieldInfo GridEntitiesField = c.GetType().GetField("allGridEntries", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly);
                    GridItemCollection oItemCollection = (GridItemCollection)GridEntitiesField.GetValue(c);

                    if (oItemCollection == null)
                    {
                        return;
                    }
                    Graphics oGraphics = Graphics.FromHwnd(propertyGrid1.Handle);
                    int CurWidth = 0;
                    int MaxWidth = 0;

                    foreach (GridItem oItem in oItemCollection)
                    {
                        if (oItem.GridItemType == GridItemType.Property)
                        {
                            CurWidth = (int)oGraphics.MeasureString(oItem.Label, this.Font).Width + 32;
                            if (CurWidth > MaxWidth)
                            {
                                MaxWidth = CurWidth;
                            }
                        }
                    }

                    MethodInfo mst = c.GetType().GetMethod("MoveSplitterTo", BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.DeclaredOnly);

                    mst.Invoke(c, new object[] { MaxWidth });
                    break;
                }
            }
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            if (!this.Modal)
                this.Close();
        }

        #region ObjectEditorCode
        public object SelectedObject
        {
            set
            {
                propertyGrid1.SelectedObject = new EntityWrapper(value);
                //Type t = value.GetType();
                //EntityCaptionAttribute eca = //as EntityCaptionAttribute;
                //if (eca != null)
                //	Text = eca.EntityCaption;
            }
        }

        #endregion

        public IObjectStorage ObjectFactory
        {
            get { return this.objectFactory; }
        }

        public bool InputMode
        {
            get { return this.checkBox1.Checked; }
        }

        public bool CanChangeInputMode
        {
            get { return this.checkBox1.Visible; }
            set { this.checkBox1.Visible = value; }
        }

        public bool OnlyExitButton
        {
            get { return _onlyExitButton; }
            set
            {
                _onlyExitButton = value;
                if (_onlyExitButton)
                {
                    okButton.Visible = false;
                    cancelButton.Text = "Закрыть";
                }
                else
                {
                    okButton.Visible = true;
                    cancelButton.Text = "Отмена";
                }
            }
        }
    }
}