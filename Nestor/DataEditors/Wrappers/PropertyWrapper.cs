﻿using System;
using System.ComponentModel;
using System.Collections;

using System.Reflection;

using Shturman.Nestor.DataAttributes;
using Shturman.MultyLanguage;


namespace Shturman.Nestor.DataEditors
{
    internal enum PropertyWrapperState { PropertyWrapperForDataGrid, PropertyWrapperForPropertyGrid };
    /// <summary>
    /// Summary description for PropertyWrapper.
    /// </summary>
    public class PropertyWrapper : PropertyDescriptor, IComparable
    {
        private PropertyWrapperState propertyWrapperState;
        private PropertyDescriptor property;

        public PropertyWrapper(string name, PropertyInfo propInfo)
            : base(name, null)
        {
            propertyWrapperState = PropertyWrapperState.PropertyWrapperForDataGrid;
            if (propInfo.DeclaringType != null)
                foreach (PropertyDescriptor propertyDescriptor in TypeDescriptor.GetProperties(propInfo.DeclaringType))
                {
                    if (propertyDescriptor.Name.Equals(propInfo.Name))
                        property = propertyDescriptor;
                }
        }

        public PropertyWrapper(PropertyDescriptor propDesc)
            : base(propDesc)
        {
            propertyWrapperState = PropertyWrapperState.PropertyWrapperForPropertyGrid;
            property = propDesc;
        }

        public override string Category
        {
            get
            {
                PropertyCategoryAttribute mna =
                property.Attributes[typeof(PropertyCategoryAttribute)] as PropertyCategoryAttribute;
                if (mna != null)
                {
                    string localizeStr = Vocabruary.Translate(mna.Category);
                    if (localizeStr != null)
                        return localizeStr;
                    else
                        return mna.Category;
                }
                return property.Category;
            }
        }

        public override string Description
        {
            get
            {
                string descriptionStr = base.Description;

                string localizeStr = Vocabruary.Translate(descriptionStr);
                if (localizeStr != null)
                    return localizeStr;
                else
                    return descriptionStr;
            }
        }


        // Это свойство возвращает название свойства, 
        // отображаемое в propertyGrid
        private string _displayName = string.Empty;
        public override string DisplayName
        {
            get
            {
                if (_displayName != string.Empty)
                    return _displayName;
                else
                {
                    // Пытаемся получить атрибут PropertyCaptionAttribute.
                    // В случае неудачи будет возвращен null.
                    PropertyCaptionAttribute mna =
                    property.Attributes[typeof(PropertyCaptionAttribute)] as PropertyCaptionAttribute;
                    if (mna != null)
                    {
                        // Если имеется атрибут PropertyCaptionAttribute,
                        // возвращаем текст, помещенный в него.
                        string localizeStr = Vocabruary.Translate(mna.Caption);
                        if (localizeStr != null)
                            return localizeStr;
                        else
                            return mna.Caption;
                    }
                    // Если атрибут PropertyCaptionAttribute не задан,
                    // возвращаем оригинальное имя свойства.
                    return property.Name;
                }
            }
        }

        public override string Name
        {
            get
            {
                if (propertyWrapperState == PropertyWrapperState.PropertyWrapperForDataGrid)
                {
                    return this.DisplayName;
                }
                else
                {
                    return base.Name;
                }
            }
        }

        public string OriginName
        {
            get
            {
                return base.Name;
            }
        }

        public override Type ComponentType
        {
            get
            {
                return property.ComponentType;
            }
        }

        public override bool IsReadOnly
        {
            get
            {
                // Пытаемся получить атрибут PropertyReadOnlyAttribute.
                // В случае неудачи будет возвращен null.
                PropertyReadOnlyAttribute roa =
                property.Attributes[typeof(PropertyReadOnlyAttribute)] as PropertyReadOnlyAttribute;

                /// Если имеется атрибут PropertyReadOnlyAttribute,
                /// возвращаем текст, помещенный в него.
                if (roa != null)
                    return roa.IsReadOnly;
                return false;
            }
        }

        /*Ошибка: Данный метод не вызывается;
        * Пока не ясно с чем это может быть связано! Поэтому данный метод,
        * реализован пока в конструкторе....
        * 
        * самое интерестное, это то, что при работе с datagrid, это свойство вызывается
        * вполне нормально. ????????
        * 
        * Очень жаль, что приходится так извращатся... :(
        * 
        *public override bool IsBrowsable
        *{
        *	get
        *	{
        *	// Пытаемся получить атрибут PropertyVisibleAttribute.
        *	// В случае неудачи будет возвращен null.
        *	PropertyVisibleAttribute mna = 
        * property.Attributes[typeof(PropertyVisibleAttribute)] as PropertyVisibleAttribute;
        *	// Если имеется атрибут BrowsableAttribute,
        *	// возвращаем текст, помещенный в него.
        *	if(mna != null)	
        *	return mna.IsVisible;
        *	else return false;
        *	}
        *}
        */
        public override Type PropertyType
        {
            get
            {
                return property.PropertyType;
            }
        }

        public override bool CanResetValue(object component)
        {

            if (component is EntityWrapper)
                return property.CanResetValue(((EntityWrapper)component).Instance);
            else
                return property.CanResetValue(component);
        }

        public override object GetValue(object component)
        {

            if (component is EntityWrapper)
                return property.GetValue(((EntityWrapper)component).Instance);
            else
                return property.GetValue(component);
        }

        public override void ResetValue(object component)
        {
            if (component is EntityWrapper)
                property.ResetValue(((EntityWrapper)component).Instance);
            else
                property.ResetValue(component);
        }

        public override void SetValue(object component, object value)
        {
            if (component is EntityWrapper)
                property.SetValue(((EntityWrapper)component).Instance, value);
            else
                property.SetValue(component, value);
        }

        public override bool ShouldSerializeValue(object component)
        {
            if (component is EntityWrapper)
                return property.ShouldSerializeValue(((EntityWrapper)component).Instance);
            else
                return property.ShouldSerializeValue(component);
        }

        public override object GetEditor(Type editorBaseType)
        {
            if (typeof(IList).IsAssignableFrom(property.PropertyType) &&
            (property.PropertyType != typeof(string[])))
                return new CollectionUITypeEditor();
            else
                if (property.PropertyType.GetInterface("IBizObject", true) != null)
                    return new ReferenceUITypeEditor();
                else
                    return base.GetEditor(editorBaseType);
        }

        public override TypeConverter Converter
        {
            get
            {
                TypeConverter conv = null;
                if (typeof(IList).IsAssignableFrom(property.PropertyType))
                    conv = new CollectionTypeConverter();
                else
                    //	if (property.PropertyType.IsSubclassOf(typeof(Persistent)))
                    //	conv = new ReferenceTypeConverter();
                    //else 
                    conv = base.Converter;
                return conv;//new ConverterWrapper(conv);
            }
        }

        #region IComparable Members

        public int CompareTo(object obj)
        {
            if (obj is PropertyWrapper)
                return new PropertyGridSortComparer().Compare(this, obj);
            else
                return Comparer.Default.Compare(this, obj);
            throw new Exception("The method or operation is not implemented.");
        }

        #endregion
    }
}