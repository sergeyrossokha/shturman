﻿using System;
using System.Collections;
using System.ComponentModel;

using Shturman.Nestor.DataAttributes;

namespace Shturman.Nestor.DataEditors
{
    /// <summary>
    /// Класс необходим для оборачивания конкретного объекта, и 
    /// его подготовки для отображения в PropertyGrid
    /// </summary>
    public class EntityWrapper : ICustomTypeDescriptor
    {
        // Оборачиваемый экземпляр сущности.
        private object instance;

        // Коллекция, хранящая обертки над описаниями свойств.
        private PropertyDescriptorCollection propertyCollection;

        // Позволяет получить обернутый объект.
        public object Instance { get { return instance; } }


        public EntityWrapper(object instance)
        {
            // Запоминам оборачиваемый объект.
            this.instance = instance;

            // Создаем новую (пустую) колекцию описаний свойсв 
            // в которую поместим обертки над реальными описаниями.
            propertyCollection = new PropertyDescriptorCollection(null);
            PropertyDescriptorCollection pdc = TypeDescriptor.GetProperties(this.instance, true);
            // Перебираем описания свойств, создаем для каждого 
            // из них обертку и помещаем ее в коллекцию.

            ArrayList al = new ArrayList();

            foreach (PropertyDescriptor pd in pdc)
            {
                /*
                * Из-за того, что не удаётся перекрыть свойство IsBrowsable в 
                * файле PropertyWrapper.cs приходится фильтровать и отбрасывате те свойства,
                * которые не имеют аттрибута PropertyVisible установленного в true.
                * 
                * Понимаю, что это извращение, но ничего поделать не могу :(
                * 
                * Россоха С.В. 14.07.2005
                * */

                // Пытаемся получить атрибут PropertyVisibleAttribute.
                // В случае неудачи будет возвращен null.
                PropertyVisibleAttribute mna =
                pd.Attributes[typeof(PropertyVisibleAttribute)] as PropertyVisibleAttribute;
                // Если имеется атрибут BrowsableAttribute,
                // возвращаем текст, помещенный в него.
                if (mna != null && mna.IsVisible)
                    al.Add(pd);
            }

            al.Sort(0, al.Count, new PropertyGridSortComparer());

            for (int index = 0; index < al.Count; index++)
                propertyCollection.Add(new PropertyWrapper((PropertyDescriptor)al[index]));

        }

        #region ICustomTypeDescriptor implementation
        AttributeCollection ICustomTypeDescriptor.GetAttributes()
        {
            return new AttributeCollection(null);
        }

        string ICustomTypeDescriptor.GetClassName()
        {
            return null;
        }

        string ICustomTypeDescriptor.GetComponentName()
        {
            return null;
        }

        TypeConverter ICustomTypeDescriptor.GetConverter()
        {
            return null;
        }

        EventDescriptor ICustomTypeDescriptor.GetDefaultEvent()
        {
            return null;
        }


        PropertyDescriptor ICustomTypeDescriptor.GetDefaultProperty()
        {
            return null;
        }

        object ICustomTypeDescriptor.GetEditor(Type editorBaseType)
        {
            return null;
        }

        EventDescriptorCollection ICustomTypeDescriptor.GetEvents()
        {
            return new EventDescriptorCollection(null);
        }

        EventDescriptorCollection ICustomTypeDescriptor.GetEvents
        (Attribute[] attributes)
        {
            return new EventDescriptorCollection(null);
        }

        PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties()
        {
            return propertyCollection;
        }

        PropertyDescriptorCollection ICustomTypeDescriptor.GetProperties(
        Attribute[] attributes)
        {
            return propertyCollection;
        }

        object ICustomTypeDescriptor.GetPropertyOwner(PropertyDescriptor pd)
        {
            return this;
        }
        #endregion

    }
}