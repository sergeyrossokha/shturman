﻿// My Assembles
using System;
using System.Collections;
using System.ComponentModel;
using System.Reflection;
using System.Windows.Forms;

using Shturman.MultyLanguage;

using Shturman.Nestor.DataAttributes;
using Shturman.Nestor.DataEditors;
using Shturman.Nestor.DataExporter;
using Shturman.Nestor.DataFilter;
using Shturman.Nestor.DataLists;
using Shturman.Nestor.Interfaces;

namespace Shturman.Nestor.DataBrowser
{
    /// <summary>
    /// Summary description for Browser.
    /// </summary>
    public class Browser : Form
    {
        public delegate void AddMasterItemEvent(object item);

        public event AddMasterItemEvent OnAddItem;

        public delegate void DelMasterItemEvent(object item);

        public event DelMasterItemEvent OnDelItem;

        public delegate void ChangeMasterItemEvent(object item);

        public event ChangeMasterItemEvent OnChangeItem;

        #region System Variables

        private ImageList imageList;
        private IContainer components;

        #endregion

        #region User defined variables

        private Type MasterType = null;
        private Type DetailType = null;
        private ArrayList DetailTypes = new ArrayList();

        private PropertyInfo DetailProp = null;
        private ArrayList DetailPropList = new ArrayList();
        private bool CreateNew = false;
        private bool DeleteManual = false;
        private bool DetailSelected = false;

        //private CurrencyManager detailTypesCurrencyManager;

        private IList masterList;
        private CurrencyManager masterCurrencyManager;

        private IList detailList;
        private CurrencyManager detailCurrencyManager;

        /// <summary>
        /// Фабрика объектов, позволяет добавлять, удалять, изменять объекты в базе данных
        /// </summary>
        private IObjectStorage objectStorage = null;

        private ToolBarButton toolBarButton13;
        private ContextMenu contextMenu1;
        private MenuItem menuItemNewObject;
        private MenuItem menuItemNewLikeObject;
        private Panel masterPanel;
        private DataGrid dataGrid;
        private ToolBar toolBar;
        private ToolBarButton NewBtn;
        private ToolBarButton EditBtn;
        private ToolBarButton DeleteBtn;
        private ToolBarButton separator1;
        private ToolBarButton RefreshBtn;
        private ToolBarButton separator2;
        private ToolBarButton FilterBtn;
        private ToolBarButton CanselFilterBtn;
        private ToolBarButton separator3;
        private ToolBarButton SaveBtn;
        private ToolBarButton PrintBtn;
        private ToolBarButton separator4;
        private ToolBarButton helpBtn;
        private Splitter masterDetailSplitter;
        private Panel detailPanel;
        private ComboBox SubObjectsBox;
        private DataGrid detailGrid;
        private ToolBar detailBar;
        private ToolBarButton toolBarButton1;
        private ToolBarButton toolBarButton2;
        private ToolBarButton toolBarButton3;
        private ToolBarButton toolBarButton4;
        private ToolBarButton toolBarButton5;
        private ToolBarButton toolBarButton6;
        private ToolBarButton toolBarButton7;
        private ToolBarButton toolBarButton9;
        private ToolBarButton toolBarButton8;
        private ToolBarButton toolBarButton10;
        private ToolBarButton toolBarButton11;
        private ToolBarButton toolBarButton12;
        private Panel panelMain;

        #endregion

        private void ShowOrHideDetailPanel(bool visible)
        {
            if (visible)
            {
                detailPanel.Visible = false;
                masterDetailSplitter.Visible = false;
                this.Refresh();
            }
            else
            {
                masterDetailSplitter.Visible = true;
                detailPanel.Visible = true;
                this.Refresh();
            }
        }

        public Browser(IObjectStorage objectFactory, Type elementType, IList elementList)
        {
            InitializeComponent();

            this.objectStorage = objectFactory;

            MasterType = elementType;
            masterList = new ListWrapper(elementType, elementList);
            //(masterList as ListWrapper).Sort(1);
            this.dataGrid.DataSource = masterList;
            masterCurrencyManager = (CurrencyManager)dataGrid.BindingContext[masterList];

            #region Set New Button Style

            if (MasterType.GetInterface("ICloneable", true) != null)
            {
                toolBar.Buttons[0].Style = ToolBarButtonStyle.DropDownButton;
                //toolBar.Buttons[0].DropDownMenu = this.contextMenu1;
            }
            else
            {
                toolBar.Buttons[0].Style = ToolBarButtonStyle.PushButton;
                //toolBar.Buttons[0].DropDownMenu = null;
            }

            #endregion

            #region hold the avaible sublist ComboBox

            DetailTypes.Clear();
            SubObjectsBox.Items.Clear();

            PropertyInfo[] pi = MasterType.GetProperties();
            foreach (PropertyInfo property in pi)
            {
                object[] BrowserDetailAttrs = property.GetCustomAttributes(typeof(PropertyBrowserDetailAttribute), false);

                foreach (PropertyBrowserDetailAttribute pba in BrowserDetailAttrs)
                {
                    DetailPropList.Add(property);
                    DetailTypes.Add(pba.DetailType);

                    object[] CaptionAttrs = property.GetCustomAttributes(typeof(PropertyCaptionAttribute), false);

                    foreach (PropertyCaptionAttribute pca in CaptionAttrs)
                    {
                        // Try To Localize String
                        string localizeStr = Vocabruary.Translate(pca.Caption);
                        if (localizeStr != null)
                            SubObjectsBox.Items.Add(localizeStr);
                        else
                            SubObjectsBox.Items.Add(pca.Caption);
                    }
                }
            }

            ShowOrHideDetailPanel(DetailTypes.Count == 0);

            #endregion
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Browser));
            this.contextMenu1 = new System.Windows.Forms.ContextMenu();
            this.menuItemNewObject = new System.Windows.Forms.MenuItem();
            this.menuItemNewLikeObject = new System.Windows.Forms.MenuItem();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.toolBarButton13 = new System.Windows.Forms.ToolBarButton();
            this.masterPanel = new System.Windows.Forms.Panel();
            this.dataGrid = new System.Windows.Forms.DataGrid();
            this.toolBar = new System.Windows.Forms.ToolBar();
            this.NewBtn = new System.Windows.Forms.ToolBarButton();
            this.EditBtn = new System.Windows.Forms.ToolBarButton();
            this.DeleteBtn = new System.Windows.Forms.ToolBarButton();
            this.separator1 = new System.Windows.Forms.ToolBarButton();
            this.RefreshBtn = new System.Windows.Forms.ToolBarButton();
            this.separator2 = new System.Windows.Forms.ToolBarButton();
            this.FilterBtn = new System.Windows.Forms.ToolBarButton();
            this.CanselFilterBtn = new System.Windows.Forms.ToolBarButton();
            this.separator3 = new System.Windows.Forms.ToolBarButton();
            this.SaveBtn = new System.Windows.Forms.ToolBarButton();
            this.PrintBtn = new System.Windows.Forms.ToolBarButton();
            this.separator4 = new System.Windows.Forms.ToolBarButton();
            this.helpBtn = new System.Windows.Forms.ToolBarButton();
            this.masterDetailSplitter = new System.Windows.Forms.Splitter();
            this.detailPanel = new System.Windows.Forms.Panel();
            this.SubObjectsBox = new System.Windows.Forms.ComboBox();
            this.detailGrid = new System.Windows.Forms.DataGrid();
            this.detailBar = new System.Windows.Forms.ToolBar();
            this.toolBarButton1 = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton2 = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton3 = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton4 = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton5 = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton6 = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton7 = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton9 = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton8 = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton10 = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton11 = new System.Windows.Forms.ToolBarButton();
            this.toolBarButton12 = new System.Windows.Forms.ToolBarButton();
            this.panelMain = new System.Windows.Forms.Panel();
            this.masterPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid)).BeginInit();
            this.detailPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.detailGrid)).BeginInit();
            this.panelMain.SuspendLayout();
            this.SuspendLayout();
            // 
            // contextMenu1
            // 
            this.contextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuItemNewObject,
this.menuItemNewLikeObject});
            // 
            // menuItemNewObject
            // 
            this.menuItemNewObject.Index = 0;
            this.menuItemNewObject.Text = "Новая запись";
            this.menuItemNewObject.Click += new System.EventHandler(this.menuItemNewObject_Click);
            // 
            // menuItemNewLikeObject
            // 
            this.menuItemNewLikeObject.Index = 1;
            this.menuItemNewLikeObject.Text = "Подобная запись";
            this.menuItemNewLikeObject.Click += new System.EventHandler(this.menuItemNewLikeObject_Click);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "");
            this.imageList.Images.SetKeyName(1, "");
            this.imageList.Images.SetKeyName(2, "");
            this.imageList.Images.SetKeyName(3, "");
            this.imageList.Images.SetKeyName(4, "");
            this.imageList.Images.SetKeyName(5, "");
            this.imageList.Images.SetKeyName(6, "");
            this.imageList.Images.SetKeyName(7, "");
            this.imageList.Images.SetKeyName(8, "");
            // 
            // toolBarButton13
            // 
            this.toolBarButton13.ImageIndex = 4;
            this.toolBarButton13.Name = "toolBarButton13";
            this.toolBarButton13.Text = "Отменить фильтр";
            this.toolBarButton13.ToolTipText = "Отменить фильтр";
            // 
            // masterPanel
            // 
            this.masterPanel.BackColor = System.Drawing.SystemColors.Control;
            this.masterPanel.Controls.Add(this.dataGrid);
            this.masterPanel.Controls.Add(this.toolBar);
            this.masterPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.masterPanel.Location = new System.Drawing.Point(0, 0);
            this.masterPanel.Name = "masterPanel";
            this.masterPanel.Size = new System.Drawing.Size(720, 186);
            this.masterPanel.TabIndex = 15;
            // 
            // dataGrid
            // 
            this.dataGrid.AllowNavigation = false;
            this.dataGrid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.dataGrid.DataMember = "";
            this.dataGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGrid.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.dataGrid.Location = new System.Drawing.Point(0, 28);
            this.dataGrid.Name = "dataGrid";
            this.dataGrid.Size = new System.Drawing.Size(720, 158);
            this.dataGrid.TabIndex = 16;
            this.dataGrid.CurrentCellChanged += new System.EventHandler(this.dataGrid_CurrentCellChanged);
            this.dataGrid.BindingContextChanged += new System.EventHandler(this.dataGrid_BindingContextChanged);
            // 
            // toolBar
            // 
            this.toolBar.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
            this.toolBar.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
this.NewBtn,
this.EditBtn,
this.DeleteBtn,
this.separator1,
this.RefreshBtn,
this.separator2,
this.FilterBtn,
this.CanselFilterBtn,
this.separator3,
this.SaveBtn,
this.PrintBtn,
this.separator4,
this.helpBtn});
            this.toolBar.ButtonSize = new System.Drawing.Size(23, 22);
            this.toolBar.DropDownArrows = true;
            this.toolBar.ImageList = this.imageList;
            this.toolBar.Location = new System.Drawing.Point(0, 0);
            this.toolBar.Name = "toolBar";
            this.toolBar.ShowToolTips = true;
            this.toolBar.Size = new System.Drawing.Size(720, 28);
            this.toolBar.TabIndex = 15;
            this.toolBar.ButtonClick += new System.Windows.Forms.ToolBarButtonClickEventHandler(this.toolBar_ButtonClick);
            // 
            // NewBtn
            // 
            this.NewBtn.DropDownMenu = this.contextMenu1;
            this.NewBtn.ImageIndex = 0;
            this.NewBtn.Name = "NewBtn";
            this.NewBtn.Style = System.Windows.Forms.ToolBarButtonStyle.DropDownButton;
            this.NewBtn.ToolTipText = "Создать";
            // 
            // EditBtn
            // 
            this.EditBtn.ImageIndex = 1;
            this.EditBtn.Name = "EditBtn";
            this.EditBtn.ToolTipText = "Редактировать";
            // 
            // DeleteBtn
            // 
            this.DeleteBtn.ImageIndex = 2;
            this.DeleteBtn.Name = "DeleteBtn";
            this.DeleteBtn.ToolTipText = "Удалить";
            // 
            // separator1
            // 
            this.separator1.Name = "separator1";
            this.separator1.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // RefreshBtn
            // 
            this.RefreshBtn.ImageIndex = 6;
            this.RefreshBtn.Name = "RefreshBtn";
            this.RefreshBtn.ToolTipText = "Обновить";
            // 
            // separator2
            // 
            this.separator2.Name = "separator2";
            this.separator2.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // FilterBtn
            // 
            this.FilterBtn.ImageIndex = 3;
            this.FilterBtn.Name = "FilterBtn";
            this.FilterBtn.ToolTipText = "Фильтр";
            // 
            // CanselFilterBtn
            // 
            this.CanselFilterBtn.ImageIndex = 4;
            this.CanselFilterBtn.Name = "CanselFilterBtn";
            // 
            // separator3
            // 
            this.separator3.Name = "separator3";
            this.separator3.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // SaveBtn
            // 
            this.SaveBtn.ImageIndex = 5;
            this.SaveBtn.Name = "SaveBtn";
            // 
            // PrintBtn
            // 
            this.PrintBtn.ImageIndex = 6;
            this.PrintBtn.Name = "PrintBtn";
            this.PrintBtn.ToolTipText = "Печать";
            this.PrintBtn.Visible = false;
            // 
            // separator4
            // 
            this.separator4.Name = "separator4";
            this.separator4.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            this.separator4.Visible = false;
            // 
            // helpBtn
            // 
            this.helpBtn.ImageIndex = 7;
            this.helpBtn.Name = "helpBtn";
            this.helpBtn.ToolTipText = "Помощь";
            this.helpBtn.Visible = false;
            // 
            // masterDetailSplitter
            // 
            this.masterDetailSplitter.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.masterDetailSplitter.Location = new System.Drawing.Point(0, 186);
            this.masterDetailSplitter.Name = "masterDetailSplitter";
            this.masterDetailSplitter.Size = new System.Drawing.Size(720, 3);
            this.masterDetailSplitter.TabIndex = 14;
            this.masterDetailSplitter.TabStop = false;
            // 
            // detailPanel
            // 
            this.detailPanel.BackColor = System.Drawing.SystemColors.Control;
            this.detailPanel.Controls.Add(this.SubObjectsBox);
            this.detailPanel.Controls.Add(this.detailGrid);
            this.detailPanel.Controls.Add(this.detailBar);
            this.detailPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.detailPanel.Location = new System.Drawing.Point(0, 189);
            this.detailPanel.Name = "detailPanel";
            this.detailPanel.Size = new System.Drawing.Size(720, 184);
            this.detailPanel.TabIndex = 13;
            // 
            // SubObjectsBox
            // 
            this.SubObjectsBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.SubObjectsBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.SubObjectsBox.ImeMode = System.Windows.Forms.ImeMode.NoControl;
            this.SubObjectsBox.IntegralHeight = false;
            this.SubObjectsBox.ItemHeight = 13;
            this.SubObjectsBox.Location = new System.Drawing.Point(544, 4);
            this.SubObjectsBox.Name = "SubObjectsBox";
            this.SubObjectsBox.Size = new System.Drawing.Size(176, 21);
            this.SubObjectsBox.TabIndex = 12;
            this.SubObjectsBox.SelectedIndexChanged += new System.EventHandler(this.SubObjectsBox_SelectedIndexChanged);
            // 
            // detailGrid
            // 
            this.detailGrid.AllowDrop = true;
            this.detailGrid.AllowNavigation = false;
            this.detailGrid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.detailGrid.DataMember = "";
            this.detailGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.detailGrid.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.detailGrid.Location = new System.Drawing.Point(0, 28);
            this.detailGrid.Name = "detailGrid";
            this.detailGrid.Size = new System.Drawing.Size(720, 156);
            this.detailGrid.TabIndex = 11;
            this.detailGrid.DragEnter += new System.Windows.Forms.DragEventHandler(this.detailGrid_DragEnter);
            this.detailGrid.DragDrop += new System.Windows.Forms.DragEventHandler(this.detailGrid_DragDrop);
            this.detailGrid.MouseMove += new System.Windows.Forms.MouseEventHandler(this.detailGrid_MouseMove);
            // 
            // detailBar
            // 
            this.detailBar.Appearance = System.Windows.Forms.ToolBarAppearance.Flat;
            this.detailBar.Buttons.AddRange(new System.Windows.Forms.ToolBarButton[] {
this.toolBarButton1,
this.toolBarButton2,
this.toolBarButton3,
this.toolBarButton4,
this.toolBarButton5,
this.toolBarButton6,
this.toolBarButton7,
this.toolBarButton9,
this.toolBarButton8,
this.toolBarButton10,
this.toolBarButton11,
this.toolBarButton12});
            this.detailBar.ButtonSize = new System.Drawing.Size(23, 22);
            this.detailBar.DropDownArrows = true;
            this.detailBar.ImageList = this.imageList;
            this.detailBar.Location = new System.Drawing.Point(0, 0);
            this.detailBar.Name = "detailBar";
            this.detailBar.ShowToolTips = true;
            this.detailBar.Size = new System.Drawing.Size(720, 28);
            this.detailBar.TabIndex = 10;
            this.detailBar.ButtonClick += new System.Windows.Forms.ToolBarButtonClickEventHandler(this.DetailBar_ButtonClick);
            // 
            // toolBarButton1
            // 
            this.toolBarButton1.ImageIndex = 0;
            this.toolBarButton1.Name = "toolBarButton1";
            this.toolBarButton1.ToolTipText = "Создать";
            // 
            // toolBarButton2
            // 
            this.toolBarButton2.ImageIndex = 1;
            this.toolBarButton2.Name = "toolBarButton2";
            this.toolBarButton2.ToolTipText = "Редактировать";
            // 
            // toolBarButton3
            // 
            this.toolBarButton3.ImageIndex = 2;
            this.toolBarButton3.Name = "toolBarButton3";
            this.toolBarButton3.ToolTipText = "Удалить";
            // 
            // toolBarButton4
            // 
            this.toolBarButton4.Name = "toolBarButton4";
            this.toolBarButton4.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // toolBarButton5
            // 
            this.toolBarButton5.ImageIndex = 6;
            this.toolBarButton5.Name = "toolBarButton5";
            this.toolBarButton5.ToolTipText = "Обновить";
            // 
            // toolBarButton6
            // 
            this.toolBarButton6.Name = "toolBarButton6";
            this.toolBarButton6.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            // 
            // toolBarButton7
            // 
            this.toolBarButton7.ImageIndex = 3;
            this.toolBarButton7.Name = "toolBarButton7";
            this.toolBarButton7.ToolTipText = "Фильтр";
            // 
            // toolBarButton9
            // 
            this.toolBarButton9.ImageIndex = 4;
            this.toolBarButton9.Name = "toolBarButton9";
            // 
            // toolBarButton8
            // 
            this.toolBarButton8.Name = "toolBarButton8";
            this.toolBarButton8.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            this.toolBarButton8.Visible = false;
            // 
            // toolBarButton10
            // 
            this.toolBarButton10.ImageIndex = 6;
            this.toolBarButton10.Name = "toolBarButton10";
            this.toolBarButton10.ToolTipText = "Печать";
            this.toolBarButton10.Visible = false;
            // 
            // toolBarButton11
            // 
            this.toolBarButton11.Name = "toolBarButton11";
            this.toolBarButton11.Style = System.Windows.Forms.ToolBarButtonStyle.Separator;
            this.toolBarButton11.Visible = false;
            // 
            // toolBarButton12
            // 
            this.toolBarButton12.ImageIndex = 7;
            this.toolBarButton12.Name = "toolBarButton12";
            this.toolBarButton12.ToolTipText = "Помощь";
            this.toolBarButton12.Visible = false;
            // 
            // panelMain
            // 
            this.panelMain.Controls.Add(this.masterPanel);
            this.panelMain.Controls.Add(this.masterDetailSplitter);
            this.panelMain.Controls.Add(this.detailPanel);
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(0, 0);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(720, 373);
            this.panelMain.TabIndex = 3;
            // 
            // Browser
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(720, 373);
            this.Controls.Add(this.panelMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(560, 304);
            this.Name = "Browser";
            this.Text = "Навигатор";
            this.Load += new System.EventHandler(this.Browser_Load);
            this.masterPanel.ResumeLayout(false);
            this.masterPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGrid)).EndInit();
            this.detailPanel.ResumeLayout(false);
            this.detailPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.detailGrid)).EndInit();
            this.panelMain.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private void SubObjectsBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (masterList.Count == 0)
            {
                (sender as ComboBox).SelectedIndex = -1;
                return;
            }
            DetailType = DetailTypes[(sender as ComboBox).SelectedIndex] as Type;
            DetailProp = DetailPropList[(sender as ComboBox).SelectedIndex] as PropertyInfo;

            //bool CreateNew;

            object[] CreateAttrs = DetailProp.GetCustomAttributes(typeof(PropertyInsertListItemAttribute), false);
            if (CreateAttrs.Length == 1)
            {
                if ((CreateAttrs[0] as PropertyInsertListItemAttribute).InsertMode == InsertListItemMode.imNewItem)
                {
                    CreateNew = true;
                }
                else
                {
                    CreateNew = false;
                }
            }

            //PropertyRemoveListItem(RemoveListItemMode.rmRemoveItem)
            object[] DeleteAttrs = DetailProp.GetCustomAttributes(typeof(PropertyRemoveListItemAttribute), false);
            if (DeleteAttrs.Length == 1)
            {
                if ((DeleteAttrs[0] as PropertyRemoveListItemAttribute).RemoveMode == RemoveListItemMode.rmRemoveItem)
                {
                    DeleteManual = true;
                }
                else
                {
                    DeleteManual = false;
                }
            }

            object[] BrowserDetailAttrs = DetailProp.GetCustomAttributes(typeof(PropertyBrowserDetailAttribute), false);
            if (BrowserDetailAttrs.Length == 1)
            {
                if ((BrowserDetailAttrs[0] as PropertyBrowserDetailAttribute).NMRelation)
                {
                    DetailSelected = true;
                    this.toolBarButton2.Visible = false;
                }
                else
                {
                    DetailSelected = false;
                    this.toolBarButton2.Visible = true;
                }
            }
            detailGrid.Tag = detailGrid.CaptionText = (sender as ComboBox).Text;
            dataGrid_CurrentCellChanged(sender, e);
        }

        private void dataGrid_CurrentCellChanged(object sender, EventArgs e)
        {
            if (SubObjectsBox.SelectedIndex != -1)
            {
                /// следующую строку необходимо изменить на вызов свойства выделенного объекта

                //detailList = new ListWrapper(DetailType, this.objectStorage.ObjectList(DetailType, masterCurrencyManager.Current ));
                detailList = new ListWrapper(DetailType, DetailProp.GetValue(masterCurrencyManager.Current, null) as IList);

                (detailList as ListWrapper).Sort(1);
                detailGrid.DataSource = detailList;
                detailCurrencyManager = (CurrencyManager)detailGrid.BindingContext[detailList];

                dataGrid_BindingContextChanged(detailGrid, e);
            }
            //dataGrid.Select(dataGrid.CurrentRowIndex);
        }

        private void toolBar_ButtonClick(object sender, ToolBarButtonClickEventArgs e)
        {
            switch ((sender as ToolBar).Buttons.IndexOf(e.Button))
            {
                // Новая запись
                case 0:
                    {
                        menuItemNewObject_Click(e.Button.DropDownMenu.MenuItems[0], e);
                        break;
                    }
                // Редактирование записи
                case 1:
                    {
                        ObjectEditor oe = new ObjectEditor(this.objectStorage);
                        oe.SelectedObject = masterCurrencyManager.Current;
                        if (oe.ShowDialog(this) == DialogResult.OK)
                        {
                            int CurrentPos = dataGrid.CurrentRowIndex;
                            if (masterCurrencyManager.Current is IBizObject)
                                if ((masterCurrencyManager.Current as IBizObject).Store != null)
                                    (masterCurrencyManager.Current as IBizObject).Store.Save(masterCurrencyManager.Current);

                            if (OnChangeItem != null)
                                OnChangeItem(masterCurrencyManager.Current);

                            (dataGrid.DataSource as ListWrapper).OnListChanged(new ListChangedEventArgs(ListChangedType.ItemChanged, CurrentPos));
                        }
                        break;
                    }
                // Удаление записи
                case 2:
                    {
                        if (MessageBox.Show("Подтвердите удаление записи", "Удаление", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            // Получение списка выделенных строк
                            ArrayList selectedRows = new ArrayList();
                            for (int index = 0; index < masterList.Count; index++)
                                if (dataGrid.IsSelected(index))
                                    selectedRows.Add(index);

                            for (int index = selectedRows.Count - 1; index >= 0; index--)
                            {
                                // Проверяем объект на наличие перекрестных ссылок
                                // Для сохранения связей
                                foreach (PropertyInfo pi in DetailPropList)
                                {
                                    if (pi.PropertyType.Equals(typeof(IList)))
                                    {
                                        object[] attributes = pi.GetCustomAttributes(typeof(PropertyBrowserDetailAttribute), false);
                                        if (attributes.Length == 1)
                                        {
                                            if ((attributes[0] as PropertyBrowserDetailAttribute).NMRelation == true)
                                            {
                                                // получить список который ссылается на объекты типа МастерТайп 
                                                if (masterList[(int)selectedRows[index]] != null)
                                                {
                                                    IList refList = pi.GetValue(masterList[(int)selectedRows[index]], null) as IList;

                                                    if (refList != null)
                                                    {
                                                        foreach (object obj in refList)
                                                        {
                                                            PropertyInfo[] opiarray = obj.GetType().GetProperties();
                                                            foreach (PropertyInfo opi in opiarray)
                                                            {
                                                                if (opi.PropertyType.Equals(typeof(IList)))
                                                                {
                                                                    object[] opattributes = opi.GetCustomAttributes(typeof(PropertyBrowserDetailAttribute), false);
                                                                    if (opattributes.Length == 1)
                                                                    {
                                                                        if ((opattributes[0] as PropertyBrowserDetailAttribute).DetailType == MasterType && (opattributes[0] as PropertyBrowserDetailAttribute).NMRelation == true)
                                                                        {
                                                                            IList oprefList = opi.GetValue(obj, null) as IList;
                                                                            if (oprefList.Contains(masterList[(int)selectedRows[index]]))
                                                                                oprefList.Remove(masterList[(int)selectedRows[index]]);
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }


                                                        if (refList.Contains(masterCurrencyManager.Current))
                                                            refList.Remove(masterCurrencyManager.Current);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }


                                if (OnDelItem != null)
                                    OnDelItem(masterList[(int)selectedRows[index]]);
                                // Удаляем из хранилища объектов
                                if ( /*masterCurrencyManager.List[index]*/ masterList[(int)selectedRows[index]] is IBizObject)
                                    if (( /*masterCurrencyManager.List[index]*/ masterList[(int)selectedRows[index]] as IBizObject).Store != null)
                                    {
                                        objectStorage.Remove(masterList[(int)selectedRows[index]] as IBizObject /*masterCurrencyManager.List[index]*/);
                                    }
                                // Удаляем из списка объектов
                                masterList.RemoveAt((int)selectedRows[index] /*masterCurrencyManager.List[(int)selectedRows[index]]*/);
                            }

                            // Вызов обработчика события изменения списка
                            (dataGrid.DataSource as ListWrapper).OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
                        }
                        break;
                    }
                //Обновление
                case 3:
                    {
                        (dataGrid.DataSource as ListWrapper).Sort();
                        (dataGrid.DataSource as ListWrapper).OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
                        break;
                    }
                //Фильтр данных
                case 6:
                    {
                        FilterForm filterForm = new FilterForm(MasterType, this.objectStorage, (this.dataGrid.DataSource as ListWrapper).objectFilter);

                        if (filterForm.ShowDialog(this) == DialogResult.OK)
                        {
                            (this.dataGrid.DataSource as ListWrapper).isFiltered = true;
                        }

                        break;
                    }
                //Отмена фильтра
                case 7:
                    {
                        (this.dataGrid.DataSource as ListWrapper).isFiltered = false;
                        break;
                    }

                //Сохранение
                case 9:
                    {
                        DataExporter.DataExporter.Save(FileType.XMLFile, MasterType, masterList);
                        break;
                    }
            }
        }

        private void dataGrid_BindingContextChanged(object sender, EventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            ///Делает колонки DataGrid'а необходимой ширины
            Type dgt = sender.GetType();
            MethodInfo mi = dgt.GetMethod("ColAutoResize", BindingFlags.NonPublic | BindingFlags.Instance);
            for (int i = dg.FirstVisibleColumn; i < dg.VisibleColumnCount; i++)
            {
                object[] methodArgs = { i };
                mi.Invoke(sender, methodArgs);
            }

            if (dg.Tag != null)
                dg.CaptionText = dataGrid.Tag.ToString() + " [" + (dg.DataSource as IList).Count.ToString() + " записів]";
        }


        private void DetailBar_ButtonClick(object sender, ToolBarButtonClickEventArgs e)
        {
            if (SubObjectsBox.SelectedIndex == -1) return;
            switch ((sender as ToolBar).Buttons.IndexOf(e.Button))
            {
                //Добавление подчинённой записи 
                case 0:
                    {
                        if (DetailSelected && !CreateNew)
                        {
                            IList availableValues = (masterCurrencyManager.Current as IBizObject).OnPropertyValues(this.DetailProp);
                            CollectionEditorForm objsel = new CollectionEditorForm(objectStorage, CollectionEditorType.AsCollectionBrowser, DetailType, objectStorage.ObjectList(DetailType), availableValues);

                            if (objsel.ShowDialog() == DialogResult.OK)
                            {
                                foreach (object obj in objsel.SelectedItems)
                                {
                                    detailList.Add(obj);
                                    // Для сохранения связи
                                    PropertyInfo[] piarray = obj.GetType().GetProperties();
                                    foreach (PropertyInfo pi in piarray)
                                    {
                                        if (pi.PropertyType.Equals(typeof(IList)))
                                        {
                                            object[] attributes = pi.GetCustomAttributes(typeof(PropertyBrowserDetailAttribute), false);
                                            if (attributes.Length == 1)
                                            {
                                                if ((attributes[0] as PropertyBrowserDetailAttribute).DetailType == MasterType && (attributes[0] as PropertyBrowserDetailAttribute).NMRelation == true)
                                                {
                                                    // получить список который ссылается на объекты типа МастерТайп 
                                                    IList refList = pi.GetValue(obj, null) as IList;

                                                    if (!refList.Contains(masterCurrencyManager.Current))
                                                        refList.Add(masterCurrencyManager.Current);
                                                    break;
                                                }
                                            }
                                        }
                                    }
                                    if (this.OnChangeItem != null)
                                        this.OnChangeItem(masterCurrencyManager.Current);
                                }
                            }
                            // Сохранение изменений
                            if (masterCurrencyManager.Current is IBizObject)
                                if ((masterCurrencyManager.Current as IBizObject).Store != null)
                                    (masterCurrencyManager.Current as IBizObject).Store.Save(masterCurrencyManager.Current);
                        }
                        else
                        {
                            ObjectEditor oe = new ObjectEditor(this.objectStorage);
                            object obj = objectStorage.New(DetailType, masterCurrencyManager.Current);

                            oe.SelectedObject = obj;
                            if (oe.ShowDialog(this) == DialogResult.OK)
                            {
                                // Для сохранения связи
                                PropertyInfo[] piarray = obj.GetType().GetProperties();
                                foreach (PropertyInfo pi in piarray)
                                {
                                    if (pi.PropertyType.Equals(typeof(IList)))
                                    {
                                        object[] attributes = pi.GetCustomAttributes(typeof(PropertyBrowserDetailAttribute), false);
                                        if (attributes.Length == 1)
                                        {
                                            if ((attributes[0] as PropertyBrowserDetailAttribute).DetailType == MasterType && (attributes[0] as PropertyBrowserDetailAttribute).NMRelation == true)
                                            {
                                                // получить список который ссылается на объекты типа МастерТайп 
                                                IList refList = pi.GetValue(obj, null) as IList;

                                                if (!refList.Contains(masterCurrencyManager.Current))
                                                    refList.Add(masterCurrencyManager.Current);
                                                break;
                                            }
                                        }
                                    }
                                }
                                // Обновление detailGrid`а
                                (detailGrid.DataSource as ListWrapper).OnListChanged(new ListChangedEventArgs(ListChangedType.ItemAdded, detailList.Add(obj)));
                                // Сохраняем подчинённый объект в хранилище
                                if (obj is IBizObject)
                                    if ((obj as IBizObject).Store != null)
                                        (obj as IBizObject).Store.Save(obj);
                            }
                        }
                        // Выход case 0
                        break;
                    }
                //Редактирование подчинённой записи
                case 1:
                    {
                        ObjectEditor oe = new ObjectEditor(this.objectStorage);
                        oe.SelectedObject = detailList[detailCurrencyManager.Position];
                        if (oe.ShowDialog(this) == DialogResult.OK)
                        {
                            (dataGrid.DataSource as ListWrapper).OnListChanged(new ListChangedEventArgs(ListChangedType.ItemChanged, detailGrid.CurrentRowIndex));
                            //Сохранение изменений
                            if (detailCurrencyManager.Current is IBizObject)
                                if ((detailCurrencyManager.Current as IBizObject).Store != null)
                                    (detailCurrencyManager.Current as IBizObject).Store.Save(detailCurrencyManager.Current);
                        }
                        // Выход case 1
                        break;
                    }
                // Удаление записи
                case 2:
                    {
                        if (MessageBox.Show("Подтвердите удаление записи", "Удаление", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            // Получение списка выделенных строк
                            ArrayList selectedRows = new ArrayList();
                            for (int index = 0; index < detailList.Count; index++)
                                if (detailGrid.IsSelected(index))
                                    selectedRows.Add(index);

                            if (DetailSelected && !DeleteManual)
                            {
                                // удаление только из списка	
                                for (int index = selectedRows.Count - 1; index >= 0; index--)
                                {
                                    object obj = detailList[(int)selectedRows[index]];
                                    // Для сохранения связи
                                    if (obj != null)
                                    {
                                        PropertyInfo[] piarray = obj.GetType().GetProperties();
                                        foreach (PropertyInfo pi in piarray)
                                        {
                                            if (pi.PropertyType.Equals(typeof(IList)))
                                            {
                                                object[] attributes = pi.GetCustomAttributes(typeof(PropertyBrowserDetailAttribute), false);
                                                if (attributes.Length == 1)
                                                {
                                                    if ((attributes[0] as PropertyBrowserDetailAttribute).DetailType == MasterType && (attributes[0] as PropertyBrowserDetailAttribute).NMRelation == true)
                                                    {
                                                        // получить список который ссылается на объекты типа МастерТайп 
                                                        IList refList = pi.GetValue(obj, null) as IList;

                                                        if (refList.Contains(masterCurrencyManager.Current))
                                                            refList.Remove(masterCurrencyManager.Current);
                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    // Удаление из списка
                                    detailList.RemoveAt((int)selectedRows[index]);
                                }

                                // Сохрание изменений
                                if (masterCurrencyManager.Current is IBizObject)
                                    if ((masterCurrencyManager.Current as IBizObject).Store != null)
                                        (masterCurrencyManager.Current as IBizObject).Store.Save(masterCurrencyManager.Current);
                            }
                            else
                            {
                                bool _can_Delete = true;
                                /// удаление из хранилища
                                for (int index = selectedRows.Count - 1; index >= 0; index--)
                                {
                                    object obj = detailList[(int)selectedRows[index]];
                                    // Для сохранения связи
                                    if (obj != null)
                                    {
                                        PropertyInfo[] piarray = obj.GetType().GetProperties();
                                        foreach (PropertyInfo pi in piarray)
                                        {
                                            if (pi.PropertyType.Equals(typeof(IList)))
                                            {
                                                object[] attributes = pi.GetCustomAttributes(typeof(PropertyBrowserDetailAttribute), false);
                                                if (attributes.Length == 1)
                                                {
                                                    if ((attributes[0] as PropertyBrowserDetailAttribute).DetailType == MasterType && (attributes[0] as PropertyBrowserDetailAttribute).NMRelation == true)
                                                    {
                                                        // получить список который ссылается на объекты типа МастерТайп 
                                                        IList refList = pi.GetValue(obj, null) as IList;

                                                        if (refList.Contains(masterCurrencyManager.Current))
                                                            refList.Remove(masterCurrencyManager.Current);

                                                        if (refList.Count != 0)
                                                            _can_Delete = false;

                                                        break;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    // Удаление из хранилища
                                    if (_can_Delete)
                                        if (detailCurrencyManager.List[(int)selectedRows[index]] is IBizObject)
                                            if ((detailCurrencyManager.List[(int)selectedRows[index]] as IBizObject).Store != null)
                                                (detailCurrencyManager.List[(int)selectedRows[index]] as IBizObject).Store.Remove(detailList[(int)selectedRows[index]]);

                                    // удаление со списка
                                    detailList.RemoveAt((int)selectedRows[index]);

                                    if (DeleteManual)
                                        if (masterCurrencyManager.Current is IBizObject)
                                            if ((masterCurrencyManager.Current as IBizObject).Store != null)
                                                (masterCurrencyManager.Current as IBizObject).Store.Save(masterCurrencyManager.Current);

                                }

                            }
                            // Обновление DataGrid
                            (detailGrid.DataSource as ListWrapper).OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));

                        }
                        // Выход case 2
                        break;
                    }
                // Обновление сетки данных
                case 3:
                    {
                        (detailGrid.DataSource as ListWrapper).Sort();
                        // Обновление DataGrid
                        (detailGrid.DataSource as ListWrapper).OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
                        // Выход case 3
                        break;
                    }
                // Фильтрация
                case 6:
                    {
                        FilterForm ff = new FilterForm(DetailType, this.objectStorage, (this.detailGrid.DataSource as ListWrapper).objectFilter);

                        if (ff.ShowDialog(this) == DialogResult.OK)
                        {
                            (this.detailGrid.DataSource as ListWrapper).isFiltered = true;
                        }
                        // Выход case 6
                        break;
                    }
                //Отмена фильтра (при его наличии)
                case 7:
                    {
                        (this.detailGrid.DataSource as ListWrapper).isFiltered = false;
                        break;
                    }
            }
        }

        private void menuItemNewObject_Click(object sender, EventArgs e)
        {
            //InputMode

            ObjectEditor oe = new ObjectEditor(this.objectStorage);
            object newobject = objectStorage.New(MasterType);
            oe.SelectedObject = newobject;

            do
            {
                if (oe.ShowDialog(this) == DialogResult.OK)
                {
                    if (newobject is IBizObject)
                        if ((newobject as IBizObject).Store != null)
                            (newobject as IBizObject).Store.Save(newobject);

                    if (masterList.Contains(newobject))
                        if (MessageBox.Show("Такая запись уже существует, добавить?", "Подтверждите", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        {
                            int newrowindex = masterList.Add(newobject);
                            this.OnAddItem(newobject);
                            (dataGrid.DataSource as ListWrapper).OnListChanged(new ListChangedEventArgs(ListChangedType.ItemAdded, newrowindex));
                            dataGrid.CurrentRowIndex = newrowindex;
                        }
                        else
                        {
                        }
                    else
                    {
                        int newrowindex = masterList.Add(newobject);
                        if (this.OnAddItem != null)
                            this.OnAddItem(newobject);
                        (dataGrid.DataSource as ListWrapper).OnListChanged(new ListChangedEventArgs(ListChangedType.ItemAdded, newrowindex));
                        dataGrid.CurrentRowIndex = newrowindex;
                    }
                }
                dataGrid_BindingContextChanged(dataGrid, e);

                if (oe.InputMode)
                {
                    newobject = objectStorage.New(MasterType);
                    oe.SelectedObject = newobject;
                }
            } while (oe.InputMode);
        }

        private void menuItemNewLikeObject_Click(object sender, EventArgs e)
        {
            if (masterCurrencyManager.Current is ICloneable)
            {
                ObjectEditor oe = new ObjectEditor(this.objectStorage);
                object newobject = /*objectStorage.New(MasterType)*/ (masterCurrencyManager.Current as ICloneable).Clone();
                do
                {
                    oe.SelectedObject = newobject;
                    if (oe.ShowDialog(this) == DialogResult.OK)
                    {
                        if (newobject is IBizObject)
                            if ((newobject as IBizObject).Store != null)
                                (newobject as IBizObject).Store.Save(newobject);

                        if (masterList.Contains(newobject))
                            if (MessageBox.Show("Такая запись уже существует, добавить?", "Подтверждите", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                            {
                                int index = masterList.Add(newobject);
                                this.OnAddItem(newobject);
                                (dataGrid.DataSource as ListWrapper).OnListChanged(new ListChangedEventArgs(ListChangedType.ItemAdded, index));
                                dataGrid.CurrentRowIndex = index;
                            }
                            else
                            {
                            }
                        else
                        {
                            int index = masterList.Add(newobject);
                            if (this.OnAddItem != null)
                                this.OnAddItem(newobject);
                            (dataGrid.DataSource as ListWrapper).OnListChanged(new ListChangedEventArgs(ListChangedType.ItemAdded, index));
                            dataGrid.CurrentRowIndex = index;
                        }
                    }
                    dataGrid_BindingContextChanged(dataGrid, e);
                    if (oe.InputMode)
                    {
                        newobject = (masterCurrencyManager.Current as ICloneable).Clone();
                        oe.SelectedObject = newobject;
                    }
                } while (oe.InputMode);
            }
        }

        private void Browser_Load(object sender, System.EventArgs e)
        {
            this.Text = Vocabruary.Translate("Navigator.Browser.Caption");
            //this.dataGrid.


            #region Build Form and DataGrid captions

            object[] eca = MasterType.GetCustomAttributes(typeof(EntityCaptionAttribute), true);
            if (eca.Length == 1)
            {
                string localizeStr = Vocabruary.Translate((eca[0] as EntityCaptionAttribute).EntityCaption);
                if (localizeStr != null)
                {
                    this.Text += " - " + localizeStr;
                    dataGrid.Tag = dataGrid.CaptionText = localizeStr;
                }
                else
                {
                    this.Text += " - " + (eca[0] as EntityCaptionAttribute).EntityCaption;
                    dataGrid.Tag = dataGrid.CaptionText = (eca[0] as EntityCaptionAttribute).EntityCaption;
                }
                dataGrid.CaptionText += " [" + masterList.Count.ToString() + " записів]";
            }

            #endregion

            this.menuItemNewObject.Text = Vocabruary.Translate("Navigator.Browser.NewButton.Caption");
            this.menuItemNewLikeObject.Text = Vocabruary.Translate("Navigator.Browser.LikeButton.Caption");

            this.NewBtn.ToolTipText = this.toolBarButton1.ToolTipText = Vocabruary.Translate("Navigator.Browser.NewButton.ToolTip");
            this.EditBtn.ToolTipText = this.toolBarButton2.ToolTipText = Vocabruary.Translate("Navigator.Browser.EditButton.ToolTip");
            this.DeleteBtn.ToolTipText = this.toolBarButton3.ToolTipText = Vocabruary.Translate("Navigator.Browser.DeleteButton.ToolTip");
            this.RefreshBtn.ToolTipText = this.toolBarButton5.ToolTipText = Vocabruary.Translate("Navigator.Browser.RefreshButton.ToolTip");
            this.FilterBtn.ToolTipText = this.toolBarButton7.ToolTipText = Vocabruary.Translate("Navigator.Browser.FilterButton.ToolTip");
            this.PrintBtn.ToolTipText = this.toolBarButton10.ToolTipText = Vocabruary.Translate("Navigator.Browser.PrintButton.ToolTip");
            this.helpBtn.ToolTipText = this.toolBarButton12.ToolTipText = Vocabruary.Translate("Navigator.Browser.HelpButton.ToolTip");
        }

        private void detailGrid_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(this.DetailType))
                e.Effect = DragDropEffects.Copy;
            else
                e.Effect = DragDropEffects.None;
        }

        private void detailGrid_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                DataGrid grid = (sender as DataGrid);
                object dragObject = null;
                if (grid.Name == "detailGrid" && grid.CurrentRowIndex != -1)
                    dragObject = detailCurrencyManager.List[(int)grid.CurrentRowIndex];
                else
                    if (grid.Name == "grid" && grid.CurrentRowIndex != -1)
                        dragObject = masterCurrencyManager.List[(int)grid.CurrentRowIndex];
                    else
                        return;

                if (dragObject != null)
                {
                    grid.AllowDrop = false;
                    grid.DoDragDrop(dragObject, DragDropEffects.Copy);
                    grid.AllowDrop = true;
                }
            }
        }

        private void detailGrid_DragDrop(object sender, DragEventArgs e)
        {
            object obj = e.Data.GetData(this.DetailType);

            detailList.Add(obj);

            // Для сохранения связи
            PropertyInfo[] piarray = obj.GetType().GetProperties();
            foreach (PropertyInfo pi in piarray)
            {
                if (pi.PropertyType.Equals(typeof(IList)))
                {
                    object[] attributes = pi.GetCustomAttributes(typeof(PropertyBrowserDetailAttribute), false);
                    if (attributes.Length == 1)
                    {
                        if ((attributes[0] as PropertyBrowserDetailAttribute).DetailType == MasterType && (attributes[0] as PropertyBrowserDetailAttribute).NMRelation == true)
                        {
                            // получить список который ссылается на объекты типа МастерТайп 
                            IList refList = pi.GetValue(obj, null) as IList;

                            if (!refList.Contains(masterCurrencyManager.Current))
                                refList.Add(masterCurrencyManager.Current);
                            break;
                        }
                    }
                }
            }
            if (this.OnChangeItem != null)
                this.OnChangeItem(masterCurrencyManager.Current);

            // Сохранение изменений
            if (masterCurrencyManager.Current is IBizObject)
                if ((masterCurrencyManager.Current as IBizObject).Store != null)
                    (masterCurrencyManager.Current as IBizObject).Store.Save(masterCurrencyManager.Current);
        }

    }
}