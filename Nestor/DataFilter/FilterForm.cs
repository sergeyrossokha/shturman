﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Reflection;
using System.Windows.Forms;
using Shturman.Nestor.DataAttributes;
using Shturman.Nestor.DataLists;
using Shturman.Nestor.Interfaces;

namespace Shturman.Nestor.DataFilter
{
    /// <summary>
    /// Summary description for FilterForm.
    /// </summary>
    public class FilterForm : Form
    {
        private GroupBox groupBox1;
        private ComboBox propertyName;
        private ComboBox logicCondition;
        private Label label2;
        private Label label3;
        private Label label4;
        private ComboBox objectValueCombo;
        private TextBox objectValueText;
        private Button buttonAdd;
        private Button buttonApply;
        private Button button2;
        private Button button3;
        private Label label5;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private Container components = null;

        private IObjectStorage _os;
        private ListFiltrator _listFiltrator = new ListFiltrator();
        private ListBox listBox1;
        private Label label1;
        private ArrayList propertyList = new ArrayList();

        public ListFiltrator listFiltrator
        {
            get { return _listFiltrator; }
        }

        public FilterForm(Type filterType, IObjectStorage os, ListFiltrator lf)
            : this(filterType, os)
        {
            _listFiltrator = lf;
            this.listBox1.Items.Clear();
            foreach (object obj in lf.LogicExpessions)
            {
                this.listBox1.Items.Add(obj.ToString());
            }
        }

        public FilterForm(Type filterType, IObjectStorage os)
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            _os = os;

            propertyList.Clear();
            propertyName.Items.Clear();

            PropertyInfo[] pi = filterType.GetProperties();
            foreach (PropertyInfo property in pi)
            {
                object[] BrowserDetailAttrs = property.GetCustomAttributes(typeof(PropertyVisibleAttribute), false);
                if (BrowserDetailAttrs.Length == 1 && ((PropertyVisibleAttribute)(BrowserDetailAttrs[0])).IsVisible)
                {
                    propertyList.Add(property);

                    object[] CaptionAttrs = property.GetCustomAttributes(typeof(PropertyCaptionAttribute), false);

                    foreach (PropertyCaptionAttribute pca in CaptionAttrs)
                    {
                        string translateString = Shturman.MultyLanguage.Vocabruary.Translate(pca.Caption);
                        if (translateString != null && translateString != string.Empty)
                            propertyName.Items.Add(translateString);
                        else
                            propertyName.Items.Add(pca.Caption);
                    }
                }
            }
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.objectValueText = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.objectValueCombo = new System.Windows.Forms.ComboBox();
            this.logicCondition = new System.Windows.Forms.ComboBox();
            this.propertyName = new System.Windows.Forms.ComboBox();
            this.buttonAdd = new System.Windows.Forms.Button();
            this.buttonApply = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.objectValueText);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.objectValueCombo);
            this.groupBox1.Controls.Add(this.logicCondition);
            this.groupBox1.Controls.Add(this.propertyName);
            this.groupBox1.Controls.Add(this.buttonAdd);
            this.groupBox1.Location = new System.Drawing.Point(8, 128);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(400, 96);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Новое условие";
            // 
            // objectValueText
            // 
            this.objectValueText.Location = new System.Drawing.Point(240, 32);
            this.objectValueText.Name = "objectValueText";
            this.objectValueText.Size = new System.Drawing.Size(152, 20);
            this.objectValueText.TabIndex = 2;
            this.objectValueText.Text = "";
            this.objectValueText.Visible = false;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(240, 16);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 16);
            this.label4.TabIndex = 7;
            this.label4.Text = "Значение*";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(136, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 16);
            this.label3.TabIndex = 6;
            this.label3.Text = "Условие";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(8, 16);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 16);
            this.label2.TabIndex = 5;
            this.label2.Text = "Свойство";
            // 
            // objectValueCombo
            // 
            this.objectValueCombo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.objectValueCombo.Location = new System.Drawing.Point(240, 32);
            this.objectValueCombo.Name = "objectValueCombo";
            this.objectValueCombo.Size = new System.Drawing.Size(152, 21);
            this.objectValueCombo.TabIndex = 3;
            this.objectValueCombo.Visible = false;
            // 
            // logicCondition
            // 
            this.logicCondition.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.logicCondition.DropDownWidth = 150;
            this.logicCondition.Location = new System.Drawing.Point(136, 32);
            this.logicCondition.Name = "logicCondition";
            this.logicCondition.Size = new System.Drawing.Size(96, 21);
            this.logicCondition.TabIndex = 2;
            // 
            // propertyName
            // 
            this.propertyName.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.propertyName.DropDownWidth = 200;
            this.propertyName.Location = new System.Drawing.Point(8, 32);
            this.propertyName.Name = "propertyName";
            this.propertyName.Size = new System.Drawing.Size(120, 21);
            this.propertyName.TabIndex = 1;
            this.propertyName.SelectedIndexChanged += new System.EventHandler(this.propertyName_SelectedIndexChanged);
            // 
            // buttonAdd
            // 
            this.buttonAdd.Location = new System.Drawing.Point(320, 66);
            this.buttonAdd.Name = "buttonAdd";
            this.buttonAdd.TabIndex = 8;
            this.buttonAdd.Text = "Добавить";
            this.buttonAdd.Click += new System.EventHandler(this.buttonAdd_Click);
            // 
            // buttonApply
            // 
            this.buttonApply.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.buttonApply.Location = new System.Drawing.Point(251, 232);
            this.buttonApply.Name = "buttonApply";
            this.buttonApply.TabIndex = 10;
            this.buttonApply.Text = "Применить";
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(331, 232);
            this.button2.Name = "button2";
            this.button2.TabIndex = 11;
            this.button2.Text = "Закрыть";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(328, 16);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(77, 23);
            this.button3.TabIndex = 12;
            this.button3.Text = "Очистить";
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(9, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 16);
            this.label5.TabIndex = 13;
            this.label5.Text = "Текущий фильтр";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // listBox1
            // 
            this.listBox1.Location = new System.Drawing.Point(8, 16);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(312, 108);
            this.listBox1.TabIndex = 14;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(304, 26);
            this.label1.TabIndex = 9;
            this.label1.Text = "* для строк поддерживается использование регулярных выражений (см. помощь)";
            // 
            // FilterForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(410, 258);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.buttonApply);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "FilterForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Фильтр";
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion

        private void propertyName_SelectedIndexChanged(object sender, EventArgs e)
        {
            if ((sender as ComboBox).SelectedIndex == -1) return;
            Type tp = (propertyList[(sender as ComboBox).SelectedIndex] as PropertyInfo).PropertyType;
            if (tp.GetInterface("IBizObject", true) != null)
            {
                this.objectValueText.Visible = false;
                this.objectValueCombo.Visible = true;
                this.objectValueCombo.Items.Clear();

                FieldInfo[] mis = typeof(ConditionOperators).GetFields();
                this.logicCondition.Items.Clear();

                foreach (FieldInfo fi in mis)
                {
                    if (fi.IsStatic)
                    {
                        if (fi.Name == "coEqual")
                            this.logicCondition.Items.Add("равно");
                        if (fi.Name == "coNotEqual")
                            this.logicCondition.Items.Add("не равно");
                    }
                }

                IList list = _os.ObjectList(tp);
                object[] objects = new object[list.Count];
                list.CopyTo(objects, 0);
                this.objectValueCombo.Items.AddRange(objects);
            }
            else
            {
                this.objectValueCombo.Visible = false;
                this.objectValueText.Visible = true;
                FieldInfo[] mis = typeof(ConditionOperators).GetFields();
                this.logicCondition.Items.Clear();

                foreach (FieldInfo fi in mis)
                {
                    if (fi.IsStatic)
                    {
                        //coIn, //часть массива y in [1,2,3,5]
                        //coInclude //включает, т.е. массив y сожержит 3
                        if (fi.Name == "coEqual")
                            this.logicCondition.Items.Add("равно");
                        if (fi.Name == "coNotEqual")
                            this.logicCondition.Items.Add("не равно");
                        if (fi.Name == "coLess")
                            this.logicCondition.Items.Add("меньше");
                        if (fi.Name == "coLessOrEqual")
                            this.logicCondition.Items.Add("меньше или равно");
                        if (fi.Name == "coGreater")
                            this.logicCondition.Items.Add("больше");
                        if (fi.Name == "coGreatOrEqual")
                            this.logicCondition.Items.Add("больше или равно");
                    }
                }
            }
        }

        private void buttonAdd_Click(object sender, EventArgs e)
        {
            if (this.objectValueText.Visible)
                _listFiltrator.AddCondition(LogicOperations.loAND, (propertyList[propertyName.SelectedIndex] as PropertyInfo).Name, propertyName.Text, (ConditionOperators)logicCondition.SelectedIndex, this.objectValueText.Text);
            else
                _listFiltrator.AddCondition(LogicOperations.loAND, (propertyList[propertyName.SelectedIndex] as PropertyInfo).Name, propertyName.Text, (ConditionOperators)logicCondition.SelectedIndex, this.objectValueCombo.SelectedItem);

            if (this.objectValueText.Visible)
                this.listBox1.Items.Add(propertyName.Text + " " + logicCondition.Text + " " + this.objectValueText.Text);
            else
                this.listBox1.Items.Add(propertyName.Text + " " + logicCondition.Text + " " + this.objectValueCombo.Text);

            this.propertyName.SelectedIndex = -1;
            this.logicCondition.SelectedIndex = -1;

            this.objectValueText.Visible = false;
            this.objectValueCombo.Visible = false;
        }

        private void label5_Click(object sender, EventArgs e)
        {
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this._listFiltrator.Clear();
            this.listBox1.Items.Clear();
        }
    }
}