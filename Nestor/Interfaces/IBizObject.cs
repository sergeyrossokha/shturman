using System.Collections;
using System.Reflection;

namespace Shturman.Nestor.Interfaces
{
	public interface IBizObject
	{
		event EditEventHandler Edit;
		event ChangeEventHandler Change;
		event DeleteEventHandler Delete;

		event ChangePropertyEventHandler ChangeProperty;
		event PropertyValuesEventHandler PropertyValues;

		event CreateEventHandler Create;
		event DestroyEventHandler Destroy;

		bool OnEdit();

		void OnChange();

		void OnDelete();

		IList OnPropertyValues(PropertyInfo pi);
        
		void OnChangeProperty(PropertyInfo pi, object oldvalue, object newValue);

		void OnCreate();

		void OnDestroy();

		IObjectStorage Store
		{
			get;
		}
	}
}