﻿using System;
using System.Collections;

namespace Shturman.Nestor.Interfaces
{
    /// <summary>
    /// Summary description for IObjectFactory.
    /// </summary>
    public interface IObjectStorage
    {
        void Open();
        void Close();

        object GetObjectByID(long objectID);
        object GetObjectByUUID(byte[] signature, long uuid);

        long GetID(object customObject);
        long GetUUID(object customObject, out byte[] signature, out long uuid);

        /// <summary>
        /// Создание нового сохраняемого объекта
        /// </summary>
        /// <param name="objectType">тип создаваемого объекта</param>
        /// <returns>новый, созданный объект</returns>
        object New(Type objectType);

        /// <summary>
        /// Создание нового подчинённого объекта
        /// </summary>
        /// <param name="objectType">тип создаваемого объекта</param>
        /// <param name="parent">родитель создаваемого объекта</param>
        /// <returns>новый, созданный объект</returns>
        object New(Type objectType, object parent);

        /// <summary>
        /// Сохранение объекта в хранилище данных
        /// </summary>
        /// <param name="storedObject">Объект который необходимо сохранить</param>
        void Save(object storedObject);

        /// <summary>
        /// Удаление объекта из хранилища данных
        /// </summary>
        /// <param name="storedObject">объект который необходимо удалить из хранилища данных</param>
        void Remove(object storedObject);

        /// <summary>
        /// Получение массива объектов, заданного типа
        /// </summary>
        /// <param name="listType">задаёт тип получаемых объектов</param>
        /// <returns>список объектов</returns>
        IList ObjectList(Type listType);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        IList LikeObjectList(object obj);

        /// <summary>
        /// Получение массива объектов, заданного типа, с заданным родителем
        /// </summary>
        /// <param name="listType">задаёт тип получаемых объектов</param>
        /// <param name="parent">родительский объект</param>
        /// <returns>список объектов</returns>
        IList ObjectList(Type listType, object parent);

        /// <summary>
        /// Запрос 
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        IList QueryObjects(object predicate);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList EntityList();
    }
}