﻿using System;

namespace Shturman.Nestor.DataAttributes
{
    /// <summary>
    /// Аттрибут PropertyReadOnlyAttribute необходим для оперделения свойства,
    /// как свойства только для чтения, используется редактором свойств(PropertyGrid)
    /// </summary>
    [Serializable]
    public class PropertyReadOnlyAttribute : Attribute
    {
        private bool isReadOnly;

        public PropertyReadOnlyAttribute(bool isReadOnly)
        {
            this.isReadOnly = isReadOnly;
        }

        public bool IsReadOnly
        {
            get { return isReadOnly; }
        }
    }
}