﻿using System;
using System.Collections;
using System.ComponentModel;

namespace Shturman.Nestor.DataAttributes
{
    /// <summary>
    /// Аттрибут PropertyIndexAttribute необходим для задания свойству
    /// порядкового индекса, который используется как сеткой(DataGrid) 
    /// так и редактором свойств(PropertyGrid) 
    /// </summary>
    [Serializable]
    [AttributeUsage(AttributeTargets.Property)]
    public class PropertyIndexAttribute : Attribute
    {
        /// <summary>
        /// Индекс свойства объекта 
        /// </summary>
        private int index;

        public PropertyIndexAttribute(int index)
        {
            this.index = index;
        }

        public int Index
        {
            get { return index; }
        }
    }

    public class PropertyGridSortComparer :
    IComparer
    {
        public PropertyGridSortComparer() { }

        public int Compare(object x, object y)
        {
            AttributeCollection attX = (x as PropertyDescriptor).Attributes;
            AttributeCollection attY = (y as PropertyDescriptor).Attributes;

            int indexX = 0, indexY = 0;

            foreach (Attribute at in attX)
            {
                if (at is PropertyIndexAttribute)
                {
                    indexX = (at as PropertyIndexAttribute).Index;
                    break;
                }
            }

            foreach (Attribute at in attY)
            {
                if (at is PropertyIndexAttribute)
                {
                    indexY = (at as PropertyIndexAttribute).Index;
                    break;
                }
            }
            return indexX - indexY;
        }
    }
}