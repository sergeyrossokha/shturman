﻿using System;

namespace Shturman.Nestor.DataAttributes
{
    /// <summary>
    /// Аттрибут PropertyCaptionAttribute необходим для задания свойству
    /// заголовка, который отображается в колонке сетки(DataGrid) и в 
    /// редакторе свойств(PropertyGrid) 
    /// </summary>
    [Serializable]
    public class PropertyCaptionAttribute : Attribute
    {
        private string caption;

        public PropertyCaptionAttribute(string caption)
        {
            this.caption = caption;
        }

        public string Caption
        {
            get { return caption; }
        }
    }
}