﻿using System;

namespace Shturman.Nestor.DataAttributes
{
    /// <summary>
    /// Аттрибут PropertyCategoryAttribute необходим для задания свойству
    /// категории, который отображается в редакторе свойств(PropertyGrid) 
    /// </summary>
    [Serializable]
    public class PropertyCategoryAttribute : Attribute
    {
        private string category;

        public PropertyCategoryAttribute(string category)
        {
            this.category = category;
        }

        public string Category
        {
            get { return category; }
        }
    }
}