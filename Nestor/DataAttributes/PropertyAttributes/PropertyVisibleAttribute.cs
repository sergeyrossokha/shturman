﻿using System;

namespace Shturman.Nestor.DataAttributes
{
    /// <summary>
    /// Атрибут описивает видимое это свойство или нет,
    /// если оно отсутствует, то данное свойство не будет видимо как
    /// в сетке(DataGrid), так и в редакторе свойств(PropertyGrid)
    /// </summary>
    [Serializable]
    public class PropertyVisibleAttribute : Attribute
    {
        private bool visible;

        public PropertyVisibleAttribute(bool visible)
        {
            this.visible = visible;
        }

        public bool IsVisible
        {
            get { return visible; }
        }

    }
}