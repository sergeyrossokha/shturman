﻿using System;
using System.Collections;

namespace Shturman.Nestor.DataAttributes
{
    /// <summary>
    /// Аттрибут PropertyReferenceAttribute необходим для задания что свойство
    /// есть внешний ключ(указатель) на другой объект, используется редактором 
    /// свойств(PropertyGrid)
    /// </summary>
    [Serializable]
    [AttributeUsage(AttributeTargets.Property)]
    public class PropertyReferenceAttribute : Attribute
    {
        private Type referenceType;
        private IList referenceList = null;

        public PropertyReferenceAttribute(Type referenceType)
        {
            this.referenceType = referenceType;
        }

        public PropertyReferenceAttribute(Type referenceType, IList referenceList)
            : this(referenceType)
        {
            this.referenceList = referenceList;
        }

        public Type ReferenceType
        {
            get { return referenceType; }
        }

        public IList ReferenceList
        {
            get { return referenceList; }
        }
    }
}