﻿using System;

namespace Shturman.Nestor.DataAttributes
{
    /// <summary>
    /// Данный атрибут применим к описанию только классов, 
    /// являющихся бизнес объектами и хранит их заголовок 
    /// при отображении типа в дереве доступных объектов.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
    [Serializable]
    public class EntityCaptionAttribute : Attribute
    {
        /// <summary>
        /// Заголовок класса в дереве доступных объектов
        /// </summary>
        private string entityCaption = "";

        public EntityCaptionAttribute(string entityCaption)
        {
            this.entityCaption = entityCaption;
        }

        public string EntityCaption
        {
            get { return entityCaption; }
        }
    }
}