﻿using System;

namespace Shturman.Nestor.DataAttributes
{
    /// <summary>
    /// Данный атрибут применим к описанию только классов, 
    /// являющихся бизнес объектами и хранит категорию к которой
    /// объект относится, используется при при отображении типа в 
    /// дереве доступных объектов.
    /// </summary>
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
    [Serializable]
    public class EntityCategoryAttribute : Attribute
    {
        /// <summary>
        /// Заголовок категории к которй относится класс
        /// </summary>
        private string entityCategory = "";

        public EntityCategoryAttribute(string entityCategory)
        {
            this.entityCategory = entityCategory;
        }

        public string Category
        {
            get { return entityCategory; }
            set { entityCategory = value; }
        }
    }
}