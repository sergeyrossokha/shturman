﻿using System;
using System.Collections;
using System.Text.RegularExpressions;

namespace Shturman.Sherlock.TrueLogic
{
    /// <summary>
    /// Summary description for RuleCalculator.
    /// </summary>
    public class Deductor
    {
        //Example: DC[d1,d2,d3]0.2->D1[=,'1';=,'2']:D2[!=,'5']

        #region regular expressions strings

        // Divide rule to goal part and expression part
        private static string ValueExpressionRegEx = @"\x29|\x28|\->";
        // Divide goal part of rule to fields
        private static string ValueRegEx = @"\[|\]|,";
        // Divide logical expression to domens
        private static string ExpressionRegEx = @":";
        // Divide domen to items
        private static string DomensRegEx = @"\[|\]|;";
        // Divide domen items to fields	
        private static string DomenRegEx = @"\x29|\x28|,";

        #endregion

        /// <summary>
        /// Список уровней базы знаний
        /// </summary>
        public static IList RuleSetList = new ArrayList();

        /// <summary>
        /// Список уровней БЗ которые уже использовались при выводе
        /// </summary>
        public static IList UsedRuleSetList = new ArrayList();

        /// <summary>
        /// Функция дедуктивного вывода правила заданного откомпилированным правилом
        /// </summary>
        /// <param name="compiledRule">откомпилированное правило</param>
        /// <param name="facts">список фактов</param>
        /// <param name="goalfacts">список достигнутых целей</param>
        public static void Evaluate(Rule rule, IList facts, IList goalfacts)
        {
            if (!UsedRuleSetList.Contains(rule.Name))
                try
                {
                    float rulepd = 1.0f;
                    foreach (RuleItem ri in rule.RuleItems)
                    {
                        //Calculate Pd for ruleitems(domen)
                        float ruleitempd = 0.0f;
                        foreach (ExpressionItem ei in ri.ItemExpressions)
                            foreach (Fact fact in facts)
                                if (fact.Name == ri.ItemName && fact.HasValue == true)
                                    if (fact.Value.ToString() == ei.ItemValue.ToString())
                                        ruleitempd = Math.Max(ruleitempd, /*ei.ItemPd*/fact.FactPd);

                        // Try to find answer based on existing facts in other layer
                        if (ruleitempd == 0.0f)
                        {
                            foreach (RuleSet rs in RuleSetList)
                                if (rs.GoalPropertyName == ri.ItemName)
                                {
                                    // во избежание зацикливания
                                    UsedRuleSetList.Add(rs.GoalPropertyName);

                                    IList newFacts = rs.Evaluate(facts);
                                    if (newFacts.Count != 0)
                                        foreach (ExpressionItem ei in ri.ItemExpressions)
                                            foreach (Fact fact in newFacts)
                                            {
                                                if (fact.Name == ri.ItemName && fact.HasValue == true)
                                                    if (fact.Value.ToString() == ei.ItemValue.ToString())
                                                        ruleitempd = Math.Max(ruleitempd, /*ei.ItemPd*/fact.FactPd);
                                                // Add all new fact
                                                facts.Add(fact);
                                            }
                                }
                            // Если хотя бы один элемент правила не сработал выход 
                            if (ruleitempd == 0.0f) return;
                        }
                        rulepd = Math.Min(rulepd, ruleitempd);
                    }

                    rulepd *= rule.FactPd;

                    if (rulepd > 0)
                        foreach (object obj in rule.RuleGoal.ItemExpressions)
                        {
                            Fact tempfact = new Fact(rule.Name, (obj as string).Replace("\'", ""), rulepd);

                            if (!goalfacts.Contains(tempfact))
                                goalfacts.Add(tempfact);
                        }
                }
                finally
                {
                    UsedRuleSetList.Remove(rule.Name);
                }
        }


        /// <summary>
        /// Компиляция правила заданного строкой
        /// </summary>
        /// <param name="strFormula">правило заданное в виде строки символов</param>
        /// <param name="compiledRule">откомпилированное правило</param>
        public static void Compile(string strFormula, Rule rule)
        {
            Regex regex = new Regex(ValueExpressionRegEx);
            string[] rawRuleTokins = regex.Split(strFormula);
            if (rawRuleTokins.Length != 2)
            {
                // Error
            }

            # region Parse Goal Part Of Rule, Get Goal Domen Name and Rule PD

            // Разбор следствий для правила
            regex = new Regex(ValueRegEx);
            string[] rawThenTokins = regex.Split(rawRuleTokins[0] as string);

            //compiledRule.RuleGoals = new ArrayList();
            rule.Name = rawThenTokins[0].Trim();

            if (rawThenTokins.Length >= 2)
                for (int index = 1; index < rawThenTokins.Length - 1; index++)
                {
                    if (rawThenTokins[index].Trim() != "")
                        rawThenTokins[index].Trim();
                    rule.RuleGoal = new RuleItem(rawThenTokins[index]);
                }
            rule.FactPd = float.Parse(rawThenTokins[rawThenTokins.Length - 1].Replace(".", ","));

            #endregion

            #region Parse Expression Part Of Rule, Calculate PD Of "Then"

            // Разбор логического выражения для его дальнейшего расчета
            regex = new Regex(ExpressionRegEx);
            string[] rawExpressionTokins = regex.Split(rawRuleTokins[1] as string);

            //compiledRule.RuleItems = new ArrayList();
            foreach (string expressionStr in rawExpressionTokins)
            {
                if (expressionStr.Trim() != "") expressionStr.Trim();

                regex = new Regex(DomensRegEx);
                string[] rawDomensTokins = regex.Split(expressionStr);
                if (rawDomensTokins.Length >= 2)
                {
                    RuleItem ruleitem = new RuleItem(rawDomensTokins[0].Trim());

                    for (int index = 1; index < rawDomensTokins.Length - 1; index++)
                    {
                        regex = new Regex(DomenRegEx);
                        string[] rawDomenTokins = regex.Split(rawDomensTokins[index].Trim());

                        ExpressionItem expressionitem = new ExpressionItem();

                        if (rawDomenTokins.Length == 2 /*3*/)
                        {
                            expressionitem.ItemValue = rawDomenTokins[1].Replace("\'", "");
                            if (rawDomenTokins[0] == "=")
                                expressionitem.boolOperator = ExpressionItem.ExpressionOperator.iboEqual;
                            else if (rawDomenTokins[0] == "!=")
                                expressionitem.boolOperator = ExpressionItem.ExpressionOperator.iboNotEqual;
                            else
                            {
                                // error
                            }
                            //expressionitem.ItemPd = float.Parse(rawDomenTokins[2].Replace(".",","));	
                        }
                        else
                        {
                            // Error
                        }
                        // Добавляем
                        ruleitem.ItemExpressions.Add(expressionitem);
                    }
                    // Добавляем 
                    rule.RuleItems.Add(ruleitem);
                }
                else
                {
                    // Error
                }
            }

            #endregion
        }

        /// <summary>
        /// Чтение базы знаний из файла
        /// </summary>
        public static void ReadRuleSets(string filename)
        {
        }

        /// <summary>
        /// Запись базы данных в файл
        /// </summary>
        public static void WriteRuleSets(string filename)
        {
        }
    }
}