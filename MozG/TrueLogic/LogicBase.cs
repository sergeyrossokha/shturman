﻿using System;
using System.Collections;
using System.IO;
using System.Runtime.Serialization.Formatters.Soap;
using System.Xml.Serialization;

namespace Shturman.Sherlock.TrueLogic
{
    /// <summary>
    /// Summary description for LogicBase.
    /// </summary>
    [Serializable]
    [XmlRoot(Namespace = "Sherlock.com.ua", ElementName = "KnowledgeBase")]
    public class LogicBase
    {
        [XmlElement("Filename")]
        private string selfFileName = "";

        [XmlIgnore()]
        private bool hasChanges = false;

        public bool HasChanges
        {
            get { return hasChanges; }
            set { hasChanges = value; }
        }

        private LogicBase()
        {
            //this.hasChanges = false;
        }

        /// <summary>
        /// Конструктор класса "Базы Знаний"
        /// </summary>
        /// <param name="filename"> имя файла в котором хранится "Базы Знаний"</param>
        public LogicBase(string filename)
        {
            selfFileName = filename;
        }

        public string FileName
        {
            get { return selfFileName; }
            set { selfFileName = value; }
        }

        private Properties properties = new Properties();

        /// <summary>
        /// Описание типов свойств
        /// </summary>
        [XmlArray]
        public Properties ExistingProperties
        {
            get { return properties; }
        }

        private IList ruleSets = new ArrayList();

        /// <summary>
        /// Уровни базы знаний
        /// </summary>
        [XmlArray]
        [XmlArrayItem("RuleSet", typeof(RuleSet))]
        public IList RuleSets
        {
            get { return ruleSets; }
        }

        [XmlIgnore]
        private IList ruleSetInfoLst = new ArrayList();

        /// <summary>
        /// 
        /// </summary>
        [XmlIgnore]
        public IList RuleSetInfoList
        {
            get { return this.ruleSetInfoLst; }
        }

        public void Save()
        {
            FileStream myStream = File.Create(selfFileName);
            SoapFormatter myXmlFormatter = new SoapFormatter();
            myXmlFormatter.Serialize(myStream, this);

            myStream.Close();
        }

        public static LogicBase Open(string filename)
        {

            LogicBase newLogicBase;

            SoapFormatter myXmlFormatter = new SoapFormatter();
            try
            {
                FileStream myStream = File.OpenRead(filename);
                object obj = myXmlFormatter.Deserialize(myStream);
                newLogicBase = (LogicBase)obj;
                myStream.Close();
            }
            catch
            {
                newLogicBase = new LogicBase(filename);
            }



            if (filename[0] != char.Parse("."))
                newLogicBase.FileName = filename;
            else
                newLogicBase.FileName = filename.Replace(".\\", Directory.GetCurrentDirectory() + "\\");

            return newLogicBase;
        }
    }
}