using System;
using System.Collections;

namespace Shturman.Sherlock.TrueLogic
{
	/// <summary>
	/// Summary description for Properties.
	/// </summary>
	[Serializable]
	public class Properties : IList
	{
		private IList _propertiesInfoList = new ArrayList();

		public Properties()
		{
		}

		public int Add(CustomPropertyInfo value)
		{
			return _propertiesInfoList.Add(value);
		}

		public bool Contains(CustomPropertyInfo item)
		{
			return _propertiesInfoList.Contains(item);
		}

		#region IList Members

		public bool IsReadOnly
		{
			get { return _propertiesInfoList.IsReadOnly; }
		}

		public object this[int index]
		{
			get { return _propertiesInfoList[index]; }
			set
			{
				if (!IsReadOnly)
					_propertiesInfoList[index] = value;
			}
		}

		public void Insert(int index, object value)
		{
			_propertiesInfoList.Insert(index, value);
		}

		public void Remove(object value)
		{
			_propertiesInfoList.Remove(value);
		}

		public void RemoveAt(int index)
		{
			_propertiesInfoList.RemoveAt(index);
		}

		public bool Contains(object value)
		{
			return _propertiesInfoList.Contains(value);
		}

		public void Clear()
		{
			_propertiesInfoList.Clear();
		}

		public int IndexOf(object value)
		{
			return _propertiesInfoList.IndexOf(value);
		}

		int IList.Add(object value)
		{
			return _propertiesInfoList.Add(value);
		}

		public bool IsFixedSize
		{
			get { return _propertiesInfoList.IsFixedSize; }
		}

		#endregion

		#region ICollection Members

		public bool IsSynchronized
		{
			get { return _propertiesInfoList.IsSynchronized; }
		}

		public int Count
		{
			get { return _propertiesInfoList.Count; }
		}

		public void CopyTo(Array array, int index)
		{
			_propertiesInfoList.CopyTo(array, index);
		}

		public object SyncRoot
		{
			get { return _propertiesInfoList.SyncRoot; }
		}

		#endregion

		#region IEnumerable Members

		public IEnumerator GetEnumerator()
		{
			return _propertiesInfoList.GetEnumerator();
		}

		#endregion
	}
}