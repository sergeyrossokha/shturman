using System;
using System.Collections;

namespace Shturman.Sherlock.TrueLogic
{
	/// <summary>
	/// Summary description for FactDefinition.
	/// </summary>
	[Serializable]
	public class PropertyInfo : CustomPropertyInfo
	{
		private IList _propertyValues = new ArrayList();

		public PropertyInfo(string propertyName, IList propertyValues) : base(propertyName)
		{
			_propertyValues = propertyValues;
		}

		public override IList PropertyValues
		{
			get { return _propertyValues; }
		}
	}
}