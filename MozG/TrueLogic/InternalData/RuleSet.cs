﻿using System;
using System.Collections;

namespace Shturman.Sherlock.TrueLogic
{
    /// <summary>
    /// Набор правил связанных одним целевым признаком
    /// Аналог "Кванта знаний" второго уровня по теория И.Б.Сироджи 
    /// </summary>
    [Serializable]
    public class RuleSet
    {
        // Метаданные
        private RuleSetInfo _ruleSetInfo;
        // список правил
        private IList _rulesSet = new ArrayList();

        public RuleSet(RuleSetInfo ruleSetInfo)
        {
            _ruleSetInfo = ruleSetInfo;
        }

        public void AddRule(Rule rule)
        {
            lock (_rulesSet)
            {
                if (_rulesSet.Count != 0)
                    // Проверяем чтобы все правила давали результат одного типа
                    if (_ruleSetInfo.GoalProperty.PropertyName == rule.RuleGoal.ItemName)
                        _rulesSet.Add(rule);
                    else
                    {
                        // Error
                    }
                else
                    _rulesSet.Add(rule);
            }
        }

        public Rule GetRule(int id)
        {
            return (Rule)_rulesSet[id];
        }

        public IList Evaluate(IList facts)
        {
            IList goalFactsSet = new ArrayList();
            foreach (Rule rule in _rulesSet)
                rule.EvaluateRule(facts, goalFactsSet);
            return goalFactsSet;
        }

        /// <summary>
        /// Название целевого признака
        /// </summary>
        public string GoalPropertyName
        {
            get { return _ruleSetInfo.GoalProperty.PropertyName; }
        }

        /// <summary>
        /// Метаданные набора правил 
        /// </summary>
        public RuleSetInfo Info
        {
            get { return this._ruleSetInfo; }
        }

        /// <summary>
        /// Набор правил связаннях одной целью(одним целевым признаком) 
        /// </summary>
        public IList Rules
        {
            get { return this._rulesSet; }
        }

        /// <summary>
        /// Индексированное свойство для доступа к правилам
        /// </summary>
        public Rule this[int index]
        {
            get { return this._rulesSet[index] as Rule; }
        }

        public override string ToString()
        {
            return Info.ToString() + " [" + GoalPropertyName + "]";
        }

    }
}