using System;
using System.Collections;

namespace Shturman.Sherlock.TrueLogic
{
	/// <summary>
	/// Summary description for RuleItem.
	/// </summary>
	[Serializable]
	public class RuleItem
	{
		private string _itemName;
		private IList _itemExpressions = new ArrayList();

		public RuleItem(string itemName)
		{
			_itemName = itemName;
		}

		public string ItemName
		{
			get { return _itemName; }
			set { _itemName = value; }
		}

		public IList ItemExpressions
		{
			get { return _itemExpressions; }
		}
	}
}