﻿using System;

namespace Shturman.Sherlock.TrueLogic
{
    /// <summary>
    /// Summary description for RuleInfo.
    /// </summary>
    [Serializable]
    public class RuleSetInfo
    {
        // целевой признак
        private CustomPropertyInfo _goalProperty = null;
        // признаки
        private Properties _properties = new Properties();

        private string _caption = "";

        /// <summary>
        /// Конструктор
        /// </summary>
        /// <param name="goalProperty">целевой признак</param>
        public RuleSetInfo(string caption, CustomPropertyInfo goalProperty)
        {
            this._caption = caption;
            this._goalProperty = goalProperty;
        }

        #region Properties

        /// <summary>
        /// Целевой признак
        /// </summary>
        public CustomPropertyInfo GoalProperty
        {
            get { return this._goalProperty; }
        }

        /// <summary>
        /// Свойства доступные для задания правил
        /// </summary>
        public Properties PropertiesInfoList
        {
            get { return this._properties; }
            set { this._properties = value; }
        }

        public string Caption
        {
            get { return this._caption; }
            set { this._caption = value; }
        }

        #endregion

        public override string ToString()
        {
            return this._caption;
        }

    }
}

