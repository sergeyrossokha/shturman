﻿using System;

namespace Shturman.Sherlock.TrueLogic
{
    /// <summary>
    /// Summary description for Fact.
    /// </summary>
    [Serializable]
    public class Fact
    {
        private string _name = ""; // Наименование признака
        private object _value; // Значение признака
        private bool _hasValue = false; // Флаг определяющий установленно ли для данного факта значение
        private float _factPd = 0.0f; //Коэффициент уверенности в значении данного признака

        public Fact(string Name)
        {
            if (Name == null || Name == String.Empty)
                throw new ArgumentException("Name must not be null or empty string.");

            this._name = Name;
        }

        public Fact(string Name, object Value, float Pd)
            : this(Name)
        {
            this._value = Value;
            _hasValue = true;
            _factPd = Pd;
        }

        /// <summary>
        /// Наименование факта
        /// </summary>
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        /// <summary>
        /// Числовое значение факта 
        /// </summary>
        public object Value
        {
            get { return _value; }
            set
            {
                _value = value;
                _hasValue = true;
            }
        }

        /// <summary>
        /// Признак определяющий установлено ли значение данного факта
        /// </summary>
        public bool HasValue
        {
            get { return _hasValue; }
        }

        public float FactPd
        {
            get { return _factPd; }
            set { _factPd = value; }
        }

        #region overrided methods

        public override bool Equals(object obj)
        {
            if (obj is Fact)
                return (obj as Fact).Name == this.Name && (obj as Fact).Value == this.Value;
            else
                return base.Equals(obj);
        }

        public override string ToString()
        {
            if (this._factPd == 1.0f)
                return this._name + "=" + this.Value;
            else
                return this._name + "=" + this.Value + "(ПД = " + this._factPd + ")";
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        #endregion
    }
}