﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Shturman.Nestor.Interfaces;
using Shturman.Sherlock.TrueLogic;

namespace Shturman.Sherlock.Forms
{
    /// <summary>
    /// Summary description for NewRule.
    /// </summary>
    public class RuleDlg : Form
    {
        private Button button2;
        private Button button1;
        private TreeView treeView1;
        private ContextMenu contextMenu1;
        private MenuItem menuItem1;
        private MenuItem menuItem2;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private Container components = null;

        private MenuItem menuItem3;
        private MenuItem menuItem4;

        private IObjectStorage _os = null;
        private Rule _rule;
        private RuleSetInfo _ruleSetInfo;

        public RuleDlg(IObjectStorage os, Rule rule, RuleSetInfo rsi)
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            _os = os;
            _rule = rule;
            _ruleSetInfo = rsi;

            TreeNode tn = new TreeNode();
            tn.Text = rsi.Caption;
            tn.Tag = rule;

            // Задание характеристических празнаков
            TreeNode iftn = new TreeNode();
            iftn.Text = "<ЕСЛИ>";

            for (int domenindex = 0; domenindex < rsi.PropertiesInfoList.Count; domenindex++)
            {
                TreeNode domtn = new TreeNode();
                domtn.Text = (rsi.PropertiesInfoList[domenindex] as CustomPropertyInfo).PropertyName;
                domtn.Tag = rsi.PropertiesInfoList[domenindex];

                if (rule.RuleItems.Count > 0)
                {
                    foreach (RuleItem ri in rule.RuleItems)
                    {
                        //domtn.Tag = rule.RuleItems[domenindex] as RuleItem;
                        if (ri.ItemName == domtn.Text && ri.ItemExpressions.Count > 0)
                            foreach (ExpressionItem ei in ri.ItemExpressions)
                            {
                                TreeNode domval = new TreeNode();
                                if (ei.ItemValue != null)
                                    domval.Text = ei.ItemValue.ToString();
                                else
                                    domval.Text = "(null)";
                                domval.Tag = ei;
                                domtn.Nodes.Add(domval);
                            }
                    }
                }
                iftn.Nodes.Add(domtn);
            }
            tn.Nodes.Add(iftn);

            // Задание целевого признака
            TreeNode thenDom = new TreeNode();
            thenDom.Text = "<ТО>";

            TreeNode goalDom = new TreeNode();
            goalDom.Text = rsi.GoalProperty.PropertyName;
            goalDom.Tag = rsi.GoalProperty;
            if (rule.RuleGoal != null)
                foreach (ExpressionItem ei in rule.RuleGoal.ItemExpressions)
                {
                    TreeNode domval = new TreeNode();
                    if (ei.ItemValue != null)
                        domval.Text = ei.ItemValue.ToString();
                    else
                        domval.Text = "(пусто)";
                    domval.Tag = ei;
                    goalDom.Nodes.Add(domval);
                }
            thenDom.Nodes.Add(goalDom);
            tn.Nodes.Add(thenDom);

            this.treeView1.Nodes.Add(tn);

            // Раскріваю дерево
            this.treeView1.ExpandAll();
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.contextMenu1 = new System.Windows.Forms.ContextMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(232, 312);
            this.button2.Name = "button2";
            this.button2.TabIndex = 7;
            this.button2.Text = "Отменить";
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button1.Location = new System.Drawing.Point(152, 312);
            this.button1.Name = "button1";
            this.button1.TabIndex = 6;
            this.button1.Text = "Применить";
            // 
            // treeView1
            // 
            this.treeView1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.treeView1.ContextMenu = this.contextMenu1;
            this.treeView1.ImageIndex = -1;
            this.treeView1.Location = new System.Drawing.Point(8, 8);
            this.treeView1.Name = "treeView1";
            this.treeView1.SelectedImageIndex = -1;
            this.treeView1.Size = new System.Drawing.Size(296, 296);
            this.treeView1.TabIndex = 8;
            // 
            // contextMenu1
            // 
            this.contextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[]
{
this.menuItem1,
this.menuItem2,
this.menuItem3,
this.menuItem4
});
            this.contextMenu1.Popup += new System.EventHandler(this.contextMenu1_Popup);
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 0;
            this.menuItem1.Text = "Добавить значение";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 1;
            this.menuItem2.Text = "Удалить значение";
            this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
            // 
            // menuItem3
            // 
            this.menuItem3.Index = 2;
            this.menuItem3.Text = "-";
            // 
            // menuItem4
            // 
            this.menuItem4.Index = 3;
            this.menuItem4.Text = "Отмена";
            this.menuItem4.Click += new System.EventHandler(this.menuItem4_Click);
            // 
            // RuleDlg
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(312, 341);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "RuleDlg";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Новое правило*";
            this.ResumeLayout(false);

        }

        #endregion

        private void menuItem1_Click(object sender, EventArgs e)
        {
            SelectValueDlg selectValueFrm = new SelectValueDlg((this.treeView1.SelectedNode.Tag as CustomPropertyInfo));
            if (selectValueFrm.ShowDialog() == DialogResult.OK)
            {
                RuleItem ri = null;
                bool isnew = true;
                // поиск среди посылочных признаков
                foreach (RuleItem rii in _rule.RuleItems)
                    if (rii.ItemName == (this.treeView1.SelectedNode.Tag as CustomPropertyInfo).PropertyName)
                    {
                        ri = rii;
                        isnew = false;
                        break;
                    }
                // поиск среди целевых признаков
                if (ri == null)
                    if (_rule.RuleGoal != null)
                        if (_rule.RuleGoal.ItemName == (this.treeView1.SelectedNode.Tag as CustomPropertyInfo).PropertyName)
                        {
                            ri = _rule.RuleGoal;
                            isnew = false;
                        }
                // если ранее не был создан данный элемент, то создаем его
                if (ri == null)
                    ri = new RuleItem((this.treeView1.SelectedNode.Tag as CustomPropertyInfo).PropertyName);

                foreach (object obj in selectValueFrm.SelectedItems)
                {
                    if (this.treeView1.SelectedNode.Tag is CustomPropertyInfo)
                    {
                        ExpressionItem ei = new ExpressionItem(ExpressionItem.ExpressionOperator.iboEqual, obj, (float)0.0);
                        ri.ItemExpressions.Add(ei);

                        TreeNode domval = new TreeNode();
                        domval.Text = obj.ToString();
                        domval.Tag = ei;
                        this.treeView1.SelectedNode.Nodes.Add(domval);
                    }
                }
                if (isnew)
                {
                    // определение целевой это признак или нет
                    if (ri.ItemName == _ruleSetInfo.GoalProperty.PropertyName)
                        // если целевой то добавляю
                        _rule.RuleGoal = ri;
                    else
                        _rule.RuleItems.Add(ri);
                }


            }
            this.treeView1.SelectedNode.ExpandAll();
        }

        private void contextMenu1_Popup(object sender, EventArgs e)
        {
            if (this.treeView1.SelectedNode.Tag is ExpressionItem)
            {
                menuItem1.Visible = false;
                menuItem2.Visible = true;
                menuItem3.Visible = true;
                menuItem4.Visible = true;
            }
            else if (this.treeView1.SelectedNode.Tag is CustomPropertyInfo)
            {
                menuItem1.Visible = true;
                menuItem2.Visible = false;
                menuItem3.Visible = true;
                menuItem4.Visible = true;
            }
            else
            {
                menuItem1.Visible = false;
                menuItem2.Visible = false;
                menuItem3.Visible = false;
                menuItem4.Visible = false;
            }
        }

        private void menuItem4_Click(object sender, EventArgs e)
        {
        }

        private void menuItem2_Click(object sender, EventArgs e)
        {
            if (this.treeView1.SelectedNode.Tag is ExpressionItem)
            {
                string itemName = (this.treeView1.SelectedNode.Parent.Tag as CustomPropertyInfo).PropertyName;
                RuleItem ri = null;

                // поиск среди посылочных признаков
                foreach (RuleItem rii in _rule.RuleItems)
                    if (rii.ItemName == itemName)
                    {
                        ri = rii;
                        break;
                    }
                // поиск среди целевых признаков
                if (ri == null)
                    if (_rule.RuleGoal != null)
                        if (_rule.RuleGoal.ItemName == itemName)
                        {
                            ri = _rule.RuleGoal;
                        }

                if (ri != null)
                {
                    ri.ItemExpressions.Remove(this.treeView1.SelectedNode.Tag);
                    this.treeView1.Nodes.Remove(this.treeView1.SelectedNode);
                }
            }
        }
    }
}