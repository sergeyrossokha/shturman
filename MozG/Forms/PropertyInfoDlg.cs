﻿using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

using Shturman.Nestor.Interfaces;

using Shturman.Sherlock.TrueLogic;

namespace Shturman.Sherlock.Forms
{
    /// <summary>
    /// Summary description for NewDomen.
    /// </summary>
    public class NewPropertyInfoDlg : System.Windows.Forms.Form
    {
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.ComboBox cbExistingDbObjects;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.TextBox textDomenName;
        private System.Windows.Forms.Label labelDomenName;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.Label label1;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
        private IObjectStorage _os = null;

        public NewPropertyInfoDlg(IObjectStorage os)
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            _os = os;
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelDomenName = new System.Windows.Forms.Label();
            this.textDomenName = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.cbExistingDbObjects = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelDomenName
            // 
            this.labelDomenName.Location = new System.Drawing.Point(8, 8);
            this.labelDomenName.Name = "labelDomenName";
            this.labelDomenName.Size = new System.Drawing.Size(64, 16);
            this.labelDomenName.TabIndex = 0;
            this.labelDomenName.Text = "Имя домена";
            // 
            // textDomenName
            // 
            this.textDomenName.Location = new System.Drawing.Point(72, 4);
            this.textDomenName.Name = "textDomenName";
            this.textDomenName.Size = new System.Drawing.Size(192, 20);
            this.textDomenName.TabIndex = 1;
            this.textDomenName.Text = "";
            this.textDomenName.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textDomenName_KeyPress);
            // 
            // checkBox1
            // 
            this.checkBox1.Location = new System.Drawing.Point(6, 32);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(258, 16);
            this.checkBox1.TabIndex = 2;
            this.checkBox1.Text = "Значения признака хранятся в БД";
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // cbExistingDbObjects
            // 
            this.cbExistingDbObjects.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbExistingDbObjects.Enabled = false;
            this.cbExistingDbObjects.Location = new System.Drawing.Point(72, 51);
            this.cbExistingDbObjects.Name = "cbExistingDbObjects";
            this.cbExistingDbObjects.Size = new System.Drawing.Size(192, 21);
            this.cbExistingDbObjects.TabIndex = 3;
            this.cbExistingDbObjects.DropDown += new System.EventHandler(this.cbExistingDbObjects_DropDown);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(112, 83);
            this.button1.Name = "button1";
            this.button1.TabIndex = 4;
            this.button1.Text = "Добавить";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(189, 83);
            this.button2.Name = "button2";
            this.button2.TabIndex = 5;
            this.button2.Text = "Отменить";
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(0, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(72, 16);
            this.label1.TabIndex = 6;
            this.label1.Text = "Объект БД";
            // 
            // NewDomen
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(274, 111);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cbExistingDbObjects);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.textDomenName);
            this.Controls.Add(this.labelDomenName);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "NewDomen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Новый признак*";
            this.ResumeLayout(false);

        }
        #endregion

        private void checkBox1_CheckedChanged(object sender, System.EventArgs e)
        {
            cbExistingDbObjects.Enabled = (sender as CheckBox).Checked;
            //cbExistingDbObjects.DataSource = _os.EntityList();
            foreach (object obj in _os.EntityList())
            {
                object[] eca = (obj as Type).GetCustomAttributes(typeof(Nestor.DataAttributes.EntityCaptionAttribute), true);
                cbExistingDbObjects.Items.Add((eca[0] as Nestor.DataAttributes.EntityCaptionAttribute).EntityCaption);
            }
            if (!(sender as CheckBox).Checked)
                errorProvider1.SetError(cbExistingDbObjects, "");
        }

        private void button1_Click(object sender, System.EventArgs e)
        {
            errorProvider1.SetError(textDomenName, "");
            errorProvider1.SetError(cbExistingDbObjects, "");

            if (textDomenName.Text == "")
            {
                errorProvider1.SetError(textDomenName, "Недопустимое имя домена");
                return;
            }

            if (cbExistingDbObjects.Enabled && cbExistingDbObjects.SelectedIndex == -1)
            {
                errorProvider1.SetError(cbExistingDbObjects, "Не выбран элемент списка");
                return;
            }

            this.DialogResult = DialogResult.OK;
        }

        private void textDomenName_KeyPress(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            errorProvider1.SetError(textDomenName, "");
        }

        private void cbExistingDbObjects_DropDown(object sender, System.EventArgs e)
        {
            errorProvider1.SetError(cbExistingDbObjects, "");
        }

        public CustomPropertyInfo GetPropertyInfo()
        {
            if (checkBox1.Checked)
            {
                PropertyExtInfo pei = new PropertyExtInfo(_os, this.textDomenName.Text, _os.EntityList()[cbExistingDbObjects.SelectedIndex] as Type);
                return pei;
            }
            else
            {
                PropertyInfo pi = new PropertyInfo(this.textDomenName.Text, new ArrayList());
                return pi;
            }
        }
    }
}