﻿using System;
using System.ComponentModel;
using System.Windows.Forms;
using Shturman.Sherlock.TrueLogic;

namespace Shturman.Sherlock.Forms
{
    /// <summary>
    /// Summary description for NewLayerForm.
    /// </summary>
    public class RuleSetInfoDlg : Form
    {
        private Button button2;
        private Button button1;
        private TextBox textBox1;
        private Label label1;
        private Label label2;
        private ComboBox comboPropertyInfo;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private Container components = null;

        public RuleSetInfoDlg(Properties allProperties)
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            this.button1.Enabled = false;
            this.comboPropertyInfo.DataSource = allProperties;
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboPropertyInfo = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(208, 73);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(70, 23);
            this.button2.TabIndex = 11;
            this.button2.Text = "Отменить";
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button1.Location = new System.Drawing.Point(128, 73);
            this.button1.Name = "button1";
            this.button1.TabIndex = 10;
            this.button1.Text = "Добавить";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(125, 5);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(152, 20);
            this.textBox1.TabIndex = 9;
            this.textBox1.Text = "";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(5, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 14);
            this.label1.TabIndex = 8;
            this.label1.Text = "Наименование уровня";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(5, 36);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 14);
            this.label2.TabIndex = 12;
            this.label2.Text = "Целевой признак";
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // comboPropertyInfo
            // 
            this.comboPropertyInfo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboPropertyInfo.Location = new System.Drawing.Point(125, 32);
            this.comboPropertyInfo.Name = "comboPropertyInfo";
            this.comboPropertyInfo.Size = new System.Drawing.Size(153, 21);
            this.comboPropertyInfo.TabIndex = 13;
            this.comboPropertyInfo.SelectedIndexChanged += new System.EventHandler(this.comboPropertyInfo_SelectedIndexChanged);
            // 
            // RuleSetInfoDlg
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(280, 103);
            this.Controls.Add(this.comboPropertyInfo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "RuleSetInfoDlg";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Новый уровень*";
            this.Load += new System.EventHandler(this.RuleSetInfoDlg_Load);
            this.ResumeLayout(false);

        }

        #endregion

        public RuleSetInfo GetRuleSetInfo()
        {
            return new RuleSetInfo(this.textBox1.Text, comboPropertyInfo.SelectedItem as CustomPropertyInfo);
        }

        private void label2_Click(object sender, EventArgs e)
        {
        }

        private void comboPropertyInfo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboPropertyInfo.SelectedItem != null)
                this.button1.Enabled = true;
        }

        private void RuleSetInfoDlg_Load(object sender, EventArgs e)
        {
            this.comboPropertyInfo.SelectedItem = null;
            this.comboPropertyInfo.Text = "";
        }
    }
}