﻿using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Shturman.Sherlock.TrueLogic;

namespace Shturman.Sherlock.Forms
{
    /// <summary>
    /// Summary description for SelectDomen2Layer.
    /// </summary>
    internal class BrowseForPropertyDlg : Form
    {
        private Button button2;
        private Button button1;
        private Label label1;
        private CheckedListBox checkedListProperty;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private Container components = null;

        public BrowseForPropertyDlg(RuleSetInfo rsi, Properties allProperties)
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            foreach (CustomPropertyInfo obj in allProperties)
                if ((obj.PropertyName != rsi.GoalProperty.PropertyName) && (!rsi.PropertiesInfoList.Contains(obj)))
                    this.checkedListProperty.Items.Add(obj);
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.checkedListProperty = new System.Windows.Forms.CheckedListBox();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(160, 320);
            this.button2.Name = "button2";
            this.button2.TabIndex = 11;
            this.button2.Text = "Отменить";
            // 
            // button1
            // 
            this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button1.Location = new System.Drawing.Point(80, 320);
            this.button1.Name = "button1";
            this.button1.TabIndex = 10;
            this.button1.Text = "Добавить";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(224, 14);
            this.label1.TabIndex = 8;
            this.label1.Text = "Выделите характеристические признаки";
            // 
            // checkedListProperty
            // 
            this.checkedListProperty.Location = new System.Drawing.Point(8, 22);
            this.checkedListProperty.Name = "checkedListProperty";
            this.checkedListProperty.Size = new System.Drawing.Size(224, 289);
            this.checkedListProperty.TabIndex = 12;
            // 
            // BrowseForPropertyDlg
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(238, 349);
            this.Controls.Add(this.checkedListProperty);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "BrowseForPropertyDlg";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Выбор признаков";
            this.ResumeLayout(false);

        }

        #endregion

        public IList LayerProperties
        {
            get { return this.checkedListProperty.CheckedItems; }
        }
    }
}