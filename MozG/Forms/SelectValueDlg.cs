﻿using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Shturman.Sherlock.TrueLogic;

namespace Shturman.Sherlock.Forms
{
    /// <summary>
    /// Summary description for SelectDomenValue.
    /// </summary>
    public class SelectValueDlg : Form
    {
        private Button button2;
        private Button button1;
        private CheckedListBox checkedListBox1;
        private Label label1;

        /// <summary>
        /// Required designer variable.
        /// </summary>
        private Container components = null;

        public SelectValueDlg(CustomPropertyInfo cpi)
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            foreach (object obj in cpi.PropertyValues)
                this.checkedListBox1.Items.Add(obj);
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(157, 320);
            this.button2.Name = "button2";
            this.button2.TabIndex = 9;
            this.button2.Text = "Отменить";
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.button1.Location = new System.Drawing.Point(77, 320);
            this.button1.Name = "button1";
            this.button1.TabIndex = 8;
            this.button1.Text = "Добавить";
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.checkedListBox1.CheckOnClick = true;
            this.checkedListBox1.IntegralHeight = false;
            this.checkedListBox1.Location = new System.Drawing.Point(8, 24);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(224, 288);
            this.checkedListBox1.TabIndex = 10;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(224, 23);
            this.label1.TabIndex = 11;
            this.label1.Text = "Выберите возможные значения";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // SelectDomenValue
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(238, 349);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.checkedListBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "SelectDomenValue";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Выбор значений";
            this.ResumeLayout(false);

        }

        #endregion

        public IList SelectedItems
        {
            get { return this.checkedListBox1.CheckedItems; }
        }
    }
}