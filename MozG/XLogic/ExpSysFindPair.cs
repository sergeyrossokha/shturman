using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.XPath;

namespace Shturman.Sherlock.XLogic
{
    public class ExpSysFindPair : CustomExpSys
    {
        // strings template for building XPath query
        protected string layerName = "BrowseForLessonNum";
        protected string goalPropertyName = "LessonNum";

        // Properties by default
        protected string[] properties = { "StudyType"};
        protected string[] properties2 = { "Teacher" };

        #region Constructors
        public ExpSysFindPair()
            : base()
        {
        }

        public ExpSysFindPair(string[] propertiesList)
            : this()
        {
            properties = propertiesList;
        }
        #endregion

        public string Find(XPathDocument xPathDoc, IRuleContext ruleContext)
        {
            string[] xPathQueryList = new string[]
            {
                base.BuildXPathQuery(layerName, goalPropertyName, properties, ruleContext),
                base.BuildXPathQuery(layerName, goalPropertyName, properties2, ruleContext)
            };

            return base.RunXPathQuery(xPathDoc, xPathQueryList, true);
        }
    }
}
