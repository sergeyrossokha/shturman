using System;
using System.Collections.Generic;
using System.Text;

namespace Shturman.Sherlock.XLogic
{
    public interface IRuleContext
    {
        void AddRuleContext(string property, string value);
        void RemoveRuleContext(string property);
        void ClearContext();

        string GetContext(string property);
    }
}
