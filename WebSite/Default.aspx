﻿<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Інтернет сторінка проекту "Штурман-Розклад"</title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="font-size: 16pt; color: red; font-style: italic; font-family: Arial; text-align: right">
        Штурман-Розклад<br />
        <hr />
        </div>
        <div>
         <asp:Menu  ID="Menu1" runat="server" Height="37px" Orientation="Horizontal" Width="330px" OnMenuItemClick="Menu1_MenuItemClick" style="cursor: hand" BackColor="#B5C7DE" DynamicHorizontalOffset="2" Font-Names="Verdana" Font-Size="0.8em" ForeColor="#284E98" StaticSubMenuIndent="10px">
            <Items>
                <asp:MenuItem Text="Довідники" Value="Справочные данные">
                    <asp:MenuItem Text="Факультети" Value="Faculty"></asp:MenuItem>
                    <asp:MenuItem Text="Кафедри" Value="Department"></asp:MenuItem>
                    <asp:MenuItem Text="Спеціальності" Value="Specialty"></asp:MenuItem>
                    <asp:MenuItem Text="Викладачі" Value="Tutor"></asp:MenuItem>
                    <asp:MenuItem Text="Аудиторії" Value="LectureHall"></asp:MenuItem>
                    <asp:MenuItem Text="Групи" Value="Group"></asp:MenuItem>
                </asp:MenuItem>
                <asp:MenuItem Text="Переглянути розклад" Value="Просмотреть расписание">
                    <asp:MenuItem Text="Викладача" Value="Викладача"></asp:MenuItem>
                    <asp:MenuItem Text="Групи" Value="Групи"></asp:MenuItem>
                    <asp:MenuItem Text="Аудиторії" Value="Аудиторії"></asp:MenuItem>
                </asp:MenuItem>
                <asp:MenuItem Text="Про проект" Value="New Item"></asp:MenuItem>
            </Items>
             <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
             <DynamicHoverStyle BackColor="#284E98" ForeColor="White" />
             <DynamicMenuStyle BackColor="#B5C7DE" />
             <StaticSelectedStyle BackColor="#507CD1" />
             <DynamicSelectedStyle BackColor="#507CD1" />
             <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
             <StaticHoverStyle BackColor="#284E98" ForeColor="White" />
        </asp:Menu>
            &nbsp; &nbsp;&nbsp;
            <asp:Table ID="Table1" runat="server" GridLines="Both">
            </asp:Table>
            <br />
            <asp:Repeater ID="Repeater1" runat="server" DataMember="News" DataSourceID="XmlDataSource1">
            </asp:Repeater>
            <asp:XmlDataSource ID="XmlDataSource1" runat="server" DataFile="~/News.xml" OnTransforming="XmlDataSource1_Transforming">
            </asp:XmlDataSource>
        </div>
    </form>
</body>
</html>
