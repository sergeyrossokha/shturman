<%@ Application Language="C#" %>

<script runat="server">

    void Application_Start(object sender, EventArgs e) 
    {
        string lng = ConfigurationManager.AppSettings["Language"];
        Shturman.MultyLanguage.Vocabruary vocabryary = new Shturman.MultyLanguage.Vocabruary(
            (NameValueCollection)ConfigurationManager.GetSection(lng)
        );
        Application.Add("Vocabruary", vocabryary);
        
        // Code that runs on application startup
        System.Runtime.Remoting.RemotingConfiguration.Configure("~/web.config", false);
    }
    
    void Application_End(object sender, EventArgs e) 
    {
        //  Code that runs on application shutdown

    }
        
    void Application_Error(object sender, EventArgs e) 
    { 
        // Code that runs when an unhandled error occurs

    }

    void Session_Start(object sender, EventArgs e) 
    {
        Shturman.Shedule.Objects.Container.DB4OBridgeRem.theOneObject.Open();
        Session.Add("ObjectStorage", Shturman.Shedule.Objects.Container.DB4OBridgeRem.theOneObject);

        Session.Add("ViewDataPage.Title", "");
        Session.Add("ViewDataPage.Objects", null);
    }

    void Session_End(object sender, EventArgs e) 
    {
        // Code that runs when a session ends. 
        // Note: The Session_End event is raised only when the sessionstate mode
        // is set to InProc in the Web.config file. If session mode is set to StateServer 
        // or SQLServer, the event is not raised.

    }
       
</script>
