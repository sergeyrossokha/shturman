using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using System.Runtime.InteropServices;
using System.Runtime.Remoting;

// ������������ ������� �������
using Shturman.Shedule.Server.Remoting;
using Shturman.Nestor.Interfaces;
using Shturman.Shedule.Objects;
using Shturman.Shedule.Objects.Container;
using System.Collections;
using Shturman.Nestor.DataAttributes;
using System.Reflection;
using Shturman.Nestor.DataEditors;
using System.ComponentModel;
using System.Drawing;

public partial class _Default : System.Web.UI.Page 
{
    private IList _objects = null;
    
    protected void Page_Load(object sender, EventArgs e)
    {
        _objects = (IList)Session["ViewDataPage.Objects"];

       
        //Session.Add("RemoteSheduleServer", new Shturman.Shedule.Server.Remoting.RemoteSheduleServer());
    }

    public void Show_Gid()
    {
        if (_objects != null)
        {
            EntityWrapper ew = new EntityWrapper(_objects[0]);
            PropertyDescriptorCollection pdc = (ew as ICustomTypeDescriptor).GetProperties();
            
            TableHeaderRow thr = new TableHeaderRow();
            foreach (PropertyDescriptor pd in pdc)
            {
                if (!pd.PropertyType.IsInterface)
                {
                    TableHeaderCell thc = new TableHeaderCell();
                    thc.Text = pd.DisplayName;
                    thr.Cells.Add(thc);
                }
            }
            thr.BackColor = Color.LightGray;
            this.Table1.Rows.Add(thr);

            foreach (object obj in _objects)
            {
                TableRow tr = new TableRow();
                foreach (PropertyDescriptor pd in pdc)
                {
                    if (!pd.PropertyType.IsInterface)
                    {

                        TableCell tc = new TableCell();
                        object val = pd.GetValue(obj);
                        if (val != null)
                            tc.Text = val.ToString();
                        else
                            tc.Text = string.Empty;
                        tr.Cells.Add(tc);
                    }
                }
                if (this.Table1.Rows.Count % 2 == 0)
                    tr.BackColor = Color.Snow;
                this.Table1.Rows.Add(tr);
            }
        }
    }

    protected void Menu1_MenuItemClick(object sender, MenuEventArgs e)
    {
        Session["ViewDataPage.Title"] = e.Item.Text;

        switch (e.Item.Value)
        {
            case "Tutor":
                {
                    try
                    {
                        _objects = (Session["ObjectStorage"] as IObjectStorage).ObjectList(typeof(Tutor));
                    }
                    catch
                    {
                        Session["ViewDataPage.Objects"] = null;
                    }
                    break;
                }
            case "Group":
                {
                    try
                    {
                        _objects = (Session["ObjectStorage"] as IObjectStorage).ObjectList(typeof(Group));
                    }
                    catch
                    {
                        Session["ViewDataPage.Objects"] = null;
                    }

                    break;
                }
            case "Faculty":
                {
                    try
                    {
                        _objects = (Session["ObjectStorage"] as IObjectStorage).ObjectList(typeof(Faculty));
                    }
                    catch
                    {
                        Session["ViewDataPage.Objects"] = null;
                    }

                    break;
                }
            case "Department":
                {
                    try
                    {
                        _objects = (Session["ObjectStorage"] as IObjectStorage).ObjectList(typeof(Department));
                    }
                    catch
                    {
                        Session["ViewDataPage.Objects"] = null;
                    }

                    break;
                }
            case "Specialty":
                {
                    try
                    {
                        _objects = (Session["ObjectStorage"] as IObjectStorage).ObjectList(typeof(Specialty));
                    }
                    catch
                    {
                        Session["ViewDataPage.Objects"] = null;
                    }

                    break;
                }
            case "LectureHall":
                {
                    try
                    {
                        _objects = (Session["ObjectStorage"] as IObjectStorage).ObjectList(typeof(LectureHall));
                    }
                    catch
                    {
                        Session["ViewDataPage.Objects"] = null;
                    }

                    break;
                }
        }

        this.Show_Gid();
    }

    protected void Page_UnLoad(object sender, EventArgs e)
    {
       
        Session["ViewDataPage.Objects"] = _objects;
    }
    protected void DataGrid1_DataBinding(object sender, EventArgs e)
    {
    }
    protected void DataGrid1_ItemDataBound(object sender, DataGridItemEventArgs e)
    {

    }
    protected void XmlDataSource1_Transforming(object sender, EventArgs e)
    {

    }
}
