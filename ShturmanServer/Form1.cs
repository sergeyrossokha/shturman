﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.Runtime.Remoting;
using System.Windows.Forms;
using Db4objects.Db4o;
using db4o.mytools;
using Shturman.Shedule.Server.Remoting;
using Shturman.Shedule.Objects;

namespace Shturman.Shedule.Server
{
    /// <summary>
    /// Summary description for Form1.
    /// </summary>
    public class Form1 : Form
    {
        private IContainer components;
        private NotifyIcon notifyIcon1;
        private ContextMenu contextMenu1;
        private MenuItem menuItem1;
        private GroupBox groupBox1;
        private MenuItem menuItem2;
        private MenuItem menuItem3;
        private MenuItem menuItem4;
        private MenuItem menuItem5;
        private TextBox textBox1;
        private MenuItem menuItem6;
        private MenuItem menuItem7;
        private System.Windows.Forms.MenuItem menuItem8;
        private System.Windows.Forms.MenuItem menuItem9;
        private System.Windows.Forms.MenuItem menuItem10;

        private IObjectServer Os = null;

        public Form1()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            this.Visible = false;
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
                //we dispose our tray icon here
                this.notifyIcon1.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(Form1));
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.contextMenu1 = new System.Windows.Forms.ContextMenu();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.menuItem8 = new System.Windows.Forms.MenuItem();
            this.menuItem9 = new System.Windows.Forms.MenuItem();
            this.menuItem10 = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.menuItem5 = new System.Windows.Forms.MenuItem();
            this.menuItem6 = new System.Windows.Forms.MenuItem();
            this.menuItem7 = new System.Windows.Forms.MenuItem();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.ContextMenu = this.contextMenu1;
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "Штурман Сервер";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.DoubleClick += new System.EventHandler(this.notifyIcon1_DoubleClick);
            // 
            // contextMenu1
            // 
            this.contextMenu1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuItem2,
this.menuItem1,
this.menuItem3,
this.menuItem8,
this.menuItem10,
this.menuItem4,
this.menuItem5,
this.menuItem6,
this.menuItem7});
            this.contextMenu1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.contextMenu1.Popup += new System.EventHandler(this.contextMenu1_Popup);
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 0;
            this.menuItem2.Text = "Start Server";
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 1;
            this.menuItem1.Text = "Stop Server";
            this.menuItem1.Click += new System.EventHandler(this.menuItem1_Click);
            // 
            // menuItem3
            // 
            this.menuItem3.Index = 2;
            this.menuItem3.Text = "-";
            // 
            // menuItem8
            // 
            this.menuItem8.Index = 3;
            this.menuItem8.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuItem9});
            this.menuItem8.Text = "DataBase";
            // 
            // menuItem9
            // 
            this.menuItem9.Index = 0;
            this.menuItem9.Text = "Defragmentation";
            this.menuItem9.Click += new System.EventHandler(this.menuItem9_Click);
            // 
            // menuItem10
            // 
            this.menuItem10.Index = 4;
            this.menuItem10.Text = "-";
            // 
            // menuItem4
            // 
            this.menuItem4.Index = 5;
            this.menuItem4.Text = "Open Shedule";
            this.menuItem4.Click += new System.EventHandler(this.menuItem4_Click);
            // 
            // menuItem5
            // 
            this.menuItem5.Index = 6;
            this.menuItem5.Text = "Save Shedule";
            this.menuItem5.Click += new System.EventHandler(this.menuItem5_Click);
            // 
            // menuItem6
            // 
            this.menuItem6.Index = 7;
            this.menuItem6.Text = "-";
            // 
            // menuItem7
            // 
            this.menuItem7.Index = 8;
            this.menuItem7.Text = "About";
            this.menuItem7.Click += new System.EventHandler(this.menuItem7_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(322, 127);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // textBox1
            // 
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.textBox1.Location = new System.Drawing.Point(3, 16);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.ReadOnly = true;
            this.textBox1.Size = new System.Drawing.Size(316, 108);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "Розроблено з використанням:\r\n\tMicrosoft(c) .NET(c) Framework 1.1\r\n\tdb4o 5.0 (c) w" +
            "ww.db4o.com\r\n\tSourceGrid2 2.2 (c) Davide Icardi\r\n\r\nCopyright (c) 2005 Розсоха С" +
            "ергій Володимирович,\r\n\t\tРозсоха Ірина Евгеніївна";
            // 
            // Form1
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(322, 127);
            this.ControlBox = false;
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.ShowInTaskbar = false;
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Hide;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Штурман Сервер";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.Form1_Closing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Closed += new System.EventHandler(this.Form1_Closed);
            this.Activated += new System.EventHandler(this.Form1_Activated);
            this.Deactivate += new System.EventHandler(this.Form1_Deactivate);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }
        #endregion

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            RemotingConfiguration.Configure("Shturman.Shedule.Server.App.exe.config", false);
            Application.Run(new Form1());
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            this.Visible = false;
            /* Set Server Settings */
            NameValueCollection serversettings = ConfigurationManager.GetSection("serversettings") as NameValueCollection;

            /* Index Fields*/
            Db4oFactory.Configure().ObjectClass(typeof(Group)).ObjectField("_department").Indexed(true);
            Db4oFactory.Configure().GenerateUUIDs(Int32.MaxValue);
            Db4oFactory.Configure().GenerateVersionNumbers(Int32.MaxValue);

            Os = Db4oFactory.OpenServer(serversettings["StorageFile"], int.Parse(serversettings["ServerPort"]));

            /* Grant user to use server */
            if (Os != null)
                Os.GrantAccess(serversettings["ServerUser"], serversettings["ServerPwd"]);
            else
                MessageBox.Show("Не удалось запустить сервер базы данных");

            /* Запуск сервера расписания */


            this.Text += " " + ConfigurationManager.AppSettings["ServerVersion"];
        }

        private void Form1_Closing(object sender, CancelEventArgs e)
        {
            this.Visible = false;
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            this.Visible = true;
        }

        private void Form1_Closed(object sender, EventArgs e)
        {
            this.notifyIcon1.Visible = false;
        }

        private void menuItem1_Click(object sender, EventArgs e)
        {
            if (Os != null)
                Os.Close();

            this.Close();
        }

        private void Form1_Activated(object sender, EventArgs e)
        {
            if (this.Tag == null)
            {
                this.Tag = 1;
                this.Visible = false;
            }
        }

        private void menuItem5_Click(object sender, EventArgs e)
        {
            //new RemoteSheduleServer().Save();
        }

        private void menuItem4_Click(object sender, EventArgs e)
        {
            OpenFileDialog opfdlg = new OpenFileDialog();

            opfdlg.Filter = "shedule files (*.shedule)|*.shedule|All files (*.*)|*.*";
            opfdlg.DefaultExt = "shedule";

            if (opfdlg.ShowDialog() == DialogResult.OK)
            {
                //new RemoteSheduleServer(). Open(opfdlg.FileName);

                //IShedule shdl = new RemoteSheduleServer().NewShedule("1.shedule");

                //NameValueCollection serversettings = ConfigurationSettings.GetConfig("serversettings") as NameValueCollection;
                //ObjectContainer objectContainer = Os.OpenClient(
                //	"localhost", 
                //	int.Parse(serversettings["ServerPort"]), 
                //	serversettings["ServerUser"], 
                //	serversettings["ServerPwd"]);
            }
        }

        private void Form1_Deactivate(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        private void menuItem7_Click(object sender, EventArgs e)
        {
            this.Visible = true;
        }

        private void contextMenu1_Popup(object sender, System.EventArgs e)
        {

        }

        private void menuItem9_Click(object sender, System.EventArgs e)
        {
            NameValueCollection serversettings = ConfigurationManager.GetSection("serversettings") as NameValueCollection;

            if (Os != null)
                Os.Close();

            new Defragment().Run(serversettings["StorageFile"], true);
        }
    }
}