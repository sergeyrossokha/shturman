using System.Collections.Specialized;

namespace Shturman.MultyLanguage
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class Vocabruary
	{
		private static NameValueCollection _vocabruary = null;
		
		public Vocabruary()
		{
		}

		public Vocabruary(NameValueCollection nvc): this()
		{
			_vocabruary = nvc;
		}

		public static string Translate(string index)
		{
            try
            {
                return _vocabruary[index];
            }
            catch
            {
                return index;
            }
		}
	}
}
