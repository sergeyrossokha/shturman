using System;
using System.ComponentModel;
using Shturman.Nestor.DataAttributes;
using Shturman.Shedule.Objects;

namespace Shturman.Shedule
{
	/// <summary>
	/// ��������� ����� �����������
	/// </summary>
	public class Restrict2
	{
		private DateTime day;
		private StudyHour hour;

		private string _restrictMessage = "";

		public Restrict2()
		{
			_restrictMessage = "";
		}

		public Restrict2(string restrictMessage)
		{
			_restrictMessage = restrictMessage;
		}

		[PropertyCaption("���� ������")]
		[PropertyVisible(true)]
		[PropertyIndex(1)]
		[PropertyReference(typeof(StudyDay))]
		[PropertyNotNull(true)]
		[Description("������� ���� ������")]
		public DateTime RestrictDay
		{
			get{ return day; }
			set{ day = value; }
		}
		
		[PropertyCaption("����� �������")]
		[PropertyVisible(true)]
		[PropertyIndex(2)]
		[PropertyReference(typeof(StudyHour))]
		[PropertyNotNull(true)]
		[Description("����� �������")]
		public StudyHour RestrictPair
		{
			get{ return hour; }
			set{ hour = value; }
		}

		[PropertyCaption("����������")]
		[PropertyVisible(true)]
		[PropertyIndex(3)]
		[Description("����������, ������� �����������")]
		public string Comment
		{
			get {return _restrictMessage;}
			set {_restrictMessage = value;}
		}

		public override string ToString()
		{
			return RestrictDay.ToShortDateString()+"  "+RestrictPair.ToString()+" [ "+Comment+" ]";
		}

	}
}
