﻿using System;
using System.Collections.Generic;

namespace Shturman.Shedule.Import.New
{
    public interface IExcelReader
    {
        string FileName { get; set; }
        List<string> Sheets { get; }

        event ColumnReadCompleate OnColumnRead;
        event DocumentEvent OnDocumentReadEnd;
        event DocumentEvent OnDocumentReadStart;
        event RowReadCompleate OnRowRead;
        event SheetReadEvent OnSheetReadBegin;
        event SheetReadEvent OnSheetReadEnd;
        
        void ParseDocument();
    }
}
