using System;
using System.Collections.Generic;
using System.Text;

using System.ComponentModel;
using Shturman.Nestor.DataAttributes;

namespace Shturman.Shedule.Import.New
{
    public class ImportSupplementSchema
    {
        int _facultyRow = 2;
        int _facultyColumn = 10;
        int _departmentRow = 3;
        int _departmentColumn = 10;

        int _itemNumberColumn = 1;
        int _itemSubjectNameColumn = 2;
        int _itemSubjectTypeNameColumn = 3;
        int _itemHoursColumnStart = 4;
        int _itemHoursColumn = 4;
        int _itemGroupNameColumn = 6;
        int _itemGroupStudentCountColumn = 7;
        int _itemTutorPostColumn = 8;
        int _itemTutorNameColumn = 9;
        int _itemFlatColumn = 10;
        int _itemComment = 11;

        #region Properties ImportSupplementSchema
        [PropertyCaption("ImportSupplementSchema.FacultyRow.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(0)]
        [Description("ImportSupplementSchema.FacultyRow.Description")]
        public int FacultyRow
        { get {return _facultyRow; } set {_facultyRow = value; }}

        [PropertyCaption("ImportSupplementSchema.FacultyColumn.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(1)]
        [Description("ImportSupplementSchema.FacultyColumn.Description")]
        public int FacultyColumn
        { get { return _facultyColumn; } set { _facultyColumn = value; } }

        [PropertyCaption("ImportSupplementSchema.DepartmentRow.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(2)]
        [Description("ImportSupplementSchema.DepartmentRow.Description")]
        public int DepartmentRow
        { get { return _departmentRow;} set {_departmentRow = value;}  }

        [PropertyCaption("ImportSupplementSchema.DepartmentColumn.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(3)]
        [Description("ImportSupplementSchema.DepartmentColumn.Description")]
        public int DepartmentColumn
        { get { return _departmentColumn; } set { _departmentColumn = value; } }

        [PropertyCaption("ImportSupplementSchema.NumberColumn.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(4)]
        [Description("ImportSupplementSchema.NumberColumn.Description")]
        public int NumberColumn
        { get {return _itemNumberColumn;} }

        [PropertyCaption("ImportSupplementSchema.SubjectNameColumn.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(5)]
        [Description("ImportSupplementSchema.SubjectNameColumn.Description")]
        public int SubjectNameColumn
        { get { return _itemSubjectNameColumn; } set { _itemSubjectNameColumn = value; } }

        [PropertyCaption("ImportSupplementSchema.SubjectTypeNameColumn.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(6)]
        [Description("ImportSupplementSchema.SubjectTypeNameColumn.Description")]
        public int SubjectTypeNameColumn
        { get { return _itemSubjectTypeNameColumn; } set { _itemSubjectTypeNameColumn = value; } }

        [PropertyCaption("ImportSupplementSchema.HoursColumn.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(7)]
        [Description("ImportSupplementSchema.HoursColumn.Description")]
        public int HoursColumn
        { get { return _itemHoursColumn; } set { _itemHoursColumn = value; } }

        [PropertyCaption("ImportSupplementSchema.HoursColumnStart.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(7)]
        [Description("ImportSupplementSchema.HoursColumnStart.Description")]
        public int HoursColumnStart
        { get { return _itemHoursColumnStart; } set { _itemHoursColumnStart = value; } }

        [PropertyCaption("ImportSupplementSchema.GroupNameColumn.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(8)]
        [Description("ImportSupplementSchema.GroupNameColumn.Description")]
        public int GroupNameColumn
        { get { return _itemGroupNameColumn; } set { _itemGroupNameColumn = value; } }

        [PropertyCaption("ImportSupplementSchema.GroupStudentCountColumn.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(9)]
        [Description("ImportSupplementSchema.GroupStudentCountColumn.Description")]
        public int GroupStudentCountColumn
        { get { return _itemGroupStudentCountColumn; } set { _itemGroupStudentCountColumn = value; } }

        [PropertyCaption("ImportSupplementSchema.TutorPostColumn.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(10)]
        [Description("ImportSupplementSchema.TutorPostColumn.Description")]
        public int TutorPostColumn
        { get { return _itemTutorPostColumn; } set { _itemTutorPostColumn = value; } }

        [PropertyCaption("ImportSupplementSchema.TutorNameColumn.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(11)]
        [Description("ImportSupplementSchema.TutorNameColumn.Description")]
        public int TutorNameColumn
        { get { return _itemTutorNameColumn; } set { _itemTutorNameColumn = value; } }

        [PropertyCaption("ImportSupplementSchema.FlatColumn.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(12)]
        [Description("ImportSupplementSchema.FlatColumn.Description")]
        public int FlatColumn
        { get { return _itemFlatColumn; } set { _itemFlatColumn = value; } }

        [PropertyCaption("ImportSupplementSchema.Comment.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(12)]
        [Description("ImportSupplementSchema.Comment.Description")]
        public int Comment
        { get { return _itemComment; } set { _itemComment = value; } }
        #endregion ImportSupplementSchema
    }
}
