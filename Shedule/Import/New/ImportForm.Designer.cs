namespace Shturman.Shedule.Import.New
{
    partial class ImportForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ImportForm));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripOpenButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSaveButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripScanButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripScanAllButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.stripButtonEdit = new System.Windows.Forms.ToolStripButton();
            this.stripButtonDrop = new System.Windows.Forms.ToolStripButton();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripProgressBar1 = new System.Windows.Forms.ToolStripProgressBar();
            this.ViewReadItems = new System.Windows.Forms.ListView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.splitter1 = new System.Windows.Forms.Splitter();
            this.listViewRecognize = new System.Windows.Forms.ListView();
            this.recognizeWorker = new System.ComponentModel.BackgroundWorker();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripOpenButton,
            this.toolStripSaveButton,
            this.toolStripLabel1,
            this.toolStripScanButton,
            this.toolStripScanAllButton,
            this.toolStripButton1,
            this.toolStripLabel2,
            this.stripButtonEdit,
            this.stripButtonDrop,
            this.toolStripLabel3,
            this.toolStripProgressBar1});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(1033, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripOpenButton
            // 
            this.toolStripOpenButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripOpenButton.Image = ((System.Drawing.Image)(resources.GetObject("toolStripOpenButton.Image")));
            this.toolStripOpenButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripOpenButton.Name = "toolStripOpenButton";
            this.toolStripOpenButton.Size = new System.Drawing.Size(23, 22);
            this.toolStripOpenButton.Text = "³������ ������� �� ������������";
            this.toolStripOpenButton.Click += new System.EventHandler(this.toolStripOpenButton_Click);
            // 
            // toolStripSaveButton
            // 
            this.toolStripSaveButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripSaveButton.Image = ((System.Drawing.Image)(resources.GetObject("toolStripSaveButton.Image")));
            this.toolStripSaveButton.ImageTransparentColor = System.Drawing.Color.White;
            this.toolStripSaveButton.Name = "toolStripSaveButton";
            this.toolStripSaveButton.Size = new System.Drawing.Size(23, 22);
            this.toolStripSaveButton.Text = "������� ��������� ������";
            this.toolStripSaveButton.Click += new System.EventHandler(this.toolStripSaveButton_Click);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(65, 22);
            this.toolStripLabel1.Text = "���������:";
            this.toolStripLabel1.Click += new System.EventHandler(this.toolStripLabel1_Click);
            // 
            // toolStripScanButton
            // 
            this.toolStripScanButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripScanButton.Enabled = false;
            this.toolStripScanButton.Image = ((System.Drawing.Image)(resources.GetObject("toolStripScanButton.Image")));
            this.toolStripScanButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripScanButton.Name = "toolStripScanButton";
            this.toolStripScanButton.Size = new System.Drawing.Size(23, 22);
            this.toolStripScanButton.Text = "One";
            this.toolStripScanButton.Visible = false;
            this.toolStripScanButton.Click += new System.EventHandler(this.toolStripScanButton_Click);
            // 
            // toolStripScanAllButton
            // 
            this.toolStripScanAllButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripScanAllButton.Enabled = false;
            this.toolStripScanAllButton.Image = ((System.Drawing.Image)(resources.GetObject("toolStripScanAllButton.Image")));
            this.toolStripScanAllButton.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripScanAllButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripScanAllButton.Name = "toolStripScanAllButton";
            this.toolStripScanAllButton.Size = new System.Drawing.Size(32, 22);
            this.toolStripScanAllButton.Text = "��������� ��";
            this.toolStripScanAllButton.Click += new System.EventHandler(this.toolStripScanAllButton_Click);
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton1.Text = "�������� �� ������";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(73, 22);
            this.toolStripLabel2.Text = "�����������";
            // 
            // stripButtonEdit
            // 
            this.stripButtonEdit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.stripButtonEdit.Image = ((System.Drawing.Image)(resources.GetObject("stripButtonEdit.Image")));
            this.stripButtonEdit.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.stripButtonEdit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.stripButtonEdit.Name = "stripButtonEdit";
            this.stripButtonEdit.Size = new System.Drawing.Size(23, 22);
            this.stripButtonEdit.Text = "���������� �����";
            this.stripButtonEdit.Click += new System.EventHandler(this.stripButtonEdit_Click);
            // 
            // stripButtonDrop
            // 
            this.stripButtonDrop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.stripButtonDrop.Image = ((System.Drawing.Image)(resources.GetObject("stripButtonDrop.Image")));
            this.stripButtonDrop.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.stripButtonDrop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.stripButtonDrop.Name = "stripButtonDrop";
            this.stripButtonDrop.Size = new System.Drawing.Size(23, 22);
            this.stripButtonDrop.Text = "�������� ����� ";
            this.stripButtonDrop.Click += new System.EventHandler(this.stripButtonDrop_Click);
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(67, 22);
            this.toolStripLabel3.Text = "����������";
            this.toolStripLabel3.Visible = false;
            // 
            // toolStripProgressBar1
            // 
            this.toolStripProgressBar1.Name = "toolStripProgressBar1";
            this.toolStripProgressBar1.Size = new System.Drawing.Size(150, 22);
            this.toolStripProgressBar1.Visible = false;
            // 
            // ViewReadItems
            // 
            this.ViewReadItems.Dock = System.Windows.Forms.DockStyle.Top;
            this.ViewReadItems.FullRowSelect = true;
            this.ViewReadItems.GridLines = true;
            this.ViewReadItems.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.ViewReadItems.Location = new System.Drawing.Point(0, 25);
            this.ViewReadItems.Name = "ViewReadItems";
            this.ViewReadItems.Size = new System.Drawing.Size(1033, 254);
            this.ViewReadItems.StateImageList = this.imageList1;
            this.ViewReadItems.TabIndex = 1;
            this.ViewReadItems.UseCompatibleStateImageBehavior = false;
            this.ViewReadItems.View = System.Windows.Forms.View.Details;
            this.ViewReadItems.SelectedIndexChanged += new System.EventHandler(this.ViewReadItems_SelectedIndexChanged);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "MISC02.ICO");
            this.imageList1.Images.SetKeyName(1, "MISC03.ICO");
            // 
            // splitter1
            // 
            this.splitter1.Dock = System.Windows.Forms.DockStyle.Top;
            this.splitter1.Location = new System.Drawing.Point(0, 279);
            this.splitter1.Name = "splitter1";
            this.splitter1.Size = new System.Drawing.Size(1033, 3);
            this.splitter1.TabIndex = 2;
            this.splitter1.TabStop = false;
            // 
            // listViewRecognize
            // 
            this.listViewRecognize.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewRecognize.FullRowSelect = true;
            this.listViewRecognize.GridLines = true;
            this.listViewRecognize.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.listViewRecognize.Location = new System.Drawing.Point(0, 282);
            this.listViewRecognize.Name = "listViewRecognize";
            this.listViewRecognize.Size = new System.Drawing.Size(1033, 287);
            this.listViewRecognize.TabIndex = 3;
            this.listViewRecognize.UseCompatibleStateImageBehavior = false;
            this.listViewRecognize.View = System.Windows.Forms.View.Details;
            this.listViewRecognize.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.listViewRecognize_MouseDoubleClick);
            // 
            // recognizeWorker
            // 
            this.recognizeWorker.WorkerReportsProgress = true;
            this.recognizeWorker.WorkerSupportsCancellation = true;
            this.recognizeWorker.DoWork += new System.ComponentModel.DoWorkEventHandler(this.recognizeWorker_DoWork);
            this.recognizeWorker.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.recognizeWorker_RunWorkerCompleted);
            this.recognizeWorker.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.recognizeWorker_ProgressChanged);
            // 
            // ImportForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1033, 569);
            this.Controls.Add(this.listViewRecognize);
            this.Controls.Add(this.splitter1);
            this.Controls.Add(this.ViewReadItems);
            this.Controls.Add(this.toolStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ImportForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "������ ������� �� ������������ �����";
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripOpenButton;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton toolStripScanButton;
        private System.Windows.Forms.ToolStripButton toolStripScanAllButton;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripButton stripButtonEdit;
        private System.Windows.Forms.ToolStripButton stripButtonDrop;
        private System.Windows.Forms.ToolStripLabel toolStripLabel3;
        private System.Windows.Forms.ToolStripProgressBar toolStripProgressBar1;
        private System.Windows.Forms.ListView ViewReadItems;
        private System.Windows.Forms.Splitter splitter1;
        private System.Windows.Forms.ListView listViewRecognize;
        private System.Windows.Forms.ToolStripButton toolStripSaveButton;
        private System.Windows.Forms.ImageList imageList1;
        private System.ComponentModel.BackgroundWorker recognizeWorker;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
    }
}