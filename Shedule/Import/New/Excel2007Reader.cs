using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using ICSharpCode.SharpZipLib.Zip;
using System.Xml;
using System.Collections;

namespace Shturman.Shedule.Import.New
{
    class Excel2007Reader: IExcelReader, IDisposable
    {
        private string _fileName = "";
        private string _tempDirectory = "";

        private List<string> _sheets = new List<string>();
        private List<string> _strings = new List<string>();

        public Excel2007Reader()
        {
        }

        public Excel2007Reader(string fileName):this()
        {
            _fileName = fileName;

            /* Create temp directory*/
            _tempDirectory = Path.Combine(Path.GetTempPath(), Path.GetFileNameWithoutExtension(fileName));
            if (Directory.Exists(_tempDirectory))
                this.DeleteTempDirectory();
            Directory.CreateDirectory(_tempDirectory);
            
            /*UnZip excel 2007 format file to temp directory*/
            this.UnzipFile(_fileName, _tempDirectory);

            /*Read excel sheets*/
            DirectoryInfo di = new DirectoryInfo(Path.Combine(_tempDirectory, "xl\\worksheets\\"));
            foreach (FileInfo currFile in di.GetFiles())
                _sheets.Add( Path.GetFileNameWithoutExtension(currFile.FullName));

            /*Read excel strings*/
            using (XmlTextReader reader = new XmlTextReader(Path.Combine(_tempDirectory, "xl\\sharedStrings.xml")))
            {
                for (reader.MoveToContent(); reader.Read(); )
                    if (reader.NodeType == XmlNodeType.Element && reader.Name == "t")
                        _strings.Add(reader.ReadElementString());
            }
        }

        private void DeleteTempDirectory()
        {
            DirectoryInfo di = new DirectoryInfo(_tempDirectory);

            foreach (FileInfo currFile in di.GetFiles())
                currFile.Delete();

            foreach (DirectoryInfo currDir in di.GetDirectories())
                currDir.Delete(true);

            di.Delete(true);
        }

        private void UnzipFile(string zipFileName, string targetDirectory)
        {
            (new FastZip()).ExtractZip(zipFileName, targetDirectory, null);
        }

        #region IExcelReader Members

        public event ColumnReadCompleate OnColumnRead;
        public event RowReadCompleate OnRowRead;
        public event SheetReadEvent OnSheetReadBegin;
        public event SheetReadEvent OnSheetReadEnd;
        public event DocumentEvent OnDocumentReadStart;
        public event DocumentEvent OnDocumentReadEnd;

        void IExcelReader.ParseDocument()
        {
            if (!File.Exists(_fileName))
                throw new Exception("File not exists.");

            /* On start read document */
            if (OnDocumentReadStart != null)
                foreach (DocumentEvent de in OnDocumentReadStart.GetInvocationList())
                    de.Invoke();

            foreach (string currentSheet in _sheets)
            {
                /* On start read sheet */
                if (this.OnSheetReadBegin != null)
                    foreach (SheetReadEvent sre in OnSheetReadBegin.GetInvocationList())
                        sre.Invoke(currentSheet);

                XmlTextReader reader = new XmlTextReader(Path.Combine(_tempDirectory, "xl\\worksheets\\" + currentSheet + ".xml"));
                try
                {

                    string type, cellString;
                    int val, rowIndex = 0, columnIndex = 0;
                    IList cellValueList = new ArrayList();

                    while (!reader.EOF)
                    {
                        if (reader.NodeType == XmlNodeType.Element)
                            switch (reader.Name)
                            {
                                case "row":
                                    rowIndex++;
                                    columnIndex = 0;
                                    cellValueList.Clear();
                                    break;

                                case "c":
                                    type = reader.GetAttribute("t");
                                    cellString = string.Empty;

                                    if (!reader.IsEmptyElement)
                                    {
                                        XmlReader subreader = reader.ReadSubtree();
                                        while (!subreader.EOF)
                                        {
                                            if (subreader.NodeType == XmlNodeType.Element)
                                            {
                                                if (subreader.Name == "v")
                                                {
                                                    cellString = subreader.ReadInnerXml();
                                                }
                                            }
                                            subreader.Read();
                                        }
                                    }

                                    if (cellString != string.Empty)
                                    {
                                        if (type == "s")
                                        {
                                            val = int.Parse(cellString);
                                            cellString = _strings[val];
                                        }
                                    }
                                    
                                    cellValueList.Add(cellString);
                                    columnIndex++;
                                    
                                    if (this.OnColumnRead != null)
                                        foreach (ColumnReadCompleate crc in OnColumnRead.GetInvocationList())
                                            crc.Invoke(currentSheet, rowIndex, columnIndex, cellString);

                                    break;
                            }

                        if (reader.NodeType == XmlNodeType.EndElement)
                            switch (reader.Name)
                            {
                                case "row":
                                    if (_sheets.Contains(currentSheet))
                                    {
                                        string[] cellValues = new string[cellValueList.Count];
                                        cellValueList.CopyTo(cellValues, 0);

                                        if (OnRowRead != null)
                                            foreach (RowReadCompleate rrc in OnRowRead.GetInvocationList())
                                                rrc.Invoke(currentSheet, rowIndex, cellValues);
                                    }
                                    break;
                            }
                        reader.Read();
                    }
                }
                finally
                {
                    reader.Close();
                }

                /* On end read sheet */
                if (this.OnSheetReadEnd != null)
                    foreach (SheetReadEvent sre in OnSheetReadEnd.GetInvocationList())
                        sre.Invoke(currentSheet);
            }
            /* On end read document */
            if (OnDocumentReadEnd != null)
                foreach (DocumentEvent de in OnDocumentReadEnd.GetInvocationList())
                    de.Invoke();
        }

        string IExcelReader.FileName
        {
            get { return _fileName; }
            set { _fileName = value; }
        }

        List<string> IExcelReader.Sheets
        {
            get { return _sheets; }
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            this.DeleteTempDirectory();
        }

        #endregion
    }
}
