using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using Shturman.Nestor.DataEditors;
using Shturman.Nestor.Interfaces;
using Shturman.MultyLanguage;
using SupplementXLS2XML;
using Shturman.Shedule.Objects;
using Shturman.Shedule.Server.Interface;
using Shturman.Nestor.DataBrowser;

namespace Shturman.Shedule.Import.New
{
    public partial class ImportForm : Form
    {
        public ImportForm(IObjectStorage storage, IShedule shedule)
        {
            InitializeComponent();
            
            _storage = storage;
            _shedule = shedule;

            importSupplementWorker = new BackgroundWorker();
            importSupplementWorker.DoWork += new DoWorkEventHandler(CreateImportSupplementFromXLS_DoWork);
            importSupplementWorker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(CreateImportSupplementFromXLS_Compleate);
            importSupplementWorker.RunWorkerAsync(_storage);
            
            _recognizedRecords = new ArrayList();

            ViewReadItems.Columns.Clear();
            ViewReadItems.Columns.Add(Vocabruary.Translate("StudyType.Department.Caption"), 100);
            ViewReadItems.Columns.Add(Vocabruary.Translate("StudyType.Subject.Caption"), 250);
            ViewReadItems.Columns.Add(Vocabruary.Translate("StudyType.StudyType.Caption"), 75);
            ViewReadItems.Columns.Add(Vocabruary.Translate("StudyType.HourByWeek.Caption"), 75);
            ViewReadItems.Columns.Add(Vocabruary.Translate("StudyType.Groups.Caption"), 150);
            ViewReadItems.Columns.Add(Vocabruary.Translate("StudyType.Tutors.Caption"), 250);
            ViewReadItems.Columns.Add(Vocabruary.Translate("StudyType.Note.Caption"), 100);

            listViewRecognize.Columns.Clear();
            listViewRecognize.Columns.Add(Vocabruary.Translate("StudyType.Department.Caption"), 100);
            listViewRecognize.Columns.Add(Vocabruary.Translate("StudyType.Subject.Caption"), 250);
            listViewRecognize.Columns.Add(Vocabruary.Translate("StudyType.StudyType.Caption"), 75);
            listViewRecognize.Columns.Add(Vocabruary.Translate("StudyType.HourByWeek.Caption"), 75);
            listViewRecognize.Columns.Add(Vocabruary.Translate("StudyType.Groups.Caption"), 150);
            listViewRecognize.Columns.Add(Vocabruary.Translate("StudyType.Tutors.Caption"), 250);
            listViewRecognize.Columns.Add(Vocabruary.Translate("StudyType.Note.Caption"), 100);

            Text = Vocabruary.Translate("ImportForm.Caption");
        }

        private void CreateImportSupplementFromXLS_DoWork(object sender, DoWorkEventArgs e)
        {
            isfxls = new ImportSupplementFromXLS(e.Argument as IObjectStorage);
        }

        private void CreateImportSupplementFromXLS_Compleate(object sender, RunWorkerCompletedEventArgs e)
        {
            toolStripScanButton.Enabled = toolStripScanAllButton.Enabled = true;
        }

        private OpenFileDialog opfdlg = new OpenFileDialog();

        // ����� ������� ������� �� ����������� � ����� ������� Excel �� Excel XML  
        // ��� �������� ����� Microsoft Office 2007
        private ImportSupplementSchema iss = new Shturman.Shedule.Import.New.ImportSupplementSchema();

        private ImportSupplementFromXLS isfxls;
        private IObjectStorage _storage;
        private IShedule _shedule;

        private BackgroundWorker importSupplementWorker = null;

        private IList _records;
        private IList _recognizedRecords;

        private void toolStripOpenButton_Click(object sender, EventArgs e)
        {
            toolStripSaveButton.Enabled = true;
            
            opfdlg.Filter = "Excel 2003 XML Table (*.xml)|*.xml|Excel 2007 Files (*.xlsx)|*.xlsx|Excel Files (*.xls)|*.xls";

            // ��������� ����������� � ��� ����������� ������ ������ ������� �� ��������� Nestor`�
            ObjectEditor oe = new ObjectEditor(null);
            oe.SelectedObject = iss;
            oe.CanChangeInputMode = false;
            oe.Text = Vocabruary.Translate("ImportSupplementSchema.Caption");

            if (opfdlg.ShowDialog(this) == DialogResult.OK && oe.ShowDialog(this) == DialogResult.OK)
            {
                importSupplementWorker = new BackgroundWorker();
                importSupplementWorker.DoWork += 
                    new DoWorkEventHandler(importSupplementWorker_DoWorkXML);
                importSupplementWorker.RunWorkerCompleted += 
                    new RunWorkerCompletedEventHandler(importSupplementWorker_RunWorkerCompleted);    

                if (opfdlg.FilterIndex == 3)
                {
                    importSupplementWorker.RunWorkerAsync(iss);
                }
                else
                    if (opfdlg.FilterIndex == 1 || opfdlg.FilterIndex == 2)
                    {
                        IExcelReader excelXmlReader = ExcelReaderFactory.CreateExcelReader(opfdlg.FileName);
                        List<string> sheetNames = excelXmlReader.Sheets;
                        IList _sheets = new ArrayList(sheetNames.ToArray());
                        SelectForm sheetsSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.ExcelSheets"), true, _sheets);
                        sheetsSelectForm.StartPosition = FormStartPosition.CenterScreen;

                        if (sheetsSelectForm.ShowDialog(this) == DialogResult.OK)
                        {
                            sheetNames.Clear();
                            foreach (string item in sheetsSelectForm.SelectedItems)
                            {
                                sheetNames.Add(item);
                            }

                            SupplementBuilder sb = new SupplementBuilder(iss, excelXmlReader);
                            importSupplementWorker.RunWorkerAsync(sb);
                        }
                    }
            }
        }

        private void importSupplementWorker_DoWorkXML(object sender, DoWorkEventArgs e)
        {
            if (e.Argument is SupplementBuilder)
            {
                SupplementBuilder _sb = e.Argument as SupplementBuilder;
                _sb.BuildSupplement();
                _records = _sb.Supplement;
            }
            else
            {
                SupplementXLS SupplementDocument = new SupplementXLS();
                SupplementDocument.ReadFromXls(opfdlg.FileName);
                _records = SupplementDocument.Items;
            }
        }

        private void importSupplementWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            FillSupplementView(_records);
        }
        
        private void recognizeWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker bw = (sender as BackgroundWorker);

            // Recognize simple selected object
            e.Result = new ArrayList();

            int index = iss.HoursColumn - iss.HoursColumnStart;
            IList SupplementXLSList = (e.Argument as IList);
            
            int supplementIndex = 0;

            foreach (SupplementXLSItem supplementItem in SupplementXLSList)
            {
                if (supplementItem.Hours[index] != 0)
                {
                    object obj = isfxls.Recognize(supplementItem, index+1);
                    (e.Result as IList).Add(obj);
                }
                else
                    (e.Result as IList).Add(null);

                bw.ReportProgress(++supplementIndex);
            }
        }

        private void recognizeWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            toolStripLabel3.Visible = toolStripProgressBar1.Visible = false;
            toolStripProgressBar1.Value = 0;

            int index = 0;
            IList LoadingList = e.Result as IList;
            foreach (StudyLoading item in LoadingList)
            {
                if (item != null)
                {
                    _recognizedRecords.Add(item);

                    ListViewItem lvi = new ListViewItem();
                    lvi.Text = item.Dept.MnemoCode;

                    ListViewItem.ListViewSubItem subjectSubItem = new ListViewItem.ListViewSubItem(lvi, item.SubjectName.Name);
                    lvi.SubItems.Add(subjectSubItem);

                    ListViewItem.ListViewSubItem subjectTypeSubItem = new ListViewItem.ListViewSubItem(lvi, item.StudyForm.MnemoCode);
                    lvi.SubItems.Add(subjectTypeSubItem);

                    ListViewItem.ListViewSubItem hourSubItem = new ListViewItem.ListViewSubItem(lvi, item.HourByWeek.ToString());
                    lvi.SubItems.Add(hourSubItem);

                    ListViewItem.ListViewSubItem groupsSubItem = new ListViewItem.ListViewSubItem(lvi, item.GroupsList);
                    lvi.SubItems.Add(groupsSubItem);

                    ListViewItem.ListViewSubItem tutorsSubItem = new ListViewItem.ListViewSubItem(lvi, "");
                    foreach (Tutor tutor in item.Tutors)
                    {
                        if (tutorsSubItem.Text == "")
                            tutorsSubItem.Text = tutor.TutorSurname;
                        else
                            tutorsSubItem.Text += ", " + tutor.TutorSurname;

                        if (tutor.TutorName != null)
                            tutorsSubItem.Text += " " + tutor.TutorName;
                        if (tutor.TutorPatronimic != null)
                            tutorsSubItem.Text += " " + tutor.TutorPatronimic;
                        if (tutor.TutorPost != null)
                            tutorsSubItem.Text += " (" + tutor.TutorPost.ToString() + ")";
                    }

                    lvi.SubItems.Add(tutorsSubItem);

                    ListViewItem.ListViewSubItem commentSubItem = new ListViewItem.ListViewSubItem(lvi, item.Note);
                    lvi.SubItems.Add(commentSubItem);

                    listViewRecognize.Items.Add(lvi);

                    ViewReadItems.Items[itemsToScan[index]].StateImageIndex = 1;
                    ViewReadItems.Items[itemsToScan[index]].Tag = lvi;
                    lvi.Tag = ViewReadItems.Items[itemsToScan[index]];
                }
                index++;
            }
        }
        
        private void recognizeWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            toolStripLabel3.Text = "���������� " + e.ProgressPercentage.ToString() + 
                " � " + toolStripProgressBar1.Maximum;
            toolStripProgressBar1.Value = e.ProgressPercentage;
        }
        
        ListViewItem prev = null;
        private void ViewReadItems_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (sender is ListView)
            {
                if (prev != null)
                    prev.BackColor = Color.White;

                ListView lv = sender as ListView;
                if (lv.SelectedItems.Count == 1)
                {
                    ListViewItem obj = ((lv.SelectedItems[0] as ListViewItem).Tag as ListViewItem);
                    if (obj != null)
                    {       
                        prev = obj; 
                        prev.BackColor = Color.Bisque;
                    }
                }
            }
        }

        #region Scan Buttons Click
        private List<int> itemsToScan = new List<int>();
        private void toolStripScanAllButton_Click(object sender, EventArgs e)
        {
            itemsToScan.Clear();

            IList selecdedItems = new ArrayList();

            for (int index = 0; index < ViewReadItems.Items.Count; index++ )
                if (ViewReadItems.Items[index].StateImageIndex == 0)
                {
                    itemsToScan.Add(index);
                    selecdedItems.Add(_records[index]);
                }
            toolStripLabel3.Text = "���������� 0" +
                " � " + selecdedItems.Count;

            toolStripLabel3.Visible = toolStripProgressBar1.Visible = true;
            toolStripProgressBar1.Maximum = selecdedItems.Count;
            recognizeWorker.RunWorkerAsync(selecdedItems);
        }

        private void toolStripScanButton_Click(object sender, EventArgs e)
        {
            itemsToScan.Clear();
            IList selecdedItems = new ArrayList();

            foreach (int index in ViewReadItems.SelectedIndices)
                if (ViewReadItems.Items[index].StateImageIndex == 0)
                {
                    itemsToScan.Add(index);
                    selecdedItems.Add(_records[index]);
                }
            toolStripLabel3.Text = "���������� 0" +
                " � " + selecdedItems.Count;

            toolStripLabel3.Visible = toolStripProgressBar1.Visible = true;
            toolStripProgressBar1.Maximum = selecdedItems.Count;

            recognizeWorker.RunWorkerAsync(selecdedItems);
        }
        #endregion Scan Buttons Click

        private void toolStripSaveButton_Click(object sender, EventArgs e)
        {
            toolStripSaveButton.Enabled = false;
            foreach (StudyLoading stdld in _recognizedRecords)
            {
                _shedule.AddStudyLeading(RemoteAdapter.Adapter.AdaptStudyLeading(stdld));
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            // ��������� ���������� �� ������������ ������
            for (int index=0; index<_records.Count; index++)
            {
                SupplementXLSItem item = _records[index] as SupplementXLSItem;
                if(item.Groups.Count>1)
                {
                    if (item.SubjectTypeCaption.Contains("���") || item.SubjectTypeCaption.Contains("��")
                        || item.SubjectTypeCaption.Contains("��"))
                    {
                        bool oneGroupOneTutor = item.Groups.Count == item.Tutors.Count;
                        for (int groupIndex=1; groupIndex<item.Groups.Count; groupIndex++)
                        {
                            SupplementXLSItem newItem = new SupplementXLSItem();
                            newItem.DepartmentCaption = item.DepartmentCaption;
                            newItem.SubjectCaption = item.SubjectCaption;
                            newItem.SubjectTypeCaption = item.SubjectTypeCaption;
                            for (int hour=0; hour < item.Hours.Length; hour++)
                                newItem.Hours[hour] = item.Hours[hour];

                            newItem.Groups.Add(item.Groups[groupIndex]);
                            if (oneGroupOneTutor)
                                newItem.Tutors.Add(item.Tutors[groupIndex]);
                            else
                                newItem.Tutors.AddRange(item.Tutors.ToArray());
 
                            newItem.Comment = item.Comment;
                            _records.Insert(index, newItem);
                            index++;
                        }
                        item.Groups.RemoveRange(1, item.Groups.Count - 1);
                        if (oneGroupOneTutor)
                            item.Tutors.RemoveRange(1, item.Groups.Count - 1);
                    }
                }
            }

            FillSupplementView(_records);
        }

        private void FillSupplementView(IList supplementList)
        {
            ViewReadItems.Items.Clear();

            foreach (SupplementXLSItem item in supplementList)
            {
                ListViewItem lvi = new ListViewItem();
                lvi.Text = item.DepartmentCaption;
                lvi.ImageIndex = 0;
                lvi.StateImageIndex = 0;

                ListViewItem.ListViewSubItem subjectSubItem = new ListViewItem.ListViewSubItem(lvi, item.SubjectCaption);
                lvi.SubItems.Add(subjectSubItem);

                ListViewItem.ListViewSubItem subjectTypeSubItem = new ListViewItem.ListViewSubItem(lvi, item.SubjectTypeCaption);
                lvi.SubItems.Add(subjectTypeSubItem);

                ListViewItem.ListViewSubItem hourSubItem = new ListViewItem.ListViewSubItem(lvi, item.Hours[iss.HoursColumn - iss.HoursColumnStart].ToString());
                lvi.SubItems.Add(hourSubItem);

                ListViewItem.ListViewSubItem groupsSubItem = new ListViewItem.ListViewSubItem(lvi, "");
                foreach (GroupXLS group in item.Groups)
                    if (groupsSubItem.Text == "")
                        groupsSubItem.Text = group.GroupCaption;
                    else
                        groupsSubItem.Text += ", " + group.GroupCaption;

                lvi.SubItems.Add(groupsSubItem);

                ListViewItem.ListViewSubItem tutorsSubItem = new ListViewItem.ListViewSubItem(lvi, "");
                foreach (TutorXLS tutor in item.Tutors)
                    if (tutorsSubItem.Text == "")
                        tutorsSubItem.Text = tutor.TutorCaption + " (" + tutor.TutorDegree+")";
                    else
                        tutorsSubItem.Text += ", " + tutor.TutorCaption + " (" + tutor.TutorDegree + ")";

                lvi.SubItems.Add(tutorsSubItem);

                ListViewItem.ListViewSubItem commentSubItem = new ListViewItem.ListViewSubItem(lvi, item.Comment);
                lvi.SubItems.Add(commentSubItem);

                ViewReadItems.Items.Add( lvi );
            }
        }

        private void stripButtonDrop_Click(object sender, EventArgs e)
        {
            if (ViewReadItems.Focused)
            {
                foreach(int _sel_index in ViewReadItems.SelectedIndices)
                {
                    _records.RemoveAt(_sel_index);
                    ViewReadItems.Items.RemoveAt(_sel_index);
                }
            }
            else
                if (listViewRecognize.Focused)
                {
                    foreach (int _sel_index in listViewRecognize.SelectedIndices)
                    {
                        if (listViewRecognize.Items[_sel_index].Tag != null)
                        {
                            ListViewItem lvi = listViewRecognize.Items[_sel_index].Tag as ListViewItem;
                            lvi.StateImageIndex = 0;
                        }
                        _recognizedRecords.RemoveAt(_sel_index);
                        listViewRecognize.Items.RemoveAt(_sel_index);
                    }
                }
        }

        private void stripButtonEdit_Click(object sender, EventArgs e)
        {
            if (listViewRecognize.Focused)
            {
                foreach (int _sel_index in listViewRecognize.SelectedIndices)
                {
                    //_recognizedRecords.RemoveAt(_sel_index);
                    //listViewRecognize.Items.RemoveAt(_sel_index);
                    ObjectEditor oe = new ObjectEditor(null);
                    oe.CanChangeInputMode = false;
                    oe.Text = "����������� ������";
                    oe.SelectedObject = this._recognizedRecords[_sel_index];
                    if (oe.ShowDialog() == DialogResult.OK)
                    {
                        StudyLoading sl = this._recognizedRecords[_sel_index] as StudyLoading;
                        ListViewItem lvi = listViewRecognize.Items[_sel_index] as ListViewItem;

                        lvi.Text = sl.Dept.MnemoCode;
                        lvi.SubItems[1].Text = sl.SubjectName.Name;
                        lvi.SubItems[2].Text = sl.StudyForm.MnemoCode;
                        lvi.SubItems[3].Text = sl.HourByWeek.ToString();
                        lvi.SubItems[4].Text = sl.GroupsList;
                        lvi.SubItems[5].Text = "";
                        foreach (Tutor tutor in sl.Tutors)
                            if (lvi.SubItems[5].Text == "")
                                lvi.SubItems[5].Text = tutor.TutorSurname + " " + tutor.TutorName + " " + tutor.TutorPatronimic + " (" + tutor.TutorPost.ToString() + ")";
                            else
                                lvi.SubItems[5].Text += ", " + tutor.TutorSurname + " " + tutor.TutorName + " " + tutor.TutorPatronimic + " (" + tutor.TutorPost.ToString() + ")";

                        lvi.SubItems[6].Text = sl.Note;
                    }
                }
            }
        }

        private void listViewRecognize_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            stripButtonEdit_Click(sender, EventArgs.Empty);
        }

        private void toolStripLabel1_Click(object sender, EventArgs e)
        {

        }
    }
}