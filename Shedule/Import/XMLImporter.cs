using System;
using System.Windows.Forms;
using System.Xml;
using Shturman.Nestor.Interfaces;
using Shturman.Shedule.Server.Interface;
using System.IO;

namespace Shturman.Shedule.Import
{
	/// <summary>
	/// This class provides methods to import Supplement, TutorsRestricts
	///   FlatsRestricts, GroupsRestricts and so on.
	/// </summary>
	public class XMLImporter
	{
		private object _shedule;
		IObjectStorage _storage;

		public XMLImporter(object shedule, IObjectStorage storage)
		{
			_shedule = shedule;
			_storage = storage;
		}

		public void Import(XmlTextReader importReader)
		{			 			
			importReader.MoveToContent();
				
			if (importReader.Name == "SupplementDocument")
			{
				ImportSupplement(importReader);
			}
			else
				if(importReader.Name == "TutorsRestrictDocument")
					ImportTutorsRestricts(importReader);
			else				
				if(importReader.Name == "FlatsRestrictDocument")				
					ImportFlatsRestricts(importReader);
			else
                   if(importReader.Name == "GrousRestrictDocument")
					ImportGroupsRestricts(importReader);								
			else
					if (importReader.Name == "LeadingDocument")
					ImportLeading(importReader);
			else
                   	MessageBox.Show("��� ������ ����� �� �����������");
		}

		private void ImportSupplement(XmlTextReader xmlReader)
		{
			/* ��������� ����� �������������� ����  */
		}

		private void ImportLeading(XmlTextReader xmlReader)
		{
				if (_shedule != null)
			{
				if (_shedule is IShedule)
				{
					IShedule shedule = _shedule as IShedule;

					shedule.ImportLeading( xmlReader.ReadInnerXml() );
				}
			}
		}
		
		private void ImportTutorsRestricts(XmlTextReader xmlReader)
		{
			if (_shedule != null)
			{
				if (_shedule is IShedule)
				{
					IShedule shedule = _shedule as IShedule;

					shedule.ImportRestricts( xmlReader.ReadInnerXml() );
				}
			}
		}

		private void ImportFlatsRestricts(XmlTextReader xmlReader)
		{
			new NotImplementedException();
		}

		private void ImportGroupsRestricts(XmlTextReader xmlReader)
		{
			new NotImplementedException();
		}
	}
}
