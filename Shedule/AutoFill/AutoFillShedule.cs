using System;
using System.Collections;
using Shturman.Shedule.Objects;
using Shturman.Shedule.Server.Interface;

namespace Shturman.Shedule.AutoFill
{
	/// <summary>
	/// ����� ��� �������� ������������� ����������(��� ���������� ���������).
	///	����������� �������� ������������� ������� �� ����� ������� ��������� �����
	///	� �������������, ��� ���� ���� ������� �� ����� ���������� � ���� ����, ��
	///	����� ��������� ����� ��������������� �����(����), ����� ������������ 
	///	��������� �����, ����� ���� ����������� ���������� ����� �� ����, 
	///	������� ������������� ���� � �������
	/// </summary>
	public class AutoFillShedule
	{
		public class GraphNode
		{
			public GraphNode(StudyLoading stld)
			{
				this._stld = stld;
			}

			private int hour = 0;
			private int color = -1;
			private StudyLoading _stld = null;
			private IList _nodeLinks = new ArrayList();

			public  StudyLoading Stld
			{
				get { return this._stld; }
			}

			public int Hour
			{
				get { return hour; }
				set { hour = value; }
			}

			public int Color
			{
				get { return color; }
				set { color = value; }
			}

			public IList NodeLinks
			{
				get { return this._nodeLinks; }
			}
		}


		public class Graph
		{
			private IList _graphNodes = new ArrayList();

			public IList GraphNodes
			{
				get { return _graphNodes; }
			}

			public void SetDoubleLink(GraphNode node1, GraphNode node2)
			{
				SetLink(node1, node2);
				SetLink(node2, node1);
			}

			public void SetLink(GraphNode node1, GraphNode node2)
			{
				node1.NodeLinks.Add( node2 );
			}

			public int ColorGraph()
			{
				int maxColor = 0;
				(_graphNodes[0] as GraphNode).Color = 1;
				foreach(GraphNode gn in _graphNodes)
				{
					if (gn.Color == -1)
					{
						/* �������� ������ ������������ ������ ������� ������ */
						IList usedColors = new ArrayList();
						foreach(GraphNode ogn in gn.NodeLinks)
						{
							if (ogn.Color != -1)
								usedColors.Add(ogn.Color);
						}

						/* ��������� ������������ ����� */
						int minColor = -1;
						for (int index= 1; index<100; index ++)
							if (!usedColors.Contains(index))
							{
								minColor = index;
								maxColor = Math.Max(maxColor, minColor);
								bool isBreak = true;
								foreach(GraphNode ogn in _graphNodes)
									if (ogn.Color == minColor && gn.Stld.Uid == ogn.Stld.Uid)
									{
										isBreak = false;
										break;
									}
								if (isBreak)
									break;
							}
						/* ������������� ���� ������� */
						gn.Color = minColor;
					}
				}
				return maxColor;
			}
		}


		public AutoFillShedule()
		{
		}

		public void AutoArrange(IShedule shmatr, IList studyLoadingList, IList dayLst, 
			IList pairLst, IList weekLst, int studyForm)
		{
			Graph graph = CreateGraph( studyLoadingList, studyForm);
			int maxColor = graph.ColorGraph();
			
			
			#region ������� ������ ��������� �����������
			/*
			// ������ ������, �� ����� ����� ����� ���� � ������
            IList groups = new ArrayList(); 
			for(int index=0; index<dayLst.Count; index++)
				groups.Add( new ArrayList() );			

			// ���������� ������ �����
			foreach(GraphNode gn in graph.GraphNodes)
				if (gn.Color == 1)
					(groups[0] as ArrayList).Add(gn);

			for(int colorIndex=2; colorIndex <= maxColor; colorIndex++)
				foreach(GraphNode gn in graph.GraphNodes)
					if (gn.Color == colorIndex)
						for(int index=0; index<groups.Count; index++)
						{
							bool canAdd = true;
							// �������� ������� ������
							ArrayList group = groups[index] as ArrayList;
							for(int indexa=0; indexa<group.Count; indexa++)
							{
								GraphNode groupGraphNode = group[index] as GraphNode;
								if (groupGraphNode.Stld.Uid == gn.Stld.Uid)
								{
									canAdd = canAdd && false;
									break;
								}
							}
							
							if (index == 0)
							{	
								// �������� ��������� ������
								ArrayList group = groups[index+1] as ArrayList;
								for(int indexa=0; indexa<group.Count; indexa++)
								{
									GraphNode groupGraphNode = group[index] as GraphNode;
									if (groupGraphNode.Stld.Uid == gn.Stld.Uid)
									{
										canAdd = canAdd && false;
										break;
									}
								}	
							}
							else
								if (index == groups.Count-1)
							{
								// �������� ���������� ������
								ArrayList group = groups[index-1] as ArrayList;
								for(int indexa=0; indexa<group.Count; indexa++)
								{
									GraphNode groupGraphNode = group[index] as GraphNode;
									if (groupGraphNode.Stld.Uid == gn.Stld.Uid)
									{
										canAdd = canAdd && false;
										break;
									}
								}
							}
							else
							{
								// �������� ��������� ������
								ArrayList group = groups[index+1] as ArrayList;
								for(int indexa=0; indexa<group.Count; indexa++)
								{
									GraphNode groupGraphNode = group[index] as GraphNode;
									if (groupGraphNode.Stld.Uid == gn.Stld.Uid)
									{
										canAdd = canAdd && false;
										break;
									}
								}								
								// �������� ���������� ������
								ArrayList group = groups[index-1] as ArrayList;
								for(int indexa=0; indexa<group.Count; indexa++)
								{
									GraphNode groupGraphNode = group[index] as GraphNode;
									if (groupGraphNode.Stld.Uid == gn.Stld.Uid)
									{
										canAdd = canAdd && false;
										break;
									}
								}
							}

							if (canAdd)
								(groups[index] as ArrayList).Add(gn);
						}

			for(int index=0; index<groups.Count; index++)
			{
				ArrayList group = groups[index] as ArrayList;
				for(int indexa=0; indexa<group.Count; indexa++)
				{
						// ������ �������
						Lesson lsn = new Lesson(gn.Stld.SubjectName, 
										gn.Stld.StudyForm, 
										gn.Stld.Groups, 
										gn.Stld.Tutors, 
										new ArrayList()
								);
						lsn.LeadingUid = gn.Stld.Uid;
				}
			}
			*/
			#endregion
			
			bool isInsered = false;
			for(int colorIndex=1; colorIndex<=maxColor; colorIndex++)
			{
				foreach(GraphNode gn in graph.GraphNodes)
					if (gn.Color == colorIndex)
					{
						isInsered = false;
						/* ������ ������� */
						Lesson lsn = new Lesson(gn.Stld.SubjectName, 
										gn.Stld.StudyForm, 
										gn.Stld.Groups, 
										gn.Stld.Tutors, 
										new ArrayList()
								);
						lsn.LeadingUid = gn.Stld.Uid;

						/* ��� ����� ��� ������� */
						int pairMinIndex = 0;
						int pairMaxIndex = 5;
						
						if (lsn.SybjectType.MnemoCode == "�.")
						{
							pairMinIndex = 0;
							pairMaxIndex = 3;
						}
						else
							if(lsn.SybjectType.MnemoCode == "��.")
							{	
								pairMinIndex = 0;
								pairMaxIndex = 4;
							}

						int dayMinIndex = 0;
						int dayMaxIndex = 5;
						if (lsn.SybjectType.MnemoCode == "�.")
						{
							dayMinIndex = 0;
							dayMaxIndex = 4;
						}
						else
							if(lsn.SybjectType.MnemoCode == "��.")
							{	
								dayMinIndex = 0;
								dayMaxIndex = 5;
							}

						for(int pairIndex=pairMinIndex; pairIndex<pairMaxIndex; pairIndex++ )
						{
							for(int dayIndex=dayMinIndex; dayIndex< dayMaxIndex; dayIndex++ )
							{
								bool isFree = true;
								
								int dayId = int.Parse((dayLst[dayIndex] as StudyDay).MnemoCode);
								int pairId = int.Parse((pairLst[pairIndex] as StudyHour).MnemoCode);

								for(int weekIndex = 0; weekIndex < gn.Hour; weekIndex++ )
								{
									int weekId = int.Parse((weekLst[weekIndex] as StudyCycles).MnemoCode);	
									isFree = isFree && shmatr.CanInsertTo(weekId, dayId, pairId, RemoteAdapter.Adapter.AdaptLesson( lsn ));
								}
								if (isFree)
								{
									for(int weekIndex = 0; weekIndex < gn.Hour; weekIndex++ )
									{
										int weekId = int.Parse((weekLst[weekIndex] as StudyCycles).MnemoCode);
                                        isInsered = shmatr.Insert(weekId, dayId, pairId, RemoteAdapter.Adapter.AdaptLesson(lsn)) != -1;
									}									
									break;
								}										
							}
							if (isInsered)
								break;
						}
					}
			}
		}

		public Graph CreateGraph(IList studyLoadingList, int studyForm)
		{
			Graph graph = new Graph();
			/* �������� ������ ����� */
			if (studyForm == 1)
				foreach(StudyLoading sl in studyLoadingList)
					if (sl.StudyForm !=null && sl.StudyForm.MnemoCode == "�.")
					{
						int hourCount = sl.HourByWeek;
						while (hourCount > 0)
						{
							GraphNode graphNode = new GraphNode(sl);
							
							if ((hourCount - 2) > 0)
								graphNode.Hour = 2;
							else
								graphNode.Hour = hourCount;

							graph.GraphNodes.Add( graphNode );
							hourCount -= 2;
						}
					}
			if (studyForm == 2)
				foreach(StudyLoading sl in studyLoadingList)
					if (sl.StudyForm != null && sl.StudyForm.MnemoCode == "��.")
					{
						int hourCount = sl.HourByWeek;
						while (hourCount > 0)
						{
							GraphNode graphNode = new GraphNode(sl);
								
							if (hourCount - 2 > 0)
								graphNode.Hour = 2;
							else
								graphNode.Hour = hourCount;

							graph.GraphNodes.Add( graphNode );
							hourCount -= 2;
						}
					}

			/* ������������ ������ ����� ��������� �����*/
			foreach (GraphNode gn in graph.GraphNodes)
			{				
				foreach (GraphNode ogn in graph.GraphNodes)
				{
					if (!gn.Equals(ogn))
					{
						/* ����� �� ������� ���� */
						if (gn.Stld.Uid == ogn.Stld.Uid)
						{
							if (!gn.NodeLinks.Contains(ogn))
							{
								graph.SetLink(gn, ogn);
								continue;
							}
						}

						/* ����� �� �������������� */
						foreach(Tutor tr in gn.Stld.Tutors)
							if (ogn.Stld.Tutors.Contains(tr))
							{
								if (!gn.NodeLinks.Contains(ogn))
								{
									graph.SetLink(gn, ogn);
									continue;
								}
							}

						/* ����� �� ������� */                        
						foreach(Group gr in gn.Stld.Groups)
							if (ogn.Stld.Groups.Contains(gr))
							{
								if (!gn.NodeLinks.Contains(ogn))
								{
									graph.SetLink(gn, ogn);
									continue;
								}
							}
					}
				}
			}
			return graph;
		}
	}
}
