using System.Collections;
using System.Threading;
using Shturman.Nestor.Interfaces;
using Shturman.Shedule.Objects;
using Shturman.Shedule.Server.Interface;

namespace Shturman.Shedule.Export
{
	/// <summary>
	/// ����� ������� ��� �������� � Microsoft Excel.
	/// </summary>
	public class XLSExporter
	{	
		/// <summary>
		/// ����� �������� ���������� ������� �� �������
		/// </summary>
		internal class GroupsShedule2XLS
		{
			private IList _groups = null;
			IObjectStorage _storage = null;
			IShedule _shedule = null;
			IShedule2 _shedule2 = null;

			public GroupsShedule2XLS(IObjectStorage storage, IShedule shedule, IList groups)
			{
				_storage = storage;
				_shedule = shedule;
				_groups = groups;
			}

			public GroupsShedule2XLS(IObjectStorage storage, IShedule2 shedule, IList groups)
			{
				_storage = storage;
				_shedule2 = shedule;
				_groups = groups;
			}

			public void Export()
			{
				IList dayList = this._storage.ObjectList(typeof(StudyDay));
				IList pairList = this._storage.ObjectList(typeof(StudyHour));
				IList weekList = this._storage.ObjectList(typeof(StudyCycles));

				ExcelExporter shexex = new ExcelExporter(dayList, pairList, weekList);
				
				ProgressThread procThread = new ProgressThread();
				
				procThread.MaxValue = dayList.Count*pairList.Count*weekList.Count*_groups.Count;
				procThread.Show();
				shexex.Export("", _groups, this._shedule, new ExcelExporter.ExcelExporterCallBack(procThread.CallBack));
				procThread.Close();
			}

			public void Export2()
			{
				IList dayList = this._storage.ObjectList(typeof(StudyDay));
				IList pairList = this._storage.ObjectList(typeof(StudyHour));
				IList weekList = this._storage.ObjectList(typeof(StudyCycles));

				ExcelExporter shexex = new ExcelExporter(dayList, pairList, weekList);
				
				ProgressThread procThread = new ProgressThread();
								
				procThread.MaxValue = (this._shedule2.GetSheduleEndDate() -  this._shedule2.GetSheduleBeginDate()).Days*pairList.Count*weekList.Count*_groups.Count;
				procThread.Show();
				shexex.Export2("", _groups, this._shedule2, this._storage, new ExcelExporter.ExcelExporterCallBack(procThread.CallBack));
				procThread.Close();
			}

		}

		/// <summary>
		/// ����� �������� ���������� ������� �� ��������������
		/// </summary>
		internal class TutorShedule2XLS
		{
			private IList _tutors = null;
			IObjectStorage _storage = null;
			IShedule _shedule = null;
			IShedule2 _shedule2 = null;

			public TutorShedule2XLS(IObjectStorage storage, IShedule shedule, IList tutors)
			{
				_storage = storage;
				_shedule = shedule;
				_tutors = tutors;
			}

			public TutorShedule2XLS(IObjectStorage storage, IShedule2 shedule, IList tutors)
			{
				_storage = storage;
				_shedule2 = shedule;
				_tutors = tutors;
			}

			public void Export()
			{
				IList dayList = this._storage.ObjectList(typeof(StudyDay));
				IList pairList = this._storage.ObjectList(typeof(StudyHour));
				IList weekList = this._storage.ObjectList(typeof(StudyCycles));

				ExcelExporter shexex = new ExcelExporter(dayList, pairList, weekList);

				ProgressThread procThread = new ProgressThread();
				procThread.MaxValue = dayList.Count*pairList.Count*weekList.Count*_tutors.Count;
				procThread.Show();	
				
				shexex.ExportTutorShedule("", _tutors, this._shedule, new ExcelExporter.ExcelExporterCallBack(procThread.CallBack));

				procThread.Close();
			}	
		
			public void Export2()
			{
				IList dayList = this._storage.ObjectList(typeof(StudyDay));
				IList pairList = this._storage.ObjectList(typeof(StudyHour));
				IList weekList = this._storage.ObjectList(typeof(StudyCycles));

				ExcelExporter shexex = new ExcelExporter(dayList, pairList, weekList);

				ProgressThread procThread = new ProgressThread();				
				procThread.MaxValue = ((_shedule2.GetSheduleEndDate()-_shedule2.GetSheduleBeginDate()).Days)*pairList.Count*_tutors.Count;
				procThread.Show();	
				
				shexex.ExportTutorShedule2("", _tutors, this._shedule2, this._storage, new ExcelExporter.ExcelExporterCallBack(procThread.CallBack));

				procThread.Close();
			}	
		}

		/// <summary>
		/// ����� �������� ���������� ������� �� �������
		/// </summary>
		internal class DepartmentShedule2XLS
		{
			private Department _dept = null;
			private IObjectStorage _storage = null;
			private IShedule _shedule = null;
			private IShedule2 _shedule2 = null;

			public DepartmentShedule2XLS(IObjectStorage storage, IShedule shedule, Department dept)
			{
				_storage = storage;
				_shedule = shedule;
				_dept = dept;
			}
			
			public DepartmentShedule2XLS(IObjectStorage storage, IShedule2 shedule, Department dept)
			{
				_storage = storage;
				_shedule2 = shedule;
				_dept = dept;
			}

			public void Export()
			{
				IList dayList = this._storage.ObjectList(typeof(StudyDay));
				IList pairList = this._storage.ObjectList(typeof(StudyHour));
				IList weekList = this._storage.ObjectList(typeof(StudyCycles));

				ExcelExporter shexex = new ExcelExporter(dayList, pairList, weekList);

				ProgressThread procThread = new ProgressThread();
				procThread.MaxValue = dayList.Count*pairList.Count*weekList.Count*_dept.DepartmentTutors.Count;
				procThread.Show();	
				
				shexex.ExportDepartmentShedule("", _dept, this._shedule, new ExcelExporter.ExcelExporterCallBack(procThread.CallBack));

				procThread.Close();
			}	
		
			public void Export2()
			{
				IList dayList = this._storage.ObjectList(typeof(StudyDay));
				IList pairList = this._storage.ObjectList(typeof(StudyHour));
				IList weekList = this._storage.ObjectList(typeof(StudyCycles));
				
				ExcelExporter shexex = new ExcelExporter(dayList, pairList, weekList);

				ProgressThread procThread = new ProgressThread();
				procThread.MaxValue = (this._shedule2.GetSheduleEndDate() -  this._shedule2.GetSheduleBeginDate()).Days*pairList.Count*_dept.DepartmentTutors.Count;
				procThread.Show();				
				
				shexex.ExportDepartmentShedule2("", _dept, this._shedule2, _storage, new ExcelExporter.ExcelExporterCallBack(procThread.CallBack));

				procThread.Close();
			}			
		}

		/// <summary>
		/// ����� �������� ���������� ������� �� ���������
		/// </summary>
		internal class FlatShedule2XLS
		{
			private IList _flats = null;
			private IObjectStorage _storage = null;
			private IShedule _shedule = null;
			private IShedule2 _shedule2 = null;
			
			private bool _showSmall = false;

			public FlatShedule2XLS(IObjectStorage storage, IShedule shedule, IList flats, bool showSmall)
			{
				_storage = storage;
				_shedule = shedule;
				_flats = flats;
				_showSmall = showSmall;
			}

			public FlatShedule2XLS(IObjectStorage storage, IShedule2 shedule, IList flats, bool showSmall)
			{
				_storage = storage;
				_shedule2 = shedule;
				_flats = flats;
				_showSmall = showSmall;
			}

			public void Export()
			{
				IList dayList = this._storage.ObjectList(typeof(StudyDay));
				IList pairList = this._storage.ObjectList(typeof(StudyHour));
				IList weekList = this._storage.ObjectList(typeof(StudyCycles));

				ExcelExporter shexex = new ExcelExporter(dayList, pairList, weekList);

				ProgressThread procThread = new ProgressThread();
				procThread.MaxValue = dayList.Count*pairList.Count*weekList.Count*_flats.Count;
				procThread.Show();				

				if (_showSmall)
					shexex.ExportShortFlatShedule("", _flats, this._shedule, new ExcelExporter.ExcelExporterCallBack(procThread.CallBack));
				else
					shexex.ExportFlatShedule("", _flats, this._shedule, new ExcelExporter.ExcelExporterCallBack(procThread.CallBack));
				
				procThread.Close();
			}

			public void Export2()
			{
				IList dayList = this._storage.ObjectList(typeof(StudyDay));
				IList pairList = this._storage.ObjectList(typeof(StudyHour));
				IList weekList = this._storage.ObjectList(typeof(StudyCycles));

				ExcelExporter shexex = new ExcelExporter(dayList, pairList, weekList);

				ProgressThread procThread = new ProgressThread();
				procThread.MaxValue =  (this._shedule2.GetSheduleEndDate() -  this._shedule2.GetSheduleBeginDate()).Days*pairList.Count*_flats.Count;
				procThread.Show();				

				if (_showSmall)
					shexex.ExportShortFlatShedule2("", _flats, this._shedule2, this._storage, new ExcelExporter.ExcelExporterCallBack(procThread.CallBack));
				else
					shexex.ExportFlatShedule2("", _flats, this._shedule2, this._storage, new ExcelExporter.ExcelExporterCallBack(procThread.CallBack));
				
				procThread.Close();
			}
			
		}

		
		public void ExportGroupsShedule(IObjectStorage storage, IShedule shedule, IList groups)
		{
			GroupsShedule2XLS GroupsSheduleExporter = new GroupsShedule2XLS(storage, shedule, groups);

			Thread backgroundThread = new Thread(new ThreadStart(GroupsSheduleExporter.Export));			
			backgroundThread.Start();
		}

		public void ExportGroupsShedule2(IObjectStorage storage, IShedule2 shedule, IList groups)
		{
			GroupsShedule2XLS GroupsSheduleExporter = new GroupsShedule2XLS(storage, shedule, groups);

			Thread backgroundThread = new Thread(new ThreadStart(GroupsSheduleExporter.Export2));			
			backgroundThread.Start();
		}


		public void ExportTutorsShedule(IObjectStorage storage, IShedule shedule, IList tutors)
		{
			TutorShedule2XLS TutorsSheduleExporter = new TutorShedule2XLS(storage, shedule, tutors);

			Thread backgroundThread = new Thread(new ThreadStart(TutorsSheduleExporter.Export));			
			backgroundThread.Start();
		}

		public void ExportTutorsShedule2(IObjectStorage storage, IShedule2 shedule, IList tutors)
		{
			TutorShedule2XLS TutorsSheduleExporter = new TutorShedule2XLS(storage, shedule, tutors);

			Thread backgroundThread = new Thread(new ThreadStart(TutorsSheduleExporter.Export2));			
			backgroundThread.Start();
		}


		public void ExportDepartmentShedule(IObjectStorage storage, IShedule shedule, Department dept)
		{
			DepartmentShedule2XLS DepartmentSheduleExporter = new DepartmentShedule2XLS(storage, shedule, dept);

			Thread backgroundThread = new Thread(new ThreadStart(DepartmentSheduleExporter.Export));
			backgroundThread.Start();
		}

		public void ExportDepartmentShedule2(IObjectStorage storage, IShedule2 shedule, Department dept)
		{
			DepartmentShedule2XLS DepartmentSheduleExporter = new DepartmentShedule2XLS(storage, shedule, dept);

			Thread backgroundThread = new Thread(new ThreadStart(DepartmentSheduleExporter.Export2));
			backgroundThread.Start();
		}
		
		
		public void ExportFlatsShedule(IObjectStorage storage, IShedule shedule, IList flats, bool showSmall)
		{
			FlatShedule2XLS FlatSheduleExporter = new FlatShedule2XLS(storage, shedule, flats, showSmall);

			Thread backgroundThread = new Thread(new ThreadStart(FlatSheduleExporter.Export));
			backgroundThread.Start();
		}

		public void ExportFlatsShedule2(IObjectStorage storage, IShedule2 shedule, IList flats, bool showSmall)
		{
			FlatShedule2XLS FlatSheduleExporter = new FlatShedule2XLS(storage, shedule, flats, showSmall);

			Thread backgroundThread = new Thread(new ThreadStart(FlatSheduleExporter.Export2));
			backgroundThread.Start();
		}
	}
}
