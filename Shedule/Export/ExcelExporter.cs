﻿using System;
using System.Collections;
using System.Runtime.InteropServices;
using Shturman.Nestor.DataLists;
using Shturman.Nestor.Interfaces;
using Shturman.Shedule.Objects;
using Shturman.Shedule.Server.Interface;
using Shturman.Shedule.Server.Remoting;
using Microsoft.Office.Interop.Excel;
using System.Threading;
using System.Globalization;
//using SheduleServer.Remoting;

namespace Shturman.Shedule.Export
{
    /// <summary>
    /// Класс экспорта данных расписания занятий в MS Excel 
    /// </summary>
    public class ExcelExporter
    {
        public delegate void ExcelExporterCallBack(string text, int position);

        private IList _weekList;
        private IList _dayList;
        private IList _pairList;

        private static Application _excelApp = null;
        private _Workbook _workbook = null;
        private _Worksheet _excelSheet = null;

        public ExcelExporter(IList dayList, IList pairList, IList weekList)
        {
            _weekList = weekList;
            _dayList = dayList;
            _pairList = pairList;
        }

        /// <summary>
        /// Начало процедуры экспорта данных в MS Excel
        /// </summary>
        public void BeginExport()
        {
            StartExcel();
        }

        public void CreateTitles(_Worksheet excelSheet, int beginRowIndex)
        {
            if (excelSheet == null) return;

            #region Вывод заголовков

            {
                Range dayCaptionRange = excelSheet.Cells[beginRowIndex, 1] as Range;
                dayCaptionRange.Value2 = "ДНІ";
                dayCaptionRange.Orientation = XlOrientation.xlUpward;
                dayCaptionRange.Font.Size = "14";
                dayCaptionRange.BorderAround(1, XlBorderWeight.xlMedium, XlColorIndex.xlColorIndexAutomatic, Type.Missing);

                Range hourCaptionRange = excelSheet.Cells[beginRowIndex, 2] as Range;
                hourCaptionRange.Value2 = "Години";
                hourCaptionRange.Font.Size = "14";
                hourCaptionRange.BorderAround(1, XlBorderWeight.xlMedium, XlColorIndex.xlColorIndexAutomatic, Type.Missing);
            }

            for (int index = 0; index < _dayList.Count; index++)
            {
                int dayRowBegin = index * _weekList.Count * _pairList.Count + beginRowIndex + 1;
                int dayRowEnd = (index + 1) * _weekList.Count * _pairList.Count + beginRowIndex;
                Range dayRange = excelSheet.get_Range(excelSheet.Cells[dayRowBegin, 1],
                excelSheet.Cells[dayRowEnd, 1]) as Range;

                dayRange.VerticalAlignment = XlVAlign.xlVAlignCenter;

                dayRange.Font.Bold = true;
                dayRange.Font.Size = "16";

                dayRange.HorizontalAlignment = XlHAlign.xlHAlignCenter;

                dayRange.Orientation = XlOrientation.xlUpward;

                dayRange.MergeCells = true;

                dayRange.Value2 = (_dayList[index] as StudyDay).Name.ToUpper();

                dayRange.Borders.get_Item(XlBordersIndex.xlEdgeLeft).Weight = XlBorderWeight.xlMedium;
                if (index == 0)
                    dayRange.Borders.get_Item(XlBordersIndex.xlEdgeTop).Weight = XlBorderWeight.xlMedium;
                dayRange.Borders.get_Item(XlBordersIndex.xlEdgeRight).Weight = XlBorderWeight.xlMedium;
                dayRange.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;

                //вывод заголовков номеров занятий
                for (int pairIndex = 0; pairIndex < _pairList.Count; pairIndex++)
                {
                    Range pairRange = excelSheet.get_Range(excelSheet.Cells[dayRowBegin + pairIndex * _weekList.Count, 2],
                    excelSheet.Cells[dayRowBegin + pairIndex * _weekList.Count + _weekList.Count - 1, 2]) as Range;

                    pairRange.Font.Bold = true;
                    pairRange.Font.Size = "14";
                    pairRange.RowHeight = "25";

                    pairRange.VerticalAlignment = XlVAlign.xlVAlignCenter;

                    pairRange.HorizontalAlignment = XlHAlign.xlHAlignCenter;

                    pairRange.MergeCells = true;


                    pairRange.Value2 = (_pairList[pairIndex] as StudyHour).FromTime.Hours.ToString("00") +
                    (_pairList[pairIndex] as StudyHour).FromTime.Minutes.ToString("00") + "-" +
                    (_pairList[pairIndex] as StudyHour).ToTime.Hours.ToString("00") +
                    (_pairList[pairIndex] as StudyHour).ToTime.Minutes.ToString("00")
                    ;

                    pairRange.get_Characters(3, 2).Font.Superscript = true;
                    pairRange.get_Characters(8, 2).Font.Superscript = true;

                    pairRange.Borders.get_Item(XlBordersIndex.xlEdgeLeft).Weight = XlBorderWeight.xlMedium;
                    if (pairIndex != 0)
                        pairRange.Borders.get_Item(XlBordersIndex.xlEdgeTop).Weight = XlBorderWeight.xlMedium;
                    pairRange.Borders.get_Item(XlBordersIndex.xlEdgeRight).Weight = XlBorderWeight.xlMedium;

                    if (pairIndex == (_pairList.Count - 1))
                        pairRange.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;
                    else
                        pairRange.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlMedium;
                }
            }
            // Устанавливаю ширину столбцов
            {
                Range range;
                // ширина столбца для дня
                range = excelSheet.Cells[beginRowIndex, 1] as Range;
                range.ColumnWidth = "4";
                range.Font.Size = "16";
                // ширина столбца часов
                range = excelSheet.Cells[beginRowIndex, 2] as Range;
                range.ColumnWidth = "12";
                range.Font.Size = "14";
            }

            #endregion
        }

        public void CreateTitles2(_Worksheet excelSheet, IShedule2 shedule, int beginRowIndex)
        {
            if (excelSheet == null) return;

            #region Вывод заголовков
            Range dateCaptionRange = excelSheet.Cells[beginRowIndex, 1] as Range;
            dateCaptionRange.Value2 = "Дата";
            dateCaptionRange.Orientation = XlOrientation.xlUpward;
            dateCaptionRange.Font.Size = "14";
            dateCaptionRange.BorderAround(1, XlBorderWeight.xlMedium, XlColorIndex.xlColorIndexAutomatic, Type.Missing);
            dateCaptionRange.ColumnWidth = "4";
            dateCaptionRange.Font.Size = "16";

            Range dayCaptionRange = excelSheet.Cells[beginRowIndex, 2] as Range;
            dayCaptionRange.Value2 = "День";
            dayCaptionRange.Orientation = XlOrientation.xlUpward;
            dayCaptionRange.Font.Size = "14";
            dayCaptionRange.BorderAround(1, XlBorderWeight.xlMedium, XlColorIndex.xlColorIndexAutomatic, Type.Missing);
            dayCaptionRange.ColumnWidth = "4";
            dayCaptionRange.Font.Size = "16";

            Range hourCaptionRange = excelSheet.Cells[beginRowIndex, 3] as Range;
            hourCaptionRange.Value2 = "Година";
            hourCaptionRange.Font.Size = "14";
            hourCaptionRange.ColumnWidth = "11";
            hourCaptionRange.BorderAround(1, XlBorderWeight.xlMedium, XlColorIndex.xlColorIndexAutomatic, Type.Missing);
            hourCaptionRange.Font.Size = "16";

            DateTime beginDate = shedule.GetSheduleBeginDate();
            DateTime endDate = shedule.GetSheduleEndDate();

            int index = 0;
            while (beginDate < endDate)
            {
                int dayRowBegin = index * _pairList.Count + beginRowIndex + 1;
                int dayRowEnd = (index + 1) * _pairList.Count + beginRowIndex;
                Range dayRange = excelSheet.get_Range(excelSheet.Cells[dayRowBegin, 2],
                excelSheet.Cells[dayRowEnd, 2]) as Range;

                dayRange.VerticalAlignment = XlVAlign.xlVAlignCenter;

                dayRange.Font.Bold = true;
                dayRange.Font.Size = "16";

                dayRange.HorizontalAlignment = XlHAlign.xlHAlignCenter;

                dayRange.Orientation = XlOrientation.xlUpward;

                dayRange.MergeCells = true;

                if (beginDate.DayOfWeek == DayOfWeek.Sunday)
                    dayRange.Value2 = (_dayList[6] as StudyDay).Name.ToUpper();
                else
                    dayRange.Value2 = (_dayList[(int)beginDate.DayOfWeek - 1] as StudyDay).Name.ToUpper();

                dayRange.Borders.get_Item(XlBordersIndex.xlEdgeLeft).Weight = XlBorderWeight.xlMedium;
                if (index == 0)
                    dayRange.Borders.get_Item(XlBordersIndex.xlEdgeTop).Weight = XlBorderWeight.xlMedium;
                dayRange.Borders.get_Item(XlBordersIndex.xlEdgeRight).Weight = XlBorderWeight.xlMedium;
                dayRange.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;

                Range dateRange = excelSheet.get_Range(excelSheet.Cells[dayRowBegin, 1],
                excelSheet.Cells[dayRowEnd, 1]) as Range;

                dateRange.VerticalAlignment = XlVAlign.xlVAlignCenter;

                dateRange.Font.Bold = true;
                dateRange.Font.Size = "16";

                dateRange.HorizontalAlignment = XlHAlign.xlHAlignCenter;

                dateRange.Orientation = XlOrientation.xlUpward;

                dateRange.MergeCells = true;

                dateRange.Value2 = beginDate.ToShortDateString().ToUpper();
                dateRange.Borders.get_Item(XlBordersIndex.xlEdgeLeft).Weight = XlBorderWeight.xlMedium;
                if (index == 0)
                    dateRange.Borders.get_Item(XlBordersIndex.xlEdgeTop).Weight = XlBorderWeight.xlMedium;
                dateRange.Borders.get_Item(XlBordersIndex.xlEdgeRight).Weight = XlBorderWeight.xlMedium;
                dateRange.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;

                //вывод заголовков номеров занятий
                for (int pairIndex = 0; pairIndex < _pairList.Count; pairIndex++)
                {
                    Range pairRange = excelSheet.get_Range(excelSheet.Cells[dayRowBegin + pairIndex, 3],
                    excelSheet.Cells[dayRowBegin + pairIndex, 3]) as Range;

                    pairRange.Font.Bold = true;
                    pairRange.Font.Size = "14";

                    pairRange.VerticalAlignment = XlVAlign.xlVAlignCenter;

                    pairRange.HorizontalAlignment = XlHAlign.xlHAlignCenter;

                    pairRange.MergeCells = true;


                    pairRange.Value2 = (_pairList[pairIndex] as StudyHour).FromTime.Hours.ToString("00") +
                    (_pairList[pairIndex] as StudyHour).FromTime.Minutes.ToString("00") + "-" +
                    (_pairList[pairIndex] as StudyHour).ToTime.Hours.ToString("00") +
                    (_pairList[pairIndex] as StudyHour).ToTime.Minutes.ToString("00")
                    ;

                    pairRange.get_Characters(3, 2).Font.Superscript = true;
                    pairRange.get_Characters(8, 2).Font.Superscript = true;

                    pairRange.Borders.get_Item(XlBordersIndex.xlEdgeLeft).Weight = XlBorderWeight.xlMedium;
                    if (pairIndex != 0)
                        pairRange.Borders.get_Item(XlBordersIndex.xlEdgeTop).Weight = XlBorderWeight.xlMedium;
                    pairRange.Borders.get_Item(XlBordersIndex.xlEdgeRight).Weight = XlBorderWeight.xlMedium;

                    if (pairIndex == (_pairList.Count - 1))
                        pairRange.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;
                    else
                        pairRange.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlMedium;
                }
                beginDate = beginDate.AddDays(1);
                index++;
            }
            #endregion
        }

        private bool ListCompare(IList firstList, IList secondList)
        {
            if (firstList != null && secondList != null)
            {
                if (firstList.Equals(secondList))
                    return true;
                else
                    if (firstList.Count != secondList.Count)
                        return false;
                    else
                    {
                        bool listIsEquals = true;
                        foreach (object obj in secondList)
                        {
                            listIsEquals = listIsEquals && firstList.Contains(obj);
                        }
                        return listIsEquals;
                    }
            }
            else
                return false;
        }

        /// <summary>
        /// Экспорт данных в MS Excel
        /// </summary>

        public void Export(string filename, IList groupList, IShedule shedule, ExcelExporterCallBack callback)
        {
            if (callback != null)
                callback("Запуск Excel", 0);

            BeginExport();

            if (callback != null)
                callback("Підготовка сторінки", 0);

            CreateTitles(this._excelSheet, 1);

            #region Выод расписания по группам

            int ColumnStart = 4;
            int CheckedPos = 1;
            foreach (Group gr in groupList)
                if (gr != null)
                {
                    //Вывод номера группы	
                    Range range;
                    range = _excelSheet.Cells[1, ColumnStart - 1] as Range;
                    range.ColumnWidth = "9";

                    range = _excelSheet.Cells[1, ColumnStart] as Range;
                    range.ColumnWidth = "17";

                    range = _excelSheet.get_Range(_excelSheet.Cells[1, ColumnStart - 1], _excelSheet.Cells[1, ColumnStart]);
                    range.MergeCells = true;

                    range.Value2 = gr.MnemoCode;
                    range.Font.Size = "28";

                    range.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    range.BorderAround(1, XlBorderWeight.xlMedium, XlColorIndex.xlColorIndexAutomatic, Type.Missing);
                    range.Font.Bold = true;

                    //Вывод расписания	
                    for (int rowindex = 2; rowindex < _dayList.Count * _pairList.Count * _weekList.Count + 2; rowindex++)
                    {
                        if (callback != null)
                            callback(gr.ToString(), CheckedPos);

                        CheckedPos++;

                        Range lessonCell = _excelSheet.Cells[rowindex, ColumnStart] as Range;
                        Range lessonFlatCell = _excelSheet.Cells[rowindex, ColumnStart - 1] as Range;

                        lessonCell.Font.Size = "12";
                        lessonCell.WrapText = true;
                        lessonCell.VerticalAlignment = XlVAlign.xlVAlignCenter;
                        lessonCell.HorizontalAlignment = XlVAlign.xlVAlignCenter;

                        lessonFlatCell.Font.Size = "12";
                        lessonFlatCell.Font.Bold = true;
                        lessonFlatCell.VerticalAlignment = XlVAlign.xlVAlignCenter;
                        lessonFlatCell.HorizontalAlignment = XlVAlign.xlVAlignCenter;

                        if (shedule.GroupHasLesson(gr.ObjectHardKey, rowindex - 2))
                        {
                            if ((rowindex - 2) % 2 != 0)
                                if ((shedule.GroupHasLesson(gr.ObjectHardKey, rowindex - 2 - 1)))
                                {
                                    RemoteLesson currLesson = shedule.GetGroupLesson(gr.ObjectHardKey, rowindex - 2);
                                    RemoteLesson predLesson = shedule.GetGroupLesson(gr.ObjectHardKey, rowindex - 2 - 1);
                                    if (currLesson.leading_uid == predLesson.leading_uid)
                                        if (this.ListCompare(currLesson.flatList, predLesson.flatList))
                                        {
                                            Range Cell;
                                            Cell = _excelSheet.get_Range(_excelSheet.Cells[rowindex - 1, ColumnStart], _excelSheet.Cells[rowindex, ColumnStart]);
                                            Cell.MergeCells = true;
                                            Cell.Font.Size = "12";

                                            if ((rowindex - 3) % 12 != 0)
                                                Cell.Borders.get_Item(XlBordersIndex.xlEdgeTop).Weight = XlBorderWeight.xlMedium;

                                            if ((rowindex - 2) % 12 == 0)
                                                Cell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;
                                            else
                                                Cell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlMedium;

                                            Cell.Borders.get_Item(XlBordersIndex.xlEdgeRight).Weight = XlBorderWeight.xlMedium;

                                            Cell = _excelSheet.get_Range(_excelSheet.Cells[rowindex - 1, ColumnStart - 1], _excelSheet.Cells[rowindex, ColumnStart - 1]);
                                            Cell.MergeCells = true;
                                            Cell.Font.Size = "12";
                                            if ((rowindex - 3) % 12 != 0)
                                                Cell.Borders.get_Item(XlBordersIndex.xlEdgeTop).Weight = XlBorderWeight.xlMedium;
                                            if ((rowindex - 2) % 12 == 0)
                                                Cell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;
                                            else
                                                Cell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlMedium;
                                            Cell.Borders.get_Item(XlBordersIndex.xlEdgeLeft).Weight = XlBorderWeight.xlMedium;
                                            continue;
                                        }
                                }
                            Lesson lsn = RemoteAdapter.Adapter.AdaptLesson(shedule.GetGroupLesson(gr.ObjectHardKey, rowindex - 2));

                            //	if (lsn.SybjectType != null && lsn.SybjectType.MnemoCode == "л.")
                            //	lessonCell.Value2 = lsn.Subject.ShortName + ", " + lsn.SybjectType.MnemoCode;
                            //	else
                            string tutorsString = "";
                            foreach (Tutor tutor in lsn.TutorList)
                                if (tutor != null)
                                {
                                    tutorsString += tutor.ToString() + "\n";
                                }
                            tutorsString.Trim();
                            // Set lesson with tutors
                            lessonCell.Value2 = lsn.Subject.ShortName + "\n" + tutorsString;

                            string flatsString = "";
                            foreach (LectureHall lh in lsn.FlatList)
                                if (lh != null)
                                {
                                    flatsString += lh.MnemoCode + " " + lh.LectureHallBuilding.MnemoCode + "\n";
                                }
                            flatsString.Trim();
                            lessonFlatCell.Value2 = flatsString;
                        }

                        if ((rowindex - 2) % 12 != 0)
                            if ((rowindex) % 2 == 0)
                            {
                                lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeTop).Weight = XlBorderWeight.xlMedium;
                            }
                            else
                            {
                                Range lessonCellPred = _excelSheet.Cells[rowindex - 1, ColumnStart] as Range;
                                lessonCellPred.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThin;
                                lessonCell.Interior.ColorIndex = 6;
                            }

                        if ((rowindex - 1) % 12 == 0)
                            lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;
                        else
                            lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlMedium;

                        lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeRight).Weight = XlBorderWeight.xlMedium;

                        if ((rowindex - 2) % 12 != 0)
                            if ((rowindex) % 2 == 0)
                            {
                                lessonFlatCell.Borders.get_Item(XlBordersIndex.xlEdgeTop).Weight = XlBorderWeight.xlMedium;
                            }
                            else
                            {
                                Range lessonFlatCellPred = _excelSheet.Cells[rowindex - 1, ColumnStart - 1] as Range;
                                lessonFlatCellPred.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThin;
                                lessonFlatCell.Interior.ColorIndex = 6;
                            }

                        if ((rowindex - 1) % 12 == 0)
                            lessonFlatCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;
                        else
                            lessonFlatCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlMedium;
                        lessonFlatCell.Borders.get_Item(XlBordersIndex.xlEdgeLeft).Weight = XlBorderWeight.xlMedium;
                    }

                    ColumnStart += 2;
                }

            #endregion

            EndExport();
        }

        public void Export2(string filename, IList groupList, IShedule2 shedule, IObjectStorage storage, ExcelExporterCallBack callback)
        {
            if (callback != null)
                callback("Запуск Excel", 0);

            BeginExport();

            if (callback != null)
                callback("Підготовка сторінки", 0);

            CreateTitles2(this._excelSheet, shedule, 1);

            #region Выод расписания по группам

            int ColumnStart = 5;
            int CheckedPos = 1;
            foreach (Group gr in groupList)
                if (gr != null)
                {
                    //Вывод номера группы	
                    Range range;
                    range = _excelSheet.Cells[1, ColumnStart - 1] as Range;
                    range.ColumnWidth = "9";

                    range = _excelSheet.Cells[1, ColumnStart] as Range;
                    range.ColumnWidth = "17";

                    range = _excelSheet.get_Range(_excelSheet.Cells[1, ColumnStart - 1], _excelSheet.Cells[1, ColumnStart]);
                    range.MergeCells = true;

                    range.Value2 = gr.MnemoCode;
                    range.Font.Size = "28";

                    range.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    range.BorderAround(1, XlBorderWeight.xlMedium, XlColorIndex.xlColorIndexAutomatic, Type.Missing);
                    range.Font.Bold = true;

                    DateTime beginDate = shedule.GetSheduleBeginDate();
                    DateTime endDate = shedule.GetSheduleEndDate();

                    //Вывод расписания	
                    for (int rowindex = 2; rowindex < (endDate - beginDate).Days * _pairList.Count + 2; rowindex++)
                    {
                        if (callback != null)
                            callback(gr.ToString(), CheckedPos);

                        CheckedPos++;

                        Range lessonCell = _excelSheet.Cells[rowindex, ColumnStart] as Range;
                        Range lessonFlatCell = _excelSheet.Cells[rowindex, ColumnStart - 1] as Range;
                        lessonCell.Font.Size = "12";
                        lessonCell.WrapText = true;
                        lessonCell.VerticalAlignment = XlVAlign.xlVAlignCenter;
                        lessonCell.HorizontalAlignment = XlVAlign.xlVAlignCenter;

                        lessonFlatCell.Font.Size = "12";
                        lessonFlatCell.Font.Bold = true;
                        lessonFlatCell.VerticalAlignment = XlVAlign.xlVAlignCenter;
                        lessonFlatCell.HorizontalAlignment = XlVAlign.xlVAlignCenter;

                        ExtObjKey key = new ExtObjKey();
                        key.UID = storage.GetUUID(gr, out key.Signature, out key.Index);

                        int dayIndex = (rowindex - 2) / (_pairList.Count) + 1;
                        int pairIndex = ((rowindex - 2) - (dayIndex - 1) * _pairList.Count) + 1;

                        if (shedule.GroupHasLesson(key, beginDate.AddDays(dayIndex - 1), pairIndex))
                        {
                            Lesson2 lsn = RemoteAdapter.Adapter.AdaptLesson(shedule.GetGroupLesson(key, beginDate.AddDays(dayIndex - 1), pairIndex));

                            if (lsn.SybjectType != null && lsn.SybjectType.MnemoCode == "л.")
                                lessonCell.Value2 = lsn.Subject.ShortName + ", " + lsn.SybjectType.MnemoCode;
                            else
                                if (lsn.SybjectType != null && lsn.SybjectType.MnemoCode == "конс.")
                                    lessonCell.Value2 = lsn.Subject.ShortName + ", " + lsn.SybjectType.Name;
                                else
                                    lessonCell.Value2 = lsn.Subject.ShortName;

                            string tutorsString = "";
                            foreach (Tutor tutor in lsn.TutorList)
                                if (tutor != null)
                                {
                                    tutorsString += tutor.ToString() + "\n";
                                }
                            tutorsString.Trim();
                            // Set lesson with tutors
                            lessonCell.Value2 += "\n" + tutorsString;

                            string flatsString = "";
                            foreach (LectureHall lh in lsn.FlatList)
                                if (lh != null)
                                {
                                    flatsString += lh.MnemoCode + " " + lh.LectureHallBuilding.MnemoCode + "\n";
                                }
                            flatsString.Trim();
                            lessonFlatCell.Value2 = flatsString;
                        }

                        if ((rowindex - 1) % 6 == 0)
                            lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;
                        else
                            lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlMedium;

                        lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeRight).Weight = XlBorderWeight.xlMedium;

                        if ((rowindex - 1) % 6 == 0)
                            lessonFlatCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;
                        else
                            lessonFlatCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlMedium;
                        lessonFlatCell.Borders.get_Item(XlBordersIndex.xlEdgeLeft).Weight = XlBorderWeight.xlMedium;
                    }
                    ColumnStart += 2;
                }

            #endregion

            EndExport();
        }


        public void ExportLessonsBySubject(string filename, IList lessons, ExcelExporterCallBack callback)
        {
            if (callback != null)
                callback("Запуск Excel", 0);

            BeginExport();

            if (callback != null)
                callback("Підготовка сторінки", 0);

            CreateTitles(this._excelSheet, 1);

            int ColumnStart = 4;
            int CheckedPos = 1;

            foreach (Lesson lsn in lessons)
            {
                int rowindex = (int.Parse(lsn.CurLessonDay.MnemoCode) - 1) * _weekList.Count * _pairList.Count + (int.Parse(lsn.CurLessonPair.MnemoCode) - 1) * _weekList.Count + (int.Parse(lsn.CurLessonWeek.MnemoCode) - 1) + 2;

                if (callback != null)
                    callback(lsn.Text, CheckedPos);

                CheckedPos++;

                string groupsString = "";
                foreach (Group gr in lsn.GroupList)
                    if (gr != null)
                        if (groupsString != "")
                            groupsString += ", " + gr.MnemoCode;
                        else
                            groupsString = gr.MnemoCode;


                string lessonCellStr = lsn.Subject.ShortName + "\n" + groupsString;

                string flatsString = "";
                foreach (LectureHall lh in lsn.FlatList)
                    if (lh != null)
                        if (flatsString != "")
                            flatsString += "\n" + lh.MnemoCode + " " + lh.LectureHallBuilding.MnemoCode;
                        else
                            flatsString = lh.MnemoCode + " " + lh.LectureHallBuilding.MnemoCode;

                string lessonFlatCellStr = flatsString;

                int CurrentColumn = ColumnStart;

                while (true)
                {
                    string cellval = (_excelSheet.Cells[rowindex, CurrentColumn] as Range).Value2 as string;

                    if (cellval == "")
                        break;

                    if (cellval == null)
                        break;

                    CurrentColumn += 2;
                }

                Range lessonCell = _excelSheet.Cells[rowindex, CurrentColumn] as Range;
                Range lessonFlatCell = _excelSheet.Cells[rowindex, CurrentColumn - 1] as Range;

                lessonCell.ColumnWidth = "17";
                lessonCell.Font.Size = "12";
                lessonCell.WrapText = true;
                lessonCell.VerticalAlignment = XlVAlign.xlVAlignCenter;
                lessonCell.HorizontalAlignment = XlVAlign.xlVAlignCenter;
                lessonCell.Value2 = lessonCellStr;

                lessonFlatCell.Font.Size = "12";
                lessonFlatCell.ColumnWidth = "9";
                lessonFlatCell.Font.Bold = true;
                lessonFlatCell.VerticalAlignment = XlVAlign.xlVAlignCenter;
                lessonFlatCell.HorizontalAlignment = XlVAlign.xlVAlignCenter;
                lessonFlatCell.Value2 = lessonFlatCellStr;

                // borders
                if ((rowindex - 2) % 12 != 0)
                    if ((rowindex) % 2 == 0)
                    {
                        lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeTop).Weight = XlBorderWeight.xlMedium;
                    }
                    else
                    {
                        Range lessonCellPred = _excelSheet.Cells[rowindex - 1, ColumnStart] as Range;
                        lessonCellPred.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThin;
                        lessonCell.Interior.ColorIndex = 6;
                    }

                if ((rowindex - 1) % 12 == 0)
                    lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;
                else
                    lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlMedium;

                lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeRight).Weight = XlBorderWeight.xlMedium;

                if ((rowindex - 2) % 12 != 0)
                    if ((rowindex) % 2 == 0)
                    {
                        lessonFlatCell.Borders.get_Item(XlBordersIndex.xlEdgeTop).Weight = XlBorderWeight.xlMedium;
                    }
                    else
                    {
                        Range lessonFlatCellPred = _excelSheet.Cells[rowindex - 1, ColumnStart - 1] as Range;
                        lessonFlatCellPred.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThin;
                        lessonFlatCell.Interior.ColorIndex = 6;
                    }

                if ((rowindex - 1) % 12 == 0)
                    lessonFlatCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;
                else
                    lessonFlatCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlMedium;
                lessonFlatCell.Borders.get_Item(XlBordersIndex.xlEdgeLeft).Weight = XlBorderWeight.xlMedium;
            }

            EndExport();
        }

        public void ExportTutorShedule(string filename, IList tutorList, IShedule shedule, ExcelExporterCallBack callback)
        {
            if (callback != null)
                callback("Запуск Excel", 0);

            BeginExport();

            if (callback != null)
                callback("Підготовка сторінки", 0);

            CreateTitles(this._excelSheet, 1);

            #region Выод расписания по преподавателям

            int ColumnStart = 4;
            int CheckedPos = 1;

            foreach (Tutor ttr in tutorList)
                if (ttr != null)
                {
                    //Вывод номера группы
                    Range range;
                    range = _excelSheet.Cells[1, ColumnStart - 1] as Range;
                    range.ColumnWidth = "9";

                    range = _excelSheet.Cells[1, ColumnStart] as Range;
                    range.ColumnWidth = "17";

                    range = _excelSheet.get_Range(_excelSheet.Cells[1, ColumnStart - 1], _excelSheet.Cells[1, ColumnStart]);
                    range.MergeCells = true;

                    range.Value2 = ttr.TutorSurname + " " + ttr.TutorName + "." + ttr.TutorPatronimic + ".";
                    range.Font.Size = "14";

                    range.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    range.BorderAround(1, XlBorderWeight.xlMedium, XlColorIndex.xlColorIndexAutomatic, Type.Missing);
                    range.Font.Bold = true;

                    //Вывод расписания
                    for (int rowindex = 2; rowindex < _dayList.Count * _pairList.Count * _weekList.Count + 2; rowindex++)
                    {
                        if (callback != null)
                            callback(ttr.ToString(), CheckedPos);

                        CheckedPos++;

                        Range lessonCell = _excelSheet.Cells[rowindex, ColumnStart] as Range;
                        Range lessonFlatCell = _excelSheet.Cells[rowindex, ColumnStart - 1] as Range;
                        lessonCell.Font.Size = "12";
                        lessonCell.WrapText = true;
                        lessonCell.VerticalAlignment = XlVAlign.xlVAlignCenter;
                        lessonCell.HorizontalAlignment = XlVAlign.xlVAlignCenter;

                        lessonFlatCell.Font.Size = "12";
                        lessonFlatCell.Font.Bold = true;
                        lessonFlatCell.VerticalAlignment = XlVAlign.xlVAlignCenter;
                        lessonFlatCell.HorizontalAlignment = XlVAlign.xlVAlignCenter;

                        string lessonstr = shedule.GetTutorRestrictsText(ttr.ObjectHardKey, rowindex - 2);
                        if (lessonstr != string.Empty && lessonstr[0] == '#')
                        {
                            lessonCell.Value2 = lessonstr.Remove(0, 1);
                        }

                        if (shedule.TutorHasLesson(ttr.ObjectHardKey, rowindex - 2))
                        {
                            if ((rowindex - 2) % 2 != 0)
                                if ((shedule.TutorHasLesson(ttr.ObjectHardKey, rowindex - 2 - 1)))
                                {
                                    RemoteLesson currLesson = shedule.GetTutorLesson(ttr.ObjectHardKey, rowindex - 2);
                                    RemoteLesson predLesson = shedule.GetTutorLesson(ttr.ObjectHardKey, rowindex - 2 - 1);
                                    if (currLesson.leading_uid == predLesson.leading_uid)
                                        if (this.ListCompare(currLesson.flatList, predLesson.flatList))
                                        {
                                            Range Cell;
                                            Cell = _excelSheet.get_Range(_excelSheet.Cells[rowindex - 1, ColumnStart], _excelSheet.Cells[rowindex, ColumnStart]);
                                            Cell.MergeCells = true;
                                            Cell.Font.Size = "12";

                                            if ((rowindex - 3) % 12 != 0)
                                                Cell.Borders.get_Item(XlBordersIndex.xlEdgeTop).Weight = XlBorderWeight.xlMedium;

                                            if ((rowindex - 2) % 12 == 0)
                                                Cell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;
                                            else
                                                Cell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlMedium;

                                            Cell.Borders.get_Item(XlBordersIndex.xlEdgeRight).Weight = XlBorderWeight.xlMedium;

                                            Cell = _excelSheet.get_Range(_excelSheet.Cells[rowindex - 1, ColumnStart - 1], _excelSheet.Cells[rowindex, ColumnStart - 1]);
                                            Cell.MergeCells = true;
                                            Cell.Font.Size = "12";
                                            if ((rowindex - 3) % 12 != 0)
                                                Cell.Borders.get_Item(XlBordersIndex.xlEdgeTop).Weight = XlBorderWeight.xlMedium;
                                            if ((rowindex - 2) % 12 == 0)
                                                Cell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;
                                            else
                                                Cell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlMedium;
                                            Cell.Borders.get_Item(XlBordersIndex.xlEdgeLeft).Weight = XlBorderWeight.xlMedium;
                                            continue;
                                        }
                                }
                            Lesson lsn = RemoteAdapter.Adapter.AdaptLesson(shedule.GetTutorLesson(ttr.ObjectHardKey, rowindex - 2));
                            string groupsString = "";
                            foreach (Group gr in lsn.GroupList)
                                if (gr != null)
                                    if (groupsString != "")
                                        groupsString += ", " + gr.MnemoCode;
                                    else
                                        groupsString = gr.MnemoCode;


                            lessonCell.Value2 = lsn.Subject.ShortName + "\n" + groupsString;

                            string flatsString = "";
                            foreach (LectureHall lh in lsn.FlatList)
                                if (lh != null)
                                    if (flatsString != "")
                                        flatsString += "\n" + lh.MnemoCode + " " + lh.LectureHallBuilding.MnemoCode;
                                    else
                                        flatsString = lh.MnemoCode + " " + lh.LectureHallBuilding.MnemoCode;

                            lessonFlatCell.Value2 = flatsString;
                        }

                        if ((rowindex - 2) % 12 != 0)
                            if ((rowindex) % 2 == 0)
                            {
                                lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeTop).Weight = XlBorderWeight.xlMedium;
                            }
                            else
                            {
                                Range lessonCellPred = _excelSheet.Cells[rowindex - 1, ColumnStart] as Range;
                                lessonCellPred.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThin;
                                lessonCell.Interior.ColorIndex = 6;
                            }

                        if ((rowindex - 1) % 12 == 0)
                            lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;
                        else
                            lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlMedium;

                        lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeRight).Weight = XlBorderWeight.xlMedium;

                        if ((rowindex - 2) % 12 != 0)
                            if ((rowindex) % 2 == 0)
                            {
                                lessonFlatCell.Borders.get_Item(XlBordersIndex.xlEdgeTop).Weight = XlBorderWeight.xlMedium;
                            }
                            else
                            {
                                Range lessonFlatCellPred = _excelSheet.Cells[rowindex - 1, ColumnStart - 1] as Range;
                                lessonFlatCellPred.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThin;
                                lessonFlatCell.Interior.ColorIndex = 6;
                            }

                        if ((rowindex - 1) % 12 == 0)
                            lessonFlatCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;
                        else
                            lessonFlatCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlMedium;
                        lessonFlatCell.Borders.get_Item(XlBordersIndex.xlEdgeLeft).Weight = XlBorderWeight.xlMedium;
                    }

                    ColumnStart += 2;
                }

            #endregion

            EndExport();
        }

        public void ExportTutorShedule2(string filename, IList tutorList, IShedule2 shedule, IObjectStorage storage, ExcelExporterCallBack callback)
        {
            if (callback != null)
                callback("Запуск Excel", 0);

            BeginExport();

            if (callback != null)
                callback("Підготовка сторінки", 0);

            CreateTitles2(this._excelSheet, shedule, 1);

            #region Выод расписания по преподавателям

            int ColumnStart = 5;
            int CheckedPos = 1;

            foreach (Tutor ttr in tutorList)
                if (ttr != null)
                {
                    //Вывод номера группы
                    Range range;
                    range = _excelSheet.Cells[1, ColumnStart - 1] as Range;
                    range.ColumnWidth = "9";

                    range = _excelSheet.Cells[1, ColumnStart] as Range;
                    range.ColumnWidth = "17";

                    range = _excelSheet.get_Range(_excelSheet.Cells[1, ColumnStart - 1], _excelSheet.Cells[1, ColumnStart]);
                    range.MergeCells = true;

                    range.Value2 = ttr.TutorSurname + " " + ttr.TutorName + "." + ttr.TutorPatronimic + ".";
                    range.Font.Size = "14";

                    range.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    range.BorderAround(1, XlBorderWeight.xlMedium, XlColorIndex.xlColorIndexAutomatic, Type.Missing);
                    range.Font.Bold = true;

                    DateTime beginDate = shedule.GetSheduleBeginDate();
                    DateTime endDate = shedule.GetSheduleEndDate();

                    //Вывод расписания	
                    for (int rowindex = 2; rowindex < (endDate - beginDate).Days * _pairList.Count + 2; rowindex++)
                    {
                        if (callback != null)
                            callback(ttr.ToString(), CheckedPos);

                        CheckedPos++;

                        Range lessonCell = _excelSheet.Cells[rowindex, ColumnStart] as Range;
                        Range lessonFlatCell = _excelSheet.Cells[rowindex, ColumnStart - 1] as Range;
                        lessonCell.Font.Size = "12";
                        lessonCell.WrapText = true;
                        lessonCell.VerticalAlignment = XlVAlign.xlVAlignCenter;
                        lessonCell.HorizontalAlignment = XlVAlign.xlVAlignCenter;

                        lessonFlatCell.Font.Size = "12";
                        lessonFlatCell.Font.Bold = true;
                        lessonFlatCell.VerticalAlignment = XlVAlign.xlVAlignCenter;
                        lessonFlatCell.HorizontalAlignment = XlVAlign.xlVAlignCenter;

                        int dayIndex = (rowindex - 2) / (_pairList.Count) + 1;
                        int pairIndex = ((rowindex - 2) - (dayIndex - 1) * _pairList.Count) + 1;

                        string lessonstr = shedule.GetTutorRestrictsText(ttr.ObjectHardKey, beginDate.AddDays(dayIndex - 1), pairIndex);
                        if (lessonstr != string.Empty && lessonstr[0] == '#')
                        {
                            lessonCell.Value2 = lessonstr.Remove(0, 1);
                        }

                        RemoteLesson2 currLesson = shedule.GetTutorLesson(ttr.ObjectHardKey, beginDate.AddDays(dayIndex - 1), pairIndex);

                        if (currLesson != null)
                        {
                            Lesson2 lsn = RemoteAdapter.Adapter.AdaptLesson(shedule.GetTutorLesson(ttr.ObjectHardKey, beginDate.AddDays(dayIndex - 1), pairIndex));

                            string groupsString = "";
                            foreach (Group gr in lsn.GroupList)
                                if (gr != null)
                                    if (groupsString != "")
                                        groupsString += ", " + gr.MnemoCode;
                                    else
                                        groupsString = gr.MnemoCode;

                            if (lsn.SybjectType != null && lsn.SybjectType.MnemoCode == "конс.")
                                lessonCell.Value2 = lsn.Subject.ShortName + "\n" + lsn.SybjectType.Name + "\n" + groupsString;
                            else
                                lessonCell.Value2 = lsn.Subject.ShortName + "\n" + groupsString;

                            string flatsString = "";
                            foreach (LectureHall lh in lsn.FlatList)
                                if (lh != null)
                                    if (flatsString != "")
                                        flatsString += "\n" + lh.MnemoCode + " " + lh.LectureHallBuilding.MnemoCode;
                                    else
                                        flatsString = lh.MnemoCode + " " + lh.LectureHallBuilding.MnemoCode;

                            lessonFlatCell.Value2 = flatsString;
                        }

                        if ((rowindex - 1) % 6 == 0)
                            lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;
                        else
                            lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlMedium;

                        lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeRight).Weight = XlBorderWeight.xlMedium;

                        if ((rowindex - 1) % 6 == 0)
                            lessonFlatCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;
                        else
                            lessonFlatCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlMedium;
                        lessonFlatCell.Borders.get_Item(XlBordersIndex.xlEdgeLeft).Weight = XlBorderWeight.xlMedium;

                    }

                    ColumnStart += 2;
                }

            #endregion

            EndExport();
        }


        public void ExportDepartmentShedule(string filename, Department department, IShedule shedule, ExcelExporterCallBack callback)
        {
            if (callback != null)
                callback("Запуск Excel", 0);

            BeginExport();

            if (callback != null)
                callback("Підготовка сторінки", 0);

            CreateTitles(this._excelSheet, 1);

            #region Выод расписания по преподавателям

            long department_uid_const = department.Uid;

            int ColumnStart = 4;
            int CheckedPos = 1;

            ListWrapper sortedDeptTutors = new ListWrapper(typeof(Tutor), department.DepartmentTutors);
            sortedDeptTutors.Sort(1);

            foreach (Tutor ttr in sortedDeptTutors)
                if (ttr != null)
                {
                    //Вывод номера группы
                    Range range;
                    range = _excelSheet.Cells[1, ColumnStart - 1] as Range;
                    range.ColumnWidth = "9";

                    range = _excelSheet.Cells[1, ColumnStart] as Range;
                    range.ColumnWidth = "17";

                    range = _excelSheet.get_Range(_excelSheet.Cells[1, ColumnStart - 1], _excelSheet.Cells[1, ColumnStart]);
                    range.MergeCells = true;

                    range.Value2 = ttr.TutorSurname + " " + ttr.TutorName + "." + ttr.TutorPatronimic + ".";
                    range.Font.Size = "14";

                    range.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    range.BorderAround(1, XlBorderWeight.xlMedium, XlColorIndex.xlColorIndexAutomatic, Type.Missing);
                    range.Font.Bold = true;

                    //Вывод расписания
                    for (int rowindex = 2; rowindex < _dayList.Count * _pairList.Count * _weekList.Count + 2; rowindex++)
                    {
                        if (callback != null)
                            callback(ttr.ToString(), CheckedPos);

                        CheckedPos++;

                        Range lessonCell = _excelSheet.Cells[rowindex, ColumnStart] as Range;
                        Range lessonFlatCell = _excelSheet.Cells[rowindex, ColumnStart - 1] as Range;
                        lessonCell.Font.Size = "12";
                        lessonCell.WrapText = true;
                        lessonCell.VerticalAlignment = XlVAlign.xlVAlignCenter;
                        lessonCell.HorizontalAlignment = XlVAlign.xlVAlignCenter;

                        lessonFlatCell.Font.Size = "12";
                        lessonFlatCell.Font.Bold = true;
                        lessonFlatCell.VerticalAlignment = XlVAlign.xlVAlignCenter;
                        lessonFlatCell.HorizontalAlignment = XlVAlign.xlVAlignCenter;

                        string lessonstr = shedule.GetTutorRestrictsText(ttr.ObjectHardKey, rowindex - 2);
                        if (lessonstr != string.Empty && lessonstr[0] == '#')
                        {
                            lessonCell.Value2 = lessonstr.Remove(0, 1);
                        }

                        RemoteLesson currLesson = shedule.GetTutorLesson(ttr.ObjectHardKey, rowindex - 2);

                        if (currLesson != null)
                        {
                            bool mustShowLesson = shedule.GetStudyLeading(currLesson.leading_uid).department_key.UID == department_uid_const;

                            RemoteLesson predLesson = shedule.GetTutorLesson(ttr.ObjectHardKey, rowindex - 2 - 1);
                            if ((rowindex - 2) % 2 != 0)
                                if (predLesson != null)
                                {
                                    //	RemoteLesson currLesson = shedule.GetTutorLesson(ttr.Uid, rowindex - 2);
                                    //	RemoteLesson predLesson = shedule.GetTutorLesson(ttr.Uid, rowindex - 2 - 1);
                                    if (currLesson.leading_uid == predLesson.leading_uid && mustShowLesson)
                                        if (this.ListCompare(currLesson.flatList, predLesson.flatList))
                                        {
                                            Range Cell;
                                            Cell = _excelSheet.get_Range(_excelSheet.Cells[rowindex - 1, ColumnStart], _excelSheet.Cells[rowindex, ColumnStart]);
                                            Cell.MergeCells = true;
                                            Cell.Font.Size = "12";

                                            if ((rowindex - 3) % 12 != 0)
                                                Cell.Borders.get_Item(XlBordersIndex.xlEdgeTop).Weight = XlBorderWeight.xlMedium;

                                            if ((rowindex - 2) % 12 == 0)
                                                Cell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;
                                            else
                                                Cell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlMedium;

                                            Cell.Borders.get_Item(XlBordersIndex.xlEdgeRight).Weight = XlBorderWeight.xlMedium;

                                            Cell = _excelSheet.get_Range(_excelSheet.Cells[rowindex - 1, ColumnStart - 1], _excelSheet.Cells[rowindex, ColumnStart - 1]);
                                            Cell.MergeCells = true;
                                            Cell.Font.Size = "12";
                                            if ((rowindex - 3) % 12 != 0)
                                                Cell.Borders.get_Item(XlBordersIndex.xlEdgeTop).Weight = XlBorderWeight.xlMedium;
                                            if ((rowindex - 2) % 12 == 0)
                                                Cell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;
                                            else
                                                Cell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlMedium;
                                            Cell.Borders.get_Item(XlBordersIndex.xlEdgeLeft).Weight = XlBorderWeight.xlMedium;
                                            continue;
                                        }
                                }
                            Lesson lsn = RemoteAdapter.Adapter.AdaptLesson(shedule.GetTutorLesson(ttr.ObjectHardKey, rowindex - 2));

                            if (mustShowLesson)
                            {
                                string groupsString = "";
                                foreach (Group gr in lsn.GroupList)
                                    if (gr != null)
                                        if (groupsString != "")
                                            groupsString += ", " + gr.MnemoCode;
                                        else
                                            groupsString = gr.MnemoCode;


                                lessonCell.Value2 = lsn.Subject.ShortName + "\n" + groupsString;

                                string flatsString = "";
                                foreach (LectureHall lh in lsn.FlatList)
                                    if (lh != null)
                                        if (flatsString != "")
                                            flatsString += "\n" + lh.MnemoCode + " " + lh.LectureHallBuilding.MnemoCode;
                                        else
                                            flatsString = lh.MnemoCode + " " + lh.LectureHallBuilding.MnemoCode;

                                lessonFlatCell.Value2 = flatsString;
                            }
                        }

                        if ((rowindex - 2) % 12 != 0)
                            if ((rowindex) % 2 == 0)
                            {
                                lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeTop).Weight = XlBorderWeight.xlMedium;
                            }
                            else
                            {
                                Range lessonCellPred = _excelSheet.Cells[rowindex - 1, ColumnStart] as Range;
                                lessonCellPred.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThin;
                                lessonCell.Interior.ColorIndex = 6;
                            }

                        if ((rowindex - 1) % 12 == 0)
                            lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;
                        else
                            lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlMedium;

                        lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeRight).Weight = XlBorderWeight.xlMedium;

                        if ((rowindex - 2) % 12 != 0)
                            if ((rowindex) % 2 == 0)
                            {
                                lessonFlatCell.Borders.get_Item(XlBordersIndex.xlEdgeTop).Weight = XlBorderWeight.xlMedium;
                            }
                            else
                            {
                                Range lessonFlatCellPred = _excelSheet.Cells[rowindex - 1, ColumnStart - 1] as Range;
                                lessonFlatCellPred.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThin;
                                lessonFlatCell.Interior.ColorIndex = 6;
                            }

                        if ((rowindex - 1) % 12 == 0)
                            lessonFlatCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;
                        else
                            lessonFlatCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlMedium;
                        lessonFlatCell.Borders.get_Item(XlBordersIndex.xlEdgeLeft).Weight = XlBorderWeight.xlMedium;
                    }

                    ColumnStart += 2;
                }

            #endregion

            EndExport();
        }

        public void ExportDepartmentShedule2(string filename, Department department, IShedule2 shedule, IObjectStorage storage, ExcelExporterCallBack callback)
        {
            if (callback != null)
                callback("Запуск Excel", 0);

            BeginExport();

            if (callback != null)
                callback("Підготовка сторінки", 0);

            CreateTitles2(this._excelSheet, shedule, 1);

            #region Выод расписания по преподавателям

            long department_uid_const = department.Uid;

            int ColumnStart = 5;
            int CheckedPos = 1;

            ListWrapper sortedDeptTutors = new ListWrapper(typeof(Tutor), department.DepartmentTutors);
            sortedDeptTutors.Sort(1);

            foreach (Tutor ttr in sortedDeptTutors)
                if (ttr != null)
                {
                    //Вывод номера группы
                    Range range;
                    range = _excelSheet.Cells[1, ColumnStart - 1] as Range;
                    range.ColumnWidth = "9";

                    range = _excelSheet.Cells[1, ColumnStart] as Range;
                    range.ColumnWidth = "17";

                    range = _excelSheet.get_Range(_excelSheet.Cells[1, ColumnStart - 1], _excelSheet.Cells[1, ColumnStart]);
                    range.MergeCells = true;

                    range.Value2 = ttr.TutorSurname + " " + ttr.TutorName + "." + ttr.TutorPatronimic + ".";
                    range.Font.Size = "14";

                    range.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    range.BorderAround(1, XlBorderWeight.xlMedium, XlColorIndex.xlColorIndexAutomatic, Type.Missing);
                    range.Font.Bold = true;

                    DateTime beginDate = shedule.GetSheduleBeginDate();
                    DateTime endDate = shedule.GetSheduleEndDate();

                    //Вывод расписания	
                    for (int rowindex = 2; rowindex < (endDate - beginDate).Days * _pairList.Count + 2; rowindex++)
                    {
                        if (callback != null)
                            callback(ttr.ToString(), CheckedPos);

                        CheckedPos++;

                        Range lessonCell = _excelSheet.Cells[rowindex, ColumnStart] as Range;
                        Range lessonFlatCell = _excelSheet.Cells[rowindex, ColumnStart - 1] as Range;
                        lessonCell.Font.Size = "12";
                        lessonCell.WrapText = true;
                        lessonCell.VerticalAlignment = XlVAlign.xlVAlignCenter;
                        lessonCell.HorizontalAlignment = XlVAlign.xlVAlignCenter;

                        lessonFlatCell.Font.Size = "12";
                        lessonFlatCell.Font.Bold = true;
                        lessonFlatCell.VerticalAlignment = XlVAlign.xlVAlignCenter;
                        lessonFlatCell.HorizontalAlignment = XlVAlign.xlVAlignCenter;

                        int dayIndex = (rowindex - 2) / (_pairList.Count) + 1;
                        int pairIndex = ((rowindex - 2) - (dayIndex - 1) * _pairList.Count) + 1;

                        string lessonstr = shedule.GetTutorRestrictsText(ttr.ObjectHardKey, beginDate.AddDays(dayIndex - 1), pairIndex);
                        if (lessonstr != string.Empty && lessonstr[0] == '#')
                        {
                            lessonCell.Value2 = lessonstr.Remove(0, 1);
                        }

                        RemoteLesson2 currLesson = shedule.GetTutorLesson(ttr.ObjectHardKey, beginDate.AddDays(dayIndex - 1), pairIndex);

                        if (currLesson != null)
                        {
                            bool mustShowLesson = shedule.GetStudyLeading(currLesson.leading_uid).department_key.UID == department_uid_const;

                            RemoteLesson2 predLesson = shedule.GetTutorLesson(ttr.ObjectHardKey, beginDate.AddDays(dayIndex - 1), pairIndex);
                            Lesson2 lsn = RemoteAdapter.Adapter.AdaptLesson(shedule.GetTutorLesson(ttr.ObjectHardKey, beginDate.AddDays(dayIndex - 1), pairIndex));

                            if (mustShowLesson)
                            {
                                string groupsString = "";
                                foreach (Group gr in lsn.GroupList)
                                    if (gr != null)
                                        if (groupsString != "")
                                            groupsString += ", " + gr.MnemoCode;
                                        else
                                            groupsString = gr.MnemoCode;

                                if (lsn.SybjectType != null && lsn.SybjectType.MnemoCode == "конс.")
                                    lessonCell.Value2 = lsn.Subject.ShortName + "\n" + lsn.SybjectType.Name + "\n" + groupsString;
                                else
                                    lessonCell.Value2 = lsn.Subject.ShortName + "\n" + groupsString;

                                string flatsString = "";
                                foreach (LectureHall lh in lsn.FlatList)
                                    if (lh != null)
                                        if (flatsString != "")
                                            flatsString += "\n" + lh.MnemoCode + " " + lh.LectureHallBuilding.MnemoCode;
                                        else
                                            flatsString = lh.MnemoCode + " " + lh.LectureHallBuilding.MnemoCode;

                                lessonFlatCell.Value2 = flatsString;
                            }
                        }

                        if ((rowindex - 1) % 6 == 0)
                            lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;
                        else
                            lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlMedium;

                        lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeRight).Weight = XlBorderWeight.xlMedium;

                        if ((rowindex - 1) % 6 == 0)
                            lessonFlatCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;
                        else
                            lessonFlatCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlMedium;
                        lessonFlatCell.Borders.get_Item(XlBordersIndex.xlEdgeLeft).Weight = XlBorderWeight.xlMedium;

                    }

                    ColumnStart += 2;
                }

            #endregion

            EndExport();
        }



        public void ExportFlatShedule(string filename, IList flatList, IShedule shedule, ExcelExporterCallBack callback)
        {
            if (callback != null)
                callback("Запуск Excel", 0);

            BeginExport();

            if (callback != null)
                callback("Підготовка сторінки", 0);

            CreateTitles(this._excelSheet, 1);

            #region Выод расписания по аудиториям

            int ColumnStart = 4;
            int CheckedPos = 1;

            foreach (LectureHall lh in flatList)
                if (lh != null)
                {
                    //Вывод номера группы
                    Range range;
                    range = _excelSheet.Cells[1, ColumnStart - 1] as Range;
                    range.ColumnWidth = "9";

                    range = _excelSheet.Cells[1, ColumnStart] as Range;
                    range.ColumnWidth = "17";

                    range = _excelSheet.get_Range(_excelSheet.Cells[1, ColumnStart - 1], _excelSheet.Cells[1, ColumnStart]);
                    range.MergeCells = true;

                    range.Value2 = lh.ToString();
                    range.Font.Size = "14";

                    range.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    range.BorderAround(1, XlBorderWeight.xlMedium, XlColorIndex.xlColorIndexAutomatic, Type.Missing);
                    range.Font.Bold = true;

                    //Вывод расписания
                    for (int rowindex = 2; rowindex < _dayList.Count * _pairList.Count * _weekList.Count + 2; rowindex++)
                    {
                        if (callback != null)
                            callback(lh.ToString(), CheckedPos);

                        CheckedPos++;

                        Range lessonCell = _excelSheet.Cells[rowindex, ColumnStart] as Range;
                        Range lessonGroupCell = _excelSheet.Cells[rowindex, ColumnStart - 1] as Range;
                        lessonCell.Font.Size = "12";
                        lessonCell.WrapText = true;
                        lessonCell.VerticalAlignment = XlVAlign.xlVAlignCenter;
                        lessonCell.HorizontalAlignment = XlVAlign.xlVAlignCenter;

                        lessonGroupCell.Font.Size = "12";
                        lessonGroupCell.Font.Bold = true;
                        lessonGroupCell.VerticalAlignment = XlVAlign.xlVAlignCenter;
                        lessonGroupCell.HorizontalAlignment = XlVAlign.xlVAlignCenter;


                        lessonCell.Value2 = shedule.GetFlatRestrictsText(lh.ObjectHardKey, rowindex - 2);

                        if (shedule.FlatHasLesson(lh.ObjectHardKey, rowindex - 2))
                        {
                            // Text of group list for group cell
                            string groupsString = "";

                            // Text of subjects foк subjects cell
                            string subjectsString = "";

                            IList lessonList = shedule.GetFlatLesson(lh.ObjectHardKey, rowindex - 2);

                            foreach (RemoteLesson remlsn in lessonList)
                            {
                                if (groupsString != "")
                                {
                                    lessonGroupCell.RowHeight = "30";
                                    lessonCell.RowHeight = "30";
                                }
                                Lesson lsn = RemoteAdapter.Adapter.AdaptLesson(remlsn);

                                // перехід на нову строку, у зв`язку з тим що проводится більше одного заняття
                                if (groupsString != "")
                                    groupsString += "\n";

                                foreach (Group gr in lsn.GroupList)
                                    if (gr != null)
                                        if (groupsString != "" && !groupsString.EndsWith("\n"))
                                            groupsString += ", " + gr.MnemoCode;
                                        else
                                            groupsString += gr.MnemoCode;
                                // перехід на нову строку, у зв`язку з тим що проводится більше одного заняття
                                if (subjectsString != "")
                                    subjectsString += "\n";

                                if (lsn.SybjectType != null)
                                    subjectsString += lsn.SybjectType.MnemoCode + ", ";
                                subjectsString += lsn.Subject.ShortName;

                            }
                            Range prevlessonCell = _excelSheet.Cells[rowindex - 1, ColumnStart] as Range;
                            Range prevlessonFlatCell = _excelSheet.Cells[rowindex - 1, ColumnStart - 1] as Range;

                            string prevsubjectsString = prevlessonCell.Value2 as string;
                            string prevlessonFlatCellString = prevlessonFlatCell.Value2 as string;

                            if ((rowindex - 2) % 2 != 0)
                                if (prevsubjectsString == subjectsString && prevlessonFlatCellString == groupsString)
                                {
                                    Range Cell;
                                    Cell = _excelSheet.get_Range(_excelSheet.Cells[rowindex - 1, ColumnStart], _excelSheet.Cells[rowindex, ColumnStart]);
                                    Cell.MergeCells = true;
                                    Cell.Borders.get_Item(XlBordersIndex.xlEdgeRight).Weight = XlBorderWeight.xlMedium;

                                    Cell = _excelSheet.get_Range(_excelSheet.Cells[rowindex - 1, ColumnStart - 1], _excelSheet.Cells[rowindex, ColumnStart - 1]);
                                    Cell.MergeCells = true;
                                    Cell.Borders.get_Item(XlBordersIndex.xlEdgeLeft).Weight = XlBorderWeight.xlMedium;
                                }
                                else
                                {
                                    lessonGroupCell.Value2 = groupsString;
                                    lessonCell.Value2 = subjectsString;
                                }
                            else
                            {
                                lessonGroupCell.Value2 = groupsString;
                                lessonCell.Value2 = subjectsString;
                            }
                        }

                        if ((rowindex - 2) % 12 != 0)
                            if ((rowindex) % 2 == 0)
                            {
                                lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeTop).Weight = XlBorderWeight.xlMedium;
                            }
                            else
                            {
                                Range lessonCellPred = _excelSheet.Cells[rowindex - 1, ColumnStart] as Range;
                                lessonCellPred.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThin;
                                lessonCell.Interior.ColorIndex = 6;
                            }

                        if ((rowindex - 1) % 12 == 0)
                            lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;
                        else
                            lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlMedium;

                        lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeRight).Weight = XlBorderWeight.xlMedium;

                        if ((rowindex - 2) % 12 != 0)
                            if ((rowindex) % 2 == 0)
                            {
                                lessonGroupCell.Borders.get_Item(XlBordersIndex.xlEdgeTop).Weight = XlBorderWeight.xlMedium;
                            }
                            else
                            {
                                Range lessonFlatCellPred = _excelSheet.Cells[rowindex - 1, ColumnStart - 1] as Range;
                                lessonFlatCellPred.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThin;
                                lessonGroupCell.Interior.ColorIndex = 6;
                            }

                        if ((rowindex - 1) % 12 == 0)
                            lessonGroupCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;
                        else
                            lessonGroupCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlMedium;
                        lessonGroupCell.Borders.get_Item(XlBordersIndex.xlEdgeLeft).Weight = XlBorderWeight.xlMedium;
                    }

                    ColumnStart += 2;
                }

            #endregion

            EndExport();
        }

        public void ExportFlatShedule2(string filename, IList flatList, IShedule2 shedule, IObjectStorage storage, ExcelExporterCallBack callback)
        {
            if (callback != null)
                callback("Запуск Excel", 0);

            BeginExport();

            if (callback != null)
                callback("Підготовка сторінки", 0);

            CreateTitles2(this._excelSheet, shedule, 1);

            #region Выод расписания по аудиториям

            int ColumnStart = 5;
            int CheckedPos = 1;

            foreach (LectureHall lh in flatList)
                if (lh != null)
                {
                    //Вывод номера группы
                    Range range;
                    range = _excelSheet.Cells[1, ColumnStart - 1] as Range;
                    range.ColumnWidth = "9";

                    range = _excelSheet.Cells[1, ColumnStart] as Range;
                    range.ColumnWidth = "17";

                    range = _excelSheet.get_Range(_excelSheet.Cells[1, ColumnStart - 1], _excelSheet.Cells[1, ColumnStart]);
                    range.MergeCells = true;

                    range.Value2 = lh.ToString();
                    range.Font.Size = "14";

                    range.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    range.BorderAround(1, XlBorderWeight.xlMedium, XlColorIndex.xlColorIndexAutomatic, Type.Missing);
                    range.Font.Bold = true;

                    DateTime beginDate = shedule.GetSheduleBeginDate();
                    DateTime endDate = shedule.GetSheduleEndDate();

                    //Вывод расписания
                    for (int rowindex = 2; rowindex < (endDate - beginDate).Days * _pairList.Count + 2; rowindex++)
                    {
                        if (callback != null)
                            callback(lh.ToString(), CheckedPos);

                        CheckedPos++;

                        Range lessonCell = _excelSheet.Cells[rowindex, ColumnStart] as Range;
                        Range lessonGroupCell = _excelSheet.Cells[rowindex, ColumnStart - 1] as Range;
                        lessonCell.Font.Size = "12";
                        lessonCell.WrapText = true;
                        lessonCell.VerticalAlignment = XlVAlign.xlVAlignCenter;
                        lessonCell.HorizontalAlignment = XlVAlign.xlVAlignCenter;

                        lessonGroupCell.Font.Size = "12";
                        lessonGroupCell.Font.Bold = true;
                        lessonGroupCell.VerticalAlignment = XlVAlign.xlVAlignCenter;
                        lessonGroupCell.HorizontalAlignment = XlVAlign.xlVAlignCenter;

                        ExtObjKey key = new ExtObjKey();
                        key.UID = storage.GetUUID(lh, out key.Signature, out key.Index);

                        int dayIndex = (rowindex - 2) / (_pairList.Count) + 1;
                        int pairIndex = ((rowindex - 2) - (dayIndex - 1) * _pairList.Count) + 1;

                        if (shedule.FlatHasLesson(key, beginDate.AddDays(dayIndex - 1), pairIndex))
                        {
                            // Text of group list for group cell
                            string groupsString = "";

                            // Text of subjects foк subjects cell
                            string subjectsString = "";

                            IList lessonList = shedule.GetFlatLesson(key, beginDate.AddDays(dayIndex - 1), pairIndex);

                            foreach (RemoteLesson2 remlsn in lessonList)
                            {
                                if (groupsString != "")
                                {
                                    lessonGroupCell.RowHeight = "30";
                                    lessonCell.RowHeight = "30";
                                }
                                Lesson2 lsn = RemoteAdapter.Adapter.AdaptLesson(remlsn);

                                // перехід на нову строку, у зв`язку з тим що проводится більше одного заняття
                                if (groupsString != "")
                                    groupsString += "\n";

                                foreach (Group gr in lsn.GroupList)
                                    if (gr != null)
                                        if (groupsString != "" && !groupsString.EndsWith("\n"))
                                            groupsString += ", " + gr.MnemoCode;
                                        else
                                            groupsString += gr.MnemoCode;
                                // перехід на нову строку, у зв`язку з тим що проводится більше одного заняття
                                if (subjectsString != "")
                                    subjectsString += "\n";

                                if (lsn.SybjectType != null)
                                    subjectsString += lsn.SybjectType.MnemoCode + ", ";
                                subjectsString += lsn.Subject.ShortName;

                            }

                            //	Range prevlessonCell = _excelSheet.Cells[rowindex - 1, ColumnStart] as Range;
                            //	Range prevlessonFlatCell = _excelSheet.Cells[rowindex - 1, ColumnStart - 1] as Range;

                            //	string prevsubjectsString = prevlessonCell.Value2 as string;
                            //	string prevlessonFlatCellString = prevlessonFlatCell.Value2 as string;
                            //
                            //	if ((rowindex - 2)%2 != 0)
                            //	if (prevsubjectsString == subjectsString && prevlessonFlatCellString == groupsString)
                            //	{
                            //	Range Cell;
                            //	Cell = _excelSheet.get_Range(_excelSheet.Cells[rowindex - 1, ColumnStart], _excelSheet.Cells[rowindex, ColumnStart]);
                            //	Cell.MergeCells = true;
                            //	Cell.Borders.get_Item(XlBordersIndex.xlEdgeRight).Weight = XlBorderWeight.xlMedium;
                            //
                            //	Cell = _excelSheet.get_Range(_excelSheet.Cells[rowindex - 1, ColumnStart - 1], _excelSheet.Cells[rowindex, ColumnStart - 1]);
                            //	Cell.MergeCells = true;
                            //	Cell.Borders.get_Item(XlBordersIndex.xlEdgeLeft).Weight = XlBorderWeight.xlMedium;
                            //	}
                            //	else
                            //	{
                            lessonGroupCell.Value2 = groupsString;
                            lessonCell.Value2 = subjectsString;
                            //	}
                            //	else
                            //	{
                            //	lessonGroupCell.Value2 = groupsString;
                            //	lessonCell.Value2 = subjectsString;
                            //	}
                        }


                        if ((rowindex - 1) % 6 == 0)
                            lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;
                        else
                            lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlMedium;

                        lessonCell.Borders.get_Item(XlBordersIndex.xlEdgeRight).Weight = XlBorderWeight.xlMedium;

                        if ((rowindex - 1) % 6 == 0)
                            lessonGroupCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;
                        else
                            lessonGroupCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlMedium;
                        lessonGroupCell.Borders.get_Item(XlBordersIndex.xlEdgeLeft).Weight = XlBorderWeight.xlMedium;
                    }

                    ColumnStart += 2;
                }

            #endregion

            EndExport();
        }


        public void ExportShortFlatShedule(string filename, IList flatList, IShedule shedule, ExcelExporterCallBack callback)
        {
            if (callback != null)
                callback("Запуск Excel", 0);

            BeginExport();

            if (callback != null)
                callback("Підготовка сторінки", 0);

            CreateTitles(this._excelSheet, 1);

            #region Выод расписания по аудиториям

            int ColumnStart = 3;
            int CheckedPos = 1;

            foreach (LectureHall lh in flatList)
                if (lh != null)
                {
                    //Вывод номера группы
                    Range range;
                    //	range = _excelSheet.Cells[1, ColumnStart - 1] as Range;
                    //	range.ColumnWidth = "9";

                    range = _excelSheet.Cells[1, ColumnStart] as Range;
                    range.ColumnWidth = "9";

                    //	range = _excelSheet.get_Range(_excelSheet.Cells[1, ColumnStart - 1], _excelSheet.Cells[1, ColumnStart]);
                    //	range.MergeCells = true;

                    range.Value2 = lh.MnemoCode + " " + lh.HallType.MnemoCode + "[" + lh.Capacity + "]";
                    range.Font.Size = "14";

                    range.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    range.BorderAround(1, XlBorderWeight.xlMedium, XlColorIndex.xlColorIndexAutomatic, Type.Missing);
                    range.Font.Bold = true;

                    //Вывод расписания
                    for (int rowindex = 2; rowindex < _dayList.Count * _pairList.Count * _weekList.Count + 2; rowindex++)
                    {
                        if (callback != null)
                            callback(lh.ToString(), CheckedPos);

                        CheckedPos++;

                        //Range lessonCell = _excelSheet.Cells[rowindex, ColumnStart] as Range;
                        Range lessonGroupCell = _excelSheet.Cells[rowindex, ColumnStart] as Range;

                        //	lessonGroupCell.Font.Size = "12";
                        //	lessonGroupCell.WrapText = true;
                        //	lessonGroupCell.VerticalAlignment = XlVAlign.xlVAlignCenter;
                        //	lessonGroupCell.HorizontalAlignment = XlVAlign.xlVAlignCenter;

                        lessonGroupCell.Font.Size = "12";
                        lessonGroupCell.Font.Bold = true;
                        lessonGroupCell.VerticalAlignment = XlVAlign.xlVAlignCenter;
                        lessonGroupCell.HorizontalAlignment = XlVAlign.xlVAlignCenter;
                        //lessonGroupCell.NumberFormat = "@";

                        lessonGroupCell.Value2 = shedule.GetFlatRestrictsText(lh.ObjectHardKey, rowindex - 2);

                        if (shedule.FlatHasLesson(lh.ObjectHardKey, rowindex - 2))
                        {
                            // Text of group list for group cell
                            string groupsString = "";

                            IList lessonList = shedule.GetFlatLesson(lh.ObjectHardKey, rowindex - 2);

                            foreach (RemoteLesson remlsn in lessonList)
                            {
                                if (groupsString != "")
                                {
                                    lessonGroupCell.RowHeight = "30";
                                    //lessonCell.RowHeight = "30";
                                }
                                Lesson lsn = RemoteAdapter.Adapter.AdaptLesson(remlsn);

                                // перехід на нову строку, у зв`язку з тим що проводится більше одного заняття
                                if (groupsString != "")
                                    groupsString += "\n";

                                foreach (Group gr in lsn.GroupList)
                                    if (gr != null)
                                        if (groupsString != "" && !groupsString.EndsWith("\n"))
                                            groupsString += ", " + gr.MnemoCode;
                                        else
                                            groupsString += gr.MnemoCode;
                            }

                            Range prevlessonFlatCell = _excelSheet.Cells[rowindex - 1, ColumnStart] as Range;
                            string prevlessonFlatCellString = prevlessonFlatCell.Value2 as string;

                            if ((rowindex - 2) % 2 != 0)
                                if (prevlessonFlatCellString == groupsString)
                                {
                                    Range Cell;
                                    Cell = _excelSheet.get_Range(_excelSheet.Cells[rowindex - 1, ColumnStart], _excelSheet.Cells[rowindex, ColumnStart]);
                                    try
                                    {
                                        Cell.MergeCells = true;
                                    }
                                    catch
                                    {
                                    }
                                    Cell.Borders.get_Item(XlBordersIndex.xlEdgeRight).Weight = XlBorderWeight.xlMedium;
                                }
                                else
                                {
                                    lessonGroupCell.Value2 = groupsString as string;
                                }
                            else
                            {
                                lessonGroupCell.Value2 = groupsString as string;
                            }
                        }

                        lessonGroupCell.Borders.get_Item(XlBordersIndex.xlEdgeRight).Weight = XlBorderWeight.xlMedium;

                        if ((rowindex - 2) % 12 != 0)
                            if ((rowindex) % 2 == 0)
                            {
                                lessonGroupCell.Borders.get_Item(XlBordersIndex.xlEdgeTop).Weight = XlBorderWeight.xlMedium;
                            }
                            else
                            {
                                Range lessonCellPred = _excelSheet.Cells[rowindex - 1, ColumnStart] as Range;
                                lessonCellPred.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThin;
                                lessonGroupCell.Interior.ColorIndex = 6;
                            }

                        if ((rowindex - 2) % 12 != 0)
                            if ((rowindex) % 2 == 0)
                            {
                                lessonGroupCell.Borders.get_Item(XlBordersIndex.xlEdgeTop).Weight = XlBorderWeight.xlMedium;
                            }

                        if ((rowindex - 1) % 12 == 0)
                            lessonGroupCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;
                        else
                            lessonGroupCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlMedium;
                        lessonGroupCell.Borders.get_Item(XlBordersIndex.xlEdgeLeft).Weight = XlBorderWeight.xlMedium;
                    }

                    ColumnStart++;
                }

            #endregion

            EndExport();
        }

        public void ExportShortFlatShedule2(string filename, IList flatList, IShedule2 shedule, IObjectStorage storage, ExcelExporterCallBack callback)
        {
            if (callback != null)
                callback("Запуск Excel", 0);

            BeginExport();

            if (callback != null)
                callback("Підготовка сторінки", 0);

            CreateTitles2(this._excelSheet, shedule, 1);

            #region Выод расписания по аудиториям

            int ColumnStart = 4;
            int CheckedPos = 1;

            foreach (LectureHall lh in flatList)
                if (lh != null)
                {
                    //Вывод номера группы
                    Range range;
                    //	range = _excelSheet.Cells[1, ColumnStart - 1] as Range;
                    //	range.ColumnWidth = "9";

                    range = _excelSheet.Cells[1, ColumnStart] as Range;
                    range.ColumnWidth = "9";

                    //	range = _excelSheet.get_Range(_excelSheet.Cells[1, ColumnStart - 1], _excelSheet.Cells[1, ColumnStart]);
                    //	range.MergeCells = true;

                    range.Value2 = lh.MnemoCode + " " + lh.HallType.MnemoCode + "[" + lh.Capacity + "]";
                    range.Font.Size = "14";

                    range.HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    range.BorderAround(1, XlBorderWeight.xlMedium, XlColorIndex.xlColorIndexAutomatic, Type.Missing);
                    range.Font.Bold = true;

                    DateTime beginDate = shedule.GetSheduleBeginDate();
                    DateTime endDate = shedule.GetSheduleEndDate();

                    //Вывод расписания
                    for (int rowindex = 2; rowindex < (endDate - beginDate).Days * _pairList.Count + 2; rowindex++)
                    {
                        if (callback != null)
                            callback(lh.ToString(), CheckedPos);

                        CheckedPos++;

                        //Range lessonCell = _excelSheet.Cells[rowindex, ColumnStart] as Range;
                        Range lessonGroupCell = _excelSheet.Cells[rowindex, ColumnStart] as Range;

                        //	lessonGroupCell.Font.Size = "12";
                        //	lessonGroupCell.WrapText = true;
                        //	lessonGroupCell.VerticalAlignment = XlVAlign.xlVAlignCenter;
                        //	lessonGroupCell.HorizontalAlignment = XlVAlign.xlVAlignCenter;

                        lessonGroupCell.Font.Size = "12";
                        lessonGroupCell.Font.Bold = true;
                        lessonGroupCell.VerticalAlignment = XlVAlign.xlVAlignCenter;
                        lessonGroupCell.HorizontalAlignment = XlVAlign.xlVAlignCenter;
                        lessonGroupCell.NumberFormat = "@";

                        ExtObjKey key = new ExtObjKey();
                        key.UID = storage.GetUUID(lh, out key.Signature, out key.Index);

                        int dayIndex = (rowindex - 2) / (_pairList.Count) + 1;
                        int pairIndex = ((rowindex - 2) - (dayIndex - 1) * _pairList.Count) + 1;

                        if (shedule.FlatHasLesson(key, beginDate.AddDays(dayIndex - 1), pairIndex))
                        {
                            // Text of group list for group cell
                            string groupsString = "";

                            IList lessonList = shedule.GetFlatLesson(key, beginDate.AddDays(dayIndex - 1), pairIndex);

                            foreach (RemoteLesson2 remlsn in lessonList)
                            {
                                if (groupsString != "")
                                {
                                    lessonGroupCell.RowHeight = "30";
                                    //lessonCell.RowHeight = "30";
                                }
                                Lesson2 lsn = RemoteAdapter.Adapter.AdaptLesson(remlsn);

                                // перехід на нову строку, у зв`язку з тим що проводится більше одного заняття
                                if (groupsString != "")
                                    groupsString += "\n";

                                foreach (Group gr in lsn.GroupList)
                                    if (gr != null)
                                        if (groupsString != "" && !groupsString.EndsWith("\n"))
                                            groupsString += ", " + gr.MnemoCode;
                                        else
                                            groupsString += gr.MnemoCode;
                            }

                            //	Range prevlessonFlatCell = _excelSheet.Cells[rowindex - 1, ColumnStart] as Range;	
                            //	string prevlessonFlatCellString = prevlessonFlatCell.Value2 as string;

                            //	if ((rowindex - 2)%2 != 0)
                            //	if (prevlessonFlatCellString == groupsString)
                            //	{
                            //	Range Cell;
                            //	Cell = _excelSheet.get_Range(_excelSheet.Cells[rowindex - 1, ColumnStart], _excelSheet.Cells[rowindex, ColumnStart]);
                            //	Cell.MergeCells = true;
                            //	Cell.Borders.get_Item(XlBordersIndex.xlEdgeRight).Weight = XlBorderWeight.xlMedium;
                            //	}
                            //	else
                            //	{
                            //	lessonGroupCell.Value2 = groupsString as string;
                            //	}
                            //	else
                            //	{
                            lessonGroupCell.Value2 = groupsString as string;
                            //	}
                        }

                        lessonGroupCell.Borders.get_Item(XlBordersIndex.xlEdgeRight).Weight = XlBorderWeight.xlMedium;

                        if ((rowindex - 1) % 6 == 0)
                            lessonGroupCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlThick;
                        else
                            lessonGroupCell.Borders.get_Item(XlBordersIndex.xlEdgeBottom).Weight = XlBorderWeight.xlMedium;
                        lessonGroupCell.Borders.get_Item(XlBordersIndex.xlEdgeLeft).Weight = XlBorderWeight.xlMedium;
                    }

                    ColumnStart++;
                }

            #endregion

            EndExport();
        }

        /// <summary>
        /// Окончание процедуры экспорта данных в MS Excel
        /// </summary>
        public void EndExport()
        {
            ShowExcel();
        }

        /// <summary>
        /// Запуск сервера MS Excel
        /// </summary>
        public void StartExcel()
        {
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            if (_excelApp == null)
                _excelApp = new ApplicationClass();

            Workbooks workbooks = _excelApp.Workbooks;

            // The following line is the temporary workaround for the LCID problem
            _workbook = workbooks.Add(/*XlWBATemplate.xlWBATWorksheet*/);

            Sheets sheets = _workbook.Worksheets;

            _excelSheet = (_Worksheet)sheets.get_Item(1);
            if (_excelSheet == null)
            {
            }
        }

        /// <summary>
        /// Показать запущенный сервер MS Excel
        /// </summary>
        public void ShowExcel()
        {
            _excelApp.Visible = true;
        }

    }
}