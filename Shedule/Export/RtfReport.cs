using System.Collections;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using Shturman.Nestor.Interfaces;
using Shturman.Shedule.Objects;
using Shturman.Shedule.Server.Interface;

namespace Shturman.Shedule.Export
{
	/// <summary>
	/// Summary description for RtfReport.
	/// </summary>
	public class RtfReport
	{
		private string _openfile;
		private IWin32Window _window;

		public RtfReport(IWin32Window window)
		{
			_window = window;
		}

		/// <summary>
		/// ��������� ���� �� ������������ ��������
		/// </summary>
		/// <param name="storage">������� �����</param>
		/// <param name="shedule">������� ������</param>
		public void FlatsLoadByBuilding(IObjectStorage storage, IShedule shedule)
		{
			SaveFileDialog savefdlg = new SaveFileDialog();
			savefdlg.Filter = "rtf (*.rtf)|*.rtf";
			if (savefdlg.ShowDialog() == DialogResult.OK)
			{
				StreamWriter sw = new StreamWriter(savefdlg.FileName, false, Encoding.GetEncoding(1251));
				// rtf write
				sw.WriteLine("{\\rtf1\\ansi\\ansicpg1252");

				//Write the title and author for the document properties
				sw.WriteLine("{\\info{\\title ���}{\\author �������-�������}}");
				//Write the page header and footer
				sw.WriteLine("{\\header\\pard\\qr{\\fs16 " +
					"��� ������� EDU}{\\par\\fs18 ���� ���������: \\chdate\\par}}");
				sw.WriteLine("{\\footer\\pard\\qr{\\brdrt\\brdrs\\brdrw10\\brsp100" +
					"\\fs16 ������� " +
					"{\\field{\\*\\fldinst PAGE}{\\fldrslt 1}} � " +
					"{\\field{\\*\\fldinst NUMPAGES}{\\fldrslt 1}}}}");
			
				IList columns = new ArrayList();
				columns.Add("�");
				columns.Add("���");
				columns.Add("̳������");
				columns.Add("�������������");
				columns.Add("������������");
            
				//Create the table header row (appears at top of table on each page)
				string tableColumnString = "\\trowd\\trhdr\\trgaph30\\trleft0\\trrh262\\trautofit1";
				string tableTitleString = "";
				int columnRightPos = 0;
				for( int i = 0; i< columns.Count; i++)
				{
					columnRightPos += 2500;
					tableColumnString += "\\cellx"+ columnRightPos.ToString();
					tableTitleString += "\\pard\\intbl\\qc\\b\\fs24 "+ columns[i]+ "\\b0\\par\\cell";
				}
				tableTitleString += "\\pard\\intbl\\row\\fs24";

				IList buildingList = storage.ObjectList(typeof(Building));
				
				foreach(Building build in buildingList)
					if (build != null)
					{
						sw.WriteLine("\\par\\b\\fs24 ������: "+build.Name+"\\b0\\par");
						//Start table
						sw.WriteLine("{");
						// write table header
						sw.WriteLine(tableColumnString + tableTitleString);
						// ������ ������� � ��������� ������� � ��������
						foreach(LectureHall lh in build.LectureHalls)
							if (lh != null)
							{
								//Add a new row 
								// Flat number
								string sRTF = "\\pard\\intbl\\qc "+ lh.MnemoCode +" \\cell";
								// Flat Type
								if (lh.HallType != null)
									sRTF += "\\pard\\intbl\\qc "+ lh.HallType.ToString() +" \\cell";
								else
									sRTF += "\\pard\\intbl\\qc  \\cell";
								// Flat Capacity
								sRTF +=	"\\pard\\intbl\\qc "+ lh.GroupsCapacity.ToString()+"["+lh.Capacity.ToString()+"] \\cell";
								// Flat Department
								if (lh.Department != null)
									sRTF += "\\pard\\intbl\\qc "+ lh.Department.ToString() +" \\cell";
								else
									sRTF += "\\pard\\intbl\\qc  \\cell";

								// Calculate using percent
								float _usingPercent = 0; 							
								int _lessonCount = shedule.GetFlatLessons(lh.ObjectHardKey).Count;
								switch(lh.HallType.MnemoCode)
								{
									case "�.":
										_usingPercent = (_lessonCount*100)/(shedule.GetWeeksCount()*5*/*shedule.GetDaysPerWeekCount()*shedule.GetPairsPerDayCount()*/3);
										break;
									default:
										_usingPercent = (_lessonCount*100)/(shedule.GetWeeksCount()*5*/*shedule.GetDaysPerWeekCount()*shedule.GetPairsPerDayCount()*/4);
										break;
								}
								//_usingPercent = (_lessonCount*100)/(shedule.GetWeeksCount()*5*/*shedule.GetDaysPerWeekCount()*shedule.GetPairsPerDayCount()*/4);								

								// Flat Using Percent
								sRTF += "\\pard\\intbl\\qc "+ _usingPercent.ToString() +"% \\cell" +								
									"\\pard\\intbl\\row\\fs24";
								sw.WriteLine(tableColumnString + sRTF);
							}
						// End Table
						sw.WriteLine("}");
					}
				// close the RTF string and file
				sw.WriteLine("}");

				sw.Flush();
				sw.Close();
					
				if (MessageBox.Show(_window, "³������ ���?", "�������", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					_openfile = savefdlg.FileName;
					Thread backgroundThread = new Thread( new ThreadStart(this.OpenFile));
					backgroundThread.Start();
				}
			}			

		}


		public void GroupLoad(IObjectStorage storage, IShedule shedule)
		{
			SaveFileDialog savefdlg = new SaveFileDialog();
			savefdlg.Filter = "rtf (*.rtf)|*.rtf";
			if (savefdlg.ShowDialog() == DialogResult.OK)
			{
				StreamWriter sw = new StreamWriter(savefdlg.FileName, false, Encoding.GetEncoding(1251));
				// rtf write
				sw.WriteLine("{\\rtf1\\ansi\\ansicpg1252");

				//Write the title and author for the document properties
				sw.WriteLine("{\\info{\\title ���}{\\author ������� EDU}}");
				//Write the page header and footer
				sw.WriteLine("{\\header\\pard\\qr{\\fs16 " +
					"��� ������� EDU}{\\par\\fs18 ���� ���������: \\chdate\\par}}");
				sw.WriteLine("{\\footer\\pard\\qr{\\brdrt\\brdrs\\brdrw10\\brsp100" +
					"\\fs16 ������� " +
					"{\\field{\\*\\fldinst PAGE}{\\fldrslt 1}} � " +
					"{\\field{\\*\\fldinst NUMPAGES}{\\fldrslt 1}}}}");
			
				IList columns = new ArrayList();
				columns.Add("�");				
				columns.Add("ʳ������ ��������");				
				columns.Add("������������, �����");
                columns.Add("������������, %");
            
				//Create the table header row (appears at top of table on each page)
				string tableColumnString = "\\trowd\\trhdr\\trgaph30\\trleft0\\trrh262\\trautofit1";
				string tableTitleString = "";
				int columnRightPos = 0;
				for( int i = 0; i< columns.Count; i++)
				{
					columnRightPos += 2500;
					tableColumnString += "\\cellx"+ columnRightPos.ToString();
					tableTitleString += "\\pard\\intbl\\qc\\b\\fs24 "+ columns[i]+ "\\b0\\par\\cell";
				}
				tableTitleString += "\\pard\\intbl\\row\\fs24";

				IList groupList = storage.ObjectList(typeof(Group));
				
						//Start table
						sw.WriteLine("{");
						// write table header
						sw.WriteLine(tableColumnString + tableTitleString);
						// ������ ������� � ��������� ������� � ��������
						float AvergUsingPercent = 0.0f;
						float AvergUsingHour = 0.0f;
						foreach(Group gr in groupList)
							if (gr != null)
							{
								//Add a new row 
								// Flat number
								string sRTF = "\\pard\\intbl\\qc "+ gr.MnemoCode +" \\cell";								
								sRTF += "\\pard\\intbl\\qc "+ gr.GroupCapacity +" \\cell";
								
								// Calculate hour in week
								int _lessonCount = shedule.GetGroupLessons(gr.ObjectHardKey).Count;
								float _usingHour = 0;
								_usingHour = _lessonCount;
								AvergUsingHour += _usingHour;
								sRTF += "\\pard\\intbl\\qc "+ _usingHour +" \\cell";

								// Calculate using percent
								float _usingPercent = 0;
								_usingPercent = (_lessonCount*100)/(shedule.GetWeeksCount()*shedule.GetDaysPerWeekCount()*(shedule.GetPairsPerDayCount()-2));
								AvergUsingPercent += _usingPercent;

								// Flat Using Percent
								sRTF += "\\pard\\intbl\\qc "+ _usingPercent.ToString() +"% \\cell" +								
									"\\pard\\intbl\\row\\fs24";
								sw.WriteLine(tableColumnString + sRTF);
							}
						// End Table
						sw.WriteLine("}");

				sw.WriteLine("\\par\\b\\fs24 �������: "+AvergUsingHour/groupList.Count +" ���./���. \\b0\\par");
				sw.WriteLine("\\par\\b\\fs24 �������: "+AvergUsingPercent/groupList.Count +" % \\b0\\par");
				
				// close the RTF string and file
				sw.WriteLine("}");

				sw.Flush();
				sw.Close();
					
				if (MessageBox.Show(_window, "³������ ���?", "�������", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					_openfile = savefdlg.FileName;
					Thread backgroundThread = new Thread( new ThreadStart(this.OpenFile));
					backgroundThread.Start();
				}
			}			

		}
		/// <summary>
		/// ������� ������ � dataGrid � ����� ������������ �������
		/// </summary>
		/// <param name="rtfGridCaption">��������� ������� � ������ rtf</param>
		/// <param name="dataGrid">dataGrid � ����� ����������� �����</param>
		public void LessonsDataGrid2Rtf(string rtfGridCaption, DataGrid dataGrid)
		{
			SaveFileDialog savefdlg = new SaveFileDialog();
			savefdlg.Filter = "rtf (*.rtf)|*.rtf";
			if (savefdlg.ShowDialog() == DialogResult.OK)
			{
				StreamWriter sw = new StreamWriter(savefdlg.FileName, false, Encoding.GetEncoding(1251));
				// rtf write
				sw.WriteLine("{\\rtf1\\ansi\\ansicpg1252");

				//Write the title and author for the document properties
				sw.WriteLine("{\\info{\\title ���}{\\author ������� EDU}}");
				//Write the page header and footer
				sw.WriteLine("{\\header\\pard\\qr{\\fs16 " +
					"��� ������� EDU}{\\par\\fs18 ���� ���������: \\chdate\\par}}");
				sw.WriteLine("{\\footer\\pard\\qr{\\brdrt\\brdrs\\brdrw10\\brsp100" +
					"\\fs16 ������� " +
					"{\\field{\\*\\fldinst PAGE}{\\fldrslt 1}} � " +
					"{\\field{\\*\\fldinst NUMPAGES}{\\fldrslt 1}}}}");

				sw.WriteLine(rtfGridCaption);

				//Start table
				sw.WriteLine("{");

				//Write the table header row (appears at top of table on each page)
				string RTF = "\\trowd\\trhdr\\trgaph30\\trleft0\\trrh262";//\\trautofit1";
				string sRTF = "";
				int columnRightPos = 0;
				for( int i = dataGrid.FirstVisibleColumn; i< dataGrid.VisibleColumnCount; i++)
				{
					columnRightPos += dataGrid.TableStyles[0].GridColumnStyles[i].Width * 20;
					RTF += "\\cellx"+ columnRightPos.ToString();
					sRTF += "\\pard\\intbl\\qc\\b\\fs24 "+ dataGrid.TableStyles[0].GridColumnStyles[i].HeaderText + "\\b0\\par\\cell";				
				}
				sRTF += "\\pard\\intbl\\row\\fs24";
			
				sw.WriteLine(RTF+sRTF);
			
				foreach(Lesson lsn in (dataGrid.DataSource as IList))
				{
					//Add a new row 
					sRTF = "\\pard\\intbl\\qc "+ lsn.CurLessonDay.ToString() +" \\cell" +
						"\\pard\\intbl\\qc "+ lsn.CurLessonPair.ToString() +" \\cell" +
						"\\pard\\intbl\\qc "+ lsn.CurLessonWeek.ToString() +" \\cell" +
						"\\pard\\intbl\\qc "+ lsn.Subject.ToString() +" \\cell" +
						"\\pard\\intbl\\qc "+ lsn.SybjectType.ToString() +" \\cell" +
						"\\pard\\intbl\\qc "+ lsn.GroupListString +" \\cell" +
						"\\pard\\intbl\\qc "+ lsn.FlatListString +" \\cell" +
						"\\pard\\intbl\\row\\fs24";
					sw.WriteLine(RTF + sRTF);
				}			

				// End Table
				sw.WriteLine("}");
				// close the RTF string and file
				sw.WriteLine("}");

				sw.Flush();
				sw.Close();
					
				if (MessageBox.Show(_window, "³������ ���?", "�������", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
				{
					_openfile = savefdlg.FileName;
					Thread backgroundThread = new Thread( new ThreadStart(this.OpenFile));
					backgroundThread.Start();
				}
			}			
		}

		
		/// <summary>
		/// ³������� ����������� ���� � �������� ���� �������������� �� �����������
		/// � ����������� ������
		/// </summary>
		public void OpenFile()
		{
			// open file in word					
			Process myProcess = new Process();            
			try
			{
				try
				{
					// Get the path that stores user documents.			
					myProcess.StartInfo.FileName = _openfile; 
					myProcess.StartInfo.Verb = "Open";						
					myProcess.Start();
				}
				catch
				{
					MessageBox.Show(_window, "������� �������� ����� "+ _openfile, "�������");
				}
			}
			finally
			{
				_openfile = "";
			}
		}
	}
}