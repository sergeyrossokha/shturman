using System;
using System.ComponentModel;
using System.Windows.Forms;

namespace Shturman.Shedule.Export
{
	/// <summary>
	/// Summary description for ProgressForm.
	/// </summary>
	public class ProgressForm : Form
	{
		private ProgressBar progressBar1;
		private Label label1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private Container components = null;

		public ProgressForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
this.progressBar1 = new System.Windows.Forms.ProgressBar();
this.label1 = new System.Windows.Forms.Label();
this.SuspendLayout();
// 
// progressBar1
// 
this.progressBar1.Location = new System.Drawing.Point(2, 4);
this.progressBar1.Name = "progressBar1";
this.progressBar1.Size = new System.Drawing.Size(342, 16);
this.progressBar1.Step = 1;
this.progressBar1.TabIndex = 0;
// 
// label1
// 
this.label1.Location = new System.Drawing.Point(352, 5);
this.label1.Name = "label1";
this.label1.Size = new System.Drawing.Size(35, 16);
this.label1.TabIndex = 2;
this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
// 
// ProgressForm
// 
this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
this.ClientSize = new System.Drawing.Size(392, 26);
this.Controls.Add(this.label1);
this.Controls.Add(this.progressBar1);
this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
this.Name = "ProgressForm";
this.ShowInTaskbar = false;
this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
this.TopMost = true;
this.ResumeLayout(false);

		}
		#endregion

		public int Position
		{
			get { return this.progressBar1.Value; }
		}

		public int MaxPosition
		{
			get { return this.progressBar1.Maximum; }
			set { this.progressBar1.Maximum = value; }
		}

        delegate void SetTextCallback(string text);
        delegate void SetPositionCallback(int position);
        delegate void SetPositionTextCallback(string text);


        public void SetPosition(int position)
        {
            if (this.progressBar1.InvokeRequired)
            {
                SetPositionCallback d = new SetPositionCallback(SetPosition);
                this.Invoke(d, new object[] { position });
            }
            else
            {
                this.progressBar1.Value = position;
            }
        }

        public void SetPositionText(string text)
        {
            if (this.label1.InvokeRequired)
            {
                SetPositionTextCallback d = new SetPositionTextCallback(SetPositionText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.label1.Text = text;
            }
        }

        public void SetText(string text)
        {
            if (this.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.Text = text;
            }
        }
	}
}
