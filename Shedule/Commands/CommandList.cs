using System;
using System.Collections.Generic;
using System.Text;

namespace Shturman.Shedule.Commands
{
    class CommandList<T> where T: ICommand
    {
        private List<T> _internalList;
        private int _capacity = 25;

        public CommandList(int capacity)
        {
            _internalList = new List<T>(capacity);
            _capacity = capacity;
        }

        public int Count
        {
            get { return _internalList.Count;  }
        }
        public T Head
        {
            get { return _internalList[0]; }
        }

        public void Push(T obj)
        {
            if (_internalList.Count == _capacity)
                _internalList.RemoveAt(_capacity - 1);

            _internalList.Insert(0, obj);
        }

        public void Clear()
        {
            _internalList.Clear();
        }

        public T Pop()
        {
            T retobj = _internalList[0];
            _internalList.RemoveAt(0);

            return retobj;
        }
    }
}
