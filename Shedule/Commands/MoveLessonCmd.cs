/* Shturman Commands */
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using Shturman.Shedule.Objects;
using Shturman.Shedule.Server.Interface;
using Shturman.Shedule.Server.Remoting;

namespace Shturman.Shedule.Commands
{
    class MoveLessonCmd : ICommand
    {
        RemoteLesson _lesson;

        IList _flatList;
        IList _flatList_old;

        StudyLoading _sl;
        
        StudyCycles _sc;
        StudyHour _sh;
        StudyDay _sd;

        ExtObjKey _sc_old, _sh_old, _sd_old;
        int _sc_index_old, _sh_index_old, _sd_index_old;

        public MoveLessonCmd(RemoteLesson lesson,  StudyLoading sl, IList flats, StudyCycles sc, StudyHour sh, StudyDay sd)
        {
            _lesson = lesson;
            _flatList = flats;
            _flatList_old = new ArrayList(lesson.flatList);
            _sl = sl;
            _sc = sc;
            _sh = sh;
            _sd = sd;
        }

        public bool Do(IShedule shedule)
        {
            if (BeforeMove != null)
                foreach (BeforeInsertLesson bil in BeforeMove.GetInvocationList())
                    if (bil.Invoke(shedule, _sd, _sc, _sh, _sl, _flatList) == false)
                        return false;

            /*Main Part of Function*/
            _sc_old = _lesson.week_key;
            _sh_old = _lesson.hour_key;
            _sd_old = _lesson.day_key;

            _sc_index_old = _lesson.week_index;
            _sh_index_old = _lesson.hour_index;
            _sd_index_old = _lesson.day_index;

            _lesson.week_key = _sc.ObjectHardKey; _lesson.day_key = _sd.ObjectHardKey; _lesson.hour_key = _sh.ObjectHardKey;
            _lesson.week_uid = _lesson.week_key.UID; _lesson.day_uid = _lesson.day_key.UID; _lesson.hour_uid = _lesson.hour_key.UID;

            _lesson.flatList.Clear();
            foreach(object obj in _flatList)
                _lesson.flatList.Add(obj);

            shedule.MoveLesson(_lesson, int.Parse(_sc.MnemoCode), int.Parse(_sd.MnemoCode), int.Parse(_sh.MnemoCode));

            _lesson.week_index = int.Parse(_sc.MnemoCode);
            _lesson.day_index = int.Parse(_sd.MnemoCode);
            _lesson.hour_index = int.Parse(_sh.MnemoCode);

            if (AfterMove != null)
                foreach (AfterInsertLesson ail in AfterMove.GetInvocationList())
                    ail.Invoke(shedule, RemoteAdapter.Adapter.AdaptLesson(_lesson));

            if (OnDo != null)
                foreach (AfterDo ad in OnDo.GetInvocationList())
                    ad.Invoke();

            return true;
        }

        public void UnDo(IShedule shedule)
        {
            _lesson.week_key = _sc_old;
            _lesson.day_key = _sd_old;
            _lesson.hour_key = _sh_old;

            _lesson.week_uid = _lesson.week_key.UID;
            _lesson.day_uid = _lesson.day_key.UID;
            _lesson.hour_uid = _lesson.hour_key.UID;

            _lesson.flatList.Clear();
            foreach (object obj in _flatList_old)
                _lesson.flatList.Add(obj);

            shedule.MoveLesson(_lesson, _sc_index_old, _sd_index_old, _sh_index_old);

            _lesson.week_index = _sc_index_old;
            _lesson.day_index = _sd_index_old;
            _lesson.hour_index = _sh_index_old;

            if (OnUndo != null)
                foreach (AfterUndo aud in OnUndo.GetInvocationList())
                    aud.Invoke();
        }

        public event BeforeInsertLesson BeforeMove;
        public event AfterInsertLesson AfterMove;
        public event AfterDo OnDo;
        public event AfterUndo OnUndo;
    }
}
