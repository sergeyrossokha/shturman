using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

using Shturman.Shedule.Objects;
using Shturman.Shedule.Server.Interface;
using Shturman.Shedule.Server.Remoting;

namespace Shturman.Shedule.Commands
{
    public delegate bool BeforeUnDo(IShedule shedule, RemoteLesson remoteLesson);

    class RemoveLessonCmd : ICommand
    {
        private int _sd;
        private int _sc;
        private int _sh;

        private IList _groups;
        
        private List<RemoteLesson> _deletedLessons = new List<RemoteLesson>();

        public RemoveLessonCmd(int sd, int sc, int sh, IList groups)
        {
            _sd = sd;
            _sc = sc;
            _sh = sh;

            _groups = groups;
        }

        public bool Do(IShedule shedule)
        {
            IList groups = new ArrayList();

            int _shedulekey = shedule.GetKey(_sc, _sd, _sh);

            try
            {
                foreach (Group gr in _groups)
                    if (gr != null)
                    {
                        ExtObjKey objKey = gr.ObjectHardKey;

                        groups.Add(objKey);
                        RemoteLesson remoteLesson = shedule.GetGroupLesson(objKey, _shedulekey);

                        if (!_deletedLessons.Contains(remoteLesson))
                        {
                            _deletedLessons.Add(remoteLesson);

                            shedule.DeleteByGroup(_sc, _sd, _sh, groups);
                        }
                    }

                if (OnDo != null)
                    foreach (AfterDo ad in OnDo.GetInvocationList())
                        ad.Invoke();
                
                return true;
            }
            catch
            {
                return false;
            }
        }
        
        public void UnDo(IShedule shedule)
        {
            foreach (RemoteLesson remoteLesson in _deletedLessons)
            {
                if (CanUndo != null)
                    foreach (BeforeUnDo bud in CanUndo.GetInvocationList())
                        bud.Invoke(shedule, remoteLesson);

                if (shedule.Insert(remoteLesson.week_index, remoteLesson.day_index, remoteLesson.hour_index, remoteLesson) != -1)
                {
                    if (OnUndo != null)
                        foreach (AfterUndo aud in OnUndo.GetInvocationList())
                            aud.Invoke();
                }
            }

            _deletedLessons.Clear();
        }

        public event AfterDo OnDo;
        public event AfterUndo OnUndo;
        public event BeforeUnDo CanUndo;
    }
}
