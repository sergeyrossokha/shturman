using System.Collections;

namespace Shturman.Shedule
{
	/// <summary>
	/// Summary description for ShedulePosition.
	/// </summary>
	public class ShedulePosition
	{

		public ShedulePosition(int weekCycle, int weekDay, int dayPair)
		{
			WeekCycle = weekCycle;
			WeekDay = weekDay;
			DayPair = dayPair;
		}

		public int WeekCycle = -1;
		public int WeekDay = -1;
		public int DayPair = -1;
        public string Text = "";

		public IList Columns = new ArrayList();

		public override int GetHashCode()
		{
			return WeekCycle^WeekDay^DayPair;
		}

		public override bool Equals(object obj)
		{
			if (obj is ShedulePosition)
				return (((obj as ShedulePosition).WeekCycle == this.WeekCycle)&&((obj as ShedulePosition).WeekDay == this.WeekDay)&&((obj as ShedulePosition).DayPair == this.DayPair));
			else 
				return base.Equals(obj);
		}		
	}
}
