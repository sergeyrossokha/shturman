using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Shturman.Shedule.Objects;
using Shturman.Shedule;
using Shturman.Shedule.Server.Interface;
using Shturman.Shedule.Server.Remoting;
using SourceGrid;
using SourceGrid.Cells;
using SourceGrid.Cells.Views;
using Common = SourceGrid.Cells.Views.Cell;
using FlatHeader = SourceGrid.Cells.Views.Header;

namespace Shturman.Shedule.GUI
{
	/// <summary>
	/// Summary description for TutorSheduleView.
	/// </summary>
	public class SheduleView : UserControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>		
		private Grid grid;
		private IView EmptyView = new RestrictCellModel();
        private IView weekView = new WeekHeaderModel();
        private IView HeaderView = new FlatHeader();

        private IView dayView;
        private IView hourView;
        private IView titleView;

		private double _scale = 1;
		
		private IShedule sheduleMatrix;
		private IList objectList;

		private Container components = null;

		public SheduleView()
		{
			///
			/// Required for Windows.Forms Class Composition Designer support
			///
			InitializeComponent();

			grid.Selection.FocusStyle = FocusStyle.None;
			
            IView defaultView = new CustomHeaderModel();
			defaultView.TextAlignment = DevAge.Drawing.ContentAlignment.MiddleCenter;
			defaultView.Font = grid.Font.Clone() as Font;

			dayView = defaultView.Clone() as IView;

            (dayView as FlatHeader).ElementText = new DevAge.Drawing.VisualElements.TextGDI();
            (dayView as CustomHeaderModel).FormatFlags = StringFormatFlags.DirectionVertical | StringFormatFlags.NoWrap;


			hourView = (defaultView.Clone() as IView);
			
            titleView = new CustomHeaderModel();
            titleView.TextAlignment = DevAge.Drawing.ContentAlignment.MiddleCenter;
            (titleView as CustomHeaderModel).ChangeDefaultColor(System.Drawing.SystemColors.Control);
            titleView.Font = grid.Font.Clone() as Font;
		}

		public void RefreshShedule()
		{
			for(int colindex = 3; colindex<grid.Columns.Count; colindex++ )			
				for (int rowindex=1; rowindex<grid.RowsCount-1; rowindex++ )
				{
					RemoteLesson remoteLesson = sheduleMatrix.GetGroupLesson((objectList[colindex-3] as Group).ObjectHardKey, rowindex-1);
					if (remoteLesson != null)						
					{							
						string lessonstr = RemoteAdapter.Adapter.AdaptLesson(remoteLesson).GroupViewString;
						grid[rowindex, colindex].Value = lessonstr;						
						continue;
					}
                    else					
					    grid[rowindex, colindex].Value = "";

					grid.Rows[rowindex].Height = (int)Math.Round( (double) 25 * _scale ); // * ������ _scale
				}
		}

		public void ClearShedule()
		{
			grid.Redim(grid.RowsCount, 3);
		}

		public void ShowShedule(IList objectList, IShedule shmatr)
		{
			this.objectList = objectList;
			sheduleMatrix = shmatr;			

			ClearShedule();

			IView EmptyView = new RestrictCellModel();
			IView FlatHeaderView = new FlatHeader();

			foreach(object gr in objectList)
			{
				int currentColumn = grid.Columns.Count;
				grid.Columns.Insert(currentColumn);

                grid[0, grid.Columns.Count-1] = new SourceGrid.Cells.Cell(gr.ToString(), typeof(string));
                grid[0, grid.Columns.Count - 1].View = titleView;
                grid[0, grid.Columns.Count-1].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);
                grid[0, grid.Columns.Count-1].AddController(SourceGrid.Cells.Controllers.Resizable.ResizeWidth);

                grid.Columns[grid.Columns.Count-1].Width = (int)Math.Round( (double) 150 * _scale );	// * ������
				
				for (int rowindex=1; rowindex<grid.RowsCount-1; rowindex++ )
				{
					if ( gr is Group)
					{
						RemoteLesson remoteLesson = sheduleMatrix.GetGroupLesson((gr as Group).ObjectHardKey, rowindex-1);
						if (remoteLesson != null)							
						{							
							string lessonstr = RemoteAdapter.Adapter.AdaptLesson(remoteLesson).GroupViewString;
							// �������� ��������� �� ����
                            if (sheduleMatrix.GroupBusyCheckBoolRestrict(rowindex - 1, (gr as Group).ObjectHardKey))
                            {
                                grid[rowindex, currentColumn] = new SourceGrid.Cells.Cell(lessonstr, typeof(string));
                                grid[rowindex, currentColumn].View = new RestrictCellModel();

                                (grid[rowindex, currentColumn].View as RestrictCellModel).HaveConstraint = true;
                                // ToolTip Text
                                grid[rowindex, currentColumn].ToolTipText = shmatr.GetGroupRestrictsText((gr as Group).ObjectHardKey, rowindex - 1);
                            }
                            else
                            {
                                grid[rowindex, currentColumn] = new SourceGrid.Cells.Cell(lessonstr, typeof(string));
                                grid[rowindex, currentColumn].View = EmptyView;
                            }
							
							grid.Rows[rowindex].Height = (int)Math.Round( (double) 25 * _scale );// * ������
							continue;
						}
					}
					if ( gr is Tutor)
					{
						RemoteLesson remoteLesson = shmatr.GetTutorLesson((gr as Tutor).ObjectHardKey, rowindex-1);
						if (remoteLesson != null)
						{
							string lessonstr = RemoteAdapter.Adapter.AdaptLesson(remoteLesson).TutorViewString;
                            if (sheduleMatrix.TutorBusyCheckBoolRestrict(rowindex - 1, (gr as Tutor).ObjectHardKey))
                            {
                                grid[rowindex, currentColumn] = new SourceGrid.Cells.Cell(lessonstr, typeof(string));
                                grid[rowindex, currentColumn].View = new RestrictCellModel();

                                (grid[rowindex, currentColumn].View as RestrictCellModel).HaveConstraint = true;
                                // ToolTip Text
                                grid[rowindex, currentColumn].ToolTipText = shmatr.GetTutorRestrictsText((gr as Tutor).ObjectHardKey, rowindex - 1);
                            }
                            else
                            {
                                grid[rowindex, currentColumn] = new SourceGrid.Cells.Cell(lessonstr, typeof(string));
                                grid[rowindex, currentColumn].View = EmptyView;
                            }

							grid.Rows[rowindex].Height = (int)Math.Round( (double) 25 * _scale );// * ������
							continue;
						}
						if(sheduleMatrix.IsTutorHasRestricts((gr as Tutor).ObjectHardKey, rowindex-1))
						{
							string lessonstr = shmatr.GetTutorRestrictsText((gr as Tutor).ObjectHardKey, rowindex-1);
							if (lessonstr != string.Empty && lessonstr[0] == '#')
							{   
                                grid[rowindex, currentColumn] = new SourceGrid.Cells.Cell(lessonstr.Substring(1,lessonstr.Length-1), typeof(string));
                                grid[rowindex, currentColumn].View = new RestrictCellModel();
								continue;
							}
						}
					}

					if ( gr is LectureHall)
					{
						IList lessons = sheduleMatrix.GetFlatLesson((gr as LectureHall).ObjectHardKey, rowindex-1);
						if (lessons != null && lessons.Count>0)							
						{
							int lectureColumn = currentColumn;							
							foreach(RemoteLesson remlsn in lessons)
							{
								string lessonstr = RemoteAdapter.Adapter.AdaptLesson(remlsn).FlatViewString;

                                if (sheduleMatrix.FlatBusyCheckBoolRestrict(rowindex - 1, (gr as LectureHall).ObjectHardKey))
                                {
                                    grid[rowindex, lectureColumn] = new SourceGrid.Cells.Cell(lessonstr, typeof(string));
                                    grid[rowindex, lectureColumn].View = new RestrictCellModel();
                                    (grid[rowindex, lectureColumn].View as RestrictCellModel).HaveConstraint = true;

                                    grid[rowindex, lectureColumn].ToolTipText = shmatr.GetFlatRestrictsText((gr as LectureHall).ObjectHardKey, rowindex - 1);
                                }
                                else
                                {
                                    grid[rowindex, lectureColumn] = new SourceGrid.Cells.Cell(lessonstr, typeof(string));
                                    grid[rowindex, lectureColumn].View = EmptyView;

                                }
								grid.Rows[rowindex].Height = (int)Math.Round( (double) 25 * _scale );// * ������
									
								if (lessons.IndexOf(remlsn) != (lessons.Count-1))
								{
									if (lectureColumn+1 == grid.Columns.Count )
									{
										grid.Columns.Insert(grid.Columns.Count);
										grid.Columns[grid.Columns.Count-1].Width = (int)Math.Round( (double) 150 * _scale );	// * ������
									}
									lectureColumn ++;
								}								
							}
							continue;						
						}		
						else
							if(sheduleMatrix.IsFlatHasRestricts((gr as LectureHall).ObjectHardKey, rowindex-1))
							{
								string lessonstr = shmatr.GetFlatRestrictsText((gr as LectureHall).ObjectHardKey, rowindex-1);

                                if (lessonstr.Length > 1)
                                {
                                    grid[rowindex, currentColumn] = new SourceGrid.Cells.Cell(lessonstr.Substring(1, lessonstr.Length - 1), typeof(string));
                                    grid[rowindex, currentColumn].View = new RestrictCellModel();
                                }
                                else
                                {
                                    grid[rowindex, currentColumn] = new SourceGrid.Cells.Cell(lessonstr, typeof(string));
                                    grid[rowindex, currentColumn].View = new RestrictCellModel();
                                }

								continue;
							}
					}
                    grid[rowindex, currentColumn] = new SourceGrid.Cells.Cell("", typeof(string));
                    grid[rowindex, currentColumn].View = EmptyView;

					grid.Rows[rowindex].Height = (int)Math.Round( (double) 25 * _scale );// * ������
				}
			}
		}

		public void InitSheduleGrid(IList dayList, IList hourList, IList weekList)
		{
            int dayCount = dayList.Count;
            int hourCount = hourList.Count;
            int weekCount = weekList.Count;

            Grid.MaxSpan = 100;
            //
            // Grid regim 2 - weeks, 6- days, 6 - hours, 5 - group
            //
            grid.Redim(2+ dayCount * hourCount * weekCount, 3);

            /*grid[0, 0] = new SourceGrid.Cells.Cell("", typeof(string));
            grid[0, 0].View = titleView;
            grid[0, 0].RowSpan = 1;
            grid[0, 0].ColumnSpan = 8;
            grid[0, 0].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);

            //���������� ������� ������� � ����
            grid.Columns[0].Width = 20;
            grid.Columns[1].Width = 20;
            grid.Columns[2].Width = 20;
            grid.Columns[3].Width = (int)Math.Round((double)150 * _scale);
            grid.Columns[4].Width = 20;
            grid.Columns[5].Width = 20;
            grid.Columns[6].Width = 20;
            grid.Columns[7].Width = (int)Math.Round((double)150 * _scale);

            int dayindex = 1;
            for (dayindex = 1; dayindex < 3 * hourCount * weekCount; dayindex += hourCount * weekCount)
            {
                grid[dayindex, 0] = new SourceGrid.Cells.Cell(dayList[(dayindex - 1) / (hourCount * weekCount)].ToString(), typeof(string));
                grid[dayindex, 0].View = dayView;

                grid[dayindex, 0].RowSpan = hourCount * weekCount;
                grid[dayindex, 0].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);

                for (int hourindex = dayindex; hourindex < (dayindex + hourCount * weekCount); hourindex += weekCount)
                {
                    grid[hourindex, 1] = new SourceGrid.Cells.Cell(hourList[(hourindex - dayindex) / (weekCount)].ToString(), typeof(string));
                    grid[hourindex, 1].View = hourView;
                    grid[hourindex, 1].RowSpan = 2;

                    grid[hourindex, 1].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);

                    for (int weekindex = hourindex; weekindex < (hourindex + 2); weekindex++)
                    {
                        grid[weekindex, 2] = new SourceGrid.Cells.Cell(weekList[(weekindex - hourindex)].ToString(), typeof(string));
                        grid[weekindex, 2].View = weekView;
                        grid[weekindex, 2].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);
                    }
                }
            }

            int lastSttop = dayindex;

            for (dayindex = lastSttop+1; dayindex < dayCount * hourCount * weekCount; dayindex += hourCount * weekCount)
            {

                int dayindex2 = dayindex - lastSttop;
                grid[dayindex2, 4] = new SourceGrid.Cells.Cell(dayList[(dayindex - 1) / (hourCount * weekCount)].ToString(), typeof(string));
                grid[dayindex2, 4].View = dayView;

                grid[dayindex2, 4].RowSpan = hourCount * weekCount;
                grid[dayindex2, 4].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);

                for (int hourindex = dayindex; hourindex < (dayindex + hourCount * weekCount); hourindex += weekCount)
                {

                    grid[hourindex - lastSttop, 5] = new SourceGrid.Cells.Cell(hourList[(hourindex - dayindex) / (weekCount)].ToString(), typeof(string));
                    grid[hourindex-lastSttop, 5].View = hourView;
                    grid[hourindex - lastSttop, 5].RowSpan = 2;

                    grid[hourindex - lastSttop, 5].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);

                    for (int weekindex = hourindex2; weekindex < (hourindex + 2); weekindex++)
                    {
                        grid[weekindex - lastSttop, 6] = new SourceGrid.Cells.Cell(weekList[(weekindex - hourindex)].ToString(), typeof(string));
                        grid[weekindex- lastSttop, 6].View = weekView;
                        grid[weekindex, 6].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);
                    }
                }
            }
            */

			//
			// Load grid header
			//
            grid[0, 0] = new SourceGrid.Cells.Cell("", typeof(string));
            grid[0, 0].View = titleView;
			grid[0,0].RowSpan =1;
			grid[0,0].ColumnSpan = 3;
            grid[0, 0].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);
			//
			// Create grid day caption
			//
			for (int dayindex=1; dayindex< grid.RowsCount-1; dayindex+= hourCount*weekCount )
			{
				grid.Columns[0].Width = 20; // * ������
				grid.Columns[1].Width = 20; // * ������
				grid.Columns[2].Width = 20; // * ������

                grid[dayindex, 0] = new SourceGrid.Cells.Cell(dayList[(dayindex-1)/(hourCount*weekCount)].ToString(), typeof(string));
                grid[dayindex, 0].View = dayView;

				grid[dayindex,0].RowSpan = hourCount*weekCount;
                grid[dayindex, 0].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);

				for(int hourindex=dayindex; hourindex<(dayindex+hourCount*weekCount); hourindex+=weekCount )
				{
                    grid[hourindex, 1] = new SourceGrid.Cells.Cell(hourList[(hourindex-dayindex)/(weekCount)].ToString(), typeof(string));
                    grid[hourindex, 1].View = hourView;
					grid[hourindex, 1].RowSpan = 2;

                    grid[hourindex, 1].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);
					
					for(int weekindex = hourindex; weekindex<(hourindex+2); weekindex++)
					{
                        grid[weekindex, 2] = new SourceGrid.Cells.Cell(weekList[(weekindex-hourindex)].ToString(), typeof(string));
                        grid[weekindex, 2].View = weekView;
                        grid[weekindex, 2].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);
					}
				}
			}

			grid.FixedColumns = 3;
			grid.FixedRows = 1;
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.grid = new SourceGrid.Grid();
			this.SuspendLayout();
			// 
			// grid
			// 
			this.grid.AutoStretchColumnsToFitWidth = false;
			this.grid.AutoStretchRowsToFitHeight = false;
			this.grid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.grid.CustomSort = false;
			this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
			this.grid.Location = new System.Drawing.Point(0, 0);
			this.grid.Name = "grid";
			this.grid.Size = new System.Drawing.Size(360, 340);
			this.grid.SpecialKeys = SourceGrid.GridSpecialKeys.Default;
			this.grid.TabIndex = 0;

			this.grid.VScrollPositionChanged+=new ScrollPositionChangedEventHandler(sheduleGrid_VScrollPositionChanged);

			// 
			// GroupSheduleGrid
			// 
			this.Controls.Add(this.grid);
			this.Name = "SheduleGrid";
			this.Size = new System.Drawing.Size(360, 340);
			this.ResumeLayout(false);
		}
		#endregion

		public IList getSheduleSelPositions()
		{
			IList shposcol = new ArrayList();
			PositionCollection  poscol = this.grid.Selection.GetSelectionRegion().GetCellsPositions();
			foreach (Position pos in poscol )
			{
				int day = (pos.Row-1) / (sheduleMatrix.GetPairsPerDayCount()*sheduleMatrix.GetWeeksCount()) + 1;
				int pair = ((pos.Row-1) - (day-1)*sheduleMatrix.GetPairsPerDayCount()*sheduleMatrix.GetWeeksCount())/sheduleMatrix.GetWeeksCount()+1;
				int week = (pos.Row-1) % sheduleMatrix.GetWeeksCount() + 1 ;

				ShedulePosition shpos = new ShedulePosition(week, day, pair);
				
				if (shposcol.Contains(shpos))
				{
					shpos = shposcol [shposcol.IndexOf(shpos)] as ShedulePosition;
					if (!shpos.Columns.Contains(objectList[pos.Column-3]))
						shpos.Columns.Add(objectList[pos.Column-3]);
				}
				else 
				{
					shposcol.Add( shpos );
					if (!shpos.Columns.Contains(objectList[pos.Column-3]))
						shpos.Columns.Add(objectList[pos.Column-3]);
				}
			}
			return shposcol;
		}

		public int GetShedulePosition(int x, int y, out int week, out int day, out int pair)
		{
			Point _clientPoint = grid.PointToClient(new Point(x,y));			
			Position _position = grid.PositionAtPoint(_clientPoint);
			
			day = (_position.Row-1) / (sheduleMatrix.GetPairsPerDayCount()*sheduleMatrix.GetWeeksCount()) + 1;
			pair = ((_position.Row-1) - (day-1)*sheduleMatrix.GetPairsPerDayCount()*sheduleMatrix.GetWeeksCount())/sheduleMatrix.GetWeeksCount()+1;
			week = (_position.Row-1) % sheduleMatrix.GetWeeksCount() + 1 ;
			
			if (_position.Row > 0 && _position.Column > 2 ) return 1;
			else return 0;
		}


		public void Scale(int scaleTo)
		{	
			if (objectList == null)
				return;

			_scale = scaleTo/100.0;
			/* ������������ ����� ����� */
			Font newFont = new Font( grid.Font.FontFamily, (float)(this.Font.Size * _scale));
			grid.Font = newFont;
			if (weekView.Font != null)
				weekView.Font = newFont;
			if (dayView.Font != null)
				dayView.Font = newFont;
			if (hourView.Font != null)
				hourView.Font = newFont;
			if (HeaderView.Font != null)
				HeaderView.Font = newFont;
			
			/* ������� ����� �������� ���� */
			for(int colindex = 0; colindex<grid.Columns.Count; colindex++ )			
			{				
				for (int rowindex=0; rowindex<grid.RowsCount-1; rowindex++ )
				if(grid[rowindex, colindex] != null)
				{				
					grid[rowindex, colindex].View.Font = newFont;							
					// ������
					grid.Rows[rowindex].Height = (int)Math.Round( (double) 25 * _scale ); 
				}
				// ������
				if (colindex > 2)
					grid.Columns[colindex].Width = (int)Math.Round( (double) 150 * _scale );
				else
					grid.Columns[colindex].Width = (int)Math.Round( (double) 20 * _scale );
			}
		}

		public void SetFocus(int day, int week, int pair)
		{
			int cellIndex = (day-1)*(sheduleMatrix.GetPairsPerDayCount()*sheduleMatrix.GetWeeksCount())+ pair*sheduleMatrix.GetWeeksCount() + week -2;
			grid.Selection.SelectCell(new Position (cellIndex, 3), true);
		}

		public void SetFocus(int column)
		{
			grid.Selection.SelectCell(new Position( column+3, 2), true);
		}

		public void ToHTML()
		{
			string l_Path = System.IO.Path.Combine(System.IO.Path.GetTempPath(), "tmpSourceGridExport.htm");

			using (System.IO.FileStream l_Stream = new System.IO.FileStream(l_Path,System.IO.FileMode.Create,System.IO.FileAccess.Write))
			{
                new SourceGrid.Exporter.HTML(SourceGrid.Exporter.ExportHTMLMode.Default, "", "", l_Stream).Export(this.grid);
                l_Stream.Close();
			}
            DevAge.Shell.Utilities.OpenFile(l_Path); 
		}

		private void sheduleGrid_VScrollPositionChanged(object sender, ScrollPositionChangedEventArgs e)
		{
            //???
			/*if (_setFocusDo)
				if (grid.Selection.fo FocusCell != null)
					if (e.OldValue > e.NewValue)
					{
						int day = (grid.Selection.ceFocusCell.Row-1) / (sheduleMatrix.GetPairsPerDayCount()*sheduleMatrix.GetWeeksCount()) + 1;
						int pair = ((grid.FocusCell.Row-1) - (day-1)*sheduleMatrix.GetPairsPerDayCount()*sheduleMatrix.GetWeeksCount())/sheduleMatrix.GetWeeksCount()+1;
						grid.ShowCell(grid[grid.FocusCell.Row+ (sheduleMatrix.GetPairsPerDayCount()-pair)*sheduleMatrix.GetWeeksCount()+1, 3]);
					}
					else
					{
						int day = (grid.FocusCell.Row-1) / (sheduleMatrix.GetPairsPerDayCount()*sheduleMatrix.GetWeeksCount()) + 1;
						int pair = ((grid.FocusCell.Row-1) - (day-1)*sheduleMatrix.GetPairsPerDayCount()*sheduleMatrix.GetWeeksCount())/sheduleMatrix.GetWeeksCount()+1;
						grid.ShowCell(grid[grid.FocusCell.Row + (sheduleMatrix.GetPairsPerDayCount()-pair)*sheduleMatrix.GetWeeksCount()+1, 3]);
					}
             * */
			//_setFocusDo = false;
		}
	}
}
