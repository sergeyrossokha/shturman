using System.Drawing;

using SourceGrid;
using SourceGrid.Cells;
using SourceGrid.Cells.Views;
using Common = SourceGrid.Cells.Views.Cell;
using FlatHeader = SourceGrid.Cells.Views.Header;

namespace Shturman.Shedule.GUI
{
    // �������� ���������
    public delegate Color GetFlatHeaderColor(int flatIndex);
    public delegate Color GetCellColor(int flatIndex, int day);

    // �������� ������� ���������� �� ������������� ����� � �����
    public class RestrictCellModel : Common
    {
        #region Constructors
        public RestrictCellModel()
            : base()
        {
            _DefaultBackGround.BackColor = Color.Snow;
            _ZnamenatBackGround.BackColor = Color.LightGoldenrodYellow;
            _ConstraintBackGround.BackColor = Color.Purple;
            _IsBusyBackGround.BackColor = Color.Red;

            _DefaultBackGround.BackgroundColorStyle = DevAge.Drawing.BackgroundColorStyle.Solid;
            _ZnamenatBackGround.BackgroundColorStyle = DevAge.Drawing.BackgroundColorStyle.Solid;
            _ConstraintBackGround.BackgroundColorStyle = DevAge.Drawing.BackgroundColorStyle.Solid;
            _IsBusyBackGround.BackgroundColorStyle = DevAge.Drawing.BackgroundColorStyle.Solid;

            _DefaultBackGround.Border = new DevAge.Drawing.RectangleBorder(ViewBase.DefaultBorder.Left);
            _ZnamenatBackGround.Border = new DevAge.Drawing.RectangleBorder(ViewBase.DefaultBorder.Left);
            _ConstraintBackGround.Border = new DevAge.Drawing.RectangleBorder(ViewBase.DefaultBorder.Left);
            _IsBusyBackGround.Border = new DevAge.Drawing.RectangleBorder(ViewBase.DefaultBorder.Left);
        }

        public RestrictCellModel(GetCellColor getCellColor)
            : this()
        {
            GetCellColorEvent = getCellColor;
        }

        public RestrictCellModel(RestrictCellModel p_Source)
            : base(p_Source)
        {
            _DefaultBackGround.BackColor = p_Source.BackColor;
            _ZnamenatBackGround.BackColor = p_Source.BackColor2;
            _ConstraintBackGround.BackColor = p_Source.ConstraintColor;
            _IsBusyBackGround.BackColor = p_Source.BusyColor;

            _DefaultBackGround.BackgroundColorStyle = DevAge.Drawing.BackgroundColorStyle.Solid;
            _ZnamenatBackGround.BackgroundColorStyle = DevAge.Drawing.BackgroundColorStyle.Solid;
            _ConstraintBackGround.BackgroundColorStyle = DevAge.Drawing.BackgroundColorStyle.Solid;
            _IsBusyBackGround.BackgroundColorStyle = DevAge.Drawing.BackgroundColorStyle.Solid;

            _DefaultBackGround.Border = new DevAge.Drawing.RectangleBorder(ViewBase.DefaultBorder.Left);
            _ZnamenatBackGround.Border = new DevAge.Drawing.RectangleBorder(ViewBase.DefaultBorder.Left);
            _ConstraintBackGround.Border = new DevAge.Drawing.RectangleBorder(ViewBase.DefaultBorder.Left);
            _IsBusyBackGround.Border = new DevAge.Drawing.RectangleBorder(ViewBase.DefaultBorder.Left);
        }
        #endregion

        private DevAge.Drawing.VisualElements.ColumnHeader _DefaultBackGround =
            new DevAge.Drawing.VisualElements.ColumnHeader();

        private DevAge.Drawing.VisualElements.ColumnHeader _ZnamenatBackGround =
            new DevAge.Drawing.VisualElements.ColumnHeader();

        private DevAge.Drawing.VisualElements.ColumnHeader _IsBusyBackGround =
           new DevAge.Drawing.VisualElements.ColumnHeader();

        private DevAge.Drawing.VisualElements.ColumnHeader _ConstraintBackGround =
          new DevAge.Drawing.VisualElements.ColumnHeader();

        public event GetCellColor GetCellColorEvent;

        private bool m_IsBusy = false;
        public bool IsBusy
        {
            get { return m_IsBusy; }
            set { m_IsBusy = value; }
        }

        private bool m_HaveConstraint = false;
        public bool HaveConstraint
        {
            get { return m_HaveConstraint; }
            set { m_HaveConstraint = value; }
        }

        new public Color BackColor
        {
            get { return _DefaultBackGround.BackColor; }
            set { _DefaultBackGround.BackColor = value; }
        }

        public Color BackColor2
        {
            get { return _ZnamenatBackGround.BackColor; }
            set { _ZnamenatBackGround.BackColor = value; }
        }

        public Color BusyColor
        {
            get { return _IsBusyBackGround.BackColor; }
            set { _IsBusyBackGround.BackColor = value; }
        }

        public Color ConstraintColor
        {
            get { return _ConstraintBackGround.BackColor; }
            set { _ConstraintBackGround.BackColor = value; }
        }

        public void RestoreColor()
        {
            _DefaultBackGround.BackColor = Color.Snow;
            _ZnamenatBackGround.BackColor = Color.LightGoldenrodYellow;
        }

        protected override void PrepareView(SourceGrid.CellContext context)
        {
            base.PrepareView(context);

            if (GetCellColorEvent != null)
            {
                Color backColor = GetCellColorEvent(context.Position.Column - 3, context.Position.Row - 1);

                if (backColor == Color.FromArgb(0))
                {
                    if (context.Position.Row % 2 == 0)
                    {
                        Background = new DevAge.Drawing.VisualElements.ColumnHeader();
                        (Background as DevAge.Drawing.VisualElements.ColumnHeader).BackColor = backColor;
                        (Background as DevAge.Drawing.VisualElements.ColumnHeader).BackgroundColorStyle = 
                            DevAge.Drawing.BackgroundColorStyle.Solid;
                    }
                    else
                        Background = _DefaultBackGround;
                }
            }
            else
            {
               if (IsBusy)
                   Background = _IsBusyBackGround;
               else
                   if (HaveConstraint)
                       Background = _ConstraintBackGround;
                   else
                       if (context.Position.Row % 2 == 0)
                           Background = _ZnamenatBackGround;
                       else
                           Background = _DefaultBackGround;
            }
        }
    }

    public class WeekHeaderModel : FlatHeader
    {
        #region Constructors
        public WeekHeaderModel()
            : base()
        {
            _DefaultBackGround.BackColor = Color.Snow;
            _ZnamenatBackGround.BackColor = Color.LightGoldenrodYellow; 
            
            _DefaultBackGround.BackgroundColorStyle = DevAge.Drawing.BackgroundColorStyle.Solid;
            _ZnamenatBackGround.BackgroundColorStyle = DevAge.Drawing.BackgroundColorStyle.Solid;
        }

        public WeekHeaderModel(WeekHeaderModel p_Source)
            : base(p_Source)
        {
            _DefaultBackGround.BackColor = p_Source.BackColor;
            _ZnamenatBackGround.BackColor = p_Source.BackColor2;

            _DefaultBackGround.BackgroundColorStyle = DevAge.Drawing.BackgroundColorStyle.Solid;
            _ZnamenatBackGround.BackgroundColorStyle = DevAge.Drawing.BackgroundColorStyle.Solid;
        }
        #endregion

        private DevAge.Drawing.VisualElements.ColumnHeader _DefaultBackGround =
            new DevAge.Drawing.VisualElements.ColumnHeader();

        private DevAge.Drawing.VisualElements.ColumnHeader _ZnamenatBackGround =
            new DevAge.Drawing.VisualElements.ColumnHeader();

        public Color BackColor2
        {
            get { return _ZnamenatBackGround.BackColor; }
            set { _ZnamenatBackGround.BackColor = value; }
        }

        protected override void PrepareView(SourceGrid.CellContext context)
        {
            base.PrepareView(context);

            if (context.Position.Row % 2 == 0)
                Background = _ZnamenatBackGround;
            else
                Background = _DefaultBackGround;
        }

        public override object Clone()
        {
            return new WeekHeaderModel(this);
        }
    }

    public class FlatHeaderModel : FlatHeader
    {
        #region Constructors
        public FlatHeaderModel()
            : base()
        {
        }
        public FlatHeaderModel(GetFlatHeaderColor getFlatHeaderColor)
            : base()
        {
            GetFlatHeaderColorEvent = getFlatHeaderColor;
        }

        public FlatHeaderModel(FlatHeaderModel p_Source)
            : base(p_Source)
        {
        }
        #endregion

        public event GetFlatHeaderColor GetFlatHeaderColorEvent;

        protected override void PrepareView(SourceGrid.CellContext context)
        {
            base.PrepareView(context);

            if (context.Position.Column > 2)
                if (GetFlatHeaderColorEvent != null)
                    this.ForeColor = GetFlatHeaderColorEvent(context.Position.Column - 3);
        }
    }

    public class CustomHeaderModel : FlatHeader
    {
        #region
        public CustomHeaderModel()
            : base()
        {
            BackColor = Color.LightSteelBlue;
            _DefaultBackGround.BackColor = Color.LightSteelBlue;
            _DefaultBackGround.BackgroundColorStyle = DevAge.Drawing.BackgroundColorStyle.Solid;
        }

        public CustomHeaderModel(CustomHeaderModel p_Source)
            : base(p_Source)
        {
            BackColor = Color.LightSteelBlue;
            _DefaultBackGround.BackColor = Color.LightSteelBlue;
            _DefaultBackGround.BackgroundColorStyle = DevAge.Drawing.BackgroundColorStyle.Solid;
        }
        #endregion 

        DevAge.Drawing.VisualElements.ColumnHeader _DefaultBackGround =
               new DevAge.Drawing.VisualElements.ColumnHeader();
        
        private System.Drawing.StringFormatFlags mFormatFlags = StringFormatFlags.NoWrap;
        public System.Drawing.StringFormatFlags FormatFlags
        {
            get { return mFormatFlags; }
            set { mFormatFlags = value; }
        }

        public void ChangeDefaultColor(Color color)
        {
            _DefaultBackGround.BackColor = color;
            BackColor = color;
        }

        protected override void PrepareView(SourceGrid.CellContext context)
        {
            base.PrepareView(context);

            Background = _DefaultBackGround;

            if (ElementText is DevAge.Drawing.VisualElements.TextGDI)
                ((DevAge.Drawing.VisualElements.TextGDI)ElementText).StringFormat.FormatFlags = FormatFlags; 
        }

        public override object Clone()
        {
            return new CustomHeaderModel(this);
        }

        protected override SizeF OnMeasureContent(DevAge.Drawing.MeasureHelper measure, SizeF maxSize)
        {
            SizeF result = base.OnMeasureContent(measure, maxSize);
            return new SizeF(result.Width + 1, result.Height + 1);
        }
    }
}