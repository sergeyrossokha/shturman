using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Windows.Forms;
using Shturman.Nestor.Interfaces;
using Shturman.Shedule.Objects;
using Shturman.Shedule;
using Shturman.Shedule.Server.Interface;
using Shturman.Shedule.Server.Remoting;

using SourceGrid;
using SourceGrid.Cells;
using SourceGrid.Cells.Views;
using Common = SourceGrid.Cells.Views.Cell;
using FlatHeader = SourceGrid.Cells.Views.Header;

namespace Shturman.Shedule.GUI
{
	/// <summary>
	/// Summary description for TutorSheduleView.
	/// </summary>
	public class SheduleView2 : UserControl
	{
		private Grid grid;
        private IView EmptyView = new RestrictCellModel();
        private IView weekView = new WeekHeaderModel();
        private IView HeaderView = new FlatHeader();

        private IView dayView;
        private IView hourView;
        private IView titleView;

		private double _scale = 1;
		
		private IShedule2 sheduleMatrix;
		private IList weekDaysData;
		private int weekIndex = 1;
		private IList objectList;

		private int dayCount;
		private int hourCount;

		private Container components = null;

		public SheduleView2()
		{
			///
			/// Required for Windows.Forms Class Composition Designer support
			///
			InitializeComponent();

            grid.Selection.FocusStyle = FocusStyle.None;

            IView defaultView = new CustomHeaderModel();
            defaultView.TextAlignment = DevAge.Drawing.ContentAlignment.MiddleCenter;
            defaultView.Font = grid.Font.Clone() as Font;

            dayView = defaultView.Clone() as IView;

            (dayView as FlatHeader).ElementText = new DevAge.Drawing.VisualElements.TextGDI();
            (dayView as CustomHeaderModel).FormatFlags = StringFormatFlags.DirectionVertical | StringFormatFlags.NoWrap;


            hourView = (defaultView.Clone() as IView);

            titleView = new CustomHeaderModel();
            titleView.TextAlignment = DevAge.Drawing.ContentAlignment.MiddleCenter;
            (titleView as CustomHeaderModel).ChangeDefaultColor(System.Drawing.SystemColors.Control);
            titleView.Font = grid.Font.Clone() as Font;
		}

		public void RefreshShedule()
		{
			for(int colindex = 3; colindex<grid.Columns.Count; colindex++ )			
				for (int rowindex=1; rowindex<grid.RowsCount-1; rowindex++ )
				{
					int dayIndex = (rowindex-1) / (this.hourCount)+1;
					int pairIndex = ((rowindex-1) - (dayIndex-1)*this.hourCount)+1;

                    ExtObjKey key = (objectList[colindex - 3] as StoredObject).ObjectHardKey;

					RemoteLesson2 remoteLesson = sheduleMatrix.GetGroupLesson(key, (DateTime)weekDaysData[dayIndex-1],pairIndex);
                    if (remoteLesson != null)
                    {
                        string lessonstr = RemoteAdapter.Adapter.AdaptLesson(remoteLesson).GroupViewString;
                        grid[rowindex, colindex].Value = lessonstr;
                        continue;
                    }
                    else
                        grid[rowindex, colindex].Value = "";					

					grid.Rows[rowindex].Height = (int)Math.Round( (double) 25 * _scale ); // * ������ _scale
				}
		}

		
		public void ClearShedule()
		{
			grid.Redim(grid.RowsCount, 3);
		}

		public void ShowShedule(IList objectList, IShedule2 shmatr, int weekToShow)
		{
			this.objectList = objectList;
			this.weekIndex = weekToShow;
			sheduleMatrix = shmatr;	
			
			// �������� ���� ��� ���� �� �����
			weekDaysData = SheduleKeyBuilder.WeekDatesList(shmatr, weekToShow);

			ClearShedule();

            IView EmptyView = new RestrictCellModel();
            IView FlatHeaderView = new FlatHeader();

			foreach(StoredObject gr in objectList)
			{
				int currentColumn = grid.Columns.Count;
				grid.Columns.Insert(currentColumn);

                grid[0, grid.Columns.Count - 1] = new SourceGrid.Cells.Cell(gr.ToString(), typeof(string));
                grid[0, grid.Columns.Count - 1].View = titleView;
                grid[0, grid.Columns.Count - 1].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);
                grid[0, grid.Columns.Count - 1].AddController(SourceGrid.Cells.Controllers.Resizable.ResizeWidth);
				grid.Columns[grid.Columns.Count-1].Width = (int)Math.Round( (double) 150 * _scale );	// * ������
				
				for (int rowindex=1; rowindex<grid.RowsCount-1; rowindex++ )
				{
                    ExtObjKey key = gr.ObjectHardKey;

					int dayIndex = (rowindex-1) / (this.hourCount)+1;
					int pairIndex = ((rowindex-1) - (dayIndex-1)*this.hourCount)+1;

					//³��������� ���� � ������ �������
					if ( (rowindex-2) % (hourCount) == 0)
					{	
						int	day = (rowindex-2) / hourCount;					
						grid[rowindex-1, 0].Value = ((DateTime)weekDaysData[day]).ToShortDateString();
					}

					if ( gr is Group)
					{
						RemoteLesson2 remoteLesson = sheduleMatrix.GetGroupLesson(key, (DateTime)weekDaysData[dayIndex-1],pairIndex);
						if (remoteLesson != null)							
						{							
							string lessonstr = RemoteAdapter.Adapter.AdaptLesson(remoteLesson).GroupViewString;
							// �������� ��������� �� ����
                            if (sheduleMatrix.GroupBusyCheckBoolRestrict((DateTime)weekDaysData[dayIndex - 1], pairIndex, key))
                            {
                                grid[rowindex, currentColumn] = new SourceGrid.Cells.Cell(lessonstr, typeof(string));
                                grid[rowindex, currentColumn].View = new RestrictCellModel();

                                (grid[rowindex, currentColumn].View as RestrictCellModel).HaveConstraint = true;

                                // ToolTip Text
                                grid[rowindex, currentColumn].ToolTipText = shmatr.GetGroupRestrictsText(key, (DateTime)weekDaysData[dayIndex - 1], pairIndex);
                            }
                            else
                            {
                                grid[rowindex, currentColumn] = new SourceGrid.Cells.Cell(lessonstr, typeof(string));
                                grid[rowindex, currentColumn].View = EmptyView;
                            }
							
							grid.Rows[rowindex].Height = (int)Math.Round( (double) 25 * _scale );// * ������
							continue;
						}

						if(sheduleMatrix.IsGroupHasRestricts(key, (DateTime)weekDaysData[dayIndex-1],pairIndex))
						{
							string lessonstr = shmatr.GetGroupRestrictsText(key, (DateTime)weekDaysData[dayIndex-1],pairIndex);
							if (lessonstr != string.Empty && lessonstr[0] == '#')
							{
                                grid[rowindex, currentColumn] = new SourceGrid.Cells.Cell(lessonstr.Substring(1, lessonstr.Length - 1), typeof(string));
                                grid[rowindex, currentColumn].View = new RestrictCellModel();
								continue;
							}
						}
					}
					if ( gr is Tutor)
					{
						RemoteLesson2 remoteLesson = shmatr.GetTutorLesson(key, (DateTime)weekDaysData[dayIndex-1],pairIndex);
						if (remoteLesson != null)
						{
							string lessonstr = RemoteAdapter.Adapter.AdaptLesson(remoteLesson).TutorViewString;
                            if (sheduleMatrix.TutorBusyCheckBoolRestrict((DateTime)weekDaysData[dayIndex - 1], pairIndex, key))
                            {
                                grid[rowindex, currentColumn] = new SourceGrid.Cells.Cell(lessonstr, typeof(string));
                                grid[rowindex, currentColumn].View = new RestrictCellModel();
                                (grid[rowindex, currentColumn].View as RestrictCellModel).HaveConstraint = true;

                                // ToolTip Text
                                grid[rowindex, currentColumn].ToolTipText = shmatr.GetTutorRestrictsText(key, (DateTime)weekDaysData[dayIndex - 1], pairIndex);
                            }
                            else
                            {
                                grid[rowindex, currentColumn] = new SourceGrid.Cells.Cell(lessonstr, typeof(string));
                                grid[rowindex, currentColumn].View = EmptyView;

                            }				

							grid.Rows[rowindex].Height = (int)Math.Round( (double) 25 * _scale );// * ������
							continue;
						}
						if(sheduleMatrix.IsTutorHasRestricts(key, (DateTime)weekDaysData[dayIndex-1],pairIndex))
						{
							string lessonstr = shmatr.GetTutorRestrictsText(key, (DateTime)weekDaysData[dayIndex-1],pairIndex);
							if (lessonstr != string.Empty && lessonstr[0] == '#')
							{
                                grid[rowindex, currentColumn] = new SourceGrid.Cells.Cell(lessonstr.Substring(1, lessonstr.Length - 1), typeof(string));
                                grid[rowindex, currentColumn].View = new RestrictCellModel();
								continue;
							}
						}
					}

					if ( gr is LectureHall)
					{
						IList lessons = sheduleMatrix.GetFlatLesson(key, (DateTime)weekDaysData[dayIndex-1],pairIndex);
						if (lessons != null && lessons.Count>0)							
						{
							int lectureColumn = currentColumn;							
							foreach(RemoteLesson2 remlsn in lessons)
							{
								string lessonstr = RemoteAdapter.Adapter.AdaptLesson(remlsn).FlatViewString;

                                if (sheduleMatrix.FlatBusyCheckBoolRestrict((DateTime)weekDaysData[dayIndex - 1], pairIndex, key))
                                {
                                    grid[rowindex, lectureColumn] = new SourceGrid.Cells.Cell(lessonstr, typeof(string));
                                    grid[rowindex, lectureColumn].View = new RestrictCellModel();
                                    (grid[rowindex, lectureColumn].View as RestrictCellModel).HaveConstraint = true;
                                    grid[rowindex, lectureColumn].ToolTipText = shmatr.GetFlatRestrictsText(key, (DateTime)weekDaysData[dayIndex - 1], pairIndex);
                                }
                                else
                                {
                                    grid[rowindex, lectureColumn] = new SourceGrid.Cells.Cell(lessonstr, typeof(string));
                                    grid[rowindex, lectureColumn].View = EmptyView;
                                }
								grid.Rows[rowindex].Height = (int)Math.Round( (double) 25 * _scale );// * ������
									
								if (lessons.IndexOf(remlsn) != (lessons.Count-1))
								{
									if (lectureColumn+1 == grid.Columns.Count )
									{
										grid.Columns.Insert(grid.Columns.Count);
										grid.Columns[grid.Columns.Count-1].Width = (int)Math.Round( (double) 150 * _scale );	// * ������
									}
									lectureColumn ++;
								}								
							}
							continue;						
						}		
						else
						{
							if(sheduleMatrix.IsFlatHasRestricts(key, (DateTime)weekDaysData[dayIndex-1],pairIndex))
							{
								string lessonstr = shmatr.GetFlatRestrictsText(key, (DateTime)weekDaysData[dayIndex-1],pairIndex);

                                grid[rowindex, currentColumn] = new SourceGrid.Cells.Cell(lessonstr.Substring(1, lessonstr.Length - 1), typeof(string));
                                grid[rowindex, currentColumn].View = new RestrictCellModel();
                                continue;
							}
						}
					}

                    grid[rowindex, currentColumn] = new SourceGrid.Cells.Cell("", typeof(string));
                    grid[rowindex, currentColumn].View = EmptyView;
				
					grid.Rows[rowindex].Height = (int)Math.Round( (double) 25 * _scale );// * ������
				}
			}
		}

		// ++
		public void InitSheduleGrid(IList dayList, IList hourList, IList weekList)
		{
			dayCount = dayList.Count;
			hourCount = hourList.Count;

			//
			// Grid regim 2 - weeks, 6- days, 6 - hours
			//
			grid.Redim(2+dayCount*hourCount, 3);

			//
			// Load grid header
			//
            grid[0, 0] = new SourceGrid.Cells.Cell("", typeof(string));
            grid[0, 0].View = titleView;
            grid[0, 0].RowSpan = 1;
            grid[0, 0].ColumnSpan = 3;
            grid[0, 0].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);		

			//
			// Create grid day caption
			//
			grid.Columns[0].Width = 20; // * ������
			grid.Columns[1].Width = 20; // * ������
			grid.Columns[2].Width = 20; // * ������
			
			for(int dayindex=0; dayindex<dayCount; dayindex++)
			{
				int cellindex = dayindex*hourCount+1;

                grid[cellindex, 0] = new SourceGrid.Cells.Cell("", typeof(string));
                grid[cellindex, 0].View = dayView;

                grid[cellindex, 0].RowSpan = hourCount;
                grid[cellindex, 0].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);



                grid[cellindex, 1] = new SourceGrid.Cells.Cell(dayList[dayindex], typeof(string));
                grid[cellindex, 1].View = dayView;

                grid[cellindex, 1].RowSpan = hourCount;
                grid[cellindex, 1].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);

				for(int hourindex=cellindex; hourindex<(cellindex+hourCount); hourindex++ )
				{
                    grid[hourindex, 2] = new SourceGrid.Cells.Cell(hourList[(hourindex - cellindex)].ToString(), typeof(string));
                    grid[hourindex, 2].View = hourView;
                    grid[hourindex, 2].RowSpan = 2;

                    grid[hourindex, 2].AddController(SourceGrid.Cells.Controllers.Unselectable.Default);
				}
			}
			grid.FixedColumns = 2;
			grid.FixedRows = 1;
		}

		
		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}


		#region Component Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.grid = new SourceGrid.Grid();
			this.SuspendLayout();
			// 
			// grid
			// 
			//this.grid.AutoSizeMinHeight = 10;
			//this.grid.AutoSizeMinWidth = 10;
			this.grid.AutoStretchColumnsToFitWidth = false;
			this.grid.AutoStretchRowsToFitHeight = false;
			this.grid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			//this.grid.ContextMenuStyle = SourceGrid2.ContextMenuStyle.None;
			this.grid.CustomSort = false;
			this.grid.Dock = System.Windows.Forms.DockStyle.Fill;
			//this.grid.GridToolTipActive = true;
			this.grid.Location = new System.Drawing.Point(0, 0);
			this.grid.Name = "grid";
			this.grid.Size = new System.Drawing.Size(360, 340);
			//this.grid.SpecialKeys = SourceGrid2.GridSpecialKeys.Default;
			this.grid.TabIndex = 0;

			this.grid.VScrollPositionChanged+=new ScrollPositionChangedEventHandler(sheduleGrid_VScrollPositionChanged);

			// 
			// GroupSheduleGrid
			// 
			this.Controls.Add(this.grid);
			this.Name = "SheduleGrid";
			this.Size = new System.Drawing.Size(360, 340);
			this.ResumeLayout(false);
		}
		#endregion

		//++
		public IList getSheduleSelPositions()
		{
			IList shposcol = new ArrayList();
            PositionCollection poscol = this.grid.Selection.GetSelectionRegion().GetCellsPositions();
			foreach (Position pos in poscol )
			{
				int day = (pos.Row-1) / (sheduleMatrix.GetPairsPerDayCount()) + 1;
				int pair = ((pos.Row-1) - (day-1)*sheduleMatrix.GetPairsPerDayCount())+1;
				
				ShedulePosition2 shpos = new ShedulePosition2((DateTime)weekDaysData[day-1], pair);
				
				if (shposcol.Contains(shpos))
				{
					shpos = shposcol [shposcol.IndexOf(shpos)] as ShedulePosition2;
					if (!shpos.Columns.Contains(objectList[pos.Column-3]))
						shpos.Columns.Add(objectList[pos.Column-3]);
				}
				else 
				{
					shposcol.Add( shpos );
					if (!shpos.Columns.Contains(objectList[pos.Column-3]))
						shpos.Columns.Add(objectList[pos.Column-3]);
				}
			}
			return shposcol;
		}

		//++
		public int GetShedulePosition(int x, int y, out DateTime dayDate, out int dayIndex, out int pairIndex)
		{
			Point _clientPoint = grid.PointToClient(new Point(x,y));			
			Position _position = grid.PositionAtPoint(_clientPoint);

			dayIndex = (_position.Row-1) / (this.hourCount)+1;
			pairIndex = ((_position.Row-1) - (dayIndex-1)*this.hourCount)+1;			
			dayDate = (DateTime)weekDaysData[dayIndex-1];
			
			if (_position.Row > 0 && _position.Column > 2 ) return 1;
			else return 0;
		}

		//++
		public void Scale(int scaleTo)
		{	
			if (objectList == null)
				return;

			_scale = scaleTo/100.0;
			/* ������������ ����� ����� */
			Font newFont = new Font( grid.Font.FontFamily, (float)(this.Font.Size * _scale));
			grid.Font = newFont;
			if (weekView.Font != null)
				weekView.Font = newFont;
			if (dayView.Font != null)
				dayView.Font = newFont;
			if (hourView.Font != null)
				hourView.Font = newFont;
			if (HeaderView.Font != null)
				HeaderView.Font = newFont;
			
			/* ������� ����� �������� ���� */
			for(int colindex = 0; colindex<grid.Columns.Count; colindex++ )			
			{				
				for (int rowindex=0; rowindex<grid.RowsCount-1; rowindex++ )
				if(grid[rowindex, colindex] != null)
				{				
					grid[rowindex, colindex].View.Font = newFont;							
					// ������
					grid.Rows[rowindex].Height = (int)Math.Round( (double) 25 * _scale ); 
				}
				// ������
				if (colindex > 2)
					grid.Columns[colindex].Width = (int)Math.Round( (double) 150 * _scale );
				else
					grid.Columns[colindex].Width = (int)Math.Round( (double) 20 * _scale );
			}
		}

		
//		private bool _setFocusDo = false;
//		public void SetFocus(int day, int week, int pair)
//		{
//			int cellIndex = (day-1)*(sheduleMatrix.GetPairsPerDayCount()*sheduleMatrix.GetWeeksCount())+ pair*sheduleMatrix.GetWeeksCount() + week -2;
//			_setFocusDo = true;
//			grid.SetFocusCell( grid[cellIndex, 3] );
//		}

//		public void SetFocus(int column)
//		{
//			grid.SetFocusCell( grid[column+3, 2] );
//		}

		private void sheduleGrid_VScrollPositionChanged(object sender, ScrollPositionChangedEventArgs e)
		{
//			if (_setFocusDo)
//				if (grid.FocusCell != null)
//					if (e.OldValue > e.NewValue)
//					{
//						int day = (grid.FocusCell.Row-1) / (sheduleMatrix.GetPairsPerDayCount()*sheduleMatrix.GetWeeksCount()) + 1;
//						int pair = ((grid.FocusCell.Row-1) - (day-1)*sheduleMatrix.GetPairsPerDayCount()*sheduleMatrix.GetWeeksCount())/sheduleMatrix.GetWeeksCount()+1;
//						grid.ShowCell(grid[grid.FocusCell.Row+ (sheduleMatrix.GetPairsPerDayCount()-pair)*sheduleMatrix.GetWeeksCount()+1, 3]);
//					}
//					else
//					{
//						int day = (grid.FocusCell.Row-1) / (sheduleMatrix.GetPairsPerDayCount()*sheduleMatrix.GetWeeksCount()) + 1;
//						int pair = ((grid.FocusCell.Row-1) - (day-1)*sheduleMatrix.GetPairsPerDayCount()*sheduleMatrix.GetWeeksCount())/sheduleMatrix.GetWeeksCount()+1;
//						grid.ShowCell(grid[grid.FocusCell.Row + (sheduleMatrix.GetPairsPerDayCount()-pair)*sheduleMatrix.GetWeeksCount()+1, 3]);
//					}
//			_setFocusDo = false;
		}
	}
}
