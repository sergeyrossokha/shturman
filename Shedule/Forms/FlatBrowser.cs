/*�����: ������� ����� �������������
 * ����: ����� ������������� ���������� �� ��������� ����� �� �� ��������
 * 
 * ������������ �� ������������ � ������ ������
 * */
using System;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using Shturman.MultyLanguage;
using Shturman.Nestor.DataLists;
using Shturman.Nestor.Interfaces;
using Shturman.Shedule.Objects.Container;
using Shturman.Shedule.Objects;
using Shturman.Shedule.GUI;
using Shturman.Shedule.Server.Interface;
using Shturman.Shedule.Server.Remoting;

namespace Shturman.Shedule.Forms
{
	/// <summary>
	/// Summary description for FlatBrowser.
	/// </summary>
	public class FlatBrowser : Form
	{
		private RestrictSheduleView restrictSheduleView1;
		private Button button1;
		private Button button2;
		private Label label1;
		private Label label2;
		private ListBox selectedFlatList;

		private IShedule _shmatr;		
		private IList _buildingList;
		private StudyLoading _studyLoading;
		private ImageList imageList1;
		private Button button3;
		private Button button4;
		private Button button5;
		private Label label3;
		private Label label4;
		private Label label5;
		private Label label6;
		private CheckedListBox checkedListDays;
		private CheckedListBox checkedListWeeks;
		private CheckedListBox checkedListPairs;
		private ErrorProvider errorProvider1;
		private IContainer components;
		private GroupBox timeGroupBox;
		private Label Recomend;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.Button buttonSearchFlat;
		private System.Windows.Forms.TreeView treeSearchFlat;
		private System.Windows.Forms.TreeView treeFlats;
		private System.Windows.Forms.RadioButton allRadioButton;
		private System.Windows.Forms.RadioButton notAllRadioButton;
		private ToolTip toolTip1;
        private Label ExpertRecomend;
        private Label RecomendationLbl;
        private Label ExpRecomLbl;
//		private ListFiltrator lsfiltr = new ListFiltrator();
		private IObjectStorage _storage = DB4OBridgeRem.GetObjectStorage();

		public FlatBrowser(IList weekList, IList dayList, IList pairList, IShedule shmatr, IList buildingList)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			toolTip1.SetToolTip(this.treeFlats, "");			
			toolTip1.SetToolTip(this.treeSearchFlat, "");

			this._shmatr = shmatr;
			this._buildingList = buildingList;

			restrictSheduleView1.LoadShedule(dayList, pairList, weekList);

			foreach (object obj in dayList)                
				this.checkedListDays.Items.Add( obj );
			
			foreach (object obj in weekList)
				this.checkedListWeeks.Items.Add( obj );
			
			foreach (object obj in pairList)
				this.checkedListPairs.Items.Add( obj );
		}

		public DialogResult ShowDialog(StudyDay selDay, StudyCycles selCycle, StudyHour selHour, StudyLoading stdLoad)
		{
			if (selDay != null)
			{
				this.checkedListDays.SetItemChecked( this.checkedListDays.Items.IndexOf(selDay), true);
				this.checkedListDays.SetItemCheckState( this.checkedListDays.Items.IndexOf(selDay),	CheckState.Checked);
			}
			
			if (selCycle == null)
			{
				if(selDay != null && selHour != null) // ������� ����, �� ������� �����������
					for(int index=0; index<this.checkedListWeeks.Items.Count; index++)
					{
						this.checkedListWeeks.SetItemChecked( index, true );
						this.checkedListWeeks.SetItemCheckState( index,	CheckState.Checked);
					}
			}
			else
			{
				this.checkedListWeeks.SetItemChecked( this.checkedListWeeks.Items.IndexOf(selCycle), true );
				this.checkedListWeeks.SetItemCheckState( this.checkedListWeeks.Items.IndexOf(selCycle), CheckState.Checked );
			}
			
			if (selHour != null)
			{
				this.checkedListPairs.SetItemChecked( this.checkedListPairs.Items.IndexOf(selHour), true );
				this.checkedListPairs.SetItemCheckState( this.checkedListPairs.Items.IndexOf(selHour), CheckState.Checked );
			}
			
			this._studyLoading = stdLoad;
			this.Text = stdLoad.ToString();

			this.Recomend.Text = stdLoad.Note;
            
			if ( 
				(this._studyLoading.Dept != null && this._studyLoading.Dept.MnemoCode == "705") /*����������� ��� ���������*/
				||(this._studyLoading.SubjectName.Name == "����������� ��������� �� �������") /*������������ ���������� �� ������ ��� ���������*/
				)
			{
				this.tabPage1.Enabled = false;
				this.tabPage2.Enabled = false;
				this.button1.Enabled = true;
			}
			else
			{
				// Build tree
				this.RebuildTree();
				button1.Enabled = false;
			}
			
			return ShowDialog();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		private void RebuildTree()
		{
			this.treeFlats.Nodes.Clear();
			this.BuildTree();
		}

/*
		private void UpdateTree()
		{
			foreach( StudyDay sd in this.checkedListDays.SelectedItems)
				foreach( StudyHour sp in this.checkedListPairs.SelectedItems)
					foreach( StudyCycles sc in this.checkedListWeeks.SelectedItems)
					{
						int _sheduleKey = this._shmatr.GetKey( int.Parse(sc.MnemoCode), int.Parse(sd.MnemoCode), int.Parse(sp.MnemoCode) );
						foreach( TreeNode tn in this.treeView1.Nodes)
							if (tn.Parent != null)
							{
								if (this._shmatr.FlatBusyCheck(_sheduleKey, (tn.Tag as LectureHall).Uid) == FlatBusyCheckerState.isBusy)
								{
									tn.ImageIndex = 2;
									tn.SelectedImageIndex = 2;
								}
								else
								{
									tn.ImageIndex = 1;
									tn.SelectedImageIndex = 1;
								}
							}
					}
		}
*/

		private void BuildTree()
		{			
			foreach(Building _building in _buildingList)
			{
				TreeNode _buildTreeNode = new TreeNode();
				_buildTreeNode.Text = _building.MnemoCode;
				_buildTreeNode.Tag = _building;
				_buildTreeNode.ImageIndex = 0;
				_buildTreeNode.SelectedImageIndex = 0;

				_buildTreeNode.Nodes.Add(new TreeNode("-1"));

				this.treeFlats.Nodes.Add( _buildTreeNode );
			}
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FlatBrowser));
            this.treeFlats = new System.Windows.Forms.TreeView();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.selectedFlatList = new System.Windows.Forms.ListBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.checkedListDays = new System.Windows.Forms.CheckedListBox();
            this.checkedListWeeks = new System.Windows.Forms.CheckedListBox();
            this.checkedListPairs = new System.Windows.Forms.CheckedListBox();
            this.timeGroupBox = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.Recomend = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.treeSearchFlat = new System.Windows.Forms.TreeView();
            this.notAllRadioButton = new System.Windows.Forms.RadioButton();
            this.allRadioButton = new System.Windows.Forms.RadioButton();
            this.buttonSearchFlat = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.ExpertRecomend = new System.Windows.Forms.Label();
            this.ExpRecomLbl = new System.Windows.Forms.Label();
            this.RecomendationLbl = new System.Windows.Forms.Label();
            this.restrictSheduleView1 = new Shturman.Shedule.GUI.RestrictSheduleView();
            this.timeGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.SuspendLayout();
            // 
            // treeFlats
            // 
            this.treeFlats.AllowDrop = true;
            this.treeFlats.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.treeFlats.ImageIndex = 0;
            this.treeFlats.ImageList = this.imageList1;
            this.treeFlats.ItemHeight = 16;
            this.treeFlats.Location = new System.Drawing.Point(141, 24);
            this.treeFlats.Name = "treeFlats";
            this.treeFlats.SelectedImageIndex = 0;
            this.treeFlats.Size = new System.Drawing.Size(227, 216);
            this.treeFlats.TabIndex = 0;
            this.treeFlats.DragDrop += new System.Windows.Forms.DragEventHandler(this.treeView1_DragDrop);
            this.treeFlats.BeforeExpand += new System.Windows.Forms.TreeViewCancelEventHandler(this.treeView1_BeforeExpand);
            this.treeFlats.NodeMouseHover += new System.Windows.Forms.TreeNodeMouseHoverEventHandler(this.treeFlats_NodeMouseHover);
            this.treeFlats.DoubleClick += new System.EventHandler(this.treeView1_DoubleClick);
            this.treeFlats.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeView1_AfterSelect);
            this.treeFlats.MouseMove += new System.Windows.Forms.MouseEventHandler(this.treeView1_MouseMove);
            this.treeFlats.DragEnter += new System.Windows.Forms.DragEventHandler(this.treeView1_DragEnter);
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            this.imageList1.Images.SetKeyName(2, "");
            // 
            // selectedFlatList
            // 
            this.selectedFlatList.AllowDrop = true;
            this.selectedFlatList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.selectedFlatList.IntegralHeight = false;
            this.selectedFlatList.Location = new System.Drawing.Point(408, 24);
            this.selectedFlatList.Name = "selectedFlatList";
            this.selectedFlatList.Size = new System.Drawing.Size(128, 192);
            this.selectedFlatList.TabIndex = 1;
            this.selectedFlatList.DragEnter += new System.Windows.Forms.DragEventHandler(this.selectedFlatList_DragEnter);
            this.selectedFlatList.DragDrop += new System.Windows.Forms.DragEventHandler(this.selectedFlatList_DragDrop);
            this.selectedFlatList.MouseMove += new System.Windows.Forms.MouseEventHandler(this.selectedFlatList_MouseMove);
            // 
            // button1
            // 
            this.errorProvider1.SetIconAlignment(this.button1, System.Windows.Forms.ErrorIconAlignment.MiddleLeft);
            this.button1.Location = new System.Drawing.Point(372, 500);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(85, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "���������";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.button2.Location = new System.Drawing.Point(474, 500);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(85, 23);
            this.button2.TabIndex = 4;
            this.button2.Text = "��������";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(144, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(192, 14);
            this.label1.TabIndex = 5;
            this.label1.Text = "��������� ���������";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(405, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(131, 14);
            this.label2.TabIndex = 6;
            this.label2.Text = "��������� ���������";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.label2.Click += new System.EventHandler(this.label2_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(371, 96);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(32, 23);
            this.button3.TabIndex = 7;
            this.button3.Text = ">>";
            this.button3.Click += new System.EventHandler(this.treeView1_DoubleClick);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(371, 128);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(32, 23);
            this.button4.TabIndex = 8;
            this.button4.Text = "<<";
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(464, 221);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(72, 23);
            this.button5.TabIndex = 9;
            this.button5.Text = "��������";
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // checkedListDays
            // 
            this.checkedListDays.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.checkedListDays.CheckOnClick = true;
            this.checkedListDays.IntegralHeight = false;
            this.checkedListDays.Location = new System.Drawing.Point(6, 35);
            this.checkedListDays.Name = "checkedListDays";
            this.checkedListDays.Size = new System.Drawing.Size(150, 104);
            this.checkedListDays.TabIndex = 10;
            this.checkedListDays.Validating += new System.ComponentModel.CancelEventHandler(this.checkedListDays_Validating);
            this.checkedListDays.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListDays_ItemCheck);
            // 
            // checkedListWeeks
            // 
            this.checkedListWeeks.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.checkedListWeeks.CheckOnClick = true;
            this.checkedListWeeks.IntegralHeight = false;
            this.checkedListWeeks.Location = new System.Drawing.Point(390, 35);
            this.checkedListWeeks.Name = "checkedListWeeks";
            this.checkedListWeeks.Size = new System.Drawing.Size(150, 104);
            this.checkedListWeeks.TabIndex = 11;
            this.checkedListWeeks.Validating += new System.ComponentModel.CancelEventHandler(this.checkedListDays_Validating);
            this.checkedListWeeks.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListDays_ItemCheck);
            // 
            // checkedListPairs
            // 
            this.checkedListPairs.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.checkedListPairs.CheckOnClick = true;
            this.checkedListPairs.IntegralHeight = false;
            this.checkedListPairs.Location = new System.Drawing.Point(196, 35);
            this.checkedListPairs.Name = "checkedListPairs";
            this.checkedListPairs.Size = new System.Drawing.Size(150, 104);
            this.checkedListPairs.TabIndex = 16;
            this.checkedListPairs.Validating += new System.ComponentModel.CancelEventHandler(this.checkedListDays_Validating);
            this.checkedListPairs.ItemCheck += new System.Windows.Forms.ItemCheckEventHandler(this.checkedListDays_ItemCheck);
            // 
            // timeGroupBox
            // 
            this.timeGroupBox.Controls.Add(this.label5);
            this.timeGroupBox.Controls.Add(this.checkedListWeeks);
            this.timeGroupBox.Controls.Add(this.label4);
            this.timeGroupBox.Controls.Add(this.label3);
            this.timeGroupBox.Controls.Add(this.checkedListDays);
            this.timeGroupBox.Controls.Add(this.checkedListPairs);
            this.timeGroupBox.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.timeGroupBox.Location = new System.Drawing.Point(8, 8);
            this.timeGroupBox.Name = "timeGroupBox";
            this.timeGroupBox.Size = new System.Drawing.Size(552, 144);
            this.timeGroupBox.TabIndex = 13;
            this.timeGroupBox.TabStop = false;
            this.timeGroupBox.Text = "�����";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(196, 17);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(141, 16);
            this.label5.TabIndex = 15;
            this.label5.Text = "����";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(390, 17);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(142, 16);
            this.label4.TabIndex = 14;
            this.label4.Text = "������";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(10, 17);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(142, 14);
            this.label3.TabIndex = 13;
            this.label3.Text = "����";
            // 
            // Recomend
            // 
            this.Recomend.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Recomend.Location = new System.Drawing.Point(7, 261);
            this.Recomend.Name = "Recomend";
            this.Recomend.Size = new System.Drawing.Size(228, 43);
            this.Recomend.TabIndex = 11;
            this.Recomend.Text = "label7";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(8, 8);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(128, 14);
            this.label6.TabIndex = 10;
            this.label6.Text = "�����������";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // errorProvider1
            // 
            this.errorProvider1.BlinkRate = 150;
            this.errorProvider1.BlinkStyle = System.Windows.Forms.ErrorBlinkStyle.AlwaysBlink;
            this.errorProvider1.ContainerControl = this;
            // 
            // toolTip1
            // 
            this.toolTip1.AutoPopDelay = 5000;
            this.toolTip1.InitialDelay = 250;
            this.toolTip1.ReshowDelay = 100;
            this.toolTip1.ShowAlways = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.HotTrack = true;
            this.tabControl1.Location = new System.Drawing.Point(8, 160);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.ShowToolTips = true;
            this.tabControl1.Size = new System.Drawing.Size(552, 336);
            this.tabControl1.TabIndex = 15;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.RecomendationLbl);
            this.tabPage1.Controls.Add(this.ExpRecomLbl);
            this.tabPage1.Controls.Add(this.ExpertRecomend);
            this.tabPage1.Controls.Add(this.Recomend);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.button3);
            this.tabPage1.Controls.Add(this.button5);
            this.tabPage1.Controls.Add(this.selectedFlatList);
            this.tabPage1.Controls.Add(this.treeFlats);
            this.tabPage1.Controls.Add(this.button4);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.restrictSheduleView1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(544, 310);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "���� �������";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.treeSearchFlat);
            this.tabPage2.Controls.Add(this.notAllRadioButton);
            this.tabPage2.Controls.Add(this.allRadioButton);
            this.tabPage2.Controls.Add(this.buttonSearchFlat);
            this.tabPage2.Controls.Add(this.button6);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(544, 310);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "����� �������";
            // 
            // treeSearchFlat
            // 
            this.treeSearchFlat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.treeSearchFlat.HideSelection = false;
            this.treeSearchFlat.Location = new System.Drawing.Point(3, 34);
            this.treeSearchFlat.Name = "treeSearchFlat";
            this.treeSearchFlat.Size = new System.Drawing.Size(536, 240);
            this.treeSearchFlat.TabIndex = 15;
            this.treeSearchFlat.MouseMove += new System.Windows.Forms.MouseEventHandler(this.treeView1_MouseMove);
            // 
            // notAllRadioButton
            // 
            this.notAllRadioButton.Location = new System.Drawing.Point(128, 8);
            this.notAllRadioButton.Name = "notAllRadioButton";
            this.notAllRadioButton.Size = new System.Drawing.Size(104, 24);
            this.notAllRadioButton.TabIndex = 13;
            this.notAllRadioButton.Text = "��������";
            // 
            // allRadioButton
            // 
            this.allRadioButton.Checked = true;
            this.allRadioButton.Location = new System.Drawing.Point(8, 8);
            this.allRadioButton.Name = "allRadioButton";
            this.allRadioButton.Size = new System.Drawing.Size(120, 24);
            this.allRadioButton.TabIndex = 12;
            this.allRadioButton.TabStop = true;
            this.allRadioButton.Text = "� ��� ��������";
            // 
            // buttonSearchFlat
            // 
            this.buttonSearchFlat.Location = new System.Drawing.Point(443, 8);
            this.buttonSearchFlat.Name = "buttonSearchFlat";
            this.buttonSearchFlat.Size = new System.Drawing.Size(96, 23);
            this.buttonSearchFlat.TabIndex = 11;
            this.buttonSearchFlat.Text = "������";
            this.buttonSearchFlat.Click += new System.EventHandler(this.buttonSearchFlat_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(454, 280);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(85, 23);
            this.button6.TabIndex = 10;
            this.button6.Text = "�������";
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // ExpertRecomend
            // 
            this.ExpertRecomend.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.ExpertRecomend.Location = new System.Drawing.Point(241, 261);
            this.ExpertRecomend.Name = "ExpertRecomend";
            this.ExpertRecomend.Size = new System.Drawing.Size(295, 43);
            this.ExpertRecomend.TabIndex = 12;
            // 
            // ExpRecomLbl
            // 
            this.ExpRecomLbl.AutoSize = true;
            this.ExpRecomLbl.Location = new System.Drawing.Point(244, 243);
            this.ExpRecomLbl.Name = "ExpRecomLbl";
            this.ExpRecomLbl.Size = new System.Drawing.Size(41, 13);
            this.ExpRecomLbl.TabIndex = 13;
            this.ExpRecomLbl.Text = "label7";
            // 
            // RecomendationLbl
            // 
            this.RecomendationLbl.AutoSize = true;
            this.RecomendationLbl.Location = new System.Drawing.Point(10, 243);
            this.RecomendationLbl.Name = "RecomendationLbl";
            this.RecomendationLbl.Size = new System.Drawing.Size(41, 13);
            this.RecomendationLbl.TabIndex = 14;
            this.RecomendationLbl.Text = "label8";
            // 
            // restrictSheduleView1
            // 
            this.restrictSheduleView1.CellHeight = 16;
            this.restrictSheduleView1.CellWidth = 16;
            this.restrictSheduleView1.Location = new System.Drawing.Point(8, 24);
            this.restrictSheduleView1.Name = "restrictSheduleView1";
            this.restrictSheduleView1.Size = new System.Drawing.Size(130, 216);
            this.restrictSheduleView1.TabIndex = 2;
            // 
            // FlatBrowser
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(6, 13);
            this.ClientSize = new System.Drawing.Size(562, 527);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.timeGroupBox);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FlatBrowser";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "����� ���������";
            this.Load += new System.EventHandler(this.FlatBrowser_Load);
            this.timeGroupBox.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.ResumeLayout(false);

		}
		#endregion

		private void selectedFlatList_DragEnter(object sender, DragEventArgs e)
		{			
			if (e.Data.GetDataPresent( typeof(LectureHall)))
				e.Effect = DragDropEffects.Copy;
			else
				e.Effect = DragDropEffects.None;

		}

		private void treeView1_MouseMove(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
				if(treeFlats.SelectedNode != null)
					treeFlats.DoDragDrop( treeFlats.SelectedNode.Tag , DragDropEffects.Copy | DragDropEffects.Move);
		}

		private void selectedFlatList_DragDrop(object sender, DragEventArgs e)
		{
			object data = e.Data.GetData( typeof( LectureHall ) );
			// �������� �� ��������� 
			bool isEveryWereFree = true;
			
			ExtObjKey key = new ExtObjKey();
			key.UID = _storage.GetUUID(data, out key.Signature, out key.Index);

			foreach( StudyDay sd in this.checkedListDays.CheckedItems)
				foreach( StudyHour sp in this.checkedListPairs.CheckedItems)
					foreach( StudyCycles sc in this.checkedListWeeks.CheckedItems)
					{
						int _sheduleKey = this._shmatr.GetKey( int.Parse(sc.MnemoCode), int.Parse(sd.MnemoCode), int.Parse(sp.MnemoCode) );
						isEveryWereFree = isEveryWereFree && (this._shmatr.FlatBusyCheck(_sheduleKey, key) == FlatBusyCheckerState.isNotBusy);
					}
			
			if (isEveryWereFree)
				if ( !this.selectedFlatList.Items.Contains( data ) )
					this.selectedFlatList.Items.Add( data );
			
			if (!isEveryWereFree)
				if( MessageBox.Show(this, Vocabruary.Translate("Message.CanUseFlatText"), Vocabruary.Translate("Message.WarningTitle"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
					if ( !this.selectedFlatList.Items.Contains( data ) )
						this.selectedFlatList.Items.Add( data );

			if (this.selectedFlatList.Items.Count == 0)
				this.button1.Enabled = false;
			else
				this.button1.Enabled = true;
		}

		private void treeView1_DragEnter(object sender, DragEventArgs e)
		{
			if (e.Data.GetDataPresent( typeof(LectureHall)))
				e.Effect = DragDropEffects.Copy;
			else
				e.Effect = DragDropEffects.None;
		}

		private void treeView1_DragDrop(object sender, DragEventArgs e)
		{
			object data = e.Data.GetData( typeof( LectureHall ) );
			if ( this.selectedFlatList.Items.Contains( data ) )
				this.selectedFlatList.Items.Remove( data );
			
			if (this.selectedFlatList.Items.Count == 0)
				this.button1.Enabled = false;
			else	
				this.button1.Enabled = true;
		}

		private void selectedFlatList_MouseMove(object sender, MouseEventArgs e)
		{
			if (e.Button == MouseButtons.Left)
				if (selectedFlatList.SelectedItem != null)			
					selectedFlatList.DoDragDrop( selectedFlatList.SelectedItem , DragDropEffects.Copy | DragDropEffects.Move);			
		}

		private void treeView1_AfterSelect(object sender, TreeViewEventArgs e)
		{
			if (e.Node.Tag is LectureHall)
				this.restrictSheduleView1.Show(e.Node.Tag, this._shmatr);
		}

		private void treeView1_DoubleClick(object sender, EventArgs e)
		{
			TreeNode _node = treeFlats.SelectedNode;
			if (_node.Tag is LectureHall)
			{
				ExtObjKey key = new ExtObjKey();
				key.UID = this._storage.GetUUID(_node.Tag, out key.Signature, out key.Index);

				// �������� �� ��������� 
				bool isEveryWereFree = true;
				foreach( StudyDay sd in this.checkedListDays.CheckedItems)
					foreach( StudyHour sp in this.checkedListPairs.CheckedItems)
						foreach( StudyCycles sc in this.checkedListWeeks.CheckedItems)
						{
							int _sheduleKey = this._shmatr.GetKey( int.Parse(sc.MnemoCode), int.Parse(sd.MnemoCode), int.Parse(sp.MnemoCode) );
							isEveryWereFree = isEveryWereFree && (this._shmatr.FlatBusyCheck(_sheduleKey, key) == FlatBusyCheckerState.isNotBusy);
						}

				if (isEveryWereFree)
					if ( !this.selectedFlatList.Items.Contains( _node.Tag ) )
						this.selectedFlatList.Items.Add( _node.Tag );

				if (!isEveryWereFree)
					if( MessageBox.Show(this, Vocabruary.Translate("Message.CanUseFlatText"), Vocabruary.Translate("Message.WarningTitle"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
						if ( !this.selectedFlatList.Items.Contains( _node.Tag ) )
							this.selectedFlatList.Items.Add( _node.Tag );
			}

			
			if (this.selectedFlatList.Items.Count == 0)
				this.button1.Enabled = false;
			else
				this.button1.Enabled = true;
		}

		private void button1_Click(object sender, EventArgs e)
		{			
			if (
				(this._studyLoading.Dept != null && this._studyLoading.Dept.MnemoCode == "705") /*����������� ��� ���������*/
				||(this._studyLoading.SubjectName.Name == "����������� ��������� �� �������") /*������������ ���������� �� ������ ��� ���������*/
				)
				this.DialogResult = DialogResult.OK;
			else
				if(this.selectedFlatList.Items.Count>0)
					this.DialogResult = DialogResult.OK;
		}

		private void FlatBrowser_Load(object sender, EventArgs e)
		{
			/// ������� ��������� ������
			button1.Text = Vocabruary.Translate("Button.Apply");
			button2.Text = Vocabruary.Translate("Button.Cancel");			
			button3.Text = Vocabruary.Translate("Button.MoveTo");
			button4.Text = Vocabruary.Translate("Button.MoveFrom");
			button5.Text = Vocabruary.Translate("Button.Clear");

			/// ������� ��������� ��� ����� ������
			label1.Text = Vocabruary.Translate("FlatBrowser.FreeFlatTitle");
			label2.Text = Vocabruary.Translate("FlatBrowser.SelectedFlatTitle");			
			label3.Text = Vocabruary.Translate("FlatBrowser.DayListTitle");
			label4.Text = Vocabruary.Translate("FlatBrowser.WeekListTitle");
			label5.Text = Vocabruary.Translate("FlatBrowser.PairListTitle");
			label6.Text = Vocabruary.Translate("FlatBrowser.RestrictGridTitle");
            ExpRecomLbl.Text = Vocabruary.Translate("FlatBrowser.ExpertRecomend");
            RecomendationLbl.Text = Vocabruary.Translate("FlatBrowser.Recomendation");

			timeGroupBox.Text = Vocabruary.Translate("FlatBrowser.TimeBrowseGroupTitle");
			//flatGroupBox.Text = Vocabruary.Translate("FlatBrowser.FlatBrowseGroupTitle");

			
			this.Text = Vocabruary.Translate("FlatBrowser.Title");
		}

		private void checkedListDays_ItemCheck(object sender, ItemCheckEventArgs e)
		{
			if (e.NewValue == CheckState.Checked)
				foreach(TreeNode tn in this.treeFlats.Nodes)
					if (tn.Nodes.Count != 1 || (tn.Nodes.Count == 1 && tn.Nodes[0].Text != "-1"))
					{
						tn.Nodes.Clear();
						tn.Nodes.Add(new TreeNode("-1"));
						tn.Collapse();
					}
			Check();
		}

		private int SelectedHour()
		{
			return this.checkedListDays.CheckedItems.Count*this.checkedListWeeks.CheckedItems.Count*
				this.checkedListPairs.CheckedItems.Count;
		}

		private void checkedListDays_Validating(object sender, CancelEventArgs e)
		{
			if ((sender as CheckedListBox).CheckedItems.Count == 0 )
				this.errorProvider1.SetError((sender as CheckedListBox), "������ ���� ������� ���� �� ���� �������");
			else
				this.errorProvider1.SetError((sender as CheckedListBox), "");

			if (this.SelectedHour() > (_studyLoading.HourByWeek - _studyLoading.UsedHourByWeek))
				this.errorProvider1.SetError(this.button1, "�������� ������ ����� ��� ����������");
			else
				this.errorProvider1.SetError(this.button1, "");
			
		}

		private void button4_Click(object sender, EventArgs e)
		{
			if (this.selectedFlatList.SelectedIndex != -1)
				this.selectedFlatList.Items.RemoveAt( this.selectedFlatList.SelectedIndex );
		}

		private void button5_Click(object sender, EventArgs e)
		{
			this.selectedFlatList.Items.Clear();
		}

		private void treeView1_BeforeExpand(object sender, TreeViewCancelEventArgs e)
		{
			if (e.Node.Nodes.Count == 1)
				if (e.Node.Nodes[0] != null )
					if (e.Node.Nodes[0].Text == "-1")
					{
						// Display a wait cursor while the TreeNodes are being created.
						Cursor.Current = Cursors.WaitCursor;

						e.Node.Nodes.Clear();
						// ��������� ������ �������� �� ���������� ������ �� ������ �����
						ListWrapper _buildingFlatList = new ListWrapper( typeof(LectureHall), (e.Node.Tag as Building).LectureHalls );
						_buildingFlatList.Sort(0);

						//IList filteredLst = _buildingFlatList;
						//if (_studyLoading.StudyForm != null)
						//	if (_studyLoading.StudyForm.MnemoCode == "�.")
						//		foreach (LectureHallType lht in DB4OBridge.theOneObject.ObjectList(typeof(LectureHallType)))
						//			if (lht.MnemoCode == "�.")
						//			{
						//				lsfiltr.AddCondition(LogicOperations.loEMPTY,"HallType","",ConditionOperators.coEqual, lht);
						//				break;
						//			}

						//if (_studyLoading.StudyForm != null)
						//	if (_studyLoading.StudyForm.MnemoCode == "�.")
						//		foreach (LectureHallType lht in DB4OBridge.theOneObject.ObjectList(typeof(LectureHallType)))
						//		{
						//			if (lht.MnemoCode == "�." || lht.MnemoCode == "�.")
						//			{
						//				lsfiltr.AddCondition(LogicOperations.loOR,"HallType","",ConditionOperators.coEqual, lht);
						//			}

						//			if (lht.MnemoCode == "�.")
						//			{
						//				lsfiltr.AddCondition(LogicOperations.loOR,"HallType","",ConditionOperators.coEqual, lht);
						//			}

						//			if (lht.MnemoCode == "��.")
						//			{
						//				lsfiltr.AddCondition(LogicOperations.loOR,"HallType","",ConditionOperators.coEqual, lht);
						//			}
						//		}
				
						//if (_studyLoading.StudyForm != null)
						//	if (_studyLoading.StudyForm.MnemoCode == "��.")
						//		foreach (LectureHallType lht in DB4OBridge.theOneObject.ObjectList(typeof(LectureHallType)))
						//		{
						//			if (lht.MnemoCode == "��.")
						//			{
						//				lsfiltr.AddCondition(LogicOperations.loOR,"HallType","",ConditionOperators.coEqual, lht);
						//			}

						//			if (lht.MnemoCode == "�.")
						//			{
						//				lsfiltr.AddCondition(LogicOperations.loOR,"HallType","",ConditionOperators.coEqual, lht);
						//			}	
						//		}
				
						//filteredLst = lsfiltr; //.Filter( _buildingFlatList );


						// ������ ����� �� ������
						foreach(LectureHall lh in _buildingFlatList)
						{
							TreeNode _flatTreeNode = new TreeNode();
							float _usingPercent = 0; 
							
							ExtObjKey key = new ExtObjKey();
							key.UID = this._storage.GetUUID(lh, out key.Signature, out key.Index);

							int _lessonCount = _shmatr.GetFlatLessons(key).Count;
							
							_usingPercent = (_lessonCount*100)/(_shmatr.GetWeeksCount()*_shmatr.GetDaysPerWeekCount()*_shmatr.GetPairsPerDayCount());														
							_flatTreeNode.Text = lh.ToString()+" - "+_usingPercent.ToString()+"%";
							_flatTreeNode.Tag = lh;


					
							bool isEveryWereFree = true;
							foreach( StudyDay sd in this.checkedListDays.CheckedItems)
								foreach( StudyHour sp in this.checkedListPairs.CheckedItems)
									foreach( StudyCycles sc in this.checkedListWeeks.CheckedItems)
									{
										int _sheduleKey = this._shmatr.GetKey( int.Parse(sc.MnemoCode), int.Parse(sd.MnemoCode), int.Parse(sp.MnemoCode) );
										isEveryWereFree = isEveryWereFree && (this._shmatr.FlatBusyCheck(_sheduleKey, key) == FlatBusyCheckerState.isNotBusy);
									}

							if (isEveryWereFree)
							{
								_flatTreeNode.ImageIndex = 1;
								_flatTreeNode.SelectedImageIndex = 1;
							}
							else
							{
								_flatTreeNode.ImageIndex = 2;
								_flatTreeNode.SelectedImageIndex = 2;
							}

							e.Node.Nodes.Add( _flatTreeNode );
						}
						// Reset the cursor to the default for all controls.
						Cursor.Current = Cursors.Default;
					}
		}

		private void label2_Click(object sender, EventArgs e)
		{		
		}

		public IList SelectedFlatList
		{
			get { return this.selectedFlatList.Items; }
		}

		public IList SelectedDayList
		{
			get { return this.checkedListDays.CheckedItems; }
		}

		public IList SelectedWeekList
		{
			get { return this.checkedListWeeks.CheckedItems; }
		}

		public IList SelectedPairList
		{
			get { return this.checkedListPairs.CheckedItems; }
		}

		public bool CanChangeTime
		{
			get { return timeGroupBox.Enabled; }
			set { timeGroupBox.Enabled = value; }
		}

		public bool CanChangeFlat
		{
			get { return this.tabPage1.Enabled; }
			set { this.tabPage1.Enabled = value; }
		}
	
		public void Check()
		{
			// ���� ������� 705
			if(this._studyLoading != null)
				if(this._studyLoading.Dept != null)
					if(this._studyLoading.Dept.MnemoCode == "705")
					{
						this.button1.Enabled = true;
						return;
					}
			// ���� ������ �������� �� ������
			if (this.selectedFlatList.Items.Count != 0)
			{	
				this.button1.Enabled = true;
				return;
			}
			// � ����� ��������
			this.button1.Enabled = false;
		}

		private void buttonSearchFlat_Click(object sender, System.EventArgs e)
		{
			Cursor.Current = Cursors.WaitCursor;

			/* ����� ������� */
			treeSearchFlat.Nodes.Clear();

			foreach (Building build in _buildingList)
			{
				TreeNode buildNode = new TreeNode(build.MnemoCode);
				buildNode.ImageIndex = 0;
				buildNode.SelectedImageIndex = 0;

				IList findedFlats = new ArrayList();
				foreach(LectureHall lh in  build.LectureHalls)
					if(lh != null)
					{
						ExtObjKey key = new ExtObjKey();
						key.UID = this._storage.GetUUID(lh, out key.Signature, out key.Index);

						if (allRadioButton.Checked)
						{
							bool isEveryWereFree = true;							
							foreach( StudyDay sd in this.checkedListDays.CheckedItems)
								if(!isEveryWereFree) break;
								else
									foreach( StudyHour sp in this.checkedListPairs.CheckedItems)
										if(!isEveryWereFree) break;
										else
											foreach( StudyCycles sc in this.checkedListWeeks.CheckedItems)													
												if(!isEveryWereFree) break;
												else
												{
													int _sheduleKey = this._shmatr.GetKey( int.Parse(sc.MnemoCode), int.Parse(sd.MnemoCode), int.Parse(sp.MnemoCode) );
													isEveryWereFree = isEveryWereFree && this._shmatr.FlatBusyCheck(_sheduleKey, key) == FlatBusyCheckerState.isNotBusy;													
												}
							
							if (isEveryWereFree && !findedFlats.Contains(lh))
								findedFlats.Add( lh );										
						}
						else
							if (notAllRadioButton.Checked)
							{
								foreach( StudyDay sd in this.checkedListDays.CheckedItems)
									foreach( StudyHour sp in this.checkedListPairs.CheckedItems)
										foreach( StudyCycles sc in this.checkedListWeeks.CheckedItems)
										{
											int _sheduleKey = this._shmatr.GetKey( int.Parse(sc.MnemoCode), int.Parse(sd.MnemoCode), int.Parse(sp.MnemoCode) );
											if (this._shmatr.FlatBusyCheck(_sheduleKey, key) == FlatBusyCheckerState.isNotBusy)
												if (!findedFlats.Contains(lh))
													findedFlats.Add( lh );																					
										}
							}
					}
				
				foreach(LectureHall lh in findedFlats)
				{
					TreeNode lhNode = new TreeNode();					
					lhNode.Text = lh.ToString();
					lhNode.ImageIndex = -1;
					lhNode.SelectedImageIndex = -1;					
					lhNode.Tag = lh;

					/* ����� �� ������*/
					buildNode.Nodes.Add( lhNode );
				}

				treeSearchFlat.Nodes.Add(buildNode);
			}
			Cursor.Current = Cursors.Default;
		}

		private void button6_Click(object sender, System.EventArgs e)
		{
			/* ���� ����� � ��������� ��������*/
			TreeNode _node = treeSearchFlat.SelectedNode;
			if (_node.Tag is LectureHall)
			{
				// �������� �� ��������� 
				ExtObjKey key = new ExtObjKey();
				key.UID = this._storage.GetUUID(_node.Tag, out key.Signature, out key.Index);

				bool isEveryWereFree = true;
				foreach( StudyDay sd in this.checkedListDays.CheckedItems)
					foreach( StudyHour sp in this.checkedListPairs.CheckedItems)
						foreach( StudyCycles sc in this.checkedListWeeks.CheckedItems)
						{
							int _sheduleKey = this._shmatr.GetKey( int.Parse(sc.MnemoCode), int.Parse(sd.MnemoCode), int.Parse(sp.MnemoCode) );
							isEveryWereFree = isEveryWereFree && (this._shmatr.FlatBusyCheck(_sheduleKey, key) == FlatBusyCheckerState.isNotBusy);
						}

				if (isEveryWereFree)
					if ( !this.selectedFlatList.Items.Contains( _node.Tag ) )
						this.selectedFlatList.Items.Add( _node.Tag );

				if (!isEveryWereFree)
					if( MessageBox.Show(this, Vocabruary.Translate("Message.CanUseFlatText"), Vocabruary.Translate("Message.WarningTitle"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
						if ( !this.selectedFlatList.Items.Contains( _node.Tag ) )
							this.selectedFlatList.Items.Add( _node.Tag );
			}

			
			if (this.selectedFlatList.Items.Count == 0)
				this.button1.Enabled = false;
			else
				this.button1.Enabled = true;

        }

        private void treeFlats_NodeMouseHover(object sender, TreeNodeMouseHoverEventArgs e)
        {
            if (e.Node != null && e.Node.Nodes.Count == 0)
            {
                LectureHall lh = (e.Node.Tag as LectureHall);
            	if(lh!=null)
                	if(lh.Department != null)
            			toolTip1.SetToolTip(sender as TreeView, lh.Department.MnemoCode);
            		else
                        toolTip1.SetToolTip(sender as TreeView, Vocabruary.Translate("ToolTip.TotalFlat"));
            }
        }

        public void SetRecomendation(string recomendationText)
        {
            if (ExpertRecomend.Text != "")
                ExpertRecomend.Text += "\n\t";

            ExpertRecomend.Text += recomendationText;
        }
	}
}
