﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Reflection;
using System.Windows.Forms;
using Shturman.MultyLanguage;
using System.Configuration;
using Shturman.Nestor.DataEditors;
using Shturman.Nestor.DataLists;
using Shturman.Nestor.Interfaces;
using Shturman.Shedule.Objects;
using Shturman.Shedule.Commands;
using Shturman.Shedule.Checkers;

using GUI = Shturman.Shedule.GUI;
using Shturman.Shedule;
using Shturman.Shedule.Server.Interface;
using Shturman.Shedule.Server.Remoting;

using Shturman.Sherlock.XLogic;
using System.Xml.XPath;

namespace Shturman.Shedule.Forms
{
    /// <summary>
    /// Summary description for SheduleView.
    /// </summary>
    public class SheduleBrowser : Form
    {
        private IContainer components;
        private DataGrid leadingGrid;
        private ImageList imageList1;

        private IObjectStorage objectFactory;
        private IShedule shmatr;
        private IList groupList;
        private object _dept = null;
        private IList buildingList;

        private IList weekCycles;
        private IList weekDays;
        private IList dayPairs;

        private IList leadingList;
        private ListFiltrator lsfiltr = new ListFiltrator();
        private ArrayList DetailTypes = new ArrayList();
        private ArrayList DetailPropList = new ArrayList();
        private ListBox listBox2;
        private Panel panel2;
        private GUI.GroupSheduleGrid groupSheduleView1;
        private Panel panel1;
        private Panel panel4;
        private Label label1;
        private Label label2;
        private LinkLabel linkLabel1;
        private LinkLabel linkLabel2;
        private LinkLabel linkLabel3;
        private MenuItem menuItem1;
        private MenuItem menuItem2;
        private MenuItem menuItem3;
        private MenuItem menuItem4;
        private MenuItem menuItem5;
        private MenuItem menuItem6;
        private ContextMenu SupportMenu;
        private GUI.RestrictSheduleView SheduleNav;
        private MenuItem menuItem7;
        private MenuItem menuItem8;
        private ContextMenu contextEditMenu;
        private LinkLabel linkLabel7;
        private System.Windows.Forms.ContextMenu contextMenuDelLoading;
        private System.Windows.Forms.MenuItem menuDelLessons;
        private System.Windows.Forms.MenuItem menuItemDelLoading;
        private ToolStrip toolStrip1;
        private ToolStripButton addLessonBtn;
        private ToolStripDropDownButton editLessonBtn;
        private ToolStripButton dropLessonBtn;
        private ToolStripSeparator toolStripSeparator1;
        private ToolStripButton undoButton;
        private ToolStripButton redoButton;
        private ToolStripSeparator toolStripSeparator2;
        private ToolStripLabel scaleLabel;
        private ToolStripComboBox scaleComboBox;
        private ToolStripMenuItem змінитиАудиторіюToolStripMenuItem;
        private ToolStripMenuItem перенестиToolStripMenuItem;
        private Panel panel3;
        private BackgroundWorker backgroundWorker1;

        private ListBox subObjectList;

        private XPathDocument xPathDoc;
        private ExpSysFindFlat expSysFindFlat = new ExpSysFindFlat();
        private ExpSysFindPair expSysFindPair = new ExpSysFindPair();
        private RuleContext ruleContext = new RuleContext();

        public SheduleBrowser(IObjectStorage objectFactory, IShedule shedule, IList groupList)
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            this.objectFactory = objectFactory;
            this.shmatr = shedule;
            this.groupList = groupList;

            this.buildingList = objectFactory.ObjectList(typeof(Building));

            this.weekCycles = objectFactory.ObjectList(typeof(StudyCycles));
            this.weekDays = objectFactory.ObjectList(typeof(StudyDay));
            // скорочуємо до шести днів
            this.weekDays.RemoveAt(this.weekDays.Count - 1);
            this.dayPairs = objectFactory.ObjectList(typeof(StudyHour));

            this.groupSheduleView1.LoadShedule(weekDays, dayPairs, weekCycles);

            IList uidList = new ArrayList();
            foreach (Group gr in groupList)
                uidList.Add(gr.ObjectHardKey);
            ExtObjKey[] uidListArray = new ExtObjKey[uidList.Count];
            uidList.CopyTo(uidListArray, 0);

            IList filteredStudyLeading = RemoteAdapter.Adapter.AdaptLeadingList(shmatr.GetStudyLeadingByGroups(uidListArray));

            this.leadingList = new ListWrapper(typeof(StudyLoading), filteredStudyLeading);
            this.leadingGrid.DataSource = leadingList;
            this.leadingGrid.TableStyles.Clear();
            this.leadingGrid.TableStyles.Add(CreateTableStyle());

            xPathDoc = new XPathDocument(ConfigurationManager.AppSettings["XLogicBaseFile"]);

            DetailTypes.Clear();
            DetailPropList.Clear();
        }

        public SheduleBrowser(IObjectStorage objectFactory, IShedule shedule, object dept)
        {
            InitializeComponent();

            this.objectFactory = objectFactory;
            this.shmatr = shedule;
            this._dept = dept;

            this.buildingList = objectFactory.ObjectList(typeof(Building));
            this.weekCycles = objectFactory.ObjectList(typeof(StudyCycles));
            this.weekDays = objectFactory.ObjectList(typeof(StudyDay));
            // скорочуємо до шести днів
            this.weekDays.RemoveAt(this.weekDays.Count - 1);
            this.dayPairs = objectFactory.ObjectList(typeof(StudyHour));

            this.groupSheduleView1.LoadShedule(weekDays, dayPairs, weekCycles);

            IList filteredStudyLeading = RemoteAdapter.Adapter.AdaptLeadingList(shmatr.GetStudyLeadingByDept((dept as Department).ObjectHardKey));

            this.leadingList = new ListWrapper(typeof(StudyLoading), filteredStudyLeading);
            this.leadingGrid.DataSource = leadingList;
            this.leadingGrid.TableStyles.Clear();
            this.leadingGrid.TableStyles.Add(CreateTableStyle());

            DetailTypes.Clear();
            DetailPropList.Clear();
        }

        public void RefreshStudyLeading()
        {
            IList uidList = new ArrayList();
            foreach (Group gr in groupList)
                uidList.Add(gr.ObjectHardKey);
            ExtObjKey[] uidListArray = new ExtObjKey[uidList.Count];
            uidList.CopyTo(uidListArray, 0);

            IList filteredStudyLeading = RemoteAdapter.Adapter.AdaptLeadingList(shmatr.GetStudyLeadingByGroups(uidListArray));

            this.leadingList = new ListWrapper(typeof(StudyLoading), filteredStudyLeading);
            this.leadingGrid.DataSource = leadingList;
        }

        protected override void OnResize(EventArgs e)
        {
            base.OnResize(e);

            if (weekDays != null && dayPairs != null && weekCycles != null)
            {
                this.SheduleNav.CellWidth = 18;
                this.SheduleNav.CellHeight = (this.SheduleNav.Height / 13);
                this.SheduleNav.ChangeCellSize();
            }
        }

        /// <summary>
        /// Создание стиля для отображения информации о нагрузке
        /// </summary>
        /// <returns></returns>
        DataGridTableStyle CreateTableStyle()
        {
            GUI.DataGridColorEventHandler colorEvent = new GUI.DataGridColorEventHandler(DataGridEx_CellPaint);

            GUI.DataGridColoredTextBoxColumn coloredColumn1 = new GUI.DataGridColoredTextBoxColumn();
            coloredColumn1.MappingName = "Дисципліна";
            coloredColumn1.HeaderText = "Дисципліна";
            coloredColumn1.CellPaint += colorEvent;

            GUI.DataGridColoredTextBoxColumn coloredColumn2 = new GUI.DataGridColoredTextBoxColumn();
            coloredColumn2.MappingName = "Тип заняття";
            coloredColumn2.HeaderText = "Тип заняття";
            coloredColumn2.CellPaint += colorEvent;

            GUI.DataGridColoredTextBoxColumn coloredColumn3 = new GUI.DataGridColoredTextBoxColumn();
            coloredColumn3.MappingName = "Кафедра";
            coloredColumn3.HeaderText = "Кафедра";
            coloredColumn3.CellPaint += colorEvent;

            GUI.DataGridColoredTextBoxColumn coloredColumn4 = new GUI.DataGridColoredTextBoxColumn();
            coloredColumn4.MappingName = "Год./тиж.";
            coloredColumn4.HeaderText = "Год./тиж.";
            coloredColumn4.CellPaint += colorEvent;

            GUI.DataGridColoredTextBoxColumn coloredColumn5 = new GUI.DataGridColoredTextBoxColumn();
            coloredColumn5.MappingName = "Проставлено";
            coloredColumn5.HeaderText = "Проставлено";
            coloredColumn5.CellPaint += colorEvent;

            GUI.DataGridColoredTextBoxColumn coloredColumn6 = new GUI.DataGridColoredTextBoxColumn();
            coloredColumn6.MappingName = "Групи";
            coloredColumn6.HeaderText = "Групи";
            coloredColumn6.CellPaint += colorEvent;

            DataGridTableStyle tableStyle = new DataGridTableStyle();
            tableStyle.MappingName = leadingGrid.DataSource.GetType().Name;
            tableStyle.GridColumnStyles.AddRange(new DataGridColumnStyle[] { coloredColumn1, coloredColumn2, coloredColumn3, coloredColumn4, coloredColumn5, coloredColumn6 });

            return tableStyle;
        }

        /// <summary>
        /// Метод отрисовки 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void DataGridEx_CellPaint(object sender, GUI.DataGridColorlEventArgs e)
        {
            if ((leadingList[e.Row] as StudyLoading).UsedHourByWeek >= (leadingList[e.Row] as StudyLoading).HourByWeek)
            {
                if (leadingGrid.CurrentRowIndex == e.Row)
                {
                    e.BackBrush = Brushes.DarkBlue;
                    e.ForeBrush = Brushes.White;
                }
                else
                {
                    e.BackBrush = Brushes.LightBlue;
                    e.ForeBrush = Brushes.Black;
                }
            }
            else
            {
                if (leadingGrid.CurrentRowIndex == e.Row)
                {
                    e.BackBrush = Brushes.DarkBlue;
                    e.ForeBrush = Brushes.White;
                }
                else
                {
                    e.BackBrush = Brushes.GreenYellow;
                    e.ForeBrush = Brushes.Black;
                }
            }
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SheduleBrowser));
            this.leadingGrid = new System.Windows.Forms.DataGrid();
            this.SupportMenu = new System.Windows.Forms.ContextMenu();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.menuItem5 = new System.Windows.Forms.MenuItem();
            this.menuItem6 = new System.Windows.Forms.MenuItem();
            this.subObjectList = new System.Windows.Forms.ListBox();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.listBox2 = new System.Windows.Forms.ListBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.linkLabel7 = new System.Windows.Forms.LinkLabel();
            this.linkLabel3 = new System.Windows.Forms.LinkLabel();
            this.contextMenuDelLoading = new System.Windows.Forms.ContextMenu();
            this.menuDelLessons = new System.Windows.Forms.MenuItem();
            this.menuItemDelLoading = new System.Windows.Forms.MenuItem();
            this.linkLabel2 = new System.Windows.Forms.LinkLabel();
            this.linkLabel1 = new System.Windows.Forms.LinkLabel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.contextEditMenu = new System.Windows.Forms.ContextMenu();
            this.menuItem7 = new System.Windows.Forms.MenuItem();
            this.menuItem8 = new System.Windows.Forms.MenuItem();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.addLessonBtn = new System.Windows.Forms.ToolStripButton();
            this.editLessonBtn = new System.Windows.Forms.ToolStripDropDownButton();
            this.змінитиАудиторіюToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.перенестиToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.dropLessonBtn = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.undoButton = new System.Windows.Forms.ToolStripButton();
            this.redoButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.scaleComboBox = new System.Windows.Forms.ToolStripComboBox();
            this.scaleLabel = new System.Windows.Forms.ToolStripLabel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.groupSheduleView1 = new Shturman.Shedule.GUI.GroupSheduleGrid();
            this.SheduleNav = new Shturman.Shedule.GUI.RestrictSheduleView();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.leadingGrid)).BeginInit();
            this.panel2.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // leadingGrid
            // 
            this.leadingGrid.AllowDrop = true;
            this.leadingGrid.AllowNavigation = false;
            this.leadingGrid.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.leadingGrid.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.leadingGrid.CaptionBackColor = System.Drawing.SystemColors.Control;
            this.leadingGrid.CaptionForeColor = System.Drawing.SystemColors.ControlText;
            this.leadingGrid.CaptionText = "Нагрузка";
            this.leadingGrid.ContextMenu = this.SupportMenu;
            this.leadingGrid.DataMember = "";
            this.leadingGrid.Dock = System.Windows.Forms.DockStyle.Fill;
            this.leadingGrid.HeaderForeColor = System.Drawing.SystemColors.ControlText;
            this.leadingGrid.Location = new System.Drawing.Point(0, 0);
            this.leadingGrid.Name = "leadingGrid";
            this.leadingGrid.Size = new System.Drawing.Size(813, 227);
            this.leadingGrid.TabIndex = 0;
            this.leadingGrid.Enter += new System.EventHandler(this.leadingGrid_Enter);
            this.leadingGrid.CurrentCellChanged += new System.EventHandler(this.leadingGrid_CurrentCellChanged);
            this.leadingGrid.MouseMove += new System.Windows.Forms.MouseEventHandler(this.leadingGrid_MouseMove);
            this.leadingGrid.BindingContextChanged += new System.EventHandler(this.leadingGrid_BindingContextChanged);
            this.leadingGrid.Leave += new System.EventHandler(this.leadingGrid_Leave);
            // 
            // SupportMenu
            // 
            this.SupportMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuItem1,
this.menuItem4});
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 0;
            this.menuItem1.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuItem2,
this.menuItem3});
            this.menuItem1.Text = "Подобрать";
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 0;
            this.menuItem2.Text = "Время -> Аудиторию";
            this.menuItem2.Click += new System.EventHandler(this.menuItem2_Click);
            // 
            // menuItem3
            // 
            this.menuItem3.Index = 1;
            this.menuItem3.Text = "Аудиторию -> Время";
            // 
            // menuItem4
            // 
            this.menuItem4.Index = 1;
            this.menuItem4.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuItem5,
this.menuItem6});
            this.menuItem4.Text = "Консультация";
            // 
            // menuItem5
            // 
            this.menuItem5.Index = 0;
            this.menuItem5.Text = "По времени";
            this.menuItem5.Click += new System.EventHandler(this.menuItem5_Click);
            // 
            // menuItem6
            // 
            this.menuItem6.Index = 1;
            this.menuItem6.Text = "По аудитории";
            this.menuItem6.Click += new System.EventHandler(this.menuItem6_Click);
            // 
            // subObjectList
            // 
            this.subObjectList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.subObjectList.BackColor = System.Drawing.Color.WhiteSmoke;
            this.subObjectList.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.subObjectList.IntegralHeight = false;
            this.subObjectList.Location = new System.Drawing.Point(8, 20);
            this.subObjectList.Name = "subObjectList";
            this.subObjectList.Size = new System.Drawing.Size(136, 92);
            this.subObjectList.TabIndex = 1;
            // 
            // imageList1
            // 
            this.imageList1.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList1.ImageStream")));
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList1.Images.SetKeyName(0, "");
            this.imageList1.Images.SetKeyName(1, "");
            this.imageList1.Images.SetKeyName(2, "");
            this.imageList1.Images.SetKeyName(3, "");
            this.imageList1.Images.SetKeyName(4, "");
            this.imageList1.Images.SetKeyName(5, "");
            this.imageList1.Images.SetKeyName(6, "");
            this.imageList1.Images.SetKeyName(7, "");
            this.imageList1.Images.SetKeyName(8, "");
            // 
            // listBox2
            // 
            this.listBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.listBox2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.listBox2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.listBox2.IntegralHeight = false;
            this.listBox2.Location = new System.Drawing.Point(8, 131);
            this.listBox2.Name = "listBox2";
            this.listBox2.Size = new System.Drawing.Size(136, 92);
            this.listBox2.TabIndex = 11;
            this.listBox2.Leave += new System.EventHandler(this.listBox2_Leave);
            this.listBox2.Enter += new System.EventHandler(this.listBox2_Enter);
            this.listBox2.SelectedIndexChanged += new System.EventHandler(this.listBox2_SelectedIndexChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.panel4);
            this.panel2.Controls.Add(this.panel1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel2.Location = new System.Drawing.Point(0, 297);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(965, 227);
            this.panel2.TabIndex = 11;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.linkLabel7);
            this.panel4.Controls.Add(this.linkLabel3);
            this.panel4.Controls.Add(this.linkLabel2);
            this.panel4.Controls.Add(this.linkLabel1);
            this.panel4.Controls.Add(this.leadingGrid);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(0, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(813, 227);
            this.panel4.TabIndex = 13;
            // 
            // linkLabel7
            // 
            this.linkLabel7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabel7.BackColor = System.Drawing.SystemColors.Control;
            this.linkLabel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.linkLabel7.ForeColor = System.Drawing.SystemColors.ControlText;
            this.linkLabel7.LinkArea = new System.Windows.Forms.LinkArea(0, 15);
            this.linkLabel7.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.linkLabel7.LinkColor = System.Drawing.SystemColors.ControlText;
            this.linkLabel7.Location = new System.Drawing.Point(731, 4);
            this.linkLabel7.Name = "linkLabel7";
            this.linkLabel7.Size = new System.Drawing.Size(75, 14);
            this.linkLabel7.TabIndex = 4;
            this.linkLabel7.TabStop = true;
            this.linkLabel7.Text = "Обновить";
            this.linkLabel7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.linkLabel7.UseCompatibleTextRendering = true;
            this.linkLabel7.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel7_LinkClicked);
            // 
            // linkLabel3
            // 
            this.linkLabel3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabel3.BackColor = System.Drawing.SystemColors.Control;
            this.linkLabel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.linkLabel3.ContextMenu = this.contextMenuDelLoading;
            this.linkLabel3.LinkArea = new System.Windows.Forms.LinkArea(0, 15);
            this.linkLabel3.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.linkLabel3.LinkColor = System.Drawing.SystemColors.ControlText;
            this.linkLabel3.Location = new System.Drawing.Point(653, 4);
            this.linkLabel3.Name = "linkLabel3";
            this.linkLabel3.Size = new System.Drawing.Size(75, 14);
            this.linkLabel3.TabIndex = 3;
            this.linkLabel3.TabStop = true;
            this.linkLabel3.Text = "Удалить";
            this.linkLabel3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.linkLabel3.UseCompatibleTextRendering = true;
            this.linkLabel3.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel3_LinkClicked);
            // 
            // contextMenuDelLoading
            // 
            this.contextMenuDelLoading.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuDelLessons,
this.menuItemDelLoading});
            // 
            // menuDelLessons
            // 
            this.menuDelLessons.Index = 0;
            this.menuDelLessons.Text = "Видилити заняття";
            this.menuDelLessons.Click += new System.EventHandler(this.menuDelLessons_Click);
            // 
            // menuItemDelLoading
            // 
            this.menuItemDelLoading.Index = 1;
            this.menuItemDelLoading.Text = "Видилити навантаження";
            this.menuItemDelLoading.Click += new System.EventHandler(this.menuItemDelLoading_Click);
            // 
            // linkLabel2
            // 
            this.linkLabel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabel2.BackColor = System.Drawing.SystemColors.Control;
            this.linkLabel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.linkLabel2.LinkArea = new System.Windows.Forms.LinkArea(0, 15);
            this.linkLabel2.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.linkLabel2.LinkColor = System.Drawing.SystemColors.ControlText;
            this.linkLabel2.Location = new System.Drawing.Point(563, 4);
            this.linkLabel2.Name = "linkLabel2";
            this.linkLabel2.Size = new System.Drawing.Size(88, 14);
            this.linkLabel2.TabIndex = 2;
            this.linkLabel2.TabStop = true;
            this.linkLabel2.Text = "Редактировать";
            this.linkLabel2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.linkLabel2.UseCompatibleTextRendering = true;
            this.linkLabel2.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel2_LinkClicked);
            // 
            // linkLabel1
            // 
            this.linkLabel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabel1.BackColor = System.Drawing.SystemColors.Control;
            this.linkLabel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.linkLabel1.LinkArea = new System.Windows.Forms.LinkArea(0, 15);
            this.linkLabel1.LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
            this.linkLabel1.LinkColor = System.Drawing.SystemColors.ControlText;
            this.linkLabel1.Location = new System.Drawing.Point(485, 4);
            this.linkLabel1.Name = "linkLabel1";
            this.linkLabel1.Size = new System.Drawing.Size(75, 14);
            this.linkLabel1.TabIndex = 1;
            this.linkLabel1.TabStop = true;
            this.linkLabel1.Text = "Добавить";
            this.linkLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.linkLabel1.UseCompatibleTextRendering = true;
            this.linkLabel1.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.linkLabel1_LinkClicked);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.subObjectList);
            this.panel1.Controls.Add(this.listBox2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(813, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(152, 227);
            this.panel1.TabIndex = 12;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.Location = new System.Drawing.Point(8, 117);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 12);
            this.label2.TabIndex = 13;
            this.label2.Text = "Преподаватели";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.Location = new System.Drawing.Point(8, 4);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 15);
            this.label1.TabIndex = 12;
            this.label1.Text = "Группы";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // contextEditMenu
            // 
            this.contextEditMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuItem7,
this.menuItem8});
            // 
            // menuItem7
            // 
            this.menuItem7.Index = 0;
            this.menuItem7.Text = "Изменить аудиторию";
            this.menuItem7.Click += new System.EventHandler(this.menuItem7_Click);
            // 
            // menuItem8
            // 
            this.menuItem8.Index = 1;
            this.menuItem8.Text = "Перенести";
            this.menuItem8.Click += new System.EventHandler(this.menuItem8_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
this.addLessonBtn,
this.editLessonBtn,
this.dropLessonBtn,
this.toolStripSeparator1,
this.undoButton,
this.redoButton,
this.toolStripSeparator2,
this.scaleComboBox,
this.scaleLabel});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(965, 25);
            this.toolStrip1.TabIndex = 20;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // addLessonBtn
            // 
            this.addLessonBtn.Image = ((System.Drawing.Image)(resources.GetObject("addLessonBtn.Image")));
            this.addLessonBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addLessonBtn.Name = "addLessonBtn";
            this.addLessonBtn.Size = new System.Drawing.Size(66, 22);
            this.addLessonBtn.Text = "Додати";
            this.addLessonBtn.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // editLessonBtn
            // 
            this.editLessonBtn.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
this.змінитиАудиторіюToolStripMenuItem,
this.перенестиToolStripMenuItem});
            this.editLessonBtn.Image = ((System.Drawing.Image)(resources.GetObject("editLessonBtn.Image")));
            this.editLessonBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.editLessonBtn.Name = "editLessonBtn";
            this.editLessonBtn.Size = new System.Drawing.Size(96, 22);
            this.editLessonBtn.Text = "Редагувати";
            // 
            // змінитиАудиторіюToolStripMenuItem
            // 
            this.змінитиАудиторіюToolStripMenuItem.Name = "змінитиАудиторіюToolStripMenuItem";
            this.змінитиАудиторіюToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.змінитиАудиторіюToolStripMenuItem.Text = "Змінити аудиторію";
            this.змінитиАудиторіюToolStripMenuItem.Click += new System.EventHandler(this.menuItem7_Click);
            // 
            // перенестиToolStripMenuItem
            // 
            this.перенестиToolStripMenuItem.Name = "перенестиToolStripMenuItem";
            this.перенестиToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.перенестиToolStripMenuItem.Text = "Перенести";
            this.перенестиToolStripMenuItem.Click += new System.EventHandler(this.menuItem8_Click);
            // 
            // dropLessonBtn
            // 
            this.dropLessonBtn.Image = ((System.Drawing.Image)(resources.GetObject("dropLessonBtn.Image")));
            this.dropLessonBtn.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.dropLessonBtn.Name = "dropLessonBtn";
            this.dropLessonBtn.Size = new System.Drawing.Size(76, 22);
            this.dropLessonBtn.Text = "Видалити";
            this.dropLessonBtn.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // undoButton
            // 
            this.undoButton.Enabled = false;
            this.undoButton.Image = ((System.Drawing.Image)(resources.GetObject("undoButton.Image")));
            this.undoButton.ImageTransparentColor = System.Drawing.Color.White;
            this.undoButton.Name = "undoButton";
            this.undoButton.Size = new System.Drawing.Size(74, 22);
            this.undoButton.Text = "Відмінити";
            this.undoButton.Click += new System.EventHandler(this.button1_Click);
            // 
            // redoButton
            // 
            this.redoButton.Enabled = false;
            this.redoButton.Image = ((System.Drawing.Image)(resources.GetObject("redoButton.Image")));
            this.redoButton.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.redoButton.ImageTransparentColor = System.Drawing.Color.White;
            this.redoButton.Name = "redoButton";
            this.redoButton.Size = new System.Drawing.Size(82, 22);
            this.redoButton.Text = "Повернути";
            this.redoButton.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage;
            this.redoButton.Click += new System.EventHandler(this.button2_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // scaleComboBox
            // 
            this.scaleComboBox.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.scaleComboBox.Items.AddRange(new object[] {
"10%",
"20%",
"30%",
"40%",
"50%",
"60%",
"70%",
"80%",
"90%",
"100%",
"110%",
"120%",
"130%",
"140%"});
            this.scaleComboBox.Name = "scaleComboBox";
            this.scaleComboBox.Size = new System.Drawing.Size(100, 25);
            this.scaleComboBox.SelectedIndexChanged += new System.EventHandler(this.scaleComboBox_SelectedIndexChanged);
            // 
            // scaleLabel
            // 
            this.scaleLabel.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.scaleLabel.Name = "scaleLabel";
            this.scaleLabel.Size = new System.Drawing.Size(51, 22);
            this.scaleLabel.Text = "Маштаб:";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.groupSheduleView1);
            this.panel3.Controls.Add(this.SheduleNav);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 25);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(965, 272);
            this.panel3.TabIndex = 21;
            // 
            // groupSheduleView1
            // 
            this.groupSheduleView1.AllowDrop = true;
            this.groupSheduleView1.AutoScrollOffset = new System.Drawing.Point(799, 0);
            this.groupSheduleView1.AutoSize = true;
            this.groupSheduleView1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.groupSheduleView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupSheduleView1.Location = new System.Drawing.Point(0, 0);
            this.groupSheduleView1.Name = "groupSheduleView1";
            this.groupSheduleView1.Size = new System.Drawing.Size(813, 272);
            this.groupSheduleView1.TabIndex = 0;
            this.groupSheduleView1.Click += new System.EventHandler(this.groupSheduleView1_Click);
            this.groupSheduleView1.DragDrop += new System.Windows.Forms.DragEventHandler(this.groupSheduleView1_DragDrop);
            this.groupSheduleView1.DragEnter += new System.Windows.Forms.DragEventHandler(this.groupSheduleView1_DragEnter);
            this.groupSheduleView1.SizeChanged += new System.EventHandler(this.groupSheduleView1_SizeChanged);
            // 
            // SheduleNav
            // 
            this.SheduleNav.CellHeight = 16;
            this.SheduleNav.CellWidth = 16;
            this.SheduleNav.Dock = System.Windows.Forms.DockStyle.Right;
            this.SheduleNav.Location = new System.Drawing.Point(813, 0);
            this.SheduleNav.Name = "SheduleNav";
            this.SheduleNav.Size = new System.Drawing.Size(152, 272);
            this.SheduleNav.TabIndex = 0;
            this.SheduleNav.Resize += new System.EventHandler(this.groupSheduleView1_SizeChanged);
            this.SheduleNav.MouseUp += new System.Windows.Forms.MouseEventHandler(this.SheduleNav_MouseUp);
            // 
            // SheduleBrowser
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(965, 524);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.panel2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SheduleBrowser";
            this.Text = "Расписание занятий";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SheduleView_KeyDown);
            this.Load += new System.EventHandler(this.SheduleView_Load);
            ((System.ComponentModel.ISupportInitialize)(this.leadingGrid)).EndInit();
            this.panel2.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        private void SheduleView_Load(object sender, EventArgs e)
        {
            leadingGrid_BindingContextChanged(leadingGrid, e);

            // select a 100% scale for shedule grid
            scaleComboBox.SelectedIndex = 9;

            this.SheduleNav.CellWidth = 18;
            this.SheduleNav.CellHeight = (this.SheduleNav.Height / 13);
            this.SheduleNav.LoadShedule(weekDays, dayPairs, weekCycles);

            // show shedule by existing in lesson "refference list groups" group
            leadingGrid_CurrentCellChanged(leadingGrid, e);

            // Установка заголовков на текущем языке
            this.Text = Vocabruary.Translate("SheduleBrowser.Title");
            this.leadingGrid.CaptionText = Vocabruary.Translate("SheduleBrowser.StudyLeading");
            //this.label4.Text = Vocabruary.Translate("SheduleBrowser.SheduleByGroup");
            this.label2.Text = Vocabruary.Translate("SheduleBrowser.TutorTitle");
            this.label1.Text = Vocabruary.Translate("SheduleBrowser.GroupTitle");
            this.scaleLabel.Text = Vocabruary.Translate("SheduleBrowser.Scale");

            this.linkLabel1.Text = this.addLessonBtn.Text = Vocabruary.Translate("Button.Add");
            this.linkLabel2.Text = this.editLessonBtn.Text = Vocabruary.Translate("Button.Edit");
            this.linkLabel3.Text = this.dropLessonBtn.Text = Vocabruary.Translate("Button.Drop");
            this.linkLabel7.Text = Vocabruary.Translate("Button.Update");

            this.menuItem1.Text = Vocabruary.Translate("SheduleBrowser.ChooseMenu");
            this.menuItem2.Text = this.menuItem5.Text = Vocabruary.Translate("SheduleBrowser.ByTimeMenu");
            this.menuItem3.Text = this.menuItem6.Text = Vocabruary.Translate("SheduleBrowser.ByFlatMenu");
            this.menuItem4.Text = Vocabruary.Translate("SheduleBrowser.ConsultMenu");

            this.menuItem7.Text = Vocabruary.Translate("SheduleBrowser.ChangeFlatMenu");
            this.menuItem8.Text = Vocabruary.Translate("SheduleBrowser.MoveLessonMenu");

            this.menuDelLessons.Text = Vocabruary.Translate("SheduleBrowser.DelLessons");
            this.menuItemDelLoading.Text = Vocabruary.Translate("SheduleBrowser.DelLoading");
        }

        private int _oldRow = -1; // Номер строки которая была выделена ранее
        private object[] _restrictsObjects = null;
        private void leadingGrid_CurrentCellChanged(object sender, EventArgs e)
        {
            Cursor.Current = Cursors.WaitCursor;

            CurrencyManager leadingCurrencyManager = (CurrencyManager)leadingGrid.BindingContext[leadingList];
            int _rowPosition = leadingCurrencyManager.Position;
            // Проверка изменился ли номер строки
            if (_oldRow != _rowPosition)
            {
                StudyLoading sl = leadingCurrencyManager.Current as StudyLoading;
                _oldRow = _rowPosition;

                _restrictsObjects = new object[sl.Groups.Count + sl.Tutors.Count];
                sl.Groups.CopyTo(_restrictsObjects, 0);
                sl.Tutors.CopyTo(_restrictsObjects, sl.Groups.Count);

                SheduleNav.ShowRestricts(_restrictsObjects, shmatr);

                // Отображаю группы которые заданы в нагрузке
                this.groupSheduleView1.ShowShedule(sl.Uid,
                (leadingList[_rowPosition] as StudyLoading).Groups, this.shmatr);

                //this.groupSheduleView1.ShowLessons();
                // Отображаю рекомендуемые места в расписании для расстоновки + рекоменд. аудитории(тоолтип), одновременно с 
                // ShowLessons с использованием правил


                this.subObjectList.DataSource = (leadingList[_rowPosition] as StudyLoading).Groups;
                this.listBox2.DataSource = (leadingList[_rowPosition] as StudyLoading).Tutors;

                /* Expert system Support*/
                ruleContext.ClearContext();
                if (sl.SubjectName != null)
                    ruleContext.AddRuleContext("Predmet", "%" + sl.SubjectName.Name + "%");
                else
                    ruleContext.AddRuleContext("Predmet", "%(null)%");
                if (sl.StudyForm != null)
                    ruleContext.AddRuleContext("StudyType", "%" + sl.StudyForm.MnemoCode + "%");
                else
                    ruleContext.AddRuleContext("StudyType", "%(null)%");

                if (sl.Dept != null)
                    ruleContext.AddRuleContext("Department", "%" + sl.Dept.MnemoCode + "%");
                else
                    ruleContext.AddRuleContext("Department", "%(null)%");

                ruleContext.AddRuleContext("Teacher", "%" + "(null)" + "%");
            }
            Cursor.Current = Cursors.Default;
        }

        private void leadingGrid_BindingContextChanged(object sender, EventArgs e)
        {
            DataGrid dg = (DataGrid)sender;
            ///Делает колонки DataGrid'а необходимой ширины
            Type dgt = dg.GetType();
            MethodInfo mi = dgt.GetMethod("ColAutoResize", BindingFlags.NonPublic | BindingFlags.Instance);
            for (int i = dg.FirstVisibleColumn; i < dg.VisibleColumnCount; i++)
            {
                object[] methodArgs = { i };
                mi.Invoke(sender, methodArgs);
            }
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            ObjectEditor oe = new ObjectEditor(objectFactory);
            object newobject = objectFactory.New(typeof(StudyLoading));

            oe.SelectedObject = newobject;
            if (oe.ShowDialog(this) == DialogResult.OK)
            {
                (newobject as StudyLoading).Uid = shmatr.AddStudyLeading(RemoteAdapter.Adapter/*(this.objectFactory)*/.AdaptStudyLeading(newobject as StudyLoading));

                if (lsfiltr.FilterObject(newobject))
                {
                    (leadingGrid.DataSource as ListWrapper).OnListChanged(new ListChangedEventArgs(ListChangedType.ItemAdded, leadingList.Add(newobject)));
                    leadingGrid_BindingContextChanged(leadingGrid, e);
                }
            }
        }

        private void linkLabel2_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            StudyLoading sl = (StudyLoading)leadingGrid.BindingContext[leadingList].Current;

            if (sl.UsedHourByWeek == 0)
            {
                ObjectEditor oe = new ObjectEditor(objectFactory);

                oe.SelectedObject = sl; //leadingGrid.BindingContext[leadingList].Current;


                if (oe.ShowDialog(this) == DialogResult.OK)
                {
                    StudyLoading editedStdLtd = leadingGrid.BindingContext[leadingList].Current as StudyLoading;

                    if (editedStdLtd != null)
                    {
                        shmatr.ChangeStudyLeading(editedStdLtd.Uid, RemoteAdapter.Adapter.AdaptStudyLeading(editedStdLtd));
                        /* Зміна розкладу */

                        shmatr.SetIsSaved(false);
                    }

                    int CurrentPos = leadingGrid.CurrentRowIndex;
                    (leadingGrid.DataSource as ListWrapper).OnListChanged(new ListChangedEventArgs(ListChangedType.ItemChanged, CurrentPos));
                }
            }
            else
                MessageBox.Show("Для редагування запису навантаження спочатку видалить заняття");
        }

        private void linkLabel3_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            linkLabel3.ContextMenu.Show(linkLabel3, new Point(0, linkLabel3.Height));
        }

        private void linkLabel4_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
        }

        private void refreshAll()
        {
            this.undoButton.Enabled = undoStack.Count != 0;
            this.redoButton.Enabled = redoStack.Count != 0;

            this.groupSheduleView1.RefreshShedule();
            this.groupSheduleView1.ShowLessons((leadingGrid.BindingContext[leadingList].Current as StudyLoading).Uid);
            SheduleNav.ShowRestricts(_restrictsObjects, shmatr);

            //(leadingGrid.DataSource as ListWrapper).OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, -1));
        }

        private void refreshLeadingGrid()
        {
            int rownumber = leadingGrid.CurrentCell.RowNumber;

            ListSortDirection lsd = (leadingGrid.DataSource as ListWrapper).SortDirection;
            PropertyDescriptor pd = (leadingGrid.DataSource as ListWrapper).SortProperty;

            this.RefreshStudyLeading();

            if (pd != null)
                (leadingGrid.DataSource as ListWrapper).ApplySort(pd, lsd);

            leadingGrid.CurrentCell = new DataGridCell(rownumber, 1);
        }


        private void groupSheduleView1_DragEnter(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(StudyLoading)))
            {
                StudyLoading stdLoading = e.Data.GetData(typeof(StudyLoading)) as StudyLoading;

                if (stdLoading.HourByWeek > stdLoading.UsedHourByWeek)
                    e.Effect = DragDropEffects.Copy;
                else
                    e.Effect = DragDropEffects.None;
            }
            else
                if (e.Data.GetDataPresent(typeof(int)))
                    e.Effect = DragDropEffects.Copy;
                else
                    e.Effect = DragDropEffects.None;
        }

        private void leadingGrid_MouseMove(object sender, MouseEventArgs e)
        {
            DataGrid.HitTestInfo _hti = leadingGrid.HitTest(new Point(e.X, e.Y));

            if (_hti.Row != -1 && _hti.Column != -1)
                if (e.Button == MouseButtons.Left)
                    leadingGrid.DoDragDrop(leadingGrid.BindingContext[leadingList].Current as StudyLoading, DragDropEffects.Copy | DragDropEffects.Move);
        }

        private void groupSheduleView1_DragDrop(object sender, DragEventArgs e)
        {
            #region StudyLoading
            if (e.Data.GetDataPresent(typeof(StudyLoading)))
            {
                StudyLoading stdLoad = e.Data.GetData(typeof(StudyLoading)) as StudyLoading;
                int _week, _day, _pair;
                if (groupSheduleView1.GetShedulePosition(e.X, e.Y, out _week, out _day, out _pair) == 1)
                {
                    int key = shmatr.GetKey(_week, _day, _pair);

                    // Форма для выбора аудитори(и)
                    FlatBrowser _flatBrowser = new FlatBrowser(this.weekCycles, this.weekDays, this.dayPairs, this.shmatr, this.buildingList);
                    _flatBrowser.SetRecomendation("Аудиторія: " + expSysFindFlat.Find(xPathDoc, ruleContext).Replace("%", ""));
                    _flatBrowser.SetRecomendation("Пара: " + expSysFindPair.Find(xPathDoc, ruleContext).Replace("%", ""));


                    StudyDay selDay = this.weekDays[_day - 1] as StudyDay;
                    StudyHour selHour = this.dayPairs[_pair - 1] as StudyHour;
                    StudyCycles selCycle = null;
                    if (stdLoad.HourByWeek - stdLoad.UsedHourByWeek < 2)
                        selCycle = this.weekCycles[_week - 1] as StudyCycles;

                    if (_flatBrowser.ShowDialog(selDay, selCycle, selHour, stdLoad) == DialogResult.OK)
                    {
                        if (redoStack.Count > 0)
                            redoStack.Clear();

                        ArrayList flatList = new ArrayList();
                        foreach (LectureHall lh in _flatBrowser.SelectedFlatList)
                            if (lh != null)
                                flatList.Add(lh);

                        foreach (StudyDay sd in _flatBrowser.SelectedDayList)
                            foreach (StudyHour sp in _flatBrowser.SelectedPairList)
                                foreach (StudyCycles sc in _flatBrowser.SelectedWeekList)
                                {
                                    // Додаю заняття до розкладу
                                    InsertLessonCmd insertCmd = new InsertLessonCmd(sd, sc, sp, stdLoad, flatList);
                                    insertCmd.BeforeInsert += new BeforeInsertLesson(RestrictChecker.instance.CheckGroup);
                                    insertCmd.BeforeInsert += new BeforeInsertLesson(RestrictChecker.instance.CheckTutor);
                                    insertCmd.BeforeInsert += new BeforeInsertLesson(RestrictChecker.instance.CheckPhizika);

                                    insertCmd.OnDo += new AfterDo(refreshAll);
                                    insertCmd.OnUndo += new AfterUndo(refreshAll);

                                    if (insertCmd.Do(shmatr))
                                    {
                                        if (redoStack.Count > 0)
                                        {
                                            redoStack.Clear();
                                            redoButton.Enabled = false;
                                        }
                                        undoStack.Push(insertCmd);
                                    }
                                }
                    }
                }
            }
            #endregion

            #region List<Lesson>
            if (e.Data.GetDataPresent(typeof(int)))
            {
                //List<Lesson> lessons = e.Data.GetData(typeof(List<Lesson>)) as List<Lesson>;
                //foreach (Lesson lsn in lessons)
                //{
                /**/
                //}
            }
            #endregion
        }

        private void SheduleView_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control)
            {
                if (e.KeyCode == Keys.Insert)
                    MessageBox.Show("Insert");
                if (e.KeyCode == Keys.Delete)
                    MessageBox.Show("Delete");
                e.Handled = true;
            }
        }

        private void menuItem2_Click(object sender, EventArgs e)
        {
            StudyLoading selectedObject = leadingGrid.BindingContext[leadingList].Current as StudyLoading;
            if (selectedObject != null)
            {
                //Поддержка принятия решений по выбору времени проведения занятия и месту его проведения 
                // лекция
                if (selectedObject.StudyForm.MnemoCode == "л.")
                {
                    //	DecisionSupportDlg dlg = new DecisionSupportDlg();
                    //IList Pairs = (new DB4OBridge()).ObjectList(typeof(StudyHour));
                    //for(int index = 0; index< 3; index++)
                    //{

                    //}
                }

                // лабораторная
                if (selectedObject.StudyForm.MnemoCode == "лб.")
                {

                }

                // практика
                if (selectedObject.StudyForm.MnemoCode == "п.")
                {

                }
            }
        }

        private void SheduleNav_MouseUp(object sender, MouseEventArgs e)
        {
            int _week, _day, _pair;
            if (e.Button == MouseButtons.Left)
            {
                if (SheduleNav.ShedulePositionByXY(e.X, e.Y, out _week, out _day, out _pair) == 1)
                    this.groupSheduleView1.SetFocus(_day, _week, _pair);
            }
        }

        private void listBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listBox2.Focused)
                SheduleNav.Show(listBox2.SelectedItem, this.shmatr);
        }

        private void listBox2_Enter(object sender, EventArgs e)
        {
            listBox2.BackColor = Color.LightGray;
        }

        private void listBox2_Leave(object sender, EventArgs e)
        {
            listBox2.BackColor = Color.WhiteSmoke;
        }

        private void leadingGrid_Enter(object sender, EventArgs e)
        {
            leadingGrid.BackColor = Color.LightGray;
        }

        private void leadingGrid_Leave(object sender, EventArgs e)
        {
            leadingGrid.BackColor = Color.WhiteSmoke;
        }

        private void menuItem8_Click(object sender, EventArgs e)
        {
            /* Переміщення заняття в інше місце*/
            IList lesshedpos = this.groupSheduleView1.getSheduleSelPositions();

            Lesson[] lessonList = new Lesson[lesshedpos.Count];
            for (int shpos_index = 0; shpos_index < lesshedpos.Count; shpos_index++)
            {
                ShedulePosition shpos = lesshedpos[shpos_index] as ShedulePosition;
                if (shpos.Columns.Count == 1)
                {
                    lessonList[shpos_index] = RemoteAdapter.Adapter.AdaptLesson(
                    this.shmatr.GetGroupLesson(
                    (shpos.Columns[0] as Group).ObjectHardKey,
                    shmatr.GetKey(shpos.WeekCycle, shpos.WeekDay, shpos.DayPair))
                    );

                    if (lessonList[shpos_index].Owner.UserUid > CurrentSession.instance.CurrentUser.UserUid)
                    {
                        MessageBox.Show("У Вас недостатньо прав для переміщення цього заняття, зверніться за допомогою до власника цього запису \n" + lessonList[0].Owner.Name + " або до адміністратора");
                        return;
                    }
                }
            }

            #region If Selected Both Weeks
            if (lesshedpos.Count == 2 && lesshedpos[0] != null && lesshedpos[1] != null
            && (lesshedpos[0] as ShedulePosition).Columns.Count == 1 && (lesshedpos[1] as ShedulePosition).Columns.Count == 1)
            {
                if ((lesshedpos[0] as ShedulePosition).WeekDay == (lesshedpos[1] as ShedulePosition).WeekDay &&
                (lesshedpos[0] as ShedulePosition).DayPair == (lesshedpos[1] as ShedulePosition).DayPair)
                {
                    if (lessonList[0].LeadingUid != lessonList[1].LeadingUid)
                        return;

                    // Запам'ятовую запис 
                    int leadingID = this.shmatr.GetGroupLesson(
                    ((lesshedpos[0] as ShedulePosition).Columns[0] as Group).ObjectHardKey,
                    shmatr.GetKey(
                    (lesshedpos[0] as ShedulePosition).WeekCycle,
                    (lesshedpos[0] as ShedulePosition).WeekDay,
                    (lesshedpos[0] as ShedulePosition).DayPair
                    )
                    ).leading_uid;

                    StudyLoading sl = RemoteAdapter.Adapter.AdaptStudyLeading(this.shmatr.GetStudyLeading(leadingID));
                    sl.UsedHourByWeek -= 2;

                    // Форма для выбора аудитори(и)
                    FlatBrowser _flatBrowser = new FlatBrowser(this.weekCycles, this.weekDays, this.dayPairs, this.shmatr, this.buildingList);
                    _flatBrowser.SetRecomendation("Аудиторія: " + expSysFindFlat.Find(xPathDoc, ruleContext).Replace("%", ""));
                    _flatBrowser.SetRecomendation("Пара: " + expSysFindPair.Find(xPathDoc, ruleContext).Replace("%", ""));

                    _flatBrowser.CanChangeTime = true;
                    _flatBrowser.CanChangeFlat = true;

                    foreach (object obj in lessonList[0].FlatList)
                        _flatBrowser.SelectedFlatList.Add(obj);

                    #region Select Flat and Insert Lesson
                    if (_flatBrowser.ShowDialog(null, null, null, sl) == DialogResult.OK)
                    {
                        RemoteLesson[] lesson_remList = new RemoteLesson[lessonList.Length];
                        for (int index = 0; index < lessonList.Length; index++)
                            lesson_remList[index] = RemoteAdapter.Adapter/*(this.objectFactory)*/.AdaptLesson(lessonList[index]);

                        /*New Flats for lesson*/
                        IList newFlats = new ArrayList();
                        foreach (LectureHall lh in _flatBrowser.SelectedFlatList)
                            newFlats.Add(lh.ObjectHardKey);

                        int lessonIndex = 0;
                        foreach (StudyDay sd in _flatBrowser.SelectedDayList)
                            foreach (StudyHour sp in _flatBrowser.SelectedPairList)
                                foreach (StudyCycles sc in _flatBrowser.SelectedWeekList)
                                    if (sd != null && sp != null && sc != null && lessonIndex < lesson_remList.Length)
                                    {
                                        int key = shmatr.GetKey(int.Parse(sc.MnemoCode), int.Parse(sd.MnemoCode), int.Parse(sp.MnemoCode));

                                        if (this.shmatr.FlatBusyCheckBool(key, newFlats))
                                            if (MessageBox.Show(this, Vocabruary.Translate("Message.CanUseFlatText"), Vocabruary.Translate("Message.WarningTitle"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                                                return;

                                        MoveLessonCmd moveLessonCmd = new MoveLessonCmd(lesson_remList[lessonIndex], sl, newFlats, sc, sp, sd);
                                        moveLessonCmd.BeforeMove += new BeforeInsertLesson(RestrictChecker.instance.CheckGroup);
                                        moveLessonCmd.BeforeMove += new BeforeInsertLesson(RestrictChecker.instance.CheckTutor);
                                        moveLessonCmd.BeforeMove += new BeforeInsertLesson(RestrictChecker.instance.CheckPhizika);

                                        moveLessonCmd.OnDo += new AfterDo(refreshAll);
                                        moveLessonCmd.OnUndo += new AfterUndo(refreshAll);

                                        if (moveLessonCmd.Do(shmatr))
                                        {
                                            if (redoStack.Count > 0)
                                            {
                                                redoStack.Clear();
                                                redoButton.Enabled = false;
                                            }

                                            undoStack.Push(moveLessonCmd);
                                        }

                                        lessonIndex++;
                                    }
                    }
                    #endregion
                }
            }
            #endregion

            #region If Selected One Week
            if (lesshedpos.Count == 1 && lesshedpos[0] != null && (lesshedpos[0] as ShedulePosition).Columns.Count == 1)
            {
                // Запам'ятовую запис 
                int leadingID =
                this.shmatr.GetGroupLesson(
                ((lesshedpos[0] as ShedulePosition).Columns[0] as Group).ObjectHardKey,
                shmatr.GetKey(
                (lesshedpos[0] as ShedulePosition).WeekCycle,
                (lesshedpos[0] as ShedulePosition).WeekDay,
                (lesshedpos[0] as ShedulePosition).DayPair
                )
                ).leading_uid;


                StudyLoading sl = RemoteAdapter.Adapter.AdaptStudyLeading(this.shmatr.GetStudyLeading(leadingID));
                sl.UsedHourByWeek--;

                // Форма для выбора времени
                FlatBrowser _flatBrowser = new FlatBrowser(this.weekCycles, this.weekDays, this.dayPairs, this.shmatr, this.buildingList);
                _flatBrowser.SetRecomendation("Аудиторія: " + expSysFindFlat.Find(xPathDoc, ruleContext).Replace("%", ""));
                _flatBrowser.SetRecomendation("Пара: " + expSysFindPair.Find(xPathDoc, ruleContext).Replace("%", ""));

                _flatBrowser.CanChangeTime = true;
                _flatBrowser.CanChangeFlat = true;

                foreach (object obj in lessonList[0].FlatList)
                    _flatBrowser.SelectedFlatList.Add(obj);

                if (_flatBrowser.ShowDialog(null, null, null, sl) == DialogResult.OK)
                {
                    if (redoStack.Count > 0)
                        redoStack.Clear();

                    RemoteLesson[] lesson_remList = new RemoteLesson[lessonList.Length];
                    for (int index = 0; index < lessonList.Length; index++)
                        lesson_remList[index] = RemoteAdapter.Adapter/*(this.objectFactory)*/.AdaptLesson(lessonList[index]);

                    /*New Flats for lesson*/
                    IList newFlats = new ArrayList();
                    foreach (LectureHall lh in _flatBrowser.SelectedFlatList)
                        newFlats.Add(lh.ObjectHardKey);

                    int lessonIndex = 0;
                    foreach (StudyDay sd in _flatBrowser.SelectedDayList)
                        foreach (StudyHour sp in _flatBrowser.SelectedPairList)
                            foreach (StudyCycles sc in _flatBrowser.SelectedWeekList)
                                if (sd != null && sp != null && sc != null && lessonIndex < lesson_remList.Length)
                                {
                                    int key = shmatr.GetKey(int.Parse(sc.MnemoCode), int.Parse(sd.MnemoCode), int.Parse(sp.MnemoCode));

                                    if (this.shmatr.FlatBusyCheckBool(key, newFlats))
                                        if (MessageBox.Show(this, Vocabruary.Translate("Message.CanUseFlatText"), Vocabruary.Translate("Message.WarningTitle"), MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.No)
                                            return;

                                    MoveLessonCmd moveLessonCmd = new MoveLessonCmd(lesson_remList[lessonIndex], sl, newFlats, sc, sp, sd);
                                    moveLessonCmd.BeforeMove += new BeforeInsertLesson(RestrictChecker.instance.CheckGroup);
                                    moveLessonCmd.BeforeMove += new BeforeInsertLesson(RestrictChecker.instance.CheckTutor);
                                    moveLessonCmd.BeforeMove += new BeforeInsertLesson(RestrictChecker.instance.CheckPhizika);

                                    moveLessonCmd.OnDo += new AfterDo(refreshAll);
                                    moveLessonCmd.OnUndo += new AfterUndo(refreshAll);

                                    if (moveLessonCmd.Do(shmatr))
                                    {
                                        if (redoStack.Count > 0)
                                        {
                                            redoStack.Clear();
                                            redoButton.Enabled = false;
                                        }

                                        undoStack.Push(moveLessonCmd);
                                    }

                                    lessonIndex++;
                                }
                }
            }
            #endregion

        }

        /* Зміна аудиторії нужно пересмотреть алгоритм!!!!*/
        private void menuItem7_Click(object sender, EventArgs e)
        {
            IList lesshedpos = this.groupSheduleView1.getSheduleSelPositions();

            #region If Selected Both Weeks
            if (lesshedpos.Count == 2 && lesshedpos[0] != null && lesshedpos[1] != null)
            {
                if ((lesshedpos[0] as ShedulePosition).WeekDay == (lesshedpos[1] as ShedulePosition).WeekDay &&
                (lesshedpos[0] as ShedulePosition).DayPair == (lesshedpos[1] as ShedulePosition).DayPair)
                {
                    Lesson[] lessonList = new Lesson[2];
                    lessonList[0] = RemoteAdapter.Adapter.AdaptLesson(
                    this.shmatr.GetGroupLesson(
                    ((lesshedpos[0] as ShedulePosition).Columns[0] as Group).ObjectHardKey,
                    shmatr.GetKey(
                    (lesshedpos[0] as ShedulePosition).WeekCycle,
                    (lesshedpos[0] as ShedulePosition).WeekDay,
                    (lesshedpos[0] as ShedulePosition).DayPair
                    )
                    )
                    );
                    lessonList[1] = RemoteAdapter.Adapter.AdaptLesson(
                    this.shmatr.GetGroupLesson(
                    ((lesshedpos[1] as ShedulePosition).Columns[0] as Group).ObjectHardKey,
                    shmatr.GetKey(
                    (lesshedpos[1] as ShedulePosition).WeekCycle,
                    (lesshedpos[1] as ShedulePosition).WeekDay,
                    (lesshedpos[1] as ShedulePosition).DayPair
                    )
                    )
                    );

                    if (lessonList[0].Owner != null)
                        if (lessonList[0].Owner.UserUid > CurrentSession.instance.CurrentUser.UserUid)
                        {
                            MessageBox.Show("У Вас недостатньо прав для зміни преміщення для цього заняття, зверніться за допомогою до власника цього запису \n -" + lessonList[0].Owner.Name + " або до адміністратора");
                            return;
                        }

                    if (lessonList[1].Owner != null)
                        if (lessonList[1].Owner.UserUid > CurrentSession.instance.CurrentUser.UserUid)
                        {
                            MessageBox.Show("У Вас недостатньо прав для переміщення цього заняття, зверніться за допомогою до власника цього запису \n" + lessonList[0].Owner.Name + " або до адміністратора");
                            return;
                        }

                    // Запам'ятовую запис 
                    int leadingID = this.shmatr.GetGroupLesson(
                    ((lesshedpos[0] as ShedulePosition).Columns[0] as Group).ObjectHardKey,
                    shmatr.GetKey(
                    (lesshedpos[0] as ShedulePosition).WeekCycle,
                    (lesshedpos[0] as ShedulePosition).WeekDay,
                    (lesshedpos[0] as ShedulePosition).DayPair
                    )
                    ).leading_uid;

                    StudyLoading sl = RemoteAdapter.Adapter.AdaptStudyLeading(this.shmatr.GetStudyLeading(leadingID));

                    // Форма для выбора аудитори(и)
                    FlatBrowser _flatBrowser = new FlatBrowser(this.weekCycles, this.weekDays, this.dayPairs, this.shmatr, this.buildingList);
                    _flatBrowser.SetRecomendation("Аудиторія: " + expSysFindFlat.Find(xPathDoc, ruleContext).Replace("%", ""));
                    _flatBrowser.SetRecomendation("Пара: " + expSysFindPair.Find(xPathDoc, ruleContext).Replace("%", ""));

                    _flatBrowser.CanChangeTime = false;

                    #region Select Takt of Study
                    StudyDay selDay = null;
                    foreach (StudyDay sd in this.weekDays)
                    {
                        if (sd.MnemoCode == (lesshedpos[0] as ShedulePosition).WeekDay.ToString())
                        {
                            selDay = sd;
                            break;
                        }
                    }

                    StudyHour selHour = null;
                    foreach (StudyHour sh in this.dayPairs)
                    {
                        if (sh.MnemoCode == (lesshedpos[0] as ShedulePosition).DayPair.ToString())
                        {
                            selHour = sh;
                            break;
                        }
                    }
                    #endregion

                    #region Select Flat and Insert Lesson
                    if (_flatBrowser.ShowDialog(selDay, null, selHour, sl) == DialogResult.OK)
                    {
                        ChangeFlatCmd changeCmd = new ChangeFlatCmd(new ArrayList(lessonList), _flatBrowser.SelectedFlatList);

                        changeCmd.OnDo += new AfterDo(refreshAll);
                        changeCmd.OnUndo += new AfterUndo(refreshAll);

                        if (changeCmd.Do(shmatr))
                        {
                            if (redoStack.Count > 0)
                            {
                                redoStack.Clear();
                                redoButton.Enabled = false;
                            }

                            undoStack.Push(changeCmd);
                        }
                    }
                    #endregion
                }
            }
            #endregion

            #region If Selected One Week
            if (lesshedpos.Count == 1 && lesshedpos[0] != null)
            {
                Lesson lesson = RemoteAdapter.Adapter.AdaptLesson(
                this.shmatr.GetGroupLesson(
                ((lesshedpos[0] as ShedulePosition).Columns[0] as Group).ObjectHardKey,
                shmatr.GetKey(
                (lesshedpos[0] as ShedulePosition).WeekCycle,
                (lesshedpos[0] as ShedulePosition).WeekDay,
                (lesshedpos[0] as ShedulePosition).DayPair
                )
                )
                );

                Lesson[] lessonList = new Lesson[1];
                lessonList[0] = lesson;

                if (lessonList[0].Owner != null)
                    if (lessonList[0].Owner.UserUid > CurrentSession.instance.CurrentUser.UserUid)
                    {
                        MessageBox.Show("У Вас недостатньо прав для зміни преміщення для цього заняття, зверніться за допомогою до власника цього запису \n -" + lessonList[0].Owner.Name + " або до адміністратора");
                        return;
                    }

                // Запам'ятовую запис 
                int leadingID =
                this.shmatr.GetGroupLesson(
                ((lesshedpos[0] as ShedulePosition).Columns[0] as Group).ObjectHardKey,
                shmatr.GetKey(
                (lesshedpos[0] as ShedulePosition).WeekCycle,
                (lesshedpos[0] as ShedulePosition).WeekDay,
                (lesshedpos[0] as ShedulePosition).DayPair
                )
                ).leading_uid;


                StudyLoading sl = RemoteAdapter.Adapter.AdaptStudyLeading(this.shmatr.GetStudyLeading(leadingID));

                // Форма для выбора аудитори(и)
                FlatBrowser _flatBrowser = new FlatBrowser(this.weekCycles, this.weekDays, this.dayPairs, this.shmatr, this.buildingList);
                _flatBrowser.SetRecomendation("Аудиторія: " + expSysFindFlat.Find(xPathDoc, ruleContext).Replace("%", ""));
                _flatBrowser.SetRecomendation("Пара: " + expSysFindPair.Find(xPathDoc, ruleContext).Replace("%", ""));

                _flatBrowser.CanChangeTime = false;

                #region Select Takt of Study
                StudyDay selDay = null;
                foreach (StudyDay sd in this.weekDays)
                {
                    if (sd.MnemoCode == (lesshedpos[0] as ShedulePosition).WeekDay.ToString())
                    {
                        selDay = sd;
                        break;
                    }
                }

                StudyCycles selCycle = null;
                foreach (StudyCycles sc in this.weekCycles)
                {
                    if (sc.MnemoCode == (lesshedpos[0] as ShedulePosition).WeekCycle.ToString())
                    {
                        selCycle = sc;
                        break;
                    }
                }

                StudyHour selHour = null;
                foreach (StudyHour sh in this.dayPairs)
                {
                    if (sh.MnemoCode == (lesshedpos[0] as ShedulePosition).DayPair.ToString())
                    {
                        selHour = sh;
                        break;
                    }
                }
                #endregion

                #region Select Flat and Insert Lesson
                if (_flatBrowser.ShowDialog(selDay, selCycle, selHour, sl) == DialogResult.OK)
                {
                    ChangeFlatCmd changeCmd = new ChangeFlatCmd(new ArrayList(lessonList), _flatBrowser.SelectedFlatList);

                    changeCmd.OnDo += new AfterDo(refreshAll);
                    changeCmd.OnUndo += new AfterUndo(refreshAll);

                    if (changeCmd.Do(shmatr))
                    {
                        if (redoStack.Count > 0)
                        {
                            redoStack.Clear();
                            redoButton.Enabled = false;
                        }

                        undoStack.Push(changeCmd);
                    }
                }
                #endregion
            }
            #endregion

        }

        private void linkLabel7_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            this.RefreshStudyLeading();
        }

        private void menuItemDelLoading_Click(object sender, System.EventArgs e)
        {
            if (MessageBox.Show(Vocabruary.Translate("DropDlg.Question"), Vocabruary.Translate("DropDlg.Title"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                shmatr.DelStudyLeading(RemoteAdapter.Adapter/*(this.objectFactory)*/.AdaptStudyLeading(leadingList[leadingGrid.BindingContext[leadingList].Position] as StudyLoading));
                leadingList.Remove(leadingList[leadingGrid.BindingContext[leadingList].Position]);

                (leadingGrid.DataSource as ListWrapper).OnListChanged(new ListChangedEventArgs(ListChangedType.ItemChanged, 0));

                if ((leadingList[leadingGrid.BindingContext[leadingList].Position] as StudyLoading) != null)
                    this.groupSheduleView1.ShowShedule((leadingList[leadingGrid.BindingContext[leadingList].Position] as StudyLoading).Uid,
                    (leadingList[leadingGrid.BindingContext[leadingList].Position] as StudyLoading).Groups, shmatr);
            }
        }

        private void menuDelLessons_Click(object sender, System.EventArgs e)
        {
            if (MessageBox.Show(Vocabruary.Translate("DropDlg.Question"), Vocabruary.Translate("DropDlg.Title"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                shmatr.DelLessonsByLeading((leadingGrid.BindingContext[leadingList].Current as StudyLoading).Uid, new CheckRight(CurrentSession.instance.CheckRightCallBack));

                this.groupSheduleView1.ShowShedule((leadingList[leadingGrid.BindingContext[leadingList].Position] as StudyLoading).Uid,
                (leadingList[leadingGrid.BindingContext[leadingList].Position] as StudyLoading).Groups, shmatr);
                this.RefreshStudyLeading();
            }
        }

        private void groupSheduleView1_SizeChanged(object sender, EventArgs e)
        {
            groupSheduleView1.ResumeLayout();
        }

        CommandList<ICommand> undoStack = new CommandList<ICommand>(25);
        CommandList<ICommand> redoStack = new CommandList<ICommand>(25);

        private void button1_Click(object sender, EventArgs e)
        {
            if (undoStack.Count != 0)
            {
                ICommand cmd = undoStack.Pop();
                redoStack.Push(cmd);
                redoStack.Head.UnDo(shmatr);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (redoStack.Count != 0)
            {
                ICommand cmd = redoStack.Pop();
                undoStack.Push(cmd);
                undoStack.Head.Do(shmatr);
            }
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            FlatBrowser _flatBrowser = new FlatBrowser(this.weekCycles, this.weekDays, this.dayPairs, this.shmatr, this.buildingList);

            StudyLoading stdLoad = leadingGrid.BindingContext[leadingList].Current as StudyLoading;
            _flatBrowser.Text = string.Format(Vocabruary.Translate("NewLesson.Title"), stdLoad.ToString());
            _flatBrowser.SetRecomendation("Аудиторія: " + expSysFindFlat.Find(xPathDoc, ruleContext).Replace("%", ""));
            _flatBrowser.SetRecomendation("Пара: " + expSysFindPair.Find(xPathDoc, ruleContext).Replace("%", ""));

            if (_flatBrowser.ShowDialog(null, null, null, stdLoad) == DialogResult.OK)
            {

                ArrayList flatList = new ArrayList();
                foreach (LectureHall lh in _flatBrowser.SelectedFlatList)
                    if (lh != null)
                        flatList.Add(lh);

                foreach (StudyDay sd in _flatBrowser.SelectedDayList)
                    foreach (StudyHour sp in _flatBrowser.SelectedPairList)
                        foreach (StudyCycles sc in _flatBrowser.SelectedWeekList)
                            if (sd != null && sp != null && sc != null)
                            {
                                // Додаю заняття до розкладу
                                InsertLessonCmd insertCmd = new InsertLessonCmd(sd, sc, sp, stdLoad, flatList);
                                insertCmd.BeforeInsert += new BeforeInsertLesson(RestrictChecker.instance.CheckGroup);
                                insertCmd.BeforeInsert += new BeforeInsertLesson(RestrictChecker.instance.CheckTutor);
                                insertCmd.BeforeInsert += new BeforeInsertLesson(RestrictChecker.instance.CheckPhizika);

                                insertCmd.OnDo += new AfterDo(refreshAll);
                                insertCmd.OnUndo += new AfterUndo(refreshAll);

                                if (insertCmd.Do(shmatr))
                                {
                                    if (redoStack.Count > 0)
                                    {
                                        redoStack.Clear();
                                        redoButton.Enabled = false;
                                    }

                                    undoStack.Push(insertCmd);
                                }
                            }
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            IList lesshedpos = this.groupSheduleView1.getSheduleSelPositions();

            string lessonListText = "";
            foreach (ShedulePosition shpos in lesshedpos)
                if (lessonListText != "")
                    lessonListText += "\n" + shpos.Text.Replace("\n", " ");
                else
                    lessonListText = shpos.Text.Replace("\n", " ");

            if (MessageBox.Show(this, "Ви дійсно бажаєте видалити заняття: \n" + lessonListText, "Видалення запису", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (redoStack.Count > 0)
                    redoStack.Clear();

                foreach (ShedulePosition shpos in lesshedpos)
                {
                    RemoveLessonCmd rmlcmd = new RemoveLessonCmd(shpos.WeekDay, shpos.WeekCycle, shpos.DayPair, shpos.Columns);

                    rmlcmd.OnDo += new AfterDo(refreshAll);
                    rmlcmd.OnDo += new AfterDo(refreshLeadingGrid);
                    rmlcmd.OnUndo += new AfterUndo(refreshAll);
                    rmlcmd.OnUndo += new AfterUndo(refreshLeadingGrid);

                    if (rmlcmd.Do(shmatr))
                    {
                        if (redoStack.Count > 0)
                        {
                            redoStack.Clear();
                            redoButton.Enabled = false;
                        }

                        undoStack.Push(rmlcmd);
                    }
                }
            }

        }

        private void scaleComboBox_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.scaleComboBox.SelectedIndex != -1)
                this.groupSheduleView1.Scale((this.scaleComboBox.SelectedIndex + 1) * 10);
        }

        private void groupSheduleView1_Click(object sender, EventArgs e)
        {
            /*int sel = this.groupSheduleView1.GetLeadingID();
            if (sel != -1)
            {
            int index = 0;
            foreach (StudyLoading sl in (leadingGrid.DataSource as ListWrapper).InternalList)
            {
            if (sl.Uid == sel)
            { 
            this.leadingGrid.Focus();
            this.leadingGrid.Select(index);
            (leadingGrid.DataSource as ListWrapper).OnListChanged(new ListChangedEventArgs(ListChangedType.Reset, index));
            return;
            }
            index++;
            }
            }*/
        }

        private void menuItem6_Click(object sender, EventArgs e)
        {
            MessageBox.Show(expSysFindFlat.Find(xPathDoc, ruleContext).Replace("%", ""));
        }

        private void menuItem5_Click(object sender, EventArgs e)
        {
            MessageBox.Show(expSysFindPair.Find(xPathDoc, ruleContext).Replace("%", ""));

        }
    }
}