using System.Collections;
using System.Collections.Generic;
using Shturman.Nestor.Interfaces;
using Shturman.Shedule.Objects;
using Shturman.Shedule.Objects.Container;
using Shturman.Shedule.Server.Remoting;

namespace Shturman.Shedule
{
/// <summary>
/// Класс предназначен для адаптации(преобразовании)
/// типов используемых сервером расписания к типам которые
/// используются для визуализации 
/// </summary>
public class RemoteAdapter
{
public static RemoteAdapter Adapter = new RemoteAdapter(DB4OBridgeRem.theOneObject);

private IObjectStorage _storage;
private Dictionary<ExtObjKey, StoredObject> dict = new Dictionary<ExtObjKey, StoredObject>();

/// <summary>
/// Конструктор
/// </summary>
/// <param name="storage"></param>
private RemoteAdapter(IObjectStorage storage)
{
_storage = storage;
}

/// <summary>
/// Функция возвращающая внутренний код объекта в хранилище для хранения 
/// ссылки на него во внешнем источнике
/// </summary>
/// <param name="customObject">объект из хранилища данных</param>
/// <returns>внешняя ссылка на объект</returns>
public ExtObjKey GetExtObjKey( StoredObject customObject)
{
ExtObjKey externalKey = new ExtObjKey(-1);
externalKey.UID = _storage.GetUUID(customObject, out externalKey.Signature, out externalKey.Index);
return externalKey;
}

/// <summary>
/// Функция для преобразования списка хранимых объектов в список объектов ключей ExtObjKey
/// </summary>
/// <param name="customObjectList">список объектов из хранилища данных</param>
/// <returns>список внешних ссылок на объекты типа ExtObjKey</returns>
public IList GetExtObjKey(IList customObjectList) 
{
ArrayList Keys = new ArrayList();
foreach (StoredObject so in customObjectList)
if (so != null)
{
ExtObjKey externalKey = this.GetExtObjKey(so);
if(externalKey != null)
Keys.Add(externalKey);
}
return Keys;
}

/// <summary>
/// По внешнему коду возвращает объект из хранилища данных
/// </summary>
/// <param name="key">Внешний ключ объекта</param>
/// <returns>Объект из хранилища</returns>
public object GetStoredObject(ExtObjKey key)
{
object storedObject = null;
if(dict.ContainsKey(key))
{
return dict[key];
}
else
if (key.Index != -1)
{
storedObject = _storage.GetObjectByUUID(key.Signature, key.Index);
if(storedObject==null)
storedObject = _storage.GetObjectByID(key.UID);
}
else
storedObject = _storage.GetObjectByID(key.UID);

if (storedObject != null)
dict.Add(key, (StoredObject)storedObject);

return storedObject;
}

/// <summary>
/// Функция для преобразования списка объектов ключей ExtObjKey в список хранимых объектов 
/// </summary>
/// <param name="keyList">Список объектов ключей(указателей на объекты)</param>
/// <returns>Список хранимых объектов</returns>
public IList GetStoredObject(IList keyList)
{
IList storedObjectList = new ArrayList();
foreach (ExtObjKey key in keyList)
{
object storedObject = this.GetStoredObject(key);
if (storedObject != null) 
storedObjectList.Add(storedObject);
}
return storedObjectList;
}

#region Method for RemoteLesson and RemoteLeading and so on
public string ExtractGroupViewString(RemoteLesson remoteLesson)
{
string str = "";

Subject Subject = this.GetStoredObject(remoteLesson.subject_key) as Subject;

if(Subject != null)
str = Subject.ShortName+"\n";

bool addcoma = false;
foreach (LectureHall lh in this.GetStoredObject(remoteLesson.flatList))
{
if (lh != null)
if (addcoma)
str += "," + lh.MnemoCode + lh.LectureHallBuilding.MnemoCode;
else
{
str += lh.MnemoCode + lh.LectureHallBuilding.MnemoCode;
addcoma = true;
}
}

StudyType StudyType = (StudyType)this.GetStoredObject( remoteLesson.studyType_key);

if (StudyType != null)
if(StudyType.MnemoCode == "л.")
foreach (Tutor ttr in this.GetStoredObject(remoteLesson.tutorList))
{
if (ttr != null)	
str += " " + ttr.ToString();
}
else
str += " " + StudyType.MnemoCode;

return str;
}

public RemoteLesson AdaptLesson(Lesson lesson)
{
RemoteLesson adaptedLesson = new RemoteLesson();

adaptedLesson.leading_uid = lesson.LeadingUid;

adaptedLesson.week_uid = lesson.CurLessonWeek.Uid;
adaptedLesson.week_index = int.Parse(lesson.CurLessonWeek.MnemoCode); 
adaptedLesson.week_key = this.GetExtObjKey( lesson.CurLessonWeek );

adaptedLesson.day_uid = lesson.CurLessonDay.Uid;
adaptedLesson.day_index = int.Parse(lesson.CurLessonDay.MnemoCode);
adaptedLesson.day_key = this.GetExtObjKey(lesson.CurLessonDay);

adaptedLesson.hour_uid = lesson.CurLessonPair.Uid;
adaptedLesson.hour_index = int.Parse(lesson.CurLessonPair.MnemoCode);
adaptedLesson.hour_key = this.GetExtObjKey(lesson.CurLessonPair);

//adaptedLesson.subject_key.UID = lesson.Subject.Uid;
adaptedLesson.subject_key = this.GetExtObjKey(lesson.Subject);

//adaptedLesson.studyType_key = lesson.SybjectType.Uid;
adaptedLesson.studyType_key = this.GetExtObjKey(lesson.SybjectType);

if (lesson.Owner != null)
{
adaptedLesson.user_uid = lesson.Owner.Uid;
adaptedLesson.user_key = this.GetExtObjKey(lesson.Owner);
}
else
{
adaptedLesson.user_uid = -1;
}

adaptedLesson.flatList = this.GetExtObjKey(lesson.FlatList);
adaptedLesson.tutorList = this.GetExtObjKey(lesson.TutorList);
adaptedLesson.groupList = this.GetExtObjKey(lesson.GroupList);

return adaptedLesson;
}

public Lesson AdaptLesson(RemoteLesson remoteLesson)
{
Subject Subject = null;
if (remoteLesson.subject_key.Index != -1)
{
Subject = _storage.GetObjectByUUID(remoteLesson.subject_key.Signature, remoteLesson.subject_key.Index) as Subject;
if (Subject == null)
Subject = _storage.GetObjectByID(remoteLesson.subject_key.UID) as Subject;
}
else
Subject = _storage.GetObjectByID(remoteLesson.subject_key.UID) as Subject;

StudyType StudyType = null;	
if (remoteLesson.studyType_key.Index != -1)
StudyType = _storage.GetObjectByUUID(remoteLesson.studyType_key.Signature, remoteLesson.studyType_key.Index) as StudyType;
else
StudyType = _storage.GetObjectByID(remoteLesson.studyType_key.UID) as StudyType;

Lesson adaptedLesson = new Lesson(Subject, StudyType, new ArrayList(), new ArrayList(), new ArrayList());

adaptedLesson.LeadingUid = remoteLesson.leading_uid;

if (remoteLesson.week_key.Index != -1)
adaptedLesson.CurLessonWeek = _storage.GetObjectByUUID(remoteLesson.week_key.Signature, remoteLesson.week_key.Index) as StudyCycles;
else 
adaptedLesson.CurLessonWeek = _storage.GetObjectByID(remoteLesson.week_uid) as StudyCycles;

if (remoteLesson.day_key.Index != -1)
adaptedLesson.CurLessonDay = _storage.GetObjectByUUID(remoteLesson.day_key.Signature, remoteLesson.day_key.Index) as StudyDay;
else
adaptedLesson.CurLessonDay = _storage.GetObjectByID(remoteLesson.day_uid) as StudyDay;

if (remoteLesson.hour_key.Index != -1)
adaptedLesson.CurLessonPair = _storage.GetObjectByUUID(remoteLesson.hour_key.Signature, remoteLesson.hour_key.Index) as StudyHour;
else
adaptedLesson.CurLessonPair = _storage.GetObjectByID(remoteLesson.hour_uid) as StudyHour;

if (remoteLesson.user_key.Index != -1)
adaptedLesson.Owner = _storage.GetObjectByUUID(remoteLesson.user_key.Signature, remoteLesson.user_key.Index) as ShturmanUser;
else
adaptedLesson.Owner = _storage.GetObjectByID(remoteLesson.user_uid) as ShturmanUser;

adaptedLesson.FlatList = this.GetStoredObject(remoteLesson.flatList);
adaptedLesson.TutorList = this.GetStoredObject(remoteLesson.tutorList);
adaptedLesson.GroupList = this.GetStoredObject(remoteLesson.groupList);

return adaptedLesson;
}

public RemoteStudyLeading AdaptStudyLeading(StudyLoading studyLeading)
{
RemoteStudyLeading remoteStudyLeading = new RemoteStudyLeading();

remoteStudyLeading.uid = studyLeading.Uid;
if (studyLeading.Dept != null)
remoteStudyLeading.department_key = studyLeading.Dept.ObjectHardKey;
else
remoteStudyLeading.department_key = new ExtObjKey(-1);

if (studyLeading.SubjectName != null)
remoteStudyLeading.subject_key = studyLeading.SubjectName.ObjectHardKey;
else
remoteStudyLeading.subject_key = new ExtObjKey(-1);

if (studyLeading.StudyForm != null)
remoteStudyLeading.subjectType_key = studyLeading.StudyForm.ObjectHardKey;
else
remoteStudyLeading.subjectType_key = new ExtObjKey(-1);

remoteStudyLeading.hourByWeek = studyLeading.HourByWeek;
remoteStudyLeading.note = studyLeading.Note;
remoteStudyLeading.usedHourByWeek = studyLeading.UsedHourByWeek;


remoteStudyLeading.listOfGroups = new ArrayList();
foreach (Group gr in studyLeading.Groups)
remoteStudyLeading.listOfGroups.Add(gr.ObjectHardKey);

remoteStudyLeading.listOfTutors = new ArrayList();
foreach (Tutor ttr in studyLeading.Tutors)
if(ttr != null)
remoteStudyLeading.listOfTutors.Add(ttr.ObjectHardKey);

return remoteStudyLeading;
}

public StudyLoading AdaptStudyLeading(RemoteStudyLeading remoteStudyLeading)
{
StudyLoading studyLeading = new StudyLoading();

studyLeading.Uid = remoteStudyLeading.uid;
if (remoteStudyLeading.department_key.Index != -1)
studyLeading.Dept = _storage.GetObjectByUUID(remoteStudyLeading.department_key.Signature, remoteStudyLeading.department_key.Index) as Department;
else
if(remoteStudyLeading.department_key.UID != -1)
studyLeading.Dept = _storage.GetObjectByID(remoteStudyLeading.department_key.UID) as Department;
else
studyLeading.Dept = null;

if (remoteStudyLeading.subject_key.Index != -1)
studyLeading.SubjectName = _storage.GetObjectByUUID(remoteStudyLeading.subject_key.Signature, remoteStudyLeading.subject_key.Index) as Subject;
else
if(remoteStudyLeading.subject_key.UID != -1)
studyLeading.SubjectName = _storage.GetObjectByID(remoteStudyLeading.subject_key.UID) as Subject;
else
studyLeading.SubjectName = null;

if (remoteStudyLeading.subjectType_key.Index != -1)
studyLeading.StudyForm = _storage.GetObjectByUUID(remoteStudyLeading.subjectType_key.Signature, remoteStudyLeading.subjectType_key.Index) as StudyType;
else
if(remoteStudyLeading.subjectType_key.UID != -1)
studyLeading.StudyForm = _storage.GetObjectByID(remoteStudyLeading.subjectType_key.UID) as StudyType;
else
studyLeading.StudyForm = null;

studyLeading.HourByWeek = remoteStudyLeading.hourByWeek;
studyLeading.Note = remoteStudyLeading.note;
studyLeading.UsedHourByWeek = remoteStudyLeading.usedHourByWeek;

foreach (ExtObjKey gr_uid in remoteStudyLeading.listOfGroups)
{
Group gr = null;
if(gr_uid.Index != -1 )
gr = _storage.GetObjectByUUID(gr_uid.Signature, gr_uid.Index) as Group;
else
if (gr_uid.UID != -1 )
gr = _storage.GetObjectByID(gr_uid.UID) as Group;

if (gr != null)
studyLeading.Groups.Add(gr);
}

foreach (ExtObjKey ttr_uid in remoteStudyLeading.listOfTutors)
{
Tutor ttr = null;
if(ttr_uid.Index != -1 )
ttr = _storage.GetObjectByUUID(ttr_uid.Signature, ttr_uid.Index) as Tutor;
else
if (ttr_uid.UID != -1 )
ttr = _storage.GetObjectByID(ttr_uid.UID) as Tutor;

studyLeading.Tutors.Add(ttr);
}

return studyLeading;
}

public IList AdaptLeadingList(IList remoteLeadingList)
{
IList adaptedList = new ArrayList();
foreach (object rsl in remoteLeadingList)
{
if (rsl is RemoteStudyLeading)
adaptedList.Add(this.AdaptStudyLeading(rsl as RemoteStudyLeading));
else
adaptedList.Add(this.AdaptStudyLeading(rsl as RemoteStudyLeading2));

}
return adaptedList;
}

public RemoteRestrict AdaptRestrict(Restrict restrict)
{
RemoteRestrict remoteRestrict = new RemoteRestrict();
remoteRestrict.week_key = restrict.RestrictWeek.ObjectHardKey;
remoteRestrict.WeekIndex = int.Parse(restrict.RestrictWeek.MnemoCode);
remoteRestrict.day_key = restrict.RestrictDay.ObjectHardKey;
remoteRestrict.DayIndex = int.Parse(restrict.RestrictDay.MnemoCode);
remoteRestrict.hour_key = restrict.RestrictPair.ObjectHardKey;
remoteRestrict.HourIndex = int.Parse(restrict.RestrictPair.MnemoCode);

remoteRestrict.Text = restrict.Comment;

return remoteRestrict;
}

public Restrict AdaptRestrict(RemoteRestrict remoteRestrict)
{
Restrict restrict = new Restrict(remoteRestrict.Text);
if (remoteRestrict.week_key.Index != -1)
restrict.RestrictWeek = this._storage.GetObjectByUUID(remoteRestrict.week_key.Signature, remoteRestrict.week_key.Index) as StudyCycles;
else
restrict.RestrictWeek = this._storage.GetObjectByID(remoteRestrict.week_key.UID) as StudyCycles;

if (remoteRestrict.day_key.Index != -1)
restrict.RestrictDay = this._storage.GetObjectByUUID(remoteRestrict.day_key.Signature, remoteRestrict.day_key.Index) as StudyDay;
else
restrict.RestrictDay = this._storage.GetObjectByID(remoteRestrict.day_key.UID) as StudyDay;

if (remoteRestrict.hour_key.Index != -1)
restrict.RestrictPair = this._storage.GetObjectByUUID(remoteRestrict.hour_key.Signature, remoteRestrict.hour_key.Index) as StudyHour;
else
restrict.RestrictPair = this._storage.GetObjectByID(remoteRestrict.hour_key.UID) as StudyHour;

return restrict;
}

#endregion

#region Method for RemoteLesson2 and RemoteLeading2 and so on
public string ExtractGroupViewString(RemoteLesson2 remoteLesson)
{
string str = "";
Subject Subject = null;

if (remoteLesson.subject_key.Index != -1)
Subject = _storage.GetObjectByUUID(remoteLesson.subject_key.Signature, remoteLesson.subject_key.Index) as Subject;
else
Subject = _storage.GetObjectByID(remoteLesson.subject_key.UID) as Subject;

if(Subject != null)
str = Subject.ShortName+"\n";

bool addcoma = false;
foreach (ExtObjKey lh_key in remoteLesson.flatList)
{
LectureHall lh = null;

if (lh_key.Index != -1) 
lh = _storage.GetObjectByUUID(lh_key.Signature, lh_key.Index) as LectureHall;
else
lh = _storage.GetObjectByID(lh_key.UID) as LectureHall;

if (lh != null)
{
if (addcoma)
{
str += ","+lh.MnemoCode+(lh.LectureHallBuilding as Building).MnemoCode;
}
else
{
str += lh.MnemoCode+(lh.LectureHallBuilding as Building).MnemoCode;
addcoma = true;
}
}
}

StudyType StudyType = null;

if (remoteLesson.studyType_key.Index != -1)
StudyType = _storage.GetObjectByUUID(remoteLesson.studyType_key.Signature, remoteLesson.studyType_key.Index) as StudyType;
else
StudyType = _storage.GetObjectByID(remoteLesson.studyType_key.UID) as StudyType;


if (StudyType != null)
if(StudyType.MnemoCode == "л.")
foreach (ExtObjKey ttr_key in remoteLesson.tutorList)
{
Tutor ttr = null;
if (ttr_key.Index != -1)
ttr = _storage.GetObjectByUUID(ttr_key.Signature, ttr_key.Index) as Tutor;
else
ttr = _storage.GetObjectByID(ttr_key.UID) as Tutor;

if (ttr != null)	
str += " " + ttr.ToString();
}
else
str += " " + StudyType.MnemoCode;

return str;
}

public RemoteLesson2 AdaptLesson(Lesson2 lesson)
{
RemoteLesson2 adaptedLesson = new RemoteLesson2();

adaptedLesson.leading_uid = lesson.LeadingUid;
adaptedLesson.date = lesson.CurLessonDay;
adaptedLesson.pair = int.Parse(lesson.CurLessonPair.MnemoCode);


adaptedLesson.subject_key = new ExtObjKey(-1);
adaptedLesson.subject_key.UID = _storage.GetUUID(lesson.Subject, out adaptedLesson.subject_key.Signature, out adaptedLesson.subject_key.Index);

adaptedLesson.studyType_key = new ExtObjKey(-1);
adaptedLesson.studyType_key.UID = _storage.GetUUID(lesson.SybjectType, out adaptedLesson.studyType_key.Signature, out adaptedLesson.studyType_key.Index);

adaptedLesson.flatList = new ArrayList();
foreach (LectureHall lh in lesson.FlatList)
{
ExtObjKey lh_key = new ExtObjKey(-1);
lh_key.UID = _storage.GetUUID(lh, out lh_key.Signature, out lh_key.Index);
adaptedLesson.flatList.Add(lh_key);
}

adaptedLesson.tutorList = new ArrayList();
foreach (Tutor ttr in lesson.TutorList)
{
ExtObjKey ttr_key = new ExtObjKey(-1);
ttr_key.UID = _storage.GetUUID(ttr, out ttr_key.Signature, out ttr_key.Index);
adaptedLesson.tutorList.Add(ttr_key);
}


adaptedLesson.groupList = new ArrayList();
foreach (Group gr in lesson.GroupList)
{
ExtObjKey gr_key = new ExtObjKey(-1);
gr_key.UID = _storage.GetUUID(gr, out gr_key.Signature, out gr_key.Index);
adaptedLesson.groupList.Add(gr_key);
}

return adaptedLesson;
}

public Lesson2 AdaptLesson(RemoteLesson2 remoteLesson)
{
Subject Subject = null;
if (remoteLesson.subject_key.Index != -1)
{
Subject = _storage.GetObjectByUUID(remoteLesson.subject_key.Signature, remoteLesson.subject_key.Index) as Subject;
if (Subject == null)
Subject = _storage.GetObjectByID(remoteLesson.subject_key.UID) as Subject;
}
else
Subject = _storage.GetObjectByID(remoteLesson.subject_key.UID) as Subject;

StudyType StudyType = null;	
if (remoteLesson.studyType_key.Index != -1)
StudyType = _storage.GetObjectByUUID(remoteLesson.studyType_key.Signature, remoteLesson.studyType_key.Index) as StudyType;
else
StudyType = _storage.GetObjectByID(remoteLesson.studyType_key.UID) as StudyType;

Lesson2 adaptedLesson = new Lesson2(Subject, StudyType, new ArrayList(), new ArrayList(), new ArrayList());

adaptedLesson.LeadingUid = remoteLesson.leading_uid;
//adaptedLesson.CurLessonWeek = _storage.GetObjectByID(remoteLesson.) as StudyCycles;
adaptedLesson.CurLessonDay = remoteLesson.date;	
adaptedLesson.CurLessonPair = _storage.ObjectList(typeof(StudyHour))[remoteLesson.pair-1] as StudyHour;

foreach (ExtObjKey lh_key in remoteLesson.flatList)
{
object obj = null;
if (lh_key.Index != -1)
obj = _storage.GetObjectByUUID(lh_key.Signature, lh_key.Index);
else
obj = _storage.GetObjectByID(lh_key.UID);

if (obj != null)
adaptedLesson.FlatList.Add(obj);
}

foreach (ExtObjKey ttr_key in remoteLesson.tutorList)
{
object obj = null;
if(ttr_key.Index != -1)
obj = _storage.GetObjectByUUID(ttr_key.Signature, ttr_key.Index);
else
obj = _storage.GetObjectByID(ttr_key.UID);

if (obj != null)
adaptedLesson.TutorList.Add(obj);
}

foreach (ExtObjKey gr_key in remoteLesson.groupList)
{
object obj = null;
if(gr_key.Index != -1)
{
obj = _storage.GetObjectByUUID(gr_key.Signature, gr_key.Index);
if (obj == null)
obj = _storage.GetObjectByID(gr_key.UID);
}
else
obj = _storage.GetObjectByID(gr_key.UID);

if (obj != null)
adaptedLesson.GroupList.Add(obj);
}

return adaptedLesson;
}


public RemoteStudyLeading2 AdaptStudyLeading2(StudyLoading studyLeading)
{
RemoteStudyLeading2 remoteStudyLeading = new RemoteStudyLeading2();

remoteStudyLeading.uid = studyLeading.Uid;
if (studyLeading.Dept != null)
{
remoteStudyLeading.department_key = new ExtObjKey();
remoteStudyLeading.department_key.UID = _storage.GetUUID(studyLeading.Dept, out remoteStudyLeading.department_key.Signature, out remoteStudyLeading.department_key.Index);
}
else
remoteStudyLeading.department_key = new ExtObjKey();

if (studyLeading.SubjectName != null)
{
remoteStudyLeading.subject_key = new ExtObjKey();
remoteStudyLeading.subject_key.UID = _storage.GetUUID(studyLeading.SubjectName, out remoteStudyLeading.subject_key.Signature, out remoteStudyLeading.subject_key.Index);	
}
else
remoteStudyLeading.subject_key = new ExtObjKey();

if (studyLeading.StudyForm != null)
{
remoteStudyLeading.subjectType_key = new ExtObjKey();
remoteStudyLeading.subjectType_key.UID = _storage.GetUUID(studyLeading.StudyForm, out remoteStudyLeading.subjectType_key.Signature, out remoteStudyLeading.subjectType_key.Index);	
}
else
remoteStudyLeading.subjectType_key = new ExtObjKey();

remoteStudyLeading.hourByWeek = studyLeading.HourByWeek;
remoteStudyLeading.note = studyLeading.Note;
remoteStudyLeading.usedHourByWeek = studyLeading.UsedHourByWeek;


remoteStudyLeading.listOfGroups = new ArrayList();
foreach (Group gr in studyLeading.Groups)
if(gr != null)
{
ExtObjKey gr_key = new ExtObjKey();
gr_key.UID = _storage.GetUUID(gr, out gr_key.Signature, out gr_key.Index);

remoteStudyLeading.listOfGroups.Add(gr_key);
}

remoteStudyLeading.listOfTutors = new ArrayList();
foreach (Tutor ttr in studyLeading.Tutors)
if(ttr != null)
{
ExtObjKey ttr_key = new ExtObjKey();
ttr_key.UID = _storage.GetUUID(ttr, out ttr_key.Signature, out ttr_key.Index);

remoteStudyLeading.listOfTutors.Add(ttr_key);
}

return remoteStudyLeading;
}


public StudyLoading AdaptStudyLeading(RemoteStudyLeading2 remoteStudyLeading)
{
StudyLoading studyLeading = new StudyLoading();

studyLeading.Uid = remoteStudyLeading.uid;

if (remoteStudyLeading.department_key.UID != -1 || remoteStudyLeading.department_key.Index != -1)
{
if(remoteStudyLeading.department_key.Index != -1)
{
studyLeading.Dept = _storage.GetObjectByUUID(remoteStudyLeading.department_key.Signature,
remoteStudyLeading.department_key.Index) as Department;
if (studyLeading.Dept == null)
studyLeading.Dept = _storage.GetObjectByID(remoteStudyLeading.department_key.UID) as Department;
}
else
studyLeading.Dept = _storage.GetObjectByID(remoteStudyLeading.department_key.UID) as Department;
}
else
studyLeading.Dept = null;

if (remoteStudyLeading.subject_key.UID != -1 || remoteStudyLeading.subject_key.Index != -1)
{
if (remoteStudyLeading.subject_key.Index != -1)
{
studyLeading.SubjectName = _storage.GetObjectByUUID(remoteStudyLeading.subject_key.Signature,
remoteStudyLeading.subject_key.Index) as Subject;
if (studyLeading.SubjectName == null)
studyLeading.SubjectName = _storage.GetObjectByID(remoteStudyLeading.subject_key.UID) as Subject;	
}
else
studyLeading.SubjectName = _storage.GetObjectByID(remoteStudyLeading.subject_key.UID) as Subject;
}
else
studyLeading.SubjectName = null;

if (remoteStudyLeading.subjectType_key.UID != -1||remoteStudyLeading.subjectType_key.Index != -1)
{
if (remoteStudyLeading.subjectType_key.Index != -1)
{
studyLeading.StudyForm = _storage.GetObjectByUUID(remoteStudyLeading.subjectType_key.Signature,
remoteStudyLeading.subjectType_key.Index) as StudyType;
if(studyLeading.StudyForm == null)
studyLeading.StudyForm = _storage.GetObjectByID(remoteStudyLeading.subjectType_key.UID) as StudyType;
}
else
studyLeading.StudyForm = _storage.GetObjectByID(remoteStudyLeading.subjectType_key.UID) as StudyType;
}
else
studyLeading.StudyForm = null;

studyLeading.HourByWeek = remoteStudyLeading.hourByWeek;
studyLeading.Note = remoteStudyLeading.note;
studyLeading.UsedHourByWeek = remoteStudyLeading.usedHourByWeek;

foreach (ExtObjKey gr_key in remoteStudyLeading.listOfGroups)
{
if(gr_key.Index != -1)
{
object obj = _storage.GetObjectByUUID(gr_key.Signature, gr_key.Index);
if (obj != null)
studyLeading.Groups.Add(obj);
else
{
obj = _storage.GetObjectByID(gr_key.UID);
if (obj != null)
studyLeading.Groups.Add(obj);
}
}
else
{	
object obj = _storage.GetObjectByID(gr_key.UID);
if (obj != null)
studyLeading.Groups.Add(obj);
}
}

foreach (ExtObjKey ttr_key in remoteStudyLeading.listOfTutors)
{
if (ttr_key.Index != -1)
{
object obj = _storage.GetObjectByUUID(ttr_key.Signature, ttr_key.Index);
if (obj != null)
studyLeading.Tutors.Add(obj);
else
{
obj = _storage.GetObjectByID(ttr_key.UID);
if (obj != null)
studyLeading.Tutors.Add(obj);
}
}
else
{
object obj = _storage.GetObjectByID(ttr_key.UID);
if (obj != null)
studyLeading.Tutors.Add(obj);
}
}

return studyLeading;
}


public RemoteRestrict2 AdaptRestrict(Restrict2 restrict)
{
RemoteRestrict2 remoteRestrict = new RemoteRestrict2();
remoteRestrict.date = restrict.RestrictDay;	
remoteRestrict.pair = int.Parse(restrict.RestrictPair.MnemoCode);	

remoteRestrict.Text = restrict.Comment;

return remoteRestrict;
}


public Restrict2 AdaptRestrict(RemoteRestrict2 remoteRestrict)
{
Restrict2 restrict = new Restrict2(remoteRestrict.Text);
restrict.RestrictDay = remoteRestrict.date;
restrict.RestrictPair = _storage.ObjectList(typeof(StudyHour))[remoteRestrict.pair-1] as StudyHour;;

return restrict;
}
#endregion
}
}