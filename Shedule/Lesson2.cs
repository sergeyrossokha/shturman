using System;
using System.Collections;
using Shturman.Nestor.DataAttributes;
using Shturman.Shedule.Objects;

namespace Shturman.Shedule
{
	/// <summary>
	/// ��������� ���� ������� � ���������� �������
	/// </summary>
	public class Lesson2
	{	
		#region Private Fields
		private DateTime day;
		private StudyHour hour;

		private Subject subject;
		private StudyType studyType;

		private int leadingUid;
		
		private IList groupList;
		private IList tutorList;
		private IList flatList;
		#endregion

		public Lesson2(Subject subject, StudyType subjectType, IList groupList, IList tutorList, IList flatList)
		{
			this.subject = subject;
			this.studyType = subjectType;

			this.groupList = groupList;
			this.tutorList = tutorList;
			this.flatList = flatList;
		}

		public int LeadingUid
		{
			get{ return leadingUid; }
			set{ leadingUid = value; }
		}

		/// <summary>
		/// ������� ������� ����������
		/// </summary>
		[PropertyVisible(true)]
		public Subject Subject
		{
			get{ return subject; }
			set{ subject = value; }
		}

		/// <summary>
		/// ��� ��������� ��������
		/// </summary>
		[PropertyVisible(true)]
		public StudyType SybjectType
		{
			get{ return studyType; }
			set{ studyType = value; }
		}

		/// <summary>
		/// ������ ��� ������� ���������� �������
		/// </summary>
		public IList GroupList
		{
			get{ return groupList; }
			set{ groupList = value; }
		}

		[PropertyVisible(true)]
		public string GroupListString
		{
			get {
				string groupString = "";
				foreach(Group gr in GroupList)
					if (gr != null)
						if(groupString != "")
							groupString += ", " +gr.MnemoCode;
						else
							groupString = gr.MnemoCode;
				return groupString;
			}
		}

		/// <summary>
		/// ������������� ������� �������� �������
		/// </summary>
		public IList TutorList
		{
			get{ return tutorList; }
			set{ tutorList = value; }
		}

		/// <summary>
		/// ��������� � ������ ���������� �������
		/// </summary>
		public IList FlatList
		{
			get{ return flatList; }
			set{ flatList = value; }
		}
		[PropertyVisible(true)]
		public string FlatListString
		{
			get 
			{
				string flatString = "";
				foreach(LectureHall lh in FlatList)
					if (lh != null)
						if(flatString != "")
							flatString += ", " +lh.MnemoCode+" "+lh.LectureHallBuilding.MnemoCode;
						else
							flatString = lh.MnemoCode+lh.LectureHallBuilding.MnemoCode;
				return flatString;
			}
		}

		[PropertyVisible(true)]
		public DateTime CurLessonDay
		{
			get{ return day; }
			set{ day = value; }
		}

		[PropertyVisible(true)]
		public StudyHour CurLessonPair
		{
			get{ return hour; }
			set{ hour = value; }
		}
		
		public string Text
		{
			get
			{
				string str = this.Subject.ShortName+"\n";
				bool addcoma = false;
				foreach(object obj in this.FlatList)
				{
					if (addcoma)
					{
						str += ","+(obj as LectureHall).MnemoCode+((obj as LectureHall).LectureHallBuilding as Building).MnemoCode;
					}
					else
					{
						str += (obj as LectureHall).MnemoCode+((obj as LectureHall).LectureHallBuilding as Building).MnemoCode;
						addcoma = true;
					}
				}
				return str;
			}
		}


		public string GroupViewString
		{
			get 
			{
				string str = "";
				if(this.Subject != null)
					str = this.Subject.ShortName+"\n";
				
				bool addcoma = false;
				foreach(object obj in this.FlatList)
				{
					if(obj != null)
						if (addcoma)
						{
							str += ","+(obj as LectureHall).MnemoCode+((obj as LectureHall).LectureHallBuilding as Building).MnemoCode;
						}
						else
						{
							str += (obj as LectureHall).MnemoCode+((obj as LectureHall).LectureHallBuilding as Building).MnemoCode;
							addcoma = true;
						}
				}

				if (this.SybjectType != null)
					if(this.SybjectType.MnemoCode == "�.")
						foreach(Tutor ttr in this.TutorList)
						{
							if(ttr != null)
								str += " " + ttr.ToString();
						}
					else
						str += " " + this.SybjectType.MnemoCode;

				return str;
			}
		}

		
		public string FlatViewString
		{
			get
			{
				string str = "";
				if (this.Subject != null)
					str += this.Subject.ShortName+"\n";
				bool addcoma = false;
				foreach(object obj in this.GroupList)
				{
					if(obj != null)
						if (addcoma)
						{
							str += ","+(obj as Group).MnemoCode;
						}
						else
						{
							str += (obj as Group).MnemoCode;
							addcoma = true;
						}
				}

				return str;
			}
		}

		
		public string TutorViewString
		{
			get
			{
				string str = ""; 
				bool addcoma = false;				
				foreach(object obj in this.FlatList)
				{
					if (addcoma)
					{
						str += ","+(obj as LectureHall).MnemoCode+((obj as LectureHall).LectureHallBuilding as Building).MnemoCode;
					}
					else
					{
						str += (obj as LectureHall).MnemoCode+((obj as LectureHall).LectureHallBuilding as Building).MnemoCode;
						addcoma = true;
					}
				}

				if (this.Subject != null)
					str += " " + this.Subject.ShortName + " \n";
				else
					str += " \n";

				addcoma = false;
				foreach(object obj in this.GroupList)
					if (obj != null)
					{
						if (addcoma)
						{
							str += ","+(obj as Group).MnemoCode;
						}
						else
						{
							str += (obj as Group).MnemoCode;
							addcoma = true;
						}
					}

				return str;
			}
		}
	}
}
