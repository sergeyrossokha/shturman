// Our namespaces
using System.Collections;
using Shturman.Shedule.Objects;

namespace Shturman.Shedule.Checkers
{
	/// <summary>
	/// Summary description for FlatCapacityChecker.
	/// </summary>
	public class FlatCapacityChecker
	{
		public enum FlatCapacityCheckerState
		{
			isNormal = 0,
			isLarge = 1,
			isSmall = 2
		};

		//private FlatCapacityChecker()
		//{
		//}

		private static int CheckBool(IList flats, IList groups)
		{
			if (flats.Count == 1)
			{
				LectureHall lectureHall = flats[0] as LectureHall;
				int capacityDelta = lectureHall.Capacity - groups.Count;

				if (capacityDelta == 0)
					return 0;
				if (capacityDelta > 0)
					return 1;
				if (capacityDelta < 0)
					return 2;
			}

			return 0;
		}

		public static FlatCapacityCheckerState Check(IList flats, IList groups)
		{
			return (FlatCapacityCheckerState) CheckBool(flats, groups);
		}
	}
}
