using System;
using System.Collections;
using Shturman.Shedule.Server.Interface;

namespace Shturman.Shedule
{
	/// <summary>
	/// Summary description for SheduleKeyBuilder.
	/// </summary>
	public class SheduleKeyBuilder
	{
//		static int CompileKey(int weekCycle, int weekDay, int weekPair, int weekCyclesCount, int dayPairsCount)
//		{
//			return  (weekDay-1)*weekCyclesCount*dayPairsCount+ (weekPair-1)*weekCyclesCount+ (weekCycle-1);
//		}
//
//		static void DecompileKey(int sheduleKey, out int weekCycle, out int weekDay, out int weekPair, int weekCyclesCount, int dayPairsCount, int pairPairsCount)
//		{
//			weekCycle = 0;
//			weekDay = 0;
//			weekPair = 0;
//		}

		public static int CompileKey(IShedule2 shedule, DateTime date, int pair)
		{	
			//int day = (date - this.sheduleBegin).Days;
			//return  (day-1)*dayPairsCount+ (pair-1);
		
			int day = (date - shedule.GetSheduleBeginDate()).Days + 1;
			return  (day - 1)*shedule.GetPairsPerDayCount() + pair - 1;
		}

		public static void DecompileKey(IShedule2 shedule, int sheduleKey, out DateTime date, out int pair)
		{			
			int day = sheduleKey / shedule.GetPairsPerDayCount();
			if (day != 0)
				date = shedule.GetSheduleBeginDate().AddDays( day );
			else
				date = shedule.GetSheduleBeginDate();

			pair = sheduleKey % shedule.GetPairsPerDayCount() + 1;
		}

		public static IList GetAllWeekDatesList(IShedule2 shedule)
		{
			DateTime dt = shedule.GetSheduleBeginDate();
			ArrayList weekDays = new ArrayList();

			int compensation = 0;				
			switch (dt.DayOfWeek)
			{
				case DayOfWeek.Sunday:
					compensation = 1;
					break;
				case DayOfWeek.Monday:
					compensation = 0;
					break;
				case DayOfWeek.Tuesday:
					compensation = 6;
					break;
				case DayOfWeek.Wednesday:
					compensation = 5;
					break;
				case DayOfWeek.Thursday:
					compensation = 4;
					break;
				case DayOfWeek.Friday:
					compensation = 3;
					break;
				case DayOfWeek.Saturday:
					compensation = 2;
					break;
			}

			DateTime DayOfYear = dt;
				
			if (compensation != 0)				
				DayOfYear = dt.AddDays( compensation ).AddDays(-7);

			DateTime endDate = shedule.GetSheduleEndDate();
			while (DayOfYear<endDate)
			{
				weekDays.Add(DayOfYear.ToShortDateString() +" - "+ DayOfYear.AddDays(6).ToShortDateString());
				DayOfYear = DayOfYear.AddDays(7);
			}
			
			return weekDays;
		}

		public static int WeekByDate(IShedule2 shedule, DateTime day)
		{
			DateTime dt = shedule.GetSheduleBeginDate();

			int compensation = 0;				
			switch (dt.DayOfWeek)
			{
				case DayOfWeek.Sunday:
					compensation = 1;
					break;
				case DayOfWeek.Monday:
					compensation = 0;
					break;
				case DayOfWeek.Tuesday:
					compensation = 6;
					break;
				case DayOfWeek.Wednesday:
					compensation = 5;
					break;
				case DayOfWeek.Thursday:
					compensation = 4;
					break;
				case DayOfWeek.Friday:
					compensation = 3;
					break;
				case DayOfWeek.Saturday:
					compensation = 2;
					break;
			}

			DateTime DayOfYear = dt;
			int weekNo = 0;	
			if (compensation != 0)			
				DayOfYear = dt.AddDays( compensation ).AddDays(-7);
			
			while (DayOfYear<=day)
			{
				weekNo ++;
				DayOfYear = DayOfYear.AddDays(7);
			}
			
			return weekNo;
		}

		public static IList WeekDatesList(IShedule2 shedule, int weekNumber)
		{
			DateTime dt = shedule.GetSheduleBeginDate();
			
			ArrayList weekDays = new ArrayList();
			
				int compensation = 0;				
				switch (dt.DayOfWeek)
				{
					case DayOfWeek.Sunday:
						compensation = 1;
						break;
					case DayOfWeek.Monday:
						compensation = 0;
						break;
					case DayOfWeek.Tuesday:
						compensation = 6;
						break;
					case DayOfWeek.Wednesday:
						compensation = 5;
						break;
					case DayOfWeek.Thursday:
						compensation = 4;
						break;
					case DayOfWeek.Friday:
						compensation = 3;
						break;
					case DayOfWeek.Saturday:
						compensation = 2;
						break;
				}
			
				DateTime DayOfYear = dt;
				
				
			if(compensation != 0)
				if (weekNumber == 1)
					DayOfYear = dt.AddDays( compensation ).AddDays(-7);
				else
					DayOfYear = dt.AddDays( compensation ).AddDays((weekNumber-2)*7);
			else
				if (weekNumber > 1)				
					DayOfYear = dt.AddDays( compensation ).AddDays((weekNumber-1)*7);


				for(int dayIndex=0; dayIndex<7; dayIndex++)
					weekDays.Add(DayOfYear.AddDays(dayIndex));
				
				return weekDays;
		}
	}
}
