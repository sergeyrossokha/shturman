using System;
using System.Collections;

namespace Shturman.Shedule
{
	/// <summary>
	/// Summary description for ShedulePosition.
	/// </summary>
	public class ShedulePosition2
	{

		public ShedulePosition2(DateTime weekDay, int dayPair)
		{
			WeekDay = weekDay;
			DayPair = dayPair;
		}

		public DateTime WeekDay;
		public int DayPair = -1;

		public IList Columns = new ArrayList();

		public override int GetHashCode()
		{	
			return WeekDay.DayOfYear^DayPair;
		}

		public override bool Equals(object obj)
		{
			if (obj is ShedulePosition)
				return (((obj as ShedulePosition2).WeekDay == this.WeekDay)&&((obj as ShedulePosition).DayPair == this.DayPair));
			else 
				return base.Equals(obj);
		}		
	}
}
