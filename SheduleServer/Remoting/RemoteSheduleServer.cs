using System;
using System.Collections;
using System.Configuration;

// My assemblies
using System.IO;
using Shturman.Shedule.Server.Interface;

namespace Shturman.Shedule.Server.Remoting
{
    /// <summary>
    /// Summary description for RemoteSheduleServer.
    /// </summary>
    public class RemoteSheduleServer : MarshalByRefObject, ISheduleServer
    {
        static public Hashtable remoteShedules = null;
        static public Hashtable remoteShedules2 = null;

        public RemoteSheduleServer()
        {
            if (remoteShedules == null)
            {
                remoteShedules = new Hashtable();

                DirectoryInfo di = new DirectoryInfo(ConfigurationSettings.AppSettings["SheduleDir"]);
                foreach (FileInfo fi in di.GetFiles("*.shedule"))
                {
                    RemoteShedule rshed = new RemoteShedule(2, 6, 6);
                    rshed.Open(fi.FullName);
                    remoteShedules.Add(fi.Name, rshed);
                }
            }

            if (remoteShedules2 == null)
            {
                remoteShedules2 = new Hashtable();
                DirectoryInfo di = new DirectoryInfo(ConfigurationSettings.AppSettings["SheduleDir"]);
                foreach (FileInfo fi in di.GetFiles("*.shedule2"))
                {
                    RemoteShedule2 rshed = new RemoteShedule2(7, 6);
                    rshed.Open(fi.FullName);
                    remoteShedules2.Add(fi.Name, rshed);
                }
            }
        }

        public IShedule GetSheduleByName(string sheduleName)
        {
            if (remoteShedules.Contains(sheduleName))
            {
                if (remoteShedules[sheduleName] == null)
                {
                    remoteShedules[sheduleName] = new RemoteShedule(2, 6, 6);
                    (remoteShedules[sheduleName] as RemoteShedule).Open(ConfigurationSettings.AppSettings["SheduleDir"] + sheduleName);
                    (remoteShedules[sheduleName] as RemoteShedule).SetFileName(sheduleName);

                    return remoteShedules[sheduleName] as RemoteShedule;
                }
                else
                    return remoteShedules[sheduleName] as IShedule;
            }
            return null;
        }

        public IShedule2 GetSheduleByName2(string sheduleName)
        {
            if (remoteShedules2.Contains(sheduleName))
            {
                if (remoteShedules2[sheduleName] == null)
                {
                    remoteShedules2[sheduleName] = new RemoteShedule2(7, 6);
                    (remoteShedules2[sheduleName] as RemoteShedule2).Open(ConfigurationSettings.AppSettings["SheduleDir"] + sheduleName);
                    (remoteShedules2[sheduleName] as RemoteShedule2).SetFileName(sheduleName);

                    return remoteShedules2[sheduleName] as RemoteShedule2;
                }
                else
                    return remoteShedules2[sheduleName] as IShedule2;
            }
            return null;
        }

        public IList GetAllShedules()
        {
            ArrayList list = new ArrayList(remoteShedules.Keys);
            list.AddRange(remoteShedules2.Keys);
            return list;
        }

        public IList GetAllShedulesInfo()
        {
            ArrayList list = new ArrayList();
            foreach (object key in remoteShedules.Keys)
                if (remoteShedules[key] is IShedule)
                {
                    object[] infoArray = (remoteShedules[key] as IShedule).GetSheduleInfo();
                    list.Add(infoArray);
                }
            foreach (object key in remoteShedules2.Keys)
                if (remoteShedules2[key] is IShedule2)
                {
                    object[] infoArray = (remoteShedules2[key] as IShedule2).GetSheduleInfo();
                    list.Add(infoArray);
                }
            return list;
        }


        public override object InitializeLifetimeService()
        {
            return null;
        }


        public bool Save(string sheduleName)
        {
            if (remoteShedules.Contains(sheduleName))
                if (remoteShedules[sheduleName] != null)
                {
                    (remoteShedules[sheduleName] as RemoteShedule).Save(ConfigurationSettings.AppSettings["SheduleDir"] + (remoteShedules[sheduleName] as RemoteShedule).GetFileName());
                    return true;
                }
            if (remoteShedules2.Contains(sheduleName))
                if (remoteShedules2[sheduleName] != null)
                {
                    (remoteShedules2[sheduleName] as RemoteShedule2).Save(ConfigurationSettings.AppSettings["SheduleDir"] + (remoteShedules2[sheduleName] as RemoteShedule2).GetFileName());
                    return true;
                }
            return false;
        }

        public bool SaveAs(string sheduleName, string newName)
        {
            if (remoteShedules.Contains(sheduleName))
                if (remoteShedules[sheduleName] != null)
                {
                    (remoteShedules[sheduleName] as RemoteShedule).Save(ConfigurationSettings.AppSettings["SheduleDir"] + newName);
                    return true;
                }
            if (remoteShedules2.Contains(sheduleName))
                if (remoteShedules2[sheduleName] != null)
                {
                    (remoteShedules2[sheduleName] as RemoteShedule2).Save(ConfigurationSettings.AppSettings["SheduleDir"] + newName);
                    return true;
                }
            return false;
        }


        public IShedule NewShedule(string sheduleName)
        {
            if (!remoteShedules.Contains(sheduleName))
            {
                remoteShedules[sheduleName] = new RemoteShedule(2, 6, 6);
                (remoteShedules[sheduleName] as RemoteShedule).SetFileName(sheduleName);

                return remoteShedules[sheduleName] as RemoteShedule;
            }
            return null;
        }

        public IShedule2 NewShedule2(string sheduleName)
        {
            if (!remoteShedules2.Contains(sheduleName))
            {
                remoteShedules2[sheduleName] = new RemoteShedule2(6, 6);
                (remoteShedules2[sheduleName] as RemoteShedule2).SetFileName(sheduleName);

                return remoteShedules2[sheduleName] as RemoteShedule2;
            }
            return null;
        }
    }
}