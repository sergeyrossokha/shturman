﻿using System;
using System.Collections;

namespace Shturman.Shedule.Server.Remoting
{
    /// <summary>
    /// Навантаження
    /// </summary>
    [Serializable]
    public class RemoteStudyLeading
    {
        /// <summary>
        /// Унікальний код навантаження
        /// </summary>
        public int uid;
        /// <summary>
        /// Код дисципліни
        /// </summary>
        public ExtObjKey subject_key;
        /// <summary>
        /// Код типу заняття
        /// </summary>
        public ExtObjKey subjectType_key;
        /// <summary>
        /// Код кафедри
        /// </summary>
        public ExtObjKey department_key;
        /// <summary>
        /// Список кодів груп
        /// </summary>
        public IList listOfGroups;
        /// <summary>
        /// Список кодів викладачів
        /// </summary>
        public IList listOfTutors;
        /// <summary>
        /// Кількість годин на тиждень
        /// </summary>
        public int hourByWeek;
        /// <summary>
        /// Кількість проставленних у розклад годин
        /// </summary>
        public int usedHourByWeek;
        /// <summary>
        /// Нотатки
        /// </summary>
        public string note;
    }
}