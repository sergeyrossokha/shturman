﻿using System;
using System.Collections;

namespace Shturman.Shedule.Server.Remoting
{
    /// <summary>
    /// Загальний класс "Заняття" є базовим для інших класів
    /// </summary>
    [Serializable]
    public class RemLessonBase
    {
        /// <summary>
        /// Код предмету за яким проводиться заняття
        /// </summary>
        public ExtObjKey subject_key;
        /// <summary>
        /// Код типу заняття
        /// </summary>
        public ExtObjKey studyType_key;

        /// <summary>
        /// Список кодів груп у яких проводиться заняття
        /// </summary>
        public IList groupList = new ArrayList();
        /// <summary>
        /// Список кодів викладачів які проводять заняття
        /// </summary>
        public IList tutorList = new ArrayList();
        /// <summary>
        /// Список кодів преміщень де має проводитися заняття
        /// </summary>
        public IList flatList = new ArrayList();
    }
}