﻿using System;
using System.Collections;

namespace Shturman.Shedule.Server.Remoting
{
    /// <summary>
    /// Summary description for UUIDConverter.
    /// </summary>
    public class UUIDConverter
    {
        /// <summary>
        /// Метод для отримання коду у вигляді радка символів
        /// </summary>
        /// <param name="signature">сигнатура об`єкта</param>
        /// <param name="index">індекс об`єкта</param>
        /// <returns>код обєкта у виглялді рядка символів</returns>
        public static string UUIDToString(byte[] signature, long index)
        {
            string str = string.Empty;
            // convert signature
            if (signature != null && index != -1)
            {
                foreach (byte bit in signature)
                    str += bit.ToString() + "-";
                // add index
                str += index.ToString();
            }

            return str;
        }

        /// <summary>
        /// Метод для отримання ключа з радка символів
        /// </summary>
        /// <param name="uuidString">рядок символів, що репрезентує ключ у вигляді рядка</param>
        /// <param name="signature">сигнатура об`єкта</param>
        /// <param name="index">індекс об`єкта</param>
        public static void StringToUUID(string uuidString, out byte[] signature, out long index)
        {
            if (uuidString == string.Empty)
            {
                signature = null;
                index = -1;
                return;
            }
            IList lst = new ArrayList();
            string numberStr = "";
            foreach (char ch in uuidString)
            {
                if (Char.IsNumber(ch))
                    numberStr += ch;
                else
                {
                    lst.Add(byte.Parse(numberStr));
                    numberStr = "";
                }
            }
            if (numberStr != null)
                lst.Add(long.Parse(numberStr));
            // Key
            index = (long)lst[lst.Count - 1];
            lst.RemoveAt(lst.Count - 1);
            // Signature
            signature = new byte[lst.Count];
            lst.CopyTo(signature, 0);
        }
    }
}