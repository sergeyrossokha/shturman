using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using Shturman.Shedule.Server.Remoting;

namespace Shturman.Shedule.Server.Remoting
{

    class SheduleAutoSave
    {
        public SheduleAutoSave(RemoteShedule remShedule)
        {
            _remShedule = remShedule;
        }

        Thread thread = null;
        
        RemoteShedule _remShedule;

        private void ThreadFunc(object obj)
        {
            DateTime lastSaveTime = DateTime.Now;
            RemoteShedule remShedule = obj as RemoteShedule;

            while (true)
            {
                lastSaveTime = DateTime.Now;
                Thread.Sleep(lastSaveTime.AddMinutes(9d) - lastSaveTime);
                TimeSpan goTime = DateTime.Now - lastSaveTime;
                
                if (goTime.Minutes >= 9)
                {
                    lock (remShedule)
                    {
                        if (!remShedule.GetIsSaved())
                            remShedule.Save(remShedule.GetFullFileName());
                    }

                    lastSaveTime = DateTime.Now;
                }
            }
        }

        public void RunAutoSaveMode()
        {
            this.thread = new Thread(new ParameterizedThreadStart( ThreadFunc ));
            this.thread.Priority = ThreadPriority.Lowest;
            this.thread.Start(_remShedule);
        }

        public void StopAutoSaveMode()
        {
            this.thread.Abort();
            this.thread = null;
        }
    }
}
