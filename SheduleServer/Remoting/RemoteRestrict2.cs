﻿using System;

namespace Shturman.Shedule.Server.Remoting
{
    /// <summary>
    /// Описывает класс ограничение
    /// </summary>
    [Serializable]
    public class RemoteRestrict2
    {
        public DateTime date;
        public int pair;

        public string Text;
    }
}