﻿using System;
using System.IO;
using System.Text;
using System.Windows.Forms;
using System.Xml;
using System.Collections;
using System.Collections.Generic;

using Shturman.Shedule.Server.Interface;

namespace Shturman.Shedule.Server.Remoting
{
    /// <summary>
    /// Матрица расписания занятий
    /// </summary>	
    [Serializable]
    public class RemoteShedule : MarshalByRefObject, IShedule, IDisposable
    {
        /// <summary>
        /// Навантаження для складання розкладу занять
        /// </summary>
        Dictionary<int, RemoteStudyLeading> studyLeading = new Dictionary<int, RemoteStudyLeading>();

        /// <summary>
        /// Розклад занять
        /// </summary>
        Dictionary<int, Dictionary<int, RemoteLesson>> shedule = new Dictionary<int, Dictionary<int, RemoteLesson>>();

        /// <summary>
        /// Розклад занять по аудиторіям
        /// </summary>
        private Dictionary<ExtObjKey, Dictionary<int, Dictionary<int, RemoteLesson>>> lessonsByFlat = new Dictionary<ExtObjKey, Dictionary<int, Dictionary<int, RemoteLesson>>>();

        /// <summary>
        /// Розклад занять по групам
        /// </summary>
        private Dictionary<ExtObjKey, Dictionary<int, RemoteLesson>> lessonsByGroup = new Dictionary<ExtObjKey, Dictionary<int, RemoteLesson>>();

        /// <summary>
        /// Розклад занять по викладачам
        /// </summary>
        private Dictionary<ExtObjKey, Dictionary<int, RemoteLesson>> lessonsByTutor = new Dictionary<ExtObjKey, Dictionary<int, RemoteLesson>>();

        /// <summary>
        /// Обмеження по групам
        /// </summary>
        private Dictionary<ExtObjKey, Dictionary<int, RemoteRestrict>> restrictByGroup = new Dictionary<ExtObjKey, Dictionary<int, RemoteRestrict>>();

        /// <summary>
        /// Обмеження по аудиторіям
        /// </summary>
        private Dictionary<ExtObjKey, Dictionary<int, RemoteRestrict>> restrictByFlat = new Dictionary<ExtObjKey, Dictionary<int, RemoteRestrict>>();

        /// <summary>
        /// обмеження по викладачам
        /// </summary>
        private Dictionary<ExtObjKey, Dictionary<int, RemoteRestrict>> restrictByTutor = new Dictionary<ExtObjKey, Dictionary<int, RemoteRestrict>>();



        // Число недельных циклов расписания
        private int weekCyclesCount = 0;
        // Число учебных дней в недели
        private int weekDaysCount = 0;
        // Число пар в день
        private int dayPairsCount = 0;

        // Следующий код для записей нагрузки
        private int LeadingUidCode = 1;

        // Заголовок расписания, Пример "Расписание 2004/2005 осень"
        private string sheduleTitle;

        // Версия расписания в данной версии этот аттрибут не используется
        private string version = "0.2";

        //Дата начала действия расписания
        private DateTime sheduleBegin;

        //Дата окончания действия расписания
        private DateTime sheduleEnd;

        private string fileName = "";
        private string directory = "";

        private bool isSaved = false;

        private SheduleAutoSave sasm;

        #region Конструктори
        public RemoteShedule(string title, int CyclesCount, int DaysCount, int PairsCount) :
            this(CyclesCount, DaysCount, PairsCount)
        {
            sheduleTitle = title;
        }

        public RemoteShedule(int CyclesCount, int DaysCount, int PairsCount)
        {
            weekCyclesCount = CyclesCount;
            weekDaysCount = DaysCount;
            dayPairsCount = PairsCount;

            sasm = new SheduleAutoSave(this);
            sasm.RunAutoSaveMode();
        }
        #endregion

        #region Key Generators

        public int GetKey(int weekCycle, int weekDay, int weekPair)
        {
            return (weekDay - 1) * weekCyclesCount * dayPairsCount + (weekPair - 1) * weekCyclesCount + (weekCycle - 1);
        }

        public int GetKey(DateTime date, int pair)
        {
            int day = (date - this.sheduleBegin).Days + 1;
            return (day - 1) * dayPairsCount + (pair - 1);
        }
        #endregion

        #region Фунцкії роботи з навантаженнями

        public int GetStudyLeadingUid()
        {
            return LeadingUidCode++;
        }

        public int AddStudyLeading(RemoteStudyLeading stLeading)
        {
            stLeading.uid = GetStudyLeadingUid();

            this.studyLeading.Add(stLeading.uid, stLeading);

            foreach (ExtObjKey key in stLeading.listOfGroups)
                if (key != null)
                    if (!_presentGroups.Contains(key))
                        _presentGroups.Add(key);

            this.isSaved = false;

            return stLeading.uid;
        }

        public void DelLessonsByLeading(int leadingUid)
        {
            /* Удаление связанных с удаляемой нагрузкой занятий из расписаний */
            IList Weeks = new ArrayList();
            IList Days = new ArrayList();
            IList Pairs = new ArrayList();

            // видалення з розкладу занять пов'язанних з данним навантаженням	
            IDictionaryEnumerator idea = shedule.GetEnumerator();
            idea.Reset();
            while (idea.MoveNext())
            {
                IDictionaryEnumerator ideb = (idea.Value as Hashtable).GetEnumerator();
                ideb.Reset();
                while (ideb.MoveNext())
                {
                    RemoteLesson lsn = ideb.Value as RemoteLesson;
                    if (lsn.leading_uid == leadingUid)
                    {
                        Weeks.Add(lsn.week_index);
                        Days.Add(lsn.day_index);
                        Pairs.Add(lsn.hour_index);
                    }
                }
            }
            if (studyLeading.ContainsKey(leadingUid))
            {
                IList groups = (studyLeading[leadingUid] as RemoteStudyLeading).listOfGroups;

                for (int index = 0; index < Weeks.Count; index++)
                    this.DeleteByGroup((int)Weeks[index], (int)Days[index], (int)Pairs[index], groups);
            }

        }

        public void DelLessonsByLeading(int leadingUid, CheckRight checkRight)
        {
            /* Удаление связанных с удаляемой нагрузкой занятий из расписаний */
            IList Weeks = new ArrayList();
            IList Days = new ArrayList();
            IList Pairs = new ArrayList();

            // видалення з розкладу занять пов'язанних з данним навантаженням	
            IDictionaryEnumerator idea = shedule.GetEnumerator();
            idea.Reset();
            while (idea.MoveNext())
            {
                IDictionaryEnumerator ideb = (idea.Value as Hashtable).GetEnumerator();
                ideb.Reset();
                while (ideb.MoveNext())
                {
                    RemoteLesson lsn = ideb.Value as RemoteLesson;

                    if (lsn.leading_uid == leadingUid)
                    {
                        if (checkRight != null)
                        {
                            if (!checkRight(lsn.user_key)) break;
                        }
                        Weeks.Add(lsn.week_index);
                        Days.Add(lsn.day_index);
                        Pairs.Add(lsn.hour_index);
                    }
                }
            }
            if (studyLeading.ContainsKey(leadingUid))
            {
                IList groups = (studyLeading[leadingUid] as RemoteStudyLeading).listOfGroups;

                for (int index = 0; index < Weeks.Count; index++)
                    this.DeleteByGroup((int)Weeks[index], (int)Days[index], (int)Pairs[index], groups);
            }
        }

        public void DelStudyLeading(RemoteStudyLeading stLeading)
        {
            this.DelLessonsByLeading(stLeading.uid);

            this.studyLeading.Remove(stLeading.uid);
            this.isSaved = false;
        }

        public RemoteStudyLeading GetStudyLeading(int leading_uid)
        {
            return this.studyLeading[leading_uid];
        }

        public void ChangeStudyLeading(int leading_uid, RemoteStudyLeading stLeading)
        {
            if (this.studyLeading.ContainsKey(leading_uid))
                this.studyLeading[leading_uid] = stLeading;
        }
        #endregion

        #region Функції додовання, видалення та редагування розкладу занять

        public bool CanInsertTo(int weekCycle, int weekDay, int weekPair, RemoteLesson lesson)
        {
            // Create position index in shedule matrix
            int customKey = GetKey(weekCycle, weekDay, weekPair);

            // Check ==> Group(s) is FREE 
            bool groupIsFree = (this.GroupBusyCheck(customKey, lesson.groupList) != GroupBusyCheckerState.isBusy);

            // Check ==> Tutor(s) is FREE 
            bool tutorIsFree = (this.TutorBusyCheck(customKey, lesson.tutorList) != TutorBusyCheckerState.isBusy);

            // Check ==> All is FREE
            return groupIsFree && tutorIsFree;
        }

        public int SimpleInsert(int weekCycle, int weekDay, int weekPair, RemoteLesson lesson)
        {
            // Create position index in shedule matrix
            int customKey = GetKey(weekCycle, weekDay, weekPair);

            // Check ==> Group(s) is FREE 
            bool groupIsFree = (this.GroupBusyCheck(customKey, lesson.groupList) != GroupBusyCheckerState.isBusy);

            // Check ==> Tutor(s) is FREE 
            bool tutorIsFree = (TutorBusyCheck(customKey, lesson.tutorList) != TutorBusyCheckerState.isBusy);

            // Check ==> All is FREE
            bool isFree = groupIsFree && tutorIsFree;

            if (!isFree)
            {
                studyLeading[lesson.leading_uid].usedHourByWeek--;
                return -1;
            }

            if (lesson.week_index == -1 || lesson.hour_index == -1 || lesson.day_index == -1)
            {
                // Из-за различия в форматах, т.к в пред версиях данные поля не использовались
                studyLeading[lesson.leading_uid].usedHourByWeek--;
                return -1;
            }

            // Add to existing lesson additional flatroom 
            if (!groupIsFree && shedule[customKey].ContainsKey(lesson.leading_uid))
            {
                // Insert refference to additional flatroom
                foreach (object obj in lesson.flatList)
                    shedule[customKey][lesson.leading_uid].flatList.Add(obj);

                // Insert in shedule by flats refference to lesson
                foreach (ExtObjKey flt_uid in lesson.flatList)
                {
                    // if refference list of flats does not exist do create it!
                    if (!lessonsByFlat.ContainsKey(flt_uid))
                        lessonsByFlat.Add(flt_uid, new Dictionary<int, Dictionary<int, RemoteLesson>>());

                    // Insert refference to refference list	
                    lessonsByFlat[flt_uid].Add(customKey, shedule[customKey]/*[lesson.leading_uid].*/);
                }
                // Set that shedule is changed and need to be saved 
                isSaved = false;
                // Exit
                return customKey;
            }

            // if shedule doesnt contain refference list do create tham
            if (!shedule.ContainsKey(customKey))
                shedule.Add(customKey, new Dictionary<int, RemoteLesson>());

            // Insert new lesson to a shedule	
            shedule[customKey].Add(lesson.leading_uid, lesson);

            try
            {
                // insert refference of lesson to refference list of group	
                foreach (ExtObjKey gr_uid in lesson.groupList)
                {
                    if (!lessonsByGroup.ContainsKey(gr_uid))
                        lessonsByGroup.Add(gr_uid, new Dictionary<int, RemoteLesson>());

                    lessonsByGroup[gr_uid].Add(customKey, lesson);
                }
            }
            catch
            {
                MessageBox.Show("1");
            }

            try
            {	// insert refference of lesson to refference list of flatroom
                foreach (ExtObjKey flt_uid in lesson.flatList)
                {
                    if (!lessonsByFlat.ContainsKey(flt_uid))
                        lessonsByFlat.Add(flt_uid, new Dictionary<int, Dictionary<int, RemoteLesson>>());

                    if (!lessonsByFlat[flt_uid].ContainsKey(customKey))
                        lessonsByFlat[flt_uid].Add(customKey, new Dictionary<int, RemoteLesson>());

                    lessonsByFlat[flt_uid][customKey].Add(lesson.leading_uid, lesson);
                }
            }
            catch
            {
                MessageBox.Show("1");
            }

            try
            {
                // insert refference of lesson to refference list of tutor
                foreach (ExtObjKey ttr_uid in lesson.tutorList)
                {
                    if (!lessonsByTutor.ContainsKey(ttr_uid))
                        lessonsByTutor.Add(ttr_uid, new Dictionary<int, RemoteLesson>());

                    lessonsByTutor[ttr_uid].Add(customKey, lesson);
                }
            }
            catch
            {
                MessageBox.Show("1");
            }

            // Set that shedule is changed and need to be saved 
            isSaved = false;

            return customKey;
        }

        public int Insert(int weekCycle, int weekDay, int weekPair, RemoteLesson lesson)
        {
            int key = SimpleInsert(weekCycle, weekDay, weekPair, lesson);

            // Change count used hour by lesson
            studyLeading[lesson.leading_uid].usedHourByWeek++;

            // Exit, by return lesson position index in shedule matrix
            return key;
        }

        public void DeleteByGroup(int weekCycle, int weekDay, int weekPair, IList groupList)
        {
            int lessonKey = GetKey(weekCycle, weekDay, weekPair);

            foreach (ExtObjKey gr_uid in groupList)
            {
                if (!lessonsByGroup[gr_uid].ContainsKey(lessonKey)) continue;
                RemoteLesson lesson = (lessonsByGroup[gr_uid][lessonKey]);
                if (shedule[lessonKey].ContainsKey(lesson.leading_uid))
                    shedule[lessonKey].Remove(lesson.leading_uid);

                foreach (ExtObjKey tr_uid in lesson.tutorList)
                    if (lessonsByTutor[tr_uid].ContainsKey(lessonKey))
                        lessonsByTutor[tr_uid].Remove(lessonKey);

                foreach (ExtObjKey lh_uid in lesson.flatList)
                    if (lessonsByFlat[lh_uid].ContainsKey(lessonKey))
                        if (lessonsByFlat[lh_uid][lessonKey].ContainsKey(lesson.leading_uid))
                            lessonsByFlat[lh_uid][lessonKey].Remove(lesson.leading_uid);

                foreach (ExtObjKey grp_uid in lesson.groupList)
                    if (lessonsByGroup[grp_uid].ContainsKey(lessonKey))
                        lessonsByGroup[grp_uid].Remove(lessonKey);


                RemoteStudyLeading remoteleading = studyLeading[lesson.leading_uid];
                if (remoteleading.usedHourByWeek != 0)
                    remoteleading.usedHourByWeek--;

                // Set that shedule is changed and need to be saved 
                isSaved = false;

            }
        }

        public void DeleteByFlat(int weekCycle, int weekDay, int weekPair, IList flatList)
        {
            // Create position index in shedule matrix
            int lessonKey = GetKey(weekCycle, weekDay, weekPair);

            foreach (ExtObjKey lh_uid in flatList)
            {
                if (!lessonsByFlat[lh_uid].ContainsKey(lessonKey)) continue;

                foreach (RemoteLesson lesson in new ArrayList(lessonsByFlat[lh_uid][lessonKey].Values))
                {

                    // Проверка на удаление дополнительной аудитории
                    if (lesson.flatList.Count > 1)
                    {
                        lesson.flatList.Remove(lh_uid);
                        // 
                        if (lessonsByFlat[lh_uid].ContainsKey(lessonKey))
                        {
                            lessonsByFlat[lh_uid].Remove(lessonKey);
                            lesson.flatList.Remove(lh_uid);
                            continue;
                        }
                    }

                    if (shedule[lessonKey].ContainsKey(lesson.leading_uid))
                        shedule[lessonKey].Remove(lesson.leading_uid);

                    foreach (ExtObjKey tr_uid in lesson.tutorList)
                        if (lessonsByTutor[tr_uid].ContainsKey(lessonKey))
                            lessonsByTutor[tr_uid].Remove(lessonKey);

                    foreach (ExtObjKey gr_uid in lesson.groupList)
                        if (lessonsByGroup[gr_uid].ContainsKey(lessonKey))
                            lessonsByGroup[gr_uid].Remove(lessonKey);

                    if (lessonsByFlat[lh_uid].ContainsKey(lessonKey))
                        lessonsByFlat[lh_uid].Remove(lessonKey);

                    studyLeading[lesson.leading_uid].usedHourByWeek--;
                }
            }
        }

        public void MoveLesson(RemoteLesson lesson, int nweekCycle, int nweekDay, int nweekPair)
        {
            // Перевырка на можливість перенести заняття у інше місце
            if (CanInsertTo(nweekCycle, nweekDay, nweekPair, lesson))
            {
                this.DeleteByGroup(lesson.week_index, lesson.day_index, lesson.hour_index, lesson.groupList);

                lesson.week_index = nweekCycle;
                lesson.day_index = nweekDay;
                lesson.hour_index = nweekPair;

                this.Insert(nweekCycle, nweekDay, nweekPair, lesson);
            }
        }

        public void ChangeLessonFlats(RemoteLesson[] lessonList, ExtObjKey[] flatList)
        {
            foreach (RemoteLesson lsn in lessonList)
            {
                // Create position index in shedule matrix
                int customKey = GetKey(lsn.week_index, lsn.day_index, lsn.hour_index);

                RemoteLesson existLesson = null;
                if (shedule[customKey].ContainsKey(lsn.leading_uid))
                    existLesson = shedule[customKey][lsn.leading_uid] as RemoteLesson;

                if (existLesson == null)
                    continue;

                // Вивілняю поки що зайняті аудиторії
                foreach (ExtObjKey lh_uid in existLesson.flatList)
                {
                    if (lh_uid.Index != -1)
                        if (lessonsByFlat[lh_uid].ContainsKey(customKey))
                            if (lessonsByFlat[lh_uid][customKey].ContainsKey(existLesson.leading_uid))
                            {
                                lessonsByFlat[lh_uid][customKey].Remove(existLesson.leading_uid);
                            }
                }
                existLesson.flatList.Clear();

                // Займаю нові аудиторії
                foreach (ExtObjKey lh_uid in flatList)
                {
                    if (!lessonsByFlat.ContainsKey(lh_uid))
                        lessonsByFlat.Add(lh_uid, new Dictionary<int, Dictionary<int, RemoteLesson>>());

                    if (!lessonsByFlat[lh_uid].ContainsKey(customKey))
                        lessonsByFlat[lh_uid].Add(customKey, new Dictionary<int, RemoteLesson>());

                    lessonsByFlat[lh_uid][customKey].Add(existLesson.leading_uid, existLesson);

                    existLesson.flatList.Add(lh_uid);
                }
                this.isSaved = false;
            }
        }
        #endregion

        #region Додавання та видалення обмежень викладачів, груп та аудитторій

        public int InsertGroupRestrict(int weekCycle, int weekDay, int weekPair, ExtObjKey group_uid, RemoteRestrict restrict)
        {
            this.SetIsSaved(false);
            // Create position index in shedule matrix
            int customKey = GetKey(weekCycle, weekDay, weekPair);

            if (!this.restrictByGroup.ContainsKey(group_uid))
                this.restrictByGroup.Add(group_uid, new Dictionary<int, RemoteRestrict>());

            if (this.restrictByGroup.ContainsKey(group_uid))
                if (!this.restrictByGroup[group_uid].ContainsKey(customKey))
                    this.restrictByGroup[group_uid].Add(customKey, restrict);

            return customKey;
        }


        public int InsertTutorRestrict(int weekCycle, int weekDay, int weekPair, ExtObjKey tutor_uid, RemoteRestrict restrict)
        {
            this.SetIsSaved(false);
            // Create position index in shedule matrix
            int customKey = GetKey(weekCycle, weekDay, weekPair);

            if (!this.restrictByTutor.ContainsKey(tutor_uid))
                this.restrictByTutor.Add(tutor_uid, new Dictionary<int, RemoteRestrict>());

            if (this.restrictByTutor.ContainsKey(tutor_uid))
                if (!this.restrictByTutor[tutor_uid].ContainsKey(customKey))
                    this.restrictByTutor[tutor_uid].Add(customKey, restrict);

            return customKey;
        }


        public int InsertFlatRestrict(int weekCycle, int weekDay, int weekPair, ExtObjKey flat_uid, RemoteRestrict restrict)
        {
            this.SetIsSaved(false);
            // Create position index in shedule matrix
            int customKey = GetKey(weekCycle, weekDay, weekPair);

            if (!this.restrictByFlat.ContainsKey(flat_uid))
                this.restrictByFlat.Add(flat_uid, new Dictionary<int, RemoteRestrict>());

            if (this.restrictByFlat.ContainsKey(flat_uid))
                if (!this.restrictByFlat[flat_uid].ContainsKey(customKey))
                    this.restrictByFlat[flat_uid].Add(customKey, restrict);

            return customKey;
        }


        public void DeleteGroupRestrict(int weekCycle, int weekDay, int weekPair, ExtObjKey group_uid)
        {
            this.SetIsSaved(false);
            // Create position index in shedule matrix
            int customKey = GetKey(weekCycle, weekDay, weekPair);

            if (this.restrictByGroup.ContainsKey(group_uid))
                if (this.restrictByGroup[group_uid].ContainsKey(customKey))
                    this.restrictByGroup[group_uid].Remove(customKey);
        }


        public void DeleteTutorRestrict(int weekCycle, int weekDay, int weekPair, ExtObjKey tutor_uid)
        {
            this.SetIsSaved(false);
            // Create position index in shedule matrix
            int customKey = GetKey(weekCycle, weekDay, weekPair);

            if (this.restrictByTutor.ContainsKey(tutor_uid))
                if (this.restrictByTutor[tutor_uid].ContainsKey(customKey))
                    this.restrictByTutor[tutor_uid].Remove(customKey);
        }


        public void DeleteFlatRestrict(int weekCycle, int weekDay, int weekPair, ExtObjKey flat_uid)
        {
            this.SetIsSaved(false);
            // Create position index in shedule matrix
            int customKey = GetKey(weekCycle, weekDay, weekPair);

            if (this.restrictByFlat.ContainsKey(flat_uid))
                if (this.restrictByFlat[flat_uid].ContainsKey(customKey))
                    this.restrictByFlat[flat_uid].Remove(customKey);
        }
        #endregion

        #region Функції перевірки зайнятості груп, викладачів, та аудиторій
        /// <summary>
        /// Функция для определения занятости преподавателя на занятиях,
        /// в определенное время
        /// </summary>
        /// <param name="TutorUID">Код преподавателя</param>
        /// <param name="SheduleKey">Код времени(День, Пара, Неделя)</param>
        /// <returns></returns>
        public bool TutorHasLesson(ExtObjKey TutorUID, int SheduleKey)
        {
            if (this.lessonsByTutor.ContainsKey(TutorUID))
                if (lessonsByTutor[TutorUID].ContainsKey(SheduleKey))
                    return true;
            return false;
        }

        /// <summary>
        /// Функция для определения занятости группы на занятиях, 
        /// в определенное время
        /// </summary>
        /// <param name="GroupUID">Код группы</param>
        /// <param name="SheduleKey">Код времени(День, Пара, Неделя)</param>
        /// <returns></returns>
        public bool GroupHasLesson(ExtObjKey GroupUID, int SheduleKey)
        {
            if (lessonsByGroup.ContainsKey(GroupUID))
                if (lessonsByGroup[GroupUID].ContainsKey(SheduleKey))
                    return true;
            return false;
        }

        /// <summary>
        /// Функция для определения наличия занятия у аудитории
        /// </summary>
        /// <param name="FlatUID">код аудитории</param>
        /// <param name="SheduleKey">Код времени(День, Пара, Неделя)</param>
        /// <returns>да, если занятие есть и нет в противном случае</returns>
        public bool FlatHasLesson(ExtObjKey FlatUID, int SheduleKey)
        {
            if (lessonsByFlat.ContainsKey(FlatUID))
                if (lessonsByFlat[FlatUID].ContainsKey(SheduleKey))
                    if (lessonsByFlat[FlatUID][SheduleKey].Count != 0)
                        return true;
            return false;
        }

        /* Занятия */

        /// <summary>
        /// Возвращает список ключей интервалов времени в которых у 
        /// преподавателя есть занятия
        /// </summary>
        /// <param name="TutorUID">Код преподавателя</param>
        /// <returns>Список ключей</returns>
        public int[] GetKeyTutorLessons(ExtObjKey TutorUID)
        {
            int[] keys = { };
            if (this.lessonsByTutor.ContainsKey(TutorUID))
            {
                keys = new int[lessonsByTutor[TutorUID].Keys.Count];
                lessonsByTutor[TutorUID].Keys.CopyTo(keys, 0);
            }

            return keys;
        }

        /// <summary>
        /// Функция для определения ключей интервалов времени в которых
        /// группа занята на занятиях
        /// </summary>
        /// <param name="GroupUID">Код группы</param>
        /// <returns>Список ключей времени(День, Пара, Неделя)</returns>
        public int[] GetKeyGroupLessons(ExtObjKey GroupUID)
        {
            int[] keys = { };
            if (this.lessonsByGroup.ContainsKey(GroupUID))
            {
                keys = new int[lessonsByGroup[GroupUID].Keys.Count];
                lessonsByGroup[GroupUID].Keys.CopyTo(keys, 0);
            }
            return keys;
        }

        /// <summary>
        /// Функция для определения ключей интервалов времени в которых
        /// аудитория занята проведением занятий
        /// </summary>
        /// <param name="FlatUID">Код аудитории</param>
        /// <returns>Список ключей времени(День, Пара, Неделя)</returns>
        public int[] GetKeyFlatLessons(ExtObjKey FlatUID)
        {
            int[] keys = { };
            if (this.lessonsByFlat.ContainsKey(FlatUID))
            {
                keys = new int[lessonsByFlat[FlatUID].Keys.Count];
                lessonsByFlat[FlatUID].Keys.CopyTo(keys, 0);
            }
            return keys;
        }

        /* Ограничения */

        /// <summary>
        /// Возвращает список ключей интервалов времени в которых у 
        /// преподавателя имеет ограничения
        /// </summary>
        /// <param name="TutorUID">Код преподавателя</param>
        /// <returns>Список ключей</returns>
        public int[] GetKeyTutorRestricts(ExtObjKey TutorUID)
        {
            int[] keys = { };
            if (this.restrictByTutor.ContainsKey(TutorUID))
            {
                keys = new int[restrictByTutor[TutorUID].Keys.Count];
                restrictByTutor[TutorUID].Keys.CopyTo(keys, 0);
            }

            return keys;
        }

        /// <summary>
        /// Функция для определения ключей интервалов времени в которых
        /// группа имеет ограничения
        /// </summary>
        /// <param name="GroupUID">Код группы</param>
        /// <returns>Список ключей времени(День, Пара, Неделя)</returns>
        public int[] GetKeyGroupRestricts(ExtObjKey GroupUID)
        {
            int[] keys = { };
            if (this.restrictByGroup.ContainsKey(GroupUID))
            {
                keys = new int[restrictByGroup[GroupUID].Keys.Count];
                restrictByGroup[GroupUID].Keys.CopyTo(keys, 0);
            }
            return keys;
        }

        /// <summary>
        /// Функция для определения ключей интервалов времени в которых
        /// аудитория имеет ограничения
        /// </summary>
        /// <param name="FlatUID">Код аудитории</param>
        /// <returns>Список ключей времени(День, Пара, Неделя)</returns>
        public int[] GetKeyFlatRestricts(ExtObjKey FlatUID)
        {
            int[] keys = { };
            if (this.restrictByFlat.ContainsKey(FlatUID))
            {
                keys = new int[restrictByFlat[FlatUID].Keys.Count];
                restrictByFlat[FlatUID].Keys.CopyTo(keys, 0);
            }
            return keys;
        }

        /* Занятость с учетом расписания и ограничений */

        /// <summary>
        /// Возвращает список ключей интервалов времени в которых у 
        /// преподавателя имеет ограничения
        /// </summary>
        /// <param name="TutorUID">Код преподавателя</param>
        /// <returns>Список ключей</returns>
        public int[] GetKeyTutorBusy(ExtObjKey TutorUID)
        {
            int[] keys = { };
            int[] keysA = { };
            int[] keysB = { };

            if (this.lessonsByTutor.ContainsKey(TutorUID))
            {
                keysA = new int[lessonsByTutor[TutorUID].Keys.Count];
                restrictByTutor[TutorUID].Keys.CopyTo(keys, 0);
            }

            if (this.restrictByTutor.ContainsKey(TutorUID))
            {
                keysB = new int[restrictByTutor[TutorUID].Keys.Count];
                restrictByTutor[TutorUID].Keys.CopyTo(keys, 0);
            }

            if (keysA.Length > 0 || keysB.Length > 0)
            {
                keys = new int[keysA.Length + keysB.Length];
                keysA.CopyTo(keys, 0);
                keysB.CopyTo(keys, keysA.Length);
            }

            return keys;
        }

        /// <summary>
        /// Функция для определения ключей интервалов времени в которых
        /// группа имеет ограничения
        /// </summary>
        /// <param name="GroupUID">Код группы</param>
        /// <returns>Список ключей времени(День, Пара, Неделя)</returns>
        public int[] GetKeyGroupBusy(ExtObjKey GroupUID)
        {
            int[] keys = { };
            int[] keysA = { };
            int[] keysB = { };

            if (this.lessonsByGroup.ContainsKey(GroupUID))
            {
                keysA = new int[lessonsByGroup[GroupUID].Keys.Count];
                restrictByGroup[GroupUID].Keys.CopyTo(keys, 0);
            }

            if (this.restrictByGroup.ContainsKey(GroupUID))
            {
                keysB = new int[restrictByGroup[GroupUID].Keys.Count];
                restrictByGroup[GroupUID].Keys.CopyTo(keys, 0);
            }

            if (keysA.Length > 0 || keysB.Length > 0)
            {
                keys = new int[keysA.Length + keysB.Length];
                keysA.CopyTo(keys, 0);
                keysB.CopyTo(keys, keysA.Length);
            }

            return keys;
        }

        /// <summary>
        /// Функция для определения ключей интервалов времени в которых
        /// аудитория имеет ограничения
        /// </summary>
        /// <param name="FlatUID">Код аудитории</param>
        /// <returns>Список ключей времени(День, Пара, Неделя)</returns>
        public int[] GetKeyFlatBusy(ExtObjKey FlatUID)
        {
            int[] keys = { };
            int[] keysA = { };
            int[] keysB = { };

            if (this.lessonsByFlat.ContainsKey(FlatUID))
            {
                keysA = new int[lessonsByFlat[FlatUID].Keys.Count];
                lessonsByFlat[FlatUID].Keys.CopyTo(keys, 0);
            }


            if (this.restrictByFlat.ContainsKey(FlatUID))
            {
                keysB = new int[restrictByFlat[FlatUID].Keys.Count];
                restrictByFlat[FlatUID].Keys.CopyTo(keys, 0);
            }

            if (keysA.Length > 0 || keysB.Length > 0)
            {
                keys = new int[keysA.Length + keysB.Length];
                keysA.CopyTo(keys, 0);
                keysB.CopyTo(keys, keysA.Length);
            }

            return keys;
        }
        #endregion

        public void RemoteGroupLessonAndLeading(ExtObjKey groupUid)
        {
            // видалення навантаження
            foreach (RemoteStudyLeading remStudyLeading in GetStudyLeadingByGroups(new ExtObjKey[] { groupUid }))
            {
                if (remStudyLeading.listOfGroups.Count == 1)
                    this.DelStudyLeading(remStudyLeading);
                else
                    remStudyLeading.listOfGroups.Remove(groupUid);
            }

            // видалення розкладу
            this.lessonsByGroup.Remove(groupUid);
        }


        public void CreateGroupWithSimilarLeading(ExtObjKey newGroupKey, ExtObjKey similarGroupKey, ExtObjKey subjectTypeKey)
        {
            IList similarGroupLeadings = GetStudyLeadingByGroups(new ExtObjKey[] { similarGroupKey });
            IList similarGroupLessons = GetGroupLessons(similarGroupKey);

            foreach (RemoteStudyLeading remStudyLeading in similarGroupLeadings)
            {
                if (remStudyLeading.subjectType_key == subjectTypeKey)
                {
                    // додаю групу до цього навантаження
                    remStudyLeading.listOfGroups.Add(newGroupKey);
                    // оновлення існуючіх занять
                    foreach (RemoteLesson remLesson in similarGroupLessons)
                        if (remLesson.leading_uid == remStudyLeading.uid)
                        {
                            // додаю групу до заняття
                            remLesson.groupList.Add(newGroupKey);
                            // додаю заняття до розкладу групи
                            int customKey = GetKey(remLesson.week_index, remLesson.day_index, remLesson.hour_index);

                            if (!lessonsByGroup.ContainsKey(newGroupKey))
                                lessonsByGroup.Add(newGroupKey, new Dictionary<int, RemoteLesson>());

                            lessonsByGroup[newGroupKey].Add(customKey, remLesson);
                        }
                }
                else
                {
                    // лише клоную навантаження та додаю його до списку
                    RemoteStudyLeading newRemoteStudyLeading = new RemoteStudyLeading();
                    newRemoteStudyLeading.department_key = remStudyLeading.department_key;
                    newRemoteStudyLeading.hourByWeek = remStudyLeading.hourByWeek;
                    newRemoteStudyLeading.listOfTutors = remStudyLeading.listOfTutors;

                    newRemoteStudyLeading.listOfGroups = new ArrayList();
                    newRemoteStudyLeading.listOfGroups.Add(newGroupKey);

                    newRemoteStudyLeading.note = remStudyLeading.note;
                    newRemoteStudyLeading.subject_key = remStudyLeading.subject_key;
                    newRemoteStudyLeading.subjectType_key = remStudyLeading.subjectType_key;
                    newRemoteStudyLeading.usedHourByWeek = 0;

                    // додаю навантаження
                    this.AddStudyLeading(newRemoteStudyLeading);
                }
            }
        }


        public IList GetAllStudyLeading()
        {
            return new ArrayList(this.studyLeading.Values);
        }


        public IList GetStudyLeadingByGroups(ExtObjKey[] groupUidList)
        {
            IList filtered = new ArrayList();
            foreach (RemoteStudyLeading rsl in new ArrayList(this.studyLeading.Values))
            {
                foreach (ExtObjKey group_uid in groupUidList)
                    if (rsl.listOfGroups.Contains(group_uid))
                    {
                        filtered.Add(rsl);
                        break;
                    }
            }
            return filtered;
        }


        public IList GetStudyLeadingByDept(ExtObjKey deptKey)
        {
            IList filtered = new ArrayList();

            foreach (RemoteStudyLeading rsl in new ArrayList(this.studyLeading.Values))
                if (rsl.department_key == deptKey) filtered.Add(rsl);

            return filtered;
        }


        public IList GetFlatLesson(ExtObjKey flat_uid, int SheduleKey)
        {
            if (FlatHasLesson(flat_uid, SheduleKey))
                return new ArrayList(this.lessonsByFlat[flat_uid][SheduleKey].Values);
            else
                return null;
        }


        public IList GetFlatLessons(ExtObjKey flat_uid)
        {
            if (this.lessonsByFlat.ContainsKey(flat_uid))
                return new ArrayList(this.lessonsByFlat[flat_uid].Values);
            else
                return new ArrayList();
        }


        public RemoteLesson GetGroupLesson(ExtObjKey group_uid, int SheduleKey)
        {
            if (GroupHasLesson(group_uid, SheduleKey))
                return this.lessonsByGroup[group_uid][SheduleKey];
            else
                return null;
        }


        public IList GetGroupLessons(ExtObjKey group_uid)
        {
            if (this.lessonsByGroup.ContainsKey(group_uid))
            {
                Dictionary<int, RemoteLesson> ht = lessonsByGroup[group_uid];
                ArrayList values = new ArrayList(ht.Values);
                return values;
            }
            else
                return new ArrayList();
        }


        public RemoteLesson GetTutorLesson(ExtObjKey tutor_uid, int SheduleKey)
        {
            if (TutorHasLesson(tutor_uid, SheduleKey))
                return lessonsByTutor[tutor_uid][SheduleKey];
            else
                return null;
        }


        public IList GetTutorLessons(ExtObjKey tutor_uid)
        {
            if (this.lessonsByTutor.ContainsKey(tutor_uid))
                return new ArrayList(lessonsByTutor[tutor_uid].Values);
            else
                return new ArrayList();
        }


        public IList GetTutorRestricts(ExtObjKey tutor_uid)
        {
            if (this.restrictByTutor.ContainsKey(tutor_uid))
                return new ArrayList(restrictByTutor[tutor_uid].Values);
            else
                return new ArrayList();
        }


        public bool IsTutorHasRestricts(ExtObjKey tutor_uid, int SheduleKey)
        {
            if (this.restrictByTutor.ContainsKey(tutor_uid))
                if (restrictByTutor[tutor_uid].ContainsKey(SheduleKey))
                    return true;
            return false;
        }


        public string GetTutorRestrictsText(ExtObjKey tutor_uid, int SheduleKey)
        {
            if (IsTutorHasRestricts(tutor_uid, SheduleKey))
                return restrictByTutor[tutor_uid][SheduleKey].Text;
            else
                return "";
        }


        public IList GetGroupRestricts(ExtObjKey group_uid)
        {
            if (this.restrictByGroup.ContainsKey(group_uid))
                return new ArrayList(restrictByGroup[group_uid].Values);
            else
                return new ArrayList();
        }


        public bool IsGroupHasRestricts(ExtObjKey group_uid, int SheduleKey)
        {
            if (this.restrictByGroup.ContainsKey(group_uid))
                if (restrictByGroup[group_uid].ContainsKey(SheduleKey))
                    return true;
            return false;
        }


        public string GetGroupRestrictsText(ExtObjKey group_uid, int SheduleKey)
        {
            if (IsGroupHasRestricts(group_uid, SheduleKey))
                return this.restrictByGroup[group_uid][SheduleKey].Text;
            else
                return "";
        }


        public IList GetFlatRestricts(ExtObjKey flat_uid)
        {
            if (this.restrictByFlat.ContainsKey(flat_uid))
                return new ArrayList(restrictByFlat[flat_uid].Values);
            else
                return new ArrayList();
        }


        public bool IsFlatHasRestricts(ExtObjKey flat_uid, int SheduleKey)
        {
            if (this.restrictByFlat.ContainsKey(flat_uid))
                if (restrictByFlat[flat_uid].ContainsKey(SheduleKey))
                    return true;
            return false;
        }


        public string GetFlatRestrictsText(ExtObjKey flat_uid, int SheduleKey)
        {
            if (IsFlatHasRestricts(flat_uid, SheduleKey))
                return this.restrictByFlat[flat_uid][SheduleKey].Text;
            else
                return "";
        }

        public IList GetLessons(ExtObjKey[] subject_uids, ExtObjKey[] subjectType_uids)
        {
            IList subjectList = new ArrayList(subject_uids);
            IList typeList = new ArrayList(subjectType_uids);
            IList lessons = new ArrayList();

            foreach (Dictionary<int, RemoteLesson> ht in shedule.Values)
                foreach (RemoteLesson rl in ht.Values)
                    if (rl.subject_key.UID != -1 && rl.studyType_key.UID != -1)
                        if (subjectList.Contains(rl.subject_key) && typeList.Contains(rl.studyType_key))
                            lessons.Add(rl);
            return lessons;
        }

        public IList GetLessons(int leading_uid)
        {
            IList lessons = new ArrayList();

            foreach (Dictionary<int, RemoteLesson> ht in shedule.Values)
                foreach (RemoteLesson rl in ht.Values)
                    if (rl.leading_uid == leading_uid)
                        lessons.Add(rl);
            return lessons;
        }

        #region Additional Info Methods
        //Признак того, сохранено или нет расписание
        public bool GetIsSaved()
        {
            return isSaved;
        }

        public void SetIsSaved(bool savedFlag)
        {
            isSaved = savedFlag;
        }

        // заголовок расписания
        public string GetSheduleTitle()
        {
            return sheduleTitle;
        }

        public void SetSheduleTitle(string title)
        {
            sheduleTitle = title;
        }

        //Имя файла
        public string GetFileName()
        {
            return fileName;
        }

        public string GetFullFileName()
        {
            return Path.Combine(directory, fileName);
        }

        public void SetFileName(string fileName)
        {
            this.fileName = fileName;
        }

        //Дата начала действия расписания
        public DateTime GetSheduleBeginDate()
        {
            return sheduleBegin;
        }
        public void SetSheduleBeginDate(DateTime beginDate)
        {
            sheduleBegin = beginDate;
        }

        //Дата окончания действия расписания
        public DateTime GetSheduleEndDate()
        {
            return sheduleEnd;
        }

        public void SetSheduleEndDate(DateTime sheduleEndDate)
        {
            sheduleEnd = sheduleEndDate;
        }

        // Число недельных циклов расписания
        public int GetWeeksCount()
        {
            return weekCyclesCount;
        }
        // Число учебных дней в недели
        public int GetDaysPerWeekCount()
        {
            return weekDaysCount;
        }
        // Число учебных пар в день
        public int GetPairsPerDayCount()
        {
            return dayPairsCount;
        }

        /// <summary>
        /// Function return custom information about shedule
        /// </summary>
        /// <returns>
        /// array of object that represent a parameters of shedule
        /// [0] - shedule Version
        /// [1] - shedule Title
        /// [2] - shedule File Name
        /// [3] - shedule Begin Date
        /// [4] - shedule End Date
        /// [5] - shedule Week count
        /// [6] - shedule Day count
        /// [7] - shedule Pair count
        /// [8] - shedule Leading count
        /// [9] - shedule Restrinct count
        /// [10] - shedule Lesson count
        /// </returns>
        public object[] GetSheduleInfo()
        {
            object[] infoArray = new object[11];
            infoArray[0] = this.version;
            infoArray[1] = this.sheduleTitle;
            infoArray[2] = this.fileName;
            infoArray[3] = this.sheduleBegin;
            infoArray[4] = this.sheduleEnd;
            infoArray[5] = this.weekCyclesCount;
            infoArray[6] = this.weekDaysCount;
            infoArray[7] = this.dayPairsCount;
            infoArray[8] = this.studyLeading.Count;
            infoArray[9] = this.restrictByFlat.Count + this.restrictByGroup.Count + this.restrictByTutor.Count;

            // Підраховую кількість занять
            int lsncnt = 0;
            foreach (int key in this.shedule.Keys)
            {
                Dictionary<int, RemoteLesson> ldngTbl = this.shedule[key];
                if (ldngTbl != null)
                    lsncnt += ldngTbl.Count;
            }
            infoArray[10] = lsncnt;

            return infoArray;
        }
        #endregion

        #region Open and Save
        private CodeUUIDBuilder _codeUUIDBuilder = new CodeUUIDBuilder();

        private bool writeUID = true;
        private bool writeUUID = true;
        private bool writeRef = true;

        private void WriteKeys(XmlTextWriter xmlwr, ExtObjKey extObjKey, bool writeUID, bool writeUUID, bool writeRef)
        {
            string code_str = UUIDConverter.UUIDToString(extObjKey.Signature, extObjKey.Index);
            xmlwr.WriteAttributeString("UID", extObjKey.UID.ToString());
            xmlwr.WriteAttributeString("UUID", code_str);
            xmlwr.WriteAttributeString("Ref", _codeUUIDBuilder.GetShortCode(code_str));
        }

        private void ReadKeys(XmlTextReader xmlrd, ref ExtObjKey extObjKey, bool readUID, bool readUUID, bool readByRef)
        {
            string attr_val = xmlrd.GetAttribute("UID");
            if (attr_val != string.Empty)
                extObjKey.UID = long.Parse(attr_val);

            if (!readByRef)
            {
                attr_val = xmlrd.GetAttribute("UUID");
                if (attr_val != string.Empty)
                    UUIDConverter.StringToUUID(attr_val,
                    out extObjKey.Signature, out extObjKey.Index);

            }
            else
            {
                string ref_str = xmlrd.GetAttribute("Ref");
                if (ref_str != string.Empty)
                {
                    UUIDConverter.StringToUUID(ref_str,
                    out extObjKey.Signature, out extObjKey.Index);
                }
            }
        }

        public void Save(string filename)
        {
            fileName = Path.GetFileName(filename);
            //MessageBox.Show(fileName);
            string tempDir = Path.Combine(Path.GetTempPath(), Path.GetFileName(filename) + "." + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Millisecond);
            //MessageBox.Show(tempDir);
            Directory.CreateDirectory(tempDir);

            // запись во временный файл
            XmlTextWriter xmlwr = new XmlTextWriter(Path.Combine(tempDir, "Info.xml"), System.Text.Encoding.UTF8);
            try
            {
                xmlwr.WriteStartDocument(true);
                xmlwr.WriteStartElement("Shedule");
                xmlwr.WriteAttributeString("Version", "0.2");
                xmlwr.WriteAttributeString("Title", this.sheduleTitle);

                xmlwr.WriteStartElement("Settings");
                xmlwr.WriteAttributeString("BeginDate", this.sheduleBegin.Date.ToShortDateString());
                xmlwr.WriteAttributeString("EndDate", this.sheduleEnd.Date.ToShortDateString());
                xmlwr.WriteAttributeString("DayPerWeek", this.weekDaysCount.ToString());
                xmlwr.WriteAttributeString("PairPerDay", this.dayPairsCount.ToString());
                xmlwr.WriteAttributeString("WeekCount", this.weekCyclesCount.ToString());
                xmlwr.WriteEndElement();

                xmlwr.WriteEndElement();
                xmlwr.WriteEndDocument();
                isSaved = true;
                xmlwr.Flush();
                xmlwr.Close();
            }
            catch
            {
                isSaved = false;
            }

            xmlwr = new XmlTextWriter(Path.Combine(tempDir, "Leadings.xml"), System.Text.Encoding.UTF8);
            try
            {
                xmlwr.WriteStartDocument(true);
                xmlwr.WriteStartElement("Leadings");

                WriteLeading(xmlwr);

                xmlwr.WriteEndElement();
                xmlwr.WriteEndDocument();
                isSaved = true;

                xmlwr.Flush();
                xmlwr.Close();
            }
            catch
            {
                isSaved = false;
            }

            xmlwr = new XmlTextWriter(Path.Combine(tempDir, "Shedule.xml"), System.Text.Encoding.UTF8);
            try
            {
                xmlwr.WriteStartDocument(true);
                xmlwr.WriteStartElement("Lessons");
                //Запись элементов расписания
                IEnumerator enumerator = shedule.Values.GetEnumerator();

                while (enumerator.MoveNext())
                {
                    IEnumerator lessonEnumerator = (enumerator.Current as Dictionary<int, RemoteLesson>).Values.GetEnumerator();
                    while (lessonEnumerator.MoveNext())
                    {
                        RemoteLesson lsn = (lessonEnumerator.Current as RemoteLesson);
                        if (lsn != null)
                        {
                            xmlwr.WriteStartElement("Lesson");

                            xmlwr.WriteStartElement("Cycle");
                            WriteKeys(xmlwr, lsn.week_key, writeUID, writeUUID, writeRef);
                            //xmlwr.WriteAttributeString("UID", lsn.week_key.UID.ToString());
                            //xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString( lsn.week_key.Signature, lsn.week_key.Index));
                            xmlwr.WriteAttributeString("Index", lsn.week_index.ToString());
                            xmlwr.WriteEndElement();

                            xmlwr.WriteStartElement("Day");
                            WriteKeys(xmlwr, lsn.day_key, writeUID, writeUUID, writeRef);
                            //xmlwr.WriteAttributeString("UID", lsn.day_key.UID.ToString());
                            //xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString( lsn.day_key.Signature, lsn.day_key.Index));
                            xmlwr.WriteAttributeString("Index", lsn.day_index.ToString());
                            xmlwr.WriteEndElement();

                            xmlwr.WriteStartElement("Pair");
                            WriteKeys(xmlwr, lsn.hour_key, writeUID, writeUUID, writeRef);
                            //xmlwr.WriteAttributeString("UID", lsn.hour_key.UID.ToString());
                            //xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString( lsn.hour_key.Signature, lsn.hour_key.Index));
                            xmlwr.WriteAttributeString("Index", lsn.hour_index.ToString());

                            xmlwr.WriteEndElement();

                            xmlwr.WriteStartElement("Subject");
                            if (lsn.subject_key.UID != -1)
                            {
                                WriteKeys(xmlwr, lsn.subject_key, writeUID, writeUUID, writeRef);
                                //xmlwr.WriteAttributeString("UID", lsn.subject_key.UID.ToString());
                                //xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString( lsn.subject_key.Signature, lsn.subject_key.Index));

                            }
                            xmlwr.WriteEndElement();

                            xmlwr.WriteStartElement("SubjectType");
                            if (lsn.studyType_key.UID != -1)
                            {
                                WriteKeys(xmlwr, lsn.studyType_key, writeUID, writeUUID, writeRef);
                                //xmlwr.WriteAttributeString("UID", lsn.studyType_key.UID.ToString());
                                //xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString( lsn.studyType_key.Signature, lsn.studyType_key.Index));
                            }
                            xmlwr.WriteEndElement();

                            xmlwr.WriteStartElement("LeadingUid");
                            xmlwr.WriteString(lsn.leading_uid.ToString());
                            xmlwr.WriteEndElement();

                            xmlwr.WriteStartElement("UserUid");
                            WriteKeys(xmlwr, lsn.user_key, writeUID, writeUUID, writeRef);
                            //xmlwr.WriteAttributeString("UID", lsn.user_key.UID.ToString() );
                            //xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString( lsn.user_key.Signature, lsn.user_key.Index));
                            xmlwr.WriteEndElement();

                            xmlwr.WriteStartElement("Groups");
                            foreach (ExtObjKey gr_uid in lsn.groupList)
                            {
                                if (gr_uid != null && !gr_uid.IsEmpty)
                                {
                                    xmlwr.WriteStartElement("Group");
                                    WriteKeys(xmlwr, gr_uid, writeUID, writeUUID, writeRef);
                                    //xmlwr.WriteAttributeString("UID", gr_uid.UID.ToString());
                                    //xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString( gr_uid.Signature, gr_uid.Index));
                                    xmlwr.WriteEndElement();
                                }
                            }
                            xmlwr.WriteEndElement();

                            xmlwr.WriteStartElement("Flats");
                            foreach (ExtObjKey flt_uid in lsn.flatList)
                            {
                                if (flt_uid != null && !flt_uid.IsEmpty)
                                {
                                    xmlwr.WriteStartElement("Flat");
                                    WriteKeys(xmlwr, flt_uid, writeUID, writeUUID, writeRef);
                                    //xmlwr.WriteAttributeString("UID", flt_uid.UID.ToString());
                                    //xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString( flt_uid.Signature, flt_uid.Index));
                                    xmlwr.WriteEndElement();
                                }
                            }
                            xmlwr.WriteEndElement();

                            xmlwr.WriteStartElement("Tutors");
                            foreach (ExtObjKey ttr_uid in lsn.tutorList)
                            {
                                if (ttr_uid != null && !ttr_uid.IsEmpty)
                                {
                                    xmlwr.WriteStartElement("Tutor");
                                    WriteKeys(xmlwr, ttr_uid, writeUID, writeUUID, writeRef);
                                    //xmlwr.WriteAttributeString("UID", ttr_uid.UID.ToString());
                                    //xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString( ttr_uid.Signature, ttr_uid.Index));
                                    xmlwr.WriteEndElement();
                                }
                            }
                            xmlwr.WriteEndElement();



                            xmlwr.WriteEndElement();
                        }
                    }
                }
                xmlwr.WriteEndElement();
                xmlwr.WriteEndDocument();
                isSaved = true;
                xmlwr.Flush();
                xmlwr.Close();
            }
            catch
            {
                isSaved = false;
            }

            xmlwr = new XmlTextWriter(Path.Combine(tempDir, "Restricts.xml"), System.Text.Encoding.UTF8);
            try
            {
                xmlwr.WriteStartDocument(true);
                xmlwr.WriteStartElement("Restricts");

                WriteFlatRestricts(xmlwr);

                WriteGroupRestricts(xmlwr);

                WriteTutorRestricts(xmlwr);

                xmlwr.WriteEndElement(); // restricts
                xmlwr.WriteEndDocument();

                xmlwr.Flush();
                xmlwr.Close();

                isSaved = true;
            }
            catch
            {
                isSaved = false;
            }

            xmlwr = new XmlTextWriter(Path.Combine(tempDir, "Keys.xml"), System.Text.Encoding.UTF8);
            try
            {
                xmlwr.WriteStartDocument(true);
                _codeUUIDBuilder.WriteShortCodeTable(xmlwr);
                xmlwr.WriteEndDocument();

                xmlwr.Flush();
                xmlwr.Close();

                isSaved = true;
            }
            catch
            {
                isSaved = false;
            }

            if (isSaved)
            {
                string tempFileName = Path.Combine(Path.GetDirectoryName(filename), Path.GetFileName(tempDir));

                SheduleCompressor.MultyCompress(tempFileName, Directory.GetFiles(tempDir));
                File.Copy(tempFileName, filename, true);
            }

        }

        private void WriteLeading(XmlTextWriter xmlwr)
        {
            foreach (RemoteStudyLeading sl in new ArrayList(this.studyLeading.Values))
            {
                if (sl.uid == 0)
                    sl.uid = this.LeadingUidCode++;
            }

            xmlwr.WriteAttributeString("GeneratorValue", this.LeadingUidCode.ToString());

            IEnumerator leadingenum = this.studyLeading.Values.GetEnumerator();
            while (leadingenum.MoveNext())
            {
                if (leadingenum.Current is RemoteStudyLeading)
                {
                    RemoteStudyLeading stload = leadingenum.Current as RemoteStudyLeading;

                    xmlwr.WriteStartElement("StudyLeading");

                    //Код
                    xmlwr.WriteStartElement("UID");
                    xmlwr.WriteString(stload.uid.ToString());
                    xmlwr.WriteEndElement();

                    //Предмет
                    xmlwr.WriteStartElement("Subject");
                    //if(stload.subject_uid != -1)
                    //{
                    //string uuid_str = UUIDConverter.UUIDToString(stload.subject_key.Signature, stload.subject_key.Index);
                    WriteKeys(xmlwr, stload.subject_key, writeUID, writeUUID, writeRef);
                    //xmlwr.WriteAttributeString("UID", .UID.ToString());
                    //xmlwr.WriteAttributeString("UUID", uuid_str);
                    //xmlwr.WriteAttributeString("ref", _codeUUIDBuilder.GetShortCode(uuid_str));
                    //}
                    xmlwr.WriteEndElement();

                    //Тип занятия
                    //uuid_str = UUIDConverter.UUIDToString( stload.subjectType_key.Signature, stload.subjectType_key.Index);
                    xmlwr.WriteStartElement("StudyForm");
                    WriteKeys(xmlwr, stload.subjectType_key, writeUID, writeUUID, writeRef);
                    //xmlwr.WriteAttributeString("UID", stload.subjectType_key.UID.ToString());
                    //xmlwr.WriteAttributeString("UUID", uuid_str);
                    //xmlwr.WriteAttributeString("ref", _codeUUIDBuilder.GetShortCode(uuid_str));
                    xmlwr.WriteEndElement();

                    //Кафедра
                    xmlwr.WriteStartElement("Department");
                    WriteKeys(xmlwr, stload.department_key, writeUID, writeUUID, writeRef);
                    //uuid_str = UUIDConverter.UUIDToString( stload.department_key.Signature, stload.department_key.Index);
                    //xmlwr.WriteAttributeString("UID", stload.department_key.UID.ToString());
                    //xmlwr.WriteAttributeString("UUID", uuid_str);
                    //xmlwr.WriteAttributeString("ref", _codeUUIDBuilder.GetShortCode(uuid_str));
                    xmlwr.WriteEndElement();

                    //Запланированно часов
                    xmlwr.WriteStartElement("Hour");
                    xmlwr.WriteString(stload.hourByWeek.ToString());
                    xmlwr.WriteEndElement();

                    //Использованно часов
                    xmlwr.WriteStartElement("UsedHour");
                    xmlwr.WriteString(stload.usedHourByWeek.ToString());
                    xmlwr.WriteEndElement();

                    //Примечания
                    xmlwr.WriteStartElement("Note");
                    if (stload.note != string.Empty)
                        xmlwr.WriteString(stload.note);
                    else
                        xmlwr.WriteString("");
                    xmlwr.WriteEndElement();

                    //Группы
                    xmlwr.WriteStartElement("ForGroups");
                    foreach (ExtObjKey gr_key in stload.listOfGroups)
                    {
                        if (gr_key != null && !gr_key.IsEmpty)
                        {
                            xmlwr.WriteStartElement("Group");
                            WriteKeys(xmlwr, gr_key, writeUID, writeUUID, writeRef);
                            //uuid_str = UUIDConverter.UUIDToString( gr_key.Signature, gr_key.Index);
                            //xmlwr.WriteAttributeString("UID",gr_key.UID.ToString());
                            //xmlwr.WriteAttributeString("UUID", _codeUUIDBuilder.GetShortCode(uuid_str));
                            xmlwr.WriteEndElement();
                        }
                    }
                    xmlwr.WriteEndElement();

                    //Преподаватели
                    xmlwr.WriteStartElement("ForTutors");
                    foreach (ExtObjKey tr_key in stload.listOfTutors)
                    {
                        if (tr_key != null && !tr_key.IsEmpty)
                        {
                            xmlwr.WriteStartElement("Tutor");
                            WriteKeys(xmlwr, tr_key, writeUID, writeUUID, writeRef);
                            //uuid_str = UUIDConverter.UUIDToString( tr_key.Signature, tr_key.Index);
                            //xmlwr.WriteAttributeString("UID",tr_key.UID.ToString());
                            //xmlwr.WriteAttributeString("UUID", _codeUUIDBuilder.GetShortCode(uuid_str));
                            xmlwr.WriteEndElement();
                        }
                    }
                    xmlwr.WriteEndElement();

                    xmlwr.WriteEndElement();
                }

            }
        }

        public string WriteDeptLeading(ExtObjKey deptKey)
        {
            StringBuilder sb = new StringBuilder("");
            XmlTextWriter xmlwr = new XmlTextWriter(new StringWriter(sb));
            try
            {
                xmlwr.WriteStartElement("Leadings");
                IEnumerator leadingenum = this.studyLeading.Values.GetEnumerator();
                while (leadingenum.MoveNext())
                {
                    if (leadingenum.Current is RemoteStudyLeading)
                        if ((leadingenum.Current as RemoteStudyLeading).department_key == deptKey)
                        {
                            RemoteStudyLeading stload = leadingenum.Current as RemoteStudyLeading;

                            xmlwr.WriteStartElement("StudyLeading");

                            //Код
                            xmlwr.WriteStartElement("UID");
                            xmlwr.WriteString(stload.uid.ToString());
                            xmlwr.WriteEndElement();

                            //Предмет
                            xmlwr.WriteStartElement("Subject");
                            //if(stload.subject_uid != -1)
                            //{
                            xmlwr.WriteAttributeString("UID", stload.subject_key.UID.ToString());
                            xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString(stload.subject_key.Signature, stload.subject_key.Index));

                            //}
                            xmlwr.WriteEndElement();

                            //Тип занятия
                            xmlwr.WriteStartElement("StudyForm");
                            xmlwr.WriteAttributeString("UID", stload.subjectType_key.UID.ToString());
                            xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString(stload.subjectType_key.Signature, stload.subjectType_key.Index));
                            xmlwr.WriteEndElement();

                            //Кафедра
                            xmlwr.WriteStartElement("Department");
                            xmlwr.WriteAttributeString("UID", stload.department_key.UID.ToString());
                            xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString(stload.department_key.Signature, stload.department_key.Index));
                            xmlwr.WriteEndElement();

                            //Запланированно часов
                            xmlwr.WriteStartElement("Hour");
                            xmlwr.WriteString(stload.hourByWeek.ToString());
                            xmlwr.WriteEndElement();

                            //Использованно часов
                            xmlwr.WriteStartElement("UsedHour");
                            xmlwr.WriteString(stload.usedHourByWeek.ToString());
                            xmlwr.WriteEndElement();

                            //Примечания
                            xmlwr.WriteStartElement("Note");
                            if (stload.note != string.Empty)
                                xmlwr.WriteString(stload.note);
                            else
                                xmlwr.WriteString("");
                            xmlwr.WriteEndElement();

                            //Группы
                            xmlwr.WriteStartElement("ForGroups");
                            foreach (ExtObjKey gr_key in stload.listOfGroups)
                            {
                                if (gr_key != null && !gr_key.IsEmpty)
                                {
                                    xmlwr.WriteStartElement("Group");
                                    xmlwr.WriteAttributeString("UID", gr_key.UID.ToString());
                                    xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString(gr_key.Signature, gr_key.Index));
                                    xmlwr.WriteEndElement();
                                }
                            }
                            xmlwr.WriteEndElement();

                            //Преподаватели
                            xmlwr.WriteStartElement("ForTutors");
                            foreach (ExtObjKey tr_key in stload.listOfTutors)
                            {
                                if (tr_key != null && !tr_key.IsEmpty)
                                {
                                    xmlwr.WriteStartElement("Tutor");
                                    xmlwr.WriteAttributeString("UID", tr_key.UID.ToString());
                                    xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString(tr_key.Signature, tr_key.Index));
                                    xmlwr.WriteEndElement();
                                }
                            }
                            xmlwr.WriteEndElement();

                            xmlwr.WriteEndElement();
                        }
                }
                xmlwr.WriteEndElement();
            }
            finally
            {
                xmlwr.Flush();
                xmlwr.Close();
            }
            return sb.ToString();
        }

        private void WriteTutorRestricts(XmlTextWriter xmlwr)
        {
            #region write tutor restrict
            IEnumerator enumeratorTutor = restrictByTutor.Values.GetEnumerator();
            IEnumerator keyEnumeratorTutor = restrictByTutor.Keys.GetEnumerator();
            xmlwr.WriteStartElement("TutorRestricts");
            while (enumeratorTutor.MoveNext() && keyEnumeratorTutor.MoveNext())
            {
                IEnumerator restrictEnumerator = (enumeratorTutor.Current as Dictionary<int, RemoteRestrict>).Values.GetEnumerator();
                while (restrictEnumerator.MoveNext())
                {
                    xmlwr.WriteStartElement("TutorRestrict");
                    ExtObjKey extObjKey = (keyEnumeratorTutor.Current as ExtObjKey);
                    WriteKeys(xmlwr, extObjKey, writeUID, writeUUID, writeRef);
                    //xmlwr.WriteAttributeString("UID", extObjKey.UID.ToString());
                    //xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString(extObjKey.Signature, extObjKey.Index));

                    RemoteRestrict restrict = restrictEnumerator.Current as RemoteRestrict;

                    xmlwr.WriteStartElement("Comment");
                    xmlwr.WriteString(restrict.Text);
                    xmlwr.WriteEndElement();

                    xmlwr.WriteStartElement("Cycle");
                    WriteKeys(xmlwr, restrict.week_key, writeUID, writeUUID, writeRef);
                    //xmlwr.WriteAttributeString("UID", restrict.week_key.UID.ToString());
                    //xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString( restrict.week_key.Signature, restrict.week_key.Index));
                    xmlwr.WriteAttributeString("Index", restrict.WeekIndex.ToString());
                    xmlwr.WriteEndElement();

                    xmlwr.WriteStartElement("Day");
                    WriteKeys(xmlwr, restrict.day_key, writeUID, writeUUID, writeRef);
                    //xmlwr.WriteAttributeString("UID", restrict.day_key.UID.ToString());
                    //xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString( restrict.day_key.Signature, restrict.day_key.Index));
                    xmlwr.WriteAttributeString("Index", restrict.DayIndex.ToString());
                    xmlwr.WriteEndElement();

                    xmlwr.WriteStartElement("Pair");
                    WriteKeys(xmlwr, restrict.hour_key, writeUID, writeUUID, writeRef);
                    //xmlwr.WriteAttributeString("UID", restrict.hour_key.UID.ToString());
                    //xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString( restrict.hour_key.Signature, restrict.hour_key.Index));
                    xmlwr.WriteAttributeString("Index", restrict.HourIndex.ToString());
                    xmlwr.WriteEndElement();

                    xmlwr.WriteEndElement();
                }
            }
            xmlwr.WriteEndElement();
            #endregion
        }

        public string WriteTutorRestricts()
        {
            StringBuilder sb = new StringBuilder("");
            XmlTextWriter xmlwr = new XmlTextWriter(new StringWriter(sb));
            try
            {
                WriteTutorRestricts(xmlwr);
            }
            finally
            {
                xmlwr.Flush();
                xmlwr.Close();
            }
            return sb.ToString();
        }

        private void WriteGroupRestricts(XmlTextWriter xmlwr)
        {
            #region write group restrict
            IEnumerator enumeratorGroup = restrictByGroup.Values.GetEnumerator();
            IEnumerator keyEnumeratorGroup = restrictByGroup.Keys.GetEnumerator();
            xmlwr.WriteStartElement("GroupRestricts");
            while (enumeratorGroup.MoveNext() && keyEnumeratorGroup.MoveNext())
            {
                IEnumerator restrictEnumerator = (enumeratorGroup.Current as Dictionary<int, RemoteRestrict>).Values.GetEnumerator();
                while (restrictEnumerator.MoveNext())
                {
                    xmlwr.WriteStartElement("GroupRestrict");
                    ExtObjKey extObjKey = (keyEnumeratorGroup.Current as ExtObjKey);
                    WriteKeys(xmlwr, extObjKey, writeUID, writeUUID, writeRef);
                    //xmlwr.WriteAttributeString("UID", extObjKey.UID.ToString());
                    //xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString(extObjKey.Signature, extObjKey.Index));

                    RemoteRestrict restrict = restrictEnumerator.Current as RemoteRestrict;

                    xmlwr.WriteStartElement("Comment");
                    xmlwr.WriteString(restrict.Text);
                    xmlwr.WriteEndElement();

                    xmlwr.WriteStartElement("Cycle");
                    WriteKeys(xmlwr, restrict.week_key, writeUID, writeUUID, writeRef);
                    //xmlwr.WriteAttributeString("UID", restrict.week_key.UID.ToString());
                    xmlwr.WriteAttributeString("Index", restrict.WeekIndex.ToString());
                    xmlwr.WriteEndElement();

                    xmlwr.WriteStartElement("Day");
                    WriteKeys(xmlwr, restrict.day_key, writeUID, writeUUID, writeRef);
                    //xmlwr.WriteAttributeString("UID", restrict.day_key.UID.ToString());
                    xmlwr.WriteAttributeString("Index", restrict.DayIndex.ToString());
                    xmlwr.WriteEndElement();

                    xmlwr.WriteStartElement("Pair");
                    WriteKeys(xmlwr, restrict.hour_key, writeUID, writeUUID, writeRef);
                    //xmlwr.WriteAttributeString("UID", restrict.hour_key.UID.ToString());
                    xmlwr.WriteAttributeString("Index", restrict.HourIndex.ToString());
                    xmlwr.WriteEndElement();

                    xmlwr.WriteEndElement();
                }
            }
            xmlwr.WriteEndElement();
            #endregion
        }

        public string WriteGroupRestricts()
        {
            StringBuilder sb = new StringBuilder("");
            XmlTextWriter xmlwr = new XmlTextWriter(new StringWriter(sb));
            try
            {
                WriteGroupRestricts(xmlwr);
            }
            finally
            {
                xmlwr.Flush();
                xmlwr.Close();
            }
            return sb.ToString();
        }

        private void WriteFlatRestricts(XmlTextWriter xmlwr)
        {
            #region write flat restricts
            IEnumerator enumeratorFlat = restrictByFlat.Values.GetEnumerator();
            IEnumerator keyEnumeratorFlat = restrictByFlat.Keys.GetEnumerator();
            xmlwr.WriteStartElement("FlatRestricts");
            while (enumeratorFlat.MoveNext() && keyEnumeratorFlat.MoveNext())
            {
                IEnumerator restrictEnumerator = (enumeratorFlat.Current as Dictionary<int, RemoteRestrict>).Values.GetEnumerator();
                while (restrictEnumerator.MoveNext())
                {
                    xmlwr.WriteStartElement("FlatRestrict");
                    ExtObjKey extObjKey = (keyEnumeratorFlat.Current as ExtObjKey);
                    WriteKeys(xmlwr, extObjKey, writeUID, writeUUID, writeRef);
                    //xmlwr.WriteAttributeString("UID", extObjKey.UID.ToString());
                    //xmlwr.WriteAttributeString("UUID", UUIDConverter.UUIDToString(extObjKey.Signature, extObjKey.Index));

                    RemoteRestrict restrict = restrictEnumerator.Current as RemoteRestrict;

                    xmlwr.WriteStartElement("Comment");
                    xmlwr.WriteString(restrict.Text);
                    xmlwr.WriteEndElement();

                    xmlwr.WriteStartElement("Cycle");
                    WriteKeys(xmlwr, restrict.week_key, writeUID, writeUUID, writeRef);
                    //xmlwr.WriteAttributeString("UID", restrict.week_key.UID.ToString());
                    xmlwr.WriteAttributeString("Index", restrict.WeekIndex.ToString());
                    xmlwr.WriteEndElement();

                    xmlwr.WriteStartElement("Day");
                    WriteKeys(xmlwr, restrict.day_key, writeUID, writeUUID, writeRef);
                    //xmlwr.WriteAttributeString("UID", restrict.day_key.UID.ToString());
                    xmlwr.WriteAttributeString("Index", restrict.DayIndex.ToString());
                    xmlwr.WriteEndElement();

                    xmlwr.WriteStartElement("Pair");
                    WriteKeys(xmlwr, restrict.hour_key, writeUID, writeUUID, writeRef);
                    //xmlwr.WriteAttributeString("UID", restrict.hour_key.UID.ToString());
                    xmlwr.WriteAttributeString("Index", restrict.HourIndex.ToString());
                    xmlwr.WriteEndElement();

                    xmlwr.WriteEndElement();
                }
            }
            xmlwr.WriteEndElement();
            #endregion
        }

        public string WriteFlatRestricts()
        {
            StringBuilder sb = new StringBuilder("");
            XmlTextWriter xmlwr = new XmlTextWriter(new StringWriter(sb));
            try
            {
                WriteFlatRestricts(xmlwr);
            }
            finally
            {
                xmlwr.Flush();
                xmlwr.Close();
            }

            return sb.ToString();
        }


        public void Open(string filename)
        {
            string tempDir = Path.Combine(Path.GetTempPath(), Path.GetFileName(filename) + "." + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Millisecond);
            Directory.CreateDirectory(tempDir);

            SheduleCompressor.MultyDecompress(filename, tempDir);

            XmlTextReader xmlrd;

            #region Keys.xml
            if (File.Exists(Path.Combine(tempDir, "Keys.xml")))
            {
                xmlrd = new XmlTextReader(Path.Combine(tempDir, "Keys.xml"));
                _codeUUIDBuilder.ReadShortCodeTable(xmlrd);
            }
            #endregion Keys.xml

            #region Read Info.xml
            xmlrd = new XmlTextReader(Path.Combine(tempDir, "Info.xml"));
            try
            {
                while (xmlrd.Read())
                {
                    if (xmlrd.NodeType == XmlNodeType.Element)
                    {
                        if (xmlrd.Name == "Shedule")
                        {
                            this.directory = Path.GetDirectoryName(filename);
                            this.fileName = Path.GetFileName(filename);

                            this.version = xmlrd.GetAttribute("Version");
                            this.sheduleTitle = xmlrd.GetAttribute("Title");
                        }

                        if (xmlrd.Name == "Settings")
                        {
                            this.sheduleBegin = DateTime.Parse(xmlrd.GetAttribute("BeginDate"));
                            this.sheduleEnd = DateTime.Parse(xmlrd.GetAttribute("EndDate"));
                            this.weekDaysCount = int.Parse(xmlrd.GetAttribute("DayPerWeek"));
                            this.dayPairsCount = int.Parse(xmlrd.GetAttribute("PairPerDay"));
                            this.weekCyclesCount = int.Parse(xmlrd.GetAttribute("WeekCount"));
                        }
                    }

                }

            }
            catch (Exception e)
            {
                MessageBox.Show("Помилка читання файлу опису розкладу (Info.xml), у " + xmlrd.LineNumber + " строчці: " + e.Message, "Помилка",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            #endregion Read Info.xml

            #region Read file Leadings.xml
            xmlrd = new XmlTextReader(Path.Combine(tempDir, "Leadings.xml"));
            try
            {
                while (xmlrd.Read())
                {
                    if (xmlrd.NodeType == XmlNodeType.Element && xmlrd.Name == "Leadings")
                        this.LeadingUidCode = int.Parse(xmlrd.GetAttribute("GeneratorValue"));

                    ReadLeading(xmlrd, false);
                }
                xmlrd.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show("Помилка читання файлу навантажень (Leadings.xml), у " + xmlrd.LineNumber + " строчці: " + e.Message, "Помилка",
                MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            #endregion Read file Leadings.xml

            #region Read file Shedule.xml
            xmlrd = new XmlTextReader(Path.Combine(tempDir, "Shedule.xml"));
            try
            {
                while (xmlrd.Read())
                {
                    # region Read Lesson
                    if (xmlrd.NodeType == XmlNodeType.Element && xmlrd.Name == "Lesson")
                    {
                        int _leadingUid = 0;

                        ExtObjKey _studyCycle = new ExtObjKey(-1);
                        int _studyCycleIndex = -1;

                        ExtObjKey _studyDay = new ExtObjKey(-1);
                        int _studyDayIndex = -1;

                        ExtObjKey _studyHour = new ExtObjKey(-1);
                        int _studyHourIndex = -1;

                        ExtObjKey _studySubject = new ExtObjKey(-1);
                        ExtObjKey _studyType = new ExtObjKey(-1);

                        ExtObjKey _User = new ExtObjKey(-1);

                        ArrayList _groupList = new ArrayList();
                        ArrayList _flatList = new ArrayList();
                        ArrayList _tutorList = new ArrayList();

                        while (xmlrd.Read())
                        {
                            if (xmlrd.NodeType == XmlNodeType.Element)
                            {
                                #region Read Cycle
                                if (xmlrd.Name == "Cycle")
                                {
                                    _studyCycle.UID = long.Parse(xmlrd.GetAttribute("UID"));

                                    if (xmlrd.AttributeCount == 4)
                                    {
                                        string str = xmlrd.GetAttribute("UUID");
                                        UUIDConverter.StringToUUID(str, out _studyCycle.Signature, out _studyCycle.Index);
                                    }
                                    string indexString = xmlrd.GetAttribute("Index");
                                    if (indexString != string.Empty && indexString != null)
                                        _studyCycleIndex = int.Parse(xmlrd.GetAttribute("Index"));
                                }
                                #endregion

                                #region Read Day
                                if (xmlrd.Name == "Day")
                                {
                                    _studyDay.UID = long.Parse(xmlrd.GetAttribute("UID"));

                                    if (xmlrd.AttributeCount == 4)
                                        UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out _studyDay.Signature, out _studyDay.Index);

                                    string indexString = xmlrd.GetAttribute("Index");
                                    if (indexString != string.Empty && indexString != null)
                                        _studyDayIndex = int.Parse(xmlrd.GetAttribute("Index"));
                                }
                                #endregion

                                #region Read Pair
                                if (xmlrd.Name == "Pair")
                                {
                                    _studyHour.UID = long.Parse(xmlrd.GetAttribute("UID"));

                                    if (xmlrd.AttributeCount == 4)
                                        UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out _studyHour.Signature, out _studyHour.Index);

                                    string indexString = xmlrd.GetAttribute("Index");
                                    if (indexString != string.Empty && indexString != null)
                                        _studyHourIndex = int.Parse(xmlrd.GetAttribute("Index"));
                                }
                                #endregion

                                #region Read Subject
                                if (xmlrd.Name == "Subject")
                                {
                                    _studySubject.UID = long.Parse(xmlrd.GetAttribute("UID"));

                                    if (xmlrd.AttributeCount != 1)
                                        UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out _studySubject.Signature, out _studySubject.Index);
                                }
                                #endregion

                                #region Read SubjectType
                                if (xmlrd.Name == "SubjectType")
                                {
                                    _studyType.UID = long.Parse(xmlrd.GetAttribute("UID"));

                                    if (xmlrd.AttributeCount != 1)
                                        UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out _studyType.Signature, out _studyType.Index);
                                }
                                #endregion

                                #region Read LeadingUid
                                if (xmlrd.Name == "LeadingUid")
                                {
                                    xmlrd.Read();

                                    _leadingUid = int.Parse(xmlrd.Value);
                                }
                                #endregion

                                #region Read UserUid
                                if (xmlrd.Name == "UserUid")
                                {
                                    _User.UID = long.Parse(xmlrd.GetAttribute("UID"));

                                    if (xmlrd.AttributeCount != 1)
                                        UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out _User.Signature, out _User.Index);

                                }
                                #endregion

                                #region Read Группы
                                if (xmlrd.Name == "Groups" && !xmlrd.IsEmptyElement)
                                {
                                    while (xmlrd.Read())
                                    {
                                        if (xmlrd.NodeType == XmlNodeType.Element)
                                        {
                                            if (xmlrd.Name == "Group")
                                            {
                                                ExtObjKey groupKey = new ExtObjKey(-1);

                                                groupKey.UID = long.Parse(xmlrd.GetAttribute("UID"));
                                                if (xmlrd.AttributeCount != 1)
                                                    UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out groupKey.Signature, out groupKey.Index);

                                                if (!_groupList.Contains(groupKey)) _groupList.Add(groupKey);
                                            }
                                        }
                                        if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "Groups")
                                            break;
                                    }
                                }
                                #endregion

                                #region Аудитории
                                if (xmlrd.Name == "Flats" && !xmlrd.IsEmptyElement)
                                {
                                    while (xmlrd.Read())
                                    {
                                        if (xmlrd.NodeType == XmlNodeType.Element)
                                        {
                                            ExtObjKey flatKey = new ExtObjKey(-1);
                                            flatKey.UID = long.Parse(xmlrd.GetAttribute("UID"));
                                            if (xmlrd.AttributeCount != 1)
                                                UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out flatKey.Signature, out flatKey.Index);

                                            _flatList.Add(flatKey);
                                        }
                                        if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "Flats")
                                            break;
                                    }
                                }
                                #endregion

                                #region Преподаватели
                                if (xmlrd.Name == "Tutors" && !xmlrd.IsEmptyElement)
                                {
                                    while (xmlrd.Read())
                                    {
                                        if (xmlrd.NodeType == XmlNodeType.Element)
                                        {
                                            ExtObjKey tutorKey = new ExtObjKey(-1);

                                            tutorKey.UID = long.Parse(xmlrd.GetAttribute("UID"));
                                            if (xmlrd.AttributeCount != 1)
                                                UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out tutorKey.Signature, out tutorKey.Index);

                                            _tutorList.Add(tutorKey);
                                        }
                                        if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "Tutors")
                                            break;
                                    }
                                }
                                #endregion
                            }
                            if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "Lesson")
                            {
                                RemoteLesson newRemoteLesson = new RemoteLesson();
                                newRemoteLesson.leading_uid = _leadingUid;
                                newRemoteLesson.user_key = _User;
                                newRemoteLesson.subject_key = _studySubject;
                                newRemoteLesson.studyType_key = _studyType;
                                newRemoteLesson.week_key = _studyCycle;
                                newRemoteLesson.week_index = _studyCycleIndex;
                                newRemoteLesson.day_key = _studyDay;
                                newRemoteLesson.day_index = _studyDayIndex;
                                newRemoteLesson.hour_key = _studyHour;
                                newRemoteLesson.hour_index = _studyHourIndex;
                                newRemoteLesson.groupList = _groupList;
                                newRemoteLesson.flatList = _flatList;
                                newRemoteLesson.tutorList = _tutorList;

                                //вставка считанного урока в расписание
                                try
                                {
                                    SimpleInsert(newRemoteLesson.week_index, newRemoteLesson.day_index, newRemoteLesson.hour_index, newRemoteLesson);
                                    studyLeading[newRemoteLesson.leading_uid].usedHourByWeek++;
                                }
                                catch (Exception e)
                                {
                                    MessageBox.Show("Помилка читання файлу розкладу (Shedule.xml), у " + xmlrd.LineNumber + " строчці: " + e.Message, "Помилка",
                                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                                break;
                            }
                        }
                    }
                    #endregion
                }
                xmlrd.Close();
            }
            catch
            {
            }
            #endregion Read file Shedule.xml

            #region Read file Restricts.xml
            xmlrd = new XmlTextReader(Path.Combine(tempDir, "Restricts.xml"));
            try
            {
                while (xmlrd.Read())
                    ReadRestricts(xmlrd);

                xmlrd.Close();
            }
            catch (Exception e)
            {
                MessageBox.Show("Ошибка чтения файла: /n" + e.Message);
            }
            #endregion Read file Restricts.xml

            isSaved = true;
        }

        System.Collections.Generic.List<ExtObjKey> _presentGroups =
        new System.Collections.Generic.List<ExtObjKey>();

        public ExtObjKey[] GetPresentGroups()
        {
            return _presentGroups.ToArray();
        }

        private void ReadLeading(XmlTextReader xmlrd, bool createNew)
        {
            if (xmlrd.NodeType == XmlNodeType.Element && xmlrd.Name == "StudyLeading")
            {
                int _leading_uid = -1;
                ExtObjKey _subject_uid = new ExtObjKey(-1);
                ExtObjKey _studyType_uid = new ExtObjKey(-1);
                ExtObjKey _department_uid = new ExtObjKey(-1);
                int _hourByWeek = 0;
                int _usedHourByWeek = 0;
                string _note = "";
                IList _groups = new ArrayList();
                IList _tutors = new ArrayList();

                while (xmlrd.Read())
                {
                    if (xmlrd.NodeType == XmlNodeType.Element)
                    {
                        #region UID
                        if (xmlrd.Name == "UID")
                        {
                            xmlrd.Read();
                            _leading_uid = int.Parse(xmlrd.Value);
                        }
                        #endregion

                        #region Subject
                        if (xmlrd.Name == "Subject")
                        {
                            _subject_uid.UID = long.Parse(xmlrd.GetAttribute("UID"));

                            if (xmlrd.AttributeCount != 1)
                                UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out _subject_uid.Signature, out _subject_uid.Index);
                        }
                        #endregion

                        #region StudyForm
                        if (xmlrd.Name == "StudyForm")
                        {
                            _studyType_uid.UID = long.Parse(xmlrd.GetAttribute("UID"));

                            if (xmlrd.AttributeCount != 1)
                                UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out _studyType_uid.Signature, out _studyType_uid.Index);
                        }
                        #endregion

                        #region Department
                        if (xmlrd.Name == "Department")
                        {
                            _department_uid.UID = long.Parse(xmlrd.GetAttribute("UID"));

                            if (xmlrd.AttributeCount != 1)
                                UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out _department_uid.Signature, out _department_uid.Index);
                        }
                        #endregion

                        #region Hour
                        if (xmlrd.Name == "Hour")
                        {
                            xmlrd.Read();

                            _hourByWeek = int.Parse(xmlrd.Value);
                        }
                        #endregion

                        #region UsedHour
                        if (xmlrd.Name == "UsedHour")
                        {
                            xmlrd.Read();

                            //_usedHourByWeek = int.Parse( xmlrd.Value);
                        }
                        #endregion

                        #region Note
                        if (xmlrd.Name == "Note")
                        {
                            xmlrd.Read();

                            _note = xmlrd.Value;
                        }
                        #endregion

                        #region Read Группы
                        if (xmlrd.Name == "ForGroups")
                        {
                            if (!xmlrd.IsEmptyElement)
                            {
                                while (xmlrd.Read())
                                {
                                    if (xmlrd.NodeType == XmlNodeType.Element)
                                    {
                                        if (xmlrd.Name == "Group")
                                        {
                                            ExtObjKey groupUID = new ExtObjKey();

                                            try
                                            {
                                                groupUID.UID = long.Parse(xmlrd.GetAttribute("UID"));

                                                if (xmlrd.AttributeCount != 1)
                                                    UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out groupUID.Signature, out groupUID.Index);

                                                if (!_groups.Contains(groupUID))
                                                {
                                                    _groups.Add(groupUID);

                                                    if (!_presentGroups.Contains(groupUID))
                                                        _presentGroups.Add(groupUID);
                                                }
                                            }
                                            catch (Exception e)
                                            {
                                                MessageBox.Show(e.Message);
                                            }
                                        }
                                    }
                                    if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "ForGroups")
                                        break;
                                }
                            }
                        }
                        #endregion

                        #region Преподаватели
                        if (xmlrd.Name == "ForTutors")
                        {
                            if (!xmlrd.IsEmptyElement)
                            {
                                while (xmlrd.Read())
                                {
                                    if (xmlrd.NodeType == XmlNodeType.Element)
                                    {
                                        if (xmlrd.Name == "Tutor")
                                        {
                                            ExtObjKey tutorUID = new ExtObjKey();

                                            tutorUID.UID = long.Parse(xmlrd.GetAttribute("UID"));

                                            if (xmlrd.AttributeCount != 1)
                                                UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out tutorUID.Signature, out tutorUID.Index);

                                            if (!_tutors.Contains(tutorUID))
                                            {
                                                _tutors.Add(tutorUID);


                                            }
                                        }
                                    }
                                    if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "ForTutors")
                                        break;
                                }
                            }
                        }
                        #endregion

                    }
                    if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "StudyLeading")
                    {
                        /* Создаем новую нагрузку */

                        RemoteStudyLeading newRemoteStudyLeading = new RemoteStudyLeading();
                        newRemoteStudyLeading.uid = _leading_uid;
                        newRemoteStudyLeading.subject_key = _subject_uid;
                        newRemoteStudyLeading.subjectType_key = _studyType_uid;
                        newRemoteStudyLeading.department_key = _department_uid;
                        newRemoteStudyLeading.hourByWeek = _hourByWeek;
                        newRemoteStudyLeading.usedHourByWeek = _usedHourByWeek;
                        newRemoteStudyLeading.note = _note;
                        newRemoteStudyLeading.listOfGroups = _groups;
                        newRemoteStudyLeading.listOfTutors = _tutors;

                        if (createNew)
                        {
                            newRemoteStudyLeading.uid = this.GetStudyLeadingUid();
                            newRemoteStudyLeading.usedHourByWeek = 0;
                        }

                        studyLeading.Add(newRemoteStudyLeading.uid, newRemoteStudyLeading);
                        break;
                    }
                }

            }
        }

        public void ImportLeading(string xmlString)
        {
            XmlTextReader xmlReader = new XmlTextReader(new StringReader(xmlString));
            while (xmlReader.Read())
            {
                ReadLeading(xmlReader, true);
            }

            this.isSaved = false;
        }

        private void ReadRestricts(XmlTextReader xmlrd)
        {
            #region Read Flat Restricts
            if (xmlrd.NodeType == XmlNodeType.Element && xmlrd.Name == "FlatRestrict")
            {
                ExtObjKey studyCycle_uid = new ExtObjKey(-1);
                int studyCycle_index = -1;
                ExtObjKey studyDay_uid = new ExtObjKey(-1);
                int studyDay_index = -1;
                ExtObjKey studyHour_uid = new ExtObjKey(-1);
                int studyHour_index = -1;
                string comment = "";


                #region Read lecture hall
                ExtObjKey flat_uid = new ExtObjKey(long.Parse(xmlrd.GetAttribute("UID")));

                if (xmlrd.AttributeCount != 1)
                    UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out flat_uid.Signature, out flat_uid.Index);
                #endregion

                while (xmlrd.Read())
                {
                    if (xmlrd.NodeType == XmlNodeType.Element)
                    {
                        #region Read Comment
                        if (xmlrd.Name == "Comment")
                        {
                            xmlrd.Read();
                            try
                            {
                                comment = xmlrd.Value;
                            }
                            catch
                            {
                                //MessageBox.Show(e.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                //throw new Exception(e.Message);
                                //while( xmlrd.NodeType != XmlNodeType.Text ) xmlrd.Read();
                                //string cycleMnemo = xmlrd.Value;
                                //studyCycle = new StudyCycles(cycleMnemo, cycleMnemo);
                            }
                        }
                        #endregion

                        #region Read Cycle
                        if (xmlrd.Name == "Cycle")
                        {
                            studyCycle_uid.UID = long.Parse(xmlrd.GetAttribute("UID"));
                            if (xmlrd.AttributeCount != 1)
                                UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out studyCycle_uid.Signature, out studyCycle_uid.Index);

                            studyCycle_index = int.Parse(xmlrd.GetAttribute("Index"));
                        }
                        #endregion

                        #region Read Day
                        if (xmlrd.Name == "Day")
                        {
                            studyDay_uid.UID = long.Parse(xmlrd.GetAttribute("UID"));
                            if (xmlrd.AttributeCount != 1)
                                UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out studyDay_uid.Signature, out studyDay_uid.Index);

                            studyDay_index = int.Parse(xmlrd.GetAttribute("Index"));
                        }
                        #endregion

                        #region Read Pair
                        if (xmlrd.Name == "Pair")
                        {
                            studyHour_uid.UID = long.Parse(xmlrd.GetAttribute("UID"));
                            if (xmlrd.AttributeCount != 1)
                                UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out studyHour_uid.Signature, out studyHour_uid.Index);

                            studyHour_index = int.Parse(xmlrd.GetAttribute("Index"));
                        }
                        #endregion
                    }
                    if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "FlatRestrict")
                    {
                        RemoteRestrict newRestrict = new RemoteRestrict();
                        newRestrict.day_key = studyDay_uid;
                        newRestrict.DayIndex = studyDay_index;
                        newRestrict.hour_key = studyHour_uid;
                        newRestrict.HourIndex = studyHour_index;
                        newRestrict.week_key = studyCycle_uid;
                        newRestrict.WeekIndex = studyCycle_index;
                        newRestrict.Text = comment;

                        InsertFlatRestrict(studyCycle_index, studyDay_index, studyHour_index, flat_uid, newRestrict);
                        break;
                    }
                }
            }
            #endregion

            #region Read Group Restricts
            if (xmlrd.NodeType == XmlNodeType.Element && xmlrd.Name == "GroupRestrict")
            {
                ExtObjKey studyCycle_uid = new ExtObjKey(-1);
                int studyCycle_index = -1;
                ExtObjKey studyDay_uid = new ExtObjKey(-1);
                int studyDay_index = -1;
                ExtObjKey studyHour_uid = new ExtObjKey(-1);
                int studyHour_index = -1;
                string comment = "";

                #region Read group
                ExtObjKey group_uid = new ExtObjKey(long.Parse(xmlrd.GetAttribute("UID")));

                if (xmlrd.AttributeCount != 1)
                    UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out group_uid.Signature, out group_uid.Index);

                #endregion

                while (xmlrd.Read())
                {
                    if (xmlrd.NodeType == XmlNodeType.Element)
                    {
                        #region Read Comment
                        if (xmlrd.Name == "Comment")
                        {
                            try
                            {
                                xmlrd.Read();
                                comment = xmlrd.ReadString();
                            }
                            catch
                            {
                                //MessageBox.Show(e.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                //throw new Exception(e.Message);
                            }
                        }
                        #endregion

                        #region Read Cycle
                        if (xmlrd.Name == "Cycle")
                        {
                            studyCycle_uid.UID = long.Parse(xmlrd.GetAttribute("UID"));
                            if (xmlrd.AttributeCount != 1)
                                UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out studyCycle_uid.Signature, out studyCycle_uid.Index);

                            studyCycle_index = int.Parse(xmlrd.GetAttribute("Index"));
                        }
                        #endregion

                        #region Read Day
                        if (xmlrd.Name == "Day")
                        {
                            studyDay_uid.UID = long.Parse(xmlrd.GetAttribute("UID"));
                            if (xmlrd.AttributeCount != 1)
                                UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out studyDay_uid.Signature, out studyDay_uid.Index);

                            studyDay_index = int.Parse(xmlrd.GetAttribute("Index"));
                        }
                        #endregion

                        #region Read Pair
                        if (xmlrd.Name == "Pair")
                        {
                            studyHour_uid.UID = long.Parse(xmlrd.GetAttribute("UID"));
                            if (xmlrd.AttributeCount != 1)
                                UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out studyHour_uid.Signature, out studyHour_uid.Index);

                            studyHour_index = int.Parse(xmlrd.GetAttribute("Index"));
                        }
                        #endregion
                    }
                    if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "GroupRestrict")
                    {
                        RemoteRestrict newRestrict = new RemoteRestrict();
                        newRestrict.day_key = studyDay_uid;
                        newRestrict.DayIndex = studyDay_index;
                        newRestrict.hour_key = studyHour_uid;
                        newRestrict.HourIndex = studyHour_index;
                        newRestrict.week_key = studyCycle_uid;
                        newRestrict.WeekIndex = studyCycle_index;
                        newRestrict.Text = comment;

                        InsertGroupRestrict(studyCycle_index, studyDay_index, studyHour_index, group_uid, newRestrict);
                        break;
                    }
                }
            }
            #endregion

            #region Read Tutor Restricts
            if (xmlrd.NodeType == XmlNodeType.Element && xmlrd.Name == "TutorRestrict")
            {
                ExtObjKey studyCycle_uid = new ExtObjKey(-1);
                int studyCycle_index = -1;
                ExtObjKey studyDay_uid = new ExtObjKey(-1);
                int studyDay_index = -1;
                ExtObjKey studyHour_uid = new ExtObjKey(-1);
                int studyHour_index = -1;
                string comment = "";


                #region Read tutor
                ExtObjKey tutor_uid = new ExtObjKey(long.Parse(xmlrd.GetAttribute("UID")));
                if (xmlrd.AttributeCount != 1)
                    UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out tutor_uid.Signature, out tutor_uid.Index);
                #endregion

                while (xmlrd.Read())
                {
                    if (xmlrd.NodeType == XmlNodeType.Element)
                    {
                        #region Read Comment
                        if (xmlrd.Name == "Comment")
                        {
                            try
                            {
                                xmlrd.Read();
                                comment = xmlrd.Value;
                            }
                            catch
                            {
                                //MessageBox.Show(e.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                //throw new Exception(e.Message);
                            }
                        }
                        #endregion

                        #region Read Cycle
                        if (xmlrd.Name == "Cycle")
                        {
                            studyCycle_uid.UID = long.Parse(xmlrd.GetAttribute("UID"));
                            if (xmlrd.AttributeCount != 1)
                                UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out studyCycle_uid.Signature, out studyCycle_uid.Index);

                            studyCycle_index = int.Parse(xmlrd.GetAttribute("Index"));
                        }
                        #endregion

                        #region Read Day
                        if (xmlrd.Name == "Day")
                        {
                            studyDay_uid.UID = long.Parse(xmlrd.GetAttribute("UID"));
                            if (xmlrd.AttributeCount != 1)
                                UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out studyDay_uid.Signature, out studyDay_uid.Index);

                            studyDay_index = int.Parse(xmlrd.GetAttribute("Index"));
                        }
                        #endregion

                        #region Read Pair
                        if (xmlrd.Name == "Pair")
                        {
                            studyHour_uid.UID = long.Parse(xmlrd.GetAttribute("UID"));
                            if (xmlrd.AttributeCount != 1)
                                UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out studyHour_uid.Signature, out studyHour_uid.Index);

                            studyHour_index = int.Parse(xmlrd.GetAttribute("Index"));
                        }
                        #endregion
                    }
                    if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "TutorRestrict")
                    {
                        RemoteRestrict newRestrict = new RemoteRestrict();
                        newRestrict.day_key = studyDay_uid;
                        newRestrict.DayIndex = studyDay_index;
                        newRestrict.hour_key = studyHour_uid;
                        newRestrict.HourIndex = studyHour_index;
                        newRestrict.week_key = studyCycle_uid;
                        newRestrict.WeekIndex = studyCycle_index;
                        newRestrict.Text = comment;

                        InsertTutorRestrict(studyCycle_index, studyDay_index, studyHour_index, tutor_uid, newRestrict);
                        break;
                    }
                }
            }
            #endregion
        }

        public void ImportRestricts(string xmlString)
        {
            XmlTextReader xmlReader = new XmlTextReader(new StringReader(xmlString));
            while (xmlReader.Read())
            {
                ReadRestricts(xmlReader);
            }
        }
        #endregion

        #region All Checkers
        /* FlatBusyChecker */
        public bool FlatBusyCheckBool(int sheduleKey, ExtObjKey flat_uid)
        {
            if (this.lessonsByFlat.ContainsKey(flat_uid))
                if (lessonsByFlat[flat_uid].ContainsKey(sheduleKey))
                    if (lessonsByFlat[flat_uid][sheduleKey].Count != 0)
                        return true;
            return false;
        }

        public bool FlatBusyCheckBoolRestrict(int sheduleKey, ExtObjKey flat_uid)
        {
            if (restrictByFlat.ContainsKey(flat_uid))
                if (restrictByFlat[flat_uid].ContainsKey(sheduleKey))
                    return true;
                else
                    return false;
            else
                return false;
        }

        public bool FlatBusyCheckBool(int sheduleKey, IList flats)
        {
            bool isNotBusy = true;
            foreach (ExtObjKey flt_uid in flats)
                isNotBusy = isNotBusy && !FlatBusyCheckBool(sheduleKey, flt_uid);
            return !isNotBusy;
        }

        public bool FlatBusyCheckBoolRestrict(int sheduleKey, IList flats)
        {
            bool isNotBusy = true;
            foreach (ExtObjKey flt_uid in flats)
                isNotBusy = isNotBusy && !FlatBusyCheckBoolRestrict(sheduleKey, flt_uid);
            return !isNotBusy;
        }

        public FlatBusyCheckerState FlatBusyCheck(int sheduleKey, ExtObjKey flat_uid)
        {
            if (FlatBusyCheckBool(sheduleKey, flat_uid))
                return FlatBusyCheckerState.isBusy;
            else
                if (FlatBusyCheckBoolRestrict(sheduleKey, flat_uid))
                    return FlatBusyCheckerState.isRestrict;
                else
                    return FlatBusyCheckerState.isNotBusy;
        }

        public FlatBusyCheckerState FlatBusyCheck(int sheduleKey, IList flats)
        {
            if (FlatBusyCheckBool(sheduleKey, flats) == true)
                return FlatBusyCheckerState.isBusy;
            else
                if (FlatBusyCheckBoolRestrict(sheduleKey, flats))
                    return FlatBusyCheckerState.isRestrict;
                else
                    return FlatBusyCheckerState.isNotBusy;
        }

        /*GroupBusyChecker*/
        public bool GroupBusyCheckBool(int sheduleKey, ExtObjKey group_uid)
        {
            if (lessonsByGroup.ContainsKey(group_uid))
                if (lessonsByGroup[group_uid].ContainsKey(sheduleKey))
                    return true;
                else
                    return false;
            else
                return false;
        }

        public bool GroupBusyCheckBoolRestrict(int sheduleKey, ExtObjKey group_uid)
        {
            if (restrictByGroup.ContainsKey(group_uid))
                if (restrictByGroup[group_uid].ContainsKey(sheduleKey))
                    return true;
                else
                    return false;
            else
                return false;
        }

        public bool GroupBusyCheckBool(int sheduleKey, IList groups)
        {
            bool isNotBusy = true;
            foreach (ExtObjKey group_uid in groups)
                isNotBusy = isNotBusy && !GroupBusyCheckBool(sheduleKey, group_uid);
            return !isNotBusy;
        }

        public bool GroupBusyCheckBoolRestrict(int sheduleKey, IList groups)
        {
            bool isNotBusy = true;
            foreach (ExtObjKey group_uid in groups)
                isNotBusy = isNotBusy && !GroupBusyCheckBoolRestrict(sheduleKey, group_uid);
            return !isNotBusy;
        }

        public GroupBusyCheckerState GroupBusyCheck(int sheduleKey, ExtObjKey group_uid)
        {
            if (GroupBusyCheckBool(sheduleKey, group_uid))
                return GroupBusyCheckerState.isBusy;
            else
                if (GroupBusyCheckBoolRestrict(sheduleKey, group_uid))
                    return GroupBusyCheckerState.isRestrict;
                else
                    return GroupBusyCheckerState.isNotBusy;
        }

        public GroupBusyCheckerState GroupBusyCheck(int sheduleKey, IList groups)
        {
            if (GroupBusyCheckBool(sheduleKey, groups) == true)
                return GroupBusyCheckerState.isBusy;
            else
                if (GroupBusyCheckBoolRestrict(sheduleKey, groups) == true)
                    return GroupBusyCheckerState.isRestrict;
                else
                    return GroupBusyCheckerState.isNotBusy;
        }

        /*TutorBusyChecker*/
        public bool TutorBusyCheckBool(int sheduleKey, ExtObjKey tutor_uid)
        {
            if (lessonsByTutor.ContainsKey(tutor_uid))
                if (lessonsByTutor[tutor_uid].ContainsKey(sheduleKey))
                    return true;
                else
                    return false;
            else
                return false;
        }

        public bool TutorBusyCheckBoolRestrict(int sheduleKey, ExtObjKey tutor_uid)
        {
            if (restrictByTutor.ContainsKey(tutor_uid))
                if (restrictByTutor[tutor_uid].ContainsKey(sheduleKey))
                    return true;
                else
                    return false;
            else
                return false;
        }

        public bool TutorBusyCheckBool(int sheduleKey, IList tutors)
        {
            bool isNotBusy = true;
            foreach (ExtObjKey tutor_uid in tutors)
                isNotBusy = isNotBusy && !TutorBusyCheckBool(sheduleKey, tutor_uid);
            return !isNotBusy;
        }

        public bool TutorBusyCheckBoolRestrict(int sheduleKey, IList tutors)
        {
            bool isNotBusy = true;
            foreach (ExtObjKey tutor_uid in tutors)
                isNotBusy = isNotBusy && !TutorBusyCheckBoolRestrict(sheduleKey, tutor_uid);
            return !isNotBusy;
        }

        public TutorBusyCheckerState TutorBusyCheck(int sheduleKey, ExtObjKey tutor_uid)
        {
            if (TutorBusyCheckBool(sheduleKey, tutor_uid))
                return TutorBusyCheckerState.isBusy;
            else
                if (TutorBusyCheckBoolRestrict(sheduleKey, tutor_uid))
                    return TutorBusyCheckerState.isRestrict;
                else
                    return TutorBusyCheckerState.isNotBusy;
        }

        public TutorBusyCheckerState TutorBusyCheck(int sheduleKey, IList tutors)
        {
            if (TutorBusyCheckBool(sheduleKey, tutors) == true)
                return TutorBusyCheckerState.isBusy;
            else
                if (TutorBusyCheckBoolRestrict(sheduleKey, tutors) == true)
                    return TutorBusyCheckerState.isRestrict;
                else
                    return TutorBusyCheckerState.isNotBusy;
        }

        public bool CheckLessonCountByPair(int sheduleKey, ExtObjKey studyForm, ExtObjKey subject, int limit)
        {
            if (!shedule.ContainsKey(sheduleKey))
                return false;
            IList lst = new ArrayList(shedule[sheduleKey].Values);
            int entryCount = 0;
            foreach (RemoteLesson remLes in lst)
                if (remLes.studyType_key == studyForm && remLes.subject_key == subject)
                    entryCount++;
            return (entryCount >= limit);
        }

        public bool CheckDeptLessonCountByPair(int sheduleKey, ExtObjKey[] studyForm, ExtObjKey[] department, int limit)
        {
            if (!shedule.ContainsKey(sheduleKey))
                return false;
            IList lst = new ArrayList(shedule[sheduleKey].Values);
            int entryCount = 0;
            ;

            foreach (RemoteLesson remLes in lst)
                if (Array.IndexOf(studyForm, remLes.studyType_key) != -1 && Array.IndexOf(department, (studyLeading[remLes.leading_uid] as RemoteStudyLeading).department_key) != -1)
                    entryCount++;
            return (entryCount >= limit);
        }

        #endregion

        #region IDisposable Members

        public override object InitializeLifetimeService()
        {
            return null;
        }

        public void Dispose()
        {
            sasm.StopAutoSaveMode();
        }

        #endregion

        #region посилання на інші розклади

        private Dictionary<string, object> referenceList = new Dictionary<string, object>();

        public void AddReference(string name, object shedule)
        {
            if (!referenceList.ContainsKey(name))
                referenceList.Add(name, shedule);
        }

        public void RemoveReference(string name)
        {
            if (referenceList.ContainsKey(name))
                referenceList.Remove(name);
        }

        public string[] GetReferenceNameList()
        {
            string[] refNameList = new string[referenceList.Keys.Count];
            referenceList.Keys.CopyTo(refNameList, 0);
            return refNameList;
        }

        public object[] GelReferenceList()
        {
            string[] refList = new string[referenceList.Values.Count];
            referenceList.Values.CopyTo(refList, 0);
            return refList;
        }

        public bool CkeckBusy(ExtObjKey objKey, int day, int pair, int week, out string message)
        {
            message = string.Empty;
            return false;
        }

        public bool CkeckBusy(List<ExtObjKey> objKeyList, int day, int pair, int week, out string message)
        {
            message = string.Empty;
            return false;
        }

        public bool CkeckBusy(ExtObjKey objKey, DateTime date, int pair, out string message)
        {
            message = string.Empty;
            return false;
        }

        public bool CkeckBusy(List<ExtObjKey> objKeyList, DateTime date, int pair, out string message)
        {
            message = string.Empty;
            return false;
        }

        #endregion
    }

}