﻿using System;
using System.IO;
using System.Xml;
using System.Collections;
using System.Windows.Forms;

using Shturman.Shedule.Server.Interface;
// Using Compression Library
using Zlib;

namespace Shturman.Shedule.Server.Remoting
{
    /// <summary>
    /// Матрица расписания занятий
    /// </summary>	
    [Serializable]
    public class RemoteShedule2 : MarshalByRefObject, IShedule2
    {
        // Занятия по группам
        private Hashtable lessonsByGroup = new Hashtable();
        // Занятия по аудиториям
        private Hashtable lessonsByFlat = new Hashtable();
        // Занятия по преподавателям
        private Hashtable lessonsByTutor = new Hashtable();

        // Ограничения по группам
        private Hashtable restrictByGroup = new Hashtable();
        // Ограничения по аудиториям
        private Hashtable restrictByFlat = new Hashtable();
        // Ограничения по преподавателям
        private Hashtable restrictByTutor = new Hashtable();

        // Нагрузка заданная для данного расписания
        private Hashtable studyLeading = new Hashtable();

        // Расписание занятий
        private Hashtable shedule = new Hashtable();

        // Число недельных циклов расписания
        private int weekCyclesCount = 0;
        // Число учебных дней в недели
        private int weekDaysCount = 0;
        // Число пар в день
        private int dayPairsCount = 0;

        // Следующий код для записей нагрузки
        private int LeadingUidCode = 1;

        // Заголовок расписания, Пример "Расписание 2004/2005 осень"
        private string sheduleTitle;

        // Версия расписания в данной версии этот аттрибут не используется
        private string version = "2.0";

        //Дата начала действия расписания
        private DateTime sheduleBegin;

        //Дата окончания действия расписания
        private DateTime sheduleEnd;

        private string fileName = "";

        private bool isSaved = false;

        public RemoteShedule2(string title, int DaysCount, int PairsCount) :
            this(DaysCount, PairsCount)
        {
            sheduleTitle = title;
        }

        public RemoteShedule2(int DaysCount, int PairsCount)
        {
            weekDaysCount = DaysCount;
            dayPairsCount = PairsCount;
        }

        /// <summary>
        /// Генерирует хэш-код для занятия
        /// </summary>
        public int GetKey(DateTime date, int pair)
        {
            int day = (date - this.sheduleBegin).Days + 1;
            return (day - 1) * dayPairsCount + (pair - 1);
        }


        public int GetStudyLeadingUid()
        {
            return LeadingUidCode++;
        }

        #region Фунцкії роботи з навантаженнями
        public int AddStudyLeading(RemoteStudyLeading2 stLeading)
        {
            stLeading.uid = GetStudyLeadingUid();

            this.studyLeading.Add(stLeading.uid, stLeading);

            this.isSaved = false;

            return stLeading.uid;
        }

        public void DelStudyLeading(RemoteStudyLeading2 stLeading)
        {
            /* Удаление связанных с удаляемой нагрузкой занятий из расписаний */
            IList Days = new ArrayList();
            IList Pairs = new ArrayList();

            // видалення з розкладу занять пов'язанних з данним навантаженням	
            IDictionaryEnumerator idea = shedule.GetEnumerator();
            idea.Reset();
            while (idea.MoveNext())
            {
                IDictionaryEnumerator ideb = (idea.Value as Hashtable).GetEnumerator();
                ideb.Reset();
                while (ideb.MoveNext())
                {
                    RemoteLesson2 lsn = ideb.Value as RemoteLesson2;
                    if (lsn.leading_uid == stLeading.uid)
                    {
                        Days.Add(lsn.date);
                        Pairs.Add(lsn.pair);
                    }
                }
            }

            for (int index = 0; index < Days.Count; index++)
            {
                this.DeleteByGroup((DateTime)Days[index], (int)Pairs[index], stLeading.listOfGroups);
            }

            this.studyLeading.Remove(stLeading.uid);
            this.isSaved = false;
        }

        public RemoteStudyLeading2 GetStudyLeading(int leading_uid)
        {
            return this.studyLeading[leading_uid] as RemoteStudyLeading2;
        }

        public void ChangeStudyLeading(int leading_uid, RemoteStudyLeading2 stLeading)
        {
            if (this.studyLeading.Contains(leading_uid))
                this.studyLeading[leading_uid] = stLeading;
        }
        #endregion

        #region Функції додовання, видалення та редагування розкладу занять
        public bool CanInsertTo(DateTime date, int pair, RemoteLesson2 lesson)
        {
            // Create position index in shedule matrix
            //	int customKey = GetKey(date, pair);

            // Check ==> Group(s) is FREE 
            bool groupIsFree = (this.GroupBusyCheck(date, pair, lesson.groupList) != GroupBusyCheckerState.isBusy);

            // Check ==> Tutor(s) is FREE 
            bool tutorIsFree = (this.TutorBusyCheck(date, pair, lesson.tutorList) != TutorBusyCheckerState.isBusy);

            // Check ==> All is FREE
            return groupIsFree && tutorIsFree;
        }


        public int SimpleInsert(DateTime date, int pair, RemoteLesson2 lesson)
        {
            // Create position index in shedule matrix
            int customKey = GetKey(date, pair);

            // Check ==> Group(s) is FREE 
            bool groupIsFree = (this.GroupBusyCheck(date, pair, lesson.groupList) != GroupBusyCheckerState.isBusy);

            // Check ==> Tutor(s) is FREE 
            bool tutorIsFree = (TutorBusyCheck(date, pair, lesson.tutorList) != TutorBusyCheckerState.isBusy);

            // Check ==> All is FREE
            bool isFree = groupIsFree && tutorIsFree;

            if (!isFree)
            {
                this.GetStudyLeading(lesson.leading_uid).usedHourByWeek -= 2;
                return -1;
            }

            // Add to existing lesson additional flatroom 
            if (!groupIsFree && (shedule[customKey] as Hashtable).ContainsKey(lesson.leading_uid))
            {
                // Insert refference to additional flatroom
                foreach (object obj in lesson.flatList)
                    ((shedule[customKey] as Hashtable)[lesson.leading_uid] as RemoteLesson2).flatList.Add(obj);

                // Insert in shedule by flats refference to lesson
                foreach (ExtObjKey flt_key in lesson.flatList)
                {
                    // if refference list of flats does not exist do create it!
                    if (!lessonsByFlat.Contains(flt_key))
                        lessonsByFlat.Add(flt_key, new Hashtable());

                    // Insert refference to refference list	
                    (lessonsByFlat[flt_key] as Hashtable).Add(customKey, (shedule[customKey] as Hashtable)[lesson.leading_uid] as RemoteLesson2);
                }
                // Set that shedule is changed and need to be saved 
                isSaved = false;
                // Exit
                return customKey;
            }

            // if shedule doesnt contain refference list do create tham
            if (!shedule.Contains(customKey))
                shedule.Add(customKey, new Hashtable());

            // Insert new lesson to a shedule	
            (shedule[customKey] as Hashtable).Add(lesson.leading_uid, lesson);

            // insert refference of lesson to refference list of group
            foreach (ExtObjKey gr_key in lesson.groupList)
            {
                if (!lessonsByGroup.Contains(gr_key))
                    lessonsByGroup.Add(gr_key, new Hashtable());

                (lessonsByGroup[gr_key] as Hashtable).Add(customKey, lesson);
            }

            // insert refference of lesson to refference list of flatroom
            foreach (ExtObjKey flt_key in lesson.flatList)
            {
                if (!lessonsByFlat.Contains(flt_key))
                    lessonsByFlat.Add(flt_key, new Hashtable());

                if (!(lessonsByFlat[flt_key] as Hashtable).Contains(customKey))
                    (lessonsByFlat[flt_key] as Hashtable).Add(customKey, new Hashtable());

                ((lessonsByFlat[flt_key] as Hashtable)[customKey] as Hashtable).Add(lesson.leading_uid, lesson);
            }

            // insert refference of lesson to refference list of tutor
            foreach (ExtObjKey ttr_key in lesson.tutorList)
            {
                if (!lessonsByTutor.Contains(ttr_key))
                    lessonsByTutor.Add(ttr_key, new Hashtable());

                (lessonsByTutor[ttr_key] as Hashtable).Add(customKey, lesson);
            }

            // Set that shedule is changed and need to be saved 
            isSaved = false;

            return customKey;
        }


        public int Insert(DateTime date, int pair, RemoteLesson2 lesson)
        {
            int key = SimpleInsert(date, pair, lesson);

            // Change count used hour by lesson
            this.GetStudyLeading(lesson.leading_uid).usedHourByWeek += 2;

            // Exit, by return lesson position index in shedule matrix
            return key;
        }


        public void DeleteByGroup(DateTime date, int pair, IList groupList)
        {
            int lessonKey = this.GetKey(date, pair);

            foreach (ExtObjKey gr_uid in groupList)
            {
                if (!(lessonsByGroup[gr_uid] as Hashtable).Contains(lessonKey)) continue;
                RemoteLesson2 lesson = ((lessonsByGroup[gr_uid] as Hashtable)[lessonKey] as RemoteLesson2);
                if ((shedule[lessonKey] as Hashtable).Contains(lesson.leading_uid))
                    (shedule[lessonKey] as Hashtable).Remove(lesson.leading_uid);

                foreach (ExtObjKey tr_key in lesson.tutorList)
                    if ((lessonsByTutor[tr_key] as Hashtable).Contains(lessonKey))
                        (lessonsByTutor[tr_key] as Hashtable).Remove(lessonKey);

                foreach (ExtObjKey lh_key in lesson.flatList)
                    if ((lessonsByFlat[lh_key] as Hashtable).Contains(lessonKey))
                        if (((lessonsByFlat[lh_key] as Hashtable)[lessonKey] as Hashtable).Contains(lesson.leading_uid))
                            ((lessonsByFlat[lh_key] as Hashtable)[lessonKey] as Hashtable).Remove(lesson.leading_uid);

                foreach (ExtObjKey grp_key in lesson.groupList)
                    if ((lessonsByGroup[grp_key] as Hashtable).Contains(lessonKey))
                        (lessonsByGroup[grp_key] as Hashtable).Remove(lessonKey);


                RemoteStudyLeading2 remoteleading = GetStudyLeading(lesson.leading_uid);
                if (remoteleading.usedHourByWeek != 0)
                    remoteleading.usedHourByWeek -= 2;

                // Set that shedule is changed and need to be saved 
                isSaved = false;

            }
        }

        public void DeleteByFlat(DateTime date, int pair, IList flatList)
        {
            // Create position index in shedule matrix
            int lessonKey = this.GetKey(date, pair);

            foreach (ExtObjKey lh_uid in flatList)
            {
                if (!(lessonsByFlat[lh_uid] as Hashtable).Contains(lessonKey)) continue;

                foreach (RemoteLesson2 lesson in new ArrayList(((lessonsByFlat[lh_uid] as Hashtable)[lessonKey] as Hashtable).Values))
                {

                    // Проверка на удаление дополнительной аудитории
                    if (lesson.flatList.Count > 1)
                    {
                        lesson.flatList.Remove(lh_uid);
                        // 
                        if ((lessonsByFlat[lh_uid] as Hashtable).Contains(lessonKey))
                        {
                            (lessonsByFlat[lh_uid] as Hashtable).Remove(lessonKey);
                            lesson.flatList.Remove(lh_uid);
                            continue;
                        }
                    }

                    if ((shedule[lessonKey] as Hashtable).Contains(lesson.leading_uid))
                        (shedule[lessonKey] as Hashtable).Remove(lesson.leading_uid);

                    foreach (ExtObjKey tr_key in lesson.tutorList)
                        if ((lessonsByTutor[tr_key] as Hashtable).Contains(lessonKey))
                            (lessonsByTutor[tr_key] as Hashtable).Remove(lessonKey);

                    foreach (ExtObjKey gr_key in lesson.groupList)
                        if ((lessonsByGroup[gr_key] as Hashtable).Contains(lessonKey))
                            (lessonsByGroup[gr_key] as Hashtable).Remove(lessonKey);

                    if ((lessonsByFlat[lh_uid] as Hashtable).Contains(lessonKey))
                        (lessonsByFlat[lh_uid] as Hashtable).Remove(lessonKey);

                    GetStudyLeading(lesson.leading_uid).usedHourByWeek -= 2;
                }
            }
        }


        public void MoveLesson(RemoteLesson2 lesson, DateTime date, int pair)
        {
            // Перевырка на можливість перенести заняття у інше місце
            if (CanInsertTo(date, pair, lesson))
            {
                this.DeleteByGroup(lesson.date, lesson.pair, lesson.groupList);
                lesson.date = date;
                lesson.pair = pair;
                //!!!lesson.hour_index = nweekPair;
                this.Insert(date, pair, lesson);
            }
        }


        public void DelLessonsByLeading(int leadingUid)
        {
            /* Удаление связанных с удаляемой нагрузкой занятий из расписаний */
            IList Days = new ArrayList();
            IList Pairs = new ArrayList();

            // видалення з розкладу занять пов'язанних з данним навантаженням	
            IDictionaryEnumerator idea = shedule.GetEnumerator();
            idea.Reset();
            while (idea.MoveNext())
            {
                IDictionaryEnumerator ideb = (idea.Value as Hashtable).GetEnumerator();
                ideb.Reset();
                while (ideb.MoveNext())
                {
                    RemoteLesson2 lsn = ideb.Value as RemoteLesson2;
                    if (lsn.leading_uid == leadingUid)
                    {
                        Days.Add(lsn.date);
                        Pairs.Add(lsn.pair);
                    }
                }
            }
            if (studyLeading.Contains(leadingUid))
            {
                IList groups = (studyLeading[leadingUid] as RemoteStudyLeading2).listOfGroups;

                for (int index = 0; index < Days.Count; index++)
                    this.DeleteByGroup((DateTime)Days[index], (int)Pairs[index], groups);
            }

        }


        public void ChangeLessonFlats(RemoteLesson2[] lessonList, ExtObjKey[] flatList)
        {
            foreach (RemoteLesson2 lsn in lessonList)
            {
                // Create position index in shedule matrix
                int customKey = GetKey(lsn.date, lsn.pair);

                RemoteLesson2 existLesson = null;
                if ((shedule[customKey] as Hashtable).Contains(lsn.leading_uid))
                    existLesson = (shedule[customKey] as Hashtable)[lsn.leading_uid] as RemoteLesson2;

                if (existLesson == null)
                    continue;

                // Вивілняю поки що зайняті аудиторії
                foreach (ExtObjKey lh_key in existLesson.flatList)
                {
                    if ((lessonsByFlat[lh_key] as Hashtable).Contains(customKey))
                    {
                        if (((lessonsByFlat[lh_key] as Hashtable)[customKey] as Hashtable).Contains(existLesson.leading_uid))
                        {
                            ((lessonsByFlat[lh_key] as Hashtable)[customKey] as Hashtable).Remove(existLesson.leading_uid);
                        }
                        if (((lessonsByFlat[lh_key] as Hashtable)[customKey] as Hashtable).Count == 0)
                            (lessonsByFlat[lh_key] as Hashtable).Remove(customKey);
                    }
                }
                existLesson.flatList.Clear();

                // Займаю нові аудиторії
                foreach (ExtObjKey lh_key in flatList)
                {
                    if (!lessonsByFlat.Contains(lh_key))
                        lessonsByFlat.Add(lh_key, new Hashtable());

                    if (!(lessonsByFlat[lh_key] as Hashtable).Contains(customKey))
                        (lessonsByFlat[lh_key] as Hashtable).Add(customKey, new Hashtable());

                    ((lessonsByFlat[lh_key] as Hashtable)[customKey] as Hashtable).Add(existLesson.leading_uid, existLesson);

                    existLesson.flatList.Add(lh_key);
                }
            }
        }
        #endregion

        #region Додавання та видалення обмежень викладачів, груп та аудитторій
        public int InsertGroupRestrict(DateTime date, int pair, ExtObjKey group_uid, RemoteRestrict2 restrict)
        {
            this.SetIsSaved(false);
            // Create position index in shedule matrix
            int customKey = GetKey(date, pair);

            if (!this.restrictByGroup.ContainsKey(group_uid))
                this.restrictByGroup.Add(group_uid, new Hashtable());

            if (this.restrictByGroup.ContainsKey(group_uid))
                if (!(this.restrictByGroup[group_uid] as Hashtable).ContainsKey(customKey))
                    (this.restrictByGroup[group_uid] as Hashtable).Add(customKey, restrict);

            return customKey;
        }

        public int InsertTutorRestrict(DateTime date, int pair, ExtObjKey tutor_uid, RemoteRestrict2 restrict)
        {
            this.SetIsSaved(false);
            // Create position index in shedule matrix
            int customKey = GetKey(date, pair);

            if (!this.restrictByTutor.ContainsKey(tutor_uid))
                this.restrictByTutor.Add(tutor_uid, new Hashtable());

            if (this.restrictByTutor.ContainsKey(tutor_uid))
                if (!(this.restrictByTutor[tutor_uid] as Hashtable).ContainsKey(customKey))
                    (this.restrictByTutor[tutor_uid] as Hashtable).Add(customKey, restrict);

            return customKey;
        }

        public int InsertFlatRestrict(DateTime date, int pair, ExtObjKey flat_uid, RemoteRestrict2 restrict)
        {
            this.SetIsSaved(false);
            // Create position index in shedule matrix
            int customKey = GetKey(date, pair);

            if (!this.restrictByFlat.ContainsKey(flat_uid))
                this.restrictByFlat.Add(flat_uid, new Hashtable());

            if (this.restrictByFlat.ContainsKey(flat_uid))
                if (!(this.restrictByFlat[flat_uid] as Hashtable).ContainsKey(customKey))
                    (this.restrictByFlat[flat_uid] as Hashtable).Add(customKey, restrict);

            return customKey;
        }

        public void DeleteGroupRestrict(DateTime date, int pair, ExtObjKey group_uid)
        {
            this.SetIsSaved(false);
            // Create position index in shedule matrix
            int customKey = GetKey(date, pair);

            if (this.restrictByGroup.ContainsKey(group_uid))
                if ((this.restrictByGroup[group_uid] as Hashtable).ContainsKey(customKey))
                    (this.restrictByGroup[group_uid] as Hashtable).Remove(customKey);
        }

        public void DeleteTutorRestrict(DateTime date, int pair, ExtObjKey tutor_uid)
        {
            this.SetIsSaved(false);
            // Create position index in shedule matrix
            int customKey = GetKey(date, pair);

            if (this.restrictByTutor.ContainsKey(tutor_uid))
                if ((this.restrictByTutor[tutor_uid] as Hashtable).ContainsKey(customKey))
                    (this.restrictByTutor[tutor_uid] as Hashtable).Remove(customKey);
        }

        public void DeleteFlatRestrict(DateTime date, int pair, ExtObjKey flat_uid)
        {
            this.SetIsSaved(false);
            // Create position index in shedule matrix
            int customKey = GetKey(date, pair);

            if (this.restrictByFlat.ContainsKey(flat_uid))
                if ((this.restrictByFlat[flat_uid] as Hashtable).ContainsKey(customKey))
                    (this.restrictByFlat[flat_uid] as Hashtable).Remove(customKey);
        }

        public IList GetTutorRestricts(ExtObjKey tutor_uid)
        {
            if (this.restrictByTutor.Contains(tutor_uid))
                return new ArrayList((this.restrictByTutor[tutor_uid] as Hashtable).Values);
            else
                return new ArrayList();
        }

        public bool IsTutorHasRestricts(ExtObjKey tutor_uid, DateTime date, int pair)
        {
            int SheduleKey = GetKey(date, pair);
            if (this.restrictByTutor.Contains(tutor_uid))
                if ((this.restrictByTutor[tutor_uid] as Hashtable).Contains(SheduleKey))
                    return true;
            return false;
        }

        public string GetTutorRestrictsText(ExtObjKey tutor_uid, DateTime date, int pair)
        {
            int SheduleKey = GetKey(date, pair);
            if (IsTutorHasRestricts(tutor_uid, date, pair))
                return ((this.restrictByTutor[tutor_uid] as Hashtable)[SheduleKey] as RemoteRestrict2).Text;
            else
                return "";
        }

        public IList GetGroupRestricts(ExtObjKey group_uid)
        {
            if (this.restrictByGroup.Contains(group_uid))
                return new ArrayList((this.restrictByGroup[group_uid] as Hashtable).Values);
            else
                return new ArrayList();
        }

        public bool IsGroupHasRestricts(ExtObjKey group_uid, DateTime date, int pair)
        {
            int SheduleKey = GetKey(date, pair);
            if (this.restrictByGroup.Contains(group_uid))
                if ((this.restrictByGroup[group_uid] as Hashtable).Contains(SheduleKey))
                    return true;
            return false;
        }

        public string GetGroupRestrictsText(ExtObjKey group_uid, DateTime date, int pair)
        {
            int SheduleKey = GetKey(date, pair);
            if (IsGroupHasRestricts(group_uid, date, pair))
                return ((this.restrictByGroup[group_uid] as Hashtable)[SheduleKey] as RemoteRestrict2).Text;
            else
                return "";
        }

        public IList GetFlatRestricts(ExtObjKey flat_uid)
        {
            if (this.restrictByFlat.Contains(flat_uid))
                return new ArrayList((this.restrictByFlat[flat_uid] as Hashtable).Values);
            else
                return new ArrayList();
        }

        public bool IsFlatHasRestricts(ExtObjKey flat_uid, DateTime date, int pair)
        {
            int SheduleKey = GetKey(date, pair);
            if (this.restrictByFlat.Contains(flat_uid))
                if ((this.restrictByFlat[flat_uid] as Hashtable).Contains(SheduleKey))
                    return true;
            return false;
        }

        public string GetFlatRestrictsText(ExtObjKey flat_uid, DateTime date, int pair)
        {
            int SheduleKey = GetKey(date, pair);
            if (IsFlatHasRestricts(flat_uid, date, pair))
                return ((this.restrictByFlat[flat_uid] as Hashtable)[SheduleKey] as RemoteRestrict2).Text;
            else
                return "";
        }
        #endregion

        #region Функції перевірки зайнятості груп, викладачів, та аудиторій

        public bool TutorHasLesson(ExtObjKey TutorUID, DateTime date, int pair)
        {
            int SheduleKey = GetKey(date, pair);
            if (this.lessonsByTutor.Contains(TutorUID))
                if ((lessonsByTutor[TutorUID] as Hashtable).Contains(SheduleKey))
                    return true;
            return false;
        }

        public bool GroupHasLesson(ExtObjKey GroupUID, DateTime date, int pair)
        {
            int SheduleKey = GetKey(date, pair);
            if (lessonsByGroup.Contains(GroupUID))
                if ((lessonsByGroup[GroupUID] as Hashtable).Contains(SheduleKey))
                    return true;
            return false;
        }

        public bool FlatHasLesson(ExtObjKey FlatUID, DateTime date, int pair)
        {
            int SheduleKey = GetKey(date, pair);
            if (lessonsByFlat.Contains(FlatUID))
                if ((lessonsByFlat[FlatUID] as Hashtable).Contains(SheduleKey))
                    if (((lessonsByFlat[FlatUID] as Hashtable)[SheduleKey] as Hashtable).Count != 0)
                        return true;
            return false;
        }

        /* Занятия */

        /// <summary>
        /// Возвращает список ключей интервалов времени в которых у 
        /// преподавателя есть занятия
        /// </summary>
        /// <param name="TutorUID">Код преподавателя</param>
        /// <returns>Список ключей</returns>
        public int[] GetKeyTutorLessons(ExtObjKey TutorUID)
        {
            int[] keys = { };
            if (this.lessonsByTutor.Contains(TutorUID))
            {
                keys = new int[(lessonsByTutor[TutorUID] as Hashtable).Keys.Count];
                (lessonsByTutor[TutorUID] as Hashtable).Keys.CopyTo(keys, 0);
            }

            return keys;
        }

        /// <summary>
        /// Функция для определения ключей интервалов времени в которых
        /// группа занята на занятиях
        /// </summary>
        /// <param name="GroupUID">Код группы</param>
        /// <returns>Список ключей времени(День, Пара, Неделя)</returns>
        public int[] GetKeyGroupLessons(ExtObjKey GroupUID)
        {
            int[] keys = { };
            if (this.lessonsByGroup.Contains(GroupUID))
            {
                keys = new int[(lessonsByGroup[GroupUID] as Hashtable).Keys.Count];
                (lessonsByGroup[GroupUID] as Hashtable).Keys.CopyTo(keys, 0);
            }
            return keys;
        }

        /// <summary>
        /// Функция для определения ключей интервалов времени в которых
        /// аудитория занята проведением занятий
        /// </summary>
        /// <param name="FlatUID">Код аудитории</param>
        /// <returns>Список ключей времени(День, Пара, Неделя)</returns>
        public int[] GetKeyFlatLessons(ExtObjKey FlatUID)
        {
            int[] keys = { };
            if (this.lessonsByFlat.Contains(FlatUID))
            {
                keys = new int[(lessonsByFlat[FlatUID] as Hashtable).Keys.Count];
                (lessonsByFlat[FlatUID] as Hashtable).Keys.CopyTo(keys, 0);
            }
            return keys;
        }

        /* Ограничения */

        /// <summary>
        /// Возвращает список ключей интервалов времени в которых у 
        /// преподавателя имеет ограничения
        /// </summary>
        /// <param name="TutorUID">Код преподавателя</param>
        /// <returns>Список ключей</returns>
        public int[] GetKeyTutorRestricts(ExtObjKey TutorUID)
        {
            int[] keys = { };
            if (this.restrictByTutor.Contains(TutorUID))
            {
                keys = new int[(restrictByTutor[TutorUID] as Hashtable).Keys.Count];
                (restrictByTutor[TutorUID] as Hashtable).Keys.CopyTo(keys, 0);
            }

            return keys;
        }

        /// <summary>
        /// Функция для определения ключей интервалов времени в которых
        /// группа имеет ограничения
        /// </summary>
        /// <param name="GroupUID">Код группы</param>
        /// <returns>Список ключей времени(День, Пара, Неделя)</returns>
        public int[] GetKeyGroupRestricts(ExtObjKey GroupUID)
        {
            int[] keys = { };
            if (this.restrictByGroup.Contains(GroupUID))
            {
                keys = new int[(restrictByGroup[GroupUID] as Hashtable).Keys.Count];
                (restrictByGroup[GroupUID] as Hashtable).Keys.CopyTo(keys, 0);
            }
            return keys;
        }

        /// <summary>
        /// Функция для определения ключей интервалов времени в которых
        /// аудитория имеет ограничения
        /// </summary>
        /// <param name="FlatUID">Код аудитории</param>
        /// <returns>Список ключей времени(День, Пара, Неделя)</returns>
        public int[] GetKeyFlatRestricts(ExtObjKey FlatUID)
        {
            int[] keys = { };
            if (this.restrictByFlat.Contains(FlatUID))
            {
                keys = new int[(restrictByFlat[FlatUID] as Hashtable).Keys.Count];
                (restrictByFlat[FlatUID] as Hashtable).Keys.CopyTo(keys, 0);
            }
            return keys;
        }

        /* Занятость с учетом расписания и ограничений */

        /// <summary>
        /// Возвращает список ключей интервалов времени в которых у 
        /// преподавателя имеет ограничения
        /// </summary>
        /// <param name="TutorUID">Код преподавателя</param>
        /// <returns>Список ключей</returns>
        public int[] GetKeyTutorBusy(ExtObjKey TutorUID)
        {
            int[] keys = { };
            int[] keysA = { };
            int[] keysB = { };

            if (this.lessonsByTutor.Contains(TutorUID))
            {
                keysA = new int[(lessonsByTutor[TutorUID] as Hashtable).Keys.Count];
                (restrictByTutor[TutorUID] as Hashtable).Keys.CopyTo(keys, 0);
            }

            if (this.restrictByTutor.Contains(TutorUID))
            {
                keysB = new int[(restrictByTutor[TutorUID] as Hashtable).Keys.Count];
                (restrictByTutor[TutorUID] as Hashtable).Keys.CopyTo(keys, 0);
            }

            if (keysA.Length > 0 || keysB.Length > 0)
            {
                keys = new int[keysA.Length + keysB.Length];
                keysA.CopyTo(keys, 0);
                keysB.CopyTo(keys, keysA.Length);
            }

            return keys;
        }

        /// <summary>
        /// Функция для определения ключей интервалов времени в которых
        /// группа имеет ограничения
        /// </summary>
        /// <param name="GroupUID">Код группы</param>
        /// <returns>Список ключей времени(День, Пара, Неделя)</returns>
        public int[] GetKeyGroupBusy(ExtObjKey GroupUID)
        {
            int[] keys = { };
            int[] keysA = { };
            int[] keysB = { };

            if (this.lessonsByGroup.Contains(GroupUID))
            {
                keysA = new int[(lessonsByGroup[GroupUID] as Hashtable).Keys.Count];
                (restrictByGroup[GroupUID] as Hashtable).Keys.CopyTo(keys, 0);
            }

            if (this.restrictByGroup.Contains(GroupUID))
            {
                keysB = new int[(restrictByGroup[GroupUID] as Hashtable).Keys.Count];
                (restrictByGroup[GroupUID] as Hashtable).Keys.CopyTo(keys, 0);
            }

            if (keysA.Length > 0 || keysB.Length > 0)
            {
                keys = new int[keysA.Length + keysB.Length];
                keysA.CopyTo(keys, 0);
                keysB.CopyTo(keys, keysA.Length);
            }

            return keys;
        }

        /// <summary>
        /// Функция для определения ключей интервалов времени в которых
        /// аудитория имеет ограничения
        /// </summary>
        /// <param name="FlatUID">Код аудитории</param>
        /// <returns>Список ключей времени(День, Пара, Неделя)</returns>
        public int[] GetKeyFlatBusy(ExtObjKey FlatUID)
        {
            int[] keys = { };
            int[] keysA = { };
            int[] keysB = { };

            if (this.lessonsByFlat.Contains(FlatUID))
            {
                keysA = new int[(lessonsByFlat[FlatUID] as Hashtable).Keys.Count];
                (lessonsByFlat[FlatUID] as Hashtable).Keys.CopyTo(keys, 0);
            }


            if (this.restrictByFlat.Contains(FlatUID))
            {
                keysB = new int[(restrictByFlat[FlatUID] as Hashtable).Keys.Count];
                (restrictByFlat[FlatUID] as Hashtable).Keys.CopyTo(keys, 0);
            }

            if (keysA.Length > 0 || keysB.Length > 0)
            {
                keys = new int[keysA.Length + keysB.Length];
                keysA.CopyTo(keys, 0);
                keysB.CopyTo(keys, keysA.Length);
            }

            return keys;
        }
        #endregion

        public IList GetAllStudyLeading()
        {
            return new ArrayList(this.studyLeading.Values);
        }

        public IList GetStudyLeadingByGroups(ExtObjKey[] groupUidList)
        {
            IList filtered = new ArrayList();
            foreach (RemoteStudyLeading2 rsl in new ArrayList(this.studyLeading.Values))
            {
                foreach (ExtObjKey group_key in groupUidList)
                    if (rsl.listOfGroups.Contains(group_key))
                    {
                        filtered.Add(rsl);
                        break;
                    }
            }
            return filtered;
        }

        public IList GetFlatLesson(ExtObjKey flat_uid, DateTime date, int pair)
        {
            int SheduleKey = GetKey(date, pair);
            if (FlatHasLesson(flat_uid, date, pair))
                return new ArrayList(((this.lessonsByFlat[flat_uid] as Hashtable)[SheduleKey] as Hashtable).Values);
            else
                return null;
        }

        public IList GetLessons(DateTime date, int pair)
        {
            int SheduleKey = GetKey(date, pair);

            if (shedule.Contains(SheduleKey))
                return new ArrayList((shedule[SheduleKey] as Hashtable).Values);
            else
                return new ArrayList();
        }

        public IList GetFlatLessons(ExtObjKey flat_uid)
        {
            if (this.lessonsByFlat.Contains(flat_uid))
                return new ArrayList((this.lessonsByFlat[flat_uid] as Hashtable).Values);
            else
                return new ArrayList();
        }

        public RemoteLesson2 GetGroupLesson(ExtObjKey group_uid, DateTime date, int pair)
        {
            int SheduleKey = GetKey(date, pair);
            if (GroupHasLesson(group_uid, date, pair))
            {
                RemoteLesson2 remLess = (this.lessonsByGroup[group_uid] as Hashtable)[SheduleKey] as RemoteLesson2;
                return remLess;
            }
            else
                return null;
        }

        public IList GetGroupLessons(ExtObjKey group_uid)
        {
            if (this.lessonsByGroup.Contains(group_uid))
                return new ArrayList((this.lessonsByGroup[group_uid] as Hashtable).Values);
            else
                return new ArrayList();
        }

        public RemoteLesson2 GetTutorLesson(ExtObjKey tutor_uid, DateTime date, int pair)
        {
            int SheduleKey = GetKey(date, pair);
            if (TutorHasLesson(tutor_uid, date, pair))
                return (this.lessonsByTutor[tutor_uid] as Hashtable)[SheduleKey] as RemoteLesson2;
            else
                return null;
        }

        public IList GetTutorLessons(ExtObjKey tutor_uid)
        {
            if (this.lessonsByTutor.Contains(tutor_uid))
                return new ArrayList((this.lessonsByTutor[tutor_uid] as Hashtable).Values);
            else
                return new ArrayList();
        }



        public IList GetLessons(ExtObjKey subject_uid, ExtObjKey subjectType_uid)
        {
            IList lessons = new ArrayList();
            foreach (Hashtable ht in shedule.Values)
                foreach (RemoteLesson2 rl in ht.Values)
                    if (rl.subject_key.Equals(subject_uid) && rl.studyType_key.Equals(subjectType_uid))
                        lessons.Add(rl);
            return lessons;
        }

        #region Additional Info Methods
        //Признак того, сохранено или нет расписание
        public bool GetIsSaved()
        {
            return isSaved;
        }

        public void SetIsSaved(bool savedFlag)
        {
            isSaved = savedFlag;
        }

        // заголовок расписания
        public string GetSheduleTitle()
        {
            return sheduleTitle;
        }

        public void SetSheduleTitle(string title)
        {
            sheduleTitle = title;
        }

        //Имя файла
        public string GetFileName()
        {
            return fileName;
        }
        public void SetFileName(string fileName)
        {
            this.fileName = fileName;
        }

        //Дата начала действия расписания
        public DateTime GetSheduleBeginDate()
        {
            return sheduleBegin;
        }
        public void SetSheduleBeginDate(DateTime beginDate)
        {
            sheduleBegin = beginDate;
        }

        //Дата окончания действия расписания
        public DateTime GetSheduleEndDate()
        {
            return sheduleEnd;
        }

        public void SetSheduleEndDate(DateTime sheduleEndDate)
        {
            sheduleEnd = sheduleEndDate;
        }

        // Число недельных циклов расписания
        public int GetWeeksCount()
        {
            return weekCyclesCount;
        }
        // Число учебных дней в недели
        public int GetDaysPerWeekCount()
        {
            return weekDaysCount;
        }
        // Число учебных пар в день
        public int GetPairsPerDayCount()
        {
            return dayPairsCount;
        }

        public object[] GetSheduleInfo()
        {
            object[] infoArray = new object[11];
            infoArray[0] = this.version;
            infoArray[1] = this.sheduleTitle;
            infoArray[2] = this.fileName;
            infoArray[3] = this.sheduleBegin;
            infoArray[4] = this.sheduleEnd;
            infoArray[5] = this.weekCyclesCount;
            infoArray[6] = this.weekDaysCount;
            infoArray[7] = this.dayPairsCount;
            infoArray[8] = this.studyLeading.Count;
            infoArray[9] = this.restrictByFlat.Count + this.restrictByGroup.Count + this.restrictByTutor.Count;

            // Підраховую кількість занять
            //int lsncnt = 0;
            //foreach (int key in this.shedule.Keys)
            //{
            // Dictionary<int, RemoteLesson> ldngTbl = this.shedule[key];
            // if (ldngTbl != null)
            // lsncnt += ldngTbl.Count;
            //}
            //infoArray[10] = lsncnt;

            return infoArray;
        }
        #endregion

        #region Open and Save
        public bool SaveInfo(string dirName)
        {
            #region Запис файлу Info.xml
            string infoFileName = Path.Combine(dirName, "Info.xml");
            XmlTextWriter infoWriter = new XmlTextWriter(infoFileName, System.Text.Encoding.UTF8);
            try
            {
                infoWriter.WriteStartDocument(true);
                infoWriter.WriteStartElement("SheduleInfo");
                infoWriter.WriteAttributeString("Version", "2.0");
                infoWriter.WriteAttributeString("Title", this.sheduleTitle);
                infoWriter.WriteAttributeString("BeginDate", this.sheduleBegin.Date.ToShortDateString());
                infoWriter.WriteAttributeString("EndDate", this.sheduleEnd.Date.ToShortDateString());
                infoWriter.WriteEndElement();
                infoWriter.WriteEndDocument();
                infoWriter.Flush();
                infoWriter.Close();
            }
            catch
            {
                return false;
            }
            return true;
            #endregion
        }

        public bool SaveSupplement(string dirName)
        {
            #region Запис файлу Supplement.xml
            string supplementFileName = Path.Combine(dirName, "Supplement.xml");
            XmlTextWriter supplementWriter = new XmlTextWriter(supplementFileName, System.Text.Encoding.UTF8);
            try
            {
                supplementWriter.WriteStartDocument(true);
                supplementWriter.WriteStartElement("Supplements");

                foreach (RemoteStudyLeading2 sl in new ArrayList(this.studyLeading.Values))
                {
                    if (sl.uid == 0)
                        sl.uid = this.LeadingUidCode++;
                }

                supplementWriter.WriteAttributeString("GeneratorValue", this.LeadingUidCode.ToString());

                IEnumerator leadingenum = this.studyLeading.Values.GetEnumerator();
                while (leadingenum.MoveNext())
                {
                    if (leadingenum.Current is RemoteStudyLeading2)
                    {
                        RemoteStudyLeading2 stload = leadingenum.Current as RemoteStudyLeading2;

                        supplementWriter.WriteStartElement("Supplement");

                        //Код
                        supplementWriter.WriteStartElement("UID");
                        supplementWriter.WriteString(stload.uid.ToString());
                        supplementWriter.WriteEndElement();

                        //Предмет
                        supplementWriter.WriteStartElement("Subject");
                        supplementWriter.WriteAttributeString("UID", stload.subject_key.UID.ToString());
                        supplementWriter.WriteAttributeString("UUID", UUIDConverter.UUIDToString(stload.subject_key.Signature, stload.subject_key.Index));
                        supplementWriter.WriteEndElement();

                        //Тип занятия
                        supplementWriter.WriteStartElement("StudyForm");
                        supplementWriter.WriteAttributeString("UID", stload.subjectType_key.UID.ToString());
                        supplementWriter.WriteAttributeString("UUID", UUIDConverter.UUIDToString(stload.subjectType_key.Signature, stload.subjectType_key.Index));
                        supplementWriter.WriteEndElement();

                        //Кафедра
                        supplementWriter.WriteStartElement("Department");
                        supplementWriter.WriteAttributeString("UID", stload.department_key.UID.ToString());
                        supplementWriter.WriteAttributeString("UUID", UUIDConverter.UUIDToString(stload.department_key.Signature, stload.department_key.Index));
                        supplementWriter.WriteEndElement();

                        //Запланированно часов
                        supplementWriter.WriteStartElement("Hour");
                        supplementWriter.WriteString(stload.hourByWeek.ToString());
                        supplementWriter.WriteEndElement();

                        //Использованно часов
                        supplementWriter.WriteStartElement("UsedHour");
                        supplementWriter.WriteString(stload.usedHourByWeek.ToString());
                        supplementWriter.WriteEndElement();

                        //Примечания
                        supplementWriter.WriteStartElement("Note");
                        if (stload.note != string.Empty)
                            supplementWriter.WriteString(stload.note);
                        else
                            supplementWriter.WriteString("");
                        supplementWriter.WriteEndElement();

                        //Группы
                        supplementWriter.WriteStartElement("ForGroups");
                        foreach (ExtObjKey gr_key in stload.listOfGroups)
                        {
                            if (gr_key.UID != -1 || gr_key.Index != -1)
                            {
                                supplementWriter.WriteStartElement("Group");
                                supplementWriter.WriteAttributeString("UID", gr_key.UID.ToString());
                                supplementWriter.WriteAttributeString("UUID", UUIDConverter.UUIDToString(gr_key.Signature, gr_key.Index));
                                supplementWriter.WriteEndElement();
                            }
                        }
                        supplementWriter.WriteEndElement();

                        //Преподаватели
                        supplementWriter.WriteStartElement("ForTutors");
                        foreach (ExtObjKey tr_key in stload.listOfTutors)
                        {
                            if (tr_key.UID != -1 || tr_key.Index != -1)
                            {
                                supplementWriter.WriteStartElement("Tutor");
                                supplementWriter.WriteAttributeString("UID", tr_key.UID.ToString());
                                supplementWriter.WriteAttributeString("UUID", UUIDConverter.UUIDToString(tr_key.Signature, tr_key.Index));
                                supplementWriter.WriteEndElement();
                            }
                        }
                        supplementWriter.WriteEndElement();

                        supplementWriter.WriteEndElement();
                    }

                }
                supplementWriter.WriteEndElement();
                supplementWriter.WriteEndDocument();
                supplementWriter.Flush();
                supplementWriter.Close();
            }
            catch
            {
                return false;
            }
            return true;
            #endregion
        }

        public bool SaveRestrict(string dirName)
        {
            #region Запис файлу Restrict.xml
            string restrictFileName = Path.Combine(dirName, "Restrict.xml");
            XmlTextWriter restrictWriter = new XmlTextWriter(restrictFileName, System.Text.Encoding.UTF8);
            try
            {
                restrictWriter.WriteStartDocument(true);
                restrictWriter.WriteStartElement("Restricts");

                #region write flat restricts
                IEnumerator enumeratorFlat = restrictByFlat.Values.GetEnumerator();
                IEnumerator keyEnumeratorFlat = restrictByFlat.Keys.GetEnumerator();
                while (enumeratorFlat.MoveNext() && keyEnumeratorFlat.MoveNext())
                {
                    restrictWriter.WriteStartElement("FlatRestricts");


                    IEnumerator restrictEnumerator = (enumeratorFlat.Current as Hashtable).Values.GetEnumerator();
                    while (restrictEnumerator.MoveNext())
                    {
                        restrictWriter.WriteStartElement("FlatRestrict");

                        restrictWriter.WriteAttributeString("FlatUID", (keyEnumeratorFlat.Current as ExtObjKey).UID.ToString());
                        restrictWriter.WriteAttributeString("FlatUUID", UUIDConverter.UUIDToString((keyEnumeratorFlat.Current as ExtObjKey).Signature, (keyEnumeratorFlat.Current as ExtObjKey).Index));

                        RemoteRestrict2 restrict = restrictEnumerator.Current as RemoteRestrict2;

                        restrictWriter.WriteStartElement("Comment");
                        restrictWriter.WriteString(restrict.Text);
                        restrictWriter.WriteEndElement();

                        restrictWriter.WriteStartElement("Day");
                        restrictWriter.WriteAttributeString("Date", restrict.date.ToShortDateString());
                        restrictWriter.WriteEndElement();

                        restrictWriter.WriteStartElement("Pair");
                        restrictWriter.WriteAttributeString("Number", restrict.pair.ToString());
                        restrictWriter.WriteEndElement();

                        restrictWriter.WriteEndElement();
                    }
                    restrictWriter.WriteEndElement();
                }
                #endregion

                #region write group restrict
                IEnumerator enumeratorGroup = restrictByGroup.Values.GetEnumerator();
                IEnumerator keyEnumeratorGroup = restrictByGroup.Keys.GetEnumerator();
                while (enumeratorGroup.MoveNext() && keyEnumeratorGroup.MoveNext())
                {
                    restrictWriter.WriteStartElement("GroupRestricts");

                    IEnumerator restrictEnumerator = (enumeratorGroup.Current as Hashtable).Values.GetEnumerator();
                    while (restrictEnumerator.MoveNext())
                    {
                        restrictWriter.WriteStartElement("GroupRestrict");
                        restrictWriter.WriteAttributeString("GroupUID", (keyEnumeratorGroup.Current as ExtObjKey).UID.ToString());
                        restrictWriter.WriteAttributeString("GroupUUID", UUIDConverter.UUIDToString((keyEnumeratorGroup.Current as ExtObjKey).Signature, (keyEnumeratorGroup.Current as ExtObjKey).Index));

                        RemoteRestrict2 restrict = restrictEnumerator.Current as RemoteRestrict2;

                        restrictWriter.WriteStartElement("Comment");
                        restrictWriter.WriteString(restrict.Text);
                        restrictWriter.WriteEndElement();

                        restrictWriter.WriteStartElement("Day");
                        restrictWriter.WriteAttributeString("Date", restrict.date.ToShortDateString());
                        restrictWriter.WriteEndElement();

                        restrictWriter.WriteStartElement("Pair");
                        restrictWriter.WriteAttributeString("Number", restrict.pair.ToString());
                        restrictWriter.WriteEndElement();

                        restrictWriter.WriteEndElement();
                    }
                    restrictWriter.WriteEndElement();
                }
                #endregion

                #region write tutor restrict
                IEnumerator enumeratorTutor = restrictByTutor.Values.GetEnumerator();
                IEnumerator keyEnumeratorTutor = restrictByTutor.Keys.GetEnumerator();
                while (enumeratorTutor.MoveNext() && keyEnumeratorTutor.MoveNext())
                {
                    restrictWriter.WriteStartElement("TutorRestricts");

                    IEnumerator restrictEnumerator = (enumeratorTutor.Current as Hashtable).Values.GetEnumerator();
                    while (restrictEnumerator.MoveNext())
                    {
                        restrictWriter.WriteStartElement("TutorRestrict");
                        restrictWriter.WriteAttributeString("TutorUID", (keyEnumeratorTutor.Current as ExtObjKey).UID.ToString());
                        restrictWriter.WriteAttributeString("TutorUUID", UUIDConverter.UUIDToString((keyEnumeratorTutor.Current as ExtObjKey).Signature, (keyEnumeratorTutor.Current as ExtObjKey).Index));

                        RemoteRestrict2 restrict = restrictEnumerator.Current as RemoteRestrict2;

                        restrictWriter.WriteStartElement("Comment");
                        restrictWriter.WriteString(restrict.Text);
                        restrictWriter.WriteEndElement();

                        restrictWriter.WriteStartElement("Day");
                        restrictWriter.WriteAttributeString("Date", restrict.date.ToShortDateString());
                        restrictWriter.WriteEndElement();

                        restrictWriter.WriteStartElement("Pair");
                        restrictWriter.WriteAttributeString("Number", restrict.pair.ToString());
                        restrictWriter.WriteEndElement();

                        restrictWriter.WriteEndElement();
                    }
                    restrictWriter.WriteEndElement();
                }
                #endregion

                restrictWriter.WriteEndElement(); // restricts
                restrictWriter.WriteEndDocument();
                restrictWriter.Flush();
                restrictWriter.Close();
            }
            catch
            {
                return false;
            }
            return true;
            #endregion
        }

        public bool SaveShedule(string dirName)
        {
            #region Запис файлу Shedule.xml
            string sheduleFileName = Path.Combine(dirName, "Shedule.xml");
            XmlTextWriter sheduleWriter = new XmlTextWriter(sheduleFileName, System.Text.Encoding.UTF8);
            try
            {
                sheduleWriter.WriteStartDocument(true);
                sheduleWriter.WriteStartElement("Shedule");

                //Запись элементов расписания
                IEnumerator enumerator = shedule.Values.GetEnumerator();

                while (enumerator.MoveNext())
                {
                    IEnumerator lessonEnumerator = (enumerator.Current as Hashtable).Values.GetEnumerator();
                    while (lessonEnumerator.MoveNext())
                    {
                        RemoteLesson2 lsn = (lessonEnumerator.Current as RemoteLesson2);
                        if (lsn != null)
                        {
                            sheduleWriter.WriteStartElement("Lesson");

                            sheduleWriter.WriteStartElement("Day");
                            sheduleWriter.WriteAttributeString("Date", lsn.date.ToShortDateString());
                            sheduleWriter.WriteEndElement();

                            sheduleWriter.WriteStartElement("Pair");
                            sheduleWriter.WriteAttributeString("Number", lsn.pair.ToString());
                            sheduleWriter.WriteEndElement();

                            sheduleWriter.WriteStartElement("Subject");
                            if (lsn.subject_key.UID != -1 || lsn.subject_key.Index != -1)
                            {
                                sheduleWriter.WriteAttributeString("UID", lsn.subject_key.UID.ToString());
                                sheduleWriter.WriteAttributeString("UUID", UUIDConverter.UUIDToString(lsn.subject_key.Signature, lsn.subject_key.Index));
                            }
                            sheduleWriter.WriteEndElement();

                            sheduleWriter.WriteStartElement("SubjectType");
                            if (lsn.studyType_key.UID != -1 || lsn.studyType_key.Index != -1)
                            {
                                sheduleWriter.WriteAttributeString("UID", lsn.studyType_key.UID.ToString());
                                sheduleWriter.WriteAttributeString("UUID", UUIDConverter.UUIDToString(lsn.studyType_key.Signature, lsn.studyType_key.Index));
                            }
                            sheduleWriter.WriteEndElement();

                            sheduleWriter.WriteStartElement("LeadingUid");
                            sheduleWriter.WriteString(lsn.leading_uid.ToString());
                            sheduleWriter.WriteEndElement();


                            sheduleWriter.WriteStartElement("Groups");
                            foreach (ExtObjKey gr_key in lsn.groupList)
                            {
                                if (gr_key.UID != -1 || gr_key.Index != -1)
                                {
                                    sheduleWriter.WriteStartElement("Group");
                                    sheduleWriter.WriteAttributeString("UID", gr_key.UID.ToString());
                                    sheduleWriter.WriteAttributeString("UUID", UUIDConverter.UUIDToString(gr_key.Signature, gr_key.Index));
                                    sheduleWriter.WriteEndElement();
                                }
                            }
                            sheduleWriter.WriteEndElement();

                            sheduleWriter.WriteStartElement("Flats");
                            foreach (ExtObjKey flt_key in lsn.flatList)
                            {
                                if (flt_key.UID != -1 || flt_key.Index != -1)
                                {
                                    sheduleWriter.WriteStartElement("Flat");
                                    sheduleWriter.WriteAttributeString("UID", flt_key.UID.ToString());
                                    sheduleWriter.WriteAttributeString("UUID", UUIDConverter.UUIDToString(flt_key.Signature, flt_key.Index));
                                    sheduleWriter.WriteEndElement();
                                }
                            }
                            sheduleWriter.WriteEndElement();

                            sheduleWriter.WriteStartElement("Tutors");
                            foreach (ExtObjKey ttr_key in lsn.tutorList)
                            {
                                if (ttr_key.UID != -1 || ttr_key.Index != -1)
                                {
                                    sheduleWriter.WriteStartElement("Tutor");
                                    sheduleWriter.WriteAttributeString("UID", ttr_key.UID.ToString());
                                    sheduleWriter.WriteAttributeString("UUID", UUIDConverter.UUIDToString(ttr_key.Signature, ttr_key.Index));
                                    sheduleWriter.WriteEndElement();
                                }
                            }
                            sheduleWriter.WriteEndElement();

                            sheduleWriter.WriteEndElement();
                        }
                    }
                }
                sheduleWriter.WriteEndElement();
                sheduleWriter.WriteEndDocument();
                sheduleWriter.Flush();
                sheduleWriter.Close();
            }
            catch
            {
                return false;
            }
            return true;
            #endregion
        }

        public void Save(string filename)
        {
            string tempFileName = Path.GetFileName(filename) + "." + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Millisecond;
            string tempDir = Path.Combine(Path.GetTempPath(), tempFileName);

            Directory.CreateDirectory(tempDir);
            try
            {
                isSaved = SaveInfo(tempDir) && SaveSupplement(tempDir) &&
                SaveRestrict(tempDir) && SaveShedule(tempDir);

            }
            finally
            {
                if (isSaved)
                {
                    try
                    {
                        SheduleCompressor.MultyCompress(tempFileName, Directory.GetFiles(tempDir));

                    }
                    catch (Exception e)
                    {

                        MessageBox.Show(e.Message);
                    }

                    File.Copy(tempFileName, filename, true);
                }
                Directory.Delete(tempDir, true);
            }
        }


        public bool OpenInfo(string dirName)
        {
            XmlTextReader xmlrd = new XmlTextReader(Path.Combine(dirName, "Info.xml"));
            try
            {
                xmlrd.MoveToContent();
                if (xmlrd.HasAttributes)
                {

                    //this.fileName = Path.GetFileName(fileName);

                    this.version = xmlrd.GetAttribute("Version");
                    this.sheduleTitle = xmlrd.GetAttribute("Title");

                    if (xmlrd.AttributeCount == 4)
                    {
                        this.sheduleBegin = DateTime.Parse(xmlrd.GetAttribute("BeginDate"));
                        this.sheduleEnd = DateTime.Parse(xmlrd.GetAttribute("EndDate"));
                    }
                }
            }
            catch
            {
                xmlrd.Close();
                return false;
            }
            xmlrd.Close();
            return true;
        }

        public bool OpenSupplement(string dirName)
        {
            XmlTextReader xmlrd = new XmlTextReader(Path.Combine(dirName, "Supplement.xml"));
            try
            {
                xmlrd.MoveToContent();
                this.LeadingUidCode = int.Parse(xmlrd.GetAttribute("GeneratorValue"));

                while (xmlrd.Read())
                {
                    if (xmlrd.NodeType == XmlNodeType.Element && xmlrd.Name == "Supplement")
                    {
                        int _leading_uid = -1;
                        long _subject_uid = -1;
                        long _subject_uuid = -1;
                        byte[] _subject_signature = null;
                        long _studyType_uid = -1;
                        long _studyType_uuid = -1;
                        byte[] _studyType_signature = null;
                        long _department_uid = -1;
                        long _department_uuid = -1;
                        byte[] _department_signature = null;
                        int _hourByWeek = 0;
                        int _usedHourByWeek = 0;
                        string _note = "";
                        IList _groups = new ArrayList();
                        IList _tutors = new ArrayList();

                        while (xmlrd.Read())
                        {
                            if (xmlrd.NodeType == XmlNodeType.Element)
                            {
                                #region UID
                                if (xmlrd.Name == "UID")
                                {
                                    xmlrd.Read();
                                    _leading_uid = int.Parse(xmlrd.Value);
                                }
                                #endregion

                                #region Subject
                                if (xmlrd.Name == "Subject")
                                {
                                    if (xmlrd.AttributeCount == 1)
                                        _subject_uid = long.Parse(xmlrd.GetAttribute("UID"));

                                    if (xmlrd.AttributeCount == 2)
                                    {
                                        _subject_uid = long.Parse(xmlrd.GetAttribute("UID"));
                                        UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out _subject_signature, out _subject_uuid);
                                    }

                                }
                                #endregion

                                #region StudyForm
                                if (xmlrd.Name == "StudyForm")
                                {
                                    if (xmlrd.AttributeCount == 1)
                                        _studyType_uid = long.Parse(xmlrd.GetAttribute("UID"));

                                    if (xmlrd.AttributeCount == 2)
                                    {
                                        _studyType_uid = long.Parse(xmlrd.GetAttribute("UID"));
                                        UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out _studyType_signature, out _studyType_uuid);
                                    }
                                }
                                #endregion

                                #region Department
                                if (xmlrd.Name == "Department")
                                {

                                    if (xmlrd.AttributeCount == 1)
                                        _department_uid = long.Parse(xmlrd.GetAttribute("UID"));
                                    if (xmlrd.AttributeCount == 2)
                                    {
                                        _department_uid = long.Parse(xmlrd.GetAttribute("UID"));
                                        UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out _department_signature, out _department_uuid);
                                    }
                                }
                                #endregion

                                #region Hour
                                if (xmlrd.Name == "Hour")
                                {
                                    xmlrd.Read();

                                    _hourByWeek = int.Parse(xmlrd.Value);
                                }
                                #endregion

                                #region UsedHour
                                if (xmlrd.Name == "UsedHour")
                                {
                                    xmlrd.Read();

                                    _usedHourByWeek = int.Parse(xmlrd.Value);
                                }
                                #endregion

                                #region Note
                                if (xmlrd.Name == "Note")
                                {
                                    xmlrd.Read();

                                    _note = xmlrd.Value;
                                }
                                #endregion

                                #region Read Группы
                                if (xmlrd.Name == "ForGroups")
                                {
                                    if (!xmlrd.IsEmptyElement)
                                    {
                                        while (xmlrd.Read())
                                        {
                                            if (xmlrd.NodeType == XmlNodeType.Element)
                                            {
                                                if (xmlrd.Name == "Group")
                                                {
                                                    if (xmlrd.AttributeCount == 1)
                                                    {
                                                        long groupUID = long.Parse(xmlrd.GetAttribute("UID"));
                                                        _groups.Add(new ExtObjKey(groupUID));
                                                    }

                                                    if (xmlrd.AttributeCount == 2)
                                                    {
                                                        long _group_uuid = -1;
                                                        byte[] _group_signature = null;

                                                        long groupUID = long.Parse(xmlrd.GetAttribute("UID"));
                                                        UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out _group_signature, out _group_uuid);

                                                        _groups.Add(new ExtObjKey(groupUID, _group_signature, _group_uuid));
                                                    }

                                                }
                                            }
                                            if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "ForGroups")
                                                break;
                                        }
                                    }
                                }
                                #endregion

                                #region Преподаватели
                                if (xmlrd.Name == "ForTutors")
                                {
                                    if (!xmlrd.IsEmptyElement)
                                    {
                                        while (xmlrd.Read())
                                        {
                                            if (xmlrd.NodeType == XmlNodeType.Element)
                                            {
                                                if (xmlrd.Name == "Tutor")
                                                {
                                                    if (xmlrd.AttributeCount == 1)
                                                    {
                                                        long tutorUID = long.Parse(xmlrd.GetAttribute("UID"));
                                                        _tutors.Add(new ExtObjKey(tutorUID));
                                                    }

                                                    if (xmlrd.AttributeCount == 2)
                                                    {
                                                        long _tutor_uuid = -1;
                                                        byte[] _tutor_signature = null;

                                                        long tutorUID = long.Parse(xmlrd.GetAttribute("UID"));
                                                        UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out _tutor_signature, out _tutor_uuid);

                                                        _tutors.Add(new ExtObjKey(tutorUID, _tutor_signature, _tutor_uuid));
                                                    }
                                                }
                                            }
                                            if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "ForTutors")
                                                break;
                                        }
                                    }
                                }
                                #endregion

                            }
                            if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "Supplement")
                            {
                                /* Создаем новую нагрузку */

                                RemoteStudyLeading2 newRemoteStudyLeading = new RemoteStudyLeading2();
                                newRemoteStudyLeading.uid = _leading_uid;

                                newRemoteStudyLeading.subject_key = new ExtObjKey(_subject_uid, _subject_signature, _subject_uuid);
                                newRemoteStudyLeading.subjectType_key = new ExtObjKey(_studyType_uid, _studyType_signature, _studyType_uuid);
                                newRemoteStudyLeading.department_key = new ExtObjKey(_department_uid, _department_signature, _department_uuid);

                                newRemoteStudyLeading.hourByWeek = _hourByWeek;
                                newRemoteStudyLeading.usedHourByWeek = _usedHourByWeek;
                                newRemoteStudyLeading.note = _note;

                                newRemoteStudyLeading.listOfGroups = _groups;
                                newRemoteStudyLeading.listOfTutors = _tutors;

                                studyLeading.Add(newRemoteStudyLeading.uid, newRemoteStudyLeading);
                                break;
                            }
                        }
                    }

                }
            }
            catch
            {
                xmlrd.Close();
                return false;
            }
            xmlrd.Close();
            return true;
        }

        public bool OpenRestrict(string dirName)
        {
            XmlTextReader xmlrd = new XmlTextReader(Path.Combine(dirName, "Restrict.xml"));
            try
            {
                xmlrd.MoveToContent();
                while (xmlrd.Read())
                {
                    #region Read Flat Restricts
                    if (xmlrd.NodeType == XmlNodeType.Element && xmlrd.Name == "FlatRestrict")
                    {
                        DateTime studyDay_index = DateTime.MinValue;
                        int studyHour_index = -1;
                        string comment = "";


                        #region Read lecture hall
                        ExtObjKey flat_uid = new ExtObjKey();
                        flat_uid.UID = long.Parse(xmlrd.GetAttribute("FlatUID"));
                        UUIDConverter.StringToUUID(xmlrd.GetAttribute("FlatUUID"), out flat_uid.Signature, out flat_uid.Index);
                        //long flat_uid = long.Parse( xmlrd.GetAttribute("FlatUID") );
                        #endregion

                        while (xmlrd.Read())
                        {
                            if (xmlrd.NodeType == XmlNodeType.Element)
                            {
                                #region Read Comment
                                if (xmlrd.Name == "Comment")
                                {
                                    xmlrd.Read();
                                    try
                                    {
                                        comment = xmlrd.Value;
                                    }
                                    catch
                                    {
                                        //MessageBox.Show(e.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        //throw new Exception(e.Message);
                                        //while( xmlrd.NodeType != XmlNodeType.Text ) xmlrd.Read();
                                        //string cycleMnemo = xmlrd.Value;
                                        //studyCycle = new StudyCycles(cycleMnemo, cycleMnemo);
                                    }
                                }
                                #endregion

                                #region Read Day
                                if (xmlrd.Name == "Day")
                                {
                                    studyDay_index = DateTime.Parse(xmlrd.GetAttribute("Date"));
                                }
                                #endregion

                                #region Read Pair
                                if (xmlrd.Name == "Pair")
                                {
                                    studyHour_index = int.Parse(xmlrd.GetAttribute("Number"));
                                }
                                #endregion
                            }
                            if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "FlatRestrict")
                            {
                                RemoteRestrict2 newRestrict = new RemoteRestrict2();
                                newRestrict.date = studyDay_index;
                                newRestrict.pair = studyHour_index;
                                newRestrict.Text = comment;

                                InsertFlatRestrict(newRestrict.date, newRestrict.pair, flat_uid, newRestrict);
                                break;
                            }
                        }
                    }
                    #endregion

                    #region Read Group Restricts
                    if (xmlrd.NodeType == XmlNodeType.Element && xmlrd.Name == "GroupRestrict")
                    {

                        DateTime studyDay = DateTime.MinValue;
                        int studyHour = -1;
                        string comment = "";

                        #region Read group
                        ExtObjKey group_uid = new ExtObjKey();
                        group_uid.UID = long.Parse(xmlrd.GetAttribute("GroupUID"));
                        UUIDConverter.StringToUUID(xmlrd.GetAttribute("GroupUUID"), out group_uid.Signature, out group_uid.Index);
                        //long group_uid = long.Parse( xmlrd.GetAttribute("GroupUID") );
                        #endregion

                        while (xmlrd.Read())
                        {
                            if (xmlrd.NodeType == XmlNodeType.Element)
                            {
                                #region Read Comment
                                if (xmlrd.Name == "Comment")
                                {
                                    try
                                    {
                                        xmlrd.Read();
                                        comment = xmlrd.ReadString();
                                    }
                                    catch
                                    {
                                        //MessageBox.Show(e.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        //throw new Exception(e.Message);
                                    }
                                }
                                #endregion

                                #region Read Day
                                if (xmlrd.Name == "Day")
                                {
                                    studyDay = DateTime.Parse(xmlrd.GetAttribute("Date"));
                                }
                                #endregion

                                #region Read Pair
                                if (xmlrd.Name == "Pair")
                                {
                                    studyHour = int.Parse(xmlrd.GetAttribute("Number"));
                                }
                                #endregion
                            }
                            if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "GroupRestrict")
                            {
                                RemoteRestrict2 newRestrict = new RemoteRestrict2();
                                newRestrict.date = studyDay;
                                newRestrict.pair = studyHour;
                                newRestrict.Text = comment;

                                InsertGroupRestrict(newRestrict.date, newRestrict.pair, group_uid, newRestrict);
                                break;
                            }
                        }
                    }
                    #endregion

                    #region Read Tutor Restricts
                    if (xmlrd.NodeType == XmlNodeType.Element && xmlrd.Name == "TutorRestrict")
                    {
                        DateTime studyDay = DateTime.MinValue;
                        int studyHour = -1;
                        string comment = "";


                        #region Read tutor
                        //long tutor_uid = long.Parse( xmlrd.GetAttribute("TutorUID") );
                        ExtObjKey tutor_uid = new ExtObjKey();
                        tutor_uid.UID = long.Parse(xmlrd.GetAttribute("TutorUID"));
                        UUIDConverter.StringToUUID(xmlrd.GetAttribute("TutorUUID"), out tutor_uid.Signature, out tutor_uid.Index);
                        #endregion

                        while (xmlrd.Read())
                        {
                            if (xmlrd.NodeType == XmlNodeType.Element)
                            {
                                #region Read Comment
                                if (xmlrd.Name == "Comment")
                                {
                                    try
                                    {
                                        xmlrd.Read();
                                        comment = xmlrd.Value;
                                    }
                                    catch
                                    {
                                        //MessageBox.Show(e.Message, "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        //throw new Exception(e.Message);
                                    }
                                }
                                #endregion

                                #region Read Day
                                if (xmlrd.Name == "Day")
                                {
                                    studyDay = DateTime.Parse(xmlrd.GetAttribute("Date"));
                                }
                                #endregion

                                #region Read Pair
                                if (xmlrd.Name == "Pair")
                                {
                                    studyHour = int.Parse(xmlrd.GetAttribute("Number"));
                                }
                                #endregion
                            }
                            if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "TutorRestrict")
                            {
                                RemoteRestrict2 newRestrict = new RemoteRestrict2();
                                newRestrict.date = studyDay;
                                newRestrict.pair = studyHour;
                                newRestrict.Text = comment;

                                InsertTutorRestrict(newRestrict.date, newRestrict.pair, tutor_uid, newRestrict);
                                break;
                            }
                        }
                    }
                    #endregion
                }
            }
            catch
            {
                xmlrd.Close();
                return false;
            }
            xmlrd.Close();
            return true;
        }

        public bool OpenShedule(string dirName)
        {
            XmlTextReader xmlrd = new XmlTextReader(Path.Combine(dirName, "Shedule.xml"));
            try
            {
                xmlrd.MoveToContent();
                while (xmlrd.Read())
                {
                    # region Read Lesson
                    if (xmlrd.NodeType == XmlNodeType.Element && xmlrd.Name == "Lesson")
                    {
                        DateTime _studyDayIndex = DateTime.MinValue;

                        int _studyHourIndex = -1;

                        long _studySubject = -1;
                        long _studySubject_uuid = -1;
                        byte[] _studySubject_signature = null;

                        long _studyType = -1;
                        long _studyType_uuid = -1;
                        byte[] _studyType_signature = new byte[24];

                        int _leadingUid = 0;

                        ArrayList _groupList = new ArrayList();
                        ArrayList _flatList = new ArrayList();
                        ArrayList _tutorList = new ArrayList();

                        while (xmlrd.Read())
                        {
                            if (xmlrd.NodeType == XmlNodeType.Element)
                            {
                                #region Read Day
                                if (xmlrd.Name == "Day")
                                {
                                    _studyDayIndex = DateTime.Parse(xmlrd.GetAttribute("Date"));
                                }
                                #endregion

                                #region Read Pair
                                if (xmlrd.Name == "Pair")
                                {
                                    _studyHourIndex = int.Parse(xmlrd.GetAttribute("Number"));
                                    string indexString = xmlrd.GetAttribute("Index");
                                    if (indexString != string.Empty && indexString != null)
                                        _studyHourIndex = int.Parse(xmlrd.GetAttribute("Index"));
                                }
                                #endregion

                                #region Read Subject
                                if (xmlrd.Name == "Subject")
                                {
                                    _studySubject = long.Parse(xmlrd.GetAttribute("UID"));
                                    if (xmlrd.AttributeCount == 1)
                                        _studySubject = long.Parse(xmlrd.GetAttribute("UID"));

                                    if (xmlrd.AttributeCount == 2)
                                    {
                                        _studySubject = long.Parse(xmlrd.GetAttribute("UID"));
                                        UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out _studySubject_signature, out _studySubject_uuid);
                                    }
                                }
                                #endregion

                                #region Read SubjectType
                                if (xmlrd.Name == "SubjectType")
                                {
                                    if (xmlrd.AttributeCount == 1)
                                        _studyType = long.Parse(xmlrd.GetAttribute("UID"));

                                    if (xmlrd.AttributeCount == 2)
                                    {
                                        _studyType = long.Parse(xmlrd.GetAttribute("UID"));
                                        UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out _studyType_signature, out _studyType_uuid);
                                    }
                                }
                                #endregion

                                #region Read LeadingUid
                                if (xmlrd.Name == "LeadingUid")
                                {
                                    xmlrd.Read();

                                    _leadingUid = int.Parse(xmlrd.Value);
                                }
                                #endregion

                                #region Read Группы
                                if (xmlrd.Name == "Groups" && !xmlrd.IsEmptyElement)
                                {
                                    while (xmlrd.Read())
                                    {
                                        if (xmlrd.NodeType == XmlNodeType.Element)
                                        {
                                            if (xmlrd.Name == "Group")
                                            {
                                                if (xmlrd.AttributeCount == 1)
                                                {
                                                    long groupUID = long.Parse(xmlrd.GetAttribute("UID"));
                                                    _groupList.Add(new ExtObjKey(groupUID));
                                                }

                                                if (xmlrd.AttributeCount == 2)
                                                {
                                                    long _group_uuid = -1;
                                                    byte[] _group_signature = null;

                                                    long groupUID = long.Parse(xmlrd.GetAttribute("UID"));
                                                    UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out _group_signature, out _group_uuid);

                                                    _groupList.Add(new ExtObjKey(groupUID, _group_signature, _group_uuid));
                                                }
                                            }
                                        }
                                        if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "Groups")
                                            break;
                                    }
                                }
                                #endregion

                                #region Аудитории
                                if (xmlrd.Name == "Flats" && !xmlrd.IsEmptyElement)
                                {
                                    while (xmlrd.Read())
                                    {
                                        if (xmlrd.NodeType == XmlNodeType.Element)
                                        {
                                            if (xmlrd.AttributeCount == 1)
                                            {
                                                long flatUID = long.Parse(xmlrd.GetAttribute("UID"));
                                                _flatList.Add(new ExtObjKey(flatUID));
                                            }

                                            if (xmlrd.AttributeCount == 2)
                                            {
                                                long _flat_uuid = -1;
                                                byte[] _flat_signature = null;

                                                long flatUID = long.Parse(xmlrd.GetAttribute("UID"));
                                                UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out _flat_signature, out _flat_uuid);

                                                _flatList.Add(new ExtObjKey(flatUID, _flat_signature, _flat_uuid));
                                            }
                                        }
                                        if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "Flats")
                                            break;
                                    }
                                }
                                #endregion

                                #region Преподаватели
                                if (xmlrd.Name == "Tutors" && !xmlrd.IsEmptyElement)
                                {
                                    while (xmlrd.Read())
                                    {
                                        if (xmlrd.NodeType == XmlNodeType.Element)
                                        {
                                            if (xmlrd.AttributeCount == 1)
                                            {
                                                long tutorUID = long.Parse(xmlrd.GetAttribute("UID"));
                                                _tutorList.Add(new ExtObjKey(tutorUID));
                                            }

                                            if (xmlrd.AttributeCount == 2)
                                            {
                                                long _tutor_uuid = -1;
                                                byte[] _tutor_signature = null;

                                                long tutorUID = long.Parse(xmlrd.GetAttribute("UID"));
                                                UUIDConverter.StringToUUID(xmlrd.GetAttribute("UUID"), out _tutor_signature, out _tutor_uuid);

                                                _tutorList.Add(new ExtObjKey(tutorUID, _tutor_signature, _tutor_uuid));
                                            }
                                        }
                                        if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "Tutors")
                                            break;
                                    }
                                }
                                #endregion
                            }
                            if (xmlrd.NodeType == XmlNodeType.EndElement && xmlrd.Name == "Lesson")
                            {
                                RemoteLesson2 newRemoteLesson = new RemoteLesson2();
                                newRemoteLesson.leading_uid = _leadingUid;
                                newRemoteLesson.subject_key = new ExtObjKey(_studySubject, _studySubject_signature, _studySubject_uuid);
                                newRemoteLesson.studyType_key = new ExtObjKey(_studyType, _studyType_signature, _studyType_uuid);
                                newRemoteLesson.date = _studyDayIndex;
                                newRemoteLesson.pair = _studyHourIndex;
                                newRemoteLesson.groupList = _groupList;
                                newRemoteLesson.flatList = _flatList;
                                newRemoteLesson.tutorList = _tutorList;
                                //вставка считанного урока в расписание
                                SimpleInsert(newRemoteLesson.date, newRemoteLesson.pair, newRemoteLesson);
                                break;
                            }
                        }
                    }
                    #endregion
                }
            }
            catch
            {
                xmlrd.Close();
                return false;
            }
            xmlrd.Close();
            return true;
        }

        public void Open(string filename)
        {
            isSaved = true;
            string tempFileName = Path.GetFileName(filename) + "." + DateTime.Now.Year + DateTime.Now.Month + DateTime.Now.Day + DateTime.Now.Hour + DateTime.Now.Minute + DateTime.Now.Millisecond;
            string tempDir = Path.Combine(Path.GetTempPath(), tempFileName);

            Directory.CreateDirectory(tempDir);
            try
            {
                SheduleCompressor.MultyDecompress(filename, tempDir);

                OpenInfo(tempDir);
                OpenSupplement(tempDir);
                OpenRestrict(tempDir);
                OpenShedule(tempDir);

                this.fileName = Path.GetFileName(filename);
            }
            finally
            {
                Directory.Delete(tempDir, true);
            }
        }

        #endregion

        #region All Checkers
        /* FlatBusyChecker */
        public bool FlatBusyCheckBool(DateTime date, int pair, ExtObjKey flat_uid)
        {
            int sheduleKey = GetKey(date, pair);
            if (this.lessonsByFlat.Contains(flat_uid))
                if ((lessonsByFlat[flat_uid] as Hashtable).Contains(sheduleKey))
                    if (((lessonsByFlat[flat_uid] as Hashtable)[sheduleKey] as Hashtable).Count != 0)
                        return true;
            return false;
        }

        public bool FlatBusyCheckBoolRestrict(DateTime date, int pair, ExtObjKey flat_uid)
        {
            int sheduleKey = GetKey(date, pair);
            if (restrictByFlat.Contains(flat_uid))
                if ((restrictByFlat[flat_uid] as Hashtable).Contains(sheduleKey))
                    return true;
                else
                    return false;
            else
                return false;
        }

        public bool FlatBusyCheckBool(DateTime date, int pair, IList flats)
        {
            bool isNotBusy = true;
            foreach (ExtObjKey flt_key in flats)
                isNotBusy = isNotBusy && !FlatBusyCheckBool(date, pair, flt_key);
            return !isNotBusy;
        }

        public bool FlatBusyCheckBoolRestrict(DateTime date, int pair, IList flats)
        {
            bool isNotBusy = true;
            foreach (ExtObjKey flt_key in flats)
                isNotBusy = isNotBusy && !FlatBusyCheckBoolRestrict(date, pair, flt_key);
            return !isNotBusy;
        }

        public FlatBusyCheckerState FlatBusyCheck(DateTime date, int pair, ExtObjKey flat_uid)
        {
            if (FlatBusyCheckBool(date, pair, flat_uid))
                return FlatBusyCheckerState.isBusy;
            else
                if (FlatBusyCheckBoolRestrict(date, pair, flat_uid))
                    return FlatBusyCheckerState.isRestrict;
                else
                    return FlatBusyCheckerState.isNotBusy;
        }

        public FlatBusyCheckerState FlatBusyCheck(DateTime date, int pair, IList flats)
        {
            if (FlatBusyCheckBool(date, pair, flats) == true)
                return FlatBusyCheckerState.isBusy;
            else
                if (FlatBusyCheckBoolRestrict(date, pair, flats))
                    return FlatBusyCheckerState.isRestrict;
                else
                    return FlatBusyCheckerState.isNotBusy;
        }

        /*GroupBusyChecker*/
        public bool GroupBusyCheckBool(DateTime date, int pair, ExtObjKey group_uid)
        {
            int sheduleKey = GetKey(date, pair);
            if (lessonsByGroup.Contains(group_uid))
                if ((lessonsByGroup[group_uid] as Hashtable).Contains(sheduleKey))
                    return true;
                else
                    return false;
            else
                return false;
        }

        public bool GroupBusyCheckBoolRestrict(DateTime date, int pair, ExtObjKey group_uid)
        {
            int sheduleKey = GetKey(date, pair);
            if (restrictByGroup.Contains(group_uid))
                if ((restrictByGroup[group_uid] as Hashtable).Contains(sheduleKey))
                    return true;
                else
                    return false;
            else
                return false;
        }

        public bool GroupBusyCheckBool(DateTime date, int pair, IList groups)
        {
            bool isNotBusy = true;
            foreach (ExtObjKey group_key in groups)
                isNotBusy = isNotBusy && !GroupBusyCheckBool(date, pair, group_key);
            return !isNotBusy;
        }

        public bool GroupBusyCheckBoolRestrict(DateTime date, int pair, IList groups)
        {
            bool isNotBusy = true;
            foreach (ExtObjKey group_key in groups)
                isNotBusy = isNotBusy && !GroupBusyCheckBoolRestrict(date, pair, group_key);
            return !isNotBusy;
        }

        public GroupBusyCheckerState GroupBusyCheck(DateTime date, int pair, ExtObjKey group_uid)
        {
            if (GroupBusyCheckBool(date, pair, group_uid))
                return GroupBusyCheckerState.isBusy;
            else
                if (GroupBusyCheckBoolRestrict(date, pair, group_uid))
                    return GroupBusyCheckerState.isRestrict;
                else
                    return GroupBusyCheckerState.isNotBusy;
        }

        public GroupBusyCheckerState GroupBusyCheck(DateTime date, int pair, IList groups)
        {
            if (GroupBusyCheckBool(date, pair, groups) == true)
                return GroupBusyCheckerState.isBusy;
            else
                if (GroupBusyCheckBoolRestrict(date, pair, groups) == true)
                    return GroupBusyCheckerState.isRestrict;
                else
                    return GroupBusyCheckerState.isNotBusy;
        }

        /*TutorBusyChecker*/
        public bool TutorBusyCheckBool(DateTime date, int pair, ExtObjKey tutor_uid)
        {
            int sheduleKey = GetKey(date, pair);
            if (lessonsByTutor.Contains(tutor_uid))
                if ((lessonsByTutor[tutor_uid] as Hashtable).Contains(sheduleKey))
                    return true;
                else
                    return false;
            else
                return false;
        }

        public bool TutorBusyCheckBoolRestrict(DateTime date, int pair, ExtObjKey tutor_uid)
        {
            int sheduleKey = GetKey(date, pair);
            if (restrictByTutor.Contains(tutor_uid))
                if ((restrictByTutor[tutor_uid] as Hashtable).Contains(sheduleKey))
                    return true;
                else
                    return false;
            else
                return false;
        }

        public bool TutorBusyCheckBool(DateTime date, int pair, IList tutors)
        {
            bool isNotBusy = true;
            foreach (ExtObjKey tutor_key in tutors)
                isNotBusy = isNotBusy && !TutorBusyCheckBool(date, pair, tutor_key);
            return !isNotBusy;
        }

        public bool TutorBusyCheckBoolRestrict(DateTime date, int pair, IList tutors)
        {
            bool isNotBusy = true;
            foreach (ExtObjKey tutor_key in tutors)
                isNotBusy = isNotBusy && !TutorBusyCheckBoolRestrict(date, pair, tutor_key);
            return !isNotBusy;
        }

        public TutorBusyCheckerState TutorBusyCheck(DateTime date, int pair, ExtObjKey tutor_uid)
        {
            if (TutorBusyCheckBool(date, pair, tutor_uid))
                return TutorBusyCheckerState.isBusy;
            else
                if (TutorBusyCheckBoolRestrict(date, pair, tutor_uid))
                    return TutorBusyCheckerState.isRestrict;
                else
                    return TutorBusyCheckerState.isNotBusy;
        }

        public TutorBusyCheckerState TutorBusyCheck(DateTime date, int pair, IList tutors)
        {
            if (TutorBusyCheckBool(date, pair, tutors) == true)
                return TutorBusyCheckerState.isBusy;
            else
                if (TutorBusyCheckBoolRestrict(date, pair, tutors) == true)
                    return TutorBusyCheckerState.isRestrict;
                else
                    return TutorBusyCheckerState.isNotBusy;
        }

        public bool CheckLessonCountByPair(DateTime date, int pair, ExtObjKey studyForm, ExtObjKey subject, int limit)
        {
            int sheduleKey = GetKey(date, pair);
            if (!shedule.Contains(sheduleKey))
                return false;
            IList lst = new ArrayList((shedule[sheduleKey] as Hashtable).Values);
            int entryCount = 0;
            foreach (RemoteLesson2 remLes in lst)
                if (remLes.studyType_key.Equals(studyForm) && remLes.subject_key.Equals(subject))
                    entryCount++;
            return (entryCount >= limit);
        }

        public bool CheckDeptLessonCountByPair(DateTime date, int pair, ExtObjKey studyForm, ExtObjKey department, int limit)
        {
            int sheduleKey = GetKey(date, pair);
            if (!shedule.Contains(sheduleKey))
                return false;
            IList lst = new ArrayList((shedule[sheduleKey] as Hashtable).Values);
            int entryCount = 0;
            foreach (RemoteLesson2 remLes in lst)
                if (remLes.studyType_key.Equals(studyForm) && (studyLeading[remLes.leading_uid] as RemoteStudyLeading2).department_key.Equals(department))
                    entryCount++;
            return (entryCount >= limit);
        }

        #endregion

        public override object InitializeLifetimeService()
        {
            return null;
        }
    }

}