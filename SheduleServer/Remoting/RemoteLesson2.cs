﻿using System;
using System.Collections;

namespace Shturman.Shedule.Server.Remoting
{
    /// <summary>
    /// Summary description for LessonStruct.
    /// </summary>
    [Serializable]
    public class RemoteLesson2
    {
        public int leading_uid;

        /* порядковые номера недели пары и дня */
        public DateTime date;
        public int pair;

        public ExtObjKey subject_key;
        public ExtObjKey studyType_key;

        public IList groupList;
        public IList tutorList;
        public IList flatList;
    }
}