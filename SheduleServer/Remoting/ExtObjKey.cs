﻿using System;
using System.Runtime.Serialization;


namespace Shturman.Shedule.Server.Remoting
{
    /// <summary>
    /// Клас, для репрезентаціії унікального ключа об"єкта, та ключа залежного від
    /// хранилища де він був створенний
    /// </summary>
    [Serializable]
    public class ExtObjKey : ISerializable
    {
        public string TypeName = "";
        // UID is a internal db key that can change
        public long UID = -1;
        // UUID is index + signature and this key is not change
        public long Index = -1;

        public byte[] Signature = new byte[0];

        public ExtObjKey(long UID, byte[] Signature, long Index)
        {
            this.UID = UID;
            this.Index = Index;
            this.Signature = Signature;
        }

        public ExtObjKey(long UID)
        {
            this.UID = UID;
        }

        public ExtObjKey()
        {
            this.UID = -1;
            this.Signature = null;
            this.Index = -1;
        }


        #region Support Custom Serialization
        private ExtObjKey(SerializationInfo si, StreamingContext ctx)
        {
            this.TypeName = si.GetString("TypeName");
            this.UID = si.GetInt64("UID");
            string UUIDStr = si.GetString("UUIDStr");
            if (UUIDStr != "")
                UUIDConverter.StringToUUID(UUIDStr, out this.Signature, out this.Index);
        }

        public void GetObjectData(SerializationInfo si, StreamingContext ctx)
        {
            si.AddValue("TypeName", this.TypeName);
            si.AddValue("UID", this.UID);
            si.AddValue("UUIDStr", UUIDConverter.UUIDToString(this.Signature, this.Index));
        }
        #endregion


        public override bool Equals(object obj)
        {
            if (obj is ExtObjKey)
            {
                ExtObjKey cmpObject = obj as ExtObjKey;
                bool same = true;
                if (this.Signature != null && cmpObject.Signature != null)
                    if (this.Signature.Length == cmpObject.Signature.Length)
                    {
                        for (int i = 0; i < this.Signature.Length; i++)
                        {
                            if (this.Signature[i] != cmpObject.Signature[i])
                            {
                                same = false;
                                break;
                            }
                        }
                    }
                    else
                    {
                        same = false;
                    }

                return same && ((obj as ExtObjKey).Index == this.Index) && ((obj as ExtObjKey).UID == this.UID);
            }
            else
                return false;
        }

        static public bool operator ==(ExtObjKey a, ExtObjKey b)
        {
            bool anull = object.Equals(a, null), bnull = object.Equals(b, null);

            if (anull && bnull)
                return true;
            else
                if (!anull && !bnull)
                    return a.Equals(b);
                else
                    return false;
        }

        static public bool operator !=(ExtObjKey a, ExtObjKey b)
        {
            bool anull = object.Equals(a, null), bnull = object.Equals(b, null);

            if (anull && bnull)
                return false;
            else
                if (!anull && !bnull)
                    return !a.Equals(b);
                else
                    return true;

        }

        public override int GetHashCode()
        {
            int hashcode = 0;
            for (int index = 0; index < Signature.Length; index++)
            {
                hashcode <<= 1;
                hashcode ^= Signature[index];
            }

            hashcode ^= Index.GetHashCode();
            return hashcode;
        }


        public bool IsEmpty
        {
            get { return UID == -1 && Index == -1; }
        }
    }
}