﻿using System;
using System.Collections.Generic;
using System.Text;

using Shturman.Shedule.Server.Remoting;

namespace Shturman.Shedule.Server.Interface
{
    /// <summary>
    /// Інтерфейс який розширює можливості стандартного класу розкладу,
    /// можливостями роботи з посиланнями на інші розклади занять
    /// </summary>
    public interface ISheduleReference
    {
        /// <summary>
        /// Додавання посилання на розклад занять
        /// </summary>
        /// <param name="name">Назва посилання</param>
        /// <param name="shedule">Розклад занять</param>
        void AddReference(string name, object shedule);

        /// <summary>
        /// Видалення посилання
        /// </summary>
        /// <param name="name">Назва посилання</param>
        void RemoveReference(string name);

        /// <summary>
        /// Отримання списку назв посилань
        /// </summary>
        /// <returns>список назв посилань</returns>
        string[] GetReferenceNameList();

        /// <summary>
        /// Отримання посилань
        /// </summary>
        /// <returns>список посилань на інші розклади</returns>
        object[] GelReferenceList();

        /// <summary>
        /// Перевірка зайнятості викладача, групи, аудиторії
        /// </summary>
        /// <param name="objKey">Ключ викладача, групи, аудиторії</param>
        /// <param name="day">індекс дня тижня</param>
        /// <param name="pair">індекс пари</param>
        /// <param name="week">індекс тижня</param>
        /// <param name="message">повідомлення</param>
        /// <returns>true, якщо присутня зайнятість та false у іншому випадку</returns>
        bool CkeckBusy(ExtObjKey objKey, int day, int pair, int week, out string message);

        /// <summary>
        /// Перевірка зайнятості кількох викладача, групи, аудиторії
        /// </summary>
        /// <param name="objKeyList">Список ключів викладача, групи, аудиторії</param>
        /// <param name="day">індекс дня тижня</param>
        /// <param name="pair">індекс пари</param>
        /// <param name="week">індекс тижня</param>
        /// <param name="message">повідомлення</param>
        /// <returns>true, якщо присутня зайнятість та false у іншому випадку</returns>
        bool CkeckBusy(List<ExtObjKey> objKeyList, int day, int pair, int week, out string message);

        /// <summary>
        /// Перевірка зайнятості кількох викладача, групи, аудиторії
        /// </summary>
        /// <param name="objKey">Ключ викладача, групи, аудиторії</param>
        /// <param name="date">індекс дня тижня</param>
        /// <param name="pair">індекс пари</param>
        /// <param name="message">повідомлення</param>
        /// <returns>true, якщо присутня зайнятість та false у іншому випадку</returns>
        bool CkeckBusy(ExtObjKey objKey, DateTime date, int pair, out string message);

        /// Перевірка зайнятості кількох викладача, групи, аудиторії
        /// </summary>
        /// <param name="objKeyList">Список ключів викладача, групи, аудиторії</param>
        /// <param name="date">індекс дня тижня</param>
        /// <param name="pair">індекс пари</param>
        /// <param name="week">індекс тижня</param>
        /// <param name="message">повідомлення</param>
        /// <returns>true, якщо присутня зайнятість та false у іншому випадку</returns>
        bool CkeckBusy(List<ExtObjKey> objKeyList, DateTime date, int pair, out string message);
    }
}