using System;
using System.Collections;
using System.Xml;
using Shturman.Shedule.Server.Remoting;

namespace Shturman.Shedule.Server.Interface
{
	/// <summary>
	/// Summary description for IShedule.
	/// </summary>
	public enum FlatBusyCheckerState { isBusy, isRestrict, isNotBusy};
	public enum GroupBusyCheckerState {isBusy, isRestrict, isNotBusy};
	public enum TutorBusyCheckerState {isBusy, isRestrict, isNotBusy};

	public delegate bool CheckRight(ExtObjKey user_uid);

    public interface IShedule : ISheduleReference
	{
		 
		int GetKey(int weekCycle, int weekDay, int weekPair);
        int GetKey(DateTime date, int pair);
		
		int GetStudyLeadingUid();
		IList GetAllStudyLeading();
		IList GetStudyLeadingByGroups(ExtObjKey[] groupUidList);
		IList GetStudyLeadingByDept(ExtObjKey deptUid);
		int AddStudyLeading(RemoteStudyLeading stLeading);
		
		void DelLessonsByLeading(int leadingUid);
		void DelLessonsByLeading(int leadingUid, CheckRight checkRight);

		void DelStudyLeading(RemoteStudyLeading stLeading);
		void ChangeStudyLeading(int leading_uid, RemoteStudyLeading stLeading);
		RemoteStudyLeading GetStudyLeading(int leading_uid);

		bool CanInsertTo(int weekCycle, int weekDay, int weekPair, RemoteLesson lesson);
		
		int Insert(int weekCycle, int weekDay, int weekPair, RemoteLesson lesson);
		int SimpleInsert(int weekCycle, int weekDay, int weekPair, RemoteLesson lesson);
		
		void DeleteByGroup(int weekCycle, int weekDay, int weekPair, IList groupList );
		void DeleteByFlat(int weekCycle, int weekDay, int weekPair, IList flatList );
		
		void ChangeLessonFlats(RemoteLesson[] lessonList, ExtObjKey[] flatList);
		void MoveLesson(RemoteLesson lesson, int nweekCycle, int nweekDay, int nweekPair);
		
		int InsertGroupRestrict(int weekCycle, int weekDay, int weekPair, ExtObjKey group_uid, RemoteRestrict restrict);
		int InsertTutorRestrict(int weekCycle, int weekDay, int weekPair, ExtObjKey tutor_uid, RemoteRestrict restrict);
		int InsertFlatRestrict(int weekCycle, int weekDay, int weekPair, ExtObjKey flat_uid, RemoteRestrict restrict);
		
		void DeleteGroupRestrict(int weekCycle, int weekDay, int weekPair, ExtObjKey group_uid);
		void DeleteTutorRestrict(int weekCycle, int weekDay, int weekPair, ExtObjKey tutor_uid);
		void DeleteFlatRestrict(int weekCycle, int weekDay, int weekPair, ExtObjKey flat_uid);
		
		bool TutorHasLesson(ExtObjKey TutorUID, int SheduleKey);
		RemoteLesson GetTutorLesson(ExtObjKey tutor_uid, int SheduleKey);
		IList GetTutorLessons(ExtObjKey tutor_uid);

		bool GroupHasLesson(ExtObjKey GroupUID, int SheduleKey);
		RemoteLesson GetGroupLesson(ExtObjKey group_uid, int SheduleKey);
		IList GetGroupLessons(ExtObjKey group_uid);

		bool FlatHasLesson(ExtObjKey FlatUID, int SheduleKey);
		IList GetFlatLesson(ExtObjKey flat_uid, int SheduleKey);
		IList GetFlatLessons(ExtObjKey flat_uid);

		IList GetTutorRestricts(ExtObjKey tutor_uid);
		bool IsTutorHasRestricts(ExtObjKey tutor_uid, int SheduleKey);
		string GetTutorRestrictsText(ExtObjKey tutor_uid, int SheduleKey);

		IList GetGroupRestricts(ExtObjKey group_uid);
		bool IsGroupHasRestricts(ExtObjKey group_uid, int SheduleKey);
		string GetGroupRestrictsText(ExtObjKey group_uid, int SheduleKey);

		IList GetFlatRestricts(ExtObjKey flat_uid);
		bool IsFlatHasRestricts(ExtObjKey flat_uid, int SheduleKey);		
		string GetFlatRestrictsText(ExtObjKey flat_uid, int SheduleKey);

		IList GetLessons(ExtObjKey[] subject_uids, ExtObjKey[] subjectType_uids);
		IList GetLessons(int leading_uid);

		int[] GetKeyTutorLessons(ExtObjKey TutorUID);
		int[] GetKeyGroupLessons(ExtObjKey GroupUID);
		int[] GetKeyFlatLessons(ExtObjKey FlatUID);
		int[] GetKeyTutorRestricts(ExtObjKey TutorUID);
		int[] GetKeyGroupRestricts(ExtObjKey GroupUID);
		int[] GetKeyFlatRestricts(ExtObjKey FlatUID);
		int[] GetKeyTutorBusy(ExtObjKey TutorUID);
		int[] GetKeyGroupBusy(ExtObjKey GroupUID);
		int[] GetKeyFlatBusy(ExtObjKey FlatUID);

		// Info Methods
		bool GetIsSaved();
		void SetIsSaved(bool savedFlag);
		string GetSheduleTitle();
		void SetSheduleTitle(string title);
		string GetFileName();
		void SetFileName(string fileName);
		DateTime GetSheduleBeginDate();
		void SetSheduleBeginDate(DateTime beginDate);
		DateTime GetSheduleEndDate();
		void SetSheduleEndDate(DateTime sheduleEndDate);
		int GetWeeksCount();
		int GetDaysPerWeekCount();
		int GetPairsPerDayCount();

        object[] GetSheduleInfo();

		// Checkers Methods
		bool FlatBusyCheckBool(int sheduleKey, ExtObjKey flat_uid);
		bool FlatBusyCheckBoolRestrict(int sheduleKey, ExtObjKey flat_uid);
		bool FlatBusyCheckBool(int sheduleKey, IList flats);
		bool FlatBusyCheckBoolRestrict(int sheduleKey, IList flats);
		FlatBusyCheckerState FlatBusyCheck(int sheduleKey, ExtObjKey flat_uid);
		FlatBusyCheckerState FlatBusyCheck(int sheduleKey, IList flats);
		bool GroupBusyCheckBool(int sheduleKey, ExtObjKey group_uid);
		bool GroupBusyCheckBoolRestrict(int sheduleKey, ExtObjKey group_uid);
		bool GroupBusyCheckBool(int sheduleKey, IList groups);
		bool GroupBusyCheckBoolRestrict(int sheduleKey, IList groups);
		GroupBusyCheckerState GroupBusyCheck(int sheduleKey, ExtObjKey group_uid);
		GroupBusyCheckerState GroupBusyCheck(int sheduleKey, IList groups);
		bool TutorBusyCheckBool(int sheduleKey, ExtObjKey tutor_uid);
		bool TutorBusyCheckBoolRestrict(int sheduleKey, ExtObjKey tutor_uid);
		bool TutorBusyCheckBool(int sheduleKey, IList tutors);
		bool TutorBusyCheckBoolRestrict(int sheduleKey, IList tutors);
		TutorBusyCheckerState TutorBusyCheck(int sheduleKey, IList tutors);
		TutorBusyCheckerState TutorBusyCheck(int sheduleKey, ExtObjKey tutor_uid);

//		 Additional Checkers Method 
		bool CheckLessonCountByPair(int sheduleKey, ExtObjKey studyForm, ExtObjKey subject, int limit);
        bool CheckDeptLessonCountByPair(int sheduleKey, ExtObjKey[] studyForm, ExtObjKey[] department, int limit);
		
		// Extended function
		void RemoteGroupLessonAndLeading(ExtObjKey groupUid);
		void CreateGroupWithSimilarLeading(ExtObjKey newGroupUid, ExtObjKey similarGroupUid, ExtObjKey subjectType_uid);
        ExtObjKey[] GetPresentGroups();

		// Import - Export Restricts
		string WriteTutorRestricts();
		string WriteGroupRestricts();
		string WriteFlatRestricts();		

		void ImportRestricts(string xmlString);

		// Import - Export Suppliment
		string WriteDeptLeading(ExtObjKey dept);
		
		void ImportLeading(string xmlString); 

    }
}
