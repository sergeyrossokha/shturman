using System;
using System.Collections;
using Shturman.Shedule.Server.Remoting;

namespace Shturman.Shedule.Server.Interface
{
	/// <summary>
	/// Summary description for IShedule.
	/// </summary>
	public interface IShedule2
	{
		int GetKey(DateTime date, int pair);
		
		#region Work With StudyLeading
		int GetStudyLeadingUid();
		IList GetAllStudyLeading();		
		IList GetStudyLeadingByGroups(ExtObjKey[] groupUidList);
		int AddStudyLeading(RemoteStudyLeading2 stLeading);
		void DelStudyLeading(RemoteStudyLeading2 stLeading);
		void ChangeStudyLeading(int leading_uid, RemoteStudyLeading2 stLeading);
		RemoteStudyLeading2 GetStudyLeading(int leading_uid);
		#endregion

		bool CanInsertTo(DateTime date, int pair, RemoteLesson2 lesson);
		
		int Insert(DateTime date, int pair, RemoteLesson2 lesson);
		int SimpleInsert(DateTime date, int pair, RemoteLesson2 lesson);
		
		void DeleteByGroup(DateTime date, int pair, IList groupList );
		void DeleteByFlat(DateTime date, int pair, IList flatList );
		
		void ChangeLessonFlats(RemoteLesson2[] lessonList, ExtObjKey[] flatList);
		void MoveLesson(RemoteLesson2 lesson, DateTime date, int pair);
		
		void DelLessonsByLeading(int leadingUid);

		#region Work With Restricts
		int InsertGroupRestrict(DateTime date, int pair, ExtObjKey group_uid, RemoteRestrict2 restrict);
		int InsertTutorRestrict(DateTime date, int pair, ExtObjKey tutor_uid, RemoteRestrict2 restrict);
		int InsertFlatRestrict(DateTime date, int pair, ExtObjKey flat_uid, RemoteRestrict2 restrict);		
		
		void DeleteGroupRestrict(DateTime date, int pair, ExtObjKey group_uid);
		void DeleteTutorRestrict(DateTime date, int pair, ExtObjKey tutor_uid);
		void DeleteFlatRestrict(DateTime date, int pair, ExtObjKey flat_uid);

		IList GetTutorRestricts(ExtObjKey tutor_uid);
		bool IsTutorHasRestricts(ExtObjKey tutor_uid, DateTime date, int pair);
		string GetTutorRestrictsText(ExtObjKey tutor_uid, DateTime date, int pair);

		IList GetGroupRestricts(ExtObjKey group_uid);
		bool IsGroupHasRestricts(ExtObjKey group_uid, DateTime date, int pair);
		string GetGroupRestrictsText(ExtObjKey group_uid, DateTime date, int pair);

		IList GetFlatRestricts(ExtObjKey flat_uid);
		bool IsFlatHasRestricts(ExtObjKey flat_uid, DateTime date, int pair);		
		string GetFlatRestrictsText(ExtObjKey flat_uid, DateTime date, int pair);
		#endregion
		
		bool TutorHasLesson(ExtObjKey TutorUID, DateTime date, int pair);
		RemoteLesson2 GetTutorLesson(ExtObjKey tutor_uid, DateTime date, int pair);
		IList GetTutorLessons(ExtObjKey tutor_uid);

		bool GroupHasLesson(ExtObjKey GroupUID, DateTime date, int pair);
		RemoteLesson2 GetGroupLesson(ExtObjKey group_uid, DateTime date, int pair);
		IList GetGroupLessons(ExtObjKey group_uid);

		bool FlatHasLesson(ExtObjKey FlatUID, DateTime date, int pair);
		IList GetFlatLesson(ExtObjKey flat_uid, DateTime date, int pair);
		IList GetFlatLessons(ExtObjKey flat_uid);

		IList GetLessons(ExtObjKey subject_uid, ExtObjKey subjectType_uid);

		int[] GetKeyTutorLessons(ExtObjKey TutorUID);
		int[] GetKeyGroupLessons(ExtObjKey GroupUID);
		int[] GetKeyFlatLessons(ExtObjKey FlatUID);
		
		int[] GetKeyTutorRestricts(ExtObjKey TutorUID);
		int[] GetKeyGroupRestricts(ExtObjKey GroupUID);
		int[] GetKeyFlatRestricts(ExtObjKey FlatUID);
		
		int[] GetKeyTutorBusy(ExtObjKey TutorUID);
		int[] GetKeyGroupBusy(ExtObjKey GroupUID);
		int[] GetKeyFlatBusy(ExtObjKey FlatUID);

		// Info Methods
		bool GetIsSaved();
		void SetIsSaved(bool savedFlag);
		string GetSheduleTitle();
		void SetSheduleTitle(string title);
		string GetFileName();
		void SetFileName(string fileName);
		DateTime GetSheduleBeginDate();
		void SetSheduleBeginDate(DateTime beginDate);
		DateTime GetSheduleEndDate();
		void SetSheduleEndDate(DateTime sheduleEndDate);
		int GetWeeksCount();
		int GetDaysPerWeekCount();
		int GetPairsPerDayCount();

        object[] GetSheduleInfo();

		#region Flat Checkers
		bool FlatBusyCheckBool(DateTime date, int pair, ExtObjKey flat_uid);
		bool FlatBusyCheckBoolRestrict(DateTime date, int pair, ExtObjKey flat_uid);
		bool FlatBusyCheckBool(DateTime date, int pair, IList flats);
		bool FlatBusyCheckBoolRestrict(DateTime date, int pair, IList flats);
		FlatBusyCheckerState FlatBusyCheck(DateTime date, int pair, ExtObjKey flat_uid);
		FlatBusyCheckerState FlatBusyCheck(DateTime date, int pair, IList flats);
		#endregion

		#region Group Checkers
		bool GroupBusyCheckBool(DateTime date, int pair, ExtObjKey group_uid);
		bool GroupBusyCheckBoolRestrict(DateTime date, int pair, ExtObjKey group_uid);
		bool GroupBusyCheckBool(DateTime date, int pair, IList groups);
		bool GroupBusyCheckBoolRestrict(DateTime date, int pair, IList groups);
		GroupBusyCheckerState GroupBusyCheck(DateTime date, int pair, ExtObjKey group_uid);
		GroupBusyCheckerState GroupBusyCheck(DateTime date, int pair, IList groups);
		#endregion

		#region Tutor Checkers
		bool TutorBusyCheckBool(DateTime date, int pair, ExtObjKey tutor_uid);
		bool TutorBusyCheckBoolRestrict(DateTime date, int pair, ExtObjKey tutor_uid);
		bool TutorBusyCheckBool(DateTime date, int pair, IList tutors);
		bool TutorBusyCheckBoolRestrict(DateTime date, int pair, IList tutors);
		TutorBusyCheckerState TutorBusyCheck(DateTime date, int pair, IList tutors);
		TutorBusyCheckerState TutorBusyCheck(DateTime date, int pair, ExtObjKey tutor_uid);
		#endregion

		//
		IList GetLessons(DateTime date, int pair);

//		 Additional Checkers Method 
		bool CheckLessonCountByPair(DateTime date, int pair, ExtObjKey studyForm, ExtObjKey subject, int limit);
		bool CheckDeptLessonCountByPair(DateTime date, int pair, ExtObjKey studyForm, ExtObjKey department, int limit);
	}
}
