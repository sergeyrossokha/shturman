﻿// Система унифицированного ввода данных
// Система хранения объектов (db4o)
// Remoting
using System;
using System.Collections;
using System.Configuration;
using System.Reflection;
using Db4objects.Db4o;
using Db4objects.Db4o.Ext;
using Db4objects.Db4o.Query;
using db4o.mytools;
using Shturman.Nestor.Interfaces;
using Shturman.Shedule.Objects;
using System.Collections.Generic;

namespace Shturman.Shedule.Objects.Container
{
    /// <summary>
    /// Summary description for ObjectFactory.
    /// </summary>
    [Serializable]
    public class DB4OBridgeRem : MarshalByRefObject, IObjectStorage
    {
        public static readonly DB4OBridgeRem theOneObject = new DB4OBridgeRem();

        /// <summary>
        /// Контейнер объектов 
        /// </summary>
        private IObjectContainer objectContainer;
        private Hashtable cachedObjects = new Hashtable();

        /// <summary>
        /// Конструктор класса реализующего интерфейс IObjectFactory
        /// </summary>
        private DB4OBridgeRem()
        {
            Db4oFactory.Configure().DiscardFreeSpace(25);

            Db4oFactory.Configure().CallConstructors(true);

            // recommended setting for large database files is 8
            Db4oFactory.Configure().BlockSize(8);
            /// Устанавливает максимальное значение для генерируемых идентификаторов
            Db4oFactory.Configure().GenerateUUIDs(Int32.MaxValue);
            Db4oFactory.Configure().GenerateVersionNumbers(Int32.MaxValue);
            /// Устанавливаем глубину обновления объекта, при его изменении
            /// необходимо для обновления значений во вложенных классах, массивах и т.д. 
            Db4oFactory.Configure().UpdateDepth(3);

            Db4oFactory.Configure().DetectSchemaChanges(true);
            /// Данный параметр обязателен так как при значении по умолчанию = 5, прак
            /// тически не выполняются запросы к БД, а точнее по моим догадкам происходит зацикливание
            /// при активации в графе объектов
            Db4oFactory.Configure().ActivationDepth(3);

            Db4oFactory.Configure().Freespace().UseRamSystem();
        }

        public void Open()
        {
            /// Подключение к базе данных
            objectContainer = Db4oFactory.OpenClient(
            ConfigurationSettings.AppSettings.Get("Server"),
            int.Parse(ConfigurationSettings.AppSettings.Get("ServerPort")),
            ConfigurationSettings.AppSettings.Get("ServerUser"),
            ConfigurationSettings.AppSettings.Get("ServerPwd"));
        }

        /// <summary>
        /// Возвращает указатель на объект реализующий интерфейс IObjectFactory
        /// </summary>
        /// <returns>Объект реализующий IObjectFactory</returns>
        public static IObjectStorage GetObjectStorage()
        {
            return theOneObject;
        }

        public object GetObjectByUUID(byte[] signature, long uuid)
        {
            Db4oUUID objectUUID = new Db4oUUID(uuid, signature);
            object currentObject = objectContainer.Ext().GetByUUID(objectUUID);
            objectContainer.Activate(currentObject, 3);
            return currentObject;
        }
        /// <summary>
        /// Метод получения объекта по его внутреннему коду
        /// </summary>
        /// <param name="objectID">Внутренний код объекта</param>
        /// <returns>Экземпляр объекта</returns>
        public object GetObjectByID(long objectID)
        {
            if (!cachedObjects.ContainsKey(objectID))
            {
                object currentObject = objectContainer.Ext().GetByID(objectID);
                objectContainer.Activate(currentObject, 3);

                cachedObjects.Add(objectID, currentObject);
                return currentObject;
            }
            else
                return cachedObjects[objectID];
        }

        /// <summary>
        /// Метод получения внутреннего кода обїекта 
        /// </summary>
        /// <param name="customObject">Объект код которого необходимо получить</param>
        /// <returns>Внутренний код объекта</returns>
        public long GetID(object customObject)
        {
            long id = objectContainer.Ext().GetID(customObject);
            return id;
        }

        public long GetUUID(object customObject, out byte[] signature, out long uuid)
        {
            long id = objectContainer.Ext().GetID(customObject);

            IObjectInfo oi = objectContainer.Ext().GetObjectInfo(customObject);

            Db4oUUID _uuid = oi.GetUUID();

            signature = _uuid.GetSignaturePart(); //objectContainer.Ext().GetObjectInfo(customObject).GetUUID().GetSignaturePart();	
            uuid = _uuid.GetLongPart(); //objectContainer.Ext().GetObjectInfo(customObject).GetUUID().GetLongPart();


            return id;
        }

        /// <summary>
        /// Метод для создания новых экземпляров класса
        /// </summary>
        /// <param name="objectType">Тип создаваемого объекта</param>
        /// <returns>Экземпляр созданного класса</returns>
        public object New(Type objectType)
        {
            object obj = Activator.CreateInstance(objectType);
            return obj;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectType"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        public object New(Type objectType, object parent)
        {
            object newObject = Activator.CreateInstance(objectType);

            foreach (FieldInfo fi in objectType.GetFields(BindingFlags.NonPublic | BindingFlags.Instance))
            {
                if (fi.FieldType == parent.GetType())
                {
                    fi.SetValue(newObject, parent);
                    return newObject;
                }
            }

            return newObject;
        }

        /// <summary>
        /// Метод для сохранения объекта в хранилище
        /// </summary>
        /// <param name="storedObject">Объект который необходимо сохранить в хранилище</param>
        public void Save(object storedObject)
        {
            // спроба уникнути збереження пустих об`єктів
            if (storedObject != null)
            {
                // добавление объекта в хранилище
                objectContainer.Set(storedObject);
                // заверщение транзакции
                objectContainer.Commit();
            }
        }

        /// <summary>
        /// Метод удаления объекта из хранилища
        /// </summary>
        /// <param name="storedObject">Объект который необходимо удалить из хранилища</param>
        public void Remove(object storedObject)
        {
            if (storedObject is StoredObject)
            {
                //object obj = this.GetObjectByID((storedObject as StoredObject).Uid);
                // удаление объекта из хранилища
                objectContainer.Delete(storedObject);
                // заверщение транзакции
                objectContainer.Commit();
            }
        }

        /// <summary>
        /// Метод возвращающий список объектов из хранилища определённого типа 
        /// </summary>
        /// <param name="listType">Тип возвращаемых объектов</param>
        /// <returns>Список объектов заданного типа</returns>
        public IList ObjectList(Type listType)
        {
            //IQuery query = objectContainer.Query();
            //query.Constrain(typeof(ObjectModel4db4o.StoredObject));
            //IList os1 = (IList)query.Execute();

            IList os = (IList)objectContainer.Get(listType);
            ArrayList lst = new ArrayList(os);


            // сортировка списка
            try
            {
                lst.Sort();
            }
            catch
            {
            }
            return lst;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="listType"></param>
        /// <param name="parent"></param>
        /// <returns></returns>
        public IList ObjectList(Type listType, object parent)
        {
            foreach (FieldInfo fi in listType.GetFields(BindingFlags.NonPublic | BindingFlags.Instance))
            {
                if (fi.FieldType == parent.GetType())
                {
                    IQuery query = objectContainer.Query();
                    query.Constrain(listType);
                    query.Descend(fi.Name).Constrain(parent).Identity();
                    ArrayList lst = new ArrayList(query.Execute());
                    lst.Sort();
                    return lst;
                }
            }
            return new ArrayList();
        }

        public IList LikeObjectList(object obj)
        {
            ArrayList lst = new ArrayList(objectContainer.Get(obj));

            //foreach (StoredObject so in lst)
            //	so.UpdateUid();

            // сортировка списка
            lst.Sort();
            return lst;
        }

        public IList QueryObjects(object predicate)
        {
            return objectContainer.Query(predicate as Predicate);
        }

        /// <summary>
        /// Закрытие файла базы данных
        /// </summary>
        public void Close()
        {
            if (this.objectContainer != null)
            {
                this.objectContainer.Close();
                this.objectContainer = null;
                //Db4oFactory.Configure().TimeoutClientSocket(0);	
            }
        }

        /// <summary>
        /// Дефрагментация и сборка мусора в базе данных 
        /// </summary>
        public void Defragmentation()
        {
            this.Close();
            new Defragment().Run(ConfigurationSettings.AppSettings["StorageFile"], true);
            this.objectContainer = Db4oFactory.OpenFile(ConfigurationSettings.AppSettings["StorageFile"]);
        }

        public IList EntityList()
        {
            Assembly currentAssembly = Assembly.GetExecutingAssembly();
            Type[] assemblyTypes = currentAssembly.GetTypes();

            ArrayList entityList = new ArrayList();

            foreach (Type elem in assemblyTypes)
            {
                if (elem.IsSubclassOf(typeof(StoredObject)) && elem.IsPublic)
                    entityList.Add(elem);
            }

            return entityList;
        }
    }
}