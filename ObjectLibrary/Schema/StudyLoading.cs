﻿// Использование подсистемы унифицированного ввода данных
using System;
using System.Collections;
using System.ComponentModel;
using System.Reflection;
using Shturman.Nestor.DataAttributes;
using Shturman.Nestor.Interfaces;

namespace Shturman.Shedule.Objects
{
    /// <summary>
    /// Класс описывающий учебную нагрузку, т.е. какие занятия необходимо 
    /// поставить в расписание и для кого. Эта информация одна для расписания 
    /// и хранится непосредственно в файле расписания.
    /// </summary>
    [EntityCaption("StudyLoading.EntityCaption")]
    [EntityCategory("StudyLoading.EntityCategory")]
    [Serializable]
    public class StudyLoading : IBizObject, ICloneable
    {
        private int uid = 0;
        private Subject subject;
        private StudyType subjectType;
        private Department department;
        private IList listOfGroups;
        private IList listOfTutors;
        private int hourByWeek = 0;
        private int usedHourByWeek = 0;
        private string note;

        public StudyLoading()
        {
            listOfGroups = new ArrayList();
            listOfTutors = new ArrayList();

            // Цепляем обработчик события
            this.PropertyValues += new PropertyValuesEventHandler(StudyLoading_OnPropertyValues);
        }

        public StudyLoading(Subject subject, StudyType subjectType, Department department, IList listOfGroups, IList listOfTutors, int hourByWeek, string note)
        {
            this.subject = subject;
            this.subjectType = subjectType;
            this.department = department;
            this.listOfGroups = listOfGroups;
            this.listOfTutors = listOfTutors;
            this.hourByWeek = hourByWeek;
            this.note = note;

            // Цепляем обработчик события
            this.PropertyValues += new PropertyValuesEventHandler(StudyLoading_OnPropertyValues);
        }

        #region Events

        public IList StudyLoading_OnPropertyValues(PropertyInfo pi)
        {
            if (pi.PropertyType == typeof(Subject))
            {
                if (this.Dept != null)
                    return this.Dept.DepartmentSubjects;
                else
                    return new ArrayList();
            }

            //if (pi.PropertyType == typeof(Department))
            //{
            //	if (this.SubjectName != null)
            //	return this.SubjectName.DepartmentSubjects;
            //	else
            //	return new ArrayList();
            //}

            if (pi.PropertyType == typeof(IList) && pi.Name == "Tutors")
            {
                if (this.Dept != null)
                    return this.Dept.DepartmentTutors;
                else
                    return null;
            }

            return new ArrayList(); //this.ObjectList(propertyType);
        }

        #endregion

        [PropertyCaption("Уникальный код")]
        [PropertyIndex(0)]
        [PropertyVisible(false)]
        [Description("Уникальный код")]
        public int Uid
        {
            get { return uid; }
            set { uid = value; }
        }

        [PropertyCaption("StudyLoading.Department.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(1)]
        [PropertyReference(typeof(Department))]
        [Description("StudyLoading.Department.Description")]
        public Department Dept
        {
            get { return department; }
            set { department = value; }
        }

        [PropertyCaption("StudyLoading.Subject.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(2)]
        [PropertyReference(typeof(Subject))]
        [Description("StudyLoading.Subject.Description")]
        public Subject SubjectName
        {
            get { return subject; }
            set { subject = value; }
        }

        [PropertyCaption("StudyLoading.StudyType.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(3)]
        [PropertyReference(typeof(StudyType))]
        [Description("StudyLoading.StudyType.Description")]
        public StudyType StudyForm
        {
            get { return subjectType; }
            set { subjectType = value; }
        }

        [PropertyCaption("StudyLoading.Groups.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(4)]
        [Description("StudyLoading.Groups.Description")]
        [PropertyBrowserDetail(typeof(Group), true)]
        [PropertyInsertListItem(InsertListItemMode.imSelectItem)]
        [PropertyRemoveListItem(RemoveListItemMode.rmRemoveListItem)]
        public IList Groups
        {
            get { return listOfGroups; }
            set { listOfGroups = value; }
        }

        [PropertyCaption("StudyLoading.Tutors.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(5)]
        [Description("StudyLoading.Tutors.Description")]
        [PropertyBrowserDetail(typeof(Tutor), true)]
        [PropertyInsertListItem(InsertListItemMode.imSelectItem)]
        [PropertyRemoveListItem(RemoveListItemMode.rmRemoveListItem)]
        public IList Tutors
        {
            get { return listOfTutors; }
            set { listOfTutors = value; }
        }

        [PropertyCaption("StudyLoading.HourByWeek.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(6)]
        [Description("StudyLoading.HourByWeek.Description")]
        public int HourByWeek
        {
            get { return hourByWeek; }
            set { hourByWeek = value; }
        }

        [PropertyCaption("StudyLoading.UsedHourByWeek.Caption")]
        [PropertyVisible(true)]
        [PropertyReadOnly(true)]
        [PropertyIndex(7)]
        [Description("StudyLoading.UsedHourByWeek.Description")]
        public int UsedHourByWeek
        {
            get { return usedHourByWeek; }
            set { usedHourByWeek = value; }
        }
        [PropertyCaption("StudyLoading.Groups.Caption")]
        [PropertyVisible(true)]
        [PropertyReadOnly(true)]
        [PropertyIndex(8)]
        [Description("StudyLoading.Groups.Description")]
        public string GroupsList
        {
            get
            {
                string str = "";
                if (this.Groups != null)
                    for (int index = 0; index < this.Groups.Count; index++)
                        if (this.Groups[index] != null)
                            if (index == 0)
                                str = str + this.Groups[index].ToString();
                            else
                                str = str + ", " + this.Groups[index].ToString();
                return str;
            }
        }

        [PropertyCaption("StudyLoading.Note.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(8)]
        [Description("StudyLoading.Note.Description")]
        public string Note
        {
            get { return note; }
            set { note = value; }
        }

        public override int GetHashCode()
        {
            return this.uid;
        }

        public bool ArrayEquals(IList list1, IList list2)
        {
            bool isEquals = true;

            if (list1.Count != list2.Count)
                return false;

            foreach (object obj in list1)
                isEquals = isEquals && list2.Contains(obj);

            if (isEquals != false)
                foreach (object obj in list1)
                    isEquals = isEquals && list2.Contains(obj);

            return isEquals;

        }

        public override bool Equals(object obj)
        {
            if (obj is int)
                return (int)obj == this.uid;
            else if (obj is StudyLoading)
            {
                StudyLoading sl = (obj as StudyLoading);

                return (sl.Dept == this.Dept) && (sl.SubjectName == this.SubjectName) && sl.StudyForm.Equals(this.StudyForm) && sl.HourByWeek == this.HourByWeek && ArrayEquals(sl.Tutors, this.Tutors) && ArrayEquals(sl.Groups, this.Groups);
            }
            else
                return base.Equals(obj);
        }

        public override string ToString()
        {
            string studingLoading = "";
            if (this.SubjectName != null)
                studingLoading += this.SubjectName.ToString();
            if (this.subjectType != null)
                studingLoading += "(" + this.subjectType.ToString() + ") ";

            studingLoading += "Навантаження:" + this.HourByWeek.ToString();
            studingLoading += " Проставлено:" + this.UsedHourByWeek.ToString();

            return studingLoading;
        }

        public object Clone()
        {
            return new StudyLoading(this.subject, this.subjectType, this.department, new ArrayList(this.listOfGroups), new ArrayList(this.listOfTutors), this.hourByWeek, this.note);
        }

        #region Object Properties and Events

        public IObjectStorage Store
        {
            get { return null; }
        }

        public event EditEventHandler Edit;
        public event ChangeEventHandler Change;
        public event DeleteEventHandler Delete;

        public event ChangePropertyEventHandler ChangeProperty;
        public event PropertyValuesEventHandler PropertyValues;

        public event CreateEventHandler Create;
        public event DestroyEventHandler Destroy;

        public bool OnEdit()
        {
            if (this.Edit != null)
                return this.Edit();
            else
                return false;
        }

        public void OnChange()
        {
            if (this.Change != null)
                this.Change();
        }

        public void OnDelete()
        {
            if (this.Delete != null)
                this.Delete();
        }

        public IList OnPropertyValues(PropertyInfo pi)
        {
            if (this.PropertyValues != null)
                return this.PropertyValues(pi);
            else
                return null;
        }

        public void OnChangeProperty(PropertyInfo pi, object oldvalue, object newValue)
        {
            if (this.ChangeProperty != null)
                this.ChangeProperty(pi, oldvalue, newValue);
        }

        public void OnCreate()
        {
            if (this.Create != null)
                this.Create();
        }

        public void OnDestroy()
        {
            if (this.Destroy != null)
                this.Destroy();
        }

        #endregion
    }
}