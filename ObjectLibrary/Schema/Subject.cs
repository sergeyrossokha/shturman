﻿// Использование подсистемы унифицированного ввода данных
using System;
using System.Collections;
using System.ComponentModel;
using Shturman.Nestor.DataAttributes;

namespace Shturman.Shedule.Objects
{
    /// <summary>
    /// Объект описывающий сущность "Учебная дисциплина"
    /// </summary>
    [EntityCaption("Subject.EntityCaption")]
    [EntityCategory("Subject.EntityCategory")]
    [Serializable]
    public class Subject : StoredObject, ICloneable, IComparable
    {
        private string _mnemoCode;
        private string _name;
        private string _shortName;

        #region Конструкторы

        public Subject()
        {
            this._mnemoCode = null;
            ;
            this._name = null;
            this._shortName = null;
        }

        public Subject(string mnemoCode, string shortName, string name)
        {
            this._mnemoCode = mnemoCode;
            this._shortName = shortName;
            this._name = name;
        }

        #endregion

        #region Свойства

        /// <summary>
        /// Код объекта данный пользователем (мнемокод)
        /// </summary>
        [PropertyCaption("Subject.MnemoCode.Caption")]
        [PropertyVisible(false)]
        [PropertyIndex(0)]
        [Description("Subject.MnemoCode.Description")]
        public string MnemoCode
        {
            get { return _mnemoCode; }
            set { _mnemoCode = value; }
        }

        /// <summary>
        /// Наименование предмета
        /// </summary>
        [PropertyCaption("Subject.ShortName.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(2)]
        [Description("Subject.ShortName.Description")]
        public string ShortName
        {
            get { return _shortName; }
            set { _shortName = value; }
        }

        /// <summary>
        /// Наименование предмета
        /// </summary>
        [PropertyCaption("Subject.Name.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(1)]
        [Description("Subject.Name.Description")]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        #endregion

        public override string ToString()
        {
            return _name;
        }

        public override int CompareTo(object obj)
        {
            if (obj is Subject)
                return Comparer.Default.Compare(this.Name, (obj as Subject).Name);
            else
                return Comparer.Default.Compare(this.ToString(), obj.ToString());
        }

        public object Clone()
        {
            return new Subject(this.MnemoCode, this.ShortName, this.Name);
        }
    }

}