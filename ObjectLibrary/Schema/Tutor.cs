﻿// Использование подсистемы унифицированного ввода данных
using System;
using System.Collections;
using System.ComponentModel;
using Shturman.Nestor.DataAttributes;
using System.Collections.Generic;

namespace Shturman.Shedule.Objects
{
    /// <summary>
    /// Объект описывающий сущность "Преподаватель"
    /// </summary>
    [EntityCaption("Tutor.EntityCaption")]
    [EntityCategory("Tutor.EntityCategory")]
    [Serializable]
    public class Tutor : StoredObject, ICloneable, IComparable
    {
        private string _mnemoCode = null;
        private string _surName = null;
        private string _name = null;
        private string _patronimic = null;
        private AcademicStatus _tutorStatus = null;
        private Post _tutorPost = null;
        private IList _tutorDepartments = new ArrayList();
        private IList _tutorSubjects = new ArrayList();

        #region Конструкторы

        public Tutor()
        {
        }

        public Tutor(string mnemoCode, string surName, string name, string patronimic, AcademicStatus tutorStatus, Post tutorPost, IList tutorDepts, IList tutorSubjects)
        {
            this._mnemoCode = mnemoCode;
            this._surName = surName;
            this._name = name;
            this._patronimic = patronimic;
            this._tutorStatus = tutorStatus;
            this._tutorPost = tutorPost;
            this._tutorDepartments = tutorDepts;
            this._tutorSubjects = tutorSubjects;
        }

        #endregion

        # region Свойства

        /// <summary>
        /// Код объекта данный пользователем (мнемокод)
        /// </summary>
        [PropertyCaption("Tutor.MnemoCode.Caption")]
        [PropertyVisible(false)]
        [PropertyIndex(0)]
        [Description("Tutor.MnemoCode.Description")]
        public string MnemoCode
        {
            get { return _mnemoCode; }
            set { _mnemoCode = value; }
        }

        /// <summary>
        /// Фамилия преподавателя
        /// </summary>
        [PropertyCaption("Tutor.SurName.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(1)]
        [Description("Tutor.SurName.Description")]
        public string TutorSurname
        {
            get { return _surName; }
            set { _surName = value; }
        }

        /// <summary>
        /// Фамилия, Имя, Отчество преподавателя
        /// </summary>
        [PropertyCaption("Tutor.Name.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(2)]
        [Description("Tutor.Name.Description")]
        public string TutorName
        {
            get { return _name; }
            set { _name = value; }
        }

        /// <summary>
        /// Отчество преподавателя
        /// </summary>
        [PropertyCaption("Tutor.Patronimic.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(3)]
        [Description("Tutor.Patronimic.Description")]
        public string TutorPatronimic
        {
            get { return _patronimic; }
            set { _patronimic = value; }
        }

        /// <summary>
        /// Академическое звание преподавателя
        /// </summary>
        [PropertyCaption("Tutor.Status.Caption")]
        [PropertyVisible(false)]
        [PropertyIndex(4)]
        [PropertyReference(typeof(AcademicStatus))]
        [Description("Tutor.Status.Description")]
        public AcademicStatus TutorStatus
        {
            get { return _tutorStatus; }
            set { _tutorStatus = value; }
        }

        /// <summary>
        /// Должность занимаемая преподавателем
        /// </summary>
        [PropertyCaption("Tutor.Post.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(5)]
        [PropertyReference(typeof(Post))]
        [Description("Tutor.Post.Description")]
        public Post TutorPost
        {
            get { return _tutorPost; }
            set { _tutorPost = value; }
        }

        /// <summary>
        /// Возвращает список кафедр на которых работает преподаватель
        /// </summary>
        [PropertyCaption("Tutor.Departments.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(6)]
        [PropertyBrowserDetail(typeof(Department), true)]
        [Description("Tutor.Departments.Description")]
        [PropertyInsertListItem(InsertListItemMode.imSelectItem)]
        [PropertyRemoveListItem(RemoveListItemMode.rmRemoveListItem)]
        public IList TutorDepartments
        {
            get { return _tutorDepartments; }
        }

        /// <summary>
        /// Возвращает список предметов которые читает преподаватель
        /// </summary>
        [PropertyCaption("Tutor.Subjects.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(7)]
        [PropertyBrowserDetail(typeof(Subject), true)]
        [Description("Tutor.Subjects.Description")]
        [PropertyInsertListItem(InsertListItemMode.imSelectItem)]
        [PropertyRemoveListItem(RemoveListItemMode.rmRemoveListItem)]
        public IList TutorSubjects
        {
            get { return _tutorSubjects; }
        }

        #endregion

        public override string ToString()
        {
            string tutorstring = "";

            if (TutorStatus != null)
                tutorstring += TutorStatus.ToString() + ", ";

            if (TutorPost != null)
                tutorstring += TutorPost.ToString() + ", ";

            tutorstring += this._surName + " " + this._name + " " + this._patronimic;

            return tutorstring;
        }

        public override int CompareTo(object obj)
        {
            if (obj is Tutor)
                return Comparer.Default.Compare(this._surName + " " + this._name + ". " + this._patronimic + ".", (obj as Tutor)._surName + " " + (obj as Tutor)._name + ". " + (obj as Tutor)._patronimic + ".");
            else
                return Comparer.Default.Compare(this.ToString(), obj.ToString());
        }

        #region ICloneable Members

        public object Clone()
        {
            return new Tutor(this.MnemoCode, this.TutorSurname, this.TutorName, this.TutorPatronimic, this.TutorStatus, this.TutorPost, this.TutorDepartments, this.TutorSubjects);
        }

        #endregion
    }

}

