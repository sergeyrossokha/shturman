﻿// Сохранение данных в xml-файлы
// Использование подсистемы унифицированного ввода данных
using System;
using System.Collections;
using System.ComponentModel;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Shturman.Nestor.DataAttributes;

namespace Shturman.Shedule.Objects
{
    /// <summary>
    /// Объект описывающий сущность "Учёное звание"
    /// </summary>
    [EntityCaption("AcademicStatus.EntityCaption")]
    [EntityCategory("AcademicStatus.EntityCategory")]
    [Serializable]
    public class AcademicStatus : StoredObject, IXmlSerializable
    {
        private string _mnemoCode;
        private string _name = "";

        #region Конструкторы

        public AcademicStatus()
        {
            this._mnemoCode = null;
            this._name = null;
        }

        public AcademicStatus(string mnemoCode, string name)
        {
            this._mnemoCode = mnemoCode;
            this._name = name;
        }

        #endregion

        #region Свойства

        /// <summary>
        /// Код объекта данный пользователем (мнемокод)
        /// </summary>
        [PropertyCaption("AcademicStatus.MnemoCode.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(0)]
        [Description("AcademicStatus.MnemoCode.Description")]
        public string MnemoCode
        {
            get { return _mnemoCode; }
            set { _mnemoCode = value; }
        }

        /// <summary>
        /// Наименование учёного звания
        /// </summary>
        [PropertyCaption("AcademicStatus.Name.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(1)]
        [Description("AcademicStatus.Name.Description")]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        #endregion

        #region Implementation IXmlSerializable

        XmlSchema IXmlSerializable.GetSchema()
        {
            return null;
        }

        void IXmlSerializable.ReadXml(XmlReader reader)
        {
            reader.ReadStartElement("AcademicStatus");

            reader.ReadStartElement("MnemoCode");
            this._mnemoCode = reader.ReadString();
            reader.ReadEndElement();

            reader.ReadStartElement("Name");
            this._name = reader.ReadString();
            reader.ReadEndElement();

            reader.ReadEndElement();
        }

        void IXmlSerializable.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("MnemoCode");
            writer.WriteString(this._mnemoCode);
            writer.WriteEndElement();

            writer.WriteStartElement("Name");
            writer.WriteString(this._name);
            writer.WriteEndElement();
        }

        #endregion

        public override string ToString()
        {
            return _mnemoCode;
        }

        public override int CompareTo(object obj)
        {
            if (obj is AcademicStatus)
                return Comparer.Default.Compare(this.MnemoCode, (obj as AcademicStatus).MnemoCode);
            else
                return Comparer.Default.Compare(this.ToString(), obj.ToString());
        }
    }

}