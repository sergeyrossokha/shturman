﻿using System;
using System.Collections;
using System.ComponentModel;
using Shturman.Nestor.DataAttributes;

namespace Shturman.Shedule.Objects
{
    /// <summary>
    /// Summary description for User.
    /// </summary>
    /// <summary>
    /// Объект описывающий сущность "Учебная дисциплина"
    /// </summary>
    [EntityCaption("User.EntityCaption")]
    [EntityCategory("User.EntityCategory")]
    [Serializable]
    public class ShturmanUser : StoredObject, ICloneable, IComparable
    {
        private long _useruid;
        private string _name;
        private string _pwd;

        #region Конструкторы

        public ShturmanUser()
        {
            this._useruid = -1;
            this._name = null;
            this._pwd = null;
        }

        public ShturmanUser(long useruid, string name, string password)
        {
            this._useruid = useruid;
            this._name = name;
            this._pwd = password;
        }

        #endregion

        #region Свойства

        /// <summary>
        /// Код объекта данный пользователем (мнемокод)
        /// </summary>
        [PropertyCaption("User.Uid.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(0)]
        [Description("User.Uid.Description")]
        public long UserUid
        {
            get { return _useruid; }
            set { _useruid = value; }
        }

        /// <summary>
        /// Наименование пользователя
        /// </summary>
        [PropertyCaption("User.Name.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(1)]
        [Description("User.Name.Description")]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        /// <summary>
        /// Пароль пользователя
        /// </summary>
        [PropertyCaption("User.Password.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(2)]
        [Description("User.Password.Description")]
        public string Password
        {
            get { return _pwd; }
            set { _pwd = value; }
        }

        #endregion

        public override string ToString()
        {
            return _name;
        }

        public override int CompareTo(object obj)
        {
            if (obj is ShturmanUser)
                return Comparer.Default.Compare(this.Name, (obj as ShturmanUser).Name);
            else
                return Comparer.Default.Compare(this.ToString(), obj.ToString());
        }

        public object Clone()
        {
            return new ShturmanUser(this.Uid, this.Name, this.Password);
        }
    }
}