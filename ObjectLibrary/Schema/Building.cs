﻿// Сохранение данных в xml-файлы
// Использование подсистемы унифицированного ввода данных
using System;
using System.Collections;
using System.ComponentModel;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Shturman.Nestor.DataAttributes;

namespace Shturman.Shedule.Objects
{
    /// <summary>
    /// Объект описывающий сущность "Учебный корпус"
    /// </summary>
    [EntityCaption("Building.EntityCaption")]
    [EntityCategory("Building.EntityCategory")]
    [Serializable]
    public class Building : StoredObject, IXmlSerializable
    {
        private string _mnemoCode = null;
        private string _name = null;

        #region Конструкторы

        public Building()
        {
            this._mnemoCode = null;
            this._name = null;
        }

        public Building(string mnemoCode, string name)
        {
            this._mnemoCode = mnemoCode;
            this._name = name;
        }

        #endregion

        #region Свойства

        /// <summary>
        /// Код объекта данный пользователем (мнемокод)
        /// </summary>
        [PropertyCaption("Building.MnemoCode.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(0)]
        [Description("Building.MnemoCode.Description")]
        public string MnemoCode
        {
            get { return _mnemoCode; }
            set { _mnemoCode = value; }
        }

        /// <summary>
        /// Наименование корпуса
        /// </summary>
        [PropertyCaption("Building.Name.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(1)]
        [Description("Building.Name.Description")]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        [PropertyBrowserDetail(typeof(LectureHall))]
        [PropertyCaption("Building.LectureHalls.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(2)]
        [Description("Building.LectureHalls.Description")]
        [PropertyInsertListItem]
        [PropertyRemoveListItem]
        public IList LectureHalls
        {
            get { return Store.ObjectList(typeof(LectureHall), this); }
        }

        #endregion

        #region Implementation IXmlSerializable

        XmlSchema IXmlSerializable.GetSchema()
        {
            return null;
        }

        void IXmlSerializable.ReadXml(XmlReader reader)
        {
            reader.ReadStartElement("Building");

            reader.ReadStartElement("MnemoCode");
            this._mnemoCode = reader.ReadString();
            reader.ReadEndElement();

            reader.ReadStartElement("Name");
            this._name = reader.ReadString();
            reader.ReadEndElement();

            reader.ReadEndElement();
        }

        void IXmlSerializable.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("MnemoCode");
            writer.WriteString(this._mnemoCode);
            writer.WriteEndElement();

            writer.WriteStartElement("Name");
            writer.WriteString(this._name);
            writer.WriteEndElement();
        }

        #endregion

        public override string ToString()
        {
            return _mnemoCode;
        }

        public override int CompareTo(object obj)
        {
            if (obj is Building)
                return Comparer.Default.Compare(this.MnemoCode, (obj as Building).MnemoCode);
            else
                return Comparer.Default.Compare(this.ToString(), obj.ToString());
        }
    }
}