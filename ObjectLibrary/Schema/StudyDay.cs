﻿// Использование подсистемы унифицированного ввода данных
using System;
using System.Collections;
using System.ComponentModel;
using Shturman.Nestor.DataAttributes;

namespace Shturman.Shedule.Objects
{
    /// <summary>
    /// Объект описывающий сущность "Учебный день"
    /// </summary>
    [EntityCaption("StudyDay.EntityCaption")]
    [EntityCategory("StudyDay.EntityCategory")]
    [Serializable]
    public class StudyDay : StoredObject //, IXmlSerializable
    {
        private string _mnemoCode;
        private string _name;
        private string _shortName;

        #region Конструкторы

        public StudyDay()
        {
            this._mnemoCode = null;
            this._name = null;
            this._shortName = null;
        }

        public StudyDay(string mnemoCode, string name, string shortName)
        {
            this._mnemoCode = mnemoCode;
            this._name = name;
            this._shortName = shortName;
        }

        #endregion

        #region Свойства

        /// <summary>
        /// Код объекта данный пользователем (мнемокод)
        /// </summary>
        [PropertyCaption("StudyDay.MnemoCode.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(0)]
        [Description("StudyDay.MnemoCode.Description")]
        public string MnemoCode
        {
            get { return _mnemoCode; }
            set { _mnemoCode = value; }
        }

        /// <summary>
        /// Наименование учебного дня
        /// </summary>
        [PropertyCaption("StudyDay.Name.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(1)]
        [Description("StudyDay.Name.Description")]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        [PropertyCaption("StudyDay.ShortName.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(2)]
        [Description("StudyDay.ShortName.Description")]
        public string ShortName
        {
            get { return _shortName; }
            set { _shortName = value; }
        }

        #endregion

        #region Implementation IXmlSerializable

        /*
XmlSchema IXmlSerializable.GetSchema()	
{	
return null;
}

void IXmlSerializable.ReadXml(XmlReader reader)
{
reader.ReadStartElement("StudyDay");

reader.ReadStartElement("MNEMOCODE");
this._mnemoCode = reader.ReadString();
reader.ReadEndElement();

reader.ReadStartElement("NAME");
this._name = reader.ReadString();
reader.ReadEndElement();

reader.ReadStartElement("SHORTNAME");
this._shortName = reader.ReadString();
reader.ReadEndElement();

reader.ReadEndElement();
}

void IXmlSerializable.WriteXml(XmlWriter writer)
{	
writer.WriteStartElement("MNEMOCODE");
writer.WriteString(this._mnemoCode.ToString());
writer.WriteEndElement();

writer.WriteStartElement("NAME");
writer.WriteString(this._name.ToString());
writer.WriteEndElement();

writer.WriteStartElement("SHORTNAME");
writer.WriteString(this._shortName.ToString());
writer.WriteEndElement();
}*/

        #endregion

        public override string ToString()
        {
            return _shortName;
        }

        public override int CompareTo(object obj)
        {
            if (obj is StudyDay)
                return Comparer.Default.Compare(Int32.Parse(this.MnemoCode), Int32.Parse((obj as StudyDay).MnemoCode));
            else
                return Comparer.Default.Compare(this.ToString(), obj.ToString());
        }
    }
}