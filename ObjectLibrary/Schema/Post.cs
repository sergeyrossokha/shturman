﻿// Использование подсистемы унифицированного ввода данных
using System;
using System.Collections;
using System.ComponentModel;
using Shturman.Nestor.DataAttributes;

namespace Shturman.Shedule.Objects
{
    /// <summary>
    /// Объект описывающий сущность "Занимаемая должность"
    /// </summary>
    [EntityCaption("Post.EntityCaption")]
    [EntityCategory("Post.EntityCategory")]
    [Serializable]
    public class Post : StoredObject
    {
        private string _mnemoCode;
        private string _name = "";

        #region Конструкторы

        public Post()
        {
            this._mnemoCode = null;
            this._name = null;
        }

        public Post(string mnemoCode, string name)
        {
            this._mnemoCode = mnemoCode;
            this._name = name;
        }

        #endregion

        #region Свойства

        /// <summary>
        /// Код объекта данный пользователем (мнемокод)
        /// </summary>
        [PropertyCaption("Post.MnemoCode.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(0)]
        [Description("Post.MnemoCode.Description")]
        public string MnemoCode
        {
            get { return _mnemoCode; }
            set { _mnemoCode = value; }
        }

        /// <summary>
        /// Наименование занимаемой должности
        /// </summary>
        [PropertyCaption("Post.Name.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(1)]
        [Description("Post.Name.Description")]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        #endregion

        public override string ToString()
        {
            return _mnemoCode;
        }

        public override int CompareTo(object obj)
        {
            if (obj is Post)
                return Comparer.Default.Compare(this.MnemoCode, (obj as Post).MnemoCode);
            else
                return Comparer.Default.Compare(this.ToString(), obj.ToString());
        }
    }

}