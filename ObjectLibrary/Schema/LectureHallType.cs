﻿// Сохранение данных в xml-файлы
// Использование подсистемы унифицированного ввода данных
using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;
using Shturman.Nestor.DataAttributes;

namespace Shturman.Shedule.Objects
{
    /// <summary>
    /// Объект описывающий сущность "Тип учебной аудитории"
    /// </summary>
    [EntityCaption("LectureHallType.EntityCaption")]
    [EntityCategory("LectureHallType.EntityCategory")]
    [Serializable]
    public class LectureHallType : StoredObject, IXmlSerializable
    {
        private string _mnemoCode;
        private string _name = "";
        private int _color = 0;

        #region Конструкторы

        public LectureHallType()
        {
            this._mnemoCode = null;
            this._name = null;
            this._color = 0;
        }

        public LectureHallType(string mnemoCode, string name, int color)
        {
            this._mnemoCode = mnemoCode;
            this._name = name;
            this._color = color;
        }

        #endregion

        #region Свойства

        /// <summary>
        /// Код объекта данный пользователем (мнемокод)
        /// </summary>
        [PropertyCaption("LectureHallType.MnemoCode.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(0)]
        [Description("LectureHallType.MnemoCode.Description")]
        public string MnemoCode
        {
            get { return _mnemoCode; }
            set { _mnemoCode = value; }
        }

        /// <summary>
        /// Наименование типа аудитории
        /// </summary>
        [PropertyCaption("LectureHallType.Name.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(1)]
        [Description("LectureHallType.Name.Description")]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        [PropertyCaption("LectureHallType.Color.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(2)]
        [Description("LectureHallType.Color.Description")]
        public Color FlatColor
        {
            get { return Color.FromArgb(this._color); }
            set { this._color = value.ToArgb(); }
        }

        #endregion

        #region Implementation IXmlSerializable

        XmlSchema IXmlSerializable.GetSchema()
        {
            return null;
        }

        void IXmlSerializable.ReadXml(XmlReader reader)
        {
            reader.ReadStartElement("LectureHallType");

            reader.ReadStartElement("MnemoCode");
            this._mnemoCode = reader.ReadString();
            reader.ReadEndElement();

            reader.ReadStartElement("Name");
            this._name = reader.ReadString();
            reader.ReadEndElement();

            reader.ReadStartElement("Color");
            this._color = int.Parse(reader.ReadString());
            reader.ReadEndElement();

            reader.ReadEndElement();
        }

        void IXmlSerializable.WriteXml(XmlWriter writer)
        {
            writer.WriteStartElement("MnemoCode");
            writer.WriteString(this._mnemoCode);
            writer.WriteEndElement();

            writer.WriteStartElement("Name");
            writer.WriteString(this._name);
            writer.WriteEndElement();

            writer.WriteStartElement("Color");
            writer.WriteString(this._color.ToString());
            writer.WriteEndElement();
        }

        #endregion

        public override string ToString()
        {
            return _mnemoCode;
        }

        public override int CompareTo(object obj)
        {
            if (obj is LectureHallType)
                return Comparer.Default.Compare(this.MnemoCode, (obj as LectureHallType).MnemoCode);
            else
                return Comparer.Default.Compare(this.ToString(), obj.ToString());
        }
    }
}