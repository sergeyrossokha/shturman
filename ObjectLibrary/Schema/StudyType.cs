﻿// Использование подсистемы унифицированного ввода данных
using System;
using System.Collections;
using System.ComponentModel;
using Shturman.Nestor.DataAttributes;

namespace Shturman.Shedule.Objects
{
    /// <summary>
    /// Объект описывающий сущность "Тип занятия"
    /// </summary>
    [EntityCaption("StudyType.EntityCaption")]
    [EntityCategory("StudyType.EntityCategory")]
    [Serializable]
    public class StudyType : StoredObject, IComparable
    {
        private string _mnemoCode;
        private string _name = "";

        #region Конструкторы

        public StudyType()
        {
            this._mnemoCode = null;
            this._name = null;
        }

        public StudyType(string mnemoCode, string name)
        {
            this._mnemoCode = mnemoCode;
            this._name = name;
        }

        #endregion

        #region Свойства

        /// <summary>
        /// Код объекта данный пользователем (мнемокод)
        /// </summary>
        [PropertyCaption("StudyType.MnemoCode.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(0)]
        [Description("StudyType.MnemoCode.Description")]
        public string MnemoCode
        {
            get { return _mnemoCode; }
            set { _mnemoCode = value; }
        }

        /// <summary>
        /// Наименование типа занятия
        /// </summary>
        [PropertyCaption("StudyType.Name.Caption")]
        [PropertyVisible(true)]
        [PropertyIndex(1)]
        [Description("StudyType.Name.Description")]
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        #endregion

        public override string ToString()
        {
            return _mnemoCode;
        }

        public override int CompareTo(object obj)
        {
            if (obj is StudyType)
                return Comparer.Default.Compare(this.MnemoCode, (obj as StudyType).MnemoCode);
            else
                return Comparer.Default.Compare(this.ToString(), obj.ToString());
        }
    }
}