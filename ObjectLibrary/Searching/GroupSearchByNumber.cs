// My Assemblies
using Db4objects.Db4o.Query;
using Shturman.Shedule.Objects;

namespace Shturman.Shedule.Objects.Searching
{
	/// <summary>
	/// Summary description for GroupSearchByNumber.
	/// </summary>
	public class GroupSearchByNumber : Predicate
	{
		public enum GroupSearchType {equal, like};

		private string _groupNumber;
		private GroupSearchType _findEqual = GroupSearchType.like;

		public GroupSearchByNumber(string groupNumber)
		{
			_groupNumber = groupNumber;
		}
		
		public GroupSearchByNumber(string groupNumber, GroupSearchType findEqual):this(groupNumber)
		{			
			_findEqual = findEqual;
		}

		public bool Match(Group group)
		{
			switch(_findEqual)
			{
				case GroupSearchType.like:
					return StringFuzzyComparer.Compare(group.MnemoCode.ToUpper(), _groupNumber.ToUpper()) == 100;
				case GroupSearchType.equal:
                    return group.MnemoCode.ToUpper().Replace(" ", "").Replace("-", "") == _groupNumber.ToUpper().Replace(" ", "").Replace("-", "");					
				default:
					return false;
			}
		}
	}
}
