// My Assemblies
using Shturman.Shedule.Objects;
using Db4objects.Db4o.Query;

namespace Shturman.Shedule.Objects.Searching
{
	/// <summary>
	/// Summary description for StudyFormSearchByName.
	/// </summary>
    public class StudyTypeSearchByName : Db4objects.Db4o.Query.Predicate
	{
		private string _studyTypeName;

		public StudyTypeSearchByName(string studyTypeName)
		{
			_studyTypeName = studyTypeName;
		}

		public bool Match(StudyType studyType)
		{
			return studyType.MnemoCode == _studyTypeName;
		}
	}
}
