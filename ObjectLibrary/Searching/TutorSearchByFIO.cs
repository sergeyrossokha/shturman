using System;
using System.Text.RegularExpressions;
using Db4objects.Db4o.Query;
using Shturman.Shedule.Objects;

namespace Shturman.Shedule.Objects.Searching
{
	/// <summary>
	/// Summary description for TutorSearchByFIO.
	/// </summary>
	public class TutorSearchByFIO: Predicate
	{
		private string _tutorFIO;
        
		public TutorSearchByFIO(string tutorFIO)
		{
			_tutorFIO = tutorFIO;
            //_tutorFIOArray = tutorFIO.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
		}

		public bool Match(Tutor tutor)
		{
            if (tutor != null)
            {
                string[] _tutorFIOArray = _tutorFIO.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                bool isLike = StringFuzzyComparer.Compare(_tutorFIOArray[0], tutor.TutorSurname) >= 75;

                if (_tutorFIOArray.Length > 1)
                    isLike = isLike && ((_tutorFIOArray[1].Length == 1 ? tutor.TutorName.StartsWith(_tutorFIOArray[1]) :
                        StringFuzzyComparer.Compare(_tutorFIOArray[1], tutor.TutorName) > 75) || (tutor.TutorName.Length == 1 ? _tutorFIOArray[1].StartsWith(tutor.TutorName) :
                        StringFuzzyComparer.Compare(_tutorFIOArray[1], tutor.TutorName) > 75));

                if (_tutorFIOArray.Length > 2)
                    isLike = isLike && ((_tutorFIOArray[2].Length == 1 ? tutor.TutorPatronimic.StartsWith(_tutorFIOArray[2]) :
                       StringFuzzyComparer.Compare(_tutorFIOArray[2], tutor.TutorPatronimic) > 75) || (tutor.TutorPatronimic.Length == 1 ? _tutorFIOArray[2].StartsWith(_tutorFIOArray[2]) :
                       StringFuzzyComparer.Compare(_tutorFIOArray[2], tutor.TutorPatronimic) > 75));
                
                return isLike;

                //string schemaString = "^"+tutor.TutorSurname + "*" + tutor.TutorName + "*" + tutor.TutorPatronimic + "*";
                //Regex regex = new Regex(schemaString, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
                
                //bool isMatch = regex.IsMatch(_tutorFIO);
                //if (isMatch)
                //    return isMatch;
                //else
                //    if (StringFuzzyComparer.Compare(_tutorFIO, tutor.TutorSurname + " " + tutor.TutorName + ". " + tutor.TutorPatronimic + ".") > 75)
                //        return true;
                //    else
                //        return false;
            }
            else
                return false;
		}
	}
}
