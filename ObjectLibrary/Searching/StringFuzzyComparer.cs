﻿namespace Shturman.Shedule.Objects.Searching
{
    /// <summary>
    /// Summary description for StringFuzzyComparer.
    /// </summary>
    /// 

    /*	
    //------------------------------------------------------------------------------
    //MaxMatching - максимальная длина подстроки (достаточно 3-4)
    //strInputMatching - сравниваемая строка
    //strInputStandart - строка-образец

    // Сравнивание без учета регистра
    // if IndistinctMatching(4, "поисковая строка", "оригинальная строка - эталон") > 40 then ...
    Type
    TRetCount = packed record
    lngSubRows : Word;
    lngCountLike : Word;
    end;

    //------------------------------------------------------------------------------


    //------------------------------------------------------------------------------

    * */
    public class StringFuzzyComparer
    {

        public struct RetCount
        {
            public RetCount(int SubRows, int CountLike)
            {
                lngSubRows = SubRows;
                lngCountLike = CountLike;
            }

            public int lngSubRows;
            public int lngCountLike;
        }

        public static RetCount Matching(string StrA, string StrB, int lngLen)
        {
            RetCount TempRet = new RetCount(0, 0);
            for (int PosStrA = 1; PosStrA < (StrA.Length - lngLen + 1); PosStrA++)
            {
                string StrTempA = StrA.Substring(PosStrA, lngLen);

                for (int PosStrB = 1; PosStrB < StrB.Length - lngLen + 1; PosStrB++)
                {
                    string StrTempB = StrB.Substring(PosStrB, lngLen);
                    if (StrTempA.CompareTo(StrTempB) == 0)
                    {
                        TempRet.lngCountLike++;
                        break;
                    }
                }

                TempRet.lngSubRows++;
            }

            return TempRet;
        }

        public static int IndistinctMatching(int MaxMatching, string strInputMatching, string strInputStandart)
        {
            //если не передан какой-либо параметр, то выход
            if ((MaxMatching == 0) || (strInputMatching.Length == 0) || (strInputStandart.Length == 0))
                return 0;

            RetCount gret = new RetCount(0, 0);

            // Цикл прохода по длине сравниваемой фразы
            for (int lngCurLen = 1; lngCurLen < MaxMatching; lngCurLen++)
            {
                //Сравниваем строку A со строкой B
                RetCount tret = Matching(strInputMatching, strInputStandart, lngCurLen);
                gret.lngCountLike += tret.lngCountLike;
                gret.lngSubRows += tret.lngSubRows;
                //Сравниваем строку B со строкой A
                tret = Matching(strInputStandart, strInputMatching, lngCurLen);
                gret.lngCountLike += tret.lngCountLike;
                gret.lngSubRows += tret.lngSubRows;
            }

            if (gret.lngSubRows == 0)
            {
                return 0;
            }

            int result = (int)(((float)gret.lngCountLike / (float)gret.lngSubRows) * 100);
            return result;
        }

        public static int Compare(string string1, string string2)
        {
            return IndistinctMatching(3, string1, string2);
        }
    }
}