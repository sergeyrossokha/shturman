﻿using System;
using System.Collections;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Reflection;
using System.Runtime.Remoting;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Xml;

using Shturman.MultyLanguage;
using Shturman.Script;

using Shturman.Nestor.DataBrowser;
using Shturman.Nestor.DataEditors;
using Shturman.Nestor.DataLists;
using Shturman.Nestor.Interfaces;

using Shturman.Sherlock.TrueLogic;
using Shturman.Sherlock.Forms;

using Shturman.Shedule;
using Shturman.Shedule.Forms;
using Shturman.Shedule.AutoFill;
using Shturman.Shedule.Export;
using Shturman.Shedule.Import;
using Shturman.Shedule.Objects;
using Shturman.Shedule.Objects.Container;
using Shturman.Shedule.Server.Interface;
using Shturman.Shedule.Server.Remoting;
using Shturman.Shedule.Import.New;

using Shturman.Forms;
using Shturman.Forms.Utils;

using System.Diagnostics;
using System.Runtime.InteropServices;

using System.Drawing;
using Shturman.Pr1;
using Db4objects.Db4o;

namespace Shturman
{
    /// <summary>
    /// Summary description for Form1.
    /// </summary>
    public class MainForm : Form
    {
        #region My variables
        // Удаленный сервер расписаний
        private RemoteSheduleServer _remoteSheduleServer = null;
        // Непосредственно само расписание
        private bool isSheduleByDate = false;
        private IShedule _shedule = null;
        private IShedule2 _shedule2 = null;
        // набор скриптов
        private IList _scripts = new ArrayList();

        // Удаленная база данных
        private IObjectStorage _storage = null;

        private LogicBase logicBase = null;

        #endregion

        private IContainer components;
        private MainMenu mainMenu;
        private MenuItem menuArrangeByCascade;
        private MenuItem menuArrangeByVerticaly;
        private MenuItem menuArrangeByHorizontaly;
        private MenuItem menuAbout;
        private MenuItem menuTools;
        private MenuItem menuShedule;
        private MenuItem menuRestrict;
        private MenuItem menuImport;
        private MenuItem menuImportStudyList;
        private MenuItem menuExport;
        private MenuItem menuExportShedule;
        private MenuItem menuAutomation;
        private MenuItem menuBuildShedule;
        private MenuItem menuSheduleValue;
        private MenuItem menuKnowledge;
        private MenuItem menuKnowledgeBase;
        private MenuItem menuEducation;
        private MenuItem menuReEducation;
        private MenuItem menuMinimization;
        private MenuItem menuServiсe;
        private MenuItem menuWindows;
        private MenuItem menuArrange;
        private MenuItem menuRestrictOfTutor;
        private MenuItem menuRestrictOfGroup;
        private MenuItem menuRestrictOfRoom;
        private MenuItem menuSheduleByFeculty;
        private MenuItem menuSheduleBySpecialty;
        private MenuItem menuSheduleByDepartment;
        private MenuItem menuObjects;
        private MenuItem menuObjectHours;
        private MenuItem menuStudyDays;
        private MenuItem menuStudyCyclies;
        private MenuItem menuFaculties;
        private MenuItem menuDepartments;
        private MenuItem menuTutors;
        private MenuItem menuGroups;
        private MenuItem menuSubjects;
        private MenuItem menuRooms;
        private MenuItem menuBuildings;
        private MenuItem menuStudyTypes;
        private MenuItem menuRoomTypes;
        private MenuItem menuDegreis;
        private MenuItem menuApointments;
        private MenuItem menuHelp;
        private MenuItem menuOptions;
        private MenuItem menuSpeciality;
        private ImageList imageList;
        private MenuItem menuItem5;
        private MenuItem menuItem7;
        private MenuItem menuItem9;
        private MenuItem menuBuildSheduleLec;
        private MenuItem menuBuildShedulePr;
        private MenuItem menuBuildSheduleLab;
        private MenuItem menuBuildSheduleAll;
        private MenuItem menuStart;
        private MenuItem menuOpenShedule;
        private MenuItem menuSaveShadule;
        private MenuItem menuSaveSheduleAs;
        private MenuItem menuExit;
        private MenuItem menuSheduleByGroup;
        private MenuItem menuSheduleByFlat;
        private MenuItem menuSheduleByFaculty;
        private MenuItem menuSheduleByDept;
        private MenuItem menuSheduleProperties;
        private MenuItem menuCloseServer;
        private MenuItem menuLeading;
        private MenuItem menuViewSheduleByTutor;
        private MenuItem menuViewSheduleByGroup;
        private MenuItem menuViewSheduleByFlat;
        private MenuItem menuReports;
        private MenuItem menuReportByLesson;
        private MenuItem menuContextHelp;
        private System.Windows.Forms.MenuItem menuFlatUsingReport;
        private System.Windows.Forms.MenuItem menuNewShedule;
        private System.Windows.Forms.MenuItem menuItem2;
        private System.Windows.Forms.MenuItem menuOpenExistShedule;
        private System.Windows.Forms.MenuItem menuGroupLoading;
        private System.Windows.Forms.MenuItem menuLeading0;
        private System.Windows.Forms.MenuItem menuItemExportSheduleByTutor;
        private System.Windows.Forms.MenuItem menuItemFullFlatShedule;
        private System.Windows.Forms.MenuItem menuItemShotFlatShedule;
        private System.Windows.Forms.MenuItem menuItemScripts;
        private System.Windows.Forms.MenuItem menuNewSheduleByDate;
        private System.Windows.Forms.MenuItem menuItem4;
        private System.Windows.Forms.MenuItem menuImportSupplement;
        private System.Windows.Forms.MenuItem menuExportRestricts;
        private System.Windows.Forms.MenuItem menuExportRestrictsTutorsTutors;
        private System.Windows.Forms.MenuItem menuExportRestrictsFlats;
        private System.Windows.Forms.MenuItem menuExportRestrictsGroups;
        private System.Windows.Forms.MenuItem menuImportRestricts;
        private System.Windows.Forms.MenuItem menuExportLeading;
        private System.Windows.Forms.MenuItem menuExportLeadingDept;
        private System.Windows.Forms.MenuItem menuImportLeading;
        private System.Windows.Forms.MenuItem menuItem1;
        private System.Windows.Forms.MenuItem menuRestrictFromShdule;
        private System.Windows.Forms.MenuItem menuUsers;
        private System.Windows.Forms.MenuItem menuItem6;
        private System.Windows.Forms.MenuItem menuItem8;
        private MenuItem menuItem11;
        private MenuItem menuItem12;
        private MenuItem menuItem13;
        private StatusBar statusBar;

        #region Menu Break Lines

        private MenuItem menuBreakLine1;
        private MenuItem menuBreakLine2;
        private MenuItem menuBreakLine3;
        private MenuItem menuBreakLine4;
        private MenuItem menuBreakLine5;
        private MenuItem menuBreakLine6;
        private MenuItem menuBreakLine7;
        private MenuItem menuBreakLine8;
        private MenuItem menuBreakLine9;
        private MenuItem menuBreakLine11;
        private MenuItem menuBreakLine12;
        private MenuItem menuBreakLine13;
        private MenuItem menuBreakLine14;
        private MenuItem menuItem10;
        private MenuItem menuItem3;
        private MenuItem menuItem14;
        private MenuItem menuItemScriptEditor;
        private MenuItem menuItem16;
        private MenuItem menuBreakLine16;

        #endregion

        public MainForm()
        {
            //
            // Required for Windows Form Designer support
            //
            Splash.Message = Vocabruary.Translate("Splash.InitFramework");

            InitializeComponent();

            Splash.Message = Vocabruary.Translate("Splash.InitKB");

            logicBase = LogicBase.Open(ConfigurationManager.AppSettings["LogicBaseFile"]);

            Splash.Message = Vocabruary.Translate("Splash.InitApp");

            this.Text = Vocabruary.Translate("Title");

            PrepareControlText();

            Splash.Message = Vocabruary.Translate("Splash.InitScripts");
            // инициализация скриптов
            DirectoryInfo di = new DirectoryInfo(ConfigurationManager.AppSettings["ScriptDir"]);
            if (di.Exists)
            {
                foreach (FileInfo fi in di.GetFiles("*.ssc"))
                {
                    /* Компіляція коду класу, та додавання до скриптів вже 
                    * готовий до виконання екземпляр класу */
                    DotNetScriptCompiler compiler = new DotNetScriptCompiler();
                    string menuPath;
                    Assembly myAssembly = compiler.Compile(fi.FullName, out menuPath);

                    if (myAssembly != null)
                        foreach (Type type in myAssembly.GetTypes())
                        {
                            object obj = Activator.CreateInstance(type);
                            int scriptIndex = _scripts.Add(obj);

                            // додаємо скрипт до меню
                            if (menuPath != string.Empty)
                            {
                                string[] path = menuPath.Split('\\');
                                MenuItem mi = null;
                                for (int index = 0; index < path.Length - 1; index++)
                                {
                                    bool finded = false;
                                    string menutext = Vocabruary.Translate(path[index]);
                                    if (mi == null)
                                    {
                                        foreach (MenuItem curmi in this.mainMenu.MenuItems)
                                            if (curmi.Text == menutext)
                                            {
                                                mi = curmi;
                                                finded = true;
                                                break;
                                            }
                                    }
                                    else
                                        foreach (MenuItem curmi in mi.MenuItems)
                                            if (curmi.Text == menutext)
                                            {
                                                mi = curmi;
                                                finded = true;
                                            }

                                    if (!finded)
                                        mi = mi.MenuItems.Add(Vocabruary.Translate(path[index]));
                                }
                                mi = mi.MenuItems.Add(Vocabruary.Translate(path[path.Length - 1]));
                                mi.Tag = scriptIndex;
                                mi.Click += new EventHandler(menuItemScript_Click);
                            }
                            else
                            {
                                MenuItem menuItemScript = new MenuItem();
                                menuItemScript.Text = Vocabruary.Translate(fi.Name);
                                if (menuItemScript.Text == "")
                                    menuItemScript.Text = Path.GetFileNameWithoutExtension(fi.Name);
                                menuItemScript.Click += new EventHandler(menuItemScript_Click);
                                menuItemScript.Tag = scriptIndex;
                                menuItemScripts.MenuItems.Add(menuItemScript);
                            }
                        }
                }
            }

            Splash.Message = Vocabruary.Translate("Splash.InitEnd");

            Splash.Message = Vocabruary.Translate("Splash.InitClose");
        }

        private void PrepareControlText()
        {
            #region MultiLang Support

            this.menuStart.Text = Vocabruary.Translate("MenuStart.Title");
            this.menuOpenShedule.Text = Vocabruary.Translate("MenuStart.SheduleServerOpen");
            this.menuCloseServer.Text = Vocabruary.Translate("MenuStart.SheduleServerClose");
            this.menuSheduleProperties.Text = Vocabruary.Translate("MenuStart.SheduleProperties");
            this.menuSaveShadule.Text = Vocabruary.Translate("MenuStart.SheduleSave");
            this.menuSaveSheduleAs.Text = Vocabruary.Translate("MenuStart.SheduleSaveAs");
            this.menuExit.Text = Vocabruary.Translate("MenuStart.Exit");
            this.menuObjects.Text = Vocabruary.Translate("MenuBooks.Title");
            this.menuObjectHours.Text = Vocabruary.Translate("MenuBooks.StudyHours");
            this.menuStudyDays.Text = Vocabruary.Translate("MenuBooks.StudyDays");
            this.menuStudyCyclies.Text = Vocabruary.Translate("MenuBooks.StudyCyrcles");
            this.menuFaculties.Text = Vocabruary.Translate("MenuBooks.Faculties");
            this.menuDepartments.Text = Vocabruary.Translate("MenuBooks.Departments");
            this.menuTutors.Text = Vocabruary.Translate("MenuBooks.Tutors");
            this.menuSpeciality.Text = Vocabruary.Translate("MenuBooks.Specialyties");
            this.menuGroups.Text = Vocabruary.Translate("MenuBooks.Groups");
            this.menuSubjects.Text = Vocabruary.Translate("MenuBooks.Subjects");
            this.menuBuildings.Text = Vocabruary.Translate("MenuBooks.Buildings");
            this.menuRooms.Text = Vocabruary.Translate("MenuBooks.Flats");
            this.menuStudyTypes.Text = Vocabruary.Translate("MenuBooks.StudyTypes");
            this.menuRoomTypes.Text = Vocabruary.Translate("MenuBooks.FlatTypes");
            this.menuDegreis.Text = Vocabruary.Translate("MenuBooks.AcademicStatus");
            this.menuApointments.Text = Vocabruary.Translate("MenuBooks.Post");
            this.menuTools.Text = Vocabruary.Translate("MenuTools.Title");
            this.menuShedule.Text = Vocabruary.Translate("MenuTools.Shedule");
            this.menuSheduleByFeculty.Text = Vocabruary.Translate("MenuTools.SheduleByFaculty");
            this.menuSheduleByDepartment.Text = Vocabruary.Translate("MenuTools.SheduleByDepartment");
            this.menuSheduleBySpecialty.Text = Vocabruary.Translate("MenuTools.SheduleBySpecialty");
            this.menuSheduleByGroup.Text = Vocabruary.Translate("MenuTools.SheduleByGroup");
            this.menuRestrict.Text = Vocabruary.Translate("MenuTools.Restrict");
            this.menuRestrictOfRoom.Text = Vocabruary.Translate("MenuTools.RestrictFlats");
            this.menuRestrictOfGroup.Text = Vocabruary.Translate("MenuTools.RestrictGroups");
            this.menuRestrictOfTutor.Text = Vocabruary.Translate("MenuTools.RestrictTutors");
            this.menuImport.Text = Vocabruary.Translate("MenuTools.Import");
            //	this.menuImportData.Text = Vocabruary.Translate("MenuTools.ImportBookData");
            this.menuImportStudyList.Text = Vocabruary.Translate("MenuTools.ImportSheduleList");
            //	this.menuImportShedule.Text = Vocabruary.Translate("MenuTools.ImportShedule");
            this.menuExport.Text = Vocabruary.Translate("MenuTools.Export");
            //	this.menuExportData.Text = Vocabruary.Translate("MenuTools.ExportBookData");
            //	this.menuExportStudy.Text = Vocabruary.Translate("MenuTools.ExportSheduleList");
            this.menuExportShedule.Text = Vocabruary.Translate("MenuTools.ExportShedule");
            this.menuSheduleByFaculty.Text = Vocabruary.Translate("MenuTools.ExportSheduleByFaculty");
            this.menuSheduleByDept.Text = Vocabruary.Translate("MenuTools.ExportSheduleByDepartment");
            this.menuSheduleByFlat.Text = Vocabruary.Translate("MenuTools.ExportSheduleByFlat");
            this.menuItemShotFlatShedule.Text = Vocabruary.Translate("MenuTools.ExportSheduleByFlat.Shot");
            this.menuItemFullFlatShedule.Text = Vocabruary.Translate("MenuTools.ExportSheduleByFlat.Full");
            this.menuAutomation.Text = Vocabruary.Translate("MenuTools.Automate");
            this.menuBuildShedule.Text = Vocabruary.Translate("MenuTools.AutomateShedule");
            this.menuBuildSheduleLec.Text = Vocabruary.Translate("MenuTools.AutomateShedule.Lec");
            this.menuBuildShedulePr.Text = Vocabruary.Translate("MenuTools.AutomateShedule.Pr");
            this.menuBuildSheduleLab.Text = Vocabruary.Translate("MenuTools.AutomateShedule.Lab");
            this.menuBuildSheduleAll.Text = Vocabruary.Translate("MenuTools.AutomateShedule.All");
            this.menuSheduleValue.Text = Vocabruary.Translate("MenuTools.AutomateSheduleQuality");
            this.menuLeading.Text = Vocabruary.Translate("MenuTools.SheduleLeading");
            this.menuLeading0.Text = Vocabruary.Translate("MenuTools.SheduleLeading0");
            this.menuKnowledge.Text = Vocabruary.Translate("MenuKnowledge.Title");
            this.menuKnowledgeBase.Text = Vocabruary.Translate("MenuKnowledge.Base");
            this.menuEducation.Text = Vocabruary.Translate("MenuKnowledge.BaseLearning");
            this.menuReEducation.Text = Vocabruary.Translate("MenuKnowledge.BaseReLearning");
            this.menuMinimization.Text = Vocabruary.Translate("MenuKnowledge.BaseMinimize");
            this.menuServiсe.Text = Vocabruary.Translate("MenuService.Title");
            this.menuOptions.Text = Vocabruary.Translate("MenuService.Parameters");
            this.menuItemScripts.Text = Vocabruary.Translate("MenuAddIn.Title");

            this.menuItem5.Text = Vocabruary.Translate("MenuService.DataBase");
            this.menuItem7.Text = Vocabruary.Translate("MenuService.DataBaseDefrag");
            this.menuWindows.Text = Vocabruary.Translate("MenuWindows.Title");
            this.menuArrange.Text = Vocabruary.Translate("MenuWindows.Arrange");
            this.menuArrangeByCascade.Text = Vocabruary.Translate("MenuWindows.ArrangeCascade");
            this.menuArrangeByVerticaly.Text = Vocabruary.Translate("MenuWindows.ArrangeVertical");
            this.menuArrangeByHorizontaly.Text = Vocabruary.Translate("MenuWindows.ArrangeHorizontal");
            this.menuHelp.Text = Vocabruary.Translate("MenuHelp.Title");
            this.menuContextHelp.Text = Vocabruary.Translate("MenuHelp.Help");
            this.menuAbout.Text = Vocabruary.Translate("MenuHelp.About");
            this.menuItem9.Text = Vocabruary.Translate("MenuTools.ViewShedule");
            this.menuViewSheduleByTutor.Text = Vocabruary.Translate("MenuTools.ViewSheduleByTutor");
            this.menuViewSheduleByGroup.Text = Vocabruary.Translate("MenuTools.ViewSheduleByGroup");
            this.menuViewSheduleByFlat.Text = Vocabruary.Translate("MenuTools.ViewSheduleByFlat");
            #endregion
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.mainMenu = new System.Windows.Forms.MainMenu(this.components);
            this.menuStart = new System.Windows.Forms.MenuItem();
            this.menuOpenShedule = new System.Windows.Forms.MenuItem();
            this.menuCloseServer = new System.Windows.Forms.MenuItem();
            this.menuItem2 = new System.Windows.Forms.MenuItem();
            this.menuNewShedule = new System.Windows.Forms.MenuItem();
            this.menuNewSheduleByDate = new System.Windows.Forms.MenuItem();
            this.menuItem4 = new System.Windows.Forms.MenuItem();
            this.menuOpenExistShedule = new System.Windows.Forms.MenuItem();
            this.menuBreakLine1 = new System.Windows.Forms.MenuItem();
            this.menuSaveShadule = new System.Windows.Forms.MenuItem();
            this.menuSaveSheduleAs = new System.Windows.Forms.MenuItem();
            this.menuBreakLine2 = new System.Windows.Forms.MenuItem();
            this.menuSheduleProperties = new System.Windows.Forms.MenuItem();
            this.menuBreakLine3 = new System.Windows.Forms.MenuItem();
            this.menuExit = new System.Windows.Forms.MenuItem();
            this.menuObjects = new System.Windows.Forms.MenuItem();
            this.menuObjectHours = new System.Windows.Forms.MenuItem();
            this.menuStudyDays = new System.Windows.Forms.MenuItem();
            this.menuStudyCyclies = new System.Windows.Forms.MenuItem();
            this.menuBreakLine4 = new System.Windows.Forms.MenuItem();
            this.menuFaculties = new System.Windows.Forms.MenuItem();
            this.menuDepartments = new System.Windows.Forms.MenuItem();
            this.menuSpeciality = new System.Windows.Forms.MenuItem();
            this.menuGroups = new System.Windows.Forms.MenuItem();
            this.menuSubjects = new System.Windows.Forms.MenuItem();
            this.menuBreakLine5 = new System.Windows.Forms.MenuItem();
            this.menuBuildings = new System.Windows.Forms.MenuItem();
            this.menuRooms = new System.Windows.Forms.MenuItem();
            this.menuBreakLine6 = new System.Windows.Forms.MenuItem();
            this.menuStudyTypes = new System.Windows.Forms.MenuItem();
            this.menuRoomTypes = new System.Windows.Forms.MenuItem();
            this.menuBreakLine7 = new System.Windows.Forms.MenuItem();
            this.menuDegreis = new System.Windows.Forms.MenuItem();
            this.menuApointments = new System.Windows.Forms.MenuItem();
            this.menuItem14 = new System.Windows.Forms.MenuItem();
            this.menuItem3 = new System.Windows.Forms.MenuItem();
            this.menuTutors = new System.Windows.Forms.MenuItem();
            this.menuTools = new System.Windows.Forms.MenuItem();
            this.menuShedule = new System.Windows.Forms.MenuItem();
            this.menuLeading = new System.Windows.Forms.MenuItem();
            this.menuLeading0 = new System.Windows.Forms.MenuItem();
            this.menuBreakLine8 = new System.Windows.Forms.MenuItem();
            this.menuSheduleByFeculty = new System.Windows.Forms.MenuItem();
            this.menuSheduleByDepartment = new System.Windows.Forms.MenuItem();
            this.menuSheduleBySpecialty = new System.Windows.Forms.MenuItem();
            this.menuSheduleByGroup = new System.Windows.Forms.MenuItem();
            this.menuRestrict = new System.Windows.Forms.MenuItem();
            this.menuRestrictOfRoom = new System.Windows.Forms.MenuItem();
            this.menuRestrictOfGroup = new System.Windows.Forms.MenuItem();
            this.menuRestrictOfTutor = new System.Windows.Forms.MenuItem();
            this.menuBreakLine9 = new System.Windows.Forms.MenuItem();
            this.menuItem9 = new System.Windows.Forms.MenuItem();
            this.menuViewSheduleByTutor = new System.Windows.Forms.MenuItem();
            this.menuViewSheduleByGroup = new System.Windows.Forms.MenuItem();
            this.menuViewSheduleByFlat = new System.Windows.Forms.MenuItem();
            this.menuReports = new System.Windows.Forms.MenuItem();
            this.menuReportByLesson = new System.Windows.Forms.MenuItem();
            this.menuFlatUsingReport = new System.Windows.Forms.MenuItem();
            this.menuGroupLoading = new System.Windows.Forms.MenuItem();
            this.menuBreakLine11 = new System.Windows.Forms.MenuItem();
            this.menuImport = new System.Windows.Forms.MenuItem();
            this.menuImportStudyList = new System.Windows.Forms.MenuItem();
            this.menuImportSupplement = new System.Windows.Forms.MenuItem();
            this.menuImportRestricts = new System.Windows.Forms.MenuItem();
            this.menuImportLeading = new System.Windows.Forms.MenuItem();
            this.menuItem1 = new System.Windows.Forms.MenuItem();
            this.menuRestrictFromShdule = new System.Windows.Forms.MenuItem();
            this.menuExport = new System.Windows.Forms.MenuItem();
            this.menuExportShedule = new System.Windows.Forms.MenuItem();
            this.menuSheduleByFaculty = new System.Windows.Forms.MenuItem();
            this.menuSheduleByDept = new System.Windows.Forms.MenuItem();
            this.menuSheduleByFlat = new System.Windows.Forms.MenuItem();
            this.menuItemShotFlatShedule = new System.Windows.Forms.MenuItem();
            this.menuItemFullFlatShedule = new System.Windows.Forms.MenuItem();
            this.menuItemExportSheduleByTutor = new System.Windows.Forms.MenuItem();
            this.menuExportRestricts = new System.Windows.Forms.MenuItem();
            this.menuExportRestrictsTutorsTutors = new System.Windows.Forms.MenuItem();
            this.menuExportRestrictsFlats = new System.Windows.Forms.MenuItem();
            this.menuExportRestrictsGroups = new System.Windows.Forms.MenuItem();
            this.menuExportLeading = new System.Windows.Forms.MenuItem();
            this.menuExportLeadingDept = new System.Windows.Forms.MenuItem();
            this.menuBreakLine12 = new System.Windows.Forms.MenuItem();
            this.menuAutomation = new System.Windows.Forms.MenuItem();
            this.menuBuildShedule = new System.Windows.Forms.MenuItem();
            this.menuBuildSheduleLec = new System.Windows.Forms.MenuItem();
            this.menuBuildShedulePr = new System.Windows.Forms.MenuItem();
            this.menuBuildSheduleLab = new System.Windows.Forms.MenuItem();
            this.menuBreakLine13 = new System.Windows.Forms.MenuItem();
            this.menuBuildSheduleAll = new System.Windows.Forms.MenuItem();
            this.menuSheduleValue = new System.Windows.Forms.MenuItem();
            this.menuItem6 = new System.Windows.Forms.MenuItem();
            this.menuItem8 = new System.Windows.Forms.MenuItem();
            this.menuItemScripts = new System.Windows.Forms.MenuItem();
            this.menuItemScriptEditor = new System.Windows.Forms.MenuItem();
            this.menuItem16 = new System.Windows.Forms.MenuItem();
            this.menuKnowledge = new System.Windows.Forms.MenuItem();
            this.menuKnowledgeBase = new System.Windows.Forms.MenuItem();
            this.menuBreakLine14 = new System.Windows.Forms.MenuItem();
            this.menuEducation = new System.Windows.Forms.MenuItem();
            this.menuReEducation = new System.Windows.Forms.MenuItem();
            this.menuMinimization = new System.Windows.Forms.MenuItem();
            this.menuServiсe = new System.Windows.Forms.MenuItem();
            this.menuOptions = new System.Windows.Forms.MenuItem();
            this.menuItem5 = new System.Windows.Forms.MenuItem();
            this.menuItem7 = new System.Windows.Forms.MenuItem();
            this.menuItem11 = new System.Windows.Forms.MenuItem();
            this.menuItem12 = new System.Windows.Forms.MenuItem();
            this.menuItem13 = new System.Windows.Forms.MenuItem();
            this.menuItem10 = new System.Windows.Forms.MenuItem();
            this.menuUsers = new System.Windows.Forms.MenuItem();
            this.menuWindows = new System.Windows.Forms.MenuItem();
            this.menuArrange = new System.Windows.Forms.MenuItem();
            this.menuArrangeByCascade = new System.Windows.Forms.MenuItem();
            this.menuArrangeByVerticaly = new System.Windows.Forms.MenuItem();
            this.menuArrangeByHorizontaly = new System.Windows.Forms.MenuItem();
            this.menuHelp = new System.Windows.Forms.MenuItem();
            this.menuContextHelp = new System.Windows.Forms.MenuItem();
            this.menuBreakLine16 = new System.Windows.Forms.MenuItem();
            this.menuAbout = new System.Windows.Forms.MenuItem();
            this.imageList = new System.Windows.Forms.ImageList(this.components);
            this.statusBar = new System.Windows.Forms.StatusBar();
            this.SuspendLayout();
            // 
            // mainMenu
            // 
            this.mainMenu.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuStart,
this.menuObjects,
this.menuTools,
this.menuItemScripts,
this.menuKnowledge,
this.menuServiсe,
this.menuWindows,
this.menuHelp});
            // 
            // menuStart
            // 
            this.menuStart.Index = 0;
            this.menuStart.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuOpenShedule,
this.menuCloseServer,
this.menuItem2,
this.menuNewShedule,
this.menuOpenExistShedule,
this.menuBreakLine1,
this.menuSaveShadule,
this.menuSaveSheduleAs,
this.menuBreakLine2,
this.menuSheduleProperties,
this.menuBreakLine3,
this.menuExit});
            this.menuStart.Text = "Старт";
            // 
            // menuOpenShedule
            // 
            this.menuOpenShedule.Index = 0;
            this.menuOpenShedule.Shortcut = System.Windows.Forms.Shortcut.CtrlO;
            this.menuOpenShedule.Text = "Открыть";
            this.menuOpenShedule.Click += new System.EventHandler(this.menuOpenShedule_Click);
            // 
            // menuCloseServer
            // 
            this.menuCloseServer.Enabled = false;
            this.menuCloseServer.Index = 1;
            this.menuCloseServer.Text = "Закрить";
            this.menuCloseServer.Click += new System.EventHandler(this.menuCloseServer_Click);
            // 
            // menuItem2
            // 
            this.menuItem2.Index = 2;
            this.menuItem2.Text = "-";
            // 
            // menuNewShedule
            // 
            this.menuNewShedule.Enabled = false;
            this.menuNewShedule.Index = 3;
            this.menuNewShedule.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuNewSheduleByDate,
this.menuItem4});
            this.menuNewShedule.Text = "Створити";
            this.menuNewShedule.Click += new System.EventHandler(this.menuNewShedule_Click);
            // 
            // menuNewSheduleByDate
            // 
            this.menuNewSheduleByDate.Index = 0;
            this.menuNewSheduleByDate.Text = "за датами";
            this.menuNewSheduleByDate.Click += new System.EventHandler(this.menuNewSheduleByDate_Click);
            // 
            // menuItem4
            // 
            this.menuItem4.Index = 1;
            this.menuItem4.Text = "по тижням";
            this.menuItem4.Click += new System.EventHandler(this.menuNewShedule_Click);
            // 
            // menuOpenExistShedule
            // 
            this.menuOpenExistShedule.Enabled = false;
            this.menuOpenExistShedule.Index = 4;
            this.menuOpenExistShedule.Text = "Відкрити";
            this.menuOpenExistShedule.Click += new System.EventHandler(this.menuOpenExistShedule_Click);
            // 
            // menuBreakLine1
            // 
            this.menuBreakLine1.Index = 5;
            this.menuBreakLine1.Text = "-";
            // 
            // menuSaveShadule
            // 
            this.menuSaveShadule.Enabled = false;
            this.menuSaveShadule.Index = 6;
            this.menuSaveShadule.Shortcut = System.Windows.Forms.Shortcut.CtrlS;
            this.menuSaveShadule.Text = "Сохранить";
            this.menuSaveShadule.Click += new System.EventHandler(this.menuSaveShadule_Click);
            // 
            // menuSaveSheduleAs
            // 
            this.menuSaveSheduleAs.Enabled = false;
            this.menuSaveSheduleAs.Index = 7;
            this.menuSaveSheduleAs.Text = "Сохранить как";
            this.menuSaveSheduleAs.Click += new System.EventHandler(this.menuSaveSheduleAs_Click);
            // 
            // menuBreakLine2
            // 
            this.menuBreakLine2.Index = 8;
            this.menuBreakLine2.Text = "-";
            // 
            // menuSheduleProperties
            // 
            this.menuSheduleProperties.Enabled = false;
            this.menuSheduleProperties.Index = 9;
            this.menuSheduleProperties.Text = "Свойства";
            this.menuSheduleProperties.Click += new System.EventHandler(this.menuSheduleProperties_Click);
            // 
            // menuBreakLine3
            // 
            this.menuBreakLine3.Index = 10;
            this.menuBreakLine3.Text = "-";
            // 
            // menuExit
            // 
            this.menuExit.Index = 11;
            this.menuExit.Shortcut = System.Windows.Forms.Shortcut.CtrlX;
            this.menuExit.Text = "Выход";
            this.menuExit.Click += new System.EventHandler(this.menuExit_Click);
            // 
            // menuObjects
            // 
            this.menuObjects.Enabled = false;
            this.menuObjects.Index = 1;
            this.menuObjects.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuObjectHours,
this.menuStudyDays,
this.menuStudyCyclies,
this.menuBreakLine4,
this.menuFaculties,
this.menuDepartments,
this.menuSpeciality,
this.menuGroups,
this.menuSubjects,
this.menuBreakLine5,
this.menuBuildings,
this.menuRooms,
this.menuBreakLine6,
this.menuStudyTypes,
this.menuRoomTypes,
this.menuBreakLine7,
this.menuDegreis,
this.menuApointments,
this.menuItem14,
this.menuItem3});
            this.menuObjects.Text = "Справочники";
            // 
            // menuObjectHours
            // 
            this.menuObjectHours.Index = 0;
            this.menuObjectHours.Text = "Учебные часы";
            this.menuObjectHours.Click += new System.EventHandler(this.menuObjectHours_Click);
            // 
            // menuStudyDays
            // 
            this.menuStudyDays.Index = 1;
            this.menuStudyDays.Text = "Учебные дни";
            this.menuStudyDays.Click += new System.EventHandler(this.menuStudyDays_Click);
            // 
            // menuStudyCyclies
            // 
            this.menuStudyCyclies.Index = 2;
            this.menuStudyCyclies.Text = "Учебные циклы";
            this.menuStudyCyclies.Click += new System.EventHandler(this.menuStudyCyclies_Click);
            // 
            // menuBreakLine4
            // 
            this.menuBreakLine4.Index = 3;
            this.menuBreakLine4.Text = "-";
            // 
            // menuFaculties
            // 
            this.menuFaculties.Index = 4;
            this.menuFaculties.Text = "Факультеты";
            this.menuFaculties.Click += new System.EventHandler(this.menuFaculties_Click);
            // 
            // menuDepartments
            // 
            this.menuDepartments.Index = 5;
            this.menuDepartments.Text = "Кафедры";
            this.menuDepartments.Click += new System.EventHandler(this.menuDepartments_Click);
            // 
            // menuSpeciality
            // 
            this.menuSpeciality.Index = 6;
            this.menuSpeciality.Text = "Специальности";
            this.menuSpeciality.Click += new System.EventHandler(this.menuSpeciality_Click);
            // 
            // menuGroups
            // 
            this.menuGroups.Index = 7;
            this.menuGroups.Text = "Группы";
            this.menuGroups.Click += new System.EventHandler(this.menuGroups_Click);
            // 
            // menuSubjects
            // 
            this.menuSubjects.Index = 8;
            this.menuSubjects.Text = "Предметы";
            this.menuSubjects.Click += new System.EventHandler(this.menuSubjects_Click);
            // 
            // menuBreakLine5
            // 
            this.menuBreakLine5.Index = 9;
            this.menuBreakLine5.Text = "-";
            // 
            // menuBuildings
            // 
            this.menuBuildings.Index = 10;
            this.menuBuildings.Text = "Корпуса";
            this.menuBuildings.Click += new System.EventHandler(this.menuBuildings_Click);
            // 
            // menuRooms
            // 
            this.menuRooms.Index = 11;
            this.menuRooms.Text = "Аудитории";
            this.menuRooms.Click += new System.EventHandler(this.menuRooms_Click);
            // 
            // menuBreakLine6
            // 
            this.menuBreakLine6.Index = 12;
            this.menuBreakLine6.Text = "-";
            // 
            // menuStudyTypes
            // 
            this.menuStudyTypes.Index = 13;
            this.menuStudyTypes.Text = "Типы занятий";
            this.menuStudyTypes.Click += new System.EventHandler(this.menuStudyTypes_Click);
            // 
            // menuRoomTypes
            // 
            this.menuRoomTypes.Index = 14;
            this.menuRoomTypes.Text = "Типы аудиторий";
            this.menuRoomTypes.Click += new System.EventHandler(this.menuRoomTypes_Click);
            // 
            // menuBreakLine7
            // 
            this.menuBreakLine7.Index = 15;
            this.menuBreakLine7.Text = "-";
            // 
            // menuDegreis
            // 
            this.menuDegreis.Index = 16;
            this.menuDegreis.Text = "Звания";
            this.menuDegreis.Click += new System.EventHandler(this.menuDegreis_Click);
            // 
            // menuApointments
            // 
            this.menuApointments.Index = 17;
            this.menuApointments.Text = "Должности";
            this.menuApointments.Click += new System.EventHandler(this.menuApointments_Click);
            // 
            // menuItem14
            // 
            this.menuItem14.Index = 18;
            this.menuItem14.Text = "-";
            // 
            // menuItem3
            // 
            this.menuItem3.Index = 19;
            this.menuItem3.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuTutors});
            this.menuItem3.Text = "Додатково";
            // 
            // menuTutors
            // 
            this.menuTutors.Index = 0;
            this.menuTutors.Text = "Преподаватели";
            this.menuTutors.Click += new System.EventHandler(this.menuTutors_Click);
            // 
            // menuTools
            // 
            this.menuTools.Enabled = false;
            this.menuTools.Index = 2;
            this.menuTools.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuShedule,
this.menuRestrict,
this.menuBreakLine9,
this.menuItem9,
this.menuReports,
this.menuBreakLine11,
this.menuImport,
this.menuExport,
this.menuBreakLine12,
this.menuAutomation,
this.menuItem6,
this.menuItem8});
            this.menuTools.Text = "Средства";
            // 
            // menuShedule
            // 
            this.menuShedule.Index = 0;
            this.menuShedule.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuLeading,
this.menuLeading0,
this.menuBreakLine8,
this.menuSheduleByFeculty,
this.menuSheduleByDepartment,
this.menuSheduleBySpecialty,
this.menuSheduleByGroup});
            this.menuShedule.Text = "Расписание";
            // 
            // menuLeading
            // 
            this.menuLeading.Index = 0;
            this.menuLeading.Shortcut = System.Windows.Forms.Shortcut.ShiftF3;
            this.menuLeading.Text = "Нагрузка";
            this.menuLeading.Click += new System.EventHandler(this.menuLeading_Click);
            // 
            // menuLeading0
            // 
            this.menuLeading0.Index = 1;
            this.menuLeading0.Text = "1";
            this.menuLeading0.Click += new System.EventHandler(this.menuItem4_Click);
            // 
            // menuBreakLine8
            // 
            this.menuBreakLine8.Index = 2;
            this.menuBreakLine8.Text = "-";
            // 
            // menuSheduleByFeculty
            // 
            this.menuSheduleByFeculty.Index = 3;
            this.menuSheduleByFeculty.Shortcut = System.Windows.Forms.Shortcut.Ctrl1;
            this.menuSheduleByFeculty.Text = "по факультету";
            this.menuSheduleByFeculty.Click += new System.EventHandler(this.menuSheduleByFeculty_Click);
            // 
            // menuSheduleByDepartment
            // 
            this.menuSheduleByDepartment.Index = 4;
            this.menuSheduleByDepartment.Shortcut = System.Windows.Forms.Shortcut.Ctrl2;
            this.menuSheduleByDepartment.Text = "по кафедре";
            this.menuSheduleByDepartment.Click += new System.EventHandler(this.menuSheduleByDepartment_Click);
            // 
            // menuSheduleBySpecialty
            // 
            this.menuSheduleBySpecialty.Index = 5;
            this.menuSheduleBySpecialty.Shortcut = System.Windows.Forms.Shortcut.Ctrl3;
            this.menuSheduleBySpecialty.Text = "по специальности";
            this.menuSheduleBySpecialty.Click += new System.EventHandler(this.menuSheduleBySpecialty_Click);
            // 
            // menuSheduleByGroup
            // 
            this.menuSheduleByGroup.Index = 6;
            this.menuSheduleByGroup.Shortcut = System.Windows.Forms.Shortcut.Ctrl5;
            this.menuSheduleByGroup.Text = "по группе(-ах)";
            this.menuSheduleByGroup.Click += new System.EventHandler(this.menuSheduleByGroup_Click);
            // 
            // menuRestrict
            // 
            this.menuRestrict.Index = 1;
            this.menuRestrict.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuRestrictOfRoom,
this.menuRestrictOfGroup,
this.menuRestrictOfTutor});
            this.menuRestrict.Text = "Ограничения";
            // 
            // menuRestrictOfRoom
            // 
            this.menuRestrictOfRoom.Index = 0;
            this.menuRestrictOfRoom.Shortcut = System.Windows.Forms.Shortcut.ShiftF4;
            this.menuRestrictOfRoom.Text = "Аудиторий";
            this.menuRestrictOfRoom.Click += new System.EventHandler(this.menuRestrictOfRoom_Click);
            // 
            // menuRestrictOfGroup
            // 
            this.menuRestrictOfGroup.Index = 1;
            this.menuRestrictOfGroup.Shortcut = System.Windows.Forms.Shortcut.ShiftF5;
            this.menuRestrictOfGroup.Text = "Групп";
            this.menuRestrictOfGroup.Click += new System.EventHandler(this.menuRestrictOfGroup_Click);
            // 
            // menuRestrictOfTutor
            // 
            this.menuRestrictOfTutor.Index = 2;
            this.menuRestrictOfTutor.Shortcut = System.Windows.Forms.Shortcut.ShiftF6;
            this.menuRestrictOfTutor.Text = "Преподавателей";
            this.menuRestrictOfTutor.Click += new System.EventHandler(this.menuRestrictOfTutor_Click);
            // 
            // menuBreakLine9
            // 
            this.menuBreakLine9.Index = 2;
            this.menuBreakLine9.Text = "-";
            // 
            // menuItem9
            // 
            this.menuItem9.Index = 3;
            this.menuItem9.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuViewSheduleByTutor,
this.menuViewSheduleByGroup,
this.menuViewSheduleByFlat});
            this.menuItem9.Text = "Просмотреть";
            // 
            // menuViewSheduleByTutor
            // 
            this.menuViewSheduleByTutor.Index = 0;
            this.menuViewSheduleByTutor.Text = "Расписание преподавателя";
            this.menuViewSheduleByTutor.Click += new System.EventHandler(this.menuViewSheduleByTutor_Click);
            // 
            // menuViewSheduleByGroup
            // 
            this.menuViewSheduleByGroup.Index = 1;
            this.menuViewSheduleByGroup.Text = "Расписание группы";
            this.menuViewSheduleByGroup.Click += new System.EventHandler(this.menuViewSheduleByGroup_Click);
            // 
            // menuViewSheduleByFlat
            // 
            this.menuViewSheduleByFlat.Index = 2;
            this.menuViewSheduleByFlat.Text = "Расписание аудитории";
            this.menuViewSheduleByFlat.Click += new System.EventHandler(this.menuViewSheduleByFlat_Click);
            // 
            // menuReports
            // 
            this.menuReports.Index = 4;
            this.menuReports.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuReportByLesson,
this.menuFlatUsingReport,
this.menuGroupLoading});
            this.menuReports.Text = "Звіти";
            // 
            // menuReportByLesson
            // 
            this.menuReportByLesson.Index = 0;
            this.menuReportByLesson.Text = "Що? Де? Коли?";
            this.menuReportByLesson.Click += new System.EventHandler(this.menuReportByLesson_Click);
            // 
            // menuFlatUsingReport
            // 
            this.menuFlatUsingReport.Index = 1;
            this.menuFlatUsingReport.Text = "Використання аудиторій";
            this.menuFlatUsingReport.Click += new System.EventHandler(this.menuFlatUsingReport_Click);
            // 
            // menuGroupLoading
            // 
            this.menuGroupLoading.Index = 2;
            this.menuGroupLoading.Text = "Навантаження груп(и)";
            this.menuGroupLoading.Click += new System.EventHandler(this.menuGroupLoading_Click);
            // 
            // menuBreakLine11
            // 
            this.menuBreakLine11.Index = 5;
            this.menuBreakLine11.Text = "-";
            // 
            // menuImport
            // 
            this.menuImport.Index = 6;
            this.menuImport.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuImportStudyList,
this.menuImportSupplement,
this.menuImportRestricts,
this.menuImportLeading,
this.menuItem1,
this.menuRestrictFromShdule});
            this.menuImport.Text = "Импорт";
            // 
            // menuImportStudyList
            // 
            this.menuImportStudyList.Index = 0;
            this.menuImportStudyList.Text = "Триместровий план";
            this.menuImportStudyList.Click += new System.EventHandler(this.menuImportStudyList_Click);
            // 
            // menuImportSupplement
            // 
            this.menuImportSupplement.Index = 1;
            this.menuImportSupplement.Text = "Додатку до плану";
            this.menuImportSupplement.Click += new System.EventHandler(this.menuImportSupplement_Click);
            // 
            // menuImportRestricts
            // 
            this.menuImportRestricts.Index = 2;
            this.menuImportRestricts.Text = "Обмеження";
            this.menuImportRestricts.Click += new System.EventHandler(this.menuImportRestricts_Click);
            // 
            // menuImportLeading
            // 
            this.menuImportLeading.Index = 3;
            this.menuImportLeading.Text = "Навантажень";
            this.menuImportLeading.Click += new System.EventHandler(this.menuImportLeading_Click);
            // 
            // menuItem1
            // 
            this.menuItem1.Index = 4;
            this.menuItem1.Text = "-";
            // 
            // menuRestrictFromShdule
            // 
            this.menuRestrictFromShdule.Index = 5;
            this.menuRestrictFromShdule.Text = "Обмежень з розкладу";
            this.menuRestrictFromShdule.Click += new System.EventHandler(this.menuRestrictFromShdule_Click);
            // 
            // menuExport
            // 
            this.menuExport.Index = 7;
            this.menuExport.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuExportShedule,
this.menuExportRestricts,
this.menuExportLeading});
            this.menuExport.Text = "Экспорт";
            // 
            // menuExportShedule
            // 
            this.menuExportShedule.Index = 0;
            this.menuExportShedule.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuSheduleByFaculty,
this.menuSheduleByDept,
this.menuSheduleByFlat,
this.menuItemExportSheduleByTutor});
            this.menuExportShedule.Text = "Расписания";
            this.menuExportShedule.Click += new System.EventHandler(this.menuExportShedule_Click);
            // 
            // menuSheduleByFaculty
            // 
            this.menuSheduleByFaculty.Index = 0;
            this.menuSheduleByFaculty.Text = "по факультету";
            this.menuSheduleByFaculty.Click += new System.EventHandler(this.menuSheduleByFaculty_Click);
            // 
            // menuSheduleByDept
            // 
            this.menuSheduleByDept.Index = 1;
            this.menuSheduleByDept.Text = "по кафедре";
            this.menuSheduleByDept.Click += new System.EventHandler(this.menuSheduleByDept_Click);
            // 
            // menuSheduleByFlat
            // 
            this.menuSheduleByFlat.Index = 2;
            this.menuSheduleByFlat.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuItemShotFlatShedule,
this.menuItemFullFlatShedule});
            this.menuSheduleByFlat.Text = "по аудиторіям";
            // 
            // menuItemShotFlatShedule
            // 
            this.menuItemShotFlatShedule.Index = 0;
            this.menuItemShotFlatShedule.Text = "скорочений";
            this.menuItemShotFlatShedule.Click += new System.EventHandler(this.menuItemShotFlatShedule_Click);
            // 
            // menuItemFullFlatShedule
            // 
            this.menuItemFullFlatShedule.Index = 1;
            this.menuItemFullFlatShedule.Text = "повний";
            this.menuItemFullFlatShedule.Click += new System.EventHandler(this.menuItemFullFlatShedule_Click);
            // 
            // menuItemExportSheduleByTutor
            // 
            this.menuItemExportSheduleByTutor.Index = 3;
            this.menuItemExportSheduleByTutor.Text = "по викладачам";
            this.menuItemExportSheduleByTutor.Click += new System.EventHandler(this.menuItemExportSheduleByTutor_Click);
            // 
            // menuExportRestricts
            // 
            this.menuExportRestricts.Index = 1;
            this.menuExportRestricts.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuExportRestrictsTutorsTutors,
this.menuExportRestrictsFlats,
this.menuExportRestrictsGroups});
            this.menuExportRestricts.Text = "Обмежень";
            // 
            // menuExportRestrictsTutorsTutors
            // 
            this.menuExportRestrictsTutorsTutors.Index = 0;
            this.menuExportRestrictsTutorsTutors.Text = "Викладачів";
            this.menuExportRestrictsTutorsTutors.Click += new System.EventHandler(this.menuExportRestrictsTutorsTutors_Click);
            // 
            // menuExportRestrictsFlats
            // 
            this.menuExportRestrictsFlats.Index = 1;
            this.menuExportRestrictsFlats.Text = "Аудиторій";
            this.menuExportRestrictsFlats.Click += new System.EventHandler(this.menuExportRestrictsFlats_Click);
            // 
            // menuExportRestrictsGroups
            // 
            this.menuExportRestrictsGroups.Index = 2;
            this.menuExportRestrictsGroups.Text = "Груп";
            this.menuExportRestrictsGroups.Click += new System.EventHandler(this.menuExportRestrictsGroups_Click);
            // 
            // menuExportLeading
            // 
            this.menuExportLeading.Index = 2;
            this.menuExportLeading.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuExportLeadingDept});
            this.menuExportLeading.Text = "Навантажень";
            // 
            // menuExportLeadingDept
            // 
            this.menuExportLeadingDept.Index = 0;
            this.menuExportLeadingDept.Text = "по кафедрі";
            this.menuExportLeadingDept.Click += new System.EventHandler(this.menuExportLeadingDept_Click);
            // 
            // menuBreakLine12
            // 
            this.menuBreakLine12.Index = 8;
            this.menuBreakLine12.Text = "-";
            // 
            // menuAutomation
            // 
            this.menuAutomation.Index = 9;
            this.menuAutomation.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuBuildShedule,
this.menuSheduleValue});
            this.menuAutomation.Text = "Автоматизация";
            // 
            // menuBuildShedule
            // 
            this.menuBuildShedule.Index = 0;
            this.menuBuildShedule.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuBuildSheduleLec,
this.menuBuildShedulePr,
this.menuBuildSheduleLab,
this.menuBreakLine13,
this.menuBuildSheduleAll});
            this.menuBuildShedule.Text = "Составить расписание";
            // 
            // menuBuildSheduleLec
            // 
            this.menuBuildSheduleLec.Index = 0;
            this.menuBuildSheduleLec.Text = "лекции";
            this.menuBuildSheduleLec.Click += new System.EventHandler(this.menuBuildSheduleLec_Click);
            // 
            // menuBuildShedulePr
            // 
            this.menuBuildShedulePr.Index = 1;
            this.menuBuildShedulePr.Text = "практики";
            this.menuBuildShedulePr.Click += new System.EventHandler(this.menuBuildShedulePr_Click);
            // 
            // menuBuildSheduleLab
            // 
            this.menuBuildSheduleLab.Index = 2;
            this.menuBuildSheduleLab.Text = "лабораторные";
            this.menuBuildSheduleLab.Click += new System.EventHandler(this.menuBuildSheduleLab_Click);
            // 
            // menuBreakLine13
            // 
            this.menuBreakLine13.Index = 3;
            this.menuBreakLine13.Text = "-";
            // 
            // menuBuildSheduleAll
            // 
            this.menuBuildSheduleAll.Index = 4;
            this.menuBuildSheduleAll.Text = "Все";
            this.menuBuildSheduleAll.Click += new System.EventHandler(this.menuBuildSheduleAll_Click);
            // 
            // menuSheduleValue
            // 
            this.menuSheduleValue.Index = 1;
            this.menuSheduleValue.Text = "Оценить расписание";
            this.menuSheduleValue.Click += new System.EventHandler(this.menuSheduleValue_Click);
            // 
            // menuItem6
            // 
            this.menuItem6.Index = 10;
            this.menuItem6.Text = "-";
            // 
            // menuItem8
            // 
            this.menuItem8.Index = 11;
            this.menuItem8.Text = "Перенесення розкладу";
            this.menuItem8.Click += new System.EventHandler(this.menuItem8_Click);
            // 
            // menuItemScripts
            // 
            this.menuItemScripts.Enabled = false;
            this.menuItemScripts.Index = 3;
            this.menuItemScripts.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuItemScriptEditor,
this.menuItem16});
            this.menuItemScripts.Text = "Розширення";
            // 
            // menuItemScriptEditor
            // 
            this.menuItemScriptEditor.Index = 0;
            this.menuItemScriptEditor.Text = "Редактор скриптів";
            this.menuItemScriptEditor.Click += new System.EventHandler(this.menuItemScriptEditor_Click);
            // 
            // menuItem16
            // 
            this.menuItem16.Index = 1;
            this.menuItem16.Text = "-";
            // 
            // menuKnowledge
            // 
            this.menuKnowledge.Index = 4;
            this.menuKnowledge.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuKnowledgeBase,
this.menuBreakLine14,
this.menuEducation,
this.menuReEducation,
this.menuMinimization});
            this.menuKnowledge.Text = "Знания";
            // 
            // menuKnowledgeBase
            // 
            this.menuKnowledgeBase.Index = 0;
            this.menuKnowledgeBase.Shortcut = System.Windows.Forms.Shortcut.ShiftF7;
            this.menuKnowledgeBase.Text = "База знаний";
            this.menuKnowledgeBase.Click += new System.EventHandler(this.menuKnowledgeBase_Click);
            // 
            // menuBreakLine14
            // 
            this.menuBreakLine14.Index = 1;
            this.menuBreakLine14.Text = "-";
            // 
            // menuEducation
            // 
            this.menuEducation.Index = 2;
            this.menuEducation.Shortcut = System.Windows.Forms.Shortcut.ShiftF8;
            this.menuEducation.Text = "Обучение";
            // 
            // menuReEducation
            // 
            this.menuReEducation.Index = 3;
            this.menuReEducation.Shortcut = System.Windows.Forms.Shortcut.ShiftF9;
            this.menuReEducation.Text = "Дообучение";
            // 
            // menuMinimization
            // 
            this.menuMinimization.Index = 4;
            this.menuMinimization.Shortcut = System.Windows.Forms.Shortcut.ShiftF10;
            this.menuMinimization.Text = "Минимизация";
            // 
            // menuServiсe
            // 
            this.menuServiсe.Index = 5;
            this.menuServiсe.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuOptions,
this.menuItem5,
this.menuItem10,
this.menuUsers});
            this.menuServiсe.Text = "Сервис";
            // 
            // menuOptions
            // 
            this.menuOptions.Index = 0;
            this.menuOptions.Text = "Параметры";
            this.menuOptions.Click += new System.EventHandler(this.menuOptions_Click);
            // 
            // menuItem5
            // 
            this.menuItem5.Index = 1;
            this.menuItem5.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuItem7,
this.menuItem11,
this.menuItem12,
this.menuItem13});
            this.menuItem5.Text = "База данных";
            // 
            // menuItem7
            // 
            this.menuItem7.Index = 0;
            this.menuItem7.Text = "Дефрагментация";
            this.menuItem7.Click += new System.EventHandler(this.menuItem7_Click);
            // 
            // menuItem11
            // 
            this.menuItem11.Index = 1;
            this.menuItem11.Text = "-";
            // 
            // menuItem12
            // 
            this.menuItem12.Index = 2;
            this.menuItem12.Text = "Архівація";
            this.menuItem12.Click += new System.EventHandler(this.menuItem12_Click);
            // 
            // menuItem13
            // 
            this.menuItem13.Index = 3;
            this.menuItem13.Text = "Відновлення";
            this.menuItem13.Click += new System.EventHandler(this.menuItem13_Click);
            // 
            // menuItem10
            // 
            this.menuItem10.Index = 2;
            this.menuItem10.Text = "-";
            this.menuItem10.Visible = false;
            // 
            // menuUsers
            // 
            this.menuUsers.Index = 3;
            this.menuUsers.Text = "Користувачі";
            this.menuUsers.Visible = false;
            this.menuUsers.Click += new System.EventHandler(this.menuUsers_Click);
            // 
            // menuWindows
            // 
            this.menuWindows.Index = 6;
            this.menuWindows.MdiList = true;
            this.menuWindows.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuArrange});
            this.menuWindows.Text = "Окна";
            // 
            // menuArrange
            // 
            this.menuArrange.Index = 0;
            this.menuArrange.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuArrangeByCascade,
this.menuArrangeByVerticaly,
this.menuArrangeByHorizontaly});
            this.menuArrange.Text = "Выстроить";
            // 
            // menuArrangeByCascade
            // 
            this.menuArrangeByCascade.Index = 0;
            this.menuArrangeByCascade.Text = "Каскадом";
            this.menuArrangeByCascade.Click += new System.EventHandler(this.menuArrangeByCascade_Click);
            // 
            // menuArrangeByVerticaly
            // 
            this.menuArrangeByVerticaly.Index = 1;
            this.menuArrangeByVerticaly.Text = "Вертикально";
            this.menuArrangeByVerticaly.Click += new System.EventHandler(this.menuArrangeByVerticaly_Click);
            // 
            // menuArrangeByHorizontaly
            // 
            this.menuArrangeByHorizontaly.Index = 2;
            this.menuArrangeByHorizontaly.Text = "Горизонтально";
            this.menuArrangeByHorizontaly.Click += new System.EventHandler(this.menuArrangeByHorizontaly_Click);
            // 
            // menuHelp
            // 
            this.menuHelp.Index = 7;
            this.menuHelp.MenuItems.AddRange(new System.Windows.Forms.MenuItem[] {
this.menuContextHelp,
this.menuBreakLine16,
this.menuAbout});
            this.menuHelp.Text = "Помощь";
            // 
            // menuContextHelp
            // 
            this.menuContextHelp.Index = 0;
            this.menuContextHelp.Shortcut = System.Windows.Forms.Shortcut.F1;
            this.menuContextHelp.Text = "Помощь";
            // 
            // menuBreakLine16
            // 
            this.menuBreakLine16.Index = 1;
            this.menuBreakLine16.Text = "-";
            // 
            // menuAbout
            // 
            this.menuAbout.Index = 2;
            this.menuAbout.Shortcut = System.Windows.Forms.Shortcut.CtrlA;
            this.menuAbout.Text = "О программе";
            this.menuAbout.Click += new System.EventHandler(this.menuAbout_Click);
            // 
            // imageList
            // 
            this.imageList.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageList.ImageStream")));
            this.imageList.TransparentColor = System.Drawing.Color.Transparent;
            this.imageList.Images.SetKeyName(0, "");
            this.imageList.Images.SetKeyName(1, "");
            this.imageList.Images.SetKeyName(2, "");
            // 
            // statusBar
            // 
            this.statusBar.Location = new System.Drawing.Point(0, 363);
            this.statusBar.Name = "statusBar";
            this.statusBar.Size = new System.Drawing.Size(832, 18);
            this.statusBar.TabIndex = 1;
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(832, 381);
            this.Controls.Add(this.statusBar);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.IsMdiContainer = true;
            this.Menu = this.mainMenu;
            this.Name = "MainForm";
            this.Text = "Штурман";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Closing += new System.ComponentModel.CancelEventHandler(this.MainForm_Closing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.ResumeLayout(false);

        }

        #endregion

        [DllImport("User32.dll")]
        static extern int SetForegroundWindow(IntPtr hWnd);
        [DllImport("User32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            bool createdNew;

            using (Mutex mutex = new Mutex(false, "Shturman Singleton", out createdNew))
            {
                if (!createdNew)
                {
                    //получаем имя нашего процесса (название файла без расширения '.exe')
                    string processName = Process.GetCurrentProcess().MainModule.ModuleName;
                    processName = processName.Substring(0, processName.IndexOf(".exe"));

                    Process currentProcess = Process.GetCurrentProcess();
                    //перебираем все процессы с искомым именем
                    foreach (Process process in Process.GetProcessesByName(processName))
                    {
                        //текущий экземпляр нас не интересует
                        if (process.Id == currentProcess.Id)
                            continue;

                        //могут быть разные приложения с одинаковым именем
                        //исполняемого файла. Проверяем что-бы это был 'наш' файл
                        if (process.MainModule.FileName != currentProcess.MainModule.FileName)
                            continue;

                        //Активизируем основное окно приложения
                        SetForegroundWindow(process.MainWindowHandle);
                        ShowWindow(process.MainWindowHandle, 4); // разворачиваем окно
                        return;
                    }
                }

                Splash.Show();
                string languageStr = ConfigurationManager.AppSettings["Language"];
                new Vocabruary((NameValueCollection)ConfigurationManager.GetSection(languageStr));
                if (languageStr == "language-UA")
                {
                    //System.Globalization.CultureInfo
                    Application.CurrentCulture = new System.Globalization.CultureInfo("uk-UA");
                }

                Splash.Message = Vocabruary.Translate("Splash.InitBegin");
                // Remoting Configuration
                RemotingConfiguration.Configure("Shturman.Client.exe.config", false);

                Application.Run(new MainForm());
            }
        }

        private void menuArrangeByCascade_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.Cascade);
        }

        private void menuArrangeByVerticaly_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileVertical);
        }

        private void menuArrangeByHorizontaly_Click(object sender, EventArgs e)
        {
            this.LayoutMdi(MdiLayout.TileHorizontal);
        }


        private void menuAbout_Click(object sender, EventArgs e)
        {
            AboutForm aboutForm = new AboutForm();
            aboutForm.ShowDialog(this);
        }

        private void menuExit_Click(object sender, EventArgs e)
        {
            if (this._storage != null)
            {
                // Від'єднуюсь від бази даних	
                this._storage.Close();
                this._storage = null;
            }
            this.Close();
        }

        private void menuObjectHours_Click(object sender, EventArgs e)
        {
            // Display a wait cursor while the TreeNodes are being created.
            Cursor.Current = Cursors.WaitCursor;

            Browser browser = new Browser(_storage, typeof(StudyHour), _storage.ObjectList(typeof(StudyHour)));
            browser.MdiParent = this;
            browser.Show();

            // Reset the cursor to the default for all controls.
            Cursor.Current = Cursors.Default;
        }

        private void menuStudyDays_Click(object sender, EventArgs e)
        {
            // Display a wait cursor while the TreeNodes are being created.
            Cursor.Current = Cursors.WaitCursor;

            Browser browser = new Browser(_storage, typeof(StudyDay), _storage.ObjectList(typeof(StudyDay)));
            browser.MdiParent = this;
            browser.Show();

            // Reset the cursor to the default for all controls.
            Cursor.Current = Cursors.Default;
        }

        private void menuStudyCyclies_Click(object sender, EventArgs e)
        {
            // Display a wait cursor while the TreeNodes are being created.
            Cursor.Current = Cursors.WaitCursor;

            Browser browser = new Browser(_storage, typeof(StudyCycles), _storage.ObjectList(typeof(StudyCycles)));
            browser.MdiParent = this;
            browser.Show();

            // Reset the cursor to the default for all controls.
            Cursor.Current = Cursors.Default;
        }

        private void menuFaculties_Click(object sender, EventArgs e)
        {
            // Display a wait cursor while the TreeNodes are being created.
            Cursor.Current = Cursors.WaitCursor;

            Browser browser = new Browser(_storage, typeof(Faculty), _storage.ObjectList(typeof(Faculty)));
            browser.MdiParent = this;
            browser.Show();

            // Reset the cursor to the default for all controls.
            Cursor.Current = Cursors.Default;
        }

        private void menuDepartments_Click(object sender, EventArgs e)
        {
            // Display a wait cursor while the TreeNodes are being created.
            Cursor.Current = Cursors.WaitCursor;

            Browser browser = new Browser(_storage, typeof(Department), _storage.ObjectList(typeof(Department)));
            browser.MdiParent = this;
            browser.Show();

            // Reset the cursor to the default for all controls.
            Cursor.Current = Cursors.Default;
        }

        private void menuTutors_Click(object sender, EventArgs e)
        {
            // Display a wait cursor while the TreeNodes are being created.
            Cursor.Current = Cursors.WaitCursor;

            Browser browser = new Browser(_storage, typeof(Tutor), _storage.ObjectList(typeof(Tutor)));
            browser.MdiParent = this;
            browser.Show();

            // Reset the cursor to the default for all controls.
            Cursor.Current = Cursors.Default;
        }

        private void menuGroups_Click(object sender, EventArgs e)
        {
            // Display a wait cursor while the TreeNodes are being created.
            Cursor.Current = Cursors.WaitCursor;

            Browser browser = new Browser(_storage, typeof(Group), _storage.ObjectList(typeof(Group)));
            browser.MdiParent = this;
            browser.Show();

            // Reset the cursor to the default for all controls.
            Cursor.Current = Cursors.Default;
        }

        private void menuSpeciality_Click(object sender, EventArgs e)
        {
            // Display a wait cursor while the TreeNodes are being created.
            Cursor.Current = Cursors.WaitCursor;

            Browser browser = new Browser(_storage, typeof(Specialty), _storage.ObjectList(typeof(Specialty)));
            browser.MdiParent = this;
            browser.Show();

            // Reset the cursor to the default for all controls.
            Cursor.Current = Cursors.Default;
        }

        private void menuSubjects_Click(object sender, EventArgs e)
        {
            // Display a wait cursor while the TreeNodes are being created.
            Cursor.Current = Cursors.WaitCursor;

            Browser browser = new Browser(_storage, typeof(Subject), _storage.ObjectList(typeof(Subject)));
            browser.MdiParent = this;
            browser.Show();

            // Reset the cursor to the default for all controls.
            Cursor.Current = Cursors.Default;
        }

        private void menuBuildings_Click(object sender, EventArgs e)
        {
            // Display a wait cursor while the TreeNodes are being created.
            Cursor.Current = Cursors.WaitCursor;

            Browser browser = new Browser(_storage, typeof(Building), _storage.ObjectList(typeof(Building)));
            browser.MdiParent = this;
            browser.Show();

            // Reset the cursor to the default for all controls.
            Cursor.Current = Cursors.Default;
        }

        private void menuRooms_Click(object sender, EventArgs e)
        {
            // Display a wait cursor while the TreeNodes are being created.
            Cursor.Current = Cursors.WaitCursor;

            Browser browser = new Browser(_storage, typeof(LectureHall), _storage.ObjectList(typeof(LectureHall)));
            browser.MdiParent = this;
            browser.Show();

            // Reset the cursor to the default for all controls.
            Cursor.Current = Cursors.Default;
        }

        private void menuStudyTypes_Click(object sender, EventArgs e)
        {
            // Display a wait cursor while the TreeNodes are being created.
            Cursor.Current = Cursors.WaitCursor;

            Browser browser = new Browser(_storage, typeof(StudyType), _storage.ObjectList(typeof(StudyType)));
            browser.MdiParent = this;
            browser.Show();

            // Reset the cursor to the default for all controls.
            Cursor.Current = Cursors.Default;
        }

        private void menuRoomTypes_Click(object sender, EventArgs e)
        {
            // Display a wait cursor while the TreeNodes are being created.
            Cursor.Current = Cursors.WaitCursor;

            Browser browser = new Browser(_storage, typeof(LectureHallType), _storage.ObjectList(typeof(LectureHallType)));
            browser.MdiParent = this;
            browser.Show();

            // Reset the cursor to the default for all controls.
            Cursor.Current = Cursors.Default;
        }

        private void menuDegreis_Click(object sender, EventArgs e)
        {
            // Display a wait cursor while the TreeNodes are being created.
            Cursor.Current = Cursors.WaitCursor;

            Browser browser = new Browser(_storage, typeof(AcademicStatus), _storage.ObjectList(typeof(AcademicStatus)));
            browser.MdiParent = this;
            browser.Show();

            // Reset the cursor to the default for all controls.
            Cursor.Current = Cursors.Default;
        }

        private void menuApointments_Click(object sender, EventArgs e)
        {
            // Display a wait cursor while the TreeNodes are being created.
            Cursor.Current = Cursors.WaitCursor;

            Browser browser = new Browser(_storage, typeof(Post), _storage.ObjectList(typeof(Post)));
            browser.MdiParent = this;
            browser.Show();

            // Reset the cursor to the default for all controls.
            Cursor.Current = Cursors.Default;
        }

        private void menuSheduleByFeculty_Click(object sender, EventArgs e)
        {
            IList facultyList = _storage.ObjectList(typeof(Faculty));
            //выбор факультета
            SelectForm facultySelectForm = new SelectForm("Выбор факультета", false, facultyList);

            if (facultySelectForm.ShowDialog(this) == DialogResult.OK)
            {
                if (facultySelectForm.SelectedItem != null)
                {
                    Faculty fclt = facultySelectForm.SelectedItem as Faculty;

                    SelectForm groupSelectForm = new SelectForm("Выбор групп", true, _storage.ObjectList(typeof(Group), fclt));

                    //выбор групп
                    if (groupSelectForm.ShowDialog(this) == DialogResult.OK)
                    {
                        IList groupSelectedList = groupSelectForm.SelectedItems;
                        if (!isSheduleByDate)
                        {
                            SheduleBrowser sv = new SheduleBrowser(_storage, this._shedule, groupSelectedList);
                            sv.Text += " -> Факультет: " + fclt.Name;
                            sv.MdiParent = this;
                            sv.Show();
                        }
                        else
                        {
                            //SheduleBrowser2 sv = new SheduleBrowser2(_storage, this._shedule2, groupSelectedList);
                            //sv.Text += " -> Факультет: " + fclt.Name;
                            //sv.MdiParent = this;
                            //sv.Show();
                        }
                    }
                }
            }
        }

        private void menuSaveShadule_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            if (_remoteSheduleServer != null)
                _remoteSheduleServer.Save(this._currentShedule);

            Cursor = Cursors.Default;
        }

        private void menuSaveSheduleAs_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            SaveFileDialog svfdlg = new SaveFileDialog();

            svfdlg.Filter = "shedule files (*.shedule)|*.shedule|shedule files (*.shedule2)|*.shedule2|All files (*.*)|*.*";
            if (isSheduleByDate)
                svfdlg.DefaultExt = "shedule2";
            else
                svfdlg.DefaultExt = "shedule";

            if (svfdlg.ShowDialog() == DialogResult.OK)
            {
                if (_remoteSheduleServer != null)
                    _remoteSheduleServer.SaveAs(this._currentShedule, svfdlg.FileName);
            }

            Cursor = Cursors.Default;
        }

        private void menuOpenShedule_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.AppStarting;
            if (_storage == null)
            {
                DB4OBridgeRem.theOneObject.Open();
                _storage = DB4OBridgeRem.theOneObject;
            }

            try
            {
                Login lgn = new Login(_storage);

                if (lgn.ShowDialog() == DialogResult.OK)
                {
                    _remoteSheduleServer = new RemoteSheduleServer();

                    if (_storage != null && _remoteSheduleServer != null)
                    {
                        menuNewShedule.Enabled = menuOpenExistShedule.Enabled = menuCloseServer.Enabled = menuObjects.Enabled = true;
                        menuOpenShedule.Enabled = false;
                    }
                    // відразу пропоную вибрати розклад з яким потрібно працювати
                    menuOpenExistShedule_Click(menuOpenExistShedule, e);

                    if (lgn.User.UserUid == 255 || lgn.User.UserUid == 0)
                    {
                        menuItem3.Visible = true;
                        menuItem10.Visible = menuUsers.Visible = true;
                    }

                    CurrentSession.instance.CurrentUser = lgn.User;
                    CurrentSession.instance.ObjectStorage = _storage;
                }
            }
            catch (Exception exeption)
            {
                MessageBox.Show(this, exeption.Message, "Помилка", MessageBoxButtons.OK,
                MessageBoxIcon.Error);

            }
            Cursor = Cursors.Default;
        }

        private void OnAddStudyLoading(object studyLoading)
        {
            if ((studyLoading as StudyLoading).Uid == 0)
                if (!isSheduleByDate)
                    (studyLoading as StudyLoading).Uid = _shedule.AddStudyLeading(RemoteAdapter.Adapter.AdaptStudyLeading(studyLoading as StudyLoading));
                else
                    (studyLoading as StudyLoading).Uid = _shedule2.AddStudyLeading(RemoteAdapter.Adapter.AdaptStudyLeading2(studyLoading as StudyLoading));


            Department dept = (studyLoading as StudyLoading).Dept;
            if (dept != null)
            {
                // Сохраняем связи
                if (!dept.DepartmentSubjects.Contains((studyLoading as StudyLoading).SubjectName))
                    dept.DepartmentSubjects.Add((studyLoading as StudyLoading).SubjectName);

                foreach (Tutor tutor in (studyLoading as StudyLoading).Tutors)
                    if (!dept.DepartmentTutors.Contains(tutor))
                        dept.DepartmentTutors.Add(tutor);

                (studyLoading as StudyLoading).Dept.Store.Save(dept);
                if (!isSheduleByDate)
                    _shedule.SetIsSaved(false);
                else
                    _shedule2.SetIsSaved(false);
            }
        }

        private void OnDelStudyLoading(object studyLoading)
        {
            if ((studyLoading as StudyLoading).Uid != 0)
            {
                if (!isSheduleByDate)
                {
                    _shedule.DelStudyLeading(RemoteAdapter.Adapter.AdaptStudyLeading(studyLoading as StudyLoading));
                    _shedule.SetIsSaved(false);
                }
                else
                {
                    _shedule2.DelStudyLeading(RemoteAdapter.Adapter.AdaptStudyLeading2(studyLoading as StudyLoading));
                    _shedule2.SetIsSaved(false);
                }
            }
        }

        private void OnChangeStudyLoading(object studyLoading)
        {
            if (studyLoading is StudyLoading)
                if ((studyLoading as StudyLoading).Uid != 0)
                {
                    if (!isSheduleByDate)
                    {
                        _shedule.ChangeStudyLeading((studyLoading as StudyLoading).Uid, RemoteAdapter.Adapter.AdaptStudyLeading(studyLoading as StudyLoading));
                        _shedule.SetIsSaved(false);
                    }
                    else
                    {
                        _shedule2.ChangeStudyLeading((studyLoading as StudyLoading).Uid, RemoteAdapter.Adapter.AdaptStudyLeading2(studyLoading as StudyLoading));
                        _shedule2.SetIsSaved(false);
                    }
                }
        }

        private void menuLeading_Click(object sender, EventArgs e)
        {
            // Display a wait cursor while the TreeNodes are being created.
            Cursor.Current = Cursors.WaitCursor;

            SelectForm groupSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.GroupTitle"), true, _storage.ObjectList(typeof(Group)));

            //выбор групп
            if (groupSelectForm.ShowDialog(this) == DialogResult.OK)
            {
                // Получение нагрузки
                IList remobjList = null;
                if (!isSheduleByDate)
                {
                    ExtObjKey[] uidListArray = new ExtObjKey[groupSelectForm.SelectedItems.Count];
                    for (int index = 0; index < uidListArray.Length; index++)
                        uidListArray[index] = (groupSelectForm.SelectedItems[index] as Group).ObjectHardKey;

                    remobjList = new ArrayList(_shedule.GetStudyLeadingByGroups(uidListArray));
                }
                else
                {
                    ExtObjKey[] uidListArray = new ExtObjKey[groupSelectForm.SelectedItems.Count];
                    for (int index = 0; index < uidListArray.Length; index++)
                    {
                        uidListArray[index] = new ExtObjKey();

                        uidListArray[index].UID = _storage.GetUUID((groupSelectForm.SelectedItems[index] as Group), out uidListArray[index].Signature, out uidListArray[index].Index);
                    }

                    remobjList = new ArrayList(_shedule2.GetStudyLeadingByGroups(uidListArray));
                }

                // Адаптация нагрузки
                IList adoptobjList = RemoteAdapter.Adapter.AdaptLeadingList(remobjList);
                //	foreach (RemoteStudyLeading rsl in remobjList)
                //	{
                //	adoptobjList.Add(RemoteAdapter.Adapter.AdaptStudyLeading(rsl));
                //	}

                // Подготовка отображения формы с нагрузкой
                Browser browser = new Browser(_storage, typeof(StudyLoading), adoptobjList);
                browser.OnAddItem += new Browser.AddMasterItemEvent(OnAddStudyLoading);
                browser.OnDelItem += new Browser.DelMasterItemEvent(OnDelStudyLoading);
                browser.OnChangeItem += new Browser.ChangeMasterItemEvent(OnChangeStudyLoading);

                browser.MdiParent = this;
                browser.Show();
            }
            // Reset the cursor to the default for all controls.
            Cursor.Current = Cursors.Default;
        }

        private void MainForm_Closing(object sender, CancelEventArgs e)
        {
            if (_shedule != null)
                if (!_shedule.GetIsSaved())

                    if (MessageBox.Show(this, Vocabruary.Translate("SaveDlg.Question"),
                    Vocabruary.Translate("SaveDlg.Title"), MessageBoxButtons.YesNo,
                    MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        if (_shedule.GetFileName() != "")
                            _remoteSheduleServer.Save(this._currentShedule);
                        else
                            menuSaveSheduleAs_Click(sender, e);
                    }

            if (_shedule2 != null)
                if (!_shedule2.GetIsSaved())
                    if (MessageBox.Show(this, Vocabruary.Translate("SaveDlg.Question"), Vocabruary.Translate("SaveDlg.Title"),
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        if (_shedule2.GetFileName() != "")
                            _remoteSheduleServer.Save(this._currentShedule);
                        else
                            menuSaveSheduleAs_Click(sender, e);
                    }

            /*Необходимо отслеживать изменения в базу знаний и при выходе 
            * в случае внесения изменений спрашивать пользователя о желании
            * сохранить базу знаний
            * */
            if (logicBase.HasChanges)
                //	if (MessageBox.Show(this, Vocabruary.Translate("SaveKbDlg.Question"), Vocabruary.Translate("SaveKbDlg.Title"), MessageBoxButtons.YesNo) == DialogResult.Yes)
                logicBase.Save();

            if (MessageBox.Show(this, Vocabruary.Translate("ExitDlg.Question"), Vocabruary.Translate("ExitDlg.Title"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                if (_storage != null)
                {
                    _storage.Close();
                    _storage = null;
                }
                Application.ExitThread();
            }
            else
                e.Cancel = true;
        }

        private void menuSheduleByGroup_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            IList list = null;

            if (!isSheduleByDate)
                list = RemoteAdapter.Adapter.GetStoredObject(new ArrayList(_shedule.GetPresentGroups()));
            else
                list = new ArrayList(_storage.ObjectList(typeof(Group)));
            ArrayList.Adapter(list).Sort();
            SelectForm groupSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.GroupTitle"),
            true, /*_storage.ObjectList(typeof (Group))*/
            list);

            Cursor = Cursors.Default;
            //выбор групп
            if (groupSelectForm.ShowDialog(this) == DialogResult.OK)
            {
                Cursor = Cursors.WaitCursor;
                IList groupSelectedList = groupSelectForm.SelectedItems;
                if (!isSheduleByDate)
                {
                    SheduleBrowser sb = new SheduleBrowser(_storage, _shedule, groupSelectedList);
                    sb.MdiParent = this;
                    sb.Show();
                }
                else
                {
                    SheduleBrowser2 sv = new SheduleBrowser2(_storage, this._shedule2, groupSelectedList);
                    sv.MdiParent = this;
                    sv.Show();
                }
                Cursor = Cursors.Default;
            }
        }

        private void menuSheduleByDepartment_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            IList departmentList = _storage.ObjectList(typeof(Department));

            //выбор факультета
            SelectForm deptSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.DepartmentTitle"), false, departmentList);
            Cursor = Cursors.Default;
            if (deptSelectForm.ShowDialog(this) == DialogResult.OK)
            {
                if (deptSelectForm.SelectedItem != null)
                {
                    Cursor = Cursors.WaitCursor;
                    Department dept = deptSelectForm.SelectedItem as Department;

                    if (!isSheduleByDate)
                    {
                        SheduleBrowser sv = new SheduleBrowser(_storage, this._shedule, dept);
                        sv.Text += " -> Кафедра: " + dept.Name;
                        sv.MdiParent = this;
                        sv.Show();
                    }
                    else
                    {
                        SelectForm groupSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.GroupTitle"), true, _storage.ObjectList(typeof(Group), dept));

                        //выбор групп
                        Cursor = Cursors.Default;
                        if (groupSelectForm.ShowDialog(this) == DialogResult.OK)
                        {
                            Cursor = Cursors.WaitCursor;
                            IList groupSelectedList = groupSelectForm.SelectedItems;


                            SheduleBrowser2 sv = new SheduleBrowser2(_storage, this._shedule2, groupSelectedList);
                            sv.Text += " -> Кафедра: " + dept.Name;
                            sv.MdiParent = this;
                            sv.Show();
                        }
                    }
                    Cursor = Cursors.Default;
                }
            }
        }

        private void menuSheduleBySpecialty_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            IList specList = _storage.ObjectList(typeof(Specialty));

            //выбор факультета
            SelectForm specSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.SpecialtyTitle"), false, specList);
            Cursor = Cursors.Default;
            if (specSelectForm.ShowDialog(this) == DialogResult.OK)
            {
                Cursor = Cursors.WaitCursor;
                if (specSelectForm.SelectedItem != null)
                {
                    Specialty spec = specSelectForm.SelectedItem as Specialty;

                    SelectForm groupSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.GroupTitle"), true, _storage.ObjectList(typeof(Group), spec));

                    //выбор групп
                    Cursor = Cursors.Default;
                    if (groupSelectForm.ShowDialog(this) == DialogResult.OK)
                    {
                        Cursor = Cursors.WaitCursor;
                        IList groupSelectedList = groupSelectForm.SelectedItems;

                        if (!isSheduleByDate)
                        {
                            SheduleBrowser sv = new SheduleBrowser(_storage, this._shedule, groupSelectedList);
                            sv.Text += " -> Специальность: " + spec.Name;
                            sv.MdiParent = this;
                            sv.Show();
                        }
                        else
                        {
                            SheduleBrowser2 sv = new SheduleBrowser2(_storage, this._shedule2, groupSelectedList);
                            sv.Text += " -> Специальность: " + spec.Name;
                            sv.MdiParent = this;
                            sv.Show();
                        }
                        Cursor = Cursors.Default;
                    }
                }
            }
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            Splash.Close();

            this.Activate();
        }

        private void menuExportShedule_Click(object sender, EventArgs e)
        {
            IList facultyList = _storage.ObjectList(typeof(Faculty));
            SelectForm facultySelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.FacultyTitle"), false, facultyList);

            if (facultySelectForm.ShowDialog(this) == DialogResult.OK)
            {
                if (facultySelectForm.SelectedItem != null)
                {
                    Faculty fclt = facultySelectForm.SelectedItem as Faculty;

                    SelectForm groupSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.GroupTitle"), true, _storage.ObjectList(typeof(Group), fclt));

                    //выбор групп
                    if (groupSelectForm.ShowDialog(this) == DialogResult.OK)
                    {
                        new XLSExporter().ExportGroupsShedule(this._storage, this._shedule, groupSelectForm.SelectedItems);
                    }
                }
            }
        }

        private void menuKnowledgeBase_Click(object sender, EventArgs e)
        {
            KBConstructor KBConstructor = new KBConstructor(this._storage, this.logicBase);
            KBConstructor.MdiParent = this;
            KBConstructor.Show();
        }

        private void menuRestrictOfTutor_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            if (!isSheduleByDate)
            {
                ConstraintsBrowser constraintBrowser = new ConstraintsBrowser(this._storage, this._shedule, ConstraintsBrowserType.tutorConstraints);
                constraintBrowser.MdiParent = this;
                constraintBrowser.Show();
            }
            else
            {
                ConstraintsBrowser2 constraintBrowser = new ConstraintsBrowser2(this._storage, this._shedule2, ConstraintsBrowserType.tutorConstraints);
                constraintBrowser.MdiParent = this;
                constraintBrowser.Show();
            }
            Cursor = Cursors.Default;
        }

        private void menuRestrictOfGroup_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            if (!isSheduleByDate)
            {
                ConstraintsBrowser constraintBrowser = new ConstraintsBrowser(this._storage, this._shedule, ConstraintsBrowserType.groupConstraints);
                constraintBrowser.MdiParent = this;
                constraintBrowser.Show();
            }
            else
            {
                ConstraintsBrowser2 constraintBrowser = new ConstraintsBrowser2(this._storage, this._shedule2, ConstraintsBrowserType.groupConstraints);
                constraintBrowser.MdiParent = this;
                constraintBrowser.Show();

            }
            Cursor = Cursors.Default;
        }

        private void menuRestrictOfRoom_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            if (!isSheduleByDate)
            {
                ConstraintsBrowser constraintBrowser = new ConstraintsBrowser(this._storage, this._shedule, ConstraintsBrowserType.flatConstraints);
                constraintBrowser.MdiParent = this;
                constraintBrowser.Show();
            }
            else
            {
                ConstraintsBrowser2 constraintBrowser = new ConstraintsBrowser2(this._storage, this._shedule2, ConstraintsBrowserType.flatConstraints);
                constraintBrowser.MdiParent = this;
                constraintBrowser.Show();

            }
            Cursor = Cursors.Default;
        }

        private void menuItem7_Click(object sender, EventArgs e)
        {
            //	// Дефрагментация базы данных
            //	if (_storage != null)
            //	_storage.Defragmentation();
        }

        private void menuViewSheduleByTutor_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            /* вибір розкладу для перегляду */

            BrowseForShedule bfs = new BrowseForShedule();
            foreach (object obj in _remoteSheduleServer.GetAllShedulesInfo())
                bfs.AddSheduleInfo(obj as object[]);

            if (bfs.ShowDialog(this) == DialogResult.OK)
            {
                if (bfs.SelectedShedule.EndsWith(".shedule2"))
                {
                    IShedule2 viewShedule2 = _remoteSheduleServer.GetSheduleByName2(bfs.SelectedShedule); ;
                    SheduleView2 shvw = new SheduleView2(this._storage, viewShedule2, SheduleView2.SheduleViewType.byTutor);

                    shvw.Text = "[ " + Path.GetFileNameWithoutExtension(bfs.SelectedShedule) + " ] - ";
                    shvw.MdiParent = this;
                    shvw.Show();
                }
                else
                {
                    IShedule viewShedule = _remoteSheduleServer.GetSheduleByName(bfs.SelectedShedule); ;
                    SheduleView shvw = new SheduleView(this._storage, viewShedule, SheduleView.SheduleViewType.byTutor);

                    shvw.Text = Path.GetFileNameWithoutExtension(bfs.SelectedShedule) + " - ";
                    shvw.MdiParent = this;
                    shvw.Show();
                }
            }

            Cursor = Cursors.Default;
        }

        private void menuViewSheduleByGroup_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            /* вибір розкладу для перегляду */

            BrowseForShedule bfs = new BrowseForShedule();
            foreach (object obj in _remoteSheduleServer.GetAllShedulesInfo())
                bfs.AddSheduleInfo(obj as object[]);

            if (bfs.ShowDialog(this) == DialogResult.OK)
            {
                if (bfs.SelectedShedule.EndsWith(".shedule2"))
                {
                    IShedule2 viewShedule2 = _remoteSheduleServer.GetSheduleByName2(bfs.SelectedShedule); ;
                    SheduleView2 shvw = new SheduleView2(this._storage, viewShedule2, SheduleView2.SheduleViewType.byGroup);

                    shvw.Text = "[ " + Path.GetFileNameWithoutExtension(bfs.SelectedShedule) + " ] - ";
                    shvw.MdiParent = this;
                    shvw.Show();
                }
                else
                {
                    IShedule viewShedule = _remoteSheduleServer.GetSheduleByName(bfs.SelectedShedule); ;
                    SheduleView shvw = new SheduleView(this._storage, viewShedule, SheduleView.SheduleViewType.byGroup);

                    shvw.Text = Path.GetFileNameWithoutExtension(bfs.SelectedShedule) + " - ";
                    shvw.MdiParent = this;
                    shvw.Show();
                }
            }


            Cursor = Cursors.Default;
        }

        private void menuViewSheduleByFlat_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;
            /* Пергляд розкладу по аудиторії */
            BrowseForShedule bfs = new BrowseForShedule();
            foreach (object obj in _remoteSheduleServer.GetAllShedulesInfo())
                bfs.AddSheduleInfo(obj as object[]);

            if (bfs.ShowDialog(this) == DialogResult.OK)
            {
                if (bfs.SelectedShedule.EndsWith(".shedule2"))
                {
                    IShedule2 viewShedule2 = _remoteSheduleServer.GetSheduleByName2(bfs.SelectedShedule); ;
                    SheduleView2 shvw = new SheduleView2(this._storage, viewShedule2, SheduleView2.SheduleViewType.byFlat);

                    shvw.Text = "[ " + Path.GetFileNameWithoutExtension(bfs.SelectedShedule) + " ] - ";
                    shvw.MdiParent = this;
                    shvw.Show();
                }
                else
                {
                    IShedule viewShedule = _remoteSheduleServer.GetSheduleByName(bfs.SelectedShedule); ;
                    SheduleView shvw = new SheduleView(this._storage, viewShedule, SheduleView.SheduleViewType.byFlat);

                    shvw.Text = Path.GetFileNameWithoutExtension(bfs.SelectedShedule) + " - ";
                    shvw.MdiParent = this;
                    shvw.Show();
                }
            }
            Cursor = Cursors.Default;
        }

        private void menuBuildShedulePr_Click(object sender, EventArgs e)
        {
            if (!isSheduleByDate)
            {
                SelectForm groupSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.GroupTitle"), true, _storage.ObjectList(typeof(Group)));

                //выбор групп
                if (groupSelectForm.ShowDialog(this) == DialogResult.OK)
                {
                    IList groupSelectedList = groupSelectForm.SelectedItems;

                    /* фільтруємо записи для вибору лише тих, котрі підходять */
                    ListFiltrator lsfiltr = new ListFiltrator();
                    lsfiltr.Clear();
                    /* формуємо зміст фільтру */
                    foreach (Group gr in groupSelectedList)
                        lsfiltr.AddCondition(LogicOperations.loOR, "Groups", "", ConditionOperators.coInclude, gr);
                    IList filteredStudyLeading = lsfiltr.Filter(_shedule.GetAllStudyLeading());

                    AutoFillShedule aal = new AutoFillShedule();
                    aal.AutoArrange(_shedule, filteredStudyLeading, _storage.ObjectList(typeof(StudyDay)), _storage.ObjectList(typeof(StudyHour)), _storage.ObjectList(typeof(StudyCycles)), 2);

                    SheduleBrowser sv = new SheduleBrowser(_storage, this._shedule, groupSelectedList);
                    sv.MdiParent = this;
                    sv.Show();
                }
            }
            else
            {
                new NotImplementedException();
            }
        }

        private void menuImportStudyList_Click(object sender, EventArgs e)
        {
            if (!isSheduleByDate)
            {
                OpenFileDialog opfdlg = new OpenFileDialog();

                opfdlg.Filter = Vocabruary.Translate("FileFilters");

                if (opfdlg.ShowDialog() == DialogResult.OK)
                {
                    ImportStudyFromXML imp = new ImportStudyFromXML(this._storage);
                    imp.Import(opfdlg.FileName);

                    if (MessageBox.Show(Vocabruary.Translate("Message.UseImportedData"), Vocabruary.Translate("Message.QuestionTitle"), MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        foreach (StudyLoading stdld in imp.Studies)
                        {
                            _shedule.AddStudyLeading(RemoteAdapter.Adapter/*(this._storage)*/.AdaptStudyLeading(stdld));
                        }
                    }
                }
            }
            else
            {
                new NotImplementedException();
            }
        }

        private void menuCloseServer_Click(object sender, EventArgs e)
        {
            _remoteSheduleServer = null;
            _shedule = null;
            _shedule2 = null;

            // Змінюю заголовок вікна
            this.statusBar.Text = "";
            // Роблю не доступними пункти меню
            menuNewShedule.Enabled = menuOpenExistShedule.Enabled = menuTools.Enabled = menuSheduleProperties.Enabled = menuCloseServer.Enabled =
            menuObjects.Enabled = menuSaveShadule.Enabled = menuSaveSheduleAs.Enabled = false;
            menuOpenShedule.Enabled = menuItemScripts.Enabled = true;
        }

        #region Report By Subject
        internal class LessonComparer : IComparer
        {
            #region IComparer Members

            public int Compare(object x, object y)
            {
                Lesson xl = x as Lesson;
                Lesson yl = y as Lesson;


                int result = int.Parse(xl.CurLessonDay.MnemoCode).CompareTo(int.Parse(yl.CurLessonDay.MnemoCode));
                if (result != 0)
                    return result;
                else
                {
                    result = int.Parse(xl.CurLessonPair.MnemoCode).CompareTo(int.Parse(yl.CurLessonPair.MnemoCode));
                    if (result != 0)
                        return result;
                    else
                    {
                        result = xl.Subject.Name.CompareTo(yl.Subject.Name);
                        if (result != 0)
                            return result;
                        else
                        {
                            result = xl.GroupListString.CompareTo(yl.GroupListString);
                            if (result != 0)
                                return result;
                            else
                            {
                                result = int.Parse(xl.CurLessonWeek.MnemoCode).CompareTo(int.Parse(yl.CurLessonWeek.MnemoCode));
                                return result;
                            }
                        }
                    }
                }
            }

            #endregion
        }


        private IList Lessons = null;
        private DataGrid reportDataGrid = null;

        private void ExportLessonsSheduleBackGround()
        {
            IList dayList = this._storage.ObjectList(typeof(StudyDay));
            IList pairList = this._storage.ObjectList(typeof(StudyHour));
            IList weekList = this._storage.ObjectList(typeof(StudyCycles));

            ExcelExporter shexex = new ExcelExporter(dayList, pairList, weekList);

            ProgressThread procThread = new ProgressThread();

            procThread.MaxValue = dayList.Count * pairList.Count * weekList.Count * Lessons.Count;
            procThread.Show();
            shexex.ExportLessonsBySubject("", Lessons, new ExcelExporter.ExcelExporterCallBack(procThread.CallBack));
            procThread.Close();
        }

        private void OnExcelExportButton_Click(object sender, System.EventArgs e)
        {
            // Запускаю у фоновому режимі генерацію розкладу
            Thread backgroundThread = new Thread(new ThreadStart(this.ExportLessonsSheduleBackGround));
            backgroundThread.Start();
        }

        private void OnWordExportButton_Click(object sender, EventArgs e)
        {
            RtfReport rtfReport = new RtfReport(this);
            rtfReport.LessonsDataGrid2Rtf("", reportDataGrid);
        }

        private void menuReportByLesson_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            if (!isSheduleByDate)
            {
                IList departmentList = _storage.ObjectList(typeof(Department));

                //выбор факультета
                SelectForm deptSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.DepartmentTitle"), false, departmentList);
                Cursor = Cursors.Default;
                if (deptSelectForm.ShowDialog(this) == DialogResult.OK)
                {
                    if (deptSelectForm.SelectedItem != null)
                    {
                        Cursor = Cursors.WaitCursor;
                        Department dept = deptSelectForm.SelectedItem as Department;

                        SelectForm subjectSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.SubjectTitle"), true, dept.DepartmentSubjects);

                        //выбор предметов
                        Cursor = Cursors.Default;
                        if (subjectSelectForm.ShowDialog(this) == DialogResult.OK)
                        {
                            SelectForm studyTypeSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.StudyType"), true, _storage.ObjectList(typeof(StudyType)));
                            Cursor = Cursors.Default;
                            if (studyTypeSelectForm.ShowDialog(this) == DialogResult.OK)
                            {
                                ReportGrid ss = new ReportGrid();
                                ss.ShowInTaskbar = false;

                                #region Create data columns and grid style
                                ss.GridReport.TableStyles.Clear();

                                DataGridTextBoxColumn coloredColumn1 = new DataGridTextBoxColumn();
                                coloredColumn1.MappingName = "CurLessonDay";
                                coloredColumn1.HeaderText = "День";

                                DataGridTextBoxColumn coloredColumn2 = new DataGridTextBoxColumn();
                                coloredColumn2.MappingName = "CurLessonPair";
                                coloredColumn2.HeaderText = "Пара";

                                DataGridTextBoxColumn coloredColumn3 = new DataGridTextBoxColumn();
                                coloredColumn3.MappingName = "CurLessonWeek";
                                coloredColumn3.HeaderText = "Тиждень";

                                DataGridTextBoxColumn coloredColumn4 = new DataGridTextBoxColumn();
                                coloredColumn4.MappingName = "Subject";
                                coloredColumn4.HeaderText = "Дисціпліна";

                                DataGridTextBoxColumn coloredColumn5 = new DataGridTextBoxColumn();
                                coloredColumn5.MappingName = "SybjectType";
                                coloredColumn5.HeaderText = "Тип заняття";

                                DataGridTextBoxColumn coloredColumn6 = new DataGridTextBoxColumn();
                                coloredColumn6.MappingName = "GroupListString";
                                coloredColumn6.HeaderText = "Група(и)";

                                DataGridTextBoxColumn coloredColumn7 = new DataGridTextBoxColumn();
                                coloredColumn7.MappingName = "FlatListString";
                                coloredColumn7.HeaderText = "Приміщення";

                                DataGridTableStyle tableStyle = new DataGridTableStyle();
                                tableStyle.MappingName = typeof(ListWrapper).Name;
                                tableStyle.GridColumnStyles.AddRange(new DataGridColumnStyle[] { coloredColumn1, coloredColumn2, coloredColumn3, coloredColumn4, coloredColumn5, coloredColumn6, coloredColumn7 });

                                ss.GridReport.TableStyles.Add(tableStyle);
                                #endregion

                                IList subjectList = subjectSelectForm.SelectedItems;
                                IList studyTypeList = studyTypeSelectForm.SelectedItems;

                                ExtObjKey[] selSubj = new ExtObjKey[subjectList.Count];
                                for (int index = 0; index < subjectList.Count; index++)
                                    selSubj[index] = (subjectList[index] as Subject).ObjectHardKey;

                                ExtObjKey[] selStudType = new ExtObjKey[studyTypeList.Count];
                                for (int index = 0; index < studyTypeList.Count; index++)
                                    selStudType[index] = (studyTypeList[index] as StudyType).ObjectHardKey;

                                IList lessons = _shedule.GetLessons(selSubj, selStudType);

                                // Адаптація занять
                                for (int index = 0; index < lessons.Count; index++)
                                {
                                    lessons[index] = RemoteAdapter.Adapter.AdaptLesson(lessons[index] as RemoteLesson);
                                }

                                ArrayList.Adapter(lessons).Sort(new LessonComparer());

                                Lessons = lessons;
                                reportDataGrid = ss.GridReport;
                                try
                                {
                                    IList wrapedLst = new ListWrapper(typeof(Lesson), lessons);
                                    ss.GridReport.DataSource = wrapedLst;
                                    ss.ExportToExcelButton.Click += new EventHandler(this.OnExcelExportButton_Click);
                                    ss.ExportToWordButton.Click += new EventHandler(this.OnWordExportButton_Click);
                                    ss.ShowDialog();
                                }
                                finally
                                {
                                    Lessons = null;
                                    reportDataGrid = null;
                                }
                            }
                        }
                    }
                }
            }
            else
            {
                new NotImplementedException();
            }
        }

        #endregion

        private void menuItemShotFlatShedule_Click(object sender, EventArgs e)
        {
            if (!isSheduleByDate)
            {
                IList buildingList = _storage.ObjectList(typeof(Building));
                SelectForm buildingSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.BuildingTitle"), false, buildingList);

                if (buildingSelectForm.ShowDialog(this) == DialogResult.OK)
                {
                    if (buildingSelectForm.SelectedItem != null)
                    {
                        Building bld = buildingSelectForm.SelectedItem as Building;

                        SelectForm flatSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.FlatTitle"), true, bld.LectureHalls);

                        //выбор групп
                        if (flatSelectForm.ShowDialog(this) == DialogResult.OK)
                        {
                            new XLSExporter().ExportFlatsShedule(_storage, _shedule, flatSelectForm.SelectedItems, true);
                        }
                    }
                }
            }
            else
            {
                IList buildingList = _storage.ObjectList(typeof(Building));
                SelectForm buildingSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.BuildingTitle"), false, buildingList);

                if (buildingSelectForm.ShowDialog(this) == DialogResult.OK)
                {
                    if (buildingSelectForm.SelectedItem != null)
                    {
                        Building bld = buildingSelectForm.SelectedItem as Building;

                        SelectForm flatSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.FlatTitle"), true, bld.LectureHalls);

                        //выбор групп
                        if (flatSelectForm.ShowDialog(this) == DialogResult.OK)
                        {
                            new XLSExporter().ExportFlatsShedule2(_storage, _shedule2, flatSelectForm.SelectedItems, true);
                        }
                    }
                }
            }
        }

        private void menuItemFullFlatShedule_Click(object sender, EventArgs e)
        {
            if (!isSheduleByDate)
            {
                IList buildingList = _storage.ObjectList(typeof(Building));
                SelectForm buildingSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.BuildingTitle"), false, buildingList);

                if (buildingSelectForm.ShowDialog(this) == DialogResult.OK)
                {
                    if (buildingSelectForm.SelectedItem != null)
                    {
                        Building bld = buildingSelectForm.SelectedItem as Building;

                        SelectForm flatSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.FlatTitle"), true, bld.LectureHalls);

                        //выбор групп
                        if (flatSelectForm.ShowDialog(this) == DialogResult.OK)
                        {
                            new XLSExporter().ExportFlatsShedule(_storage, _shedule, flatSelectForm.SelectedItems, false);
                        }
                    }
                }
            }
            else
            {
                IList buildingList = _storage.ObjectList(typeof(Building));
                SelectForm buildingSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.BuildingTitle"), false, buildingList);

                if (buildingSelectForm.ShowDialog(this) == DialogResult.OK)
                {
                    if (buildingSelectForm.SelectedItem != null)
                    {
                        Building bld = buildingSelectForm.SelectedItem as Building;

                        SelectForm flatSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.FlatTitle"), true, bld.LectureHalls);

                        //выбор групп
                        if (flatSelectForm.ShowDialog(this) == DialogResult.OK)
                        {
                            new XLSExporter().ExportFlatsShedule2(_storage, _shedule2, flatSelectForm.SelectedItems, false);
                        }
                    }
                }

            }
        }

        private void menuBuildSheduleLec_Click(object sender, EventArgs e)
        {
            if (!isSheduleByDate)
            {
                SelectForm groupSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.GroupTitle"), true, _storage.ObjectList(typeof(Group)));

                //выбор групп
                if (groupSelectForm.ShowDialog(this) == DialogResult.OK)
                {
                    IList groupSelectedList = groupSelectForm.SelectedItems;

                    /* фільтруємо записи для вибору лише тих, котрі підходять */
                    ListFiltrator lsfiltr = new ListFiltrator();
                    lsfiltr.Clear();
                    /* формуємо зміст фільтру */
                    foreach (Group gr in groupSelectedList)
                        lsfiltr.AddCondition(LogicOperations.loOR, "Groups", "", ConditionOperators.coInclude, gr);
                    IList filteredStudyLeading = lsfiltr.Filter(_shedule.GetAllStudyLeading());

                    AutoFillShedule aal = new AutoFillShedule();
                    aal.AutoArrange(_shedule, filteredStudyLeading, _storage.ObjectList(typeof(StudyDay)), _storage.ObjectList(typeof(StudyHour)), _storage.ObjectList(typeof(StudyCycles)), 1);

                    SheduleBrowser sv = new SheduleBrowser(_storage, this._shedule, groupSelectedList);
                    sv.MdiParent = this;
                    sv.Show();
                }
            }
            else
            {
                new NotImplementedException();
            }
        }

        private void menuSheduleByFaculty_Click(object sender, EventArgs e)
        {
            if (!isSheduleByDate)
            {
                menuExportShedule_Click(sender, e);
            }
            else
            {
                IList facultyList = _storage.ObjectList(typeof(Faculty));
                SelectForm facultySelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.FacultyTitle"), false, facultyList);

                if (facultySelectForm.ShowDialog(this) == DialogResult.OK)
                {
                    if (facultySelectForm.SelectedItem != null)
                    {
                        Faculty fclt = facultySelectForm.SelectedItem as Faculty;

                        SelectForm groupSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.GroupTitle"), true, _storage.ObjectList(typeof(Group), fclt));

                        //выбор групп
                        if (groupSelectForm.ShowDialog(this) == DialogResult.OK)
                        {
                            new XLSExporter().ExportGroupsShedule2(this._storage, this._shedule2, groupSelectForm.SelectedItems);
                        }
                    }
                }
            }
        }

        private void menuSheduleByDept_Click(object sender, EventArgs e)
        {
            if (!isSheduleByDate)
            {
                IList deptList = _storage.ObjectList(typeof(Department));

                SelectForm deptSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.DepartmentTitle"), false, deptList);

                if (deptSelectForm.ShowDialog(this) == DialogResult.OK)
                {
                    new XLSExporter().ExportDepartmentShedule(_storage, _shedule, deptSelectForm.SelectedItem as Department);
                }
            }
            else
            {
                IList deptList = _storage.ObjectList(typeof(Department));

                SelectForm deptSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.DepartmentTitle"), false, deptList);

                if (deptSelectForm.ShowDialog(this) == DialogResult.OK)
                {
                    new XLSExporter().ExportDepartmentShedule2(_storage, _shedule2, deptSelectForm.SelectedItem as Department);
                }
            }
        }

        private string _currentShedule = "";

        private void menuSheduleProperties_Click(object sender, EventArgs e)
        {
            ObjectEditor oe = new ObjectEditor(this._storage);
            SheduleDescription sd = new SheduleDescription();

            if (isSheduleByDate)
            {
                sd.Caption = _shedule2.GetSheduleTitle();
                sd.FileName = _shedule2.GetFileName();
                sd.BeginDate = _shedule2.GetSheduleBeginDate();
                sd.EndDate = _shedule2.GetSheduleEndDate();
            }
            else
            {
                /// [1] - shedule Title
                /// [2] - shedule File Name
                /// [3] - shedule Begin Date
                /// [4] - shedule End Date
                /// [5] - shedule Week count
                /// [6] - shedule Day count
                /// [7] - shedule Pair count
                /// [8] - shedule Leading count
                /// [9] - shedule Restrinct count
                /// [10] - shedule Lesson count

                object[] infoList = _shedule.GetSheduleInfo();
                // [0] - shedule Version NOT USED!!!!
                sd.Caption = (string)infoList[1];
                sd.FileName = (string)infoList[2];
                sd.BeginDate = (DateTime)infoList[3];
                sd.EndDate = (DateTime)infoList[4];
                sd.WeeksCount = (int)infoList[5];
                sd.DaysPerWeek = (int)infoList[6];
                sd.PairPerDay = (int)infoList[7];
                sd.LeadingCount = (int)infoList[8];
                sd.RestrictCount = (int)infoList[9];
                sd.LessonCount = (int)infoList[10];
            }

            oe.SelectedObject = sd;
            oe.CanChangeInputMode = false;
            oe.Text = Vocabruary.Translate("SheduleProperty.Caption");
            if (oe.ShowDialog() == DialogResult.OK)
            {
                if (isSheduleByDate)
                {
                    _shedule2.SetSheduleTitle(sd.Caption);
                    _shedule2.SetFileName(sd.FileName);
                    _shedule2.SetSheduleBeginDate(sd.BeginDate);
                    _shedule2.SetSheduleEndDate(sd.EndDate);
                }
                else
                {
                    _shedule.SetSheduleTitle(sd.Caption);
                    _shedule.SetFileName(sd.FileName);
                    _shedule.SetSheduleBeginDate(sd.BeginDate);
                    _shedule.SetSheduleEndDate(sd.EndDate);

                }

                this.Text = Vocabruary.Translate("Title") + " ";

                if (isSheduleByDate)
                    this.Text += _shedule2.GetSheduleTitle();
                else
                    this.Text += _shedule2.GetSheduleTitle();
            }
        }

        private void menuFlatUsingReport_Click(object sender, System.EventArgs e)
        {
            if (!isSheduleByDate)
            {
                RtfReport rtfReport = new RtfReport(this);
                rtfReport.FlatsLoadByBuilding(this._storage, this._shedule);
            }
            else
            {
                new NotImplementedException();
            }
        }


        private void menuNewShedule_Click(object sender, System.EventArgs e)
        {
            ObjectEditor oe = new ObjectEditor(this._storage);
            SheduleDescription sd = new SheduleDescription();

            oe.SelectedObject = sd;
            oe.CanChangeInputMode = false;
            oe.Text = Vocabruary.Translate("SheduleProperty.Caption");
            if (oe.ShowDialog() == DialogResult.OK)
            {
                _shedule = _remoteSheduleServer.NewShedule(sd.FileName + ".shedule");

                if (_shedule != null)
                {
                    this._currentShedule = sd.FileName + ".shedule";
                    _shedule.SetSheduleTitle(sd.Caption);
                    _shedule.SetSheduleBeginDate(sd.BeginDate);
                    _shedule.SetSheduleEndDate(sd.EndDate);

                    if (_shedule2 != null)
                        _shedule2 = null;

                    menuTools.Enabled = menuSheduleProperties.Enabled = menuCloseServer.Enabled =
                    menuSaveShadule.Enabled = menuSaveSheduleAs.Enabled = menuItemScripts.Enabled = true;
                    this.statusBar.Text = _shedule.GetSheduleTitle();
                }
            }
        }

        private void menuOpenExistShedule_Click(object sender, System.EventArgs e)
        {
            BrowseForShedule bfs = new BrowseForShedule();

            foreach (object obj in _remoteSheduleServer.GetAllShedulesInfo())
                bfs.AddSheduleInfo(obj as object[]);

            if (bfs.ShowDialog(this) == DialogResult.OK)
            {
                // Зачиняємо усі раніше відкриті вікна у зв`язку з відкриттям іншого розкладу
                foreach (Form frm in this.MdiChildren)
                    frm.Close();

                _currentShedule = bfs.SelectedShedule;
                if (_currentShedule.EndsWith(".shedule2"))
                {
                    isSheduleByDate = true;
                    _shedule = null;
                    _shedule2 = _remoteSheduleServer.GetSheduleByName2(_currentShedule);
                }
                else
                {
                    isSheduleByDate = false;
                    _shedule2 = null;
                    _shedule = _remoteSheduleServer.GetSheduleByName(_currentShedule);
                }

                if (_shedule != null || _shedule2 != null)
                {
                    menuTools.Enabled = menuItemScripts.Enabled = menuSheduleProperties.Enabled = menuCloseServer.Enabled =
                    menuSaveShadule.Enabled = menuSaveSheduleAs.Enabled = true;
                    if (isSheduleByDate)
                        this.statusBar.Text = Vocabruary.Translate("MainForm.StatusBarText") + _shedule2.GetSheduleTitle();
                    else
                        this.statusBar.Text = Vocabruary.Translate("MainForm.StatusBarText") + _shedule.GetSheduleTitle();
                }
                else
                {
                    menuTools.Enabled = menuSheduleProperties.Enabled = menuCloseServer.Enabled =
                    menuObjects.Enabled = menuItemScripts.Enabled = menuSaveShadule.Enabled = menuSaveSheduleAs.Enabled = false;
                    this.statusBar.Text = "";
                }
            }
        }

        private void menuGroupLoading_Click(object sender, System.EventArgs e)
        {
            if (!isSheduleByDate)
            {
                RtfReport rtfReport = new RtfReport(this);
                rtfReport.GroupLoad(this._storage, this._shedule);
            }
            else
            {
                new NotImplementedException();
            }
        }

        private void menuItem4_Click(object sender, System.EventArgs e)
        {
            IList groupList = _storage.ObjectList(typeof(Group));
            // Получение списка выделенных групп

            IList remobjList = null;
            if (!isSheduleByDate)
            {
                ExtObjKey[] uidListArray = new ExtObjKey[groupList.Count];
                for (int index = 0; index < uidListArray.Length; index++)
                    uidListArray[index] = (groupList[index] as Group).ObjectHardKey;

                remobjList = new ArrayList(_shedule.GetStudyLeadingByGroups(uidListArray));
            }
            else
            {
                ExtObjKey[] uidListArray = new ExtObjKey[groupList.Count];
                for (int index = 0; index < uidListArray.Length; index++)
                {
                    uidListArray[index] = new ExtObjKey();
                    uidListArray[index].UID = _storage.GetUUID((groupList[index] as Group), out uidListArray[index].Signature, out uidListArray[index].Index);
                }

                remobjList = new ArrayList(_shedule2.GetStudyLeadingByGroups(uidListArray));
            }


            // Адаптация нагрузки
            IList adoptobjList = RemoteAdapter.Adapter.AdaptLeadingList(remobjList);

            /* фільтруємо записи для вибору лише тих, котрі підходять */
            ListFiltrator lsfiltr = new ListFiltrator();
            lsfiltr.Clear();
            /* формуємо зміст фільтру */
            lsfiltr.AddCondition(LogicOperations.loEMPTY, "UsedHourByWeek", "", ConditionOperators.coEqual, 0);
            IList filteredStudyLeading = lsfiltr.Filter(adoptobjList);

            // Подготовка отображения формы с нагрузкой
            Browser browser = new Browser(_storage, typeof(StudyLoading), filteredStudyLeading);
            browser.OnAddItem += new Browser.AddMasterItemEvent(OnAddStudyLoading);
            browser.OnDelItem += new Browser.DelMasterItemEvent(OnDelStudyLoading);
            browser.OnChangeItem += new Browser.ChangeMasterItemEvent(OnChangeStudyLoading);

            browser.MdiParent = this;
            browser.Show();
        }

        private void menuItemExportSheduleByTutor_Click(object sender, System.EventArgs e)
        {
            // Експорт розкладу по викладачам які шукаються за кафедрами
            // видаєтся весь розклад викладача
            if (!isSheduleByDate)
            {
                IList deptList = _storage.ObjectList(typeof(Department));

                SelectForm deptSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.DepartmentTitle"), false, deptList);

                if (deptSelectForm.ShowDialog(this) == DialogResult.OK)
                {
                    if (deptSelectForm.SelectedItem != null)
                    {
                        Department dept = deptSelectForm.SelectedItem as Department;

                        SelectForm tutorSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.TutorTitle"), true, dept.DepartmentTutors);

                        //выбор викладачів
                        if (tutorSelectForm.ShowDialog(this) == DialogResult.OK)
                        {
                            new XLSExporter().ExportTutorsShedule(this._storage, this._shedule, tutorSelectForm.SelectedItems);
                        }
                    }
                }
            }
            else
            {
                IList deptList = _storage.ObjectList(typeof(Department));

                SelectForm deptSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.DepartmentTitle"), false, deptList);

                if (deptSelectForm.ShowDialog(this) == DialogResult.OK)
                {
                    if (deptSelectForm.SelectedItem != null)
                    {
                        Department dept = deptSelectForm.SelectedItem as Department;

                        SelectForm tutorSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.TutorTitle"), true, dept.DepartmentTutors);

                        //выбор викладачів
                        if (tutorSelectForm.ShowDialog(this) == DialogResult.OK)
                        {
                            new XLSExporter().ExportTutorsShedule2(this._storage, this._shedule2, tutorSelectForm.SelectedItems);
                        }
                    }
                }

            }
        }

        private void menuItemScript_Click(object sender, EventArgs e)
        {
            // Обробка натиснення кнопки
            if (sender is MenuItem)
            {
                int index = (int)(sender as MenuItem).Tag;
                MethodInfo mi = _scripts[index].GetType().GetMethod("RunScript");
                if (mi != null)
                    mi.Invoke(_scripts[index], new object[] { this, _storage, _shedule });
            }
        }

        private void menuItem1_Click(object sender, System.EventArgs e)
        {
            //	SheduleBrowser2 shbrowser2 = new SheduleBrowser2(_storage, _shedule, new ArrayList());
            //	shbrowser2.MdiParent = this;
            //	shbrowser2.Show();
            //
            //	IShedule2 shedule2 = new RemoteShedule2(7, 6);
            //	shedule2.SetSheduleBeginDate(DateTime.Parse("01.09.2006"));
            //	shedule2.SetSheduleEndDate(DateTime.Parse("30.12.2006"));

            //	int key = SheduleKeyBuilder.CompileKey(shedule2, DateTime.Parse("30.12.2006"), 3);

            //	DateTime dt; 
            //	int pair;

            //	SheduleKeyBuilder.DecompileKey(shedule2, key, out dt, out pair);

            //	IList week5 = SheduleKeyBuilder.WeekDatesList(shedule2, 5);

            //	MessageBox.Show(dt.ToShortDateString());
        }

        private void menuNewSheduleByDate_Click(object sender, System.EventArgs e)
        {
            ObjectEditor oe = new ObjectEditor(this._storage);
            SheduleDescription sd = new SheduleDescription();

            oe.SelectedObject = sd;
            oe.CanChangeInputMode = false;
            oe.Text = Vocabruary.Translate("SheduleProperty.Caption");
            if (oe.ShowDialog() == DialogResult.OK)
            {
                _shedule2 = _remoteSheduleServer.NewShedule2(sd.FileName + ".shedule2");

                if (_shedule2 != null)
                {
                    this._currentShedule = sd.FileName + ".shedule2";
                    _shedule2.SetSheduleTitle(sd.Caption);
                    _shedule2.SetSheduleBeginDate(sd.BeginDate);
                    _shedule2.SetSheduleEndDate(sd.EndDate);

                    this.isSheduleByDate = true;
                    if (_shedule != null)
                        _shedule = null;

                    menuTools.Enabled = menuSheduleProperties.Enabled = menuCloseServer.Enabled =
                    menuSaveShadule.Enabled = menuSaveSheduleAs.Enabled = menuItemScripts.Enabled = true;
                    this.statusBar.Text = _shedule2.GetSheduleTitle();
                }
            }
        }

        private void menuSheduleValue_Click(object sender, System.EventArgs e)
        {

        }

        private void menuBuildSheduleLab_Click(object sender, System.EventArgs e)
        {

        }

        private void menuBuildSheduleAll_Click(object sender, System.EventArgs e)
        {

        }

        private void menuImportSupplement_Click(object sender, System.EventArgs e)
        {
            if (!isSheduleByDate)
            {
                Shturman.Shedule.Import.New.ImportForm imf = new Shturman.Shedule.Import.New.ImportForm(_storage, _shedule);
                imf.MdiParent = this;
                imf.Show();
            }
            else
            {
                new NotImplementedException();
            }
        }

        private void menuExportRestrictsTutorsTutors_Click(object sender, System.EventArgs e)
        {
            if (!isSheduleByDate)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "XML Files (*.xml)|*.xml";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    XmlTextWriter xmlwr = new XmlTextWriter(sfd.FileName, Encoding.UTF8);
                    try
                    {
                        new XMLExporter(this._shedule, this._storage).ExportTutorsRestricts(xmlwr);
                    }
                    finally
                    {
                        xmlwr.Flush();
                        xmlwr.Close();
                    }
                }
            }
            else
            {
                new NotImplementedException();
            }
        }

        private void menuExportRestrictsFlats_Click(object sender, System.EventArgs e)
        {
            if (!isSheduleByDate)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "XML Files (*.xml)|*.xml";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    XmlTextWriter xmlwr = new XmlTextWriter(sfd.FileName, Encoding.UTF8);
                    try
                    {
                        new XMLExporter(this._shedule, this._storage).ExportFlatsRestricts(xmlwr);
                    }
                    finally
                    {
                        xmlwr.Flush();
                        xmlwr.Close();
                    }
                }
            }
            else
            {
                new NotImplementedException();
            }
        }

        private void menuExportRestrictsGroups_Click(object sender, System.EventArgs e)
        {
            if (!isSheduleByDate)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "XML Files (*.xml)|*.xml";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    XmlTextWriter xmlwr = new XmlTextWriter(sfd.FileName, Encoding.UTF8);
                    try
                    {
                        new XMLExporter(this._shedule, this._storage).ExportGroupsRestricts(xmlwr);
                    }
                    finally
                    {
                        xmlwr.Flush();
                        xmlwr.Close();
                    }
                }
            }
            else
            {
                new NotImplementedException();
            }

        }

        private void menuImportRestricts_Click(object sender, System.EventArgs e)
        {
            if (!isSheduleByDate)
            {
                OpenFileDialog opfdlg = new OpenFileDialog();

                opfdlg.Filter = "XML Files (*.xml)|*.xml";

                if (opfdlg.ShowDialog() == DialogResult.OK)
                {
                    XmlTextReader xmlreader = new XmlTextReader(opfdlg.FileName);
                    try
                    {
                        new XMLImporter(this._shedule, this._storage).Import(xmlreader);
                    }
                    finally
                    {
                        xmlreader.Close();
                    }
                }
            }
            else
            {
                new NotImplementedException();
            }
        }

        private void menuExportLeadingDept_Click(object sender, System.EventArgs e)
        {
            if (!isSheduleByDate)
            {
                SaveFileDialog sfd = new SaveFileDialog();
                sfd.Filter = "XML Files (*.xml)|*.xml";
                if (sfd.ShowDialog() == DialogResult.OK)
                {
                    IList departmentList = _storage.ObjectList(typeof(Department));
                    SelectForm deptSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.DepartmentTitle"), false, departmentList);
                    Cursor = Cursors.Default;
                    if (deptSelectForm.ShowDialog(this) == DialogResult.OK)
                    {
                        XmlTextWriter xmlwr = new XmlTextWriter(sfd.FileName, Encoding.UTF8);
                        try
                        {
                            ExtObjKey deptObjectHardKey = (deptSelectForm.SelectedItem as Department).ObjectHardKey;
                            new XMLExporter(this._shedule, this._storage).ExportDeptLeading(xmlwr, deptObjectHardKey);
                        }
                        finally
                        {
                            xmlwr.Flush();
                            xmlwr.Close();
                        }
                    }
                }
            }
            else
            {
                new NotImplementedException();
            }
        }

        private void menuImportLeading_Click(object sender, System.EventArgs e)
        {
            if (!isSheduleByDate)
            {
                OpenFileDialog opfdlg = new OpenFileDialog();

                opfdlg.Filter = "XML Files (*.xml)|*.xml";

                if (opfdlg.ShowDialog() == DialogResult.OK)
                {
                    XmlTextReader xmlreader = new XmlTextReader(opfdlg.FileName);
                    try
                    {
                        new XMLImporter(this._shedule, this._storage).Import(xmlreader);
                    }
                    finally
                    {
                        xmlreader.Close();
                    }
                }
            }
            else
            {
                new NotImplementedException();
            }
        }

        private void menuRestrictFromShdule_Click(object sender, System.EventArgs e)
        {
            if (!isSheduleByDate)
            {
                new NotImplementedException();
            }
            else
            {
                SelectForm sheduleSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.SheduleTitle"),
                false, _remoteSheduleServer.GetAllShedules());

                if (sheduleSelectForm.ShowDialog(this) == DialogResult.OK)
                {
                    string fromSheduleName = sheduleSelectForm.SelectedItem.ToString();
                    if (fromSheduleName.EndsWith(".shedule2"))
                    {
                        IShedule2 fromShedule2 = _remoteSheduleServer.GetSheduleByName2(fromSheduleName);

                        DateTime beginDate = fromShedule2.GetSheduleBeginDate();
                        DateTime endDate = this._shedule2.GetSheduleEndDate();

                        if (beginDate < this._shedule2.GetSheduleBeginDate())
                            beginDate = this._shedule2.GetSheduleBeginDate();

                        while (beginDate <= endDate)
                        {
                            foreach (StudyHour sh in _storage.ObjectList(typeof(StudyHour)))
                                if (sh != null)
                                {
                                    int pair = int.Parse(sh.MnemoCode);
                                    foreach (RemoteLesson2 rl2 in fromShedule2.GetLessons(beginDate, pair))
                                    {
                                        RemoteRestrict2 rr2 = new RemoteRestrict2();
                                        rr2.date = beginDate;
                                        rr2.pair = pair;
                                        rr2.Text = "#" + RemoteAdapter.Adapter.ExtractGroupViewString(rl2);

                                        foreach (ExtObjKey lh_key in rl2.flatList)
                                            this._shedule2.InsertFlatRestrict(beginDate, pair, lh_key, rr2);

                                        foreach (ExtObjKey grp_key in rl2.groupList)
                                            this._shedule2.InsertGroupRestrict(beginDate, pair, grp_key, rr2);

                                        foreach (ExtObjKey ttr_key in rl2.tutorList)
                                            this._shedule2.InsertTutorRestrict(beginDate, pair, ttr_key, rr2);
                                    }
                                }
                            beginDate = beginDate.AddDays(1);
                        }
                    }
                }
            }
        }

        private void menuUsers_Click(object sender, System.EventArgs e)
        {
            // Display a wait cursor while the TreeNodes are being created.
            Cursor.Current = Cursors.WaitCursor;

            Browser browser = new Browser(_storage, typeof(ShturmanUser), _storage.ObjectList(typeof(ShturmanUser)));
            browser.MdiParent = this;
            browser.Show();

            // Reset the cursor to the default for all controls.
            Cursor.Current = Cursors.Default;
        }

        private void menuItem8_Click(object sender, System.EventArgs e)
        {
            /* Перенесення розкладу з іншого розкладу */

            // вибіраємо розклад з якого треба імпортувати
            SelectForm sheduleSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.SheduleTitle"), false, _remoteSheduleServer.GetAllShedules());

            Cursor = Cursors.Default;
            if (sheduleSelectForm.ShowDialog(this) == DialogResult.OK)
            {
                Cursor = Cursors.WaitCursor;

                string _fromSheduleCaption = sheduleSelectForm.SelectedItem.ToString();
                if (_fromSheduleCaption.EndsWith(".shedule"))
                {
                    IShedule _fromShedule = _remoteSheduleServer.GetSheduleByName(_fromSheduleCaption);

                    // Вибір груп для перенесення
                    SelectForm groupSelectForm = new SelectForm(Vocabruary.Translate("BrowseDlg.GroupTitle"), true, _storage.ObjectList(typeof(Group)));

                    Cursor = Cursors.Default;
                    //выбор групп
                    if (groupSelectForm.ShowDialog(this) == DialogResult.OK)
                    {
                        Cursor = Cursors.WaitCursor;
                        IList groupSelectedList = groupSelectForm.SelectedItems;

                        IList uidList = new ArrayList();
                        foreach (Group gr in groupSelectedList)
                            uidList.Add(gr.ObjectHardKey);
                        ExtObjKey[] uidListArray = new ExtObjKey[uidList.Count];
                        uidList.CopyTo(uidListArray, 0);

                        IList fromSheduleLeadings = _fromShedule.GetStudyLeadingByGroups(uidListArray);
                        foreach (RemoteStudyLeading rsl in fromSheduleLeadings)
                        {
                            IList fromLessons = _fromShedule.GetLessons(rsl.uid);

                            int newLeadingUid = this._shedule.AddStudyLeading(rsl);

                            foreach (RemoteLesson rl in fromLessons)
                            {
                                RemoteLesson new_rl = rl;
                                new_rl.leading_uid = newLeadingUid;
                                this._shedule.Insert(new_rl.week_index, new_rl.day_index, new_rl.hour_index, new_rl);
                            }
                        }
                    }
                }
            }
            Cursor = Cursors.Default;
        }

        private void menuItem13_Click(object sender, EventArgs e)
        {
            XMLObjectReader oreader = new XMLObjectReader("c:\\test2.xml");
            oreader.ReadAllObject();
            try
            {
                Db4oFactory.Configure().DiscardFreeSpace(25);

                Db4oFactory.Configure().CallConstructors(true);

                // recommended setting for large database files is 8
                Db4oFactory.Configure().BlockSize(8);
                /// Устанавливает максимальное значение для генерируемых идентификаторов
                Db4oFactory.Configure().GenerateUUIDs(Int32.MaxValue);
                Db4oFactory.Configure().GenerateVersionNumbers(Int32.MaxValue);

                /// Устанавливаем глубину обновления объекта, при его изменении
                /// необходимо для обновления значений во вложенных классах, массивах и т.д. 
                Db4oFactory.Configure().UpdateDepth(3);

                /// Данный параметр обязателен так как при значении по умолчанию = 5, прак
                /// тически не выполняются запросы к БД, а точнее по моим догадкам происходит зацикливание
                /// при активации в графе объектов
                Db4oFactory.Configure().ActivationDepth(3);

                Db4oFactory.Configure().Freespace().UseRamSystem();

                Db4objects.Db4o.IObjectContainer objectContainer = Db4objects.Db4o.Db4oFactory.OpenFile("c:\\test1.ycc");
                foreach (object obj in oreader._restoredObjects.Values)
                {
                    objectContainer.Set(obj);

                }
                objectContainer.Commit();

                objectContainer.Close();
            }
            finally
            {
                oreader._restoredObjects.Clear();
            }
        }

        private void menuItem12_Click(object sender, EventArgs e)
        {
            XMLObjectWriter converter = new XMLObjectWriter("c:\\test1.xml");

            try
            {
                try
                {
                    converter.XmlWriter.WriteStartElement("Archive");

                    Type[] arcTypes = new Type[] {
                        typeof(AcademicStatus),
                        typeof(Building),
                        typeof(Faculty),
                        typeof(LectureHallType),
                        typeof(Post),
                        typeof(ShturmanUser),
                        typeof(StudyType),
                        typeof(StudyHour),
                        typeof(StudyDay),
                        typeof(StudyCycles), typeof(Subject), typeof(Department), typeof(Tutor),
                        typeof(Specialty), typeof(Group), typeof(LectureHall)
                    };

                    foreach (Type tp in arcTypes)
                    {
                        converter.XmlWriter.WriteStartElement(tp.Name + "List");
                        foreach (object obj in this._storage.ObjectList(tp))
                        {
                            converter.Object2XML(obj);
                        }
                        converter.XmlWriter.WriteEndElement();
                    }
                    converter.XmlWriter.WriteEndElement();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            finally
            {
                converter.XmlWriter.Flush();
                converter.XmlWriter.Close();
            }
        }

        private void menuItemScriptEditor_Click(object sender, EventArgs e)
        {
            ScriptEditor se = new ScriptEditor();
            se.MdiParent = this;
            se.Show();
        }

        private void menuOptions_Click(object sender, EventArgs e)
        {
            ClientProperties cpfrm = new ClientProperties();
            if (cpfrm.ShowDialog(this) == DialogResult.OK)
            {
                if (cpfrm.Changed)
                {
                    new Vocabruary((NameValueCollection)ConfigurationManager.GetSection(
                    ConfigurationManager.AppSettings["Language"])
                    );

                    foreach (Form frm in this.MdiChildren)
                        frm.Close();

                    this.PrepareControlText();
                }
            }
        }
    }
}