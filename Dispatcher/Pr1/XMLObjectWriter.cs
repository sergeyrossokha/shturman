using System;
using Shturman.Shedule.Objects;

using System.Xml;
using System.Xml.Schema;
using System.Xml.Serialization;

namespace Shturman.Pr1
{
	/// <summary>
	/// Summary description for Converter.
	/// </summary>
	public class XMLObjectWriter
	{
		XmlTextWriter xmlWriter;

        public XMLObjectWriter(string filename)
		{
			xmlWriter = new XmlTextWriter(filename, System.Text.UTF8Encoding.UTF8);
		}

		public void Object2XML(AcademicStatus ast)
		{
			xmlWriter.WriteStartElement("AcademicStatus");
			xmlWriter.WriteAttributeString("uid", ast.Uid.ToString());
			xmlWriter.WriteAttributeString("MnemoCode", ast.MnemoCode);
			xmlWriter.WriteAttributeString("Name", ast.Name);
			xmlWriter.WriteEndElement();
		}


		public void Object2XML(Building bld)
		{
			xmlWriter.WriteStartElement("Build");
			xmlWriter.WriteAttributeString("uid", bld.Uid.ToString());
			xmlWriter.WriteAttributeString("MnemoCode", bld.MnemoCode);
			xmlWriter.WriteAttributeString("Name", bld.Name);
			xmlWriter.WriteEndElement();
		}


		public void Object2XML(Department dpt)
		{
			xmlWriter.WriteStartElement("Department");
			xmlWriter.WriteAttributeString("uid", dpt.Uid.ToString());
			xmlWriter.WriteAttributeString("MnemoCode", dpt.MnemoCode);
			xmlWriter.WriteAttributeString("Name", dpt.Name);
			xmlWriter.WriteAttributeString("Tel", dpt.Telephone);
			// reference
			if (dpt.DepartmentFaculty != null)
				xmlWriter.WriteAttributeString("DepartmentFaculty_ref", dpt.DepartmentFaculty.Uid.ToString());
			
			xmlWriter.WriteStartElement("DepartmentSubjects");
			foreach(Subject sbj in dpt.DepartmentSubjects)
			{
				if (sbj != null)
				{
					xmlWriter.WriteStartElement("ref");
					xmlWriter.WriteString( sbj.Uid.ToString());
					xmlWriter.WriteEndElement();
				}
			}
			xmlWriter.WriteEndElement();

			xmlWriter.WriteStartElement("DepartmentTutors");
			foreach(Tutor ttr in dpt.DepartmentTutors)
			{
				if (ttr != null)
				{
					xmlWriter.WriteStartElement("ref");
					xmlWriter.WriteString( ttr.Uid.ToString() );
					xmlWriter.WriteEndElement();
				}
			}
			xmlWriter.WriteEndElement();

			xmlWriter.WriteEndElement();
		}


		public void Object2XML(Faculty fcl)
		{
			xmlWriter.WriteStartElement("Faculty");
			xmlWriter.WriteAttributeString("uid", fcl.Uid.ToString());
			xmlWriter.WriteAttributeString("MnemoCode", fcl.MnemoCode);
			xmlWriter.WriteAttributeString("Name", fcl.Name);
			xmlWriter.WriteEndElement();
		}


		public void Object2XML(Group grp)
		{
			xmlWriter.WriteStartElement("Group");
			xmlWriter.WriteAttributeString("uid", grp.Uid.ToString());
			xmlWriter.WriteAttributeString("MnemoCode", grp.MnemoCode);
			
			if (grp.GroupFaculty != null)
				xmlWriter.WriteAttributeString("Faculty_ref", grp.GroupFaculty.Uid.ToString());
			
			if(grp.GroupDepartment != null)
				xmlWriter.WriteAttributeString("Department_ref", grp.GroupDepartment.Uid.ToString());
			
			if(grp.GroupSpecialty != null)
				xmlWriter.WriteAttributeString("Specialty_ref", grp.GroupSpecialty.Uid.ToString());

			xmlWriter.WriteAttributeString("GroupCapacity", grp.GroupCapacity.ToString());
			xmlWriter.WriteEndElement();
		}


		public void Object2XML(LectureHall lhl)
		{
			xmlWriter.WriteStartElement("LectureHall");
			xmlWriter.WriteAttributeString("uid", lhl.Uid.ToString());
			xmlWriter.WriteAttributeString("MnemoCode", lhl.MnemoCode);
			
			if (lhl.HallType != null)
				xmlWriter.WriteAttributeString("HallType_ref", lhl.HallType.Uid.ToString());
			
			if (lhl.Department != null)
				xmlWriter.WriteAttributeString("Department_ref", lhl.Department.Uid.ToString());
			
			if (lhl.LectureHallBuilding != null)
				xmlWriter.WriteAttributeString("Build_ref", lhl.LectureHallBuilding.Uid.ToString());

			xmlWriter.WriteAttributeString("Capacity", lhl.Capacity.ToString());

			xmlWriter.WriteEndElement();
		}


		public void Object2XML(LectureHallType lht)
		{
			xmlWriter.WriteStartElement("LectureHallType");
			xmlWriter.WriteAttributeString("uid", lht.Uid.ToString());
			xmlWriter.WriteAttributeString("MnemoCode", lht.MnemoCode);
			xmlWriter.WriteAttributeString("Name", lht.Name);
			if (!lht.FlatColor.IsEmpty)
				xmlWriter.WriteAttributeString("Color", lht.FlatColor.ToArgb().ToString());
			xmlWriter.WriteEndElement();
		}
		
		
		public void Object2XML(Post pst)
		{
			xmlWriter.WriteStartElement("Post");
			xmlWriter.WriteAttributeString("uid", pst.Uid.ToString());
			xmlWriter.WriteAttributeString("MnemoCode", pst.MnemoCode);
			xmlWriter.WriteAttributeString("Name", pst.Name);
			xmlWriter.WriteEndElement();
		}

		
		public void Object2XML(Specialty spl)
		{
			xmlWriter.WriteStartElement("Specialty");
			xmlWriter.WriteAttributeString("uid", spl.Uid.ToString());
			xmlWriter.WriteAttributeString("MnemoCode", spl.MnemoCode);
			xmlWriter.WriteAttributeString("Name", spl.Name);
			xmlWriter.WriteAttributeString("ShortName", spl.ShortName);

			if (spl.SpecialtyFaculty != null)
				xmlWriter.WriteAttributeString("Faculty_ref", spl.SpecialtyFaculty.Uid.ToString());
			
			if(spl.SpecialtyDepartment != null)
				xmlWriter.WriteAttributeString("Department_ref", spl.SpecialtyDepartment.Uid.ToString());

			xmlWriter.WriteEndElement();
		}
		

		public void Object2XML(StudyCycles scl)
		{
			xmlWriter.WriteStartElement("StudyCycles");
			xmlWriter.WriteAttributeString("uid", scl.Uid.ToString());
			xmlWriter.WriteAttributeString("MnemoCode", scl.MnemoCode);
			xmlWriter.WriteAttributeString("Name", scl.Name);
			xmlWriter.WriteAttributeString("ShortName", scl.ShortName);
			xmlWriter.WriteEndElement();
		}

		
		public void Object2XML(StudyDay sdy)
		{
			xmlWriter.WriteStartElement("StudyDay");
			xmlWriter.WriteAttributeString("uid", sdy.Uid.ToString());
			xmlWriter.WriteAttributeString("MnemoCode", sdy.MnemoCode);
			xmlWriter.WriteAttributeString("Name", sdy.Name);
			xmlWriter.WriteAttributeString("ShortName", sdy.ShortName);
			xmlWriter.WriteEndElement();
		}


		public void Object2XML(StudyHour shr)
		{
			xmlWriter.WriteStartElement("StudyHour");
			xmlWriter.WriteAttributeString("uid", shr.Uid.ToString());
			xmlWriter.WriteAttributeString("MnemoCode", shr.MnemoCode);
			xmlWriter.WriteAttributeString("FromTime", shr.FromTime.ToString());
			xmlWriter.WriteAttributeString("ToTime", shr.ToTime.ToString());
			xmlWriter.WriteEndElement();
		}


		public void Object2XML(StudyType stp)
		{
			xmlWriter.WriteStartElement("StudyType");
			xmlWriter.WriteAttributeString("uid", stp.Uid.ToString());
			xmlWriter.WriteAttributeString("MnemoCode", stp.MnemoCode);
			xmlWriter.WriteAttributeString("Name", stp.Name);
			xmlWriter.WriteEndElement();
		}

		
		public void Object2XML(Subject sbj)
		{
			xmlWriter.WriteStartElement("Subject");
			xmlWriter.WriteAttributeString("uid", sbj.Uid.ToString());
			xmlWriter.WriteAttributeString("MnemoCode", sbj.MnemoCode);
			xmlWriter.WriteAttributeString("Name", sbj.Name);
			xmlWriter.WriteAttributeString("ShortName", sbj.ShortName);
			xmlWriter.WriteEndElement();
		}


		public void Object2XML(Tutor ttr)
		{
			xmlWriter.WriteStartElement("Tutor");
			xmlWriter.WriteAttributeString("uid", ttr.Uid.ToString());
			xmlWriter.WriteAttributeString("MnemoCode", ttr.MnemoCode);
			xmlWriter.WriteAttributeString("Surname", ttr.TutorSurname);
			xmlWriter.WriteAttributeString("Name", ttr.TutorName);
			xmlWriter.WriteAttributeString("Patronimic", ttr.TutorPatronimic);
			
			if(ttr.TutorPost != null)
				xmlWriter.WriteAttributeString("Post_ref", ttr.TutorPost.Uid.ToString());
			if(ttr.TutorStatus != null)
				xmlWriter.WriteAttributeString("AcademicStatus_ref", ttr.TutorStatus.Uid.ToString());

			xmlWriter.WriteStartElement("TutorSubjects");
			foreach(Subject sbj in ttr.TutorSubjects)
			{
				if (sbj != null)
				{
					xmlWriter.WriteStartElement("ref");
					xmlWriter.WriteString( sbj.Uid.ToString());
					xmlWriter.WriteEndElement();
				}
			}
			xmlWriter.WriteEndElement();

			xmlWriter.WriteStartElement("DepartmentTutors");
			foreach(Department dpt in ttr.TutorDepartments)
			{
				if (dpt != null)
				{
					xmlWriter.WriteStartElement("ref");
					xmlWriter.WriteString( dpt.Uid.ToString() );
					xmlWriter.WriteEndElement();
				}
			}
			xmlWriter.WriteEndElement();

			xmlWriter.WriteEndElement();
		}


        public void Object2XML(ShturmanUser usr)
		{
			xmlWriter.WriteStartElement("User");
			xmlWriter.WriteAttributeString("uid", usr.Uid.ToString());
			xmlWriter.WriteAttributeString("id", usr.UserUid.ToString());
			xmlWriter.WriteAttributeString("Name", usr.Name);
			xmlWriter.WriteAttributeString("Pwd", usr.Password);
			xmlWriter.WriteEndElement();
		}

		
		public void Object2XML(object obj)
		{
			switch (obj.GetType().Name)
			{
				case "AcademicStatus":
					Object2XML(obj as AcademicStatus);
					break;
				case "Building":
					Object2XML(obj as Building);
					break;
				case "Faculty":
					Object2XML(obj as Faculty);
					break;
				case "Subject":
					Object2XML(obj as Subject);
					break;
				case "LectureHallType":
					Object2XML(obj as LectureHallType);
					break;
				case "LectureHall":
					Object2XML(obj as LectureHall);
					break;
				case "Post":
					Object2XML(obj as Post);
					break;
				case "User":
                    Object2XML(obj as ShturmanUser);
					break;
				case "StudyType":
					Object2XML(obj as StudyType);
					break;
				case "StudyHour":
					Object2XML(obj as StudyHour);
					break;
				case "StudyDay":
					Object2XML(obj as StudyDay);
					break;
				case "StudyCycles":
					Object2XML(obj as StudyCycles);
					break;
				case "Specialty":
					Object2XML(obj as Specialty);
					break;
				case "Department":
					Object2XML(obj as Department);
					break;
				case "Tutor":
					Object2XML(obj as Tutor);
					break;
				case "Group":
					Object2XML(obj as Group);
					break;
			}
		}

		public XmlTextWriter XmlWriter
		{
			get { return this.xmlWriter; }
		}
	}
}
