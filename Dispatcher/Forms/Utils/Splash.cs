using System;
using System.Windows.Forms;
using System.Threading;

namespace Shturman.Forms.Utils
{
	/// <summary>
	/// Summary description for Splash.
	/// </summary>
	public class Splash
	{
		private static SplashForm _splash;
		private static Thread _splashThread;

		private Splash()
		{
		}

		private static void ShowThread()
		{
			_splash = new SplashForm();
			Application.Run(_splash);
		}

		public static void Show()
		{
			if(_splashThread != null)
				return;

			_splashThread = new Thread(new ThreadStart(ShowThread));
			_splashThread.CurrentCulture = Thread.CurrentThread.CurrentCulture;
			_splashThread.CurrentUICulture = Thread.CurrentThread.CurrentUICulture;
			_splashThread.IsBackground = true;
			_splashThread.SetApartmentState(ApartmentState.STA);
			_splashThread.Start();
			while(_splash == null) Thread.Sleep(new TimeSpan(100));
		}

		public static void Close()
		{
			if(_splashThread == null)
				return;
			if(_splash == null)
				return;

			try
			{
				if(_splash.InvokeRequired)
					_splash.Invoke(new MethodInvoker(_splash.Close));
				else
					_splash.Close();
			}
			catch
			{
			}

			_splashThread = null;
			_splash = null;
		}

		public static string Message
		{
			get
			{
				if(_splash == null)
					return string.Empty;
				return _splash.Message;
			}
			set
			{
				if(_splash == null)
					return;
		
				_splash.SetMessageText( value );
			}
		}
	}
}
