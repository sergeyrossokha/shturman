﻿using System.ComponentModel;
using System.Windows.Forms;

namespace Shturman.Forms
{
    /// <summary>
    /// Summary description for SplashWindow.
    /// </summary>
    public class SplashForm : Form
    {
        private PictureBox pictureBox1;
        private Label versionLabel;
        private Label labelMessage;
        private PictureBox pictureBox2;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private Container components = null;

        delegate void SetTextCallback(string text);

        public SplashForm()
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();
            versionLabel.Text += Application.ProductVersion;
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Resources.ResourceManager resources = new System.Resources.ResourceManager(typeof(SplashForm));
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.versionLabel = new System.Windows.Forms.Label();
            this.labelMessage = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.SuspendLayout();
            // 
            // pictureBox1
            // 
            this.pictureBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pictureBox1.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(304, 240);
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // versionLabel
            // 
            this.versionLabel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.versionLabel.AutoSize = true;
            this.versionLabel.Cursor = System.Windows.Forms.Cursors.AppStarting;
            this.versionLabel.Location = new System.Drawing.Point(8, 8);
            this.versionLabel.Name = "versionLabel";
            this.versionLabel.Size = new System.Drawing.Size(45, 16);
            this.versionLabel.TabIndex = 2;
            this.versionLabel.Text = "Версия ";
            // 
            // labelMessage
            // 
            this.labelMessage.Location = new System.Drawing.Point(8, 216);
            this.labelMessage.Name = "labelMessage";
            this.labelMessage.Size = new System.Drawing.Size(288, 16);
            this.labelMessage.TabIndex = 3;
            this.labelMessage.Text = "Загрузка данных";
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(265, 5);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(32, 32);
            this.pictureBox2.TabIndex = 4;
            this.pictureBox2.TabStop = false;
            // 
            // SplashForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.ClientSize = new System.Drawing.Size(304, 240);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.labelMessage);
            this.Controls.Add(this.versionLabel);
            this.Controls.Add(this.pictureBox1);
            this.ForeColor = System.Drawing.Color.Black;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SplashForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Загрузка данных";
            this.ResumeLayout(false);

        }
        #endregion

        public void SetMessageText(string text)
        {
            if (this.labelMessage.InvokeRequired)
            {
                SetTextCallback d = new SetTextCallback(SetMessageText);
                this.Invoke(d, new object[] { text });
            }
            else
            {
                this.labelMessage.Text = text;
            }
        }
        public string Message
        {
            get
            {
                return this.labelMessage.Text;
            }
        }
    }
}