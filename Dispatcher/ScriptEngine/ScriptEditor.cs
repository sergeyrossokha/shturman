using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Xml;
using System.Windows.Forms;
using System.IO;
using System.CodeDom.Compiler;
using System.Diagnostics;
using System.Reflection;
using System.Security.Policy;

using Microsoft.CSharp;
using Shturman.Nestor.DataBrowser;

namespace Shturman.Script
{
    public partial class ScriptEditor : Form
    {
        private DotNetScriptCompiler _dnsc = new DotNetScriptCompiler();
        List<string> _allReferenceList = new List<string>();
        List<int> _referenceList = new List<int>();
        private string _currentScriptName = "";

        public ScriptEditor()
        {
            InitializeComponent();
            
            // ������������� ��������
            DirectoryInfo di = new DirectoryInfo("C:\\WINDOWS\\assembly");
            foreach (FileInfo fi in di.GetFiles("*.dll", SearchOption.AllDirectories))
            {
                _allReferenceList.Add(fi.Name);
            }
            
            di = new DirectoryInfo(".");
            foreach (FileInfo fi in di.GetFiles("*.dll"))
            {
                _allReferenceList.Add(fi.Name);
            }
        }

        private void ScriptEditor_Load(object sender, EventArgs e)
        {
           
        }

        private void openStriptButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            ofd.InitialDirectory = ".\\Scripts";
            ofd.Filter = "Shturman Script File(*.ssc)|*.ssc";
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                _currentScriptName = Path.GetFileName(ofd.FileName);

                XmlDocument xmlDocument = new XmlDocument();
                xmlDocument.Load(ofd.FileName);

                foreach (XmlNode xmlNode in xmlDocument.GetElementsByTagName("reference"))
                {
                    string ref_name = xmlNode.Attributes["assembly"].Value;
                    int index = _allReferenceList.IndexOf(ref_name);
                    if (index!=-1)
                        _referenceList.Add(index);

                    /*if (referenceListBox.Items.Contains(ref_name))
                    {
                        int index = referenceListBox.Items.IndexOf(ref_name);
                        referenceListBox.SetItemChecked(index, true);
                    }
                    else
                    {
                        int index = referenceListBox.Items.Add(ref_name);
                        referenceListBox.SetItemChecked(index, true);
                    }*/
                }

                //string[] source = xmlDocument.InnerText.Split('\n');
                //for (int index = 0; index < source.Length; index++ )
                //{
                //    source[index] = source[index].TrimStart(new char[] { '\n', '\r'});
                //}
                
                this.scriptTextBox.Text = xmlDocument.InnerText; //string.Concat(source);
            }
        }

        private void compileStripButton_Click(object sender, EventArgs e)
        {
            saveStripButton_Click(sender, e);
            errorListView.Items.Clear();

            CompilerResults results;
            ArrayList refList = new ArrayList();
            foreach (int index in _referenceList)
            {
                refList.Add(_allReferenceList[index]);
            }

            _dnsc.Compile(_currentScriptName, this.scriptTextBox.Text, refList, out results);

            if (results.Errors.Count > 0)
            {
                errorListView.Items.Clear();
                foreach (CompilerError error in results.Errors)
                {
                    ListViewItem lvi = new ListViewItem(new string[] { error.ErrorNumber, error.Line.ToString(), error.ErrorText });
                    errorListView.Items.Add(lvi);
                }
            }
            else
            {
                ListViewItem lvi = new ListViewItem(new string[] { "", "", "Compile success with 0 errors." });
                errorListView.Items.Add(lvi);

            }
        }

        private void saveStripButton_Click(object sender, EventArgs e)
        {
            XmlTextWriter xmlwr = new XmlTextWriter(_currentScriptName, Encoding.Unicode);
            xmlwr.WriteStartDocument();
            xmlwr.WriteStartElement("shturmanscript");
            foreach (int index in _referenceList)
            {
                xmlwr.WriteStartElement("reference");
                xmlwr.WriteAttributeString("assembly", _allReferenceList[index]);
                xmlwr.WriteEndElement();
            }

            xmlwr.WriteCData(this.scriptTextBox.Text);
            xmlwr.WriteEndElement();
            xmlwr.WriteEndDocument();
            xmlwr.Flush();
            xmlwr.Close();
        }

        private void addReferenceStripButton_Click(object sender, EventArgs e)
        {
            SelectForm sf = new SelectForm("������ ���������", true, _allReferenceList);
            foreach (int key in _referenceList)
                sf.SetSelected(key);

            if (sf.ShowDialog(this) == DialogResult.OK)
            {
                _referenceList.Clear();
                foreach (string str in sf.SelectedItems)
                {
                    int index =_allReferenceList.IndexOf(str);
                    if (index != -1)
                        _referenceList.Add(index);
                }
            }
        }
    }
}