﻿namespace Shturman.Script
{
    partial class ScriptEditor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ScriptEditor));
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.newScriptStripButton = new System.Windows.Forms.ToolStripButton();
            this.openStriptButton = new System.Windows.Forms.ToolStripButton();
            this.saveStripButton = new System.Windows.Forms.ToolStripButton();
            this.compileStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.addReferenceStripButton = new System.Windows.Forms.ToolStripButton();
            this.errorListView = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.label1 = new System.Windows.Forms.Label();
            this.scriptTextBox = new System.Windows.Forms.RichTextBox();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.newScriptStripButton,
            this.openStriptButton,
            this.saveStripButton,
            this.compileStripButton,
            this.toolStripSeparator1,
            this.addReferenceStripButton});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(841, 25);
            this.toolStrip1.TabIndex = 0;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // newScriptStripButton
            // 
            this.newScriptStripButton.Image = ((System.Drawing.Image)(resources.GetObject("newScriptStripButton.Image")));
            this.newScriptStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.newScriptStripButton.Name = "newScriptStripButton";
            this.newScriptStripButton.Size = new System.Drawing.Size(76, 22);
            this.newScriptStripButton.Text = "Створити";
            this.newScriptStripButton.ToolTipText = "Створити новий файл розширення";
            // 
            // openStriptButton
            // 
            this.openStriptButton.Image = ((System.Drawing.Image)(resources.GetObject("openStriptButton.Image")));
            this.openStriptButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openStriptButton.Name = "openStriptButton";
            this.openStriptButton.Size = new System.Drawing.Size(72, 22);
            this.openStriptButton.Text = "Відкрити";
            this.openStriptButton.Click += new System.EventHandler(this.openStriptButton_Click);
            // 
            // saveStripButton
            // 
            this.saveStripButton.Image = ((System.Drawing.Image)(resources.GetObject("saveStripButton.Image")));
            this.saveStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.saveStripButton.Name = "saveStripButton";
            this.saveStripButton.Size = new System.Drawing.Size(74, 22);
            this.saveStripButton.Text = "Зберегти";
            this.saveStripButton.Click += new System.EventHandler(this.saveStripButton_Click);
            // 
            // compileStripButton
            // 
            this.compileStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.compileStripButton.Image = ((System.Drawing.Image)(resources.GetObject("compileStripButton.Image")));
            this.compileStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.compileStripButton.Name = "compileStripButton";
            this.compileStripButton.Size = new System.Drawing.Size(64, 22);
            this.compileStripButton.Text = "Компіляція";
            this.compileStripButton.Click += new System.EventHandler(this.compileStripButton_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // addReferenceStripButton
            // 
            this.addReferenceStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.addReferenceStripButton.Image = ((System.Drawing.Image)(resources.GetObject("addReferenceStripButton.Image")));
            this.addReferenceStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.addReferenceStripButton.Name = "addReferenceStripButton";
            this.addReferenceStripButton.Size = new System.Drawing.Size(65, 22);
            this.addReferenceStripButton.Text = "Посилання";
            this.addReferenceStripButton.ToolTipText = "Додати посилання";
            this.addReferenceStripButton.Click += new System.EventHandler(this.addReferenceStripButton_Click);
            // 
            // errorListView
            // 
            this.errorListView.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3});
            this.errorListView.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.errorListView.FullRowSelect = true;
            this.errorListView.HeaderStyle = System.Windows.Forms.ColumnHeaderStyle.Nonclickable;
            this.errorListView.Location = new System.Drawing.Point(0, 384);
            this.errorListView.Name = "errorListView";
            this.errorListView.Size = new System.Drawing.Size(841, 90);
            this.errorListView.TabIndex = 2;
            this.errorListView.UseCompatibleStateImageBehavior = false;
            this.errorListView.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Код повідомлення";
            this.columnHeader1.Width = 115;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Строка";
            this.columnHeader2.Width = 64;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Повідомлення";
            this.columnHeader3.Width = 590;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.label1.Location = new System.Drawing.Point(0, 371);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(111, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Список повідомлень";
            // 
            // scriptTextBox
            // 
            this.scriptTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
            this.scriptTextBox.Location = new System.Drawing.Point(0, 25);
            this.scriptTextBox.Name = "scriptTextBox";
            this.scriptTextBox.Size = new System.Drawing.Size(841, 346);
            this.scriptTextBox.TabIndex = 6;
            this.scriptTextBox.Text = "";
            // 
            // ScriptEditor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(841, 474);
            this.Controls.Add(this.scriptTextBox);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.errorListView);
            this.Controls.Add(this.toolStrip1);
            this.Name = "ScriptEditor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Редактор скриптів";
            this.Load += new System.EventHandler(this.ScriptEditor_Load);
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton openStriptButton;
        private System.Windows.Forms.ListView errorListView;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ToolStripButton saveStripButton;
        private System.Windows.Forms.ToolStripButton compileStripButton;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.RichTextBox scriptTextBox;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton addReferenceStripButton;
        private System.Windows.Forms.ToolStripButton newScriptStripButton;
    }
}